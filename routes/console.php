<?php

use Illuminate\Foundation\Inspiring;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\GrabController;
use App\Http\Controllers\ApiController;
use App\DA\AdminModel;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');

// Artisan::command('update_aku', function (AdminController $adminmodel) {
// 	$adminmodel->paste_tagihan_data_21();
// });

Artisan::command('mitra_witel_tacticalpro', function () {
	ApiController::mitra_witel_tacticalpro();
});

Artisan::command('rekon_tacticalpro {witel} {resource} {start_date} {end_date}', function ($witel, $resource, $start_date, $end_date) {
	ApiController::rekon_tacticalpro($witel, $resource, $start_date, $end_date);
});

Artisan::command('alarm_sp', function () {
	AdminModel::alarm_sp();
});

Artisan::command('alarm_expired_khs', function () {
	AdminModel::alarm_expired_khs();
});

Artisan::command('get_proaktif {user} {pass}', function ($user, $pass) {
	GrabController::get_proaktif($user, $pass);
});

Artisan::command('inventoryOutMaterial {witel}', function ($witel) {
	GrabController::inventoryOutMaterial($witel);
});

Artisan::command('outMaterialPsb {witel}', function ($witel) {
	GrabController::outMaterialPsb($witel);
});

Artisan::command('telegram_bot {jenis}', function ($jenis, ReportController $rc) {
	$rc->telegram_bot($jenis);
});
