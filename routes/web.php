<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/login', 'LoginController@login_View');
Route::post('/login', 'LoginController@check_login');
Route::get('/reload-captcha', 'LoginController@reloadCaptcha')->name('reload-captcha');
Route::get('/logout', 'LoginController@logout');
Route::post('/sso_saved_updated', 'LoginController@sso_saved_updated');
Route::get('/get_proaktif', 'GrabController@get_proaktif');

Route::get('/dashboard_lpa/{jenis_period}/{witel}/{tgl_hidden}/{mitra}/{khs}/{jenis_dashboard}/{tgl_progress_kerja}', 'ReportController@dashboard_lpa');
Route::get('/dashboard_simple/{jenis_period}/{witel}/{tgl_hidden}/{mitra}/{khs}/{jenis_dashboard}/{tampilan}/{tgl_progress_kerja}', 'ReportController@dashboard_simple');
// Route::get('/bulk', 'AdminController@bulk');

Route::group(['middleware' => 'authlogin'], function() {

	Route::prefix('tools')->group(function () {
		//edit bayangan
		Route::get('/edit/mitra', 'ToolsController@edit_mitra_sh');
		Route::post('/edit/mitra', 'ToolsController@save_mitra_sh');
	});

	Route::group(['middleware' => 'check_role:all'], function () {
		Route::get('/', function(){
			return redirect('/home');
		});

		Route::get('/home', 'ReportController@home');

		Route::get('/dashboard', 'ReportController@dashboard');
		Route::get('/detail_per/{witel}/{tgl_hidden}/{tgl}/{mitra}/{pekerjaan}/{tgl_progress_kerja}/{jenis_period}/{aktor}', 'ReportController@detail_pekerjaan');
		Route::get('/detail_dashboard/{jenis_tbl}/{witel}/{tgl_hidden}/{tgl}/{mitra}/{pekerjaan}/{tampilan}/{tgl_progress_kerja}/{jenis_period}/{aktor}', 'ReportController@detail_dashboard');

		Route::get('/download_listspm/{jenis}/{data?}', 'ReportController@download_spm')->where('data', '(.*)');

		Route::get('/table_sla', 'ReportController@tabel_sla');
		Route::get('/detail_sla/{view_table}/{aktor}/{status}', 'ReportController@detail_sla');
		Route::get('/download_work/{view_table}/{aktor}/{status}', 'ReportController@download_sla');

		Route::get('/get_detail_laporan/{id}', 'ReportController@get_detail_laporan');
		Route::get('/timeline/{id}', 'ReportController@timeline_sp');
	});

	Route::prefix('Admin')->group(function () {
		Route::group(['middleware' => 'check_role:PA,superadmin,operation,super_superadmin'], function () {
			//input data Kontrak
			// Route::get('{id}', 'AdminController@input_pra_con');
			// Route::post('{id}', 'AdminController@save_data');
			//verify BOQ
			// Route::get('verify/boq', 'AdminController@verify_boq');
			// Route::post('verify/boq', 'AdminController@submit_rekon_final');
			//input amandemen
			Route::get('adm/{id}', 'AdminController@input_amand');
			Route::post('adm/{id}', 'AdminController@save_amand');
			//input excel
			// Route::get('excel/{id}', 'AdminController@input_excel');
			// Route::post('excel/{id}', 'AdminController@save_excel');
			//unduh data
			// Route::get('unduh/{type}/{id}', 'AdminController@download');
			Route::post('/sp/reject_data/{id}', 'AdminController@reject_sp');
			Route::post('{jenis}/budgetP/{id}', 'AdminController@sp_budgetP');
			//compare rfc
			Route::get('/compare_rfc', 'AdminController@dashboard_compare_rfc');
			Route::get('/list/compare_rfc/{mitra}', 'AdminController@comp_rfc');
			//material RFC dan WH
			Route::get('/rfc_wh_sync', 'AdminController@rfc_wh_sync');
			//massal
			Route::get('/upload_file_mass', 'AdminController@upload_file_mass');
			Route::post('/upload_massal', 'AdminController@submit_file_mass');
			Route::get('/bank_dokumen_ttd/{jenis}', 'ReportController@edit_bank_dokumen_ttd');
			Route::post('/dok_ttd/insup', 'ReportController@insert_or_update_ttd_dok');
			// Route::post('/bank_dokumen_ttd/{jenis}', 'ReportController@update_bank_dokumen_ttd');
			Route::get('/delete_user/dok/{id}', 'ReportController@delete_user_ttd_Dok');
		});

		Route::get('/get/detail/{id}', 'AdminController@get_detail_data_ajx');

		Route::post('save/filepsb', 'AdminController@save_filepsb');
		Route::get('/download/filepsb/{type}/{id}/{output}', 'AdminController@download_filepsb');
	});

	// Route::group(['middleware' => 'check_role:TL,mitra,superadmin,super_superadmin,regional_superadmin'], function () {
	// 	Route::prefix('tl')->group(function () {
	// 		//PEMBERKASAN
	// 		Route::get('/berkas', 'TlController@list_rekon');
	// 		Route::post('/berkas_submit', 'TlController@submit_boq');
	// 	});
	// });

	Route::prefix('download')->group(function () {
		Route::get('/full_rar/{id}', 'AdminController@download_full_rar');
		Route::get('/boq_op/{id}', 'AdminController@download_boq_op');

		Route::get('/full_zip_psb/{version}/{id}', 'AdminController@download_zip_psb');

		Route::get('/excel/{witel}/{mitra}/{khs}/{period}/{jenis_tanggal}/{tgl}', 'AdminController@download_excel_dashboard');

		Route::get('/rfc/{id}/{rfc}', 'AdminController@download_rfc');
		Route::get('/all_rfc/{id}', 'AdminController@download_rfc_bulk');
	});

	Route::group(['middleware' => 'check_role:mitra,superadmin,super_superadmin,regional_superadmin'], function () {
		Route::prefix('Mitra')->group(function () {
			// Route::get('/excel/tes/{id_ps}', 'AdminController@download_doc');
			//EDIT RFC
			// Route::get('edit_RFC/{id_sp}/{id_lok}', 'MitraController@edit_rfc_lok');
			// Route::post('edit_RFC/{id_sp}/{id_lok}', 'MitraController@update_rfc_lok');
			//PELURUSAN MATERIAL
			// Route::get('pelurusan/material', 'MitraController@pelurusan_material');
			Route::post('/finish_pelurusan_m/{id}', 'MitraController@save_pelurusan_material');
			Route::post('/finish_pelurusan_rfc/{id}', 'MitraController@save_pelurusan_rfc');
		});

		Route::get('/delete_perm/rfc/{id}', 'MitraController@delete_rfc_permanent');
	});

	Route::group(['middleware' => 'check_role:PA,mitra,operation,superadmin,TL,super_superadmin'], function () {
		Route::prefix('Rekon')->group(function () {
			//upload BOQ MBA MELA
			// Route::get('/upload/boq', 'RekonController@uploadBOQ');
			// Route::post('/upload/boq', 'RekonController@save_BOQ_SP');
			Route::get('/create/provisioning/order_based', 'RekonController@createProvOrderBased');
			Route::get('/dashboard', 'RekonController@dashboard');
			Route::get('/dashboardRekonProvisioning', 'RekonController@dashboardRekonProvisioning');
			Route::get('/rekonOrderKproDetail', 'RekonController@rekonOrderKproDetail');
			Route::get('/rekonOrderDnsDetail', 'RekonController@rekonOrderDnsDetail');
			Route::get('/rekonDashboardOrder', 'RekonController@rekonDashboardOrder');
			Route::get('/rekonDashboardOrderDetail', 'RekonController@rekonDashboardOrderDetail');
			Route::get('/dashboardRekonTiang', 'RekonController@dashboardRekonTiang');

			Route::prefix('provisioning')->group(function () {
				Route::prefix('create')->group(function () {
					Route::get('order-based', 'RekonController@provCreateOrderBased');
					Route::post('order-based', 'RekonController@provCreateOrderBasedPost');
					Route::post('order-based-save', 'RekonController@provCreateOrderBasedSave');
				});
				Route::prefix('cutoff')->group(function () {
					Route::get('order-based', 'RekonController@provCutoffOrderBased');
					Route::post('order-based', 'RekonController@provCutoffOrderBasedSave');
				});
				Route::prefix('dashboard')->group(function () {
					Route::get('order', 'RekonController@provDashboardOrder');
					Route::get('order-detail', 'RekonController@provDashboardOrderDetail');
				});
			});
		});
	});

	Route::group(['middleware' => 'check_role:operation,superadmin,super_superadmin,regional_superadmin'], function () {
		Route::prefix('opt')->group(function () {
			Route::get('/delete/upload/{id}', 'OperationController@delete_upload');
			Route::post('/update_rfc/material/{id}', 'OperationController@input_rfc_design');
			Route::get('/setting/wbs/{id}', 'OperationController@setting_wbs');
			Route::post('/setting/wbs/{id}', 'OperationController@update_wbs');
			Route::get('/edit/po/{id}', 'OperationController@edit_po');
			Route::post('/edit/po/{id}', 'OperationController@update_po');
			Route::get('/edit/kolom/{id}', 'OperationController@edit_kolom');
			Route::post('/edit/kolom/{id}', 'OperationController@update_kolom');
			Route::post('/return/upload/{id}', 'OperationController@return_po');
		});
	});

	Route::group(['middleware' => 'check_role:commerce,superadmin,super_superadmin,regional_superadmin'], function () {
		Route::prefix('comm')->group(function () {
			//input PID Utama
			Route::post('rekon_be/{id}', 'CommerceController@rekon_be');

			Route::get('upload_po', 'CommerceController@upload_po');
			Route::get('insert/po', 'CommerceController@insert_po');
			Route::post('insert/po', 'CommerceController@save_po');

			Route::get('edit/po/{id}', 'CommerceController@edit_po');
			Route::post('edit/po/{id}', 'CommerceController@update_po');
		});
	});

	Route::prefix('Report')->group(function () {
		Route::group(['middleware' => 'check_role:PA,superadmin,super_superadmin,regional_superadmin'], function () {
			//dalapa
			Route::get('dalapaBOQ', 'ReportController@dalapac');
			//detail RFC
			Route::get('rfc/detail/{m1}/{m2}', 'ReportController@rfc_get');
		});

		Route::group(['middleware' => 'check_role:PA,superadmin,super_superadmin,regional_superadmin,commerce'], function () {
			//pid list
			Route::get('pid/list', 'ReportController@list_pid');
			Route::get('see/pid', 'ReportController@see_pid');
			Route::get('pid/{jenis}/{id}', 'ReportController@edit_pid');
			Route::post('pid/{jenis}/{id}', 'ReportController@update_main_pid');
		});

		Route::group(['middleware' => 'check_role:operation,superadmin,super_superadmin,regional_superadmin'], function () {
			//list material
			Route::get('data/material', 'ReportController@show_material');
			Route::get('material/input', 'ReportController@input_material');
			Route::post('material/input', 'ReportController@save_material');
			Route::get('material_manual/{id}', 'ReportController@input_material_manual');
			Route::post('material_manual/{id}', 'ReportController@save_material_manual');
			Route::get('delete_material/{id}', 'ReportController@delete_material');
		});

		Route::group(['middleware' => 'check_role:all'], function () {
			//SP MITRA DETAIL
			Route::get('/list/spM/{id_mitra}/{id}', 'ReportController@mitra_list_sp');
		});

		Route::group(['middleware' => 'check_role:PA,admin,commerce,mitra,operation,superadmin,super_superadmin,regional_superadmin'], function () {
			//pengadaan
			// Route::get('pengadaan', 'ReportController@show_pra_kon');
			// Route::get('tesss', 'ReportController@tes_bulk');
			//budget_exceeded
			// Route::get('budget_exceeded', 'ReportController@list_budget_exceeded');

			//material new item
			// Route::get('edit_NI/{id}', 'ReportController@edit_NI');
			// Route::post('edit_NI/{id}', 'ReportController@save_NI');
			// PKS
			// Route::get('/pks/list', 'ReportController@list_pks');
			// Route::get('/input/pks', 'ReportController@input_pks');
			// Route::post('/input/pks', 'ReportController@save_pks');
			// Route::get('/edit/pks/{id}', 'ReportController@edit_pks');
			// Route::post('/edit/pks/{id}', 'ReportController@update_pks');
			//kelengkapa ba
			// Route::get('/add/ba/{id}', 'ReportController@add_Ba');
			// Route::post('/add/ba/{id}', 'ReportController@save_Ba');
			//SP
			// Route::get('/rekon/list/{id}', 'ReportController@list_sp');
			//tagihan 2020
			// Route::get('/tagihan/list', 'ReportController@show_gd');
			Route::get('/list/tag2020', 'ReportController@get_list_tagihan2020');
			Route::get('/input/note/{id}', 'ReportController@add_catatan');
			Route::post('/input/note/{id}', 'ReportController@copas_datagihan');
			Route::get('/edit/tag220/{id}', 'ReportController@edit_tagihan');
			Route::post('/edit/tag220/{id}', 'ReportController@update_tagihan');
			Route::get('/input/tagihan_2020', 'ReportController@input_tag');
			Route::post('/input/tagihan_2020', 'ReportController@post_tag');
			//tagihan 2021
			Route::get('/list/tag2021', 'ReportController@get_list_tagihan2021');
			Route::get('/input/note_21/{id}', 'ReportController@add_catatan_21');
			Route::post('/input/note_21/{id}', 'ReportController@copas_datagihan_21');
			//rfc tidak lurus
			Route::get('/list/rfc/problem', 'ReportController@rfc_problem');
		});
	});

	Route::get('/grab/listdalapa', 'GrabController@grab_listdalapa_by_id_mitra');

	Route::group(['middleware' => 'check_role:operation,mitra,superadmin,super_superadmin,regional_superadmin'], function () {
		Route::get('/{jns}/rfc/pr/{id}', 'MitraController@change_stts_rfc');
	});
	// Route::post('/change_rfc', 'MitraController@change_rfc');

	Route::prefix('tools')->group(function () {
		Route::group(['middleware' => 'check_role:PA,superadmin,super_superadmin,regional_superadmin'], function () {
			Route::get('/user/{id}', 'ToolsController@create_user_mitra');
			Route::post('/user/{id}', 'ToolsController@save_orUp_user');
			Route::get('/delete_user/{id}', 'ToolsController@delete_user_mitra');
			//mitra
			Route::get('/data/mitra', 'ToolsController@show_mitra');
			Route::get('/list/user_dok', 'ToolsController@list_user_dok');
			//input
			Route::get('/mitra/{id}', 'ToolsController@input_mitra');
			Route::post('/mitra/{id}', 'ToolsController@save_mitra');
			//edit
			Route::get('mitra/edit/{id}', 'ToolsController@edit_mitra');
			Route::post('mitra/edit/{id}', 'ToolsController@save_mitra');

			Route::get('hss_psb', 'ToolsController@get_hss_psb');
			Route::post('hss_psb/{witel}', 'ToolsController@save_hss_psb_witel');
			//planning fixed
			Route::get('list_planning_fixed', 'ToolsController@list_planning_fixed');
			Route::get('delete_planning/{id}', 'ToolsController@delete_planning');

			Route::get('planning_fixed', 'ToolsController@planning_fixed');
			Route::post('planning_fixed', 'ToolsController@save_planning_fixed');

			Route::get('planning_rfc/{id}', 'ToolsController@planning_rfc');
			Route::post('planning_rfc/{id}', 'ToolsController@save_planning_rfc');

			Route::get('generate_planning/{id}', 'ToolsController@generate_planning');

			Route::get('summary_rfcr', 'ToolsController@summary_rfc_rfr');
		});

		Route::group(['middleware' => 'check_role:superadmin,super_superadmin,regional_superadmin'], function () {
			//create pid
			Route::get('pid_main/create', 'ToolsController@create_pid_utama');
			Route::post('pid_main/create', 'ToolsController@save_pid');
		});
	});

	// Route::get('/file/kesepakatan/{id}', 'MitraController@download_ba_kesepakatan');
	// Route::get('/sp_full/{id}', 'AdminController@download_excel_ospfo');

	//searching
	Route::get('/searching', 'ReportController@search_data');
	//edit
	Route::get('/setting_pekerjaan', 'LoginController@setting_pekerjaan');
	Route::post('/setting_pekerjaan', 'LoginController@save_pekerjaan');
	//alur
	Route::get('/progress/{id_boq}', 'ProgressController@progress_step');
	Route::post('/progress/{id_boq}', 'ProgressController@crud_step');

	Route::get('/edit_pekerjaan/{id_boq}', 'ProgressController@edit_pekerjaan');
	Route::post('/edit_pekerjaan/{id_boq}', 'ProgressController@update_pekerjaan');

	Route::get('/rollback/{id_boq}', 'ProgressController@rollback_me');
	Route::post('/rollback/{id_boq}', 'ProgressController@update_rollback');

	Route::get('/progressList/{id_step}', 'ProgressController@list_step');
	Route::post('/progressList/{id_step}', 'ProgressController@crud_list_step');

	Route::post('/submit_rfc_used', 'MitraController@submit_used_rfc');
	Route::post('/submit_ket_rfc', 'MitraController@submit_ket_rfc');
	Route::post('/submit_rfc_data', 'MitraController@submit_file_rfc');

	//submit pekerjaan budget exceeded
	// Route::post('submit/pekerjaan_BE/{id}', 'ReportController@update_BE');
	Route::group(['middleware' => 'check_role:PA,operation,superadmin,super_superadmin,regional_superadmin'], function () {
		Route::prefix('monitor')->group(function () {
			Route::get('budget', 'ReportController@monitor_budget');
		});
	});

	//ajax
	Route::prefix('get_ajx')->group(function () {
		Route::get('/inventory', 'MitraController@get_inventory');
		Route::get('/search_inventory_pid', 'ToolsController@get_search_inventory_pid');
		Route::get('/collect_inventory_material', 'ToolsController@get_collect_inventory_material');
		Route::get('/get_tiket', 'ToolsController@get_tiket');
		Route::get('/user_find', 'ReportController@find_user');
		Route::get('/get_proaktif', 'ProgressController@get_detail_proaktif');
		Route::get('/find/used_rfc', 'MitraController@find_used_rfc');
		Route::get('/check_sub_pekerjaan', 'OperationController@check_sub_pekerjaan');
		Route::post('/check_judul_pekerjaan', 'OperationController@check_judul');
		Route::get('/rfc/search/please/help', 'AdminController@get_rfc_detail');
		Route::get('/get/detail/sp', 'AdminController@detail_sp');
		Route::post('/tagihan/list', 'ReportController@result_gd');
		Route::get('/list/ajax_gd', 'ReportController@ajax_list_full_gd');
		Route::get('/getDashboardData', 'ReportController@getDashboardData');
		Route::get('/mitra/search', 'AdminController@mitra_search');
		Route::get('/get_tematik_proaktif', 'CommerceController@get_proaktif_tematik');
		Route::get('/mitra/search/per_witel', 'AdminController@mitra_search_per_witel');
		Route::get('/mitra/search/tag', 'AdminController@mitra_searchtag');
		Route::get('/get_data/pid/witel', 'CommerceController@search_pid');
		Route::get('/search_rfc/base_comp', 'MitraController@search_rfc');
		Route::get('/search_rfc/base_plan', 'MitraController@search_rfc_plan');
		Route::get('/find_material', 'AdminController@search_material');
		Route::get('/load_user_ttd', 'AdminController@load_user_ttd');
		Route::get('/history_user', 'ReportController@history_user');
		Route::get('/rekonDashboard', 'RekonController@ajx_rekonDashboard');
		Route::get('/rekonProvisioning', 'RekonController@ajx_rekonProvisioning');
		Route::get('/rekonTiang', 'RekonController@ajx_rekonTiang');
		Route::get('/rekonProvisioningKPRO', 'RekonController@ajx_rekonProvisioningKPRO');
		Route::get('/rekonOrderKproDetail', 'RekonController@ajx_rekonOrderKproDetail');
		Route::get('/rekonOrderDnsDetail', 'RekonController@ajx_rekonOrderDnsDetail');
		Route::get('/rekonDashboardOrder', 'RekonController@ajx_rekonDashboardOrder');
		Route::get('/rekonDashboardOrderDetail', 'RekonController@ajx_rekonDashboardOrderDetail');

		Route::prefix('rekon')->group(function () {
			Route::prefix('provisioning')->group(function () {
				Route::prefix('dashboard')->group(function () {
					Route::get('order', 'RekonController@ajx_provDashboardOrder');
					Route::get('order-detail', 'RekonController@ajx_provDashboardOrderDetail');
				});
			});
		});
	});

	// Route::get('/manual/input', 'AdminController@manual_script');
	// Route::post('/manual/input', 'AdminController@save_manual_script');
});