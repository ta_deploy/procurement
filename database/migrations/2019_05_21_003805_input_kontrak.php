<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InputKontrak extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('input_kontrak', function (Blueprint $table) {
            $table->increments('id', 30);
            $table->string('z', 20)->unique();
            $table->string('hk_tetap', 20);
            $table->string('pengadaan', 30);
            $table->string('harga_borong', 18);
            $table->string('hk_kerja_sama', 30);
            $table->string('nama_company', 40);
            $table->text('alamat_company');
            $table->string('bank', 30);
            $table->string('rek', 45);
            $table->string('wakil_mitra', 20);
            $table->string('jabatan_mitra', 29);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('input_kontrak');
    }

}
