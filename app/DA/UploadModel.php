<?php

namespace App\DA;

use Illuminate\Database\Eloquent\Model;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;

class UploadModel implements ToModel, WithCalculatedFormulas
{

  public function model(array $row)
  {
    return [
      'name' => $row['0'],
    ];
  }
}
