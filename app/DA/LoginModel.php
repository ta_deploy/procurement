<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;

class LoginModel
{
	public function login($user, $pass)
	{
		return DB::select('SELECT u.id_user, u.password, u.id_karyawan, k.nama, r.id_regu, u.psb_remember_token, r.mainsector, u.maintenance_level, proc_level, k.mitra_amija, mitra_amija_pt, ma.dalapa_mitra_id, Witel_New, nama_company, pup.pekerjaan As peker_pup, pm.alias_nama_company
		FROM user u
		LEFT JOIN 1_2_employee k ON u.id_karyawan = k.nik
		LEFT JOIN mitra_amija ma ON ma.mitra_amija  = k.mitra_amija
		LEFT JOIN procurement_mitra pm ON pm.nama_company = ma.mitra_amija_pt
		LEFT JOIN regu r ON (r.nik1 = u.id_karyawan OR r.nik2 = u.id_karyawan)
		LEFT JOIN procurement_user_pekerjaan pup ON pup.id_user = k.nik
		WHERE u.id_user = ? AND password = MD5(?)
		GROUP BY u.id_user', [$user, $pass]);
	}

	public function login_without_pass($user)
	{
		return DB::select('SELECT u.id_user, u.password, u.id_karyawan, k.nama, r.id_regu, u.psb_remember_token, r.mainsector, u.maintenance_level, proc_level, k.mitra_amija, mitra_amija_pt, ma.dalapa_mitra_id, Witel_New, nama_company, pup.pekerjaan As peker_pup, pm.alias_nama_company
		FROM user u
		LEFT JOIN 1_2_employee k ON u.id_karyawan = k.nik
		LEFT JOIN mitra_amija ma ON ma.mitra_amija  = k.mitra_amija
		LEFT JOIN procurement_mitra pm ON pm.nama_company = ma.mitra_amija_pt
		LEFT JOIN regu r ON (r.nik1 = u.id_karyawan OR r.nik2 = u.id_karyawan)
		LEFT JOIN procurement_user_pekerjaan pup ON pup.id_user = k.nik
		WHERE u.id_user = ? GROUP BY u.id_user', [$user]);
	}

	public static function login_by_token($token)
	{
		//INI SESSION
		return DB::select('SELECT u.id_user, u.password, u.id_karyawan, k.nama, r.id_regu, u.psb_remember_token, r.mainsector, u.maintenance_level, u.proc_level, k.mitra_amija, ma.mitra_amija_pt, ma.dalapa_mitra_id, k.Witel_New, pm.nama_company, k.sub_directorat, pm.id as id_pm, pup.pekerjaan As peker_pup, pm.alias_nama_company
		FROM user u
		LEFT JOIN 1_2_employee k ON u.id_karyawan = k.nik
		LEFT JOIN mitra_amija ma ON ma.mitra_amija  = k.mitra_amija
		LEFT JOIN procurement_mitra pm ON ma.mitra_amija_pt = pm.nama_company
		LEFT JOIN regu r ON (r.nik1 = u.id_karyawan OR r.nik2 = u.id_karyawan)
		LEFT JOIN procurement_user_pekerjaan pup ON pup.id_user = k.nik
		WHERE psb_remember_token = ?
		GROUP BY u.id_user', [$token]);
	}

	public function remembertoke($localUser, $token)
	{
		return DB::table('user')
		->where('id_user', $localUser->id_user)
		->update([
			'psb_remember_token' => $token
		]);
	}

	public static function countMenu()
	{
		$sql = '';

		$level = session('auth')->proc_level;

		if(session('auth')->proc_level == 2)
		{
			$self_comp = ToolsModel::find_mitra(session('auth')->mitra_amija_pt);
			if($self_comp)
			{
				$sql .= "AND pbu.mitra_id = $self_comp->id";
			}
		}

		if(session('auth')->peker_pup)
		{
			$pekerjaan = explode(', ', session('auth')->peker_pup );
			$imp_peker = "'". implode("', '", $pekerjaan) . "'";
			$sql .= " AND pbu.pekerjaan IN ($imp_peker) ";
		}

		if(in_array(session('auth')->proc_level, [44, 6, 7, 8]) )
		{
			$find_witel = DB::Table('promise_witel As w1')
			->leftjoin('promise_witel As w2', 'w2.Regional', '=', 'w1.Regional')
			->where('w1.Witel', session('auth')->Witel_New)
			->get();

			foreach($find_witel as $vw)
			{
				$all_witel[] = $vw->Witel;
			}

			$sql .= " AND pbu.witel IN ('". implode("','", $all_witel) ."')";
		}
		elseif(!in_array(session('auth')->proc_level, [99]) )
		{
			$sql .= " AND pbu.witel = '". session('auth')->Witel_New ."' ";
		}

		return DB::SELECT(
			"SELECT SUM(CASE WHEN pbu.step_id IN (1, 33) THEN 1 ELSE 0 END) as operation_req_pid,
			SUM(CASE WHEN ps.step_after = 2 THEN 1 ELSE 0 END) as commerce_create_pid,
			SUM(CASE WHEN ps.step_after = 3 THEN 1 ELSE 0 END) as operation_upload_justif,
			SUM(CASE WHEN ps.step_after = 4 THEN 1 ELSE 0 END) as proca_s_pen,
			SUM(CASE WHEN ps.step_after = 5 THEN 1 ELSE 0 END) as mitra_sanggup,
			SUM(CASE WHEN ps.step_after = 6 THEN 1 ELSE 0 END) as proca_sp,
			SUM(CASE WHEN $level = 1 THEN
			-- proc area
				CASE WHEN ps2.aktor = 1 AND ps.main_menu = 'Inisiasi' THEN 1 ELSE 0 END
			WHEN $level = 2 THEN
			-- mitra
				CASE WHEN ps2.aktor = 2 AND ps.main_menu = 'Inisiasi' THEN 1 ELSE 0 END
			WHEN $level = 3 THEN
			-- operation
				CASE WHEN ps2.aktor = 3 AND ps.main_menu = 'Inisiasi' THEN 1 ELSE 0 END
			WHEN $level = 4 OR $level = 44 OR $level = 99 THEN
			-- superadmin
				CASE WHEN ps.main_menu = 'Inisiasi' THEN 1 ELSE 0 END
			WHEN $level = 7 THEN
			-- commerce
				CASE WHEN ps2.aktor = 7 AND ps.main_menu = 'Inisiasi' THEN 1 ELSE 0 END
			ELSE 0 END) as Inisiasi,
			-- ini finishing
			SUM(CASE WHEN ps.step_after = 7 THEN 1 ELSE 0 END) as mitra_boq_rek,
			SUM(CASE WHEN ps.step_after = 8 THEN 1 ELSE 0 END) as operation_verif_boq,
			SUM(CASE WHEN ps.step_after = 9 THEN 1 ELSE 0 END) as mitra_pelurusan_material,
			SUM(CASE WHEN ps.step_after = 10 THEN 1 ELSE 0 END) as commerce_budget,
			SUM(CASE WHEN ps.step_after = 11 THEN 1 ELSE 0 END) as proca_ver_doc_area,
			SUM(CASE WHEN ps.step_after = 12 THEN 1 ELSE 0 END) as mitra_lengkapi_nomor,
			SUM(CASE WHEN ps.step_after = 20 THEN 1 ELSE 0 END) as mitra_ttd,
			SUM(CASE WHEN ps.step_after = 13 THEN 1 ELSE 0 END) as procg_ver_doc_reg,
			SUM(CASE WHEN ps.step_after = 22 THEN 1 ELSE 0 END) as finance,
			SUM(CASE WHEN ps.step_after = 23 THEN 1 ELSE 0 END) as cashbank,
			SUM(CASE WHEN ps.step_after = 14 THEN 1 ELSE 0 END) as finish,
			SUM(CASE WHEN pbu.id IS NOT NULL THEN 1 ELSE 0 END) as total,
			SUM(CASE
			WHEN $level = 1 THEN
			-- proc area
			CASE WHEN (ps2.aktor = 1 OR ps2.aktor = 2 AND ps.step_after = 20 ) AND ps.main_menu = 'Finishing'  THEN 1 ELSE 0 END
			WHEN $level = 2 THEN
			-- mitra
				CASE WHEN (ps2.aktor = 2 OR ps2.aktor = 1 AND ps.step_after = 20 ) AND ps.main_menu = 'Finishing'  THEN 1 ELSE 0 END
			WHEN $level = 3 THEN
			-- operation
				CASE WHEN ps2.aktor = 3 AND ps.main_menu = 'Finishing'  THEN 1 ELSE 0 END
			WHEN $level = 4 OR $level = 44 OR $level = 99 THEN
			-- superadmin
				CASE WHEN ps.main_menu = 'Finishing' THEN 1 ELSE 0 END
			WHEN $level = 6 THEN
			-- Proc. Reg
				CASE WHEN ps2.aktor = 6 AND ps.main_menu = 'Finishing' THEN 1 ELSE 0 END
			WHEN $level = 7 THEN
			-- commerce
				CASE WHEN ps2.aktor = 7 AND ps.main_menu = 'Finishing'  THEN 1 ELSE 0 END
			WHEN $level = 8 THEN
			-- finance
				CASE WHEN ps2.aktor = 8 AND ps.main_menu = 'Finishing'  THEN 1 ELSE 0 END
			ELSE 0 END) as Finishing
			FROM procurement_boq_upload pbu
			LEFT JOIN procurement_step ps ON ps.id = pbu.step_id
			LEFT JOIN procurement_step ps2 ON ps2.id = ps.step_after
			LEFT JOIN 1_2_employee emp ON emp.nik_amija = pbu.created_by
			LEFT JOIN procurement_mitra pm ON pbu.mitra_id = pm.id
			WHERE
			pbu.id != '' AND pbu.active = 1	$sql AND ps.active = 1
			GROUP BY pbu.active
			ORDER BY pbu.id ASC")[0];
	}

	public static function insert_update_khs($req)
	{
		$chck = DB::table('procurement_user_pekerjaan')->where('id_user', session('auth')->id_user)->first();
		$pekerjaan = null;

		if($req->pekerjaan)
		{
			$pekerjaan = implode(', ', $req->pekerjaan);
		}

		if($chck)
		{
			DB::table('procurement_user_pekerjaan')->where('id_user', session('auth')->id_user)->update([
				'pekerjaan' => $pekerjaan
			]);
		}
		else
		{
			DB::table('procurement_user_pekerjaan')->insert([
				'id_user'   => session('auth')->id_user,
				'pekerjaan' => $pekerjaan
			]);
		}
	}

	public static function sso_login($username, $password)
	{
		$url = 'http://api.telkomakses.co.id/API/sso/auth_sso_post.php';
		$data = 'username=' . $username . '&password=' . $password;
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$response = curl_exec($ch);
		$hasil = json_decode($response, true);

		return $hasil;
	}

	public static function insert_updated_sso($req)
	{
		return DB::transaction(function () use ($req)
		{
			$user_detail = json_decode($req->user_detail);

			DB::table('1_2_employee')->insert([
				'u_nama'    => $user_detail->nama,
				'nik'       => $user_detail->nik,
				'Witel_New' => $req->witel,
			]);

			$get_step = DB::table('procurement_step')->Where('aktor_nama', $req->level)->first();
			$mitra = preg_replace('/\s+/', '', $req->mitra);

			$check_mitra = DB::table('procurement_mitra')->Where(DB::RAW("REPLACE(REPLACE(REPLACE(REGEXP_REPLACE(nama_company, '[^[:alnum:]]+', ' '), ' ', ''), '\t', ''), '\n', '') = '$mitra' ") )->first();

			if(count($check_mitra) == 0)
			{
				$reset_mitra = trim($req->mitra);

				if(preg_match("/^PT*/", $req->mitra) )
				{
					$exp = explode(' ', '', $reset_mitra);
					array_shift($exp);

					$new_mitra = 'PT '. implode(' ', $exp);
				}
				else
				{
					$new_mitra = 'PT '. $reset_mitra;
				}
			}


			DB::table('procurement_mitra')->insert([
				'nama_company' => $new_mitra
			]);

			DB::table('user')->insert([
				'u_nama'      => $user_detail->nama,
				'pwd'         => $user_detail->pwd,
				'id_user'     => $user_detail->nik,
				'proc_level'  => $get_step->aktor,
				'jenis_login' => 1,
				'reg'         => str_replace('REG', '', $req->regional),
			]);

			return redirect('/');
		});
	}
}
