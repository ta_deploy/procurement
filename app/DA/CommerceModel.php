<?php

namespace App\DA;
use Illuminate\Support\Facades\DB;;

date_default_timezone_set("Asia/Makassar");

class CommerceModel
{
	public static function save_pid($req, $id)
	{
		return DB::transaction(function () use ($req, $id)
		{
			$check_lokasi =  DB::table('procurement_Boq_lokasi')->where('id_upload', $id)->whereNotIn('id', $req->lokasi)->get();
			$lokasi_string = [];

			foreach($check_lokasi as $k => $v){
				$lokasi_string[$v->lokasi] = $v->lokasi;
			}

			if($lokasi_string)
			{
				$get_date = DB::Table('procurement_boq_upload')->where('id', $id)->first();
				$AdminModel = new AdminModel();

				$pph_sp = $AdminModel->find_pph($get_date->tgl_sp);
				$pph = $AdminModel->find_pph($get_date->tgl_ba_rekon);
				$apm_all = ReportModel::get_apm_budget_all();
				$load_data = [];

				foreach($req->lokasi as $k => $v)
				{
					$data = DB::table('procurement_boq_upload As pbu')
					->leftjoin('procurement_Boq_lokasi As pbl', 'pbl.id_upload', '=', 'pbu.id')
					->leftjoin('procurement_req_pid As prp', 'prp.id_lokasi', '=', 'pbl.id')
					->leftjoin('procurement_Boq_design As pbd', 'pbl.id', '=', 'pbd.id_boq_lokasi')
					->leftjoin('procurement_pid As pp', 'pp.id', '=', 'prp.id_pid_req')
					->select('pbu.judul', 'prp.*', 'pp.pid', 'pbl.id As id_lokasi', 'pbl.pid_sto',
						DB::raw("SUM(CASE WHEN pbd.status_design = 0 THEN pbd.material * pbd.sp END) AS total_material_sp"),
						DB::raw("SUM(CASE WHEN pbd.status_design = 0 THEN pbd.jasa * pbd.sp END) AS total_jasa_sp"),
						DB::raw("SUM(CASE WHEN pbd.status_design = 1 THEN pbd.material * pbd.rekon END) AS total_material_rekon"),
						DB::raw("SUM(CASE WHEN pbd.status_design = 1 THEN pbd.jasa * pbd.rekon END) AS total_jasa_rekon"),
						DB::raw("SUM(CASE WHEN pbd.status_design = 1 THEN pbd.material * pbd.sp END) AS total_material_rek_sp"),
						DB::raw("SUM(CASE WHEN pbd.status_design = 1 THEN pbd.jasa * pbd.sp END) AS total_jasa_rek_sp"),
						DB::raw("(SUM(CASE WHEN pbd.status_design = 0 THEN pbd.material * pbd.sp END) + SUM(CASE WHEN pbd.status_design = 0 THEN pbd.jasa * pbd.sp END)) AS gd_sp"),
						DB::raw("(SUM(CASE WHEN pbd.status_design = 1 THEN pbd.material * pbd.rekon END) + SUM(CASE WHEN pbd.status_design = 1 THEN pbd.jasa * pbd.rekon END)) AS gd_rekon"),
						DB::raw("(SUM(CASE WHEN pbd.status_design = 0 THEN pbd.material * pbd.sp END) + SUM(CASE WHEN pbd.status_design = 0 THEN pbd.jasa * pbd.sp END) * $pph_sp) AS gd_ppn_sp"),
						DB::raw("(SUM(CASE WHEN pbd.status_design = 1 THEN pbd.material * pbd.rekon END) + SUM(CASE WHEN pbd.status_design = 1 THEN pbd.jasa * pbd.rekon END) * $pph_sp) AS gd_ppn_rekon"),
						DB::raw("(SUM(CASE WHEN pbd.status_design = 0 THEN pbd.material * pbd.sp END) + SUM(CASE WHEN pbd.status_design = 0 THEN pbd.jasa * pbd.sp END) ) + (SUM(CASE WHEN pbd.status_design = 0 THEN pbd.material * pbd.sp END) + SUM(CASE WHEN pbd.status_design = 0 THEN pbd.jasa * pbd.sp END) * $pph) AS total_sp"),
						DB::raw("(SUM(CASE WHEN pbd.status_design = 1 THEN pbd.material * pbd.rekon END) + SUM(CASE WHEN pbd.status_design = 1 THEN pbd.jasa * pbd.rekon END) ) + (SUM(CASE WHEN pbd.status_design = 1 THEN pbd.material * pbd.rekon END) + SUM(CASE WHEN pbd.status_design = 1 THEN pbd.jasa * pbd.rekon END) * $pph) AS total_rekon") )
					->where([
						['pbl.id', $v]
					])
					->GroupBy('prp.id')
					->first();

					$data->pid = $req->pid[$k];

					$load_data[] = $data;
					$find_k = array_search($data->pid, array_column($apm_all, 'wbs') );

					if($find_k !== FALSE)
					{
						$be = $apm_all[$find_k];
						$load_data[$k]->mns = $be['keperluan']['Material : Non Stock'];
						$load_data[$k]->ms = $be['keperluan']['Material : Stock'];
					}
				}

				return redirect('/progress/'.$id)->with('alerts', [
					['type' => 'warning', 'text' => 'Tidak bisa disubmit karena lokasi '. implode(', ', $lokasi_string) . ' KOSONG!', 'load_data' => $load_data],
				]);
			}

			$idp = $idp_pbl = [];
			$step_id = 2;

			DB::Table('procurement_req_pid')->where('id_upload', $id)->delete();
			$pbu = DB::Table('procurement_boq_upload')->where('id', $id)->first();

			foreach($req->pid as $key => $val)
			{
				$check_pid = DB::SELECT("SELECT (SELECT CONCAT_WS(', ',
				( CASE WHEN (SELECT SUM(pbd.material)
				FROM procurement_Boq_design pbd
				WHERE pbd.id_boq_lokasi = ".$req->lokasi[$key]." AND pbd.jenis_khs= 16) != 0 THEN CONCAT_WS('-', ppc.wbs, '01-01-01' ) ELSE NULL END),
				( CASE WHEN (SELECT SUM(pbd.material)
				FROM procurement_Boq_design pbd
				WHERE pbd.id_boq_lokasi = ".$req->lokasi[$key]." AND (pbd.jenis_khs != 16 OR pbd.jenis_khs IS NULL) ) != 0 THEN CONCAT_WS('-', ppc.wbs, '01-01-02' ) ELSE NULL END),
				( CASE WHEN (SELECT SUM(pbd.jasa)
				FROM procurement_Boq_design pbd
				WHERE pbd.id_boq_lokasi = ".$req->lokasi[$key]." ) != 0 THEN CONCAT_WS('-', ppc.wbs, '01-02-01' ) ELSE NULL END) ) ) As idp
				FROM budget_apm ppc
				WHERE ppc.wbs = '$val'");

				if(count($check_pid) == 0)
				{
					$check_pid = DB::SELECT("SELECT (SELECT CONCAT_WS(', ',
					( CASE WHEN (SELECT SUM(pbd.material)
					FROM procurement_Boq_design pbd
					WHERE pbd.id_boq_lokasi = ".$req->lokasi[$key]." AND pbd.jenis_khs= 16) != 0 THEN CONCAT_WS('-', ppc.sapid, '01-01-01' ) ELSE NULL END),
					( CASE WHEN (SELECT SUM(pbd.material)
					FROM procurement_Boq_design pbd
					WHERE pbd.id_boq_lokasi = ".$req->lokasi[$key]." AND (pbd.jenis_khs != 16 OR pbd.jenis_khs IS NULL) ) != 0 THEN CONCAT_WS('-', ppc.sapid, '01-01-02' ) ELSE NULL END),
					( CASE WHEN (SELECT SUM(pbd.jasa)
					FROM procurement_Boq_design pbd
					WHERE pbd.id_boq_lokasi = ".$req->lokasi[$key]." ) != 0 THEN CONCAT_WS('-', ppc.sapid, '01-02-01' ) ELSE NULL END) ) ) As idp
					FROM proaktif_project ppc
					WHERE ppc.sapid = '$val'")[0];
				}
				else
				{
					$check_pid = $check_pid[0];
				}

				$idp_pbl[$req->lokasi[$key] ][] = $check_pid->idp;
				$idp[] = $check_pid->idp;

				$check_pp = DB::table('procurement_pid')->where('pid', $val)->first();

				if(!$check_pp)
				{
					$id_pp = DB::table('procurement_pid')->insertGetId([
						'pid'        => $val,
						'mitra_id'   => $pbu->mitra_id,
						'jenis_pid'  => 'NON GAMAS',
						'keterangan' => $pbu->jenis_work,
						'created_by' => session('auth')->id_user,
					]);
				}
				else
				{
					$id_pp = $check_pp->id;
				}

				$get_harga = DB::table('procurement_Boq_lokasi')->where('id', $req->lokasi[$key])->first();

				DB::Table('procurement_req_pid')->insert([
					'id_upload'   => $id,
					'id_lokasi'   => $req->lokasi[$key],
					'id_pid_req'  => $id_pp,
					'created_by'  => session('auth')->id_user,
					'budget'      => $get_harga->material_sp + $get_harga->jasa_sp
				]);
			}

			foreach($idp_pbl as $k => $v)
			{
				DB::table('procurement_Boq_lokasi')->where('id', $k)->update([
					'pid_sto' => implode(', ', $v),
				]);
			}

			// $check_step = DB::SELECT("SELECT ps.aktor, pbu.step_id, ps.urutan, ps.action
			// FROM procurement_boq_upload pbu
			// LEFT JOIN procurement_step ps ON ps.id = pbu.step_id
			// WHERE pbu.id = $id ORDER BY ps.urutan DESC");

			// $urutan = (int)round($check_step[0]->urutan, 0, PHP_ROUND_HALF_DOWN);

			// $get_step2 = DB::Table('procurement_step As ps')
			// ->leftjoin('procurement_step As ps2', 'ps2.step_after' , '=', 'ps.id')
			// ->select('ps2.id', 'ps2.nama', 'ps.urutan')
			// ->where('ps.urutan', $urutan)
			// ->first();

			// $step_id = $get_step2->id;

			// $check_rekon = DB::Table('procurement_step As ps')
			// ->leftjoin('procurement_rekon_log As prl', 'ps.id', '=', 'prl.step_id')
			// ->select('prl.*', 'ps.urutan', 'ps.nama')
			// ->where([
			// 	['prl.id_upload', $id],
			// 	['prl.status_step', 'forward']
			// ])
			// ->orderBy('prl.id', 'DESC')
			// ->first();

			// if($check_rekon->urutan > $get_step2->urutan)
			// {
			// 	$step_id = $check_rekon->step_id;
			// }

			DB::table('procurement_boq_upload')->where('id', $id)->update([
				'step_id'     => $step_id,
				'id_project'  => implode(', ', $idp),
				'modified_by' => session('auth')->id_user
			]);

			AdminModel::save_log($step_id, $id);

			$message_new = 'Request PID Untuk Pekerjaan Baru Berhasil Dibuat!';
			$type = 'info';

			return redirect('/progressList/1')->with('alerts', [
				['type' => $type, 'text' => $message_new],
			]);
		});
	}

	public static function get_data_req($id)
	{
		$data = DB::table('procurement_boq_upload As pbu')
		->select('pbu.*', DB::RAW("DATEDIFF(NOW(), pbu.tgl_last_update) as jml_hri"),
			DB::RAW("(SELECT nama FROM 1_2_employee WHERE nik = pbu.modified_by GROUP BY nik) As nama_modif")
		)
		->where([
			['pbu.step_id', $id],
			['active', 1]
		])
		->GroupBy('pbu.id')
		->orderBy('pbu.id', 'DESC');

		if(session('auth')->proc_level == 44)
		{
			$find_witel = DB::Table('promise_witel As w1')
			->leftjoin('promise_witel As w2', 'w2.Regional', '=', 'w1.Regional')
			->where('w1.Witel', session('auth')->Witel_New)
			->get();

			foreach($find_witel as $vw)
			{
				$all_witel[] = $vw->Witel;
			}

			$data->whereIn('pbu.witel', $all_witel);
		}
		elseif(session('auth')->proc_level != 99)
		{
			$data->where('pbu.witel', session('auth')->Witel_New);
		}

		return $data->get();
	}

	public static function get_data_req_exceed()
	{
		return DB::table('procurement_boq_upload As pbu')
		->leftjoin('procurement_req_pid As prp', 'prp.id_upload', '=', 'pbu.id')
		->leftjoin('procurement_pid As pp', 'pp.id', '=', 'prp.id_pid_req')
		->select('pbu.*')
		->where([
			['pp.budget_exceeded', 1]
		])
		->GroupBy('pbu.id')
		->orderBy('pbu.id', 'DESC')
		->get();
	}

	public static function get_ready_budget(...$id)
	{
		$data = DB::table('procurement_boq_upload As pbu')
		->leftjoin('procurement_req_pid As prp', 'pbu.id', '=', 'prp.id_upload')
		->leftjoin('procurement_pid As pp', 'pp.id', '=', 'prp.id_pid_req')
		->leftjoin('procurement_Boq_lokasi As pbl', 'pbl.id_upload', '=', 'pbu.id')
		->select('pbu.*', 'pp.pid', DB::RAW("DATEDIFF(NOW(), pbu.tgl_last_update) as jml_hri"), DB::RAW("(SELECT nama FROM 1_2_employee WHERE nik = pbu.modified_by GROUP BY nik) As nama_modif") )
		->where([
			['pbu.active', 1]
		])
		->whereIn('pbu.step_id', $id)
		->orderBy('pbu.id', 'DESC');

		if(session('auth')->proc_level == 44)
		{
			$find_witel = DB::Table('promise_witel As w1')
			->leftjoin('promise_witel As w2', 'w2.Regional', '=', 'w1.Regional')
			->where('w1.Witel', session('auth')->Witel_New)
			->get();

			foreach($find_witel as $vw)
			{
				$all_witel[] = $vw->Witel;
			}

			$data->whereIn('pbu.witel', $all_witel);
		}
		elseif(session('auth')->proc_level != 99)
		{
			$data->where('pbu.witel', session('auth')->Witel_New);
		}

		return $data->get();
	}

	public static function update_pid($id, $req)
	{
		$pid = $req->pid;
		$check_lokasi = DB::table("procurement_Boq_lokasi As pbl")
		->leftJoin("procurement_Boq_design As pbd", "pbl.id","=","pbd.id_boq_lokasi")
		->select('pbl.*',
		DB::raw("SUM(CASE WHEN pbd.status_design = 0 THEN pbd.material * pbd.sp END) AS total_material_sp"),
		DB::raw("SUM(CASE WHEN pbd.status_design = 0 THEN pbd.jasa * pbd.sp END) AS total_jasa_sp"),
		DB::raw("SUM(CASE WHEN pbd.status_design = 1 THEN pbd.material * pbd.rekon END) AS total_material_rekon"),
		DB::raw("SUM(CASE WHEN pbd.status_design = 1 THEN pbd.jasa * pbd.rekon END) AS total_jasa_rekon"),
		DB::raw("(SUM(CASE WHEN pbd.status_design = 0 THEN pbd.material * pbd.sp END) + SUM(CASE WHEN pbd.status_design = 0 THEN pbd.jasa * pbd.sp END)) AS gd_sp"),
		DB::raw("(SUM(CASE WHEN pbd.status_design = 1 THEN pbd.material * pbd.rekon END) + SUM(CASE WHEN pbd.status_design = 1 THEN pbd.jasa * pbd.rekon END)) AS gd_rekon") )
		->where([
			['id_upload', $id],
			['status_design', 1]
		])
		->whereNotIn('pbl.id', $req->lokasi)
		->GroupBy('pbl.id')
		->get();
		$lokasi_string = [];

		foreach($check_lokasi as $k => $v){
			$lokasi_string[$v->lokasi] = $v->lokasi;
		}

		if($lokasi_string)
		{
			$get_date = DB::Table('procurement_boq_upload')->where('id', $id)->first();
			$AdminModel = new AdminModel();

			$pph_sp = $AdminModel->find_pph($get_date->tgl_sp);
			$pph = $AdminModel->find_pph($get_date->tgl_ba_rekon);
			$apm_all = ReportModel::get_apm_budget_all();
			$load_data = [];

			foreach($req->lokasi as $k => $v)
			{
				$data = DB::table('procurement_boq_upload As pbu')
				->leftjoin('procurement_Boq_lokasi As pbl', 'pbl.id_upload', '=', 'pbu.id')
				->leftjoin('procurement_req_pid As prp', 'prp.id_lokasi', '=', 'pbl.id')
				->leftjoin('procurement_Boq_design As pbd', 'pbl.id', '=', 'pbd.id_boq_lokasi')
				->leftjoin('procurement_pid As pp', 'pp.id', '=', 'prp.id_pid_req')
				->select('pbu.judul', 'prp.*', 'pbl.id as id_lokasi', 'pbu.id As id_upload', 'pp.pid', 'pbl.pid_sto',
					DB::raw("SUM(CASE WHEN pbd.status_design = 0 THEN pbd.material * pbd.sp END) AS total_material_sp"),
					DB::raw("SUM(CASE WHEN pbd.status_design = 0 THEN pbd.jasa * pbd.sp END) AS total_jasa_sp"),
					DB::raw("SUM(CASE WHEN pbd.status_design = 1 THEN pbd.material * pbd.rekon END) AS total_material_rekon"),
					DB::raw("SUM(CASE WHEN pbd.status_design = 1 THEN pbd.jasa * pbd.rekon END) AS total_jasa_rekon"),
					DB::raw("SUM(CASE WHEN pbd.status_design = 1 THEN pbd.material * pbd.sp END) AS total_material_rek_sp"),
					DB::raw("SUM(CASE WHEN pbd.status_design = 1 THEN pbd.jasa * pbd.sp END) AS total_jasa_rek_sp"),
					DB::raw("(SUM(CASE WHEN pbd.status_design = 0 THEN pbd.material * pbd.sp END) + SUM(CASE WHEN pbd.status_design = 0 THEN pbd.jasa * pbd.sp END)) AS gd_sp"),
					DB::raw("(SUM(CASE WHEN pbd.status_design = 1 THEN pbd.material * pbd.rekon END) + SUM(CASE WHEN pbd.status_design = 1 THEN pbd.jasa * pbd.rekon END)) AS gd_rekon"),
					DB::raw("(SUM(CASE WHEN pbd.status_design = 0 THEN pbd.material * pbd.sp END) + SUM(CASE WHEN pbd.status_design = 0 THEN pbd.jasa * pbd.sp END) * $pph_sp) AS gd_ppn_sp"),
					DB::raw("(SUM(CASE WHEN pbd.status_design = 1 THEN pbd.material * pbd.rekon END) + SUM(CASE WHEN pbd.status_design = 1 THEN pbd.jasa * pbd.rekon END) * $pph_sp) AS gd_ppn_rekon"),
					DB::raw("(SUM(CASE WHEN pbd.status_design = 0 THEN pbd.material * pbd.sp END) + SUM(CASE WHEN pbd.status_design = 0 THEN pbd.jasa * pbd.sp END) ) + (SUM(CASE WHEN pbd.status_design = 0 THEN pbd.material * pbd.sp END) + SUM(CASE WHEN pbd.status_design = 0 THEN pbd.jasa * pbd.sp END) * $pph) AS total_sp"),
					DB::raw("(SUM(CASE WHEN pbd.status_design = 1 THEN pbd.material * pbd.rekon END) + SUM(CASE WHEN pbd.status_design = 1 THEN pbd.jasa * pbd.rekon END) ) + (SUM(CASE WHEN pbd.status_design = 1 THEN pbd.material * pbd.rekon END) + SUM(CASE WHEN pbd.status_design = 1 THEN pbd.jasa * pbd.rekon END) * $pph) AS total_rekon") )
				->where([
					['pbl.id', $v]
				])
				->GroupBy('prp.id')
				->first();

				$data->pid = $req->pid[$k];

				$load_data[] = $data;
				$find_k = array_search($data->pid, array_column($apm_all, 'wbs') );

				if($find_k !== FALSE)
				{
					$be = $apm_all[$find_k];
					$load_data[$k]->mns = $be['keperluan']['Material : Non Stock'];
					$load_data[$k]->ms = $be['keperluan']['Material : Stock'];
				}
			}

			return redirect('/progress/'.$id)->with('alerts', [
				['type' => 'warning', 'text' => 'Tidak bisa disubmit karena lokasi '. implode(', ', $lokasi_string) . ' KOSONG!', 'load_data' => $load_data],
			]);
		}

		// foreach($pid as $k => $val)
		// {
			// if(!isset($data[$val]) )
			// {
			// 	$check_budget = DB::SELECT("SELECT pp.*, prp.budget as budget_prp
			// 	FROM procurement_pid As pp
			// 	LEFT JOIN procurement_req_pid prp ON prp.id_pid_req = pp.id
			// 	WHERE pp.id = $val
			// 	GROUP BY pp.id, prp.id_upload")[0];

			// 	$data[$val]['pid']          = $val;
			// 	$data[$val]['pid_nama']     = $check_budget->pid;
			// 	$data[$val]['harga_aktual'] = $check_budget->budget;
			// 	$data[$val]['budget']       = 0;
			// }

			// $data[$val]['budget'] += str_replace('.', '', $budget[$k]);
			// $total += str_replace('.', '', $budget[$k]);
		// }

		$get_rekon = AdminModel::get_data_final($id);
		// $list_pid_be = $idp = [];
		foreach($pid as $key => $val)
		{
			// foreach($data as $k => $valc1)
			// {
			// 	if($valc1['harga_aktual'] < $valc1['budget'])
			// 	{
			// 		$be = 1;
			// 		$step_id = 24;
			// 		$list_pid_be[] = $valc1['pid'];
			// 	}

			// 	if($k == $val)
			// 	{
			// 		$check_budget = DB::SELECT("SELECT pp.*, prp.budget as budget_prp
			// 		FROM procurement_pid As pp
			// 		LEFT JOIN procurement_req_pid prp ON prp.id_pid_req = pp.id
			// 		WHERE pp.id = $val
			// 		GROUP BY pp.id, prp.id_upload")[0];

			// 		DB::Table('procurement_pid')->where('id', $k)->update([
			// 			'budget_exceeded' => $be,
			// 			'budget' => $valc1['harga_aktual'] + $check_budget->budget_prp - str_replace('.', '', $budget[$key])
			// 		]);
			// 	}
			// }

			// $check_pid = DB::SELECT("SELECT (SELECT CONCAT_WS(', ',
			// 	( CASE WHEN (SELECT SUM(pbd.material)
			// 	FROM procurement_Boq_design pbd
			// 	LEFT JOIN procurement_designator pd ON pd.id = pbd.id_design
			// 	WHERE pbd.id_boq_lokasi = ".$req->id_lokasi[$key]." AND pd.design_mitra_id= 16) != 0 THEN CONCAT_WS('-', ppc.pid, '01-01-01' ) ELSE NULL END),
			// 	( CASE WHEN (SELECT SUM(pbd.material)
			// 	FROM procurement_Boq_design pbd
			// 	LEFT JOIN procurement_designator pd ON pd.id = pbd.id_design
			// 	WHERE pbd.id_boq_lokasi = ".$req->id_lokasi[$key]." AND (pd.design_mitra_id != 16 OR pd.design_mitra_id IS NULL)) != 0 THEN CONCAT_WS('-', ppc.pid, '01-01-02' ) ELSE NULL END),
			// 	( CASE WHEN (SELECT SUM(pbd.jasa)
			// 	FROM procurement_Boq_design pbd
			// 	LEFT JOIN procurement_designator pd ON pd.id = pbd.id_design
			// 	WHERE pbd.id_boq_lokasi = ".$req->id_lokasi[$key]." ) != 0 THEN CONCAT_WS('-', ppc.pid, '01-02-01' ) ELSE NULL END) ) ) As idp
			// 	FROM procurement_pid ppc
			// 	WHERE ppc.id = ".$req->pid[$key])[0];

			// $idp[] = $check_pid->idp;
			$check_pp = DB::table('procurement_pid')->where('pid', $val)->first();

			if(!$check_pp)
			{
				$id_pp = DB::table('procurement_pid')->insertGetId([
					'pid'        => $val,
					'mitra_id'   => $get_rekon->mitra_id,
					'jenis_pid'  => 'NON GAMAS',
					'keterangan' => $get_rekon->jenis_work,
					'created_by' => session('auth')->id_user,
				]);
			}
			else
			{
				$id_pp = $check_pp->id;
			}

			$chck_data = DB::Table('procurement_req_pid')->where([
				['id_pid_req', $id_pp],
				['id_lokasi', $req->lokasi[$key] ]
			])->first();

			if($chck_data)
			{
				DB::Table('procurement_req_pid')->where([
					['id_pid_req', $id_pp],
					['id_lokasi', $req->lokasi[$key] ]
				])->update([
					'modified_by' => session('auth')->id_user,
					// 'budget_up' => str_replace('.', '', $req->budget[$key])
					'budget_up' => $get_rekon->total_material_rekon + $get_rekon->total_jasa_rekon
				]);
			}
			else
			{
				DB::Table('procurement_req_pid')->insert([
					'id_upload'  => $id,
					'id_lokasi'  => $req->lokasi[$key],
					'id_pid_req' => $id_pp,
					'budget'     => $get_rekon->total_material_rekon + $get_rekon->total_jasa_rekon,
					'budget_up'  => $get_rekon->total_material_rekon + $get_rekon->total_jasa_rekon,
					'created_by' => session('auth')->id_user,
				]);
			}
		}

		// if($total < $get_rekon->gd_rekon)
		// {
		// 	$msg['direct'] = '/progress/'.$id;
		// 	$msg['isi']['msg'] = [
		// 		'type' => 'danger',
		// 		'text' => 'Budget Tidak Sesuai, Diketahui Selisih ' . number_format(abs($get_rekon->gd_rekon - $total), '2', ',', '.')
		// 	];

		// 	return $msg;
		// }

		$modified_by = session('auth')->id_user;
		// $pd = implode(', ', $idp);

		// $check_data = DB::table('procurement_boq_upload')->where('id', $id)->first();

		// if( ($check_data->total_material_rekon + $check_data->total_jasa_rekon) > $check_data->total_sp)
		// {
		// 	$step_id = 28;
		// 	$message_new = "Budget Berhasil Diperbaharui, Masuk Budget Exceeded Karena Lebih Dari Nilai Plan";
		// 	$type = 'warning';
		// }
		// else
		// {
		// 	$step_id = 10;
		// 	$message_new = 'Budget Berhasil Diperbaharui!';
		// 	$type = 'info';
		// }

		$step_id = 10;
		$message_new = 'Budget Berhasil Diperbaharui!';
		$type = 'info';

		DB::SELECT(
			"UPDATE procurement_boq_upload pbu
			LEFT JOIN procurement_req_pid prp ON prp.id_upload = pbu.id
			LEFT JOIN procurement_pid pp ON pp.id = prp.id_pid_req
			SET pbu.step_id = $step_id, pbu.modified_by = '$modified_by'
			WHERE pbu.id = $id"
		);

		AdminModel::save_log($step_id, $id);

		return redirect('/progressList/9')->with('alerts', [
			['type' => $type, 'text' => $message_new],
		]);
	}

	public static function get_pid_ajx($data, $id_upload)
	{
		$check_mitra = DB::table('procurement_boq_upload As pbu')->where('id', $id_upload)->first();
		$explode_tgl = explode('-', $check_mitra->bulan_pengerjaan);
		return DB::SELECT(
			"SELECT pp.*, pbu.judul,
			SUM(CASE WHEN prp.budget_up != 0 THEN prp.budget_up ELSE prp.budget END ) As budget_up, pbu.active
			 FROM procurement_pid As pp
			LEFT JOIN procurement_boq_upload As pbu ON pbu.mitra_id = pp.mitra_id
			LEFT JOIN procurement_req_pid As prp ON prp.id_pid_req =  pp.id
			WHERE pid LIKE '%$data%' AND pp.mitra_id = $check_mitra->mitra_id
			-- WHERE pid LIKE '%$data%' AND pp.mitra_id = $check_mitra->mitra_id AND pp.created_at LIKE '$explode_tgl[0]%'
			GROUP BY pp.id");
	}

	public static function all_procurement_pid()
	{
		return DB::table('procurement_pid')->get();
	}

	public static function get_lokasi($id, $sd = 0)
	{
		return DB::table("procurement_Boq_lokasi As pbl")
		->leftJoin("procurement_Boq_design As pbd", "pbl.id","=","pbd.id_boq_lokasi")
		->select('pbl.*',
		DB::raw("SUM(CASE WHEN pbd.status_design = 0 THEN pbd.material * pbd.sp END) AS total_material_sp"),
		DB::raw("SUM(CASE WHEN pbd.status_design = 0 THEN pbd.jasa * pbd.sp END) AS total_jasa_sp"),
		DB::raw("SUM(CASE WHEN pbd.status_design = 1 THEN pbd.material * pbd.rekon END) AS total_material_rekon"),
		DB::raw("SUM(CASE WHEN pbd.status_design = 1 THEN pbd.jasa * pbd.rekon END) AS total_jasa_rekon"),
		DB::raw("(SUM(CASE WHEN pbd.status_design = 0 THEN pbd.material * pbd.sp END) + SUM(CASE WHEN pbd.status_design = 0 THEN pbd.jasa * pbd.sp END)) AS gd_sp"),
		DB::raw("(SUM(CASE WHEN pbd.status_design = 1 THEN pbd.material * pbd.rekon END) + SUM(CASE WHEN pbd.status_design = 1 THEN pbd.jasa * pbd.rekon END)) AS gd_rekon") )
		->where([
			['id_upload', $id],
			['status_design', $sd]
		])
		->GroupBy('pbl.id')
		->get();
	}

	public static function get_list_bex($jenis_stp)
	{
		$sql = DB::table('procurement_boq_upload As pbu')
		->leftjoin('procurement_req_pid As prp', 'pbu.id', '=', 'prp.id_upload')
		->leftjoin('procurement_pid As pp', 'pp.id', '=', 'prp.id_pid_req')
		->select('pbu.*', 'pp.id As id_pid', 'pp.pid',
		DB::RAW("(SELECT nama FROM 1_2_employee WHERE nik = pbu.modified_by GROUP BY nik) As nama_modif"),
		DB::RAW("(CASE WHEN pp.be_material_ta = 1 THEN CONCAT_WS('-', pp.pid, '01-01-01') ELSE NULL END) As be_mt"),
		DB::RAW("(CASE WHEN pp.be_material_mitra = 1 THEN CONCAT_WS('-', pp.pid, '01-01-02') ELSE NULL END) As be_mm"),
		DB::RAW("(CASE WHEN pp.be_jasa = 1 THEN CONCAT_WS('-', pp.pid, '01-02-01') ELSE NULL END) As be_j"),
		DB::RAW("DATEDIFF(NOW(), pbu.tgl_last_update) as jml_hri") )
		->where([
			['active', 1],
		])
		->whereIn('pbu.step_id', $jenis_stp)
		->orderBy('pbu.id', 'DESC');

		if(session('auth')->proc_level == 44)
		{
			$find_witel = DB::Table('promise_witel As w1')
			->leftjoin('promise_witel As w2', 'w2.Regional', '=', 'w1.Regional')
			->where('w1.Witel', session('auth')->Witel_New)
			->get();

			foreach($find_witel as $vw)
			{
				$all_witel[] = $vw->Witel;
			}

			$sql->whereIn('pbu.witel', $all_witel);
		}
		elseif(session('auth')->proc_level != 99)
		{
			$sql->where('pbu.witel', session('auth')->Witel_New);
		}

		return $sql->get();
	}

	public static function update_rekon_be($req, $id)
	{
		$data =	ReportModel::get_boq_data($id);

		foreach($req->pid_be as $v)
		{
			$arr['budget_exceeded'] = 1;

			$exp = explode('-', $v);
			$pid_s = $exp[0] .'-'. $exp[1];
			$wbs = $exp[2] .'-'. $exp[3] .'-'. $exp[4];

			if($wbs == '01-01-02')
			{
				$arr['be_material_mitra'] = 1;
			}

			if($wbs == '01-02-01')
			{
				$arr['be_jasa'] = 1;
			}

			if($wbs == '01-01-01')
			{
				$arr['be_material_ta'] = 1;
			}

			$check_data = DB::table('procurement_boq_upload As pbu')
			->leftjoin('procurement_req_pid As prp', 'prp.id_upload', '=', 'pbu.id')
			->leftjoin('procurement_pid As pp', 'pp.id', '=', 'prp.id_pid_req')
			->Where('pp.pid', $pid_s)
			->first();

			if($check_data)
			{
				DB::Table('procurement_pid')->where('pid', $pid_s)->update($arr);
			}
			else
			{
				$renew_data = [];
				foreach(json_decode($req->lokasi_get) as $vv)
				{
					$exp = explode('-', $vv->pid);
					$pid_s2 = $exp[0] .'-'. $exp[1];

					if($pid_s2 == $pid_s)
					{
						$renew_data[$vv->lokasi_id]['pid'] = $pid_s2;
						$renew_data[$vv->lokasi_id]['wbs'][] = $v;
					}
				}

				foreach($renew_data as $k => $v)
				{
					$check_pid = DB::table('procurement_pid')->where('pid', $v['pid'])->first();

					$arr['mitra_id']   = $data->mitra_id;
					$arr['keterangan'] = $data->jenis_work;
					$arr['pid']        = $v['pid'];
					$arr['jenis_pid']  = 'NON GAMAS';

					if(!$check_pid)
					{
						$arr['created_by']  = session('auth')->id_user;
						$id_pid = DB::table('procurement_pid')->insertGetId($arr);
					}
					else
					{
						$id_pid = $check_pid->id;
					}

					$check_pid_req = DB::table('procurement_req_pid')->where([
						['id_upload', $id],
						['id_lokasi', $k],
						['id_pid_req', $id_pid]
					])->first();

					if(!$check_pid_req)
					{
						DB::table('procurement_req_pid')->insert([
							'id_upload'  => $id,
							'id_lokasi'  => $k,
							'id_pid_req' => $id_pid,
							'budget'     => $data->total_material_rekon + $data->total_jasa_rekon,
							'created_by' => session('auth')->id_user
						]);
					}

					$data_lokasi = DB::Table('procurement_Boq_lokasi')->where('id', $k)->first();

					$get_last_pid = explode(', ', $data_lokasi->pid_sto);

					foreach($v['wbs'] as $vvv)
					{
						$get_last_pid[] = $vvv;
					}

					DB::Table('procurement_Boq_lokasi')->where('id', $k)->update([
						'pid_sto' => implode(', ', $get_last_pid)
					]);
				}
			}
		}

		DB::Table('procurement_boq_upload')->where('id', $id)->update([
			'step_id' => 28,
			'detail' => $req->detail
		]);

		AdminModel::save_log(28, $id, $req->detail);


		return $data;
	}

	public static function insert_po($req)
	{
		$nilai_po = str_replace('.', '', $req->nilai_po);

		$id = DB::table('procurement_po')->insertGetId([
			'no_sp'               => $req->no_sp,
			'no_po'               => $req->no_po,
			'tgl_sp'              => $req->tgl_sp,
			'tematik'             => $req->tematik,
			'bulan_terbit'        => $req->bulan,
			'id_proaktif_project' => $req->pro_id_input,
			'nilai_po'            => $nilai_po,
			'created_by'          => session('auth')->id_user,
		]);

		self::handle_uploadPo($req, $id);

		return redirect('/comm/upload_po')->with('alerts', [
			['type' => 'success', 'text' => 'Berhasil Upload Dokumen PO!!'],
		]);
	}

	public static function update_po($req, $id)
	{
		$nilai_po = str_replace('.', '', $req->nilai_po);

		$id = DB::table('procurement_po')->Where('id', $id)->update([
			'no_sp'               => $req->no_sp,
			'no_po'               => $req->no_po,
			'tgl_sp'              => $req->tgl_sp,
			'tematik'             => $req->tematik,
			'bulan_terbit'        => $req->bulan,
			'id_proaktif_project' => $req->pro_id_input,
			'nilai_po'            => $nilai_po,
			'modified_by'          => session('auth')->id_user,
		]);

		self::handle_uploadPo($req, $id);

		return redirect('/comm/upload_po')->with('alerts', [
			['type' => 'success', 'text' => 'Berhasil Edit Dokumen PO!!'],
		]);
	}

	public static function List_PO()
	{
		return DB::Table('procurement_po')->get();
	}

	private static function handle_uploadPo($req, $id_upload)
	{
		$path = public_path() . '/upload/Commerce/' . $id_upload;
		if (!file_exists($path)) {
			if (!mkdir($path, 0770, true)) {
				return 'gagal menyiapkan folder justif';
			}
		}

		if ($req->hasFile('upload_po') )
		{
			$file = $req->file('upload_po');
			try {
				$filename = 'Upload PO.'.strtolower( $file->getClientOriginalExtension() );
				$file->move("$path/PO/", "$filename");
			} catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
				return 'gagal menyimpan PO';
			}
		}

		if ($req->hasFile('upload_sp') )
		{
			$file = $req->file('upload_sp');
			try {
				$filename = 'Upload SP.'.strtolower( $file->getClientOriginalExtension() );
				$file->move("$path/SP/", "$filename");
			} catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
				return 'gagal menyimpan SP';
			}
		}
	}

	public static function get_proaktif($req)
	{
		if($req->isi)
		{

			$collect_used = [];

			if($req->jenis_input == 'input')
			{
				$valid = DB::table('procurement_po')->Where('tematik', $req->isi)->get();

				foreach($valid as $v)
				{
					$proaktif = json_decode($v->id_proaktif_project);
					$collect_used = array_merge($collect_used, $proaktif);
				}
			}

			$po = DB::table('proaktif_project')
			->Where('nama_project', 'like', "%$req->isi%");

			switch ($req->isi) {
				case 'FEEDER':
					$po->OrWhere('nama_project', 'like', "Fee%");
				break;

				default:
					# code...
				break;
			}

			$po = $po->get();

			foreach($po as $k => &$v)
			{
				if(in_array($v->id, $collect_used) )
				{
					unset($po[$k]);
				}
			}
			unset($v);

			return $po;
		}
	}

	public static function find_po($id)
	{
		return DB::Table('procurement_po')->where('id', $id)->first();
	}

}