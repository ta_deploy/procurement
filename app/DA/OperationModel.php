<?php

namespace App\DA;
use Illuminate\Support\Facades\DB;

date_default_timezone_set("Asia/Makassar");

class OperationModel
{
	public static function save_req_pid($req)
	{
		$total_mat = $total_jasa = $jumlah = 0;
    $material = json_decode($req->material_input);
		$get_data_material = AdminModel::get_design();
		$find_material = json_decode(json_encode($get_data_material), TRUE);

    foreach($material as $k => $val)
    {
			$find_me = array_search($val->id, array_column($find_material, 'id') );

      $total_mat  += $val->material * $val->val;
      $total_jasa += $val->jasa * $val->val;
			$jumlah     += $val->val;

      $data_rekon[$k]['id']        = $val->id;
      $data_rekon[$k]['nama']      = $get_data_material[$find_me]->designator;
      $data_rekon[$k]['material']  = $val->material;
      $data_rekon[$k]['jasa']      = $val->jasa;
      $data_rekon[$k]['sp']        = $val->val;
      $data_rekon[$k]['jenis_khs'] = $val->jenis_khs;
      $data_rekon[$k]['rekon']     = 0;
      $data_rekon[$k]['tambah']    = 0;
      $data_rekon[$k]['kurang']    = 0;

			if(empty($val->sto) )
			{
				return back()->with('alerts', [
					['type' => 'warning', 'text' => 'STO Tidak Boleh Kosong']
				])->withInput();
			}

			if(empty($val->sub_jenis) )
			{
				return back()->with('alerts', [
					['type' => 'warning', 'text' => 'Sub Jenis Tidak Boleh Kosong']
				])->withInput();
			}

			if(!trim($val->lokasi))
			{
				return back()->with('alerts', [
					['type' => 'warning', 'text' => 'Lokasi Tidak Boleh Kosong']
				])->withInput();
			}

			$check_judul = DB::Table('procurement_boq_upload As pbu')
			->leftjoin('procurement_step As ps', 'ps.id', '=', 'pbu.step_id')
			->where([
				['judul', trim($req->judul)],
				['pbu.active', 1]
			])->first();

			if($check_judul)
			{
				return back()->with('alerts', [
					['type' => 'warning', 'text' => 'Judul Sudah Pernah Dibuat! Loker Terakhir Adalah '.$check_judul->nama.' ('.$check_judul->aktor_nama.')']
				])->withInput();
			}

      $data_rekon[$k]['lokasi']  		= $val->lokasi;
      $data_rekon[$k]['sto']     		= $val->sto;
			$data_rekon[$k]['urutan_sto'] = 'LOK'.$val->urutan;

			if(!isset($data_lokasi[$val->urutan]['ttl_material_sp']) )
			{
				$data_lokasi[$val->urutan]['ttl_material_sp'] = 0;
				$data_lokasi[$val->urutan]['ttl_jasa_sp']     = 0;
				$data_lokasi[$val->urutan]['jml_vol']         = 0;
			}

      $data_lokasi[$val->urutan]['urutan_sto']   		 =  'LOK'.$val->urutan;
      $data_lokasi[$val->urutan]['sto']          		 =  $val->sto;
      $data_lokasi[$val->urutan]['id_proaktif']  		 =  $val->proaktif_id;
      $data_lokasi[$val->urutan]['lokasi']       		 =  $val->lokasi;
      $data_lokasi[$val->urutan]['sub_jenis']    		 =  $val->sub_jenis;
			$data_lokasi[$val->urutan]['ttl_material_sp'] += $val->material * $val->val;
			$data_lokasi[$val->urutan]['ttl_jasa_sp']     += $val->jasa * $val->val;
			$data_lokasi[$val->urutan]['jml_vol']     		+= $val->val;
    }

		if($jumlah == 0 )
		{
			return back()->with('alerts', [
				['type' => 'warning', 'text' => 'Jumlah Designator Tidak Boleh Kosong!']
			])->withInput();
		}

    $total_data = ['total_sp_material' => $total_mat, 'total_sp_jasa' => $total_jasa];

		foreach($material as $v)
		{
			$find_me = array_search($v->id, array_column($find_material, 'id') );

			$check_dup[$v->id .'_'. $v->material .'_'. $v->jasa .'_'. $v->urutan]['mat'] = $find_material[$find_me]['designator'];

			if(!isset($check_dup[$v->id .'_'. $v->material .'_'. $v->jasa .'_'. $v->urutan]['jml']) )
			{
				$check_dup[$v->id .'_'. $v->material .'_'. $v->jasa .'_'. $v->urutan]['jml'] = 0;
			}
			$check_dup[$v->id .'_'. $v->material .'_'. $v->jasa .'_'. $v->urutan]['jml'] += 1;
		}

		foreach($check_dup as $v)
		{
			if($v['jml'] > 1)
			{
				return redirect('/progress/input')->with('alerts', [
					['type' => 'danger', 'text' => $v['mat'].' terdeteksi dimasukkan dengan menggunakan <b><u>material</u></b> dan <b><u>jasa</u></b> yang sama sebanyak 2 kali!' ]
				])->withInput();
			}
		}

		$pesan = AdminModel::save_boq($req, $total_data, null, $data_rekon, $data_lokasi, 0);

		if($pesan != 'ok')
		{
			return redirect('/progress/input')->with('alerts', [
				['type' => $pesan['alerts']['type'], 'text' => $pesan['alerts']['text'] ]
			])->withInput();
		}

		return redirect('/progressList/input')->with('alerts', [
			['type' => 'success', 'text' => 'BOQ Plan dan Request PID berhasil diolah!']
		]);
	}

	public static function update_req_pid($req, $id)
	{
		$total_mat = $total_jasa = $jumlah = 0;
    $material = json_decode($req->material_input);
		$get_data_material = AdminModel::get_design();
		$find_material = json_decode(json_encode($get_data_material), TRUE);

    foreach($material as $k => $val)
    {
			$find_me = array_search($val->id, array_column($find_material, 'id') );

      $total_mat  += $val->material * $val->val;
      $total_jasa += $val->jasa * $val->val;
			$jumlah     += $val->val;

      $data_rekon[$k]['id']        = $val->id;
      $data_rekon[$k]['nama']      = $get_data_material[$find_me]->designator;
      $data_rekon[$k]['material']  = $val->material;
      $data_rekon[$k]['jasa']      = $val->jasa;
      $data_rekon[$k]['sp']        = $val->val;
      $data_rekon[$k]['jenis_khs'] = $val->jenis_khs;
      $data_rekon[$k]['rekon']     = 0;
      $data_rekon[$k]['tambah']    = 0;
      $data_rekon[$k]['kurang']    = 0;

			if(empty($val->sto) )
			{
				return back()->with('alerts', [
					['type' => 'warning', 'text' => 'STO Tidak Boleh Kosong']
				])->withInput();
			}

			if(empty($val->sub_jenis) )
			{
				return back()->with('alerts', [
					['type' => 'warning', 'text' => 'Sub Jenis Tidak Boleh Kosong']
				])->withInput();
			}

			if(!trim($val->lokasi))
			{
				return back()->with('alerts', [
					['type' => 'warning', 'text' => 'Lokasi Tidak Boleh Kosong']
				])->withInput();
			}

      $data_rekon[$k]['lokasi']  		= $val->lokasi;
      $data_rekon[$k]['sto']     		= $val->sto;
			$data_rekon[$k]['urutan_sto'] = 'LOK'.$val->urutan;

			if(!isset($data_lokasi[$val->urutan]['ttl_material_sp']) )
			{
				$data_lokasi[$val->urutan]['ttl_material_sp'] = 0;
				$data_lokasi[$val->urutan]['ttl_jasa_sp']     = 0;
				$data_lokasi[$val->urutan]['jml_vol']         = 0;
			}

      $data_lokasi[$val->urutan]['urutan_sto']   		 =  'LOK'.$val->urutan;
      $data_lokasi[$val->urutan]['sto']          		 =  $val->sto;
      $data_lokasi[$val->urutan]['id_proaktif']  		 =  $val->proaktif_id;
      $data_lokasi[$val->urutan]['lokasi']       		 =  $val->lokasi;
      $data_lokasi[$val->urutan]['sub_jenis']    		 =  $val->sub_jenis;
			$data_lokasi[$val->urutan]['ttl_material_sp'] += $val->material * $val->val;
			$data_lokasi[$val->urutan]['ttl_jasa_sp']     += $val->jasa * $val->val;
			$data_lokasi[$val->urutan]['jml_vol']     		+= $val->val;
    }

		if($jumlah == 0 )
		{
			return back()->with('alerts', [
				['type' => 'warning', 'text' => 'Jumlah Designator Tidak Boleh Kosong!']
			])->withInput();
		}

    $total_data = ['total_sp_material' => $total_mat, 'total_sp_jasa' => $total_jasa];

		foreach($material as $v)
		{
			$find_me = array_search($v->id, array_column($find_material, 'id') );

			$check_dup[$v->id .'_'. $v->material .'_'. $v->jasa .'_'. $v->urutan]['mat'] = $find_material[$find_me]['designator'];

			if(!isset($check_dup[$v->id .'_'. $v->material .'_'. $v->jasa .'_'. $v->urutan]['jml']) )
			{
				$check_dup[$v->id .'_'. $v->material .'_'. $v->jasa .'_'. $v->urutan]['jml'] = 0;
			}
			$check_dup[$v->id .'_'. $v->material .'_'. $v->jasa .'_'. $v->urutan]['jml'] += 1;
		}

		foreach($check_dup as $v)
		{
			if($v['jml'] > 1)
			{
				return redirect('/progress/input')->with('alerts', [
					['type' => 'danger', 'text' => $v['mat'].' terdeteksi dimasukkan dengan menggunakan <b><u>material</u></b> dan <b><u>jasa</u></b> yang sama sebanyak 2 kali!' ]
				])->withInput();
			}
		}

		$pesan = AdminModel::save_boq($req, $total_data, $id, $data_rekon, $data_lokasi, 0);

		if($pesan != 'ok')
		{
			return redirect('/progress/input')->with('alerts', [
				['type' => $pesan['alerts']['type'], 'text' => $pesan['alerts']['text'] ]
			])->withInput();
		}

		return redirect('/progressList/input')->with('alerts', [
			['type' => 'success', 'text' => 'BOQ Plan dan Request PID berhasil diolah!']
		]);
	}

	public static function ready_justif($id)
	{
		$data = DB::table('procurement_boq_upload As pbu')
		->leftjoin('procurement_req_pid As prp', 'pbu.id', '=', 'prp.id_upload')
		->leftjoin('1_2_employee As emp', 'emp.nik', '=', 'pbu.created_by')
		->leftjoin('procurement_pid As pp', 'pp.id', '=', 'prp.id_pid_req')
		->select('pbu.*', 'emp.nama',
			DB::RAW("DATEDIFF(NOW(), pbu.tgl_last_update) as jml_hri"),
			DB::RAW("(SELECT nama FROM 1_2_employee WHERE nik = pbu.modified_by GROUP BY nik) As nama_modif")
		)
		->where([
			['pbu.step_id', $id],
			['pbu.active', 1]
		])
		->GroupBy('pbu.id')
		->OrderBy('pbu.tgl_last_update', 'DESC');

		if(session('auth')->proc_level == 44)
		{
			$find_witel = DB::Table('promise_witel As w1')
			->leftjoin('promise_witel As w2', 'w2.Regional', '=', 'w1.Regional')
			->where('w1.Witel', session('auth')->Witel_New)
			->get();

			foreach($find_witel as $vw)
			{
				$all_witel[] = $vw->Witel;
			}

			$data->whereIn('pbu.witel', $all_witel);
		}
		elseif(session('auth')->proc_level != 99)
		{
			$data->where('pbu.witel', session('auth')->Witel_New);
		}

		return $data->get();
	}

	public static function get_data_req($step, $id)
	{
		$data = DB::table('procurement_boq_upload As pbu')
		->leftjoin('procurement_step As ps', 'pbu.step_id', '=', 'ps.id')
		->leftjoin('procurement_step As ps2', 'ps.step_after', '=', 'ps2.id')
		->leftjoin('procurement_req_pid As prp', 'pbu.id', '=', 'prp.id_upload')
		->leftJoin("procurement_Boq_lokasi As pbl", "pbu.id","=","pbl.id_upload")
		->leftJoin("procurement_mitra As pm", "pm.id","=","pbu.mitra_id")
		->select('pbu.*', 'pm.nama_company', 'ps2.nama As nama_step',
			DB::RAW("(SELECT nama FROM 1_2_employee WHERE nik = pbu.created_by GROUP BY nik) As nama_modif"),
			DB::raw("ROUND( (CASE WHEN total_sp IS NULL THEN ( total_material_sp + total_jasa_sp ) ELSE total_sp END) ) As gd_sp"),
			DB::RAW("DATEDIFF(NOW(), pbu.tgl_last_update) as jml_hri")
		)
		->where([
			['pbu.active', $id],
		])
		->OrderBy('pbu.id', 'DESC')
		->GroupBy('pbu.id');

		if(session('auth')->proc_level == 44)
		{
			$find_witel = DB::Table('promise_witel As w1')
			->leftjoin('promise_witel As w2', 'w2.Regional', '=', 'w1.Regional')
			->where('w1.Witel', session('auth')->Witel_New)
			->get();

			foreach($find_witel as $vw)
			{
				$all_witel[] = $vw->Witel;
			}

			$data->whereIn('pbu.witel', $all_witel);
		}
		elseif(session('auth')->proc_level != 99)
		{
			$data->where('pbu.witel', session('auth')->Witel_New);
		}

		if(session('auth')->peker_pup)
		{
			$pekerjaan = explode(', ', session('auth')->peker_pup );
			$data->whereIn('pbu.pekerjaan', $pekerjaan );
		}

		if(session('auth')->id_user != 18940469)
		{
			$data->where('pbu.created_by', session('auth')->id_user);
		}

		if(!is_numeric($step) )
		{
			if($step == 'awal')
			{
				$data->where('pbu.step_id', 1);
			}
			else
			{
				$data->where('pbu.step_id', '<=', 13);
			}
		}
		else
		{
			$data->where('pbu.step_id', $step);
		}

		return $data->get();
	}

	public static function upload_justif_ttd($id, $req)
	{
		$msg['direct'] = '/progressList/2';

		$check_pid = DB::Table('procurement_Boq_lokasi')->where('id_upload', $id)->get();

		foreach($check_pid as $v)
		{
			if(!$v->pid_sto)
			{
				$msg['isi']['msg'] = [
					'type' => 'warning',
					'text' => 'Segera atur PID, terdeteksi kosong!'
				];
				return $msg;
			}
		}

		DB::table('procurement_boq_upload')->where('id', $id)->update([
			'waspang_id'  => $req->waspang_id,
			'step_id'     => 3,
			'modified_by' => session('auth')->id_user
		]);

		self::handke_justifTtd($req, $id);

		AdminModel::save_log(3, $id);

		$msg['isi']['msg'] = [
			'type' => 'success',
			'text' => 'Upload Justif berhasil di proses!'
		];
		return $msg;
	}

	private static function handke_justifTtd($req, $id_upload)
	{
		$path = public_path() . '/upload2/' . $id_upload . '/dokumen_justif/justif_ttd/';
		if (!file_exists($path)) {
			if (!mkdir($path, 0770, true)) {
				return 'gagal menyiapkan folder justif';
			}
		}
		if ($req->hasFile('upload_justifikasi')) {
			$file = $req->file('upload_justifikasi');
			try {
				$filename = 'Upload Justif.'.strtolower( $file->getClientOriginalExtension() );
				$file->move("$path", "$filename");
			} catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
				return 'gagal menyimpan upload_justifikasi';
			}
		}
	}

	public static function delete_pid($id)
	{
		$check = ReportModel::get_boq_data($id);
		// dd($check->created_by, session('auth')->id_user);
		if(session('auth')->id_user != 18940469 && session('auth')->id_user != $check->created_by)
		{
			$msg['direct'] = '/progress/input';
			$msg['isi']['msg'] = [
				'type' => 'warning',
				'text' => 'Anda Bukan Pembuat Data Ini!'
			];
		}
		else
		{
			DB::table('procurement_boq_upload')->where('id', $id)->update([
				'active' => 0,
				'modified_by' => session('auth')->id_user
			]);

			$msg['direct'] = '/progressList/input';
			$msg['isi']['msg'] = [
				'type' => 'danger',
				'text' => 'Data Berhasil Dihapus!'
			];
		}
		return $msg;
	}

	public static function load_pekerjaan()
	{
		return DB::table('procurement_user_pekerjaan')->where('id_user', session('auth')->id_user)->first();
	}

	public static function find_pid_byUp($id)
	{
		return DB::table('procurement_req_pid As prp')
		->leftjoin('procurement_pid As pp', 'prp.id_pid_req', '=', 'pp.id')
		->select('pp.pid', DB::RAW("(SELECT CONCAT_WS(',',
		( CASE WHEN (SELECT SUM(pbd.material)
		FROM procurement_Boq_design pbd
		WHERE pbd.jenis_khs= 16) != 0 THEN '01-01-01' ELSE NULL END),
		( CASE WHEN (SELECT SUM(pbd.material)
		FROM procurement_Boq_design pbd
		WHERE (pbd.jenis_khs != 16 OR pbd.jenis_khs IS NULL) ) != 0 THEN '01-01-02' ELSE NULL END),
		( CASE WHEN (SELECT SUM(pbd.jasa)
		FROM procurement_Boq_design pbd ) != 0 THEN '01-02-01' ELSE NULL END) ) ) As idp"), 'prp.*')
		->where('id_upload', $id)->get();
	}

	public static function get_last_data($id, $sd)
	{
		return DB::table('procurement_boq_upload As pbu')
		->leftjoin('procurement_Boq_lokasi As pbl', 'pbu.id', '=', 'pbl.id_upload')
		->leftjoin('procurement_Boq_design As pbd', 'pbl.id', '=', 'pbd.id_boq_lokasi')
		->leftjoin('procurement_designator As pd', 'pd.id', '=', 'pbd.id_design')
		->select('pbd.*', DB::RAW("(CASE WHEN pbd.jenis_khs = 16 THEN 'Telkom Akses' ELSE 'Mitra' END) As namcomp"),
		'pd.material As material_pd', 'pd.jasa As jasa_pd', 'pd.jenis', 'pbl.sub_jenis_p', 'pbu.step_id', 'pbl.new_boq', 'pbl.lokasi', 'pbl.sto', 'pbd.material', 'pbd.jasa', 'pbd.jenis_khs As design_mitra_id', 'pd.uraian')
		->where([
			['pbu.id', $id],
			['pbd.status_design', $sd],
			['pd.active', 1]
		])
		->groupBy('pbd.id')
		->get();
	}

	public static function input_rfc_designator($req, $id)
	{
		$get_kerja = DB::table('procurement_boq_upload As pbu')
		->leftjoin('procurement_pekerjaan As pp', 'pbu.pekerjaan', '=', 'pp.pekerjaan')
		->select('pp.khs', 'pp.pekerjaan')
		->where('pbu.id', $id)
		->first();

		DB::table('procurement_design_rfc')->where([
			['design_rfc', $req->dessign_rfc],
			['pekerjaan', $get_kerja->pekerjaan],
			['witel', session('auth')->Witel_New],
		])->delete();

		foreach($req->material_rfc as $val)
		{
			DB::table('procurement_design_rfc')->insert([
				'id_design' 	 => $val,
				'design_rfc'   => $req->dessign_rfc,
				'pekerjaan'    => $get_kerja->pekerjaan,
				'witel' 			 => session('auth')->Witel_New,
				'qty_per_item' => $req->qty_per_item ?? 1,
				'created_by'   => session('auth')->id_user
			]);
		}
	}

	public static function update_wbs($req, $id)
	{
		return DB::transaction(function () use ($req, $id)
		{
			$total_mat = $total_jasa = 0;
			$material = json_decode($req->material_input);

			$material_jml = DB::SELECT("SELECT * FROM procurement_Boq_lokasi pbl
			LEFT JOIN procurement_Boq_design pbd ON pbd.id_boq_lokasi = pbl.id
			WHERE pbl.id_upload = $id AND pbd.status_design = 0");

			$material_jml = count($material_jml);

			$lokasi = json_decode($req->lokasi_input);

			foreach($lokasi as $v)
			{
				if(empty($v->pid) )
				{
					return back()->with('alerts', [
						['type' => 'warning', 'text' => 'Harap Masukkan PID!']
					])->withInput();
				}
			}

			if(empty(json_decode($req->material_input) ) )
			{
				return back()->with('alerts', [
					['type' => 'warning', 'text' => 'Harap Masukkan Material!']
				])->withInput();
			}

			$get_data_material = AdminModel::get_design();

			$find_material = json_decode(json_encode($get_data_material), TRUE);

			foreach($material as $k => $val)
			{
				$find_me = array_search($val->id, array_column($find_material, 'id') );

				$total_mat  += $val->material * $val->val;
				$total_jasa += $val->jasa * $val->val;

				$data_rekon[$k]['id']        = $val->id;
				$data_rekon[$k]['nama']      = $get_data_material[$find_me]->designator;
				$data_rekon[$k]['material']  = $val->material;
				$data_rekon[$k]['jasa']      = $val->jasa;
				$data_rekon[$k]['jenis_khs'] = $val->jenis_khs;
				$data_rekon[$k]['sp']        = $val->val ?? 0 ;
				$data_rekon[$k]['rekon']     = 0;
				$data_rekon[$k]['tambah']    = 0;
				$data_rekon[$k]['kurang']    = 0;
				$data_rekon[$k]['sto']       = 'LOK'.$val->urutan;

				if(empty($val->sto) )
				{
					return back()->with('alerts', [
						['type' => 'warning', 'text' => 'STO Tidak Boleh Kosong']
					])->withInput();
				}

				if(!trim($val->lokasi))
				{
					return back()->with('alerts', [
						['type' => 'warning', 'text' => 'Lokasi Tidak Boleh Kosong']
					])->withInput();
				}

				$data_rekon[$k]['lokasi']     = $val->lokasi;
				$data_rekon[$k]['sto']        = $val->sto;
				$data_rekon[$k]['urutan_sto'] = 'LOK'.$val->urutan;

				$sto_load[$val->sto]       = $val->sto;
				$lokasi_load[$val->lokasi] = $val->lokasi;

				$remove_lok[$val->id_lokasi] = $val->id_lokasi;

				$data_lokasi[$val->urutan]['id_lokasi']  = $val->id_lokasi;
				$data_lokasi[$val->urutan]['urutan_sto'] = 'LOK'.$val->urutan;
				$data_lokasi[$val->urutan]['sto']        = $val->sto;
				$data_lokasi[$val->urutan]['lokasi']     = $val->lokasi;
				$data_lokasi[$val->urutan]['sub_jenis']  = $val->sub_jenis;

				if(!isset($data_lokasi[$val->urutan]['jml_vol']) )
				{
					$data_lokasi[$val->urutan]['jml_vol'] = 0;
				}

				$data_lokasi[$val->urutan]['jml_vol']   += $val->val;
			}

			$all_lok = 	DB::SELECT("SELECT pbl.id
			FROM procurement_Boq_lokasi pbl
			LEFT JOIN procurement_Boq_design pbd ON pbd.id_boq_lokasi = pbl.id
			WHERE pbl.id_upload = $id AND pbd.status_design = 0");

			foreach($all_lok as $v)
			{
				$all_array_lok[$v->id] = $v->id;
			}

			$different_lok = array_diff($all_array_lok, $remove_lok);

			if($different_lok)
			{
				$different_lok = implode(', ', $different_lok);

				DB::SELECT("DELETE pbd, pbl
				FROM procurement_Boq_lokasi pbl
				LEFT JOIN procurement_Boq_design pbd ON pbd.id_boq_lokasi = pbl.id
				WHERE pbl.id IN($different_lok)");
			}

			DB::SELECT("DELETE pbd, pbl
			FROM procurement_Boq_lokasi pbl
			LEFT JOIN procurement_Boq_design pbd ON pbd.id_boq_lokasi = pbl.id
			WHERE pbl.id_upload = $id AND pbd.status_design = 0");

			$collect_id_lok = [];
			// dd($data_lokasi);
			foreach($data_lokasi as $k_c1 => $val_cl)
			{
				$id_lok = DB::table('procurement_Boq_lokasi')->insertGetId([
					'id_upload'   => $id,
					'sto'         => $val_cl['sto'],
					'lokasi'      => $val_cl['lokasi'],
					'sub_jenis_p' => $val_cl['sub_jenis'],
				]);

				DB::table('procurement_Boq_design')->where('id_boq_lokasi', $val_cl['id_lokasi'])->update([
					'id_boq_lokasi' => $id_lok
				]);

				DB::table('procurement_req_pid')->where('id_lokasi', $val_cl['id_lokasi'])->update([
					'id_lokasi' => $id_lok,
					'modified_by' => session('auth')->id_user,
				]);

				$collect_id_lok[$k_c1] = $id_lok;

				if($val_cl['jml_vol'] == 0)
				{
					return back()->with('alerts', [
						['type' => 'warning', 'text' => 'Jumlah Designator Tidak Boleh Kosong Semuanya!']
					]);
				}
				else
				{
					foreach($data_rekon as $val_rek)
					{
						if ($val_rek['urutan_sto'] == $val_cl['urutan_sto'])
						{
							if($val_rek['sp'] != 0 || $val_rek['rekon'] != 0 )
							{
								DB::table('procurement_Boq_design')->insert([
									'id_boq_lokasi' => $id_lok,
									'id_design'     => $val_rek['id'],
									'designator'    => $val_rek['nama'],
									'material'      => $val_rek['material'],
									'jasa'          => $val_rek['jasa'],
									'jenis_khs'     => $val_rek['jenis_khs'],
									'sp'            => $val_rek['sp'],
									'rekon'         => $val_rek['rekon'],
									'tambah'        => $val_rek['tambah'],
									'kurang'        => $val_rek['kurang'],
									'status_design' => 0
								]);
							}
						}
					}
				}
			}

			DB::SELECT(
				"DELETE pbl
				FROM procurement_Boq_lokasi pbl
				LEFT JOIN procurement_Boq_design pbd ON pbd.id_boq_lokasi = pbl.id
				WHERE pbl.id_upload = $id AND pbd.id IS NULL "
			);

			$total_data = ['total_sp_material' => $total_mat, 'total_sp_jasa' => $total_jasa];

			foreach($lokasi as $v)
			{
				$get_pid = implode(', ', $v->pid);

				DB::table('procurement_Boq_lokasi')->where('id', $collect_id_lok[$v->no_lok])->update([
					'pid_sto' => $get_pid
				]);

				foreach($v->pid as $vv)
				{
					$grab_pid[$vv] = $vv;
				}
			}

			$create_new_pid = implode(', ', $grab_pid);

			DB::table('procurement_boq_upload')->where('id', $id)->update([
				'total_material_sp' => $total_data['total_sp_material'],
				'total_jasa_sp'     => $total_data['total_sp_jasa'],
				'total_sp'					=> $total_data['total_sp_material'] + $total_data['total_sp_jasa'],
				'sto_pekerjaan'     => implode(', ', $sto_load),
				'lokasi_pekerjaan'  => implode(', ', $lokasi_load),
				'id_project'        => $create_new_pid,
				'modified_by'       => session('auth')->id_user
			]);

			return redirect('/progress/'.$id)->with('alerts', [
				['type' => 'info', 'text' => 'Perubahan WBS PID Sudah Berhasil!']
			])->withInput();
		});
	}

	public static function update_po($req, $id)
	{
		return DB::transaction(function () use ($req, $id)
		{
			$total_mat_sp = $total_jasa_sp = $total_mat_rekon = $total_jasa_rekon = $total_mat_rekon_sp = $total_jasa_rekon_sp =  $total_mat_tmb = $total_mat_krg = $total_jasa_tmb = $total_jasa_krg = 0;

			$material  = json_decode($req->material_input);
			$data_pbu  = AdminModel::get_data_final($id);

			$step = $data_pbu->step_id;

			if($data_pbu->action_ps != 'forward')
			{
				$step = $data_pbu->step_after;
			}

			$lokasi = json_decode($req->lokasi_input);

			foreach($material as $v)
			{
				$get_all_id[$v->id_design] = $v->id_design;
			}

			$get_all_id = "'". implode("', '", $get_all_id) . "'";

			$data_boq_plan = DB::SELECT("SELECT pbd.*, pd.uraian, pd.satuan, pbl.sub_jenis_p, pbd.material as material_default, pbd.jasa as jasa_default, pbl.lokasi, pbl.sto, pbl.pid_sto
			FROM procurement_boq_upload pbu
			LEFT JOIN procurement_Boq_lokasi pbl ON pbu.id = pbl.id_upload
			LEFT JOIN procurement_Boq_design pbd ON pbl.id = pbd.id_boq_lokasi
			LEFT JOIN procurement_designator pd ON pd.id = pbd.id_design
			WHERE pbu.id = ".$id." AND pbd.status_design = 0 AND pd.active = 1 AND pbd.id_design NOT IN ($get_all_id) GROUP BY pbd.id");

			$data_boq_plan = json_decode(json_encode($data_boq_plan), TRUE);

			foreach($data_boq_plan as $v)
			{
				$dbp_mat[$v['id_design'] ] = $v;
			}

			$get_data_material = AdminModel::get_design();
			$find_material     = json_decode(json_encode($get_data_material), TRUE);

			foreach($material as $k => $val)
			{
				$tambah = ($val->rekon > $val->sp ? $val->rekon - $val->sp : 0);
				$kurang = ($val->sp > $val->rekon ? $val->sp - $val->rekon : 0);

				if($step < 7)
				{
					$total_mat_sp  += $val->material * $val->sp;
					$total_jasa_sp += $val->jasa * $val->sp;
				}
				elseif($step >= 7)
				{
					$total_mat_rekon     += $val->material * $val->rekon;
					$total_jasa_rekon    += $val->jasa * $val->rekon;
					$total_mat_rekon_sp  += $val->material * $val->sp;
					$total_jasa_rekon_sp += $val->jasa * $val->sp;

					$total_mat_tmb  += $val->material * $tambah;
					$total_jasa_tmb += $val->jasa * $tambah;
					$total_mat_krg  += $val->material * $kurang;
					$total_jasa_krg += $val->jasa * $kurang;
				}

				$find_me = array_search($val->id, array_column($find_material, 'id') );

				$data_rekon[$k]['id']        = $val->id;
				$data_rekon[$k]['nama']      = $get_data_material[$find_me]->designator;
				$data_rekon[$k]['material']  = $val->material;
				$data_rekon[$k]['jasa']      = $val->jasa;
				$data_rekon[$k]['jenis_khs'] = $val->jenis_khs;
				$data_rekon[$k]['sp']        = $val->sp ?? 0 ;
				$data_rekon[$k]['rekon']     = $val->rekon ?? 0 ;
				$data_rekon[$k]['tambah']    = $tambah;
				$data_rekon[$k]['kurang']    = $kurang;
				$data_rekon[$k]['sto']       = 'LOK'.$val->urutan;

				if(empty($val->sto) )
				{
					return back()->with('alerts', [
						['type' => 'warning', 'text' => 'STO Tidak Boleh Kosong']
					])->withInput();
				}

				if(!trim($val->lokasi))
				{
					return back()->with('alerts', [
						['type' => 'warning', 'text' => 'Lokasi Tidak Boleh Kosong']
					])->withInput();
				}

				$data_rekon[$k]['lokasi']     = $val->lokasi;
				$data_rekon[$k]['sto']        = $val->sto;
				$data_rekon[$k]['urutan_sto'] = 'LOK'.$val->urutan;

				$sto_load[$val->sto]       = $val->sto;
				$lokasi_load[$val->lokasi] = $val->lokasi;

				$remove_lok[$val->id_lokasi] = $val->id_lokasi;

				$data_lokasi[$val->urutan]['id_lokasi']  = $val->id_lokasi;
				$data_lokasi[$val->urutan]['urutan_sto'] = 'LOK'.$val->urutan;
				$data_lokasi[$val->urutan]['sto']        = $val->sto;
				$data_lokasi[$val->urutan]['lokasi']     = $val->lokasi;
				$data_lokasi[$val->urutan]['sub_jenis']  = $val->sub_jenis;

				if(!isset($data_lokasi[$val->urutan]['ttl_material_sp_rekon']) )
				{
					$data_lokasi[$val->urutan]['ttl_material_sp_rekon'] = 0;
					$data_lokasi[$val->urutan]['ttl_jasa_sp_rekon']     = 0;
					$data_lokasi[$val->urutan]['ttl_material_rekon']    = 0;
					$data_lokasi[$val->urutan]['ttl_jasa_rekon']        = 0;

					$data_lokasi[$val->urutan]['ttl_materialT_rekon'] = 0;
					$data_lokasi[$val->urutan]['ttl_materialK_rekon'] = 0;
					$data_lokasi[$val->urutan]['ttl_jasaT_rekon']     = 0;
					$data_lokasi[$val->urutan]['ttl_jasaK_rekon']     = 0;

					$data_lokasi[$val->urutan]['ttl_material_sp'] = 0;
					$data_lokasi[$val->urutan]['ttl_jasa_sp']     = 0;
				}

				$data_lokasi[$val->urutan]['ttl_material_sp_rekon'] += $val->material * $val->sp;
				$data_lokasi[$val->urutan]['ttl_jasa_sp_rekon']     += $val->jasa * $val->sp;
				$data_lokasi[$val->urutan]['ttl_material_rekon']    += $val->material * $val->rekon;
				$data_lokasi[$val->urutan]['ttl_jasa_rekon']        += $val->jasa * $val->rekon;

				$data_lokasi[$val->urutan]['ttl_materialT_rekon'] += $val->material * $tambah;
				$data_lokasi[$val->urutan]['ttl_materialK_rekon'] += $val->material * $kurang;
				$data_lokasi[$val->urutan]['ttl_jasaT_rekon']     += $val->jasa * $tambah;
				$data_lokasi[$val->urutan]['ttl_jasaK_rekon']     += $val->jasa * $kurang;

				$data_lokasi[$val->urutan]['ttl_material_sp'] += $val->material * $val->sp;
				$data_lokasi[$val->urutan]['ttl_jasa_sp']     += $val->jasa * $val->sp;
				// dd($val);
				if($step >= 7)
				{
					$find_unused = array_keys(array_column($data_boq_plan, 'id_boq_lokasi'), $val->id_lokasi);
					foreach($find_unused as $v)
					{
						$data_dbp = $data_boq_plan[$v];

						if($data_dbp['id_boq_lokasi'] == $val->id_lokasi)
						{
							$data_rekon[$data_dbp['id_design'] . '_'. $val->urutan]['id']         = $data_dbp['id_design'];
							$data_rekon[$data_dbp['id_design'] . '_'. $val->urutan]['nama']       = $data_dbp['designator'];
							$data_rekon[$data_dbp['id_design'] . '_'. $val->urutan]['material']   = $data_dbp['material'];
							$data_rekon[$data_dbp['id_design'] . '_'. $val->urutan]['jasa']       = $data_dbp['jasa'];
							$data_rekon[$data_dbp['id_design'] . '_'. $val->urutan]['jenis_khs']  = $data_dbp['jenis_khs'];
							$data_rekon[$data_dbp['id_design'] . '_'. $val->urutan]['sp']         = $data_dbp['sp'];
							$data_rekon[$data_dbp['id_design'] . '_'. $val->urutan]['rekon']      = $data_dbp['rekon'];
							$data_rekon[$data_dbp['id_design'] . '_'. $val->urutan]['tambah']     = 0;
							$data_rekon[$data_dbp['id_design'] . '_'. $val->urutan]['kurang']     = $data_dbp['sp'];
							$data_rekon[$data_dbp['id_design'] . '_'. $val->urutan]['lokasi']     = $data_dbp['lokasi'];
							$data_rekon[$data_dbp['id_design'] . '_'. $val->urutan]['lokasi_ada'] = 'ada';
							$data_rekon[$data_dbp['id_design'] . '_'. $val->urutan]['sto']        = $data_dbp['sto'];
							$data_rekon[$data_dbp['id_design'] . '_'. $val->urutan]['urutan_sto'] = 'LOK'.$val->urutan;

							$data_lokasi[$val->urutan]['ttl_materialK_rekon'] += $data_dbp['material'] * $data_dbp['sp'];
							$data_lokasi[$val->urutan]['ttl_jasaK_rekon']     += $data_dbp['jasa'] * $data_dbp['sp'];
						}
					}
				}

				// if($val->id_lokasi == 'NEW')
				// {
				// 	foreach($dbp_mat as $v)
				// 	{
				// 		$data_rekon[$v['id_design'] . '_'. $val->urutan]['id']         = $v['id_design'];
				// 		$data_rekon[$v['id_design'] . '_'. $val->urutan]['nama']       = $v['designator'];
				// 		$data_rekon[$v['id_design'] . '_'. $val->urutan]['material']   = $v['material'];
				// 		$data_rekon[$v['id_design'] . '_'. $val->urutan]['jasa']       = $v['jasa'];
				// 		$data_rekon[$v['id_design'] . '_'. $val->urutan]['jenis_khs']  = $v['jenis_khs'];
				// 		$data_rekon[$v['id_design'] . '_'. $val->urutan]['sp']         = 0;
				// 		$data_rekon[$v['id_design'] . '_'. $val->urutan]['rekon']      = 0;
				// 		$data_rekon[$v['id_design'] . '_'. $val->urutan]['tambah']     = 0;
				// 		$data_rekon[$v['id_design'] . '_'. $val->urutan]['kurang']     = 0;
				// 		$data_rekon[$v['id_design'] . '_'. $val->urutan]['lokasi']     = $v['lokasi'];
				// 		$data_rekon[$v['id_design'] . '_'. $val->urutan]['lokasi_ada'] = NULL;
				// 		$data_rekon[$v['id_design'] . '_'. $val->urutan]['sto']        = $v['sto'];
				// 		$data_rekon[$v['id_design'] . '_'. $val->urutan]['urutan_sto'] = 'LOK'.$val->urutan;
				// 	}
				// }

				if(!isset($data_lokasi[$val->urutan]['jml_vol']) )
				{
					$data_lokasi[$val->urutan]['jml_vol'] = 0;
				}

				if($step < 7)
				{
					$data_lokasi[$val->urutan]['jml_vol'] += $val->sp;
				}
				elseif($step >= 7)
				{
					$data_lokasi[$val->urutan]['jml_vol'] += $val->rekon;
				}

				$data_lokasi[$val->urutan]['new_boq']  = $val->new_boq;
			}

			$data_rekon = array_values($data_rekon);

			$check_pid = DB::Table('procurement_req_pid')->where('id_upload', $id)->first();

			if($check_pid)
			{
				foreach($lokasi as $v)
				{
					if(empty($v->pid) )
					{
						return back()->with('alerts', [
							['type' => 'warning', 'text' => 'Harap Masukkan PID!']
						])->withInput();
					}
				}
			}

			$status_d = 0;

			if($step == 7)
			{
				$status_d = 2;
			}
			elseif($step > 7)
			{
				$status_d = 1;
			}

			if(empty(json_decode($req->material_input) ) )
			{
				return back()->with('alerts', [
					['type' => 'warning', 'text' => 'Harap Masukkan Material!']
				])->withInput();
			}

			$all_lok = 	DB::SELECT("SELECT pbl.id
			FROM procurement_Boq_lokasi pbl
			LEFT JOIN procurement_Boq_design pbd ON pbd.id_boq_lokasi = pbl.id
			WHERE pbl.id_upload = $id AND pbd.status_design = $status_d");

			foreach($all_lok as $v)
			{
				$all_array_lok[$v->id] = $v->id;
			}

			$different_lok = array_diff($all_array_lok, $remove_lok);

			if($different_lok)
			{
				$different_lok = implode(', ', $different_lok);

				DB::SELECT("DELETE pbd, pbl
				FROM procurement_Boq_lokasi pbl
				LEFT JOIN procurement_Boq_design pbd ON pbd.id_boq_lokasi = pbl.id
				WHERE pbl.id IN($different_lok)");
			}

			DB::SELECT("DELETE pbd, pbl
			FROM procurement_Boq_lokasi pbl
			LEFT JOIN procurement_Boq_design pbd ON pbd.id_boq_lokasi = pbl.id
			WHERE pbl.id_upload = $id AND pbd.status_design = $status_d");

			$collect_id_lok = [];

			foreach($data_lokasi as $k_c1 => $val_cl)
			{
				$data_pbl = [
					'id_upload'   => $id,
					'sto'         => $val_cl['sto'],
					'lokasi'      => $val_cl['lokasi'],
					'sub_jenis_p' => $val_cl['sub_jenis'],
					'new_boq'     => $val_cl['new_boq'],
				];

				if($step == 7)
				{
					$data_pbl['material_sp_rek_plan'] = $val_cl['ttl_material_sp_rekon'];
					$data_pbl['jasa_sp_rek_plan']     = $val_cl['ttl_jasa_sp_rekon'];
					$data_pbl['material_rek_plan']    = $val_cl['ttl_material_rekon'];
					$data_pbl['jasa_rek_plan']        = $val_cl['ttl_jasa_rekon'];

					$data_pbl['materialT_rek_plan']   = $val_cl['ttl_materialT_rekon'];
					$data_pbl['materialK_rek_plan']   = $val_cl['ttl_materialK_rekon'];
					$data_pbl['jasaT_rek_plan']       = $val_cl['ttl_jasaT_rekon'];
					$data_pbl['jasaK_rek_plan']       = $val_cl['ttl_jasaK_rekon'];
				}
				elseif($step > 7)
				{
					$data_pbl['material_sp_rek'] = $val_cl['ttl_material_sp_rekon'];
					$data_pbl['jasa_sp_rek']     = $val_cl['ttl_jasa_sp_rekon'];
					$data_pbl['material_rek']    = $val_cl['ttl_material_rekon'];
					$data_pbl['jasa_rek']        = $val_cl['ttl_jasa_rekon'];

					$data_pbl['materialT_rek']   = $val_cl['ttl_materialT_rekon'];
					$data_pbl['materialK_rek']   = $val_cl['ttl_materialK_rekon'];
					$data_pbl['jasaT_rek']       = $val_cl['ttl_jasaT_rekon'];
					$data_pbl['jasaK_rek']       = $val_cl['ttl_jasaK_rekon'];
				}
				else
				{
					$data_pbl['material_sp'] = $val->material * $val->sp;
					$data_pbl['jasa_sp']     = $val->jasa * $val->sp;
				}

				$id_lok = DB::table('procurement_Boq_lokasi')->insertGetId($data_pbl);

				DB::table('procurement_Boq_design')->where('id_boq_lokasi', $val_cl['id_lokasi'])->update([
					'id_boq_lokasi' => $id_lok
				]);

				$check_prp = DB::table('procurement_req_pid')->where('id_lokasi', $val_cl['id_lokasi'])->first();

				if($check_prp)
				{
					DB::table('procurement_req_pid')->where('id_lokasi', $val_cl['id_lokasi'])->update([
						'id_lokasi' => $id_lok,
						'modified_by' => session('auth')->id_user,
					]);
				}

				$collect_id_lok[$k_c1] = $id_lok;

				if($val_cl['jml_vol'] == 0)
				{
					// return back()->with('alerts', [
					// 	['type' => 'warning', 'text' => 'Jumlah Designator Tidak Boleh Kosong Semuanya!']
					// ])->withInput();
				}
				else
				{
					foreach($data_rekon as $val_rek)
					{
						if ($val_rek['urutan_sto'] == $val_cl['urutan_sto'])
						{
							if($val_rek['sp'] != 0 || $val_rek['rekon'] != 0 )
							{
								DB::table('procurement_Boq_design')->insert([
									'id_boq_lokasi' => $id_lok,
									'id_design'     => $val_rek['id'],
									'designator'    => $val_rek['nama'],
									'material'      => $val_rek['material'],
									'jasa'          => $val_rek['jasa'],
									'jenis_khs'     => $val_rek['jenis_khs'],
									'sp'            => $val_rek['sp'],
									'rekon'         => $val_rek['rekon'],
									'tambah'        => ($status_d != 0 ? $val_rek['tambah'] : 0),
									'kurang'        => ($status_d != 0 ? $val_rek['kurang'] : 0),
									'status_design' => $status_d
								]);

								if($req->sync_pasca)
								{
									$get_rekon = DB::table('procurement_Boq_design')->where([
										['id_boq_lokasi', $id_lok],
										['id_design', $val_rek['id'] ],
										['status_design', $status_d]
									])->first();

									if($get_rekon)
									{
										DB::table('procurement_Boq_design')->where([
											['id_boq_lokasi', $id_lok],
											['id_design', $val_rek['id'] ],
											['status_design', $status_d]
										])
										->update([
											'sp' => $val_rek['sp'],
											'tambah' => ($get_rekon->rekon > $val_rek['sp'] ? $get_rekon->rekon - $val_rek['sp'] : 0),
											'kurang' => ($val_rek['sp'] > $get_rekon->rekon ? $val_rek['sp'] - $get_rekon->rekon : 0)
										]);
									}
									else
									{
										DB::table('procurement_Boq_design')->insert([
											'id_boq_lokasi' => $id_lok,
											'id_design'     => $val_rek['id'],
											'designator'    => $val_rek['nama'],
											'material'      => $val_rek['material'],
											'jasa'          => $val_rek['jasa'],
											'jenis_khs'     => $val_rek['jenis_khs'],
											'sp'            => $val_rek['sp'],
											'rekon'         => $val_rek['sp'],
											'tambah'        => ($get_rekon->rekon > $val_rek['sp'] ? $get_rekon->rekon - $val_rek['sp'] : 0),
											'kurang'        => ($val_rek['sp'] > $get_rekon->rekon ? $val_rek['sp'] - $get_rekon->rekon : 0),
											'status_design' => $status_d
										]);
									}
								}
							}
						}
					}
				}
			}

			if($req->sync_pasca)
			{
				$data = DB::Table('procurement_boq_upload As pbu')
				->leftjoin('procurement_Boq_lokasi As pbl', 'pbu.id', '=', 'pbl.id_upload')
				->leftjoin('procurement_Boq_design As pbd', 'pbd.id_boq_lokasi', '=', 'pbl.id')
				->select('pbu.id As id_pbu', 'pbd.*')
				->where([
					['pbu.active', 1],
					['pbu.id', $id],
					['pbu.pekerjaan', '!=', 'PSB']
				])
				->get();

				foreach($data as $k => $d)
				{
					$final_data_sync[$d->id_pbu]['id_pbu'] = $d->id_pbu;

					$data_pbl_sync[$d->id_boq_lokasi]['id'] = $d->id_boq_lokasi;

					if(!isset($final_data_sync[$d->id_pbu]['total_material_sp']) )
					{
						$final_data_sync[$d->id_pbu]['total_material_sp'] = 0;
						$final_data_sync[$d->id_pbu]['total_jasa_sp']     = 0;
						$final_data_sync[$d->id_pbu]['total_sp']     		  = 0;

						$final_data_sync[$d->id_pbu]['total_material_rek_sp_plan'] = 0;
						$final_data_sync[$d->id_pbu]['total_jasa_rek_sp_plan']     = 0;

						$final_data_sync[$d->id_pbu]['total_material_rekon_plan'] = 0;
						$final_data_sync[$d->id_pbu]['total_jasa_rekon_plan']     = 0;
						$final_data_sync[$d->id_pbu]['total_materialT_rek_plan']  = 0;
						$final_data_sync[$d->id_pbu]['total_materialK_rek_plan']  = 0;
						$final_data_sync[$d->id_pbu]['total_jasaT_rek_plan']      = 0;
						$final_data_sync[$d->id_pbu]['total_jasaK_rek_plan']      = 0;

						$final_data_sync[$d->id_pbu]['total_material_rek_sp'] = 0;
						$final_data_sync[$d->id_pbu]['total_jasa_rek_sp']     = 0;

						$final_data_sync[$d->id_pbu]['total_material_rekon'] = 0;
						$final_data_sync[$d->id_pbu]['total_jasa_rekon']     = 0;
						$final_data_sync[$d->id_pbu]['total_materialT_rek']  = 0;
						$final_data_sync[$d->id_pbu]['total_materialK_rek']  = 0;
						$final_data_sync[$d->id_pbu]['total_jasaT_rek']      = 0;
						$final_data_sync[$d->id_pbu]['total_jasaK_rek']      = 0;
					}

					if(!isset($data_pbl_sync[$d->id_boq_lokasi]['material_sp']) )
					{
						$data_pbl_sync[$d->id_boq_lokasi]['material_sp'] = 0;
						$data_pbl_sync[$d->id_boq_lokasi]['jasa_sp']     = 0;

						$data_pbl_sync[$d->id_boq_lokasi]['material_rek_sp_plan'] = 0;
						$data_pbl_sync[$d->id_boq_lokasi]['jasa_rek_sp_plan']     = 0;

						$data_pbl_sync[$d->id_boq_lokasi]['material_rek_plan']  = 0;
						$data_pbl_sync[$d->id_boq_lokasi]['jasa_rek_plan']      = 0;
						$data_pbl_sync[$d->id_boq_lokasi]['materialT_rek_plan'] = 0;
						$data_pbl_sync[$d->id_boq_lokasi]['materialK_rek_plan'] = 0;
						$data_pbl_sync[$d->id_boq_lokasi]['jasaT_rek_plan']     = 0;
						$data_pbl_sync[$d->id_boq_lokasi]['jasaK_rek_plan']     = 0;

						$data_pbl_sync[$d->id_boq_lokasi]['material_sp_rek'] = 0;
						$data_pbl_sync[$d->id_boq_lokasi]['jasa_sp_rek']     = 0;

						$data_pbl_sync[$d->id_boq_lokasi]['material_rek']  = 0;
						$data_pbl_sync[$d->id_boq_lokasi]['jasa_rek']      = 0;
						$data_pbl_sync[$d->id_boq_lokasi]['materialT_rek'] = 0;
						$data_pbl_sync[$d->id_boq_lokasi]['materialK_rek'] = 0;
						$data_pbl_sync[$d->id_boq_lokasi]['jasaT_rek']     = 0;
						$data_pbl_sync[$d->id_boq_lokasi]['jasaK_rek']     = 0;
					}

					if($d->status_design == 0)
					{
						$final_data_sync[$d->id_pbu]['total_material_sp'] += $d->material * $d->sp;
						$final_data_sync[$d->id_pbu]['total_jasa_sp']     += $d->jasa * $d->sp;
						$final_data_sync[$d->id_pbu]['total_sp']     		 += ($d->material * $d->sp) + ($d->jasa * $d->sp);

						$data_pbl_sync[$d->id_boq_lokasi]['material_sp'] += $d->material * $d->sp;
						$data_pbl_sync[$d->id_boq_lokasi]['jasa_sp']     += $d->jasa * $d->sp;
					}

					if($d->status_design == 2)
					{
						$final_data_sync[$d->id_pbu]['total_material_rek_sp_plan'] += $d->material * $d->sp;
						$final_data_sync[$d->id_pbu]['total_jasa_rek_sp_plan']     += $d->jasa * $d->sp;

						$final_data_sync[$d->id_pbu]['total_material_rekon_plan'] += $d->material * $d->rekon;
						$final_data_sync[$d->id_pbu]['total_jasa_rekon_plan']     += $d->jasa * $d->rekon;
						$final_data_sync[$d->id_pbu]['total_materialT_rek_plan']  += $d->tambah * $d->material;
						$final_data_sync[$d->id_pbu]['total_materialK_rek_plan']  += $d->kurang * $d->material;
						$final_data_sync[$d->id_pbu]['total_jasaT_rek_plan']      += $d->tambah * $d->jasa;
						$final_data_sync[$d->id_pbu]['total_jasaK_rek_plan']      += $d->kurang * $d->jasa;

						$data_pbl_sync[$d->id_boq_lokasi]['material_rek_sp_plan'] += $d->material * $d->sp;
						$data_pbl_sync[$d->id_boq_lokasi]['jasa_rek_sp_plan']     += $d->jasa * $d->sp;

						$data_pbl_sync[$d->id_boq_lokasi]['material_rek_plan'] += $d->material * $d->rekon;
						$data_pbl_sync[$d->id_boq_lokasi]['jasa_rek_plan']     += $d->jasa * $d->rekon;
						$data_pbl_sync[$d->id_boq_lokasi]['materialT_rek_plan']  += $d->tambah * $d->material;
						$data_pbl_sync[$d->id_boq_lokasi]['materialK_rek_plan']  += $d->kurang * $d->material;
						$data_pbl_sync[$d->id_boq_lokasi]['jasaT_rek_plan']      += $d->tambah * $d->jasa;
						$data_pbl_sync[$d->id_boq_lokasi]['jasaK_rek_plan']      += $d->kurang * $d->jasa;
					}

					if($d->status_design == 1)
					{
						$final_data_sync[$d->id_pbu]['total_material_rek_sp'] += $d->material * $d->sp;
						$final_data_sync[$d->id_pbu]['total_jasa_rek_sp']     += $d->jasa * $d->sp;

						$final_data_sync[$d->id_pbu]['total_material_rekon'] += $d->material * $d->rekon;
						$final_data_sync[$d->id_pbu]['total_jasa_rekon']     += $d->jasa * $d->rekon;
						$final_data_sync[$d->id_pbu]['total_materialT_rek']  += $d->tambah * $d->material;
						$final_data_sync[$d->id_pbu]['total_materialK_rek']  += $d->kurang * $d->material;
						$final_data_sync[$d->id_pbu]['total_jasaT_rek']      += $d->tambah * $d->jasa;
						$final_data_sync[$d->id_pbu]['total_jasaK_rek']      += $d->kurang * $d->jasa;


						$data_pbl_sync[$d->id_boq_lokasi]['material_sp_rek'] += $d->material * $d->sp;
						$data_pbl_sync[$d->id_boq_lokasi]['jasa_sp_rek']     += $d->jasa * $d->sp;

						$data_pbl_sync[$d->id_boq_lokasi]['material_rek']  += $d->material * $d->rekon;
						$data_pbl_sync[$d->id_boq_lokasi]['jasa_rek']      += $d->jasa * $d->rekon;
						$data_pbl_sync[$d->id_boq_lokasi]['materialT_rek'] += $d->tambah * $d->material;
						$data_pbl_sync[$d->id_boq_lokasi]['materialK_rek'] += $d->kurang * $d->material;
						$data_pbl_sync[$d->id_boq_lokasi]['jasaT_rek']     += $d->tambah * $d->jasa;
						$data_pbl_sync[$d->id_boq_lokasi]['jasaK_rek']     += $d->kurang * $d->jasa;
					}
				}

				DB::transaction(function () use ($final_data_sync, $data_pbl_sync){
					foreach ($final_data_sync as $v) {
						DB::Table('procurement_boq_upload')->where('id', $v['id_pbu'])->update([
							"total_material_sp"          => $v['total_material_sp'],
							"total_jasa_sp"              => $v['total_jasa_sp'],
							"total_sp"		               => $v['total_sp'],
							"total_material_rek_sp_plan" => $v['total_material_rek_sp_plan'],
							"total_jasa_rek_sp_plan"     => $v['total_jasa_rek_sp_plan'],
							"total_material_rekon_plan"  => $v['total_material_rekon_plan'],
							"total_jasa_rekon_plan"      => $v['total_jasa_rekon_plan'],
							"total_materialT_rek_plan"   => $v['total_materialT_rek_plan'],
							"total_materialK_rek_plan"   => $v['total_materialK_rek_plan'],
							"total_jasaT_rek_plan"       => $v['total_jasaT_rek_plan'],
							"total_jasaK_rek_plan"       => $v['total_jasaK_rek_plan'],
							"total_material_rek_sp"      => $v['total_material_rek_sp'],
							"total_jasa_rek_sp"          => $v['total_jasa_rek_sp'],
							"total_material_rekon"       => $v['total_material_rekon'],
							"total_jasa_rekon"           => $v['total_jasa_rekon'],
							"total_materialT_rek"        => $v['total_materialT_rek'],
							"total_materialK_rek"        => $v['total_materialK_rek'],
							"total_jasaT_rek"            => $v['total_jasaT_rek'],
							"total_jasaK_rek"            => $v['total_jasaK_rek'],
						]);
					}

					foreach ($data_pbl_sync as $vv) {
						DB::Table('procurement_Boq_lokasi')->where('id', $vv['id'])->update([
							"material_sp"          => $vv['material_sp'],
							"jasa_sp"              => $vv['jasa_sp'],
							"material_sp_rek_plan" => $vv['material_rek_sp_plan'],
							"jasa_sp_rek_plan"     => $vv['jasa_rek_sp_plan'],
							"material_rek_plan"    => $vv['material_rek_plan'],
							"jasa_rek_plan"        => $vv['jasa_rek_plan'],
							"materialT_rek_plan"   => $vv['materialT_rek_plan'],
							"materialK_rek_plan"   => $vv['materialK_rek_plan'],
							"jasaT_rek_plan"       => $vv['jasaT_rek_plan'],
							"jasaK_rek_plan"       => $vv['jasaK_rek_plan'],
							"material_sp_rek"      => $vv['material_sp_rek'],
							"jasa_sp_rek"          => $vv['jasa_sp_rek'],
							"material_rek"         => $vv['material_rek'],
							"jasa_rek"             => $vv['jasa_rek'],
							"materialT_rek"        => $vv['materialT_rek'],
							"materialK_rek"        => $vv['materialK_rek'],
							"jasaT_rek"            => $vv['jasaT_rek'],
							"jasaK_rek"            => $vv['jasaK_rek'],
						]);
					}
				});
			}
			// dd($lokasi, $collect_id_lok);
			$grab_pid = [];

			foreach($lokasi as $v)
			{
				$get_pid = implode(', ', $v->pid);

				DB::table('procurement_Boq_lokasi')->where('id', $collect_id_lok[$v->no_lok])->update([
					'pid_sto' => $get_pid
				]);

				$collect_pid = [];

				foreach($v->pid as $vv)
				{
					$pid = substr($vv, 0, -9);
					$collect_pid[$pid] = $pid;
				}


				foreach($collect_pid as $vvv)
				{
					$get_pid_l = ReportModel::find_pid($vvv, 'nama');
					$chck_pid = DB::table('procurement_req_pid')
					->where([
						['id_lokasi', $collect_id_lok[$v->no_lok] ],
						['id_pid_req', $get_pid_l->id]
					])
					->first();

					if(!$chck_pid)
					{
						DB::Table('procurement_req_pid')->insert([
							'id_upload'  => $id,
							'id_lokasi'  => $collect_id_lok[$v->no_lok],
							'id_pid_req' => $get_pid_l->id,
							'created_by' => session('auth')->id_user
						]);
					}
				}
				// dd($collect_pid, $collect_id_lok, $v->no_lok);
				foreach($v->pid as $vv)
				{
					$grab_pid[$vv] = $vv;
				}
			}

			DB::SELECT(
				"DELETE pbl
				FROM procurement_Boq_lokasi pbl
				LEFT JOIN procurement_Boq_design pbd ON pbd.id_boq_lokasi = pbl.id
				WHERE pbl.id_upload = $id AND pbd.id IS NULL "
			);

			$create_new_pid = implode(', ', $grab_pid);

			$jenis_po = null;
			$get_mitra = AdminModel::get_mitra($req->mitra_id);
			// dd($total_data);
			if($req->pekerjaan == 'Construction')
			{
				$toc = $req->toc_edit;
				$jenis_po = $req->jenis_po;
			}
			else
			{
				$toc = date('t', strtotime($req->tahun.'-'. $req->bulan) );
			}

			// $get_rfc = DB::Table('procurement_rfc_osp As pro')
			// ->Leftjoin('inventory_material As im', 'pro.rfc', '=', 'im.no_rfc')
			// ->select('pro.*', 'im.wbs_element')
			// ->where('id_upload', $id)
			// ->get();

			// if($get_rfc)
			// {
			// 	foreach($get_rfc as $v)
			// 	{
			// 		$status_pro = 'LURUS';

			// 		if(!in_array($v->wbs_element, $collect_pid) )
			// 		{
			// 			$status_pro = 'TIDAK LURUS';
			// 		}

			// 		DB::Table('procurement_rfc_osp As pro')->where('rfc', $v->rfc)->update([
			// 			'status_rfc' => $status_pro,
			// 			'modified_by' => session('auth')->id_user
			// 		]);
			// 	}
			// }

			$update = [
				'judul'            => trim($req->judul),
				'witel'            => session('auth')->Witel_New,
				'jenis_kontrak'    => $req->jenis_kontrak,
				'jenis_po'         => $jenis_po,
				'pekerjaan'        => $req->pekerjaan,
				'bulan_pengerjaan' => $req->tahun.'-'. $req->bulan,
				'jenis_work'       => $req->jenis_work,
				'mitra_id'         => $req->mitra_id,
				'mitra_nm'         => $get_mitra->nama_company,
				'toc'              => $toc,
				'sto_pekerjaan'    => implode(', ', $sto_load),
				'lokasi_pekerjaan' => implode(', ', $lokasi_load),
				'id_project'       => $create_new_pid,
				'modified_by'      => session('auth')->id_user
			];

			if($step == 7)
			{
				$update['total_material_rek_sp_plan'] = $total_mat_rekon_sp;
				$update['total_jasa_rek_sp_plan']     = $total_jasa_rekon_sp;

				$update['total_material_rekon_plan'] = $total_mat_rekon;
				$update['total_jasa_rekon_plan']     = $total_jasa_rekon;
				$update['total_materialT_rek_plan']  = $total_mat_tmb;
				$update['total_materialK_rek_plan']  = $total_mat_krg;
				$update['total_jasaT_rek_plan']      = $total_jasa_tmb;
				$update['total_jasaK_rek_plan']      = $total_jasa_krg;
			}
			elseif($step > 7)
			{
				$update['total_material_rek_sp'] = $total_mat_rekon_sp;
				$update['total_jasa_rek_sp']     = $total_jasa_rekon_sp;

				$update['total_material_rekon'] = $total_mat_rekon;
				$update['total_jasa_rekon']     = $total_jasa_rekon;
				$update['total_materialT_rek']  = $total_mat_tmb;
				$update['total_materialK_rek']  = $total_mat_krg;
				$update['total_jasaT_rek']      = $total_jasa_tmb;
				$update['total_jasaK_rek']      = $total_jasa_krg;
			}
			else
			{
				$update['total_material_sp'] = $total_mat_sp;
				$update['total_jasa_sp']     = $total_jasa_sp;
				$update['total_sp']          = $total_mat_sp + $total_jasa_sp;
			}

			DB::table('procurement_boq_upload')->where('id', $id)->update($update);

			return redirect('/progressList/input')->with('alerts', [
				['type' => 'info', 'text' => 'Perubahan Data Sudah Berhasil!']
			]);
		});
	}

	public static function return_po($id)
	{
		DB::table('procurement_boq_upload')->where('id', $id)->update([
			'active' => 1
		]);

		$data = DB::table('procurement_boq_upload')->where('id', $id)->first();

		return $data;
	}

	public static function get_budget($id)
	{
		return DB::table('procurement_req_pid')->select(DB::RAW("SUM(budget) as budget_awal") )->where('id_upload', $id)->first();
	}

	public static function check_pid($data)
	{
		return DB::SELECT("SELECT (SELECT CONCAT_WS(',',
		( CASE WHEN (SELECT SUM(pbd.material)
		FROM procurement_Boq_design pbd
		WHERE pbd.id_boq_lokasi = ".$data->id_lokasi." AND pbd.jenis_khs= 16) != 0 THEN CONCAT_WS('-', ppc.pid, '01-01-01' ) ELSE NULL END),
		( CASE WHEN (SELECT SUM(pbd.material)
		FROM procurement_Boq_design pbd
		WHERE pbd.id_boq_lokasi = ".$data->id_lokasi." AND (pbd.jenis_khs != 16 OR pbd.jenis_khs IS NULL)) != 0 THEN CONCAT_WS('-', ppc.pid, '01-01-02' ) ELSE NULL END),
		( CASE WHEN (SELECT SUM(pbd.jasa)
		FROM procurement_Boq_design pbd
		WHERE pbd.id_boq_lokasi = ".$data->id_lokasi.") != 0 THEN CONCAT_WS('-', ppc.pid, '01-02-01' ) ELSE NULL END) ) ) As idp
		FROM procurement_pid ppc
		WHERE ppc.id = ".$data->id_pid_req)[0];
	}

	public static function get_lokasi($id)
	{
		return DB::table('procurement_Boq_lokasi')->where('id_upload', $id)->get();
	}

	public static function update_kolom($id, $req)
	{
		return DB::transaction(function () use ($req, $id)
		{
			$material = json_decode($req->material_input);

			foreach($material as $v)
			{
				$jenis_design_id = ($v->checkbox_isi == true ? 16 : 0);

				DB::SELECT("UPDATE procurement_boq_upload pbu
				LEFT JOIN procurement_Boq_lokasi pbl ON pbu.id = pbl.id_upload
				LEFT JOIN procurement_Boq_design pbd ON pbl.id = pbd.id_boq_lokasi
				SET pbd.jenis_khs = $jenis_design_id  WHERE pbu.id = $id AND pbd.id_design = $v->id");
			}

			$msg['direct'] = '/get_detail_laporan/'.$id;
			$msg['isi']['msg'] = [
				'type' => 'success',
				'text' => 'Perubahan Jenis Material Berhasil!'
			];

			return $msg;
		});
	}
}