<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

date_default_timezone_set("Asia/Makassar");

class ReportModel
{

	// public static function get_list_pks()
	// {
	// 	return DB::table('procurement_pks As pp')
	// 	->leftjoin('procurement_mitra As pm', 'pp.id_mitra', '=', 'pm.id')
	// 	->select('pp.*', DB::RAW("(CASE WHEN pp.jenis = 0 THEN 'Maintenance' ELSE 'PSB' END) as jenis"), 'pm.nama_company as nama_mitra')
	// 	->orderBy('tgl_last_update', 'DESC')
	// 	->get();
	// }

	// public static function update_pks($req, $id = NULL)
	// {
	// 	DB::transaction(function () use ($req, $id)
	// 	{
	// 		if($id)
	// 		{
	// 			$data = DB::table('procurement_pks')->where('id', $id)->first();

	// 			DB::table('procurement_pks')->where('id', $id)->update([
	// 				'id_mitra' => $req->mitra_select,
	// 				'pks' => $req->pks,
	// 				'jenis' => $req->jenis,
	// 				'tgl_start' => $req->tgl_start,
	// 				'tgl_end' => $req->tgl_end,
	// 				'amandemen' => $data->amandemen + 1,
	// 			]);
	// 		}
	// 		else
	// 		{
	// 			DB::table('procurement_pks')->insert([
	// 				'id_mitra' => $req->mitra_select,
	// 				'pks' => $req->pks,
	// 				'jenis' => $req->jenis,
	// 				'tgl_start' => $req->tgl_start,
	// 				'tgl_end' => $req->tgl_end,
	// 				'amandemen' => 0,
	// 			]);
	// 		}
	// 	});
	// }

	// public static function get_pkd($id)
	// {
	// 	DB::transaction(function () use ($id)
	// 	{
	// 		return DB::table('procurement_pks As pp')
	// 		->leftjoin('procurement_mitra As pm', 'pp.id_mitra', '=', 'pm.id')
	// 		->select('pp.*', DB::RAW("(CASE WHEN pp.jenis = 0 THEN 'Maintenance' ELSE 'PSB' END) as jenis"), 'pm.nama_company as nama_mitra')
	// 		->first();
	// 	});
	// }

	public static function rfc_rekap($m1, $m2) {
		return DB::SELECT("SELECT msr.rfc AS rfc_n, regu_name, nik1, nik2,
			SUM( CASE WHEN action = 1 THEN 1 ELSE 0 END) AS keluar_gudang,
			SUM( CASE WHEN action = 2 THEN 1 ELSE 0 END) AS terpakai,
			SUM( CASE WHEN action = 3 THEN 1 ELSE 0 END) AS kembali
			FROM maintenance_saldo_rfc msr
			WHERE DATE_FORMAT(msr.created_at, '%Y-%m-%d') >= '$m1' AND msr.created_at <= '$m2' AND LENGTH(rfc) > 2
			GROUP BY msr.rfc ORDER BY msr.id DESC
			");
	}

	public static function delete_material($id)
	{
		DB::table('procurement_designator')->where('id', $id)->update([
			'active' => 0
		]);
	}

	public static function save_material($req) {
		DB::transaction(function () use ($req)
		{
			$get_data   = Excel::toArray(new UploadModel, $req->file('upload_khs'));
			$get_sheet1 = $get_data[0];
			$tgl_start  = $req->tahun_start .'-'. $req->bulan_start .'-01';
			$tgl_end    = $req->tahun_end .'-'. $req->bulan_end . '-' . date('t', strtotime(date($req->tahun_end .'-'. $req->bulan_end, strtotime('last day of this month') ) ) );
			unset($get_sheet1[0]);
			unset($get_sheet1[1]);

			foreach($get_sheet1 as $k => $val)
			{
				if((int)$val[4] != 0 || (int)$val[5] != 0 )
				{
					$string = htmlentities($val[2], ENT_QUOTES, 'UTF-8', false);
					$strng = htmlentities($string, ENT_QUOTES, 'UTF-8', false);

					$chck = DB::table('procurement_designator')->where([
						['designator', preg_replace('/\s+/', '', $val[1])],
						['design_mitra_id', $req->mitra],
						['jenis',$req->pekerjaan],
						['witel', session('auth')->Witel_New],
						['uraian', $strng],
						['tgl_start', '<=', $tgl_start],
						['tgl_end', '>=', $tgl_end],
						['active', 1]
					])->first();

					$data_pd = [
						'jenis'    => $req->pekerjaan,
						'uraian'   => $strng,
						'satuan'   => $val[3],
						'material' => (int)$val[4] ?? 0,
						'jasa'     => (int)$val[5] ?? 0
					];

					if($chck)
					{
						$data_pd['modified_by'] = session('auth')->id_user;
						$data_pd['active'] = 1;

						DB::Table('procurement_designator')->where('id', $chck->id)->update($data_pd);
					}
					else
					{
						$data_pd['designator']      = preg_replace('/\s+/', ' ', $val[1]);
						$data_pd['design_mitra_id'] = $req->mitra;
						$data_pd['witel']           = session('auth')->Witel_New;
						$data_pd['material']        = (int)$val[4] ?? 0;
						$data_pd['jasa']           	= (int)$val[5] ?? 0;
						$data_pd['tgl_start']       = $tgl_start;
						$data_pd['tgl_end']         = $tgl_end;
						$data_pd['created_by']      = session('auth')->id_user;

						DB::Table('procurement_designator')->insert($data_pd);
					}
				}
			}
		});
	}

	// public static function show_pra_kon() {
	// 	return DB::TABLE('procurement_pengadaan')->leftjoin('procurement_mitra', 'procurement_pengadaan.id_mitra', '=', 'procurement_mitra.id')->select('procurement_mitra.*', 'procurement_mitra.id as kunci_mitra', 'procurement_pengadaan.*')->orderBy('procurement_pengadaan.id', 'DESC')->get();
	// }

	public static function get_all_mitra()
	{
		return DB::TABLE('procurement_mitra')->where('active', 1)->get();
	}

	public static function show_yearly_ps()
	{
		return DB::SELECT("SELECT pbu.*, pm.nama_company, pbu.judul AS judul_work, prl.fil_log, prl.fil_desc
		FROM procurement_boq_upload pbu
		LEFT JOIN procurement_mitra pm ON pbu.mitra_id = pm.id
		LEFT JOIN (SELECT
		SUBSTRING_INDEX(GROUP_CONCAT(keterangan ORDER BY created_at DESC), ',', 1 ) AS fil_desc,
		id_upload
		FROM procurement_rekon_log GROUP BY id_upload) AS prl ON prl.id_upload = pbu.id
		WHERE pbu.active = 1 AND pbu.tgl_last_update < Now() AND pbu.tgl_last_update > DATE_ADD(Now(), INTERVAL- 6 MONTH)");
	}

	public static function get_mitra_sp($id_m, $id, $witel, $tgl_hidden, $tgl, $mitra, $pekerjaan)
	{
		$sql = '';

		if(!empty($witel) && $witel != 'All')
		{
			$sql .= "AND pbu.witel LIKE '%$witel%' ";
		}

		switch ($tgl_hidden) {
			case 'Tahun':
				$sql .=" AND pbu.created_at LIKE '$tgl%' ";
			break;
			case 'Custom':
				$tgl = explode(' - ', $tgl);
				$sql .=" AND DATE_FORMAT(pbu.tgl_sp, '%Y-%m-%d') BETWEEN '$tgl[0]' AND '$tgl[1]' ";
			break;
			case 'Bulan':
				$sql .=" AND MONTH(pbu.created_at) = $tgl";
			break;
		}

		// if(!empty($mitra) && strcasecmp($mitra, 'All') != 0)
		// {
		// 	$sql .= " AND pbu.mitra_nm = '$mitra'";
		// }

		if(!empty($pekerjaan) && strcasecmp($pekerjaan, 'All') != 0)
		{
			$sql .= " AND pbu.pekerjaan = '$pekerjaan'";
		}

		switch ($id) {
			case 1:
				$sql .= " AND ps2.aktor = 1";
			break;
			case 2:
				$sql .= " AND ps2.aktor = 2";
			break;
			case 3:
				$sql .= " AND ps2.aktor = 3";
			break;
			case 6:
				$sql .= " AND ps2.aktor = 6";
			break;
			case 7:
				$sql .= " AND ps2.aktor = 7";
			break;
			case 8:
				$sql .= " AND ps2.aktor = 8";
			break;
			case 'finish':
				$sql .= "  AND ps.id = 14 ";
			break;
		}

		if(strcasecmp($id_m, 'All') != 0)
		{
			$sql .= " AND pbu.mitra_nm = '$id_m'";
		}

		return DB::SELECT(
			"SELECT pbu.*, pm.nama_company, pbu.judul as judul_work, ps2.nama as nama_step, pm.witel as witel_mitra, ps2.nama as nama_step, ps.nama as step_before, ps2.nama as step_now, ps3.nama as step_after, DATEDIFF(NOW(), pbu.tgl_last_update) as jml_hri, ps2.aktor_nama as aktor_nama
			FROM procurement_boq_upload pbu
			LEFT JOIN procurement_step ps ON ps.id = pbu.step_id
			LEFT JOIN procurement_step ps2 ON ps2.id = ps.step_after
			LEFT JOIN procurement_step ps3 ON ps3.id = ps2.step_after
			LEFT JOIN 1_2_employee emp ON emp.nik_amija = pbu.created_by
			LEFT JOIN procurement_mitra pm ON pbu.mitra_id = pm.id
			LEFT JOIN procurement_Boq_lokasi As pbl ON pbu.id = pbl.id_upload
			WHERE
			pbu.active = 1 $sql
			GROUP BY pbu.id
			ORDER BY pbu.id ASC");
	}

	public static function get_detail_dashboard($witel, $tgl_hidden, $tgl, $mitra, $pekerjaan, $tgl_progress_kerja, $jenis_period, $aktor)
	{
		$sql = '';

		if(!empty($witel) && $witel != 'All')
		{
			$sql .= "AND pbu.witel LIKE '%$witel%' ";
		}

		switch ($tgl_hidden) {
			case 'Tahun':
				$sql .=" AND pbu.created_at LIKE '$tgl%' ";
			break;
			case 'Custom':
				$tgl = explode(' - ', $tgl);
				$sql .=" AND DATE_FORMAT(pbu.tgl_sp, '%Y-%m-%d') BETWEEN '$tgl[0]' AND '$tgl[1]' ";
			break;
			case 'Bulan':
				$sql .=" AND MONTH(pbu.created_at) = $tgl";
			break;
		}

		if($jenis_period)
		{
			if($jenis_period != 'all')
			{
				$tgl_progress_kerja = explode(' - ', $tgl_progress_kerja);
				$sql .=" AND DATE_FORMAT(pbu.tgl_last_update, '%Y-%m-%d') BETWEEN '$tgl_progress_kerja[0]' AND '$tgl_progress_kerja[1]' ";
			}
		}

		if(!empty($pekerjaan) && strcasecmp($pekerjaan, 'All') != 0)
		{
			$sql .= " AND pbu.pekerjaan = '$pekerjaan'";
		}

		if(strcasecmp($mitra, 'All') != 0 )
		{
			if(!is_numeric($mitra) )
			{
				$sql .= " AND pbu.mitra_nm = '$mitra'";
			}
			else
			{
				$sql .= " AND pbu.mitra_id = '$mitra'";
			}
		}

		switch ($aktor) {
			case 'user':
				$sql .= " AND ps2.aktor = 3";
			break;
			case 'mitra':
				$sql .= " AND ps2.aktor = 2";
			break;
			case 'proc_a':
				$sql .= " AND ps2.aktor = 1";
			break;
			case 'proc_r':
				$sql .= " AND ps2.aktor = 6";
			break;
			case 'finn':
				$sql .= " AND ps2.aktor = 8";
			break;
			case 'cashbank':
				$sql .= " AND ps2.aktor = 9";
			break;
			case 'operation_req_pid':
				$sql .= " AND pbu.step_id IN (1)";
			break;
			case 'commerce_create_pid':
				$sql .= " AND ps.step_after = 2";
			break;
			case 'operation_upload_justif':
				$sql .= " AND ps.step_after = 3";
			break;
			case 'proca_s_pen':
				$sql .= " AND ps.step_after = 4";
			break;
			case 'total_pra_kerja':
				$sql .= " AND ps.main_menu = 'Inisiasi' ";
			break;
			case 'mitra_sanggup':
				$sql .= " AND ps.step_after = 5";
			break;
			case 'proca_sp':
				$sql .= " AND ps.step_after = 6";
			break;
			case 'mitra_boq_rek':
				$sql .= " AND ps.step_after = 7";
			break;
			case 'operation_verif_boq':
				$sql .= " AND ps.step_after = 8";
			break;
			case 'mitra_pelurusan_material':
				$sql .= " AND ps.step_after = 9";
			break;
			case 'commerce_budget':
				$sql .= " AND ps.step_after = 10";
			break;
			case 'proca_ver_doc_area':
				$sql .= " AND ps.step_after = 11";
			break;
			case 'mitra_lengkapi_nomor':
				$sql .= " AND ps.step_after = 12";
			break;
			case 'mitra_ttd':
				$sql .= " AND ps.step_after = 20";
			break;
			case 'procg_ver_doc_reg':
				$sql .= " AND ps.step_after = 13";
			break;
			case 'finance':
				$sql .= " AND ps.step_after = 22";
			break;
			case 'cb':
				$sql .= " AND ps.step_after = 23";
			break;
			case 'total_pra_kerja':
				$sql .= " AND ps.main_menu = 'Inisiasi' ";
			break;
		}
		// dd($sql);
		$data = DB::select("SELECT pbu.*, ps.nama as nama_step, pm.nama_company, pbu.judul as judul_work, pm.witel as witel_mitra, ps2.nama as nama_step, ps.nama as step_before, ps2.nama as step_now, ps3.nama as step_after, DATEDIFF(NOW(), pbu.tgl_last_update) as jml_hri, ps2.aktor_nama as aktor_nama
			FROM procurement_boq_upload pbu
			LEFT JOIN 1_2_employee emp ON emp.nik_amija = pbu.created_by
			LEFT JOIN procurement_mitra pm ON pbu.mitra_id = pm.id
			LEFT JOIN procurement_Boq_lokasi As pbl ON pbu.id = pbl.id_upload
			LEFT JOIN procurement_step ps ON ps.id = pbu.step_id
			LEFT JOIN procurement_step ps2 ON ps2.id = ps.step_after
			LEFT JOIN procurement_step ps3 ON ps3.id = ps2.step_after
			WHERE pbu.id IS NOT NULL AND pbu.active = 1 $sql
			GROUP BY pbu.id
			ORDER BY pekerjaan");

		return $data;
	}

	public static function get_listed_mitra($id, $witel, $tgl_hidden, $tgl, $mitra, $pekerjaan)
	{
		$sql = '';

		if(!empty($witel) && $witel != 'All')
		{
			$sql .= "AND pbu.witel LIKE '%$witel%' ";
		}

		switch ($tgl_hidden) {
			case 'Tahun':
				$sql .=" AND pbu.created_at LIKE '$tgl%' ";
			break;
			case 'Custom':
				$tgl = explode(' - ', $tgl);
				$sql .=" AND DATE_FORMAT(pbu.tgl_sp, '%Y-%m-%d') BETWEEN '$tgl[0]' AND '$tgl[1]' ";
			break;
			case 'Bulan':
				$sql .=" AND MONTH(pbu.created_at) = $tgl";
			break;
		}

		if(session('auth')->proc_level == 2)
		{
			$sql .= " AND pbu.mitra_id = ".session('auth')->id_pm;
		}
		else
		{
			if(!empty($mitra) && strcasecmp($mitra, 'All') != 0)
			{
				$sql .= " AND pbu.mitra_id = $mitra";
			}
		}

		if(!empty($pekerjaan) && strcasecmp($pekerjaan, 'All') != 0)
		{
			$sql .= " AND pbu.pekerjaan = '$pekerjaan'";
		}

		switch ($id) {
			case 1:
				$sql .= " AND ps2.aktor = 1";
			break;
			case 2:
				$sql .= " AND ps2.aktor = 2";
			break;
			case 3:
				$sql .= " AND ps2.aktor = 3";
			break;
			break;
			case 6:
				$sql .= " AND ps2.aktor = 6";
			break;
			case 7:
				$sql .= " AND ps2.aktor = 7";
			break;
			case 'finish':
				$sql .= "  AND ps.step_after = 14 ";
			break;
		}
		// dd($sql);
		return DB::SELECT(
			"SELECT emp.witel_new, nama_company, pbu.mitra_id, COUNT(*) as jml
			FROM procurement_boq_upload pbu
			LEFT JOIN procurement_step ps ON ps.id = pbu.step_id
			LEFT JOIN procurement_step ps2 ON ps2.id = ps.step_after
			LEFT JOIN 1_2_employee emp ON emp.nik_amija = pbu.created_by
			LEFT JOIN procurement_mitra pm ON pbu.mitra_id = pm.id
			WHERE
			pbu.id != '' AND pbu.active = 1 $sql
			GROUP BY pm.nama_company
			ORDER BY pbu.id ASC");
	}

	public static function dashboard($witel, $tgl_hidden, $tgl, $mitra, $pekerjaan)
	{
		$sql = '';

		if(!empty($witel) && $witel != 'All')
		{
			$sql .= "AND pbu.witel LIKE '%$witel%' ";
		}

		switch ($tgl_hidden) {
			case 'Tahun':
				$sql .=" AND pbu.created_at LIKE '$tgl%' ";
			break;
			case 'Custom':
				$tgl  = explode(' - ', $tgl);
				$sql .= " AND DATE_FORMAT(pbu.tgl_sp, '%Y-%m-%d') BETWEEN '$tgl[0]' AND '$tgl[1]' ";
			break;
			case 'Bulan':
				$sql .=" AND MONTH(pbu.created_at) = $tgl";
			break;
		}

		if(session('auth')->proc_level == 2)
		{
			$sql .= " AND pbu.mitra_id = ".session('auth')->id_pm;
		}
		else
		{
			if(!empty($mitra) && strcasecmp($mitra, 'All') != 0)
			{
				$sql .= " AND pbu.mitra_id = $mitra";
			}
		}

		if(!empty($pekerjaan) && strcasecmp($pekerjaan, 'all') != 0)
		{
			$sql .= " AND pbu.pekerjaan = '$pekerjaan'";
		}

		if(session('auth')->peker_pup)
		{
			$pekerjaan = explode(', ', session('auth')->peker_pup );
			$imp_peker = "'". implode("', '", $pekerjaan) . "'";
			$sql      .= " AND pbu.pekerjaan IN ($imp_peker) ";
		}
		// dd($sql);
		return DB::SELECT(
			"SELECT pbu.mitra_nm, pm.nama_company, SUM(CASE WHEN ps2.aktor = 1 THEN 1 ELSE 0 END) as proca,
			SUM(CASE WHEN ps2.aktor = 2 THEN 1 ELSE 0 END) as mitra,
			SUM(CASE WHEN ps2.aktor = 3 THEN 1 ELSE 0 END) as operation,
			SUM(CASE WHEN ps2.aktor = 6 THEN 1 ELSE 0 END) as prog,
			SUM(CASE WHEN ps2.aktor = 7 THEN 1 ELSE 0 END) as commerce,
			SUM(CASE WHEN ps2.aktor = 8 THEN 1 ELSE 0 END) as finance,
			SUM(CASE WHEN ps.id = 14 THEN 1 ELSE 0 END) as finish,
			emp.witel_new, pbu.id
			FROM procurement_boq_upload pbu
			LEFT JOIN procurement_step ps ON ps.id = pbu.step_id
			LEFT JOIN procurement_step ps2 ON ps2.id = ps.step_after
			LEFT JOIN 1_2_employee emp ON emp.nik_amija = pbu.created_by
			LEFT JOIN procurement_mitra pm ON pbu.mitra_id = pm.id
			WHERE
			pbu.id != '' AND pbu.active = 1 $sql
			GROUP BY pbu.mitra_nm
			ORDER BY pbu.id ASC");
	}

	public static function log_rekon($witel, $tgl_hidden, $tgl, $mitra, $pekerjaan)
	{
		$sql = '';

		if(!empty($witel) && $witel != 'All')
		{
			$sql .= "AND pbu.witel LIKE '%$witel%' ";
		}

		switch ($tgl_hidden) {
			case 'Tahun':
				$sql .=" AND pbu.created_at LIKE '$tgl%' ";
			break;
			case 'Custom':
				$tgl  = explode(' - ', $tgl);
				$sql .=" AND DATE_FORMAT(pbu.tgl_sp, '%Y-%m-%d') BETWEEN '$tgl[0]' AND '$tgl[1]' ";
			break;
			case 'Bulan':
				$sql .=" AND MONTH(pbu.created_at) = $tgl";
			break;
		}

		if(session('auth')->proc_level == 2)
		{
			$sql .= " AND pbu.mitra_id = ".session('auth')->id_pm;
		}
		else
		{
			if(!empty($mitra) && strcasecmp($mitra, 'All') != 0)
			{
				$sql .= " AND pbu.mitra_id = $mitra";
			}
		}

		if(!empty($pekerjaan) && strcasecmp($pekerjaan, 'all') != 0)
		{
			$sql .= " AND pbu.pekerjaan = '$pekerjaan'";
		}

		if(session('auth')->peker_pup)
		{
			$pekerjaan = explode(', ', session('auth')->peker_pup );
			$imp_peker = "'". implode("', '", $pekerjaan) . "'";
			$sql 			.= " AND pbu.pekerjaan IN ($imp_peker) ";
		}

		return DB::Select(
		"SELECT pstep.aktor as aktor_id, log.step_id, keterangan, jenis_work, witel_new, judul, pbu.mitra_nm, pbu.detail as catatan, log.created_at, pbu.created_at as buat_prp, log.status_step as ss,
		(CASE WHEN pstep.aktor = 1 THEN 'PROC. AREA'
			WHEN pstep.aktor = 2 THEN 'MITRA'
			WHEN pstep.aktor = 3 THEN 'OPERATION'
			WHEN pstep.aktor = 5 THEN 'TEAM LEADER'
			WHEN pstep.aktor = 6 THEN 'PROC. REGIONAL'
			WHEN pstep.aktor = 7 THEN 'COMMERCE'
		ELSE 'SUPERADMIN' END) As aktor
		FROM procurement_boq_upload pbu
		LEFT JOIN procurement_rekon_log as log ON log.id_upload = pbu.id
		LEFT JOIN procurement_step pstep ON pstep.id = log.step_id
		LEFT JOIN 1_2_employee ON 1_2_employee.nik_amija = pbu.created_by
		WHERE pbu.id IS NOT NULL AND pbu.active = 1 $sql
		ORDER BY log.id DESC
		LIMIT 10");
	}

	public static function timeline($id_up)
	{
		$tl1 = DB::SELECT(
			"SELECT prl.*, pst.aktor_nama, pst.aktor, urutan, pst.action, pst.nama
			FROM procurement_step pst
			LEFT JOIN (SELECT
			SUBSTRING_INDEX(GROUP_CONCAT(id ORDER BY id DESC), ',', 1 ) AS id,
			SUBSTRING_INDEX(GROUP_CONCAT(keterangan ORDER BY id DESC), ',', 1 ) AS keterangan,
			SUBSTRING_INDEX(GROUP_CONCAT(created_at ORDER BY id DESC), ',', 1 ) AS created_at,
			SUBSTRING_INDEX(GROUP_CONCAT(closed_at ORDER BY id DESC), ',', 1 ) AS closed_at,
			SUBSTRING_INDEX(GROUP_CONCAT(created_by ORDER BY id DESC), ',', 1 ) AS created_by,
			SUBSTRING_INDEX(GROUP_CONCAT(aktor ORDER BY id DESC), ',', 1 ) AS aktor,
			SUBSTRING_INDEX(GROUP_CONCAT(step_id ORDER BY id DESC), ',', 1 ) AS step_id,
			id_upload
			FROM procurement_rekon_log WHERE id_upload = $id_up GROUP BY step_id) AS prl ON prl.step_id = pst.id
			WHERE action = 'forward'
			ORDER BY urutan ASC");

		$tl1 = json_decode(json_encode(($tl1) ), TRUE );
		foreach($tl1 as $k => $v)
		{
			$data_fresh[$k]['id'] = $v['id'];
			$data_fresh[$k]['urutan'] = $v['urutan'];
			$data_fresh[$k]['nama'] = $v['nama'];
			$data_fresh[$k]['action'] = $v['action'];
			$data_fresh[$k]['aktor'] = $v['aktor'];
			$data_fresh[$k]['aktor_nama'] = $v['aktor_nama'];
			$data_fresh[$k]['keterangan'] = $v['keterangan'];
			$data_fresh[$k]['created_by'] = $v['created_by'];
			$data_fresh[$k]['tgl_start'] = $v['created_at'];
			$data_fresh[$k]['tgl_end'] = $v['closed_at'];

			$selisih = date_diff(date_create($v['created_at']), date_create($v['closed_at']) );

			$text = [];

			if( $selisih->format('%m') != 0 )
			{
				$text[] = $selisih->format('%m Bulan');
			}

			if( $selisih->format('%d') != 0 )
			{
				$text[] = $selisih->format('%d Hari');
			}

			if( $selisih->format('%h') != 0 )
			{
				$text[] = $selisih->format('%h Jam');
			}

			if( $selisih->format('%i') != 0 )
			{
				$text[] = $selisih->format('%i Menit');
			}

			$data_fresh[$k]['selisih_waktu'] = implode(' ', $text);
			$data_fresh[$k]['hari_diff'] = $selisih->format("%d");
		}
		return $data_fresh;
	}

	public static function log_rekon_mitra($id)
	{
		return DB::Select(
		"SELECT pstep.aktor as aktor_id, log.step_id, keterangan, jenis_work, witel_new, judul, pbu.detail as catatan, pbu.mitra_nm, log.created_at, prp.created_at as buat_prp, log.status_step as ss,
		(CASE WHEN pstep.aktor = 1 THEN 'PROC. AREA'
			WHEN pstep.aktor = 2 THEN 'MITRA'
			WHEN pstep.aktor = 3 THEN 'OPERATION'
			WHEN pstep.aktor = 5 THEN 'TEAM LEADER'
			WHEN pstep.aktor = 6 THEN 'PROC. REGIONAL'
			WHEN pstep.aktor = 7 THEN 'COMMERCE'
		ELSE 'SUPERADMIN' END) As aktor
		FROM procurement_boq_upload pbu
		LEFT JOIN procurement_rekon_log as log ON log.id_upload = pbu.id
		LEFT JOIN procurement_req_pid prp ON pbu.id = log.id_upload
		LEFT JOIN procurement_step pstep ON pstep.id = log.step_id
		LEFT JOIN 1_2_employee ON 1_2_employee.nik_amija = prp.created_by
		WHERE pbu.id = $id AND pbu.active = 1
		GROUP BY log.id
		ORDER BY log.id DESC");
	}

	public static function dalapa_mitra()
	{
		return DB::table('procurement_mitra')->select('dalapa_mitra_id As id', 'nama_company As text', 'witel')->Where('dalapa_mitra_id', '!=', 0)->get();
	}

	public static function active_mitra()
	{
		return DB::table('procurement_mitra')->select('id As id', 'nama_company As text', 'alias_nama_company', 'witel')->Where('active', '!=', 0)->get();
	}

	public static function get_dalapa_data($req)
	{
		$bln = $req->tahun .'-'. $req->bulan;
		$mitra= session('auth')->dalapa_mitra_id;
		// REPLACE(SUBSTRING_INDEX(alpro_name, '/', 1), 'ODP', 'ODC') as ODC
		return DB::SELECT(
			"SELECT *, SUM(total_harga) As total_hrg
			FROM procurement_dalapa_pekerjaan
			WHERE tahun_bulan_grab = '$bln' AND mitra_id_dalapa ='$mitra'
			GROUP BY nama_team");
	}

	public static function read_dalapa($req)
	{
		$get_data = Excel::toArray(new UploadModel, $req->file('upload_boq') );
		$get_sheet1 = $get_data[0];

		foreach ($get_sheet1 as $key => &$value) {
			if($key == 8)
			{

				unset($get_sheet1[$key][0]);
				unset($get_sheet1[$key][1]);
				unset($get_sheet1[$key][2]);
				unset($get_sheet1[$key][3]);
				unset($get_sheet1[$key][4]);
				unset($get_sheet1[$key][key(array_slice($value, -1, 1, true) )]);
				$list_odp = $get_sheet1[$key];
			}
			if($key < 9) continue;
			if(!empty($value[0]) )
			{
				unset($get_sheet1[$key][key(array_slice($value, -1, 1, true) )]);
				$list_material[$key]['design'] = $value[0];
				$list_material[$key]['detail'] = $value[1];
				$list_material[$key]['satuan'] = $value[2];
				$list_material[$key]['material'] = (int)$value[3];
				$list_material[$key]['jasa'] = (int)$value[4];

				foreach($value as $keyc1 => $valc1)
				{
					if($keyc1 < 5) continue;
					$list_material[$key]['list_odp'][$get_sheet1[8][$keyc1] ] = $value[$keyc1];
				}
			}
		}
		return [$list_odp, $list_material];
	}

	public static function getDataDashboard($witel, $tgl_hidden, $tgl, $mitra, $pekerjaan, $tampilan, $tgl_progress_kerja, $jenis_period)
	{
		$sql = '';
		if(!empty($witel) && strcasecmp($witel, 'All') )
		{
			$sql .= "AND pbu.witel LIKE '%$witel%' ";
		}

		if($jenis_period)
		{
			if($jenis_period != 'all')
			{
				if($jenis_period == 'sp')
				{
					switch ($tgl_hidden) {
						case 'Tahun':
							$sql .=" AND pbu.created_at LIKE '$tgl%' ";
						break;
						case 'Custom':
							if($tgl)
							{
								$tgl = explode(' - ', $tgl);
								$sql .=" AND DATE_FORMAT(pbu.tgl_sp, '%Y-%m-%d') BETWEEN '$tgl[0]' AND '$tgl[1]' ";
							}
						break;
						case 'Bulan':
							if($tgl)
							{
								$sql .=" AND MONTH(pbu.created_at) = $tgl";
							}
						break;
					}
				}
				else
				{
					$tgl_progress_kerja = explode(' - ', $tgl_progress_kerja);
					$sql .=" AND DATE_FORMAT(pbu.tgl_last_update, '%Y-%m-%d') BETWEEN '$tgl_progress_kerja[0]' AND '$tgl_progress_kerja[1]' ";
				}
			}
		}

		if(session('auth')->proc_level == 2)
		{
			$sql .= " AND pbu.mitra_id = ".session('auth')->id_pm;
		}
		else
		{
			if(!empty($mitra) && strcasecmp($mitra, 'all') != 0)
			{
				$sql .= " AND pbu.mitra_id = $mitra";
			}
		}

		if(!empty($pekerjaan) && strcasecmp($pekerjaan, 'all') != 0)
		{
			$sql .= " AND pbu.pekerjaan = '$pekerjaan'";
		}

		if(session('auth')->peker_pup)
		{
			$pekerjaan = explode(', ', session('auth')->peker_pup );
			$imp_peker = "'". implode("', '", $pekerjaan) . "'";
			$sql .= " AND pbu.pekerjaan IN ($imp_peker) ";
		}

		$data = DB::select("SELECT
			SUM(CASE WHEN ps2.aktor = 3 THEN 1 ELSE NULL END) As user,
			SUM(CASE WHEN ps2.aktor = 2 THEN 1 ELSE NULL END) As mitra,
			SUM(CASE WHEN ps2.aktor = 1 THEN 1 ELSE NULL END) As proc_a,
			SUM(CASE WHEN ps2.aktor = 6 THEN 1 ELSE NULL END) As proc_r,
			SUM(CASE WHEN ps2.aktor = 8 THEN 1 ELSE NULL END) As finn,
			SUM(CASE WHEN ps2.aktor = 9 THEN 1 ELSE NULL END) As cashbank,
			pekerjaan,
			SUM(ROUND( (CASE WHEN total_sp IS NULL THEN ( total_material_sp + total_jasa_sp ) ELSE total_sp END) ) ) as gd_sp,
			(CASE WHEN ps2.aktor = 3 THEN SUM((CASE WHEN total_sp IS NULL THEN ( total_material_sp + total_jasa_sp ) ELSE total_sp END)) END) as gd_sp_user,
			(SUM(CASE WHEN ps2.aktor = 2 THEN (CASE WHEN total_sp IS NULL THEN ( total_material_sp + total_jasa_sp ) ELSE total_sp END) END) ) as gd_sp_mitra,
			(SUM(CASE WHEN ps2.aktor = 1 THEN (CASE WHEN total_sp IS NULL THEN ( total_material_sp + total_jasa_sp ) ELSE total_sp END) END) ) as gd_sp_proc_a,
			(SUM(CASE WHEN ps2.aktor = 6 THEN (CASE WHEN total_sp IS NULL THEN ( total_material_sp + total_jasa_sp ) ELSE total_sp END) END) ) as gd_sp_proc_r,
			(SUM(CASE WHEN ps2.aktor = 8 THEN (CASE WHEN total_sp IS NULL THEN ( total_material_sp + total_jasa_sp ) ELSE total_sp END) END) ) as gd_sp_finn,
			(SUM(CASE WHEN ps2.aktor = 9 THEN (CASE WHEN total_sp IS NULL THEN ( total_material_sp + total_jasa_sp ) ELSE total_sp END) END) ) as gd_sp_cashbank,
			(total_material_rekon + total_jasa_rekon ) as gd_rekon,
			(SUM(CASE WHEN ps2.aktor = 3 THEN total_material_rekon + total_jasa_rekon END) ) as gd_rekon_user,
			(SUM(CASE WHEN ps2.aktor = 2 THEN total_material_rekon + total_jasa_rekon END) ) as gd_rekon_mitra,
			(SUM(CASE WHEN ps2.aktor = 1 THEN total_material_rekon + total_jasa_rekon END) ) as gd_rekon_proc_a,
			(SUM(CASE WHEN ps2.aktor = 6 THEN total_material_rekon + total_jasa_rekon END) ) as gd_rekon_proc_r,
			(SUM(CASE WHEN ps2.aktor = 8 THEN total_material_rekon + total_jasa_rekon END) ) as gd_rekon_finn,
			(SUM(CASE WHEN ps2.aktor = 9 THEN total_material_rekon + total_jasa_rekon END) ) as gd_rekon_cashbank,
			(SUM(CASE WHEN step_id != 14 THEN 1 ELSE NULL END) ) As jml_sp
			FROM procurement_boq_upload pbu
			LEFT JOIN procurement_step ps ON ps.id = pbu.step_id
			LEFT JOIN procurement_step ps2 ON ps2.id = ps.step_after
			WHERE pbu.id IS NOT NULL AND pbu.active = 1 $sql
			GROUP BY pekerjaan
			ORDER BY pekerjaan");

		$finish_data = [];
		$count= 0;
		foreach ($data as $key => $val) {
			if(in_array($tampilan, ['val_price_sp']) )
			{
				$count = 3;
				$finish_data[$val->pekerjaan]['data']['user_c']       = $val->user;
				$finish_data[$val->pekerjaan]['data']['hrg_user']     = $val->gd_sp_user;
				$finish_data[$val->pekerjaan]['data']['user_per']     = $val->gd_sp == 0 ? 0: ROUND( ($val->gd_sp_user / $val->gd_sp) * 100, 0) .'%';
				$finish_data[$val->pekerjaan]['data']['mitra_c']      = $val->mitra;
				$finish_data[$val->pekerjaan]['data']['hrg_mitra']    = $val->gd_sp_mitra;
				$finish_data[$val->pekerjaan]['data']['mitra_per']    = $val->gd_sp == 0 ? 0: ROUND( ($val->gd_sp_mitra / $val->gd_sp) * 100, 0) .'%';
				$finish_data[$val->pekerjaan]['data']['proc_a_c']     = $val->proc_a;
				$finish_data[$val->pekerjaan]['data']['hrg_proc_a']   = $val->gd_sp_proc_a;
				$finish_data[$val->pekerjaan]['data']['proc_a_per']   = $val->gd_sp == 0 ? 0: ROUND( ($val->gd_sp_proc_a / $val->gd_sp) * 100, 0) .'%';
				$finish_data[$val->pekerjaan]['data']['proc_r_c']     = $val->proc_r;
				$finish_data[$val->pekerjaan]['data']['hrg_proc_r']   = $val->gd_sp_proc_r;
				$finish_data[$val->pekerjaan]['data']['proc_r_per']   = $val->gd_sp == 0 ? 0: ROUND( ($val->gd_sp_proc_r / $val->gd_sp) * 100, 0) .'%';
				$finish_data[$val->pekerjaan]['data']['finn_c']       = $val->finn;
				$finish_data[$val->pekerjaan]['data']['hrg_finn']     = $val->gd_sp_finn;
				$finish_data[$val->pekerjaan]['data']['finn_per']     = $val->gd_sp == 0 ? 0: ROUND( ($val->gd_sp_finn / $val->gd_sp) * 100, 0) .'%';
				$finish_data[$val->pekerjaan]['data']['cashbank_c']   = $val->cashbank;
				$finish_data[$val->pekerjaan]['data']['hrg_cashbank'] = $val->gd_sp_cashbank;
				$finish_data[$val->pekerjaan]['data']['cashbank_per'] = $val->gd_sp == 0 ? 0: ROUND( ($val->gd_sp_cashbank / $val->gd_sp) * 100, 0) .'%';
			}

			if(in_array($tampilan, ['price_sp']) || empty($tampilan) )
			{
				$count = 2;

				$finish_data[$val->pekerjaan]['data']['hrg_user']     = $val->gd_sp_user;
				$finish_data[$val->pekerjaan]['data']['user_per']     = $val->gd_sp == 0 ? 0: ROUND( ($val->gd_sp_user / $val->gd_sp) * 100, 0) .'%';
				$finish_data[$val->pekerjaan]['data']['hrg_mitra']    = $val->gd_sp_mitra;
				$finish_data[$val->pekerjaan]['data']['mitra_per']    = $val->gd_sp == 0 ? 0: ROUND( ($val->gd_sp_mitra / $val->gd_sp) * 100, 0) .'%';
				$finish_data[$val->pekerjaan]['data']['hrg_proc_a']   = $val->gd_sp_proc_a;
				$finish_data[$val->pekerjaan]['data']['proc_a_per']   = $val->gd_sp == 0 ? 0: ROUND( ($val->gd_sp_proc_a / $val->gd_sp) * 100, 0) .'%';
				$finish_data[$val->pekerjaan]['data']['hrg_proc_r']   = $val->gd_sp_proc_r;
				$finish_data[$val->pekerjaan]['data']['proc_r_per']   = $val->gd_sp == 0 ? 0: ROUND( ($val->gd_sp_proc_r / $val->gd_sp) * 100, 0) .'%';
				$finish_data[$val->pekerjaan]['data']['hrg_finn']     = $val->gd_sp_finn;
				$finish_data[$val->pekerjaan]['data']['finn_per']     = $val->gd_sp == 0 ? 0: ROUND( ($val->gd_sp_finn / $val->gd_sp) * 100, 0) .'%';
				$finish_data[$val->pekerjaan]['data']['hrg_cashbank'] = $val->gd_sp_cashbank;
				$finish_data[$val->pekerjaan]['data']['cashbank_per'] = $val->gd_sp == 0 ? 0: ROUND( ($val->gd_sp_cashbank / $val->gd_sp) * 100, 0) .'%';
			}

			if(in_array($tampilan, ['val_sp']) )
			{
				$count = 2;

				$total = $val->user + $val->mitra + $val->proc_a + $val->proc_r + $val->finn + $val->cashbank;
				$finish_data[$val->pekerjaan]['data']['user_c']       = $val->user;
				$finish_data[$val->pekerjaan]['data']['user_per']     = $total == 0 ? 0: ROUND( ($val->user / $total) * 100, 0) .'%';
				$finish_data[$val->pekerjaan]['data']['mitra_c']      = $val->mitra;
				$finish_data[$val->pekerjaan]['data']['mitra_per']    = $total == 0 ? 0: ROUND( ($val->mitra / $total) * 100, 0) .'%';
				$finish_data[$val->pekerjaan]['data']['proc_a_c']     = $val->proc_a;
				$finish_data[$val->pekerjaan]['data']['proc_a_per']   = $total == 0 ? 0: ROUND( ($val->proc_a / $total) * 100, 0) .'%';
				$finish_data[$val->pekerjaan]['data']['proc_r_c']     = $val->proc_r;
				$finish_data[$val->pekerjaan]['data']['proc_r_per']   = $total == 0 ? 0: ROUND( ($val->proc_r / $total) * 100, 0) .'%';
				$finish_data[$val->pekerjaan]['data']['finn_c']       = $val->finn;
				$finish_data[$val->pekerjaan]['data']['finn_per']     = $total == 0 ? 0: ROUND( ($val->finn / $total) * 100, 0) .'%';
				$finish_data[$val->pekerjaan]['data']['cashbank_c']   = $val->cashbank;
				$finish_data[$val->pekerjaan]['data']['cashbank_per'] = $total == 0 ? 0: ROUND( ($val->cashbank / $total) * 100, 0) .'%';
			}

			if(in_array($tampilan, ['price_rekon']) )
			{
				$count = 2;

				$finish_data[$val->pekerjaan]['data']['hrg_user']     = $val->gd_rekon_user;
				$finish_data[$val->pekerjaan]['data']['user_per']     = $val->gd_rekon == 0 ? 0: ROUND( ($val->gd_rekon_user / $val->gd_rekon) * 100, 0) .'%';
				$finish_data[$val->pekerjaan]['data']['hrg_mitra']    = $val->gd_rekon_mitra;
				$finish_data[$val->pekerjaan]['data']['mitra_per']    = $val->gd_rekon == 0 ? 0: ROUND( ($val->gd_rekon_mitra / $val->gd_rekon) * 100, 0) .'%';
				$finish_data[$val->pekerjaan]['data']['hrg_proc_a']   = $val->gd_rekon_proc_a;
				$finish_data[$val->pekerjaan]['data']['proc_a_per']   = $val->gd_rekon == 0 ? 0: ROUND( ($val->gd_rekon_proc_a / $val->gd_rekon) * 100, 0) .'%';
				$finish_data[$val->pekerjaan]['data']['hrg_proc_r']   = $val->gd_rekon_proc_r;
				$finish_data[$val->pekerjaan]['data']['proc_r_per']   = $val->gd_rekon == 0 ? 0: ROUND( ($val->gd_rekon_proc_r / $val->gd_rekon) * 100, 0) .'%';
				$finish_data[$val->pekerjaan]['data']['hrg_finn']     = $val->gd_rekon_finn;
				$finish_data[$val->pekerjaan]['data']['finn_per']     = $val->gd_rekon == 0 ? 0: ROUND( ($val->gd_rekon_finn / $val->gd_rekon) * 100, 0) .'%';
				$finish_data[$val->pekerjaan]['data']['hrg_cashbank'] = $val->gd_rekon_cashbank;
				$finish_data[$val->pekerjaan]['data']['cashbank_per'] = $val->gd_rekon == 0 ? 0: ROUND( ($val->gd_rekon_cashbank / $val->gd_rekon) * 100, 0) .'%';
			}

			if(in_array($tampilan, ['val_sp_price_rekon']) )
			{
				$count = 3;

				$finish_data[$val->pekerjaan]['data']['user_c']       = $val->user;
				$finish_data[$val->pekerjaan]['data']['hrg_user']     = $val->gd_rekon_user;
				$finish_data[$val->pekerjaan]['data']['user_per']     = $val->gd_rekon == 0 ? 0: ROUND( ($val->gd_rekon_user / $val->gd_rekon) * 100, 0) .'%';
				$finish_data[$val->pekerjaan]['data']['mitra_c']      = $val->mitra;
				$finish_data[$val->pekerjaan]['data']['hrg_mitra']    = $val->gd_rekon_mitra;
				$finish_data[$val->pekerjaan]['data']['mitra_per']    = $val->gd_rekon == 0 ? 0: ROUND( ($val->gd_rekon_mitra / $val->gd_rekon) * 100, 0) .'%';
				$finish_data[$val->pekerjaan]['data']['proc_a_c']     = $val->proc_a;
				$finish_data[$val->pekerjaan]['data']['hrg_proc_a']   = $val->gd_rekon_proc_a;
				$finish_data[$val->pekerjaan]['data']['proc_a_per']   = $val->gd_rekon == 0 ? 0: ROUND( ($val->gd_rekon_proc_a / $val->gd_rekon) * 100, 0) .'%';
				$finish_data[$val->pekerjaan]['data']['proc_r_c']     = $val->proc_r;
				$finish_data[$val->pekerjaan]['data']['hrg_proc_r']   = $val->gd_rekon_proc_r;
				$finish_data[$val->pekerjaan]['data']['proc_r_per']   = $val->gd_rekon == 0 ? 0: ROUND( ($val->gd_rekon_proc_r / $val->gd_rekon) * 100, 0) .'%';
				$finish_data[$val->pekerjaan]['data']['finn_c']       = $val->finn;
				$finish_data[$val->pekerjaan]['data']['hrg_finn']     = $val->gd_rekon_finn;
				$finish_data[$val->pekerjaan]['data']['finn_per']     = $val->gd_rekon == 0 ? 0: ROUND( ($val->gd_rekon_finn / $val->gd_rekon) * 100, 0) .'%';
				$finish_data[$val->pekerjaan]['data']['cashbank_c']   = $val->cashbank;
				$finish_data[$val->pekerjaan]['data']['hrg_cashbank'] = $val->gd_rekon_cashbank;
				$finish_data[$val->pekerjaan]['data']['cashbank_per'] = $val->gd_rekon == 0 ? 0: ROUND( ($val->gd_rekon_cashbank / $val->gd_rekon) * 100, 0) .'%';
			}
		}
		return [$finish_data, $count];
	}

	public static function get_result_search($req)
	{
		$sql = '';
		$result_data = [];

		if(in_array($req->search, ['detail', 'simple']) )
		{
			$pekerjaan = trim(preg_replace('/[\t\n\r\s]+/', '', $req->judul_pekerjaan) );

			if($req->search == 'detail')
			{
				$jenis_kontrak = $req->jenis_kontrak;
				$mitra         = "'". implode("','", $req->mitra) . "'";
				$tgl           = $req->tahun .'-'. $req->bulan;

				$sql .= " AND pbu.mitra_id IN ($mitra) AND REPLACE(REPLACE(REPLACE(judul, ' ', ''), '\t', ''), '\n', '') LIKE '%$pekerjaan%' AND jenis_kontrak = '$jenis_kontrak' AND bulan_pengerjaan = '$tgl'";
			}
			else
			{
				if(@session('auth')->proc_level == 2)
				{
					$mitra = session('auth')->id_pm;
					$sql   = " AND pbu.mitra_id = $mitra";
				}
				$sql .= " AND REPLACE(REPLACE(REPLACE(judul, ' ', ''), '\t', ''), '\n', '') LIKE '%$pekerjaan%'";
			}

			if(session('auth')->peker_pup)
			{
				$pekerjaan = explode(', ', session('auth')->peker_pup );
				$imp_peker = "'". implode("', '", $pekerjaan) . "'";
				$sql .= " AND pbu.pekerjaan IN ($imp_peker) ";
			}

			$result_data = DB::SELECT("SELECT
			pbu.*, pm.id as company_id, pm.nama_company, pm.alamat_company,
			(SELECT SUM(budget) FROM procurement_req_pid WHERE id_upload = pbu.id) AS budget_req,
			(CASE WHEN tgl_s_pen IS NOT NULL THEN DATE_ADD(tgl_s_pen, INTERVAL tgl_jatuh_tmp DAY) ELSE DATE_ADD(tgl_sp, INTERVAL tgl_jatuh_tmp DAY) END) As durasi,
			ROUND( (CASE WHEN total_sp IS NULL THEN ( total_material_sp + total_jasa_sp ) ELSE total_sp END) ) as gd_sp,
			ROUND( ( (CASE WHEN total_sp IS NULL THEN ( total_material_sp + total_jasa_sp ) ELSE total_sp END) ) * (CASE WHEN tgl_bast IS NULL AND DATE_FORMAT(tgl_sp, '%Y-%m') >= '2022-04' OR DATE_FORMAT(tgl_bast, '%Y-%m') >= '2022-04' THEN 11/100 ELSE 10/100 END) ) as gd_ppn_sp,
			ROUND( ( (CASE WHEN total_sp IS NULL THEN ( total_material_sp + total_jasa_sp ) ELSE total_sp END) ) + ( ( (CASE WHEN total_sp IS NULL THEN ( total_material_sp + total_jasa_sp ) ELSE total_sp END) ) * (CASE WHEN tgl_bast IS NULL AND DATE_FORMAT(tgl_sp, '%Y-%m') >= '2022-04' OR DATE_FORMAT(tgl_bast, '%Y-%m') >= '2022-04' THEN 11/100 ELSE 10/100 END) ) ) as total_sp
			FROM procurement_boq_upload pbu
			LEFT JOIN (SELECT pbl.id_upload, MAX(id) As id
			FROM procurement_Boq_lokasi pbl GROUP BY pbl.id_upload) As pbl ON pbu.id = pbl.id_upload
			LEFT JOIN procurement_mitra pm ON pm.id = pbu.mitra_id
			WHERE pbu.id IS NOT NULL $sql
			GROUP BY pbu.id
			ORDER BY gd_sp DESC");

			if(in_array($req->search, ['detail', 'simple']) )
			{
				$head = ['#', 'Uraian', 'Mitra', 'Nilai SP', 'Detail'];
			}
		}
		elseif($req->search == 'rfc')
		{
			$rfc = array_map(function($x){
				return trim(preg_replace('/[\t\n\r\s]+/', ' ', $x) );
			}, $req->rfc_num);

			$result_data = DB::table('procurement_rfc_osp As pro')
			->leftjoin('procurement_boq_upload As pbu', 'pro.id_upload', '=', 'pbu.id')
			->select('pro.*', 'pbu.judul', 'pbu.active as active_pbu')
			->whereIn('rfc', $rfc)
			->GroupBy('rfc')
			->get();

			$head = ['#', 'Nomor RFC', 'Status RFC', 'Pekerjaan Yang Menggunakan RFC', 'Detail RFC'];

			if(count($result_data) == 0)
			{
				$result_data = DB::table('inventory_material')->select('no_rfc as rfc', DB::RAW("CONCAT('Belum Ada Di Pekerjaan') As status_rfc"), DB::RAW("NULL As id_upload"), DB::RAW("CONCAT('Kosong') As ket_im"))->whereIn('no_rfc', $rfc)->GroupBy('no_rfc')->get();
			}
		}
		elseif($req->search == 'pid')
		{
			$pid = array_map(function($x){
				return trim(preg_replace('/[\t\n\r\s]+/', ' ', $x) );
			}, $req->pid_num);

			$red = DB::table('procurement_req_pid As prp')
			->leftjoin('procurement_pid As pp', 'pp.id', '=', 'prp.id_pid_req')
			->leftjoin('procurement_boq_upload As pbu', 'prp.id_upload', '=', 'pbu.id')
			->select('pp.*', 'pbu.judul', 'pbu.active as active_pbu', 'pbu.id As id_upload')
			->whereIn('pid', $pid )
			->get();

			$apm = self::get_apm_budget_all();

			foreach($red as $v)
			{
				$result_data[$v->pid]['pid'] = $v->pid;
				$result_data[$v->pid]['status_pid'] = $v->budget_exceeded;
				$result_data[$v->pid]['po'][$v->judul]['id_upload'] = $v->id_upload;
				$result_data[$v->pid]['po'][$v->judul]['judul'] = $v->judul;
				$result_data[$v->pid]['po'][$v->judul]['active_pbu'] = $v->active_pbu;

				$find_k = array_search($v->pid, array_column($apm, 'wbs') );

				$result_data[$v->pid]['keperluan'] = [];

				if($find_k !== FALSE)
				{
					$result_data[$v->pid]['keperluan'] = $apm[$find_k]['keperluan'];
				}
			}

			$head = ['#', 'Nomor PID', 'Material : Stock', 'Material : Non Stock', 'Status PID', 'Pekerjaan Yang Menggunakan PID'];
		}
		elseif($req->search == 'sp_num')
		{
			$req->sp_num = trim(preg_replace('/[\t\n\r\s]+/', ' ', $req->sp_num) );
			$result_data = DB::SELECT("SELECT
			pbu.*, pm.id as company_id, pm.nama_company, pm.alamat_company,
			(SELECT SUM(budget) FROM procurement_req_pid WHERE id_upload = pbu.id) AS budget_req,
			(CASE WHEN tgl_s_pen IS NOT NULL THEN DATE_ADD(tgl_s_pen, INTERVAL tgl_jatuh_tmp DAY) ELSE DATE_ADD(tgl_sp, INTERVAL tgl_jatuh_tmp DAY) END) As durasi,
			ROUND( (CASE WHEN total_sp IS NULL THEN ( total_material_sp + total_jasa_sp ) ELSE total_sp END) ) as gd_sp,
			ROUND( ( (CASE WHEN total_sp IS NULL THEN ( total_material_sp + total_jasa_sp ) ELSE total_sp END) ) * (CASE WHEN tgl_bast IS NULL AND DATE_FORMAT(tgl_sp, '%Y-%m') >= '2022-04' OR DATE_FORMAT(tgl_bast, '%Y-%m') >= '2022-04' THEN 11/100 ELSE 10/100 END) ) as gd_ppn_sp,
			ROUND( ( (CASE WHEN total_sp IS NULL THEN ( total_material_sp + total_jasa_sp ) ELSE total_sp END) ) + ( ( (CASE WHEN total_sp IS NULL THEN ( total_material_sp + total_jasa_sp ) ELSE total_sp END) ) * (CASE WHEN tgl_bast IS NULL AND DATE_FORMAT(tgl_sp, '%Y-%m') >= '2022-04' OR DATE_FORMAT(tgl_bast, '%Y-%m') >= '2022-04' THEN 11/100 ELSE 10/100 END) ) ) as total_sp
			FROM procurement_boq_upload pbu
			LEFT JOIN (SELECT pbl.id_upload, MAX(id) As id
			FROM procurement_Boq_lokasi pbl GROUP BY pbl.id_upload) As pbl ON pbu.id = pbl.id_upload
			LEFT JOIN procurement_mitra pm ON pm.id = pbu.mitra_id
			WHERE pbu.id IS NOT NULL AND no_sp LIKE '%$req->sp_num%'
			GROUP BY pbu.id
			ORDER BY gd_sp DESC");

			$head = ['#', 'Uraian', 'Mitra', 'Nilai SP', 'Detail'];
		}

		return [$head, $result_data];
	}

	public static function list_pekerjaan_aktif($witel, $tgl_hidden, $tgl, $mitra, $pekerjaan, $tgl_progress_kerja, $jenis_period)
	{
		//user = 1,2,3,7,9
		//mitra = 5,10,20
		//proc_a = 4,6,8
		//finn = 22
		//cash = 23

		$sql = '';
		if(!empty($witel) && strcasecmp($witel, 'all') != 0)
		{
			$sql .= "AND pbu.witel LIKE '%$witel%' ";
		}

		if($jenis_period)
		{
			if(strcasecmp($jenis_period, 'all') != 0)
			{
				if($jenis_period == 'sp')
				{
					switch ($tgl_hidden) {
						case 'Tahun':
							$sql .=" AND pbu.created_at LIKE '$tgl%' ";
						break;
						case 'Custom':
							$tgl = explode(' - ', $tgl);
							$sql .=" AND DATE_FORMAT(pbu.tgl_sp, '%Y-%m-%d') BETWEEN '$tgl[0]' AND '$tgl[1]' ";
						break;
						case 'Bulan':
							if($tgl)
							{
								$sql .=" AND MONTH(pbu.created_at) = $tgl";
							}
						break;
					}
				}
				else
				{
					$tgl_progress_kerja = explode(' - ', $tgl_progress_kerja);
					$sql .=" AND DATE_FORMAT(pbu.tgl_last_update, '%Y-%m-%d') BETWEEN '$tgl_progress_kerja[0]' AND '$tgl_progress_kerja[1]' ";
				}
			}
		}

		if(@session('auth')->proc_level == 2)
		{
			$sql .= " AND pbu.mitra_id = ".session('auth')->id_pm;
		}
		else
		{
			if(!empty($mitra) && strcasecmp($mitra, 'all') != 0)
			{
				$sql .= " AND pbu.mitra_id = $mitra";
			}
		}

		if(!empty($pekerjaan) && strcasecmp($pekerjaan, 'all') != 0)
		{
			$sql .= " AND pbu.pekerjaan = '$pekerjaan'";
		}

		if(session('auth')->peker_pup)
		{
			$pekerjaan = explode(', ', session('auth')->peker_pup );
			$imp_peker = "'". implode("', '", $pekerjaan) . "'";
			$sql .= " AND pbu.pekerjaan IN ($imp_peker) ";
		}

		$fn = DB::SELECT("SELECT mitra_nm, CONCAT(pm.alias_nama_company, ' (', pm.witel, ')' ) As label_mitra,
		SUM(CASE WHEN pbu.step_id IN (1) THEN 1 ELSE 0 END) as operation_req_pid,
		SUM(CASE WHEN ps.step_after = 2 THEN 1 ELSE 0 END) as commerce_create_pid,
		SUM(CASE WHEN ps.step_after = 3 THEN 1 ELSE 0 END) as operation_upload_justif,
		SUM(CASE WHEN ps.step_after = 4 THEN 1 ELSE 0 END) as proca_s_pen,
		SUM(CASE WHEN ps.step_after = 5 THEN 1 ELSE 0 END) as mitra_sanggup,
		SUM(CASE WHEN ps.step_after = 6 THEN 1 ELSE 0 END) as proca_sp,
		SUM(CASE WHEN ps.main_menu = 'Inisiasi' THEN 1 ELSE 0 END) as total_pra_kerja,
		SUM(CASE WHEN ps.step_after = 7 THEN 1 ELSE 0 END) as mitra_boq_rek,
		SUM(CASE WHEN ps.step_after = 8 THEN 1 ELSE 0 END) as operation_verif_boq,
		SUM(CASE WHEN ps.step_after = 9 THEN 1 ELSE 0 END) as mitra_pelurusan_material,
		SUM(CASE WHEN ps.step_after = 10 THEN 1 ELSE 0 END) as commerce_budget,
		SUM(CASE WHEN ps.step_after = 11 THEN 1 ELSE 0 END) as proca_ver_doc_area,
		SUM(CASE WHEN ps.step_after = 12 THEN 1 ELSE 0 END) as mitra_lengkapi_nomor,
		SUM(CASE WHEN ps.step_after = 20 THEN 1 ELSE 0 END) as mitra_ttd,
		SUM(CASE WHEN ps.step_after = 13 THEN 1 ELSE 0 END) as procg_ver_doc_reg,
		SUM(CASE WHEN ps.step_after = 22 THEN 1 ELSE 0 END) as finance,
		SUM(CASE WHEN ps.step_after = 23 THEN 1 ELSE 0 END) as cashbank,
		SUM(CASE WHEN ps.step_after = 14 THEN 1 ELSE 0 END) as finish,
		SUM(CASE WHEN pbu.id IS NOT NULL THEN 1 ELSE 0 END) as total,
		SUM(CASE WHEN ps.main_menu = 'Finishing' THEN 1 ELSE 0 END) as total_pasca_kerja
		FROM procurement_boq_upload pbu
		LEFT JOIN procurement_step ps ON ps.id = pbu.step_id
		LEFT JOIN procurement_step ps2 ON ps2.id = ps.step_after
		LEFT JOIN procurement_mitra pm ON pbu.mitra_id = pm.id
		WHERE
		pbu.id != '' AND pbu.active = 1	$sql
		GROUP BY pbu.mitra_nm
		ORDER BY pbu.id ASC
		");

		return $fn;
	}

	public static function get_boq_data($id, $jns = 0)
	{
		switch ($jns) {
			case 0:
				$sql = DB::table('procurement_boq_upload As pbu')
				->leftjoin('procurement_step As ps', 'ps.id', '=', 'pbu.step_id')
				->leftjoin('1_2_employee As emp', 'emp.nik', '=', 'pbu.modified_by')
				->select('pbu.*', 'emp.nama', 'ps.urutan')
				->where([['pbu.id', $id]])
				->first();
			break;
			case 1:
				$sql = DB::table('procurement_boq_upload As pbu')
				->where([
					['mitra_id', $id],
					['pbu.witel', session('auth')->Witel_New]
				]);

				if(session('auth')->proc_level == 44)
				{
					$find_witel = DB::Table('promise_witel As w1')
					->leftjoin('promise_witel As w2', 'w2.Regional', '=', 'w1.Regional')
					->where('w1.Witel', session('auth')->Witel_New)
					->get();

					foreach($find_witel as $vw)
					{
						$all_witel[] = $vw->Witel;
					}

					$sql->whereIn('pbu.witel', $all_witel);
				}
				elseif(session('auth')->proc_level != 99)
				{
					$sql->where('pbu.witel', session('auth')->Witel_New);
				}

				$sql->get();
			break;
			case 2:
				$sql = DB::Table('procurement_boq_upload As pbu')->where([
					["active", 1],
					['pbu.witel', session('auth')->Witel_New]
				]);

				if(session('auth')->proc_level == 44)
				{
					$find_witel = DB::Table('promise_witel As w1')
					->leftjoin('promise_witel As w2', 'w2.Regional', '=', 'w1.Regional')
					->where('w1.Witel', session('auth')->Witel_New)
					->get();

					foreach($find_witel as $vw)
					{
						$all_witel[] = $vw->Witel;
					}

					$sql->whereIn('pbu.witel', $all_witel);
				}
				elseif(session('auth')->proc_level != 99)
				{
					$sql->where('pbu.witel', session('auth')->Witel_New);
				}

				$sql->get();
			break;
			case 3:
				$sql = DB::table('procurement_boq_upload As pbu')
				->leftjoin('procurement_step As ps', 'ps.id', '=', 'pbu.step_id')
				->select('pbu.*', 'ps.nama', 'ps.aktor_nama', 'ps.action', 'ps.urutan')
				->where([
					['pbu.id', $id],
				])
				->first();
			break;
		}

		return $sql;
	}

	// public static function list_budget_exceeded()
	// {
	// 	return DB::SELECT("SELECT pbu.judul, pbu.tgl_last_update, pbu.mitra_nm, pp.budget, pp.id as id_pp, pbu.id, pp.pid, pm.nama_company,
	// 	SUM(CASE WHEN prp.budget_up != 0 THEN prp.budget_up ELSE prp.budget END ) as used_budget,
	// 	COUNT(pbu.id) as jml_be
	// 	FROM procurement_boq_upload pbu
	// 	LEFT JOIN procurement_req_pid prp ON prp.id_upload = pbu.id
	// 	LEFT JOIN procurement_pid pp ON pp.id = prp.id_pid_req
	// 	LEFT JOIN procurement_mitra pm ON pm.id = pp.mitra_id
	// 	WHERE pp.budget_exceeded = 1
	// 	GROUP BY pp.id
	// 	ORDER BY pbu.tgl_last_update DESC");
	// }

	public static function get_pid_single_BE($id)
	{
		return DB::table('procurement_req_pid As prp')
    ->leftjoin('procurement_pid As pp', 'pp.id', 'prp.id_pid_req')
    ->select('pp.*')
    ->where([
			['prp.id_upload', $id],
			['pp.budget_exceeded', 1],
		])
    ->GroupBy('pp.id')
    ->first();
	}

	public static function detail_dashboard($jenis_table, $witel, $tgl_hidden, $tgl, $mitra, $pekerjaan, $tampilan, $tgl_progress_kerja, $jenis_period, $aktor)
	{
		$sql = '';
		if(!empty($witel) && strcasecmp($witel, 'All') != 0)
		{
			$sql .= "AND pbu.witel LIKE '%$witel%' ";
		}

		if($jenis_period)
		{
			if($jenis_period != 'all')
			{
				if($jenis_period == 'sp')
				{
					switch ($tgl_hidden) {
						case 'Tahun':
							$sql .=" AND pbu.created_at LIKE '$tgl%' ";
						break;
						case 'Custom':
							$tgl = explode(' - ', $tgl);
							$sql .=" AND DATE_FORMAT(pbu.tgl_sp, '%Y-%m-%d') BETWEEN '$tgl[0]' AND '$tgl[1]' ";
						break;
						case 'Bulan':
							$sql .=" AND MONTH(pbu.created_at) = $tgl";
						break;
					}
				}
				else
				{
					$tgl_progress_kerja = explode(' - ', $tgl_progress_kerja);
					$sql .=" AND DATE_FORMAT(pbu.tgl_last_update, '%Y-%m-%d') BETWEEN '$tgl_progress_kerja[0]' AND '$tgl_progress_kerja[1]' ";
				}
			}
		}

		if(session('auth')->proc_level == 2)
		{
			$sql .= " AND pbu.mitra_id = ".session('auth')->id_pm;
		}
		else
		{
			if(!empty($mitra) && strcasecmp($mitra, 'all') != 0)
			{
				$sql .= " AND pbu.mitra_id = $mitra";
			}
		}

		if(!empty($pekerjaan) && strcasecmp($pekerjaan, 'all') != 0)
		{
			$sql .= " AND pbu.pekerjaan = '$pekerjaan'";
		}

		switch ($aktor) {
			case 'user':
				$sql .= " AND ps2.aktor = 3";
			break;
			case 'mitra':
				$sql .= " AND ps2.aktor = 2";
			break;
			case 'proc_a':
				$sql .= " AND ps2.aktor = 1";
			break;
			case 'proc_r':
				$sql .= " AND ps2.aktor = 6";
			break;
			case 'finn':
				$sql .= " AND ps2.aktor = 8";
			break;
			case 'cashbank':
				$sql .= " AND ps2.aktor = 9";
			break;
			case 'operation_req_pid':
				$sql .= " AND pbu.step_id IN (1)";
			break;
			case 'commerce_create_pid':
				$sql .= " AND ps.step_after = 2";
			break;
			case 'operation_upload_justif':
				$sql .= " AND ps.step_after = 3";
			break;
			case 'proca_s_pen':
				$sql .= " AND ps.step_after = 4";
			break;
			case 'mitra_sanggup':
				$sql .= " AND ps.step_after = 5";
			break;
			case 'proca_sp':
				$sql .= " AND ps.step_after = 6";
			break;
			case 'mitra_boq_rek':
				$sql .= " AND ps.step_after = 7";
			break;
			case 'operation_verif_boq':
				$sql .= " AND ps.step_after = 8";
			break;
			case 'mitra_pelurusan_material':
				$sql .= " AND ps.step_after = 9";
			break;
			case 'commerce_budget':
				$sql .= " AND ps.step_after = 10";
			break;
			case 'proca_ver_doc_area':
				$sql .= " AND ps.step_after = 11";
			break;
			case 'mitra_lengkapi_nomor':
				$sql .= " AND ps.step_after = 12";
			break;
			case 'mitra_ttd':
				$sql .= " AND ps.step_after = 20";
			break;
			case 'procg_ver_doc_reg':
				$sql .= " AND ps.step_after = 13";
			break;
			case 'finance':
				$sql .= " AND ps.step_after = 22";
			break;
			case 'cb':
				$sql .= " AND ps.step_after = 23";
			break;
		}

		$data = DB::select("SELECT pbu.*, ps.nama as nama_step, pm.nama_company, pbu.judul as judul_work, pm.witel as witel_mitra, ps2.nama as nama_step, ps.nama as step_before, ps2.nama as step_now, ps3.nama as step_after, DATEDIFF(NOW(), pbu.tgl_last_update) as jml_hri, ps2.aktor_nama as aktor_nama
			FROM procurement_boq_upload pbu
			LEFT JOIN 1_2_employee emp ON emp.nik_amija = pbu.created_by
			LEFT JOIN procurement_mitra pm ON pbu.mitra_id = pm.id
			LEFT JOIN procurement_Boq_lokasi As pbl ON pbu.id = pbl.id_upload
			LEFT JOIN procurement_step ps ON ps.id = pbu.step_id
			LEFT JOIN procurement_step ps2 ON ps2.id = ps.step_after
			LEFT JOIN procurement_step ps3 ON ps3.id = ps2.step_after
			WHERE pbu.id IS NOT NULL AND pbu.active = 1 $sql
			GROUP BY pbu.id
			ORDER BY pekerjaan");

		return $data;
	}

	public static function get_proc_user($id, $jenis = 'id')
	{
		$sql = DB::Table('procurement_user')->where('active', 1);

		switch ($jenis) {
			case 'id':
				$sql->where('id', $id);
				$sql = $sql->first();
			break;
			case 'nik':
				$sql->where('nik', $id);
				$sql = $sql->first();
			break;
			case 'jabatan':
				$sql->where('jabatan', 'like', '%'. $id . '%');
				$sql = $sql->first();
			break;
			case 'job':
				$sql->where([
					['pekerjaan', $id],
					['witel', session('auth')->Witel_New]
					// ['status', $status]
				]);
				$sql = $sql->get();
			break;
			default:
				if(session('auth')->peker_pup)
				{
					$pekerjaan = explode(', ', session('auth')->peker_pup );
					$sql->whereIn('pekerjaan', [$pekerjaan, 'ALL']);
				}
				$sql = $sql->get();
			break;
		}
		return $sql;
	}

	public static function get_dokumen_id($rule_dok, $pekerjaan, $witel, $tgl = null)
	{
		$sql = DB::table('procurement_dok_ttd As pdt')
		->leftjoin('procurement_user As ps', 'pdt.id_user', '=', 'ps.id')
		->where([
			['pdt.pekerjaan', $pekerjaan],
			['rule_dok', $rule_dok],
			['pdt.witel', $witel]
		]);

		if($tgl == 'latest')
		{
			$sql->OrderBy('pdt.id', 'DESC');
		}
		else
		{
			$tgl_count = explode('-', $tgl);

			if(count($tgl_count) == 3)
			{
				$sql->where([
					['pdt.tgl_start', '<=', $tgl],
					['pdt.tgl_end', '>=', $tgl]
				]);
			}
			else
			{
				$sql->whereRaw("DATE_FORMAT(pdt.tgl_start, '%Y-%m') <= '$tgl' AND DATE_FORMAT(pdt.tgl_end, '%Y-%m') >= '$tgl'");
			}
		}

		$data = $sql->first();

		if(!$data)
		{
			$data = new \stdClass();
			$data->user = 'NAMA USER';
			$data->jabatan = 'JABATAN USER';
			$data->nik = 'NIK USER';
			$data->reg = 0;

		}

		return $data;
	}

	// public static function save_NI($req)
	// {
	// 	DB::transaction(function () use ($req)
	// 	{
	// 		$id_mitra = session('auth')->id_pm;

	// 		DB::table('procurement_design_NI')->where([
	// 			['id_mitra', $id_mitra]
	// 		])->delete();

	// 		foreach($req->material as $val)
	// 		{
	// 			DB::table('procurement_design_NI')->insert([
	// 				'id_mitra' => $id_mitra,
	// 				'id_design' => $val,
	// 				'created_by' => session('auth')->id_user
	// 			]);
	// 		}
	// 	});
	// }

	public static function update_pekerjaan($req, $id)
	{
		$data = AdminModel::get_data_final($id);
		$bulan = array (
			'01' =>'Januari',
			'02' =>'Februari',
			'03' =>'Maret',
			'04' =>'April',
			'05' =>'Mei',
			'06' =>'Juni',
			'07' =>'Juli',
			'08' =>'Agustus',
			'09' =>'September',
			'10' =>'Oktober',
			'11' =>'November',
			'12' =>'Desember'
		);

		if($data->step_id <= 5)
    {
			$sql_ins = [];

			if(in_array(session('auth')->proc_level, [1, 4, 99, 44]) )
			{
				if($data->step_id >= 3)
				{
					if($req->s_pen)
					{
						$split_tgl = explode('-', $req->tgl_s_pen);

						$str = preg_split("/[\/.-]/", $req->s_pen);
						$split_spen = array_reverse($str);

						if($split_spen[1] .'-'. $split_spen[0] != $split_tgl[1].'-'.$split_tgl[0])
						{
							return redirect('/home')->with('alerts', [
								['type' => 'danger', 'text' => 'Kode Surat Penetapan Tidak Sesuai!']
							]);
						}
					}

					DB::table('procurement_boq_upload')->where('id', $id)->update([
						'pks'             => $req->pks,
						'tgl_pks'         => $req->tgl_pks,
						'surat_penetapan' => $req->s_pen,
						'tgl_jatuh_tmp'   => $req->toc,
						'lokasi'          => $req->lokasi_project,
						'tgl_s_pen'       => $req->tgl_s_pen,
						'modified_by'     => session('auth')->id_user,
					]);

					$xp = explode('-', $req->tgl_pks);

					DB::Table('procurement_mitra')
					->where('id', $data->mitra_id)
					->update([
						'no_khs_maintenance'  => $req->pks,
						'tgl_khs_maintenance' => $xp[2] . ' '. $bulan[$xp[1] ] . ' ' . $xp[0]
					]);
				}

				if($data->step_id == 5)
				{
					if ($req->no_sp) {
						$split_tgl = explode('-', $req->tgl_sp);

						$str = preg_split("/[\/.-]/", $req->no_sp);
						$split_sp = array_reverse($str);

						if($split_sp[1] .'-'. $split_sp[0] != $split_tgl[1].'-'.$split_tgl[0])
						{
							return back()->with('alerts', [
								['type' => 'danger', 'text' => 'Kode Surat Pesanan Tidak Sesuai!']
							]);
						}
					}

					if ($req->pks) {
						$split_tgl = explode('-', $req->tgl_pks);
						$str       = preg_split("/[\/.-]/", $req->pks);
						$split_pks  = array_reverse($str);

						if($split_pks[1] .'-'. $split_pks[0] != $split_tgl[1].'-'.$split_tgl[0])
						{
							return back()->with('alerts', [
								['type' => 'danger', 'text' => 'Kode PKS Tidak Sesuai!']
							]);
						}
					}

					$xp = explode('-', $req->tgl_pks);

					$data_j = ReportModel::get_boq_data($id);

					DB::Table('procurement_mitra')
					->where('id', $data_j->mitra_id)
					->update([
						'no_khs_maintenance' 	=> $req->pks,
						'tgl_khs_maintenance' => $xp[2] . ' '. $bulan[$xp[1] ] . ' ' . $xp[0]
					]);

					DB::table('procurement_boq_upload')->where('id', $id)->update([
						'tgl_jatuh_tmp'     => $req->toc,
						'no_sp'             => $req->no_sp,
						'tgl_sp'            => $req->tgl_sp,
						'total_sp'          => $data_j->total_material_sp + $data_j->total_jasa_sp,
						'pks'               => $req->pks,
						'tgl_pks'           => $req->tgl_pks,
						'surat_penetapan'   => $req->s_pen,
						'tgl_s_pen'         => $req->tgl_s_pen,
						'surat_kesanggupan' => $req->no_kesanggupan_khs,
						'tgl_surat_sanggup' => $req->tgl_surat_sanggup,
						'modified_by'       => session('auth')->id_user,
					]);
				}
			}

			if(in_array(session('auth')->proc_level, [2, 3]) && $data->step_id >= 4 || in_array(session('auth')->proc_level, [4, 99, 44]) )
			{
				if($req->surat_kesanggupan)
				{
					$array_bln = array(1 => "I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
					$split_tgl = explode('-', $req->tgl_sggp);
					$str       = preg_split("/[\/.-]/", $req->surat_kesanggupan);
					$chck_sk   = array_reverse($str);

					if(preg_match("/[A-Za-z]/", $chck_sk[1]) )
					{
						$tgl_chck = $array_bln[(int)$split_tgl[1] ].'/'.$split_tgl[0];
						$nomor_s  = $chck_sk[1].'/'.$chck_sk[0];
					}
					else {
						$tgl_chck = $split_tgl[1] .'/'.$split_tgl[0];
						$nomor_s  = sprintf('%02d', $chck_sk[1]).'/'.$chck_sk[0];
					}

					if($tgl_chck != $nomor_s)
					{
						return back()->with('alerts', [
							['type' => 'danger', 'text' => 'Kode Kesanggupan Tidak Sesuai!']
						]);
					}
				}

				if($req->s_pen)
				{
					$split_tgl  = explode('-', $req->tgl_s_pen);
					$str        = preg_split("/[\/.-]/", $req->s_pen);
					$split_spen = array_reverse($str);

					if($split_spen[1] .'-'. $split_spen[0] != $split_tgl[1].'-'.$split_tgl[0])
					{
						return back()->with('alerts', [
							['type' => 'danger', 'text' => 'Kode Surat Penetapan Tidak Sesuai!']
						]);
					}
				}

				$xp = explode('-', $req->tgl_pks);

				DB::Table('procurement_mitra')
				->where('id', $data->mitra_id)
				->update([
					'no_khs_maintenance'  => $req->pks,
					'tgl_khs_maintenance' => $xp[2] . ' '. $bulan[$xp[1] ] . ' ' . $xp[0]
				]);

				$sql_ins['surat_kesanggupan'] = $req->surat_kesanggupan;
				$sql_ins['pks'] = $req->pks;
				$sql_ins['tgl_pks'] = $req->tgl_pks;
				$sql_ins['tgl_surat_sanggup'] = $req->tgl_sggp;
				$sql_ins['modified_by'] = session('auth')->id_user;
				// dd($sql_ins, $id);
				DB::table('procurement_boq_upload')->where('id', $id)->update($sql_ins);
			}

			return back()->with('alerts', [
				['type' => 'success', 'text' => 'Data Pra-Kerja Berhasil Disimpan!']
			]);
		}
		else
		{
			$sql_ins = [];

			if(session('auth')->proc_level == 1 && $data->step_id >= 10 || in_array(session('auth')->proc_level, [4, 99, 44]) )
			{
				if($req->amd_sp)
        {
          $split_tgl    = explode('-', $req->tgl_amd_sp);
          $str          = preg_split("/[\/.-]/", $req->amd_sp);
          $split_amd_sp = array_reverse($str);

          if($split_amd_sp[1].'/'.$split_amd_sp[0] != $split_tgl[1].'/'.$split_tgl[0])
          {
            return back()->with('alerts', [
              ['type' => 'danger', 'text' => 'Kode Amandemen SP Tidak Sesuai!']
            ]);
          }
        }

        if($req->amd_pks)
        {
          $split_tgl     = explode('-', $req->tgl_amd_pks);
          $str           = preg_split("/[\/.-]/", $req->amd_pks);
          $split_amd_pks = array_reverse($str);

          if($split_amd_pks[1].'/'.$split_amd_pks[0] != $split_tgl[1].'/'.$split_tgl[0])
          {
            return back()->with('alerts', [
              ['type' => 'danger', 'text' => 'Kode Amandemen PKS Tidak Sesuai!']
            ]);
          }
        }

				if($req->nomor_sp)
        {
          $split_tgl = explode('-', $req->tgl_sp);
          $str       = preg_split("/[\/.-]/", $req->nomor_sp);
          $split_sp  = array_reverse($str);

          if($split_sp[1].'/'.$split_sp[0] != $split_tgl[1].'/'.$split_tgl[0])
          {
            return back()->with('alerts', [
              ['type' => 'danger', 'text' => 'Kode Nomor SP Tidak Sesuai!']
            ]);
          }
        }

        if($req->nomor_pks)
        {
          $split_tgl = explode('-', $req->tgl_pks);
          $str       = preg_split("/[\/.-]/", $req->nomor_pks);
          $split_pks = array_reverse($str);

          if($split_pks[1].'/'.$split_pks[0] != $split_tgl[1].'/'.$split_tgl[0])
          {
            return back()->with('alerts', [
              ['type' => 'danger', 'text' => 'Kode PKS Tidak Sesuai!']
            ]);
          }
        }

        if($req->baut)
        {
          $split_tgl  = explode('-', $req->tgl_baut);
          $str        = preg_split("/[\/.-]/", $req->baut);
          $split_baut = array_reverse($str);

          if($split_baut[1] .'-'. $split_baut[0] != $split_tgl[1].'-'.$split_tgl[0])
          {
            return back()->with('alerts', [
              ['type' => 'danger', 'text' => 'Kode BAUT Tidak Sesuai!']
            ]);
          }
        }

        if($req->bast)
        {
          $split_tgl  = explode('-', $req->tgl_bast);
          $str        = preg_split("/[\/.-]/", $req->bast);
          $split_bast = array_reverse($str);

          if($split_bast[1] .'-'. $split_bast[0] != $split_tgl[1].'-'.$split_tgl[0])
          {
            return back()->with('alerts', [
              ['type' => 'danger', 'text' => 'Kode BAST Tidak Sesuai!']
            ]);
          }
        }

        if($req->ba_abd)
        {
          $split_tgl    = explode('-', $req->tgl_ba_abd);
          $str          = preg_split("/[\/.-]/", $req->ba_abd);
          $split_ba_abd = array_reverse($str);

          if($split_ba_abd[1] .'-'. $split_ba_abd[0] != $split_tgl[1].'-'.$split_tgl[0])
          {
            return back()->with('alerts', [
              ['type' => 'danger', 'text' => 'Kode BA ABD Tidak Sesuai!']
            ]);
          }
        }

        if($req->ba_rekon)
        {
          $split_tgl      = explode('-', $req->tgl_ba_rekon);
          $str            = preg_split("/[\/.-]/", $req->ba_rekon);
          $split_ba_rekon = array_reverse($str);

          if($split_ba_rekon[1] .'-'. $split_ba_rekon[0] != $split_tgl[1].'-'.$split_tgl[0])
          {
            return back()->with('alerts', [
              ['type' => 'danger', 'text' => 'Kode BA REKON Tidak Sesuai!']
            ]);
          }
        }

        if($req->bapp)
        {
          $split_tgl  = explode('-', $req->tgl_bapp);
          $str        = preg_split("/[\/.-]/", $req->bapp);
          $split_bapp = array_reverse($str);

          if($split_bapp[1] .'-'. $split_bapp[0] != $split_tgl[1].'-'.$split_tgl[0])
          {
            return back()->with('alerts', [
              ['type' => 'danger', 'text' => 'Kode BAPP Tidak Sesuai!']
            ]);
          }
        }

				$detail       = $req->detail;
				$modified_by  = session('auth')->id_user;

				DB::SELECT(
					"UPDATE procurement_boq_upload pbu
					SET pbu.detail = '$detail', pbu.modified_by = '$modified_by'
					WHERE pbu.id = $id "
				);

				$sql_ins['BAUT'] = $req->baut;
				$sql_ins['tgl_baut'] =  $req->tgl_baut;

				if($req->amd_pks)
				{
					$sql_ins['amd_pks']     = $req->amd_pks;
					$sql_ins['tgl_amd_pks'] = $req->tgl_amd_pks;
				}

				if($req->amd_sp)
				{
					$sql_ins['amd_sp']     = $req->amd_sp;
					$sql_ins['tgl_amd_sp'] = $req->tgl_amd_sp;
				}

				$sql_ins['BAST']         = $req->bast;
				$sql_ins['pks']          = $req->nomor_pks;
				$sql_ins['tgl_pks']      = $req->tgl_pks;
				$sql_ins['no_sp']        = $req->nomor_sp;
				$sql_ins['tgl_sp']       = $req->tgl_sp;
				$sql_ins['tgl_bast']     = $req->tgl_bast;
				$sql_ins['ba_abd']       = $req->ba_abd;
				$sql_ins['tgl_ba_abd']   = $req->tgl_ba_abd;
				$sql_ins['ba_rekon']     = $req->ba_rekon;
				$sql_ins['tgl_ba_rekon'] = $req->tgl_ba_rekon;
				$sql_ins['BAPP']         = $req->bapp;
				$sql_ins['tgl_bapp']     = $req->tgl_bapp;
				$sql_ins['modified_by']  = session('auth')->id_user;

				DB::table('procurement_boq_upload')->where('id', $id)->update($sql_ins);
			}

			if(in_array(session('auth')->proc_level, [2, 3]) && $data->step_id >= 11 || in_array(session('auth')->proc_level, [4, 99, 44]) )
			{
				$array_bln = array(1 => "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X", "XI", "XII");
				$split_tgl = explode('-', $req->tgl_faktur);

				if ($req->spp_num) {
					$str      = preg_split("/[\/.-]/", $req->spp_num);
					$chck_spp = array_reverse($str);

					if(strlen($chck_spp[0]) != 4)
					{
						$year = date('y', strtotime($req->tgl_faktur) );
					}
					else
					{
						$year = $split_tgl[0];
					}

					$tgl_im = $array_bln[(int)$split_tgl[1]] . '/' . $year;

					if (!isset($chck_spp[1]) || $tgl_im != $chck_spp[1] . '/' . $chck_spp[0]) {
						return back()->with('alerts', [
							['type' => 'danger', 'text' => 'Kode SPP Tidak Sesuai!']
						])->withInput();
					}
				}

				if ($req->receipt_num) {
					$str          = preg_split("/[\/.-]/", $req->receipt_num);
					$chck_receipt = array_reverse($str);

					if(strlen($chck_receipt[0]) != 4)
					{
						$year = date('y', strtotime($req->tgl_faktur) );
					}
					else
					{
						$year = $split_tgl[0];
					}

					$tgl_im = $array_bln[(int)$split_tgl[1]] . '/' . $year;

					if (!isset($chck_receipt[1]) || $tgl_im != $chck_receipt[1] . '/' . $chck_receipt[0]) {
						return back()->with('alerts', [
							['type' => 'danger', 'text' => 'Kode Kwitansi Tidak Sesuai!']
						])->withInput();
					}
				}

				if ($req->no_invoice) {
					$str          = preg_split("/[\/.-]/", $req->no_invoice);
					$chck_invoice = array_reverse($str);

					if(strlen($chck_invoice[0]) != 4)
					{
						$year = date('y', strtotime($req->tgl_faktur) );
					}
					else
					{
						$year = $split_tgl[0];
					}

					$tgl_im = $array_bln[(int)$split_tgl[1]] . '/' . $year;

					if (!isset($chck_invoice[1]) || $tgl_im != $chck_invoice[1] . '/' . $chck_invoice[0]) {
						return back()->with('alerts', [
							['type' => 'danger', 'text' => 'Kode Invoice Tidak Sesuai!']
						])->withInput();
					}
				}

				// if ($req->no_faktur) {
				// 	$str         = preg_split("/[\/.-]/", $req->no_faktur);
				// 	$chck_faktur = array_reverse($str);

				// 	if (!isset($chck_faktur[1]) || $tgl_im != $chck_faktur[1] . '/' . $chck_faktur[0]) {
				// 		return back()->with('alerts', [
				// 			['type' => 'danger', 'text' => 'Kode Faktur Tidak Sesuai!']
				// 		])->withInput();
				// 	}
				// }

				$no_faktur   = $req->no_faktur;
				$spp_num     = $req->spp_num;
				$receipt_num = $req->receipt_num;
				$no_invoice  = $req->no_invoice;

				if($req->chck_rmv_invoice)
				{
					$no_invoice = preg_replace("/[\/.-]/", '', $req->no_invoice);
				}

				if($req->chck_rmv_spp)
				{
					$spp_num = preg_replace("/[\/.-]/", '', $req->spp_num);
				}

				if($req->chck_rmv_kwitansi)
				{
					$receipt_num = preg_replace("/[\/.-]/", '', $req->receipt_num);
				}

				if($req->chck_rmv_faktur)
				{
					$no_faktur = preg_replace("/[\/.-]/", '', $req->no_faktur);
				}

				DB::table('procurement_boq_upload')->where('id', $id)->update([
					'tgl_faktur'  => $req->tgl_faktur,
					'faktur'      => $no_faktur,
					'spp_num'     => $spp_num,
					'receipt_num' => $receipt_num,
					'invoice'     => $no_invoice,
					'modified_by' => session('auth')->id_user
				]);
			}

			return back()->with('alerts', [
				['type' => 'success', 'text' => 'Data Pra-Kerja Berhasil Disimpan!']
			]);
		}
	}

	// public static function update_or_insert_ttd_dok($req, $jenis)
	// {
	// 	$input = $req->input();
	// 	unset($input['_token']);
	// 	unset($input['pekerjaan']);

	// 	$jenisP['psb']    = 'PSB';
  //   $jenisP['opt']  = 'QE';
  //   $jenisP['cons'] = 'Construction';

	// 	foreach($input as $k => $v)
	// 	{
	// 		if($v)
	// 		{
	// 			$check = DB::table('procurement_dok_ttd')->where([
	// 				['pekerjaan', $jenisP[$jenis] ],
	// 				['rule_dok', $k],
	// 				['witel', $req->witel]
	// 			])->first();

	// 			if($check)
	// 			{
	// 				DB::table('procurement_dok_ttd')->where([
	// 					['pekerjaan', $jenisP[$jenis] ],
	// 					['rule_dok', $k],
	// 					['witel', $req->witel]
	// 				])->update([
	// 					'id_user'     => $v,
	// 					'modified_by' => session('auth')->id_user
	// 				]);
	// 			}
	// 			else
	// 			{
	// 				DB::table('procurement_dok_ttd')->insert([
	// 					'pekerjaan'  => $jenisP[$jenis],
	// 					'rule_dok'   => $k,
	// 					'witel'      => $req->witel,
	// 					'id_user'    => $v,
	// 					'created_by' => session('auth')->id_user
	// 				]);
	// 			}
	// 		}
	// 	}
	// }

	public static function get_pid_budget($jns)
	{
		$sql = '';

		if($jns == 1)
		{
			$sql = 'pbu.step_id IN(26, 28, 32) AND';
		}
		return DB::SELECT("SELECT pp.*, pbu.id as id_pbu, pbu.judul, pbu.total_material_sp, pbu.total_jasa_sp, pbu.total_material_rekon, pbu.total_jasa_rekon,
			SUM(CASE WHEN prp.budget_up != 0 THEN prp.budget_up ELSE prp.budget END ) As budget_up
			 FROM procurement_pid As pp
			LEFT JOIN procurement_req_pid As prp ON prp.id_pid_req =  pp.id
			LEFT JOIN procurement_boq_upload As pbu ON pbu.id = prp.id_upload
			WHERE $sql pbu.active = 1 AND pbu.step_id != 14 AND budget_exceeded = $jns
			GROUP BY pp.id
		");
	}

	public static function get_apm_budget_all($search = null)
	{
		$data_sq = DB::Table('budget_apm');

		if($search)
		{
			$data_sq->where('wbs', 'like', '%'.$search.'%');
		}

		$data = $data_sq->get();
		$rd = [];

		foreach($data as $d)
		{
			$rd[$d->pid]['pid'] = $d->pid;
			$rd[$d->pid]['wbs'] = $d->wbs;
			$rd[$d->pid]['keperluan'][$d->keperluan] = $d->value;
		}

		if(!$rd)
		{
			$data_sq = DB::Table('proaktif_project');

			if($search)
			{
				$data_sq->where('sapid', 'like', '%'.$search.'%');
			}

			$data = $data_sq->get();
			$rd = [];

			foreach($data as $d)
			{
				$rd[$d->pid]['pid'] = $d->pid;
				$rd[$d->pid]['wbs'] = $d->sapid;
				$rd[$d->pid]['keperluan']['Material : Non Stock'] = 0;
				$rd[$d->pid]['keperluan']['Material : Stock'] = 0;
				$rd[$d->pid]['keperluan']['Non Material : Jasa'] = 0;
				$rd[$d->pid]['keperluan']['Non Material : Sewa'] = 0;
				$rd[$d->pid]['keperluan']['Non Material : Tenaga Kerja'] = 0;
				$rd[$d->pid]['keperluan']['Non Materiall : Operasional'] = 0;
			}
		}

		return array_values($rd);
	}

	public static function get_pid()
	{
		$data = DB::SELECT("SELECT pp.*, pbu.judul, pbu.total_material_rekon, pbu.total_jasa_rekon,
			SUM(CASE WHEN prp.budget_up != 0 THEN prp.budget_up ELSE prp.budget END ) as used_budget,
			SUM(CASE WHEN pbu.total_sp IS NULL THEN pbu.total_material_sp + pbu.total_jasa_sp ELSE pbu.total_sp END) As total_sp,
			0 As flag_budget_apm
			FROM procurement_pid As pp
			LEFT JOIN procurement_req_pid As prp ON prp.id_pid_req = pp.id
			LEFT JOIN procurement_boq_upload pbu ON pbu.id = prp.id_upload
			WHERE pbu.judul IS NOT NULL AND pbu.active = 1
			GROUP BY pbu.id, pp.id
			ORDER BY used_budget DESC"
		);

		$unused_budget_apm = $used_budget_apm = $used_promise = $unknown_pid = [];

		$ba = self::get_apm_budget_all();

    foreach($ba as $k => $v)
    {
      $find_k  = array_search($v['wbs'], array_column(json_decode(json_encode($data, TRUE) ), 'pid') );

      if($find_k === FALSE)
			{
				$unused_budget_apm[] = $v;
			}

      $find_k_m  = array_keys(array_column(json_decode(json_encode($data, TRUE) ), 'pid'), $v['wbs']);

      foreach($find_k_m as $kk => $vv)
			{
				$data_pbu = $data[$vv];
				$used_budget_apm[$data_pbu->pid] = $v;
				$used_budget_apm[$data_pbu->pid]['pekerjaan'][$kk]['judul_pbu']   = $data_pbu->judul;
				$used_budget_apm[$data_pbu->pid]['pekerjaan'][$kk]['total_sp']    = $data_pbu->total_sp;
				$used_budget_apm[$data_pbu->pid]['pekerjaan'][$kk]['total_rekon'] = $data_pbu->total_material_rekon + $data_pbu->total_jasa_rekon;
			}
    }

    foreach($data as $k => $v)
    {
      $find_k  = array_search($v->pid, array_column($ba, 'wbs') );

      if($find_k !== FALSE)
			{
				$used_promise[$v->pid]['pid'] = $v->pid;
				$used_promise[$v->pid]['data'][] = $v;
			}
			else
			{
				$unknown_pid[$v->pid]['pid'] = $v->pid;
				$unknown_pid[$v->pid]['data'][] = $v;
			}
    }
		return ['unused_budget_apm' => array_values($unused_budget_apm), 'used_budget_apm' => array_values($used_budget_apm), 'used_promise' => array_values($used_promise), 'unknown_pid' => array_values($unknown_pid)];
	}

	public static function get_pid_single($id)
	{
		return DB::Table('procurement_pid As pp')
		->leftjoin('procurement_mitra As pm', 'pp.mitra_id', '=', 'pm.id')
		->select('pm.nama_company', 'pm.id As id_company', 'pp.*')
		->where('pp.id', $id)
		->first();
	}

	public static function history_be($id)
	{
		return DB::table('procurement_pid As pp')
		->leftjoin('procurement_req_pid As prp', 'pp.id', '=', 'prp.id_pid_req')
		->leftjoin('procurement_boq_upload As pbu', 'pbu.id', '=', 'prp.id_upload')
		->select('pbu.*')
		->where([
			['pp.id', $id],
			['pbu.active', 1],
			['pbu.step_id', '!=', 14]
		])
		->whereIn('pbu.step_id', [26, 28, 32])
		->GroupBy('pbu.id')
		->get();
	}

	public static function list_pekerjaan_pid($id, $jenis)
	{
		$sql = DB::table('procurement_pid As pp')
		->leftjoin('procurement_req_pid As prp', 'pp.id', '=', 'prp.id_pid_req')
		->leftjoin('procurement_boq_upload As pbu', 'pbu.id', '=', 'prp.id_upload')
		->select('pbu.*', DB::RAW("(CASE WHEN pbu.step_id IN (26, 28, 32) THEN 'Budget Exceeded' ELSE 'Normal' END) As status_budget"))
		->where([
			['pp.id', $id],
			['pbu.active', 1],
			['pbu.step_id', '!=', 14]
		])
		->GroupBy('pbu.id');

		if($jenis == 'be')
		{
			$sql->whereIn('pbu.step_id', [26, 28, 32]);
		}

		return $sql->get();
	}

	public static function find_pid($id, $jenis = 'id')
	{
		switch ($jenis) {
			case 'id':
				$sql = DB::table('procurement_pid')->where('id', $id);
			break;
			default:
				$sql = DB::table('procurement_pid')->where('pid', trim($id) );
			break;
		}
		return $sql->first();
	}

	public static function update_main_pid($req, $id)
	{
		$msg['direct'] = $req->prev_url;

		if($req->status_pid == 'BE')
		{
			DB::table('procurement_pid')->where('id', $id)->update([
				'pid'               => $req->pid,
				'keterangan'        => $req->desc,
				'budget_exceeded'   => 1,
				'be_material_mitra' => 1,
				'be_material_ta'    => 1,
				'be_jasa'           => 1,
				'modified_by'       => session('auth')->id_user
			]);

			$msg['isi']['msg'] = [
				'type' => 'success',
				'text' => "PID $req->pid Telah Berubah Menjadi Overbudget!"
			];
		}
		else
		{
			// $history_be = self::history_be($id);
			// $get_current = self::find_pid($id);
			// if($get_current->budget_exceeded == 1)
			// {
			// 	$jumlah = 0;
			// 	foreach($history_be as $valc1)
			// 	{
			// 		$total_sp      = $valc1->total_material_sp + $valc1->total_jasa_sp;
			// 		$total_rekon   = $valc1->total_material_rekon + $valc1->total_jasa_rekon;
			// 		$total_kerjaan = ($total_rekon == 0 ? $total_sp: $total_rekon);
			// 		$jumlah       +=  $total_kerjaan;
			// 	}
			// 	if($jumlah > abs(str_replace('.', '', $req->nominal_harga) + ($get_current->budget < 0 ? 0 : $get_current->budget)) )
			// 	{
			// 		$sisa = $jumlah - abs(str_replace('.', '', $req->nominal_harga) + ($get_current->budget < 0 ? 0 : $get_current->budget));

			// 		$msg['direct'] = '/Report/pid/edit_data/'.$id;
			// 		$msg['isi']['msg'] = [
			// 			'type' => 'warning',
			// 			'text' => "Budget yang diinput kurang ". number_format($sisa, 0, '.', '.')
			// 		];
			// 		return $msg;
			// 	}
			// }

			DB::table('procurement_pid')->where('id', $id)->update([
				'pid'               => $req->pid,
				'keterangan'        => $req->desc,
				'budget_exceeded'   => 0,
				'be_material_mitra' => 0,
				'be_material_ta'    => 0,
				'be_jasa'           => 0,
				// 'budget'         => abs(str_replace('.', '', $req->nominal_harga) + $get_current->budget),
				'modified_by'       => session('auth')->id_user,
			]);

			$botToken = '1902669622:AAHz0cl18e8-IF5waY1uQIj33Nxqq72EzFc';
			$website="https://api.telegram.org/bot".$botToken;

			$check_data = ReportModel::history_be($id);

			if($check_data)
			{
				$pesan = 'Budget Untuk PID ' .$req->pid. " Sudah Ada, Harap Cek kembali pekerjaan Budget Exceeded dibawah ini! \n";

				foreach($check_data as $val)
				{
					$check_step = DB::SELECT("SELECT ps.aktor, pbu.step_id, ps.urutan, ps.action
					FROM procurement_boq_upload pbu
					LEFT JOIN procurement_step ps ON ps.id = pbu.step_id
					WHERE pbu.id = $val->id ORDER BY ps.urutan DESC");

					$urutan = (int)round($check_step[0]->urutan, 0, PHP_ROUND_HALF_DOWN);

					$get_step2 = DB::Table('procurement_step As ps')
					->leftjoin('procurement_step As ps2', 'ps2.step_after' , '=', 'ps.id')
					->select('ps2.id', 'ps2.nama', 'ps.urutan')
					->where('ps.urutan', $urutan)
					->first();

					$step_id = $get_step2->id;
					$loker   = $get_step2->nama;

					$check_rekon = DB::Table('procurement_step As ps')
					->leftjoin('procurement_rekon_log As prl', 'ps.id', '=', 'prl.step_id')
					->select('prl.*', 'ps.urutan', 'ps.nama')
					->where([
						['prl.id_upload', $val->id],
						['prl.status_step', 'forward']
					])
					->orderBy('prl.id', 'DESC')
					->first();

					if($check_rekon && $check_rekon->urutan > $get_step2->urutan)
					{
						$step_id = $check_rekon->step_id;
						$loker   = $check_rekon->nama;
					}
					// dd($step_id, $check_step, $val->id, $get_step2, $check_rekon, $loker);
					DB::table('procurement_boq_upload')->where('id', $val->id)->update([
						'step_id' => $step_id,
						'modified_by' => session('auth')->id_user,
					]);

					$get = AdminModel::get_step_by_id($step_id);

					$check_log = 	DB::table('procurement_rekon_log')->where('id_upload', $val->id)->get();

					if(count($check_log) == 0 )
					{
						$created_at = date('Y-m-d H:i:s');
						$closed_at  = date('Y-m-d H:i:s');
					}
					else
					{
						$get_last_data = DB::table('procurement_rekon_log')
						->where('id_upload', $val->id)
						->OrderBy('id', 'DESC')
						->limit(1)
						->first();

						$created_at = $get_last_data->closed_at;
						$closed_at  = date('Y-m-d H:i:s');
					}

					DB::table('procurement_rekon_log')->Insert([
						'id_upload'  => $val->id,
						'step_id'    => $get->id,
						'aktor'      => $get->aktor,
						'keterangan' => $get->nama,
						'detail'     => 'Update PID',
						'created_at' => $created_at,
						'closed_at'  => $closed_at,
						'created_by' => session('auth')->id_user
					]);

					$pesan .= 'Mitra :<b>'.preg_replace('/^PT/', 'PT.',$val->mitra_nm) . "</b>\n";
					$pesan .= 'Judul :'. $val->judul . "\n";
					$pesan .= 'Loker :'. $loker . "\n";
				}

				$check_mitra = DB::table('procurement_pid')->where('id', $id)->first();

				$ci = '-1001727682139';

				if($check_mitra->mitra_id == 22)
				{
					$ci = '519446576';
				}

				$params=[
					// 'chat_id' => '-1001727682139' atau 519446576
					'chat_id' => $ci,
					'parse_mode' => 'html',
					'text' => "$pesan",
				];

				$ch = curl_init();
				$optArray = array(
					CURLOPT_URL            => $website.'/sendMessage',
					CURLOPT_RETURNTRANSFER => 1,
					CURLOPT_POST           => 1,
					CURLOPT_POSTFIELDS     => $params,
				);
				curl_setopt_array($ch, $optArray);
				$result = curl_exec($ch);
				curl_close($ch);
			}

			$msg['isi']['msg'] = [
				'type' => 'success',
				'text' => "PID $req->pid Berhasil Dirubah!"
			];
		}
		return $msg;
	}

	public static function check_judul($req)
	{

		$check_judul = DB::Table('procurement_boq_upload As pbu')
			->leftjoin('procurement_step As ps', 'ps.id', '=', 'pbu.step_id')
			->leftjoin('procurement_step As ps2', 'ps2.id', '=', 'ps.step_after')
			->select('ps2.nama', 'ps2.aktor_nama')
			->where([
				['judul', trim($req->nama_judul)],
				['pbu.active', 1]
			]);

		if($req->id)
		{
			$check_judul->Where('pbu.id', '!=', $req->id);
		}

		$check_judul = $check_judul->first();

		$msg = ['Pekerjaan Bisa DiSubmit!', 'green'];

		if($check_judul)
		{
			$msg = ['Judul Sudah Pernah Dibuat! Loker Terakhir Adalah '.$check_judul->nama.' ('.$check_judul->aktor_nama.')', 'red'];
		}

		return $msg;
	}

	public static function update_be($id, $req)
	{
		$data = AdminModel::get_data_final($id);

		DB::table('procurement_boq_upload')->where('id', $id)->update([
      'surat_penetapan' => $req->spen,
      'no_sp' => $req->sp,
      'budget_exceeded' => 0,
      'modified_by' => session('auth')->id_user
    ]);

		return $data;
	}

	public static function save_material_manual($req, $id)
	{
		$tgl_start = $tgl_end = null;

    if($req->tgl_durasi == 'yes')
    {
      if($req->bulan_start)
      {
        $tgl_start = $req->tahun_start .'-'. $req->bulan_start .'-01';
      }

      if($req->bulan_end)
      {
        $tgl_end = $req->tahun_end .'-'. $req->bulan_end . '-' . date('t', strtotime(date($req->tahun_end .'-'. $req->bulan_end, strtotime('last day of this month') ) ) );
      }
    }

		$string = htmlentities(trim($req->uraian), ENT_QUOTES, 'UTF-8', false);
		$strng = htmlentities($string, ENT_QUOTES, 'UTF-8', false);

    $data_material = [
      'jenis'           => $req->pekerjaan,
      'design_mitra_id' => $req->mitra,
      'designator'      => trim($req->designator),
      'pd_refer'        => $req->relasi_mat ?? 0,
      'uraian'          => $strng,
      'tgl_start'       => $tgl_start,
      'tgl_end'         => $tgl_end,
      'satuan'          => $req->satuan,
      'material'        => str_replace('.', '', $req->material),
      'jasa'            => str_replace('.', '', $req->jasa),
    ];

    if(!preg_match("/^[0-9]+$/", $id))
    {
      $check_used =  DB::table('procurement_designator')->where([
				['uraian', $strng],
				['designator', preg_replace('/\s+/', '', $req->designator )],
				['active', 1],
				['witel', session('auth')->Witel_New]
			])->first();

      if($check_used)
      {
        return back()->with('alerts', [
          ['type' => 'danger', 'text' => 'Material '.trim($req->designator).' Sudah Ada!']
        ]);
      }

      $data_material['witel'] = session('auth')->Witel_New;
      $data_material['created_by'] = session('auth')->id_user;

      DB::table('procurement_designator')->insert($data_material);

      $msg = $req->designator .' Berhasil Ditambahkan!';
    }
    else
    {
      // $chck = DB::table('procurement_designator')->where([
      //   ['id', $id],
      //   ['tgl_start', '<=', $tgl_start],
      //   ['tgl_end', '>=', $tgl_end],
      //   ['design_mitra_id', $req->mitra]
      // ])->get();

      // if(count($chck) > 1)
      // {

      // }
      // else
      // {
        $data_material['modified_by'] = session('auth')->id_user;

        DB::table('procurement_designator')->where('id', $id)->update($data_material);

        $msg = $req->designator .' Berhasil Diperbaharui!';
      }

    // }

		return $msg;
	}

	public static function get_detail_sla($view_table, $aktor, $status, $req)
	{
		$sql = '';
    if($status != 'close')
    {
      if($aktor == 'finish')
      {
        $sql .= " AND ps2.step = 14 ";
      }
      else
      {
        if($view_table == 'all')
        {
          $sql .= " AND ps2.aktor = $aktor";
        }
        else
        {
          $sql .= " AND ps2.aktor = 2 AND pbu.mitra_nm = '$aktor'";
          // $sql .= " AND pbu.mitra_nm = '$aktor'";
        }
      }
    }
    else
    {
      if($aktor == 'finish')
      {
        $sql .= " AND ps2.step = 14 ";
      }
      else
      {
        if($view_table == 'all')
        {
          $sql .= " AND ps.aktor = $aktor";
        }
        else
        {
          $sql .= " AND ps.aktor = 2 AND pbu.mitra_nm = '$aktor'";
          // $sql .= " AND pbu.mitra_nm = '$aktor'";
        }
      }
    }

		switch ($status) {
      case 'open':
        $sql .= " AND ps.action = 'forward' ";
      break;
      case 'trouble':
        $sql .= " AND ps.action != 'forward' ";
      break;
      case 'close':
        $sql .= " AND ps.action = 'forward' ";
      break;
      case 'unclose_D1D':
        $sql .= " AND DATEDIFF(NOW(), pbu.tgl_last_update) = 0 AND ps.action = 'forward' ";
      break;
      case 'unclose_1D':
        $sql .= " AND DATEDIFF(NOW(), pbu.tgl_last_update) = 1 AND ps.action = 'forward' ";
      break;
      case 'unclose_2D':
        $sql .= " AND DATEDIFF(NOW(), pbu.tgl_last_update) = 2 AND ps.action = 'forward' ";
      break;
      case 'unclose_3D':
        $sql .= " AND DATEDIFF(NOW(), pbu.tgl_last_update) = 3 AND ps.action = 'forward' ";
      break;
      case 'unclose_3DD':
        $sql .= " AND DATEDIFF(NOW(), pbu.tgl_last_update) < 7 AND DATEDIFF(NOW(), pbu.tgl_last_update) > 3 AND ps.action = 'forward' ";
      break;
      case 'unclose_7DD':
        $sql .= " AND DATEDIFF(NOW(), pbu.tgl_last_update) > 7 AND ps.action = 'forward' ";
      break;
    }
    // dd($sql);
    if(session('auth')->proc_level == 2)
		{
			$sql .= " AND pbu.mitra_id = ".session('auth')->id_pm;
		}

		if(!empty($req->witel) && $req->witel != 'All')
		{
			$sql .= "AND pbu.witel LIKE '%$req->witel%'";
		}

		switch ($req->tgl_hidden) {
			case 'Tahun':
				$sql .=" AND pbu.created_at LIKE '$req->tgl%' ";
			break;
			case 'Custom':
				$tgl = explode(' - ', $req->tgl);
				$sql .=" AND DATE_FORMAT(pbu.tgl_sp, '%Y-%m-%d') BETWEEN '$req->tgl[0]' AND '$tgl[1]' ";
			break;
			case 'Bulan':
				$sql .=" AND MONTH(pbu.created_at) = $req->tgl";
			break;
		}

		if(session('auth')->proc_level == 2)
		{
			$sql .= " AND pbu.mitra_id = ".session('auth')->id_pm;
		}
		else
		{
			if(!empty($req->mitra) && $req->mitra != 'all')
			{
				$sql .= " AND pbu.mitra_id = $req->mitra";
			}
		}

		if(!empty($req->khs) && strcasecmp($req->khs, 'all') != 0)
		{
			$sql .= " AND pbu.pekerjaan = '$req->khs'";
		}

    $result = DB::SELECT(
			"SELECT pbu.*, pm.nama_company, pbu.judul as judul_work, ps2.nama as nama_step, ps2.aktor_nama as aktor_nama_step, ps.nama as step_before, ps2.nama as step_now, ps3.nama as step_after, pm.witel as witel_mitra, DATEDIFF(NOW(), pbu.tgl_last_update) as jml_hri
			FROM procurement_boq_upload pbu
      LEFT JOIN procurement_step pst ON pbu.step_id = pst.id
      LEFT JOIN 1_2_employee emp ON emp.nik_amija = pbu.created_by
			LEFT JOIN procurement_mitra pm ON pbu.mitra_id = pm.id
      LEFT JOIN procurement_step ps ON ps.id = pbu.step_id
      LEFT JOIN procurement_step ps2 ON ps2.id = ps.step_after
      LEFT JOIN procurement_step ps3 ON ps3.id = ps2.step_after
      WHERE pbu.active = 1 $sql
			ORDER BY pbu.id DESC");

    foreach($result as $v)
    {
      $title[$v->nama_step] = $v->nama_step;
    }
    $title = implode(', ', $title);

		return ['result' => $result, 'title' => $title];
	}

	public static function data_telegram_bot($jenis)
	{
		switch ($jenis) {
			case 'reminder_work':
				$sql = DB::table('procurement_boq_upload As pbu')
				->leftjoin('procurement_step As ps', 'pbu.step_id', '=', 'ps.id')
				->leftjoin('procurement_step As ps2', 'ps2.id', '=', 'ps.step_after')
				->leftjoin('procurement_step As ps3', 'ps3.id', '=', 'ps2.step_after')
				->select('pbu.id', 'pbu.judul', 'pbu.mitra_nm', 'ps.nama as step_sblm', 'ps2.nama as step_skrg', 'ps3.nama as step_next', 'pbu.tgl_last_update')
				->where([
					['pbu.active', 1],
					['step_id', '!=', 14]
				])
				->get();
			break;
			case 'current_work':
				$sql = DB::table('procurement_boq_upload As pbu')
        ->leftjoin('procurement_step As ps', 'pbu.step_id', '=', 'ps.id')
        ->leftjoin('procurement_step As ps2', 'ps2.id', '=', 'ps.step_after')
        ->select('pbu.id', 'pbu.judul', 'pbu.mitra_nm', 'ps.nama as step_skrg', 'ps2.nama as step_next', 'pbu.tgl_last_update')
        ->where([
          ['pbu.active', 1],
        ])
        ->where('pbu.tgl_last_update', 'LIKE', '%'.date('Y-m-d').'%')
        ->get();
			break;
		}
		return $sql;
	}

	public static function find_user($search)
	{
		return DB::table('procurement_user')
		->where('active', 1)
		->where(function($join) use($search){
			$join ->where('user', 'like',  '%'. $search . '%')
			->Orwhere('nik', 'like',  '%'. $search . '%');
		})
    ->get();
	}

	public static function get_list_user_dok($jenis, $witel, $type = 'table')
	{
		$sql = DB::table('procurement_user As pu')
		->leftjoin('procurement_dok_ttd As pdt', 'pdt.id_user', '=', 'pu.id')
		->select('pdt.pekerjaan', 'pdt.rule_dok', 'pu.*', 'pdt.witel as witel_pdt');

		if(strcasecmp($jenis, 'all') != 0)
		{
			$sql->where('pdt.pekerjaan', $jenis);
		}
		else
		{
			$sql->GroupBy('pu.id');
		}

		$sql->where('pu.active', 1);

		$find_witel = DB::Table('promise_witel')->where('Witel', $witel)->first();
		$reg = null;

		if($find_witel)
		{
			$reg = explode(' ', $find_witel->Regional);
			$reg = end($reg);
		}

		if($type == 'table')
		{

			if(session('auth')->proc_level == 44)
			{
				$find_witel = DB::Table('promise_witel As w1')
				->leftjoin('promise_witel As w2', 'w2.Regional', '=', 'w1.Regional')
				->where('w1.Witel', session('auth')->Witel_New)
				->get();

				foreach($find_witel as $vw)
				{
					$all_witel[] = $vw->Witel;
				}

				$sql->Where(function($join) use($reg, $all_witel){
					$join->whereIn('pbu.witel', $all_witel)
					->OrWhere([
						['pu.witel', NULL],
						['pu.reg', $reg]
					]);
				});
			}
			elseif(session('auth')->proc_level != 99)
			{
				$sql->where(function($join) use($reg, $witel){
					$join->where([
						['pdt.witel', $witel],
					])->Orwhere([
						['pu.witel', NULL],
						['pu.reg', $reg]
					]);
				});
			}
		}

		if($type == 'search')
		{
			$sql->where(function($join) use($reg, $witel){
				$join->where([
					['pdt.witel', $witel],
				])->Orwhere([
					['pu.witel', NULL],
					['pu.reg', $reg]
				]);
			});
		}

		return $sql->get();
	}

	public static function get_all_witel()
	{
		return DB::Table('promise_witel')->get();
	}

	public static function getBudgetList()
  {
		$all_budget = DB::table('budget_apm')->get();

		$list_pid = DB::table('proaktif_project')
		->where([
			['fase', 'Executing'],
			['status_project', 'Active']
		])
		->orderBy('tgl_mulai', 'DESC')
		->get();

		$all_list_pbu = DB::table('procurement_boq_upload As pbu')
		->leftjoin('procurement_req_pid As prp', 'pbu.id', '=', 'prp.id_upload')
		->leftjoin('procurement_pid As pp', 'pp.id', '=', 'prp.id_pid_req')
		->select('pbu.*', 'pp.pid')
		->where('active', 1)
		->GroupBy('pbu.id', 'pp.id')
		->get();

		$all_budget   = json_decode(json_encode($all_budget), TRUE);
		$list_pid     = json_decode(json_encode($list_pid), TRUE);
		$all_list_pbu = json_decode(json_encode($all_list_pbu), TRUE);

		$data = [];

		foreach($list_pid as $k => $v)
		{
			$data[$k] = $v;

			if(!isset($data[$k]['Material_Non_Stock']) )
			{
				$data[$k]['Material_Non_Stock']        = '0';
				$data[$k]['Material_Stock']            = '0';
				$data[$k]['Non_Material_Jasa']         = '0';
				$data[$k]['Non_Material_Sewa']         = '0';
				$data[$k]['Non_Material_Tenaga_Kerja'] = '0';
				$data[$k]['Non_Materiall_Operasional'] = '0';
				// $data[$k]['list_pbu']                  = [];
			}

			$key_data_budget = array_keys(array_column($all_budget, 'pid'), $v['pid']);

			foreach($key_data_budget as $vv)
			{
				$main_data = $all_budget[$vv];

				switch ($main_data['keperluan']) {
					case "Material : Non Stock":
						$key_ba_needed = 'Material_Non_Stock';
					break;
					case "Material : Stock":
						$key_ba_needed = 'Material_Stock';
					break;
					case "Non Material : Jasa":
						$key_ba_needed = 'Non_Material_Jasa';
					break;
					case "Non Material : Sewa":
						$key_ba_needed = 'Non_Material_Sewa';
					break;
					case "Non Material : Tenaga Kerja":
						$key_ba_needed = 'Non_Material_Tenaga_Kerja';
					break;
					default:
						$key_ba_needed = 'Non_Materiall_Operasional';
					break;
				}

				$data[$k][$key_ba_needed] = $main_data['value'];
			}

			if(strlen($v['sapid']) != 0)
			{
				$key_data_pbu = array_keys(array_column($all_list_pbu, 'pid'), $v['sapid']);
				// if(count($key_data_pbu) > 1)
				// {
				// 	dd($key_data_pbu);
				// }
				foreach ($key_data_pbu as $kkk => $vvv)
				{
					$main_data = $all_list_pbu[$vvv];

					$data[$k]['judul_pekerjaa_pbu'] = $main_data['judul'];
					$data[$k]['sp']                 = $main_data['no_sp'];
					$data[$k]['bulan']              = $main_data['bulan_pengerjaan'];

					if(!isset($data[$k]['total_material_sp']) )
					{
						$data[$k]['total_material_sp']          = 0;
						$data[$k]['total_jasa_sp']              = 0;
						$data[$k]['total_material_rek_sp_plan'] = 0;
						$data[$k]['total_jasa_rek_sp_plan']     = 0;
						$data[$k]['total_material_rekon_plan']  = 0;
						$data[$k]['total_jasa_rekon_plan']      = 0;
						$data[$k]['total_materialT_rek_plan']   = 0;
						$data[$k]['total_materialK_rek_plan']   = 0;
						$data[$k]['total_jasaT_rek_plan']       = 0;
						$data[$k]['total_jasaK_rek_plan']       = 0;
						$data[$k]['total_material_rekon']       = 0;
						$data[$k]['total_jasa_rekon']           = 0;
						$data[$k]['total_material_rek_sp']      = 0;
						$data[$k]['total_jasa_rek_sp']          = 0;
						$data[$k]['total_materialT_rek']        = 0;
						$data[$k]['total_materialK_rek']        = 0;
						$data[$k]['total_jasaT_rek']            = 0;
						$data[$k]['total_jasaK_rek']            = 0;
					}

					$data[$k]['total_material_sp']          += $main_data['total_material_sp'];
					$data[$k]['total_jasa_sp']              += $main_data['total_jasa_sp'];
					$data[$k]['total_material_rek_sp_plan'] += $main_data['total_material_rek_sp_plan'];
					$data[$k]['total_jasa_rek_sp_plan']     += $main_data['total_jasa_rek_sp_plan'];
					$data[$k]['total_material_rekon_plan']  += $main_data['total_material_rekon_plan'];
					$data[$k]['total_jasa_rekon_plan']      += $main_data['total_jasa_rekon_plan'];
					$data[$k]['total_materialT_rek_plan']   += $main_data['total_materialT_rek_plan'];
					$data[$k]['total_materialK_rek_plan']   += $main_data['total_materialK_rek_plan'];
					$data[$k]['total_jasaT_rek_plan']       += $main_data['total_jasaT_rek_plan'];
					$data[$k]['total_jasaK_rek_plan']       += $main_data['total_jasaK_rek_plan'];
					$data[$k]['total_material_rekon']       += $main_data['total_material_rekon'];
					$data[$k]['total_jasa_rekon']           += $main_data['total_jasa_rekon'];
					$data[$k]['total_material_rek_sp']      += $main_data['total_material_rek_sp'];
					$data[$k]['total_jasa_rek_sp']          += $main_data['total_jasa_rek_sp'];
					$data[$k]['total_materialT_rek']        += $main_data['total_materialT_rek'];
					$data[$k]['total_materialK_rek']        += $main_data['total_materialK_rek'];
					$data[$k]['total_jasaT_rek']            += $main_data['total_jasaT_rek'];
					$data[$k]['total_jasaK_rek']            += $main_data['total_jasaK_rek'];
				}
			}
		}

		$data = json_decode(json_encode($data), FALSE);
		return $data;
  }

	public static function get_dok_ttd($id, $work, $witel)
	{
		return DB::table('procurement_dok_ttd As pdt')
		->leftjoin('procurement_user As pu', 'pdt.id_user', '=', 'pu.id')
		->select('pdt.*', 'pu.nik', 'pu.jabatan', 'pu.user', DB::raw("(CASE WHEN tgl_end > NOW() THEN 'Aktif' ELSE 'Non-Aktif' END) As status_user") )
		->where([
			['pdt.pekerjaan', $work],
			['rule_dok', $id],
			['pdt.witel', $witel]
		])
		->OrderBY('status_user', 'ASC')
		->OrderBY('pdt.id', 'DESC')
		->get();
	}

	public static function get_class_dok($jenis, $witel)
	{
		return DB::table('procurement_dok_ttd As pdt')
		->leftjoin('procurement_user As pu', 'pdt.id_user', '=', 'pu.id')
		->select('pdt.*', 'pu.nik', 'pu.user', DB::raw("(CASE WHEN tgl_end > NOW() THEN 'Aktif' ELSE 'Non-Aktif' END) As status_user") )
		->where([
			['pdt.pekerjaan', $jenis ],
			['pdt.witel', $witel]
		])
		->OrderBY('status_user', 'ASC')
		->OrderBY('pdt.id', 'DESC')
		->get();
	}

	public static function ins_or_up_ttd($req)
	{
		$masa_jabatan = explode(' - ', $req->date_jbtn);
		$start = $masa_jabatan[0];
		$end   = $masa_jabatan[1];

		$check_user = DB::table('procurement_dok_ttd As pdt')
		->leftjoin('procurement_user As pu', 'pdt.id_user', '=', 'pu.id')
		->select('pdt.*', 'pu.nik', 'pu.user')
		->where([
			['rule_dok', $req->rule_dok],
			['pdt.pekerjaan', $req->pekerjaan],
			['id_user', $req->get_nik],
			['pdt.witel', $req->witel]
		])
		->first();

		if($check_user)
		{
			DB::Table('procurement_dok_ttd')->where('id', $check_user->id)->update([
				'tgl_start'   => $start,
				'tgl_end' 	  => $end,
				'modified_by' => session('auth')->id_user
			]);

			$msg['isi']['msg'] = [
				'type' => 'success',
				'text' => "Masa Jabatan $check_user->user Dengan NIK $check_user->nik Berhasil Diubah untuk TTD $req->title!"
			];
		}
		else
		{
			DB::Table('procurement_dok_ttd')->insert([
				'rule_dok'   => $req->rule_dok,
				'witel' 	   => $req->witel,
				'pekerjaan'  => $req->pekerjaan,
				'id_user'    => $req->get_nik,
				'tgl_start'  => $start,
				'tgl_end' 	 => $end,
				'created_by' => session('auth')->id_user
			]);

			$get_name = DB::Table('procurement_user')->where('id', $req->get_nik)->first();

			$msg['isi']['msg'] = [
				'type' => 'success',
				'text' => "User $get_name->user Dengan NIK $get_name->nik Berhasil Ditambah Untuk TTD $req->title!"
			];
		}
		return $msg;
	}

	public static function delete_user_dok($id)
	{
		$history_user = DB::Table('procurement_dok_ttd As pdt')
		->leftjoin('procurement_user As pu', 'pdt.id_user', '=', 'pu.id')
		->select('pdt.*', 'pu.nik', 'pu.user')
		->where('pdt.id', $id)->first();

		DB::Table('procurement_dok_ttd')->where('id', $id)->delete();

		$msg['isi']['msg'] = [
			'type' => 'success',
			'text' => "Masa Jabatan $history_user->user Dengan NIK $history_user->nik Berhasil Dihapus!"
		];

		return $msg;
	}

	public static function get_list_rfc_wh_sync()
	{
		return DB::table('procurement_design_rfc As pdr')
    ->Leftjoin('procurement_designator As pd', 'pd.id', '=', 'pdr.id_design')
		->select('pdr.*', 'pd.designator')
		->where('pd.active', 1)
		->orderBy('design_rfc', 'ASC')
    ->get();
	}
}
