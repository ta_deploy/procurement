<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;
use Session;

date_default_timezone_set("Asia/Makassar");

class MitraModel
{

  // public static function designator_revisi($id)
  // {
  //   return DB::SELECT("SELECT *, uraian as nama_item, COALESCE(pbd.sp, 0) as qty
  //   FROM procurement_material pm
  //   LEFT JOIN procuremen_Boqt_design pbd ON pm.id = pbd.id_design
  //   LEFT JOIn procurement_boq_upload pbu ON pbu.id = pbd.id_upload
  //   LEFT JOIN procurement_sp psp ON psp.id_boq_up = pbu.id
  //   WHERE psp.id = $id ");
  // }

  public static function update_kesanggupan($req, $id)
	{
		$data = ReportModel::get_boq_data($id);

		$bulan = array (
			'01' =>'Januari',
			'02' =>'Februari',
			'03' =>'Maret',
			'04' =>'April',
			'05' =>'Mei',
			'06' =>'Juni',
			'07' =>'Juli',
			'08' =>'Agustus',
			'09' =>'September',
			'10' =>'Oktober',
			'11' =>'November',
			'12' =>'Desember'
		);

		$xp = explode('-', $req->tgl_pks);

		DB::Table('procurement_mitra')
		->where('id', $data->mitra_id)
		->update([
			'no_khs_maintenance'  => $req->pks,
			'tgl_khs_maintenance' => $xp[2] . ' '. $bulan[$xp[1] ] . ' ' . $xp[0]
		]);

		$sql_ins['surat_kesanggupan'] = $req->surat_kesanggupan;
		$sql_ins['pks']               = $req->pks;
		$sql_ins['tgl_pks']           = $req->tgl_pks;
		$sql_ins['tgl_surat_sanggup'] = $req->tgl_sggp;
		$sql_ins['modified_by']       = session('auth')->id_user;

		if($req->jnis_btn == 'Submit')
		{
			$msg['msg'] = [
				'type' => 'success',
				'text' => 'Surat Kesanggupan Berhasil Disubmit!'
			];

			$step_id = 5;

		}
		elseif($req->jnis_btn == 'Save')
		{
			$msg['msg'] = [
				'type' => 'info',
				'text' => 'Surat Kesanggupan Berhasil Disimpan'
			];
			$step_id = 19;
		}

		$sql_ins['step_id'] = $step_id;
		// dd($sql_ins, $id);
		DB::table('procurement_boq_upload')->where('id', $id)->update($sql_ins);
		self::handlesepakatUpload($req, $id);
		AdminModel::save_log($step_id, $id);
		return $msg;
	}

  private static function handlesepakatUpload($req, $id_upload)
  {
    $path = public_path() . '/upload2/' . $id_upload . '/dokumen_sanggup/kesepakatan/';

    if (!file_exists($path)) {
      if (!mkdir($path, 0770, true)) {
        return 'gagal menyiapkan folder sanggup';
      }
    }

    if ($req->hasFile('up_kesepakatan')) {
      $file = $req->file('up_kesepakatan');
      try {
        $filename = 'File Kesepakatan Harga.'.strtolower( $file->getClientOriginalExtension() );
        $file->move("$path", "$filename");
      } catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
        return 'gagal menyimpan up_kesepakatan';
      }
    }
  }

  // public static function get_m($id, $jenis = false, $id_lok = 0) {
	// 	if ($jenis == true) {
	// 		return DB::SELECT('SELECT *, uraian as nama_item, 0 as qty FROM procurement_material');
	// 	} else {
	// 		return DB::SELECT('SELECT *, uraian as nama_item, qty
	// 		FROM procurement_rfc_osp
	// 		LEFT JOIN inventory_material ON (inventory_material.no_rfc = procurement_rfc_osp.rfc AND inventory_material.material = procurement_rfc_osp.material)
	// 		WHERE procurement_rfc_osp.active = 1 AND id_sp =' . $id .' AND lokasi_id ='. $id_lok .' OR id_sp IS NULL ORDER BY qty DESC');
	// 	}
	// }

	private static function handlerfcupload($req, $id_upload, $id_lok)
  {
    $path = public_path() . '/upload2/' . $id_upload . '/osp_fo/rfc/' . $id_lok . '/';

    if (!file_exists($path)) {
      if (!mkdir($path, 0770, true)) {
        return 'gagal menyiapkan folder sanggup';
      }
    }

    if ($req->hasFile('rfc')) {
      $file = $req->file('rfc');
      try {
        $filename = 'File RFC.'.strtolower( $file->getClientOriginalExtension() );
        $file->move("$path", "$filename");
      } catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
        return 'gagal menyimpan rfc';
      }
    }
  }

	// public static function handle_RFC($req, $id_upload)
  // {
	// 	$path = public_path() . '/upload2/' . $id_upload . '/RFC/';

  //   if (!file_exists($path)) {
  //     if (!mkdir($path, 0770, true)) {
  //       return 'gagal menyiapkan folder RFC';
  //     }
  //   }

  //   if ($req->hasFile('rfc')) {
  //     $file = $req->file('rfc');
  //     try {
  //       $filename = 'File RFC.'.strtolower( $file->getClientOriginalExtension() );
  //       $file->move("$path", "$filename");
  //     } catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
  //       return 'gagal menyimpan RFC';
  //     }
  //   }
  // }

	public static function save_ops($req, $id)
	{
		$no_faktur = $req->no_faktur;
		$spp_num = $req->spp_num;
		$receipt_num = $req->receipt_num;
		$no_invoice = $req->no_invoice;

		if($req->chck_rmv_invoice)
		{
			$no_invoice = preg_replace("/[\/.-]/", '', $req->no_invoice);
		}

		if($req->chck_rmv_spp)
		{
			$spp_num = preg_replace("/[\/.-]/", '', $req->spp_num);
		}

		if($req->chck_rmv_kwitansi)
		{
			$receipt_num = preg_replace("/[\/.-]/", '', $req->receipt_num);
		}

		// if($req->chck_rmv_faktur)
		// {
		// 	$no_faktur = preg_replace("/[\/.-]/", '', $req->no_faktur);
		// }

		$step_id = 12;

		DB::table('procurement_boq_upload')->where('id', $id)->update([
			'step_id' => $step_id,
			'tgl_faktur' => $req->tgl_faktur,
			'faktur' => $no_faktur,
			'spp_num' => $spp_num,
			'receipt_num' => $receipt_num,
			'invoice' => $no_invoice,
			'modified_by' => session('auth')->id_user
		]);

		// self::handle_RFC($req, $data->id_boq_up);

		// if ($req->pekerjaan == "PSB")
		// {
		// 	$total_ba_potong = ($req->rs_in_sc_1_hss * $req->rs_in_sc_1_ssl) + ($req->s_clamp_spriner_hss * $req->s_clamp_spriner_ssl) + ($req->breket_hss * $req->breket_ssl) + ($req->utp_c6_hss * $req->utp_c6_ssl) + ($req->precon_50_hss * $req->precon_50_ssl) + ($req->precon_80_hss * $req->precon_80_ssl) + ($req->precon_100_hss * $req->precon_100_ssl) + ($req->precon_150_hss * $req->precon_150_ssl) + ($req->patch_cord_2m_hss * $req->patch_cord_2m_ssl) + ($req->rj_45_hss * $req->rj_45_ssl) + ($req->soc_ils_hss * $req->soc_ils_ssl) + ($req->soc_sum_hss * $req->soc_sum_ssl) + ($req->prekso_intra_15_rs_hss * $req->prekso_intra_15_rs_ssl) + ($req->prekso_intra_20_rs_hss * $req->prekso_intra_20_rs_ssl) + ($req->pu_s7_tiang_hss * $req->pu_s7_tiang_ssl) + ($req->otp_ftth_1_hss * $req->otp_ftth_1_ssl) + ($req->tc_of_cr_200_hss * $req->tc_of_cr_200_ssl) + ($req->ac_of_sm_1b_hss * $req->ac_of_sm_1b_ssl);

		// 	DB::table('procurement_boq_upload')->where('id', $id)->update([
		// 		'step_id' => 12,
		// 		'rs_in_sc_1_hss' => $req->rs_in_sc_1_hss,
		// 		'rs_in_sc_1_ssl' => $req->rs_in_sc_1_ssl,
		// 		's_clamp_spriner_hss' => $req->s_clamp_spriner_hss,
		// 		's_clamp_spriner_ssl' => $req->s_clamp_spriner_ssl,
		// 		'breket_hss' => $req->breket_hss,
		// 		'breket_ssl' => $req->breket_ssl,
		// 		'utp_c6_hss' => $req->utp_c6_hss,
		// 		'utp_c6_ssl' => $req->utp_c6_ssl,
		// 		'precon_50_hss' => $req->precon_50_hss,
		// 		'precon_50_ssl' => $req->precon_50_ssl,
		// 		'precon_80_hss' => $req->precon_80_hss,
		// 		'precon_80_ssl' => $req->precon_80_ssl,
		// 		'precon_100_hss' => $req->precon_100_hss,
		// 		'precon_100_ssl' => $req->precon_100_ssl,
		// 		'precon_150_hss' => $req->precon_150_hss,
		// 		'precon_150_ssl' => $req->precon_150_ssl,
		// 		'patch_cord_2m_hss' => $req->patch_cord_2m_hss,
		// 		'patch_cord_2m_ssl' => $req->patch_cord_2m_ssl,
		// 		'rj_45_hss' => $req->rj_45_hss,
		// 		'rj_45_ssl' => $req->rj_45_ssl,
		// 		'soc_ils_hss' => $req->soc_ils_hss,
		// 		'soc_ils_ssl' => $req->soc_ils_ssl,
		// 		'soc_sum_hss' => $req->soc_sum_hss,
		// 		'soc_sum_ssl' => $req->soc_sum_ssl,
		// 		'prekso_intra_15_rs_hss' => $req->prekso_intra_15_rs_hss,
		// 		'prekso_intra_15_rs_ssl' => $req->prekso_intra_15_rs_ssl,
		// 		'prekso_intra_20_rs_hss' => $req->prekso_intra_20_rs_hss,
		// 		'prekso_intra_20_rs_ssl' => $req->prekso_intra_20_rs_ssl,
		// 		'pu_s7_tiang_hss' => $req->pu_s7_tiang_hss,
		// 		'pu_s7_tiang_ssl' => $req->pu_s7_tiang_ssl,
		// 		'otp_ftth_1_hss' => $req->otp_ftth_1_hss,
		// 		'otp_ftth_1_ssl' => $req->otp_ftth_1_ssl,
		// 		'tc_of_cr_200_hss' => $req->tc_of_cr_200_hss,
		// 		'tc_of_cr_200_ssl' => $req->tc_of_cr_200_ssl,
		// 		'ac_of_sm_1b_hss' => $req->ac_of_sm_1b_hss,
		// 		'ac_of_sm_1b_ssl' => $req->ac_of_sm_1b_ssl,
		// 		'uraian_npk' => $req->uraian_npk,
		// 		'total_ba_potong' => $total_ba_potong
		// 	]);

		// 	$lampiran_dokumen = ['Rekap_Tagihan', 'Rekapitulasi_BA_Instalasi', 'Laporan_Penyelesaian', 'KPI', 'KPI_Addons', 'BARM', 'ENOFA'];
		// 	foreach ($lampiran_dokumen as $input) {
		// 		$path = public_path() . "/upload2/" . $id . "/";

		// 		if (!file_exists($path)) {
		// 			if (!mkdir($path, 0770, true)) {
		// 				return 'Gagal Menyiapkan Folder Upload Lampiran Dokumen';
		// 			}
		// 		}

		// 		if ($req->hasFile($input)) {
		// 			$file = $req->file($input);
		// 			try {
		// 				$filename = $input . '.' . strtolower($file->getClientOriginalExtension());
		// 				$file->move("$path", "$filename");
		// 			} catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
		// 				return 'Gagal Menyimpan File';
		// 			}
		// 		}
		// 	}
		// }

		AdminModel::save_log($step_id, $id);
	}

	public static function save_rfc($req, $id)
	{
		$nama_rfc = DB::table('inventory_material')->where('no_rfc', $req->rfc_id)->get();

		$check_dlu = DB::table('procurement_rfc_osp')->where([
			['rfc', $req->rfc_id],
			['id_upload', $id],
			['active', 1],
		])->first();

		$get_pid = DB::table('procurement_boq_upload As pbu')
		->leftjoin('procurement_req_pid As prp', 'prp.id_upload', '=', 'pbu.id')
		->select('pp.pid')
		->leftjoin('procurement_pid As pp', 'pp.id', '=', 'prp.id_pid_req')
		->where('pbu.id', $id)
		->GroupBy('pp.id')
		->get();

		foreach($get_pid as $p)
		{
			$pid_collect[] = $p->pid;
		}

		if(!$check_dlu)
		{
			foreach($nama_rfc as $val)
			{
				$jenis = "TIDAK LURUS";

				if(in_array($val->wbs_element, $pid_collect) )
				{
					$jenis = 'LURUS';
				}

				$check_jenis = DB::table('procurement_rfc_osp')->where([
					['id_reserv', $val->no_reservasi],
					['jenis_rfc', 'Out']
				])
				->get();

				if($nama_rfc[0]->type == 'Return' && count($check_jenis) || $nama_rfc[0]->type == 'Out')
				{
					DB::table('procurement_rfc_osp')->insert([
						'id_upload'  => $id,
						'id_reserv'  => $val->no_reservasi,
						'rfc'        => $val->no_rfc,
						'status_rfc' => $jenis,
						'jenis_rfc'  => $val->type,
						'material'   => $val->material,
						'created_by' => session('auth')->id_user
					]);
				}
				else
				{
					$msg['msg'] = [
						'type' => 'danger',
						'text' => 'RFR '. $nama_rfc[0]->no_rfc . 'Tidak Bisa Diinput Karena Tidak Terdeteksi RFC nya'
					];

					return \Response::json($msg);
				}
			}

			$msg['msg'] = [
				'type' => 'success',
				'text' => 'Berhasil Tambah RFC '. $nama_rfc[0]->no_rfc
			];
		}
		else
		{
			$msg['msg'] = [
				'type' => 'danger',
				'text' => 'RFC '. $nama_rfc[0]->no_rfc .' Sudah Diinput Sebelumnya!'
			];
		}

    return \Response::json($msg);
	}

	public static function get_lokasi($id)
	{
		$AdminModel = new AdminModel();
		$get_date = DB::Table('procurement_boq_upload')->where('id', $id)->first();

		$pph = $AdminModel->find_pph($get_date->tgl_ba_rekon);

		return DB::SELECT("SELECT pbl.*, pbl.pid_sto as pid_get, pbu.total_material_rekon, pbu.id_project, pbu.total_jasa_rekon, pbu.witel, pm.nama_company,
		(SELECT SUM(budget) FROM procurement_req_pid WHERE id_upload = pbu.id) AS budget_req,
		ROUND( ( (CASE WHEN total_sp IS NULL THEN ( total_material_sp + total_jasa_sp ) ELSE total_sp END) ) ) as gd_sp,
		ROUND( ( total_material_rekon + total_jasa_rekon ) ) as gd_rekon
		FROM procurement_boq_upload pbu
		LEFT JOIN procurement_Boq_lokasi As pbl ON pbu.id = pbl.id_upload
		LEFT JOIN procurement_mitra pm ON pm.id = pbu.mitra_id
		WHERE pbu.id = $id
		GROUP BY pbl.id");
	}

	public static function get_design($id, $sd = 0)
	{
		return DB::SELECT("SELECT pbd.*, pbl.lokasi, pbl.sto, pd.jenis, pbu.pekerjaan, pd.design_mitra_id, pd.satuan, pd.uraian, pbd.material as material_default, pbd.jasa as jasa_default, pbl.sto
		FROM procurement_boq_upload pbu
		LEFT JOIN procurement_Boq_lokasi pbl ON pbu.id = pbl.id_upload
		LEFT JOIN procurement_Boq_design pbd ON pbl.id = pbd.id_boq_lokasi
		LEFT JOIN procurement_designator pd ON pd.id = pbd.id_design
		-- LEFT JOIN procurement_rfc_osp pro ON pro.id_upload = pbu.id
		LEFT JOIN procurement_mitra pm ON pm.id = pbu.mitra_id
		-- WHERE pro.active = 1 AND pro.status_rfc = 'LURUS' AND pbu.id = $id AND pbu.active = 1 AND pbd.status_design = $sd
		WHERE pbu.id = $id AND pbu.active = 1 AND pd.active = 1 AND pbd.status_design = $sd
		GROUP BY pbl.id, pbd.id");
	}

	// public static function get_rfc($id)
	// {
	// 	return DB::SELECT("SELECT pro.*, pm.material, pm.jasa
	// 	FROM procurement_rfc_osp pro
	// 	LEFT JOIN procurement_material pm ON pro.material_id = pm.id
	// 	WHERE id_sp = $id");
	// }

	// public static function get_sp_lokasi($id, $lokasi)
	// {
	// 	$sql = DB::TABLE('procurement_sp As ps')
	// 	->leftjoin('procurement_Boq_lokasi As pbl', 'ps.id_boq_up', '=', 'pbl.id_upload')
	// 	->leftjoin('procurement_rfc_osp As pro', 'pd.id_boq_up', '=', 'pro.id_upload')
	// 	->leftjoin('inventory_material As im', 'im.id_inventory', '=', 'pro.id_invent')
	// 	->select('pbl.*')
	// 	->where([
	// 		['ps.id', $id],
	// 		['pbl.jenis', 'REKON'],
	// 		['pbl.id', $lokasi]
	// 	])
	// 	->groupBy('pbl.id');

	// 	return $sql->get();
	// }

	// public static function get_rfc_osp($id, $id_lok)
	// {
	// 	return DB::TABLE('procurement_rfc_osp As pro')
	// 	->where([
	// 		['pro.id_sp', $id],
	// 		['pro.lokasi_id', $id_lok]
	// 	])
	// 	->first();
	// }

	// public static function data_avail_lok($id)
	// {
	// 	return DB::TABLE('procurement_sp As ps')
	// 	->leftjoin('procurement_Boq_lokasi As pbl', 'ps.id_boq_up', '=', 'pbl.id_upload')
	// 	->leftjoin('procurement_rfc_osp As pro', 'pro.lokasi_id', '=', 'pbl.id')
	// 	->leftjoin('procurement_material As pm', 'pro.material_id', '=', 'pm.id')
	// 	->select('pbl.id as id_pbl', 'pbl.lokasi', 'pm.id as id_pm', 'pm.uraian', 'pro.nomor_rfc_gi', 'pro.nomor_rfc_return', 'pro.qty')
	// 	->whereNotNull('pro.id')
	// 	->where([
	// 		['ps.id', $id],
	// 		['pbl.jenis', 'REKON']
	// 		])
	// 	->get();
	// }

	public static function get_ready_pelurusan($id)
	{
		$sql = DB::table('procurement_boq_upload As pbu')
		->leftjoin('procurement_req_pid As prp', 'pbu.id', '=', 'prp.id_upload')
		->leftjoin('procurement_Boq_lokasi As pbl', 'pbl.id_upload', '=', 'prp.id_upload')
		->select('pbu.*',
			DB::RAW("DATEDIFF(NOW(), pbu.tgl_last_update) as jml_hri"),
			DB::RAW("(SELECT nama FROM 1_2_employee WHERE nik = pbu.modified_by GROUP BY nik) As nama_modif")
		)
		->where([
			['pbu.step_id', $id],
			['pbu.active', 1]
		])
		->GroupBy('pbu.id');

		if(session('auth')->peker_pup)
		{
			$pekerjaan = explode(', ', session('auth')->peker_pup );
			$sql->whereIn('pbu.pekerjaan', $pekerjaan);
		}

		if(session('auth')->proc_level == 44)
		{
			$find_witel = DB::Table('promise_witel As w1')
			->leftjoin('promise_witel As w2', 'w2.Regional', '=', 'w1.Regional')
			->where('w1.Witel', session('auth')->Witel_New)
			->get();

			foreach($find_witel as $vw)
			{
				$all_witel[] = $vw->Witel;
			}

			$sql->whereIn('pbu.witel', $all_witel);
		}
		elseif(session('auth')->proc_level != 99)
		{
			$sql->where('pbu.witel', session('auth')->Witel_New);
		}

		if(session('auth')->proc_level == 2 )
		{
			if(!in_array(session('auth')->id_user, [18940469, 20981020]))
			{
				$sql->where('mitra_nm', session('auth')->mitra_amija_pt);
			}
		}

		return $sql->get();
	}

	public static function save_pelurusan($req, $id)
	{
		return DB::transaction(function () use ($req, $id)
		{
			$get_data_material = AdminModel::get_design();

			$material      = json_decode($req->material_input);
			$find_material = json_decode(json_encode($get_data_material), TRUE);

			foreach($material as $v)
			{
				$get_all_id[$v->id_design] = $v->id_design;
			}

			$get_all_id = "'". implode("', '", $get_all_id) . "'";

			$data_boq_plan = DB::SELECT("SELECT pbd.*, pd.uraian, pd.satuan, pbl.sub_jenis_p, pbd.material as material_default, pbd.jasa as jasa_default, pbl.lokasi, pbl.sto, pbl.pid_sto
			FROM procurement_boq_upload pbu
			LEFT JOIN procurement_Boq_lokasi pbl ON pbu.id = pbl.id_upload
			LEFT JOIN procurement_Boq_design pbd ON pbl.id = pbd.id_boq_lokasi
			LEFT JOIN procurement_designator pd ON pd.id = pbd.id_design
			WHERE pbu.id = ".$id." AND pbd.status_design = 0 AND pd.active = 1 AND pbu.active = 1 AND pbd.id_design NOT IN ($get_all_id) GROUP BY pbd.id");

			$data_boq_plan = json_decode(json_encode($data_boq_plan), TRUE);
			$dbp_mat = [];

			foreach($data_boq_plan as $v)
			{
				$dbp_mat[$v['id_design'] ] = $v;
			}

			foreach($material as $k => $val)
			{
				if(!isset($val->lokasi_id) )
				{
					$msg['direct'] = '/progress/'.$id;
					$msg['isi']['msg'] = [
						'type' => 'warning',
						'text' => 'Terjadi kesalahan! Silahkan input ulang!',
					];
					return $msg;
				}

				$check_lok_me = DB::table('procurement_Boq_lokasi')->where([
					['id', $val->lokasi_id],
					['id_upload', $id],
				])->first();

				$find_me = array_search($val->id, array_column($find_material, 'id') );
				$tambah  = ($val->rekon > $val->sp ? $val->rekon - $val->sp : 0);
				$kurang  = ($val->sp > $val->rekon ? $val->sp - $val->rekon : 0);

				$data_rekon[$k]['id']         = $val->id;
				$data_rekon[$k]['nama']       = $get_data_material[$find_me]->designator;
				$data_rekon[$k]['material']   = $val->material;
				$data_rekon[$k]['jasa']       = $val->jasa;
				$data_rekon[$k]['jenis_khs']  = $val->jenis_khs;
				$data_rekon[$k]['sp']         = $val->sp;
				$data_rekon[$k]['rekon']      = $val->rekon;
				$data_rekon[$k]['tambah']     = $tambah;
				$data_rekon[$k]['kurang']     = $kurang;
				$data_rekon[$k]['lokasi']     = $val->lokasi;
				$data_rekon[$k]['lokasi_ada'] = ($check_lok_me ? 'ada' : NULL);
				$data_rekon[$k]['sto']        = $val->sto;
				$data_rekon[$k]['urutan_sto'] = 'LOK'.$val->urutan;

				$count_jk[] = $val->jenis_khs;

				if(empty($val->sto) )
				{
					$msg['direct'] = '/progress/'.$id;
					$msg['isi']['msg'] = [
						'type' => 'warning',
						'text' => 'STO Tidak Boleh Kosong',
					];
					return $msg;
				}

				if(empty($val->sub_jenis) )
				{
					$msg['direct'] = '/progress/'.$id;
					$msg['isi']['msg'] = [
						'type' => 'warning',
						'text' => 'Sub Jenis Tidak Boleh Kosong',
					];
					return $msg;
				}

				if(!trim($val->lokasi) )
				{
					$msg['direct'] = '/progress/'.$id;
					$msg['isi']['msg'] = [
						'type' => 'warning',
						'text' => 'Lokasi Tidak Boleh Kosong',
					];
					return $msg;
				}

				if($check_lok_me)
				{
					$id_li = $check_lok_me->id;
				}
				else
				{
					$id_li = 'LOK'.$val->urutan;
				}

				$data_lokasi[$id_li]['lokasi_ada'] = ($check_lok_me ? 'ada': NULL);
				$data_lokasi[$id_li]['urutan_sto'] = 'LOK'.$val->urutan;
				$data_lokasi[$id_li]['sto']        = $val->sto;
				$data_lokasi[$id_li]['lokasi']     = $val->lokasi;
				$data_lokasi[$id_li]['sub_jenis']  = $val->sub_jenis;

				if(!isset($data_lokasi[$id_li]['ttl_material_sp_rekon']) )
				{
					$data_lokasi[$id_li]['ttl_material_sp_rekon'] = 0;
					$data_lokasi[$id_li]['ttl_jasa_sp_rekon']     = 0;
					$data_lokasi[$id_li]['ttl_material_rekon']    = 0;
					$data_lokasi[$id_li]['ttl_jasa_rekon']        = 0;

					$data_lokasi[$id_li]['ttl_materialT_rekon'] = 0;
					$data_lokasi[$id_li]['ttl_materialK_rekon'] = 0;
					$data_lokasi[$id_li]['ttl_jasaT_rekon']     = 0;
					$data_lokasi[$id_li]['ttl_jasaK_rekon']     = 0;
				}

				$data_lokasi[$id_li]['ttl_material_sp_rekon'] += $val->material * $val->sp;
				$data_lokasi[$id_li]['ttl_jasa_sp_rekon']     += $val->jasa * $val->sp;
				$data_lokasi[$id_li]['ttl_material_rekon']    += $val->material * $val->rekon;
				$data_lokasi[$id_li]['ttl_jasa_rekon']        += $val->jasa * $val->rekon;

				$data_lokasi[$id_li]['ttl_materialT_rekon'] += $val->material * $tambah;
				$data_lokasi[$id_li]['ttl_materialK_rekon'] += $val->material * $kurang;
				$data_lokasi[$id_li]['ttl_jasaT_rekon']     += $val->jasa * $tambah;
				$data_lokasi[$id_li]['ttl_jasaK_rekon']     += $val->jasa * $kurang;

				$find_unused = array_keys(array_column($data_boq_plan, 'id_boq_lokasi'), $val->lokasi_id);
				// dd($data_boq_plan);
				foreach($find_unused as $v)
				{
					$data_dbp = $data_boq_plan[$v];

					if($data_dbp['id_boq_lokasi'] == $val->lokasi_id)
					{
						$data_rekon[$data_dbp['id_design'] . '_'. $val->urutan]['id']         = $data_dbp['id_design'];
						$data_rekon[$data_dbp['id_design'] . '_'. $val->urutan]['nama']       = $data_dbp['designator'];
						$data_rekon[$data_dbp['id_design'] . '_'. $val->urutan]['material']   = $data_dbp['material'];
						$data_rekon[$data_dbp['id_design'] . '_'. $val->urutan]['jasa']       = $data_dbp['jasa'];
						$data_rekon[$data_dbp['id_design'] . '_'. $val->urutan]['jenis_khs']  = $data_dbp['jenis_khs'];
						$data_rekon[$data_dbp['id_design'] . '_'. $val->urutan]['sp']         = $data_dbp['sp'];
						$data_rekon[$data_dbp['id_design'] . '_'. $val->urutan]['rekon']      = $data_dbp['rekon'];
						$data_rekon[$data_dbp['id_design'] . '_'. $val->urutan]['tambah']     = 0;
						$data_rekon[$data_dbp['id_design'] . '_'. $val->urutan]['kurang']     = $data_dbp['sp'];
						$data_rekon[$data_dbp['id_design'] . '_'. $val->urutan]['lokasi']     = $data_dbp['lokasi'];
						$data_rekon[$data_dbp['id_design'] . '_'. $val->urutan]['lokasi_ada'] = 'ada';
						$data_rekon[$data_dbp['id_design'] . '_'. $val->urutan]['sto']        = $data_dbp['sto'];
						$data_rekon[$data_dbp['id_design'] . '_'. $val->urutan]['urutan_sto'] = 'LOK'.$val->urutan;

						$data_lokasi[$id_li]['ttl_materialK_rekon'] += $data_dbp['material'] * $data_dbp['sp'];
						$data_lokasi[$id_li]['ttl_jasaK_rekon']     += $data_dbp['jasa'] * $data_dbp['sp'];
					}
				}

				// if($val->lokasi_id == 'NEW')
				// {
				// 	foreach($dbp_mat as $v)
				// 	{
				// 		$data_rekon[$v['id_design'] . '_'. $val->urutan]['id']         = $v['id_design'];
				// 		$data_rekon[$v['id_design'] . '_'. $val->urutan]['nama']       = $v['designator'];
				// 		$data_rekon[$v['id_design'] . '_'. $val->urutan]['material']   = $v['material'];
				// 		$data_rekon[$v['id_design'] . '_'. $val->urutan]['jasa']       = $v['jasa'];
				// 		$data_rekon[$v['id_design'] . '_'. $val->urutan]['jenis_khs']  = $v['jenis_khs'];
				// 		$data_rekon[$v['id_design'] . '_'. $val->urutan]['sp']         = 0;
				// 		$data_rekon[$v['id_design'] . '_'. $val->urutan]['rekon']      = 0;
				// 		$data_rekon[$v['id_design'] . '_'. $val->urutan]['tambah']     = 0;
				// 		$data_rekon[$v['id_design'] . '_'. $val->urutan]['kurang']     = 0;
				// 		$data_rekon[$v['id_design'] . '_'. $val->urutan]['lokasi']     = $v['lokasi'];
				// 		$data_rekon[$v['id_design'] . '_'. $val->urutan]['lokasi_ada'] = NULL;
				// 		$data_rekon[$v['id_design'] . '_'. $val->urutan]['sto']        = $v['sto'];
				// 		$data_rekon[$v['id_design'] . '_'. $val->urutan]['urutan_sto'] = 'LOK'.$val->urutan;
				// 	}
				// }

				if(!isset($data_lokasi[$id_li]['jml_vol']) )
				{
					$data_lokasi[$id_li]['jml_vol'] = 0;
				}

				$data_lokasi[$id_li]['jml_vol'] += $val->rekon;
			}
			// dd('try me bitch', $data_rekon, $data_lokasi, $dbp_mat);
			$data_rekon = array_values($data_rekon);

			foreach($material as $v)
			{
				$find_me = array_search($v->id, array_column($find_material, 'id') );

				$check_dup[$v->id .'_'. $v->material .'_'. $v->jasa .'_'. $v->urutan]['mat'] = $find_material[$find_me]['designator'];

				if(!isset($check_dup[$v->id .'_'. $v->material .'_'. $v->jasa .'_'. $v->urutan]['jml']) )
				{
					$check_dup[$v->id .'_'. $v->material .'_'. $v->jasa .'_'. $v->urutan]['jml'] = 0;
				}
				$check_dup[$v->id .'_'. $v->material .'_'. $v->jasa .'_'. $v->urutan]['jml'] += 1;
			}

			foreach($check_dup as $v)
			{
				if($v['jml'] > 1)
				{
					$msg['direct'] = '/progress/'.$id;
					$msg['isi']['msg'] = [
						'type' => 'danger',
						'text' => $v['mat'].' terdeteksi dimasukkan dengan menggunakan <b><u>material</u></b> dan <b><u>jasa</u></b> yang sama sebanyak 2 kali!'
					];
					return $msg;
				}
			}
			// dd($check_dup, $data_rekon, $data_lokasi, 'aman');
			$count_jk = array_sum($count_jk);

			if($count_jk != 0)
			{
				$data_rfc  = self::get_rfc_history($id, 'all');
				$data_rfc  = json_decode(json_encode($data_rfc), TRUE);
				$check_rfc = [];

				// if(count($data_rfc) == 0)
				// {
				// 	$msg['direct'] = '/progress/'.$id;
				// 	$msg['isi']['msg'] = [
				// 		'type' => 'danger',
				// 		'text' => 'Terdapat Material TA, RFC tidak boleh kosong!'
				// 	];
				// 	return $msg;
				// }

				$all_design_rfc = AdminModel::find_all_rfc();
				$all_design_rfc = json_decode(json_encode($all_design_rfc), TRUE);

				foreach($data_rfc as $valc1)
				{
					$check_rfc[$valc1['material'] ]['material'] = $valc1['material'];
					$key_adr_pid = array_search($valc1['material'], array_column($all_design_rfc, 'design_rfc') );

					if(!isset($check_rfc[$valc1['material'] ]['kalkulasi']) )
					{
						$check_rfc[$valc1['material'] ]['kalkulasi'] = 0;
					}

					$multiple = 1;

					if($key_adr_pid !== FALSE)
					{
						$multiple = $all_design_rfc[$key_adr_pid]['qty_per_item'];
					}

					if($valc1['type'] == 'Out')
					{
						$check_rfc[$valc1['material'] ]['kalkulasi'] += $valc1['quantity'] * $multiple;

						$check_rfc[$valc1['material'] ]['isi_m'][$valc1['no_reservasi'] ]['nomor_rfc_gi'] = $valc1['no_rfc'];
						$check_rfc[$valc1['material'] ]['isi_m'][$valc1['no_reservasi'] ]['vol_give']     = $valc1['quantity'];
						$check_rfc[$valc1['material'] ]['isi_m'][$valc1['no_reservasi'] ]['use']          = ($valc1['terpakai'] * $multiple);

						if(!isset($rfc[$valc1['material'] ]['isi_m'][$valc1['no_reservasi'] ]['rumus_terpakai']) )
						{
							$check_rfc[$valc1['material'] ]['isi_m'][$valc1['no_reservasi'] ]['rumus_terpakai'] = ($valc1['terpakai'] * $multiple);
						}

						$check_rfc[$valc1['material'] ]['isi_m'][$valc1['no_reservasi'] ]['rumus_terpakai'] = ($valc1['terpakai'] * $multiple);

						$check_rfc[$valc1['material'] ]['isi_m'][$valc1['no_reservasi'] ]['status_rfc_gi'] = $valc1['status_rfc'];

						if(!isset($check_rfc[$valc1['material'] ]['isi_m'][$valc1['no_reservasi'] ]['nomor_rfc_return']) )
						{
							$check_rfc[$valc1['material'] ]['keterangan'] = $valc1['keterangan'];

							$check_rfc[$valc1['material'] ]['isi_m'][$valc1['no_reservasi'] ]['id_pro']            = $valc1['id_pro'];
							$check_rfc[$valc1['material'] ]['isi_m'][$valc1['no_reservasi'] ]['nomor_rfc_return']  = '-';
							$check_rfc[$valc1['material'] ]['isi_m'][$valc1['no_reservasi'] ]['return']            = 0;
							$check_rfc[$valc1['material'] ]['isi_m'][$valc1['no_reservasi'] ]['status_rfc_return'] = '-';
						}
					}

					if(!isset($check_rfc[$valc1['material'] ]['total_m']))
					{
						$check_rfc[$valc1['material'] ]['total_m'] = 0;
					}

					$check_rfc[$valc1['material'] ]['total_m'] += ($valc1['type'] == 'Out' ? $valc1['quantity'] : 0);

					if(!isset($check_rfc[$valc1['material'] ]['total_m_terpakai']))
					{
						$check_rfc[$valc1['material'] ]['total_m_terpakai'] = 0;
					}

					$check_rfc[$valc1['material'] ]['total_m_terpakai'] += ($valc1['terpakai'] * $multiple);

				}
				// dd($check_rfc);
				foreach($check_rfc as $kk => $val)
				{
					if( $val['total_m_terpakai'] != 0 && $val['total_m_terpakai'] < $val['kalkulasi'] && (empty($val['keterangan']) || trim($val['keterangan']) == '') )
					{
						$msg['direct'] = '/progress/'.$id;
						$msg['isi']['msg'] = [
							'type' => 'danger',
							'text' => 'Keterangan Harus Diisi Karena Material '.$kk.' Terpakai Sedikit!',
						];
						return $msg;
					}
				}
			}

			$pesan = AdminModel::save_boq($req, [], $id, $data_rekon, $data_lokasi, 2);

			if($pesan != 'ok')
			{
				$msg['direct'] = '/progress/'.$id;
				$msg['isi']['msg'] = [
					'type' => $pesan['alerts']['type'],
					'text' => $pesan['alerts']['text'],
				];

				return $msg;
			}

			DB::table('procurement_boq_upload')->where('id', $id)->update([
				'step_id' => 7
			]);

			AdminModel::save_log(7, $id);

			$msg['direct'] = '/progressList/6';
			$msg['isi']['msg'] = [
				'type' => 'success',
				'text' => 'Input Data Rekon berhasil dilakukan! Silahkan Tunggu Verifikasi!',
			];
			return $msg;
		});
	}

	public static function search_rfc($req)
	{
		$sql = DB::table('inventory_material')->where('no_rfc', 'LIKE', trim($req->searchTerm) . '%');
		$program = [];

		if($req->pekerjaan != 'all')
		{
			// $check_khs = AdminModel::get_pekerjaan($req->pekerjaan);

			// if($check_khs->pekerjaan == 'QE' )
			// {
			// 	$program = ['OSP', 'QE Akses', 'QE Akses Project', 'QE Recovery', 'QE Relok Alpro', 'Relok Utilitas', 'QE Recovery 2022'];
			// 	$sql->whereIn('program', $program);
			// }
		}

		$check_ada = DB::Table('procurement_rfc_osp')
		->where('id_upload', $req->id_upload)
		->where('rfc', 'LIKE', trim($req->searchTerm) . '%')
		->where('active', 1)
		->first();

		if($check_ada)
		{
			return null;
		}

		$result = null;

		$sql = $sql->Groupby('no_rfc')->get();

		$check = true;

		if(count($sql) )
		{
			if($req->jenis_grab != 'all')
			{
				$wbs = $sql[0]->wbs_element;
				$check = DB::table('procurement_boq_upload')
				->where('id', $req->id_upload)
				->where('id_project', 'like', '%'. $wbs . '%')
				->first();
			}

			if($check)
			{
				$result = $sql;
			}
		}

		return $result;
	}

	public static function search_rfc_plan($req)
	{
		$sql = DB::table('inventory_material')->where('no_rfc', 'LIKE', trim($req->searchTerm) . '%');

		$check_ada = DB::Table('procurement_fixed_plan_rfc')
		->where('id_judul', $req->id_judul)
		->where('rfc', 'LIKE', trim($req->searchTerm) . '%')
		->where('active', 1)
		->first();

		if($check_ada)
		{
			return null;
		}

		$result = null;

		$sql = $sql->Groupby('no_rfc')->get();

		$check = true;

		if(count($sql) )
		{
			if($check)
			{
				$result = $sql;
			}
		}

		return $result;
	}

	public static function get_rfc_history($id, $status = 'LURUS')
	{
		$sql =  DB::table('procurement_rfc_osp As pro')
		->leftjoin('procurement_req_pid As prp', 'prp.id_upload', '=', 'pro.id_upload')
		->leftjoin('procurement_pid As pp', 'pp.id', '=', 'prp.id_pid_req')
		->leftJoin('inventory_material As im', function($join)
		{
			$join->on('im.no_rfc', '=', 'pro.rfc');
			$join->on('im.material', '=', 'pro.material');
		})
		->leftjoin('1_2_employee As emp', 'emp.nik', '=', 'pro.created_by')
		->select('pro.*', 'im.no_reservasi', 'im.wbs_element', 'pro.id as id_pro', 'im.plant', 'im.quantity', 'im.no_rfc', 'im.mitra', 'im.type', 'im.posting_datex', 'emp.nama')
		->where([
			['prp.id_upload', $id],
			['pro.active', 1],
		]);

		if($status != 'all')
		{
			$sql->where('pro.status_rfc', $status);
		}

		return $sql->WhereNotnull('im.wbs_element')
		// return $sql
		->OrderBy('pro.jenis_rfc', 'ASC')
		->GroupBy('pro.id')
		->get();
	}

	public static function delete_rfc($id)
	{
		$get_name = DB::table('procurement_rfc_osp As pro')
		->where('id', $id)
		->first();

		DB::table('procurement_rfc_osp')->where([
			['rfc', $get_name->rfc],
			['id_upload', $get_name->id_upload]
		])->update([
			'active' => 0,
			'modified_by' => session('auth')->id_user
		]);

		return $get_name->rfc;
	}

	public static function lurus_rfc($id)
	{
		$get_name = DB::table('procurement_rfc_osp As pro')
		->where('id', $id)
		->first();

		DB::table('procurement_rfc_osp')->where([
			['rfc', $get_name->rfc],
			['id_upload', $get_name->id_upload]
		])->update([
			'status_rfc'    => 'LURUS',
			'lurus_dipaksa' => 1,
			'modified_by'		=> session('auth')->id_user
		]);

		return $get_name->rfc . ' berhasil diluruskan!';
	}

	public static function get_all_pid($id)
	{
		return DB::table('procurement_boq_upload As pbu')
		->leftjoin('procurement_req_pid As prp', 'pbu.id', '=', 'prp.id_upload')
		->leftjoin('procurement_pid As pp', 'pp.id', '=', 'prp.id_pid_req')
		->select('pbu.*', 'pp.pid', 'pp.id As id_pid', DB::RAW("(SELECT nama FROM 1_2_employee WHERE nik = pbu.modified_by GROUP BY nik) As nama_modif"),
		DB::RAW("(CASE WHEN pp.be_material_ta = 1 THEN CONCAT_WS('-', pp.pid, '01-01-01') ELSE NULL END) As be_mt"),
		DB::RAW("(CASE WHEN pp.be_material_mitra = 1 THEN CONCAT_WS('-', pp.pid, '01-01-02') ELSE NULL END) As be_mm"),
		DB::RAW("(CASE WHEN pp.be_jasa = 1 THEN CONCAT_WS('-', pp.pid, '01-02-01') ELSE NULL END) As be_j"),
		 'prp.budget', 'prp.created_at', 'prp.budget_up')
		->where([
			['pbu.id', $id],
			['pbu.active', 1]
		])
		->get();
	}

	public static function save_ttd($req, $id)
	{
		$step_id = $req->status_step_id;

		DB::table('procurement_boq_upload')->where('id', $id)->update([
			'step_id'      => $step_id,
			'detail'       => $req->keterangan_stts,
			'no_resi_send' => $req->no_resi_send,
			'modified_by'  => session('auth')->id_user
		]);

		self::handlejampelak($req, $id);
		self::handle_ttd_upload($req, $id);
		if ($req->flag == 'beta')
		{
			self::updateBeta($id, 'beta');
		};
		AdminModel::save_log($step_id, $id, $req->keterangan_stts);

		$extra_msg = '';

		if($req->hasFile('jaminan_pelaksana') )
		{
			$extra_msg .= 'Dan Jaminan Pelaksanaan';
		}
		$msg['msg'] = [
			'type' => 'info',
			'text' => 'Dokumen Tanda Tangan '.$extra_msg.' yang diperlukan, Berhasil di submit!'
		];

		return $msg;
	}

	private static function updateBeta($id, $flag)
	{
		$check = DB::table('procurement_nilai_psb')->where('flag_dok_psb', $flag)->where('id_dokumen', $id)->first();

		if ($check != null)
		{
			DB::statement('UPDATE procurement_order_psb SET is_rekon = 1, dtm_is_rekon = "'.date('Y-m-d H:i:s').'" WHERE is_addon IN (0, 1, 2) AND (DATE(last_updated_date) BETWEEN "'.$check->order_periode1.'" AND "'.$check->order_periode2.'")');
		};
	}

	private static function handlejampelak($req, $id_upload)
	{
		$path = public_path() . '/upload2/' . $id_upload . '/dokumen_jampelak/';

		if (!file_exists($path))
		{
			if (!mkdir($path, 0770, true))
			{
				return 'gagal menyiapkan folder sanggup';
			}
		}

		if ($req->hasFile('jaminan_pelaksana') )
		{
			$file = $req->file('jaminan_pelaksana');
			try {
				$filename = 'File Jaminan Pelaksana.'.strtolower( $file->getClientOriginalExtension() );
				$file->move("$path", "$filename");
			} catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
				return 'gagal menyimpan jaminan_pelaksana';
			}
		}
	}

	private static function handle_ttd_upload($req, $id_upload)
	{
		$path = public_path() . '/upload2/' . $id_upload . '/dokumen_ttd/';

		if (!file_exists($path))
		{
			if (!mkdir($path, 0770, true))
			{
				return 'gagal menyiapkan folder sanggup';
			}
		}

		if ($req->hasFile('upload_ttd') )
		{
			$file = $req->file('upload_ttd');
			try {
				$filename = 'File TTD.'.strtolower( $file->getClientOriginalExtension() );
				$file->move("$path", "$filename");
			} catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
				return 'gagal menyimpan upload_ttd';
			}
		}
	}

	public static function get_pid_base_upload($id)
	{
		return DB::table('procurement_req_pid As prp')
		->leftjoin('procurement_pid As pp', 'prp.id_pid_req', '=', 'pp.id')
		->where('prp.id_upload', $id)
		->get();
	}

	public static function inventory($rfc)
	{
		return DB::Table('inventory_material As im')
		->leftjoin('procurement_rfc_osp As pro', 'pro.rfc', '=', 'im.no_rfc')
		->select('im.*', 'pro.rfc_lama')
		->where([
			['no_rfc', $rfc],
		])
		->GroupBy('im.material')
		->get();
	}

	public static function pekerjaan_per_lok($id)
	{
		return DB::Table('procurement_Boq_lokasi As pbl')
		->leftjoin('procurement_boq_upload As pbu', 'pbl.id_upload', '=', 'pbu.id')
		->leftjoin('procurement_mitra As pm', 'pbu.mitra_id', '=', 'pm.id')
		->leftjoin('procurement_req_pid As prp', 'prp.id_upload', '=', 'pbu.id')
		->leftjoin('procurement_pid As pp', 'prp.id_pid_req', '=', 'pp.id')
		->leftjoin('procurement_Boq_design As pbd', 'pbd.id_boq_lokasi', '=', 'pbl.id')
		->select('pp.pid', 'pm.nama_company', 'pbd.*')
		->where([
			['pbu.id', $id],
			['pbd.status_design', 1]
		])
		->get();
	}

	public static function lokasi_pid($id)
	{
		return DB::Table('procurement_Boq_lokasi As pbl')
		->leftjoin('procurement_boq_upload As pbu', 'pbl.id_upload', '=', 'pbu.id')
		->leftjoin('procurement_req_pid As prp', 'prp.id_upload', '=', 'pbu.id')
		->leftjoin('procurement_pid As pp', 'prp.id_pid_req', '=', 'pp.id')
		->select('pp.pid', 'pbl.*')
		->where('pbu.id', $id)
		->get();
	}

	public static function save_terpakai_mat($req)
	{
			$pk = json_decode(json_encode($req->pk),true);

			DB::table('procurement_rfc_osp')->where('id', $pk['id_pro'])->update([
				'terpakai' => $req->value,
				'modified_by' => session('auth')->id_user
			]);
			// $path = public_path() . '/upload2/' . $pk->id_upload . '/dokumen_rfc/kesepakatan/';

			// if (!file_exists($path)) {
			// 	if (!mkdir($path, 0770, true)) {
			// 		return 'gagal menyiapkan folder sanggup';
			// 	}
			// }

			// if ($req->hasFile('up_kesepakatan')) {
			// 	$file = $req->file('up_kesepakatan');
			// 	try {
			// 		$filename = 'File Kesepakatan Harga.'.strtolower( $file->getClientOriginalExtension() );
			// 		$file->move("$path", "$filename");
			// 	} catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
			// 		return 'gagal menyimpan up_kesepakatan';
			// 	}
			// }
	}

	public static function save_keterangan_rfc($req)
	{
		DB::table('procurement_rfc_osp')->where([
			['material', $req['pk']['material'] ],
			['id_upload', $req['pk']['id_upload'] ],
		])->update([
			'keterangan' => $req['value'],
			'modified_by' => session('auth')->id_user
		]);
	}

	public static function delete_permanen_rfc($id)
	{
		$data = self::find_rfc('rfc', $id);

    if($data->rfc_lama != 0)
    {
      $data_rfc = explode(', ', $data->rfc_lama);

      DB::table('procurement_rfc_osp')
			->whereIn([
        ['rfc', $data_rfc],
      ])
			->where('id_upload', $data->id_upload)
			->update([
        'active' => 1,
        'modified_by' => session('auth')->id_user
      ]);

      $get_old_rfc =  DB::table('procurement_rfc_osp')
      ->where('rfc_lama', 'like', '%'. $id . '%')
      ->where('id_upload', $data->id_upload)
      ->GroupBy('id_upload')
      ->get();

      foreach($get_old_rfc as $v)
      {
        $arr_old = explode(', ', $v->rfc_lama);
        $renew_id = array_diff( $arr_old, [$id] );

        DB::table('procurement_rfc_osp')->where('id_upload', $v->id_upload)->update([
          'rfc_lama'    => implode(', ', $renew_id),
          'modified_by' => session('auth')->id_user
        ]);
      }
    }

		DB::table('procurement_rfc_osp')->where('rfc', $id)->delete();

	}

	public static function save_pelurusan_rfc($id)
	{
		$data_boq_rekon = DB::SELECT("SELECT pbd.*, pd.uraian, pd.satuan, pbd.material as material_default, pbd.jasa as jasa_default, pbl.sto, pbl.lokasi, pbl.pid_sto, SUM(rekon) As total_rekon
    FROM procurement_boq_upload pbu
    LEFT JOIN procurement_Boq_lokasi pbl ON pbu.id = pbl.id_upload
    LEFT JOIN procurement_Boq_design pbd ON pbl.id = pbd.id_boq_lokasi
    LEFT JOIN procurement_designator pd ON pd.id = pbd.id_design
    LEFT JOIN procurement_mitra pm ON pm.id = pbu.mitra_id
    WHERE pbu.id = ".$id." AND pbd.status_design = 1 AND pbu.active = 1 AND pd.active = 1 GROUP BY pbd.id");

    foreach($data_boq_rekon as $v)
    {
      $collect_rekon[$v->id ]['jml_rekon'] = $v->rekon ;
      $collect_rekon[$v->id ]['id_design'] = $v->id_design ;
    }

    $data_rfc = MitraModel::get_rfc_history($id);

    $data_rfc = json_decode(json_encode($data_rfc), TRUE);
    $check_rfc = [];

    foreach($data_rfc as $valc1)
    {
      $check_rfc[$valc1['material'] ]['material'] = $valc1['material'] ;

      if($valc1['type'] == 'Out')
      {
        $check_rfc[$valc1['material'] ]['isi_m'][$valc1['no_reservasi']]['nomor_rfc_gi']  = $valc1['no_rfc'];
        $check_rfc[$valc1['material'] ]['isi_m'][$valc1['no_reservasi']]['vol_give']      = $valc1['quantity'];
        $check_rfc[$valc1['material'] ]['isi_m'][$valc1['no_reservasi']]['use']           = $valc1['terpakai'];
        $check_rfc[$valc1['material'] ]['isi_m'][$valc1['no_reservasi']]['status_rfc_gi'] = $valc1['status_rfc'];

        if(!isset($check_rfc[$valc1['material'] ]['isi_m'][$valc1['no_reservasi']]['nomor_rfc_return']) )
        {
          $check_rfc[$valc1['material'] ]['keterangan'] = $valc1['keterangan'];
          $check_rfc[$valc1['material'] ]['isi_m'][$valc1['no_reservasi']]['id_pro']            = $valc1['id_pro'];
          $check_rfc[$valc1['material'] ]['isi_m'][$valc1['no_reservasi']]['nomor_rfc_return']  = '-';
          $check_rfc[$valc1['material'] ]['isi_m'][$valc1['no_reservasi']]['return']            = 0;
          $check_rfc[$valc1['material'] ]['isi_m'][$valc1['no_reservasi']]['status_rfc_return'] = '-';
        }
      }

      if(!isset($check_rfc[$valc1['material'] ]['total_m']))
      {
        $check_rfc[$valc1['material'] ]['total_m'] = 0;
      }

      $check_rfc[$valc1['material'] ]['total_m'] += ($valc1['type'] == 'Out' ? $valc1['quantity'] : 0);

      if(!isset($check_rfc[$valc1['material'] ]['total_m_terpakai']))
      {
        $check_rfc[$valc1['material'] ]['total_m_terpakai'] = 0;
      }

      $check_rfc[$valc1['material'] ]['total_m_terpakai'] += $valc1['terpakai'];
    }
    // dd($check_rfc, $data_rfc);
    $kosong_item = [];

    foreach($check_rfc as $kk => $val)
    {
			$find_pdr = DB::table('procurement_designator')->Where([
				['designator', preg_replace('/\s+/', '', $kk) ],
				['active', 1]
			])->first();

			if(!$find_pdr)
			{
				$find_pdr = DB::table('procurement_design_rfc')->where('design_rfc', $kk)->get();
				$sum_collect = [];

				foreach ($find_pdr as $v)
				{
					$key = array_search($v->id_design, array_column($collect_rekon, 'id_design') );
					$collect_rekon = array_values($collect_rekon);

					if(!empty($collect_rekon[$key]) )
					{
						$sum_collect[] = $collect_rekon[$key]['jml_rekon'];
					}
					else
					{
						if($v->free_use == 0)
						{
							$kosong_item[] = $kk;
						}
					}
				}

				$sum_collect = array_sum($sum_collect);
				// dd($check_rfc,$collect_rekon[$key], $sum_collect, $kk, $val['total_m_terpakai'], $val['total_m']);
			}
    }

    if($kosong_item)
    {
      $step_id    = 7;
      $msg        = 'Berhasil Pelurusan Semua RFC! Tetapi Dikembalikan Ke Loker Sebelumnya Karena '. implode(', ', $kosong_item) .'Tidak Memiliki Designator Rekon';
      $detail_msg = 'Rollback Karena '. implode(', ', $kosong_item) .'Tidak Memiliki Designator Rekon';
    }
    else
    {
      $step_id    = 9;
      $msg        = 'Berhasil Pelurusan Semua RFC!';
      $detail_msg = null;
    }

		DB::table('procurement_boq_upload')->where('id', $id)->update([
      'step_id' => $step_id
    ]);

    DB::Table('procurement_rfc_osp')->where([
      ['status_rfc', '!=', 'LURUS'],
      ['id_upload', $id],
    ])->update([
      'active'      => 0,
      'modified_by' => session('auth')->id_user
    ]);

    if($kosong_item)
    {
      AdminModel::save_log_rollback($step_id, $id, $detail_msg);
    }
    else
    {
      AdminModel::save_log($step_id, $id, $detail_msg);
    }

		return $msg;
	}

	public static function find_rfc($jenis, $isi)
	{
		switch ($jenis) {
			case 'old_rfc':
				$sql = DB::Table('procurement_rfc_osp')->where('rfc_lama', 'LIKE', '%'. $isi . '%')->first();
			break;
			default:
				$sql = DB::table('procurement_rfc_osp')->where('rfc', $isi)->first();
			break;
		}
		return $sql;
	}

	public static function get_pid_by_id($id)
	{
		return DB::SELECT("SELECT pp.*
		FROM procurement_pid As pp
		LEFT JOIN procurement_req_pid prp ON prp.id_pid_req = pp.id
		WHERE prp.id_upload = $id
		GROUP BY pp.id");
	}
}
