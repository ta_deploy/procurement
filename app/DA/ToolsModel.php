<?php

namespace App\DA;
use Illuminate\Support\Facades\DB;

date_default_timezone_set("Asia/Makassar");

class ToolsModel
{
  public static function show_mitra()
	{
		return DB::TABLE('procurement_mitra')->orderBy('witel', 'DESC')->orderBy('nama_company', 'ASC')->where('active', 1)->get();
	}

  public static function list_pekerjaan()
	{
		$pekerjaan = DB::table('procurement_pekerjaan')
		->select('pekerjaan as id', 'pekerjaan as text')
		->where('active', 1)
		->orderBy('pekerjaan', 'ASC')
		->GroupBy('pekerjaan')
		->get();

		$jenis_pekerjaan = DB::table('procurement_pekerjaan')
		->select('pekerjaan', 'jenis as id', 'jenis as text')
		->where('active', 1)
		->orderBy('pekerjaan', 'ASC')
		->orderBy('jenis', 'ASC')
		->GroupBy('jenis', 'pekerjaan')
		->get();

		$jenis_khs = DB::table('procurement_pekerjaan')
		->select('pekerjaan', 'khs as id', 'khs as text')
		->where('active', 1)
		->orderBy('khs', 'ASC')
		->GroupBy('khs')
		->get();

		return [$pekerjaan, $jenis_pekerjaan, $jenis_khs];
	}

	public static function convert_month($date, $cetak_hari = false)
    {
        $bulan = array (1 => 'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );

        $hari = array ( 1 => 'Senin',
            'Selasa',
            'Rabu',
            'Kamis',
            'Jumat',
            'Sabtu',
            'Minggu'
        );

        $result = date('d m Y', strtotime($date) );
        $split = explode(' ', $result);
        $hasilnya = $split[0] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[2];

        if($cetak_hari)
        {
            $num = date('N', strtotime($date) );
            return $hari[$num].' '.$hasilnya;
        }

        return $hasilnya;
    }

	public static function save_mitra($req, $id)
	{
		// dd($req->all(), $id);
		if(session('auth')->mitra_amija_pt)
		{
			$check = self::get_mitra($id);
			$bulan = array (
				'01' => 'Januari',
				'02' => 'Februari',
				'03' => 'Maret',
				'04' => 'April',
				'05' => 'Mei',
				'06' => 'Juni',
				'07' => 'Juli',
				'08' => 'Agustus',
				'09' => 'September',
				'10' => 'Oktober',
				'11' => 'November',
				'12' => 'Desember',
			);
			$tgl                 = explode('-', $req->tgl_khs_maintenance);
			$tgl_khs_maintenance = $tgl[2] .' '. $bulan[$tgl[1] ] .' '. $tgl[0];

			if ($req->no_khs_maintenance) {
				$split_sp  = explode('/', $req->no_khs_maintenance);
				$split_tgl = explode('-', $req->tgl_khs_maintenance);

				// if(@$split_sp[3] != $split_tgl[1].'-'.$split_tgl[0])
				// {
				// 	$msg['direct'] ='/tools/mitra/edit/'.$id;
				// 	$msg['isi']['msg'] = [
				// 		'type' => 'danger',
				// 		'text' => 'Kode KHS Tidak Sesuai!'
				// 	];
				// }
			}

			$witel = DB::table('witel')->where('id_witel', $req->witel)->first();
			// dd($witel);

			$data_mitra = [
				'nama_company'				=> $req->nama_company,
				'alamat_company'			=> $req->alamat_company,
				'lokasi_comp'				=> $req->lokasi_comp,
				'wakil_mitra'				=> $req->wakil_mitra,
				'jabatan_mitra'				=> $req->jabatan_mitra,
				'telp'						=> $req->telp,
				'witel'						=> $witel->nama_witel,
				'area'						=> $witel->psa_witel,
				'area_sto'					=> $witel->sto_witel,
				'rek'						=> str_replace('-', '', $req->rek),
				'pph'						=> $req->pph,
				'bank'						=> $req->bank,
				'npwp'						=> $req->npwp,
				'lokasi_npwp'				=> $req->lokasi_npwp,
				'cabang_bank'				=> $req->cabang_bank,
				'atas_nama'					=> $req->atas_nama,
				'no_kontrak_ta'				=> $req->no_kontrak_ta,
				'tgl_kontrak_ta'			=> self::convert_month($req->date_kontrak_ta),
				'date_kontrak_ta'			=> $req->date_kontrak_ta,
				'no_penetapan_khs'			=> $req->no_penetapan_khs,
				'tgl_khs_penetapan'			=> self::convert_month($req->date_khs_penetapan),
				'date_khs_penetapan'		=> $req->date_khs_penetapan,
				'no_kesanggupan_khs'		=> $req->no_kesanggupan_khs,
				'tgl_khs_kesanggupan'		=> self::convert_month($req->date_khs_kesanggupan),
				'date_khs_kesanggupan'		=> $req->date_khs_kesanggupan,
				'no_amandemen1'				=> $req->no_amandemen1,
				'date_amandemen1'			=> $req->date_amandemen1,
				'date_expired_amandemen1'	=> $req->date_expired_amandemen1,
				'no_amandemen2'				=> $req->no_amandemen2,
				'date_amandemen2'			=> $req->date_amandemen2,
				'date_expired_amandemen2'	=> $req->date_expired_amandemen2,
				'no_amandemen3'				=> $req->no_amandemen3,
				'date_amandemen3'			=> $req->date_amandemen3,
				'date_expired_amandemen3'	=> $req->date_expired_amandemen3,
				'no_amandemen4'				=> $req->no_amandemen4,
				'date_amandemen4'			=> $req->date_amandemen4,
				'date_expired_amandemen4'	=> $req->date_expired_amandemen4,
				'no_amandemen5'				=> $req->no_amandemen5,
				'date_amandemen5'			=> $req->date_amandemen5,
				'date_expired_amandemen5'	=> $req->date_expired_amandemen5,
				'no_khs_maintenance'		=> $req->no_khs_maintenance,
				'tgl_khs_maintenance'		=> $tgl_khs_maintenance,
			];

			// dd($check);

			if ($check)
			{
				unset($data_mitra['nama_company']);
				DB::table('procurement_mitra')->where('id', $id)->update($data_mitra);

				$msg['direct'] = '/tools/data/mitra';
				$msg['isi']['msg'] = [
					'type' => 'success',
					'text' => 'Mitra '. $req->nama_company .' Berhasil Ditambahkan!'
				];
			}
			else
			{
				$id = DB::table('procurement_mitra')->insertGetId($data_mitra);
				$check_amija = DB::table('mitra_amija')->where('mitra_amija_pt', $req->nama_company)->first();

				if(!$check_amija)
				{
					DB::table('mitra_amija')->insert([
						'mitra_amija_pt' => $req->nama_company,
						'mitra_amija'    => $req->alias_mitra,
						'kat'            => 'DELTA',
						'mitra_status'   => 'ACTIVE'
					]);
				}
				else
				{
					DB::table('mitra_amija')->where('mitra_amija_pt', $req->nama_company)->update([
						'mitra_amija' => $req->alias_mitra,
					]);
				}

				$msg['direct'] = '/tools/data/mitra';
				$msg['isi']['msg'] = [
					'type' => 'success',
					'text' => 'Mitra '. $req->nama_company .' Berhasil Diperbaharui!'
				];
			}

			$that = new AdminModel();

			foreach ($that->file_mitra as $input)
			{
				$path = public_path() . "/upload_mitra2/" . $id . "/";

				if (!file_exists($path) )
				{
					if (!mkdir($path, 0770, true) ) {
						return 'Gagal Menyiapkan Folder Mitra';
					}
				}

				if ($req->hasFile($input) )
				{
					$file = $req->file($input);
					try
					{
						$filename = $input.'.'.strtolower($file->getClientOriginalExtension() );
						$file->move("$path", "$filename");
					}
					catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e)
					{
						return 'Gagal Menyimpan File';
					}
				}
			}
		}
		else
		{
			$get_data = DB::TABLE("mitra_amija")->where('mitra_amija_pt', $req->nama_company)->first();
			DB::TABLE("1_2_employee")->where('nik', session('auth')->id_user)->update([
				'mitra_amija' => $get_data->mitra_amija
			]);

			$msg['direct'] = '/tools/edit/mitra';
			$msg['isi']['msg'] = [
				'type' => 'success',
				'text' => 'Berhasil Tambah Mitra Ke Profil User!'
			];
		}
		return $msg;
	}

  public static function save_or_update_user_dok($req, $id)
	{
		$check_data = DB::TABLE("procurement_user")->where('id', $id)->first();
		$find_witel = DB::Table('promise_witel')->where('Witel', session('auth')->Witel_New)->first();
		$reg = null;

		if($find_witel)
		{
			$reg = explode(' ', $find_witel->Regional);
			$reg = end($reg);
		}

    $data_saved = [
      'nik'       => $req->nik,
      'user'      => $req->nama,
      'jabatan'   => $req->jabatan,
      'pekerjaan' => $req->pekerjaan,
      // 'status' => $req->status,
      'reg'       => $reg,
      'witel'     => session('auth')->Witel_New,
    ];

		if(!$check_data)
		{
			DB::table('procurement_user')->insert($data_saved);

			$msg['direct'] = '/tools/list/user_dok';
			$msg['isi']['msg'] = [
				'type' => 'success',
				'text' => 'Berhasil Tambah Profil User!'
			];
		}
		else
		{
			// $check_dup = DB::TABLE("procurement_user")->where([
			// 	['id', '!=', $id],
			// 	['nik', $req->nik],
			// 	['active', 1],
			// ])->first();

			// if($check_dup)
			// {
			// 	$msg['direct'] = '/tools/user/'.$id;
			// 	$msg['isi']['msg'] = [
			// 		'type' => 'danger',
			// 		'text' => 'Nik Tidak Boleh Sama!'
			// 	];
			// }
			// else
			// {
				DB::table('procurement_user')->where('id', $id)->update($data_saved);

				$msg['direct'] = '/tools/list/user_dok';
				$msg['isi']['msg'] = [
					'type' => 'success',
					'text' => 'Data User Berhasil Diinput!'
				];
			// }
		}
		return $msg;
	}

  public static function find_mitra($id)
	{
		return DB::table('procurement_mitra')->where('nama_company', $id)->first();
	}

	public static function get_mitra($id)
	{
		return DB::table('procurement_mitra')->where('id', $id)->first();
	}

	public static function save_pid_main($req)
	{
		$budget_exceeded = 0;

		$msg['direct']= '/Report/pid/list';

		if($req->status_pid == 'BE')
		{
			$budget_exceeded = 1;
		}

		$check_pid = ReportModel::find_pid($req->pid, 'pid');

		if($check_pid)
		{
			$msg['isi']['msg'] = [
				'type' => 'danger',
				'text' => "PID $req->pid Sudah Ada!"
			];
			return $msg;
		}

		$arr = [];

		if($req->be_mm)
		{
			$arr['be_material_mitra'] = 1;
		}

		if($req->be_j)
		{
			$arr['be_jasa'] = 1;
		}

		if($req->be_mt)
		{
			$arr['be_material_ta'] = 1;
		}

		$arr['pid']             = trim($req->pid);
		$arr['keterangan']      = $req->desc;
		$arr['mitra_id']        = $req->mitra_id;
	  // $arr['budget']          = $harga;
		$arr['budget_exceeded'] = $budget_exceeded;
		$arr['created_by']      = session('auth')->id_user;
		$arr['modified_by']     = session('auth')->id_user;

		DB::table('procurement_pid')->insert($arr);

		$msg['isi']['msg'] = [
			'type' => 'success',
			'text' => "PID $req->pid Berhasil Ditambahkan!"
		];
		return $msg;
	}

	public static function delete_user_ttd($id)
	{
		DB::table('procurement_user')->where('id', $id)->update([
			'active' => 0
		]);

		$check_ttd_user = DB::table('procurement_dok_ttd')->where('id_user', $id)->first();
		$msg['direct']= '/tools/list/user_dok';

		if(!$check_ttd_user)
		{
			$msg['isi']['msg'] = [
				'type' => 'danger',
				'text' => "User Berhasil Dinonaktifkan!"
			];
		}
		else
		{
			$msg['isi']['msg'] = [
				'type' => 'danger',
				'text' => "User berhasil dihapus. Segera perbaharui list TTD User!"
			];
		}

		return $msg;
	}

	public static function get_witel()
	{
		return DB::table('witel')->where('active', 1)->select('id_witel as id', 'nama_witel as text')->get();
	}

	public static function get_hss_psb()
	{
		if (session('auth')->proc_level == 4)
		{
			return DB::table('procurement_hss_psb')->get();
		}
		else
		{
			return DB::table('procurement_hss_psb')->where('witel', session('auth')->Witel_New)->get();
		}
	}

	public static function save_hss_psb_witel($req, $witel)
	{
		DB::table('procurement_hss_psb')->where('witel', $witel)->update([
			'project_id'         => $req->input('project_id'),
			'p1'                 => $req->input('p1'),
			'p2_inet_voice'      => $req->input('p2_inet_voice'),
			'p2_inet_iptv'       => $req->input('p2_inet_iptv'),
			'p3'                 => $req->input('p3'),
			'p1_ku'              => $req->input('p1_ku'),
			'p2'                 => $req->input('p2'),
			'p2_inet_voice_ku'   => $req->input('p2_inet_voice_ku'),
			'p2_inet_iptv_ku'    => $req->input('p2_inet_iptv_ku'),
			'p3_ku'              => $req->input('p3_ku'),
			'migrasi_1p'         => $req->input('migrasi_1p'),
			'migrasi_2p'         => $req->input('migrasi_2p'),
			'migrasi_3p'         => $req->input('migrasi_3p'),
			'stb_tambahan'       => $req->input('stb_tambahan'),
			'smartcam_only'      => $req->input('smartcam_only'),
			'wifiext'            => $req->input('wifiext'),
			'plc'                => $req->input('plc'),
			'lme_wifi'           => $req->input('lme_wifi'),
			'lme_ap_indoor'      => $req->input('lme_ap_indoor'),
			'lme_ap_outdoor'     => $req->input('lme_ap_outdoor'),
			'ont_premium'        => $req->input('ont_premium'),
			'migrasi_stb'        => $req->input('migrasi_stb'),
			'change_stb'         => $req->input('change_stb'),
			'instalasi_ipcam'    => $req->input('instalasi_ipcam'),
			'insert_tiang'       => $req->input('insert_tiang'),
			'insert_tiang_9'     => $req->input('insert_tiang_9'),
			'tray_cable'         => $req->input('tray_cable'),
			'rs_in_sc_1'         => $req->input('rs_in_sc_1'),
			's_clamp_spriner'    => $req->input('s_clamp_spriner'),
			'breket'             => $req->input('breket'),
			'utp_c6'             => $req->input('utp_c6'),
			'precon_50'          => $req->input('precon_50'),
			'precon_80'          => $req->input('precon_80'),
			'precon_100'         => $req->input('precon_100'),
			'precon_150'         => $req->input('precon_150'),
			'patch_cord_2m'      => $req->input('patch_cord_2m'),
			'rj_45'              => $req->input('rj_45'),
			'soc_ils'            => $req->input('soc_ils'),
			'soc_sum'            => $req->input('soc_sum'),
			'prekso_intra_15_rs' => $req->input('prekso_intra_15_rs'),
			'prekso_intra_20_rs' => $req->input('prekso_intra_20_rs'),
			'pu_s7_tiang'        => $req->input('pu_s7_tiang'),
			'otp_ftth_1'         => $req->input('otp_ftth_1'),
			'tc_of_cr_200'       => $req->input('tc_of_cr_200'),
			'ac_of_sm_1b'        => $req->input('ac_of_sm_1b'),
			'updated_by'         => session('auth')->id_user,
			'updated_dtm'        => date('Y-m-d H:i:s')
		]);
	}

	public static function get_distinct_material()
	{
		return DB::SELECT("SELECT a.* FROM procurement_designator a LEFT JOIN procurement_designator b ON a.designator = b.designator AND a.id < b.id WHERE a.witel = 'KALSEL' AND a.active = 1 GROUP BY a.designator, a.material, a.jasa");
	}

	public static function get_tiket($isi, $id_design)
	{
		$get_design = DB::table('procurement_designator')->where([
			['id', $id_design],
			['active', 1]
		])->first();

		return DB::table('maintaince As m')
		->leftjoin('maintaince_mtr As mm', 'm.id', '=', 'mm.maintaince_id')
		->select('m.*')
		->Where([
			['status', 'close'],
			['isHapus', 0],
			['no_tiket', 'like', "%$isi%"],
			[DB::RAW("REPLACE(REPLACE(REPLACE(id_item, ' ', ''), '\t', ''), '\n', '')"), preg_replace('/\s+/', '', $get_design->designator) ]
		])
		->limit(10)
		->GroupBy('no_tiket')
		->get();
	}

	public static function list_planning()
	{
		return DB::table('procurement_fixed_plan_master As a')
		->Leftjoin('procurement_fixed_plan_data As c', 'a.id', '=', 'c.id_judul')
		->Leftjoin('procurement_designator As b', 'c.id_design', '=', 'b.id')
		->Select('a.*', 'c.id_design', 'c.id_tiket', 'b.designator')
		->Where('a.actived', 1)
		->GroupBy('judul', 'id_design')
		->OrderBy('a.id', 'DESC')
		->get();
	}

	public static function get_planning_id($id)
	{
		return DB::table('procurement_fixed_plan_master As a')
		->Leftjoin('procurement_fixed_plan_data As c', 'a.id', '=', 'c.id_judul')
		->Where('a.id', $id)
		->get();
	}

	public static function save_planning($req)
	{
		$material = json_decode($req->field_table);

		DB::transaction(function () use ($req, $material)
		{
			$no = DB::table('procurement_fixed_plan_master')->insertGetId([
				'judul'      => $req->judul,
				'created_by' => session('auth')->id_user
			]);

			foreach($material as $v)
			{
				DB::table('procurement_fixed_plan_data')->insert([
					'id_judul'	 => $no,
					'id_design'  => $v->design,
					'id_tiket'   => json_encode($v->no_tiket),
					'created_by' => session('auth')->id_user
				]);
			}
		});
	}

	public static function find_tiket($id)
	{
		return DB::Table('maintaince')->Where('id', $id)->first();
	}

	public static function save_planning_rfc($req, $id)
	{
		foreach($req->tiket as $k => $v)
		{
			$insert[$k]['id_tiket']   = $v;
			$insert[$k]['rfc']        = $req->rfc_id;
			$insert[$k]['id_judul']   = $id;
			$insert[$k]['created_by'] = session('auth')->id_user;
		}

		DB::table('procurement_fixed_plan_master')->Where('id', $id)->update([
			'step' => 2
		]);

		DB::table('procurement_fixed_plan_rfc')->insert($insert);
	}

	public static function get_tiket_with_rfc($id)
	{
		return DB::Table('procurement_fixed_plan_rfc As a')
		->Leftjoin('maintaince As b', 'a.id_tiket', '=', 'b.id')
		->Leftjoin('inventory_material As c', 'a.rfc', '=', 'c.no_rfc')
		->select('a.*', 'b.no_tiket', 'c.type')
		->Where('a.id_judul', $id)
		->get();
	}

	public static function get_download_planning($id)
	{
		$data = DB::table('procurement_fixed_plan_master As a')
		->leftjoin('procurement_fixed_plan_rfc As c', 'c.id_judul', '=', 'a.id')
		->leftjoin('maintaince As d', 'd.id', '=', 'c.id_tiket')
		->leftjoin('maintaince_mtr As e', 'd.id', '=', 'e.maintaince_id')
		->select('a.judul', 'c.rfc', 'c.id_tiket', 'e.id_item', 'e.qty', 'd.no_tiket', 'd.sto')
		->Where('a.id', $id)
		->get();

		$designator = DB::table('procurement_designator As a')
		->select('a.*', DB::RAW("REPLACE(REPLACE(REPLACE(designator, ' ', ''), '\t', ''), '\n', '') As design_2") )
		->where([
			['witel', 'KALSEL'],
			['active', 1]
		])
		->get();

		$renew_data = $material = $list_design = [];

		$data_arr = array_map(function($item) {
			return (array)$item;
		}, $data->toArray() );

		$designator = array_map(function($item) {
			return (array)$item;
		}, $designator->toArray() );

		$material = array_map(function(){
			return 0;
		}, array_flip(array_unique(array_map(function($x){
			return $x['id_item'];
		}, $data_arr) ) ) );

		$material_arr = array_map(function(){
			return [];
		}, array_flip(array_unique(array_map(function($x){
			return $x['id_item'];
		}, $data_arr) ) ) );

		foreach($data as $k => $v)
		{
			$renew_data['judul'] = $v->judul;

			if(!isset($renew_data['material_tomman'][$v->sto][$v->id_item]) )
			{
				$renew_data['material_tomman'][$v->sto] = $material;
				$renew_data['tiket_tomman'][$v->sto] = $material_arr;
			}

			$renew_data['tiket_tomman'][$v->sto][$v->id_item]['no_tiket'][$k] = $v->no_tiket;
			$renew_data['tiket_tomman'][$v->sto][$v->id_item]['rfc'][$k] = $v->rfc;
			$renew_data['material_tomman'][$v->sto][$v->id_item] += $v->qty;
		}

		foreach($material as $k => $v)
		{
			$find_k = array_search($k, array_column($designator, 'design_2') );

			if($find_k !== FALSE)
			{
				$des = $designator[$find_k];

				$list_design[$des['designator'] ]['designator'] = $des['designator'];
				$list_design[$des['designator'] ]['uraian']     = $des['uraian'];
				$list_design[$des['designator'] ]['satuan']     = $des['satuan'];
				$list_design[$des['designator'] ]['material']   = $des['material'];
				$list_design[$des['designator'] ]['jasa']       = $des['jasa'];
			}
			else
			{
				$list_design[$k]['designator'] = $k;
				$list_design[$k]['uraian']     = '';
				$list_design[$k]['satuan']     = '';
				$list_design[$k]['material']   = '';
				$list_design[$k]['jasa']       = '';
			}
		}

		return ['data' => $renew_data, 'designator' => $list_design];
	}

	public static function delete_planning_rfc($id)
	{
		return DB::Table('procurement_fixed_plan_master')->Where('id', $id)->update([
			'actived' => 0
		]);
	}

	public static function search_wbs_pid($data)
	{
		return DB::Table('inventory_material')->Where('wbs_element', 'like', '%'.$data.'%')->limit(20)->GroupBy('wbs_element')->get();
	}

	public static function get_inventory_material_by_wbs($data)
	{
		return DB::Table('inventory_material')->WhereIn('wbs_element', $data)->get();
	}
}