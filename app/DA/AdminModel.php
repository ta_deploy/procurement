<?php

namespace App\DA;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use ZipArchive;
use Illuminate\Support\Facades\DB;
use App\DA\MitraModel;
use App\DA\ReportModel;
use App\Http\Controllers\GrabController;
use PhpOffice\PhpWord\Element\Table;

date_default_timezone_set("Asia/Makassar");

class AdminModel
{
	public $file_mitra = ['PKS_PSB_MIGRASI_2020', 'BAK_PEKERJAAN_PSB_MIGRASI', 'AMANDEMEN_PERTAMA_PEMBAYARAN_PSB', 'KHS_PSB_MIGRASI_2020', 'SIUJK_OSS', 'SURAT_KUASA', 'SBU', 'NPWP', 'SPT_PPN'];

	public function find_pph($id)
	{
		return (date('Y-m', strtotime($id ) ) >= '2022-04' ? 11/100 : 10/100);
	}

	public function find_pph_text($id)
	{
		return (date('Y-m', strtotime($id) ) >= '2022-04' ? '11%' : '10%');
	}

	// public static function search_material($id) {
	// 	return DB::table('procurement_material_dispt')
	// 	->leftjoin('procurement_material', 'procurement_material_dispt.material_id', '=', 'procurement_material.id')
	// 	->where('id_data', $id)->get();
	// }

	public static function show_Single_id_data($id) {
		return DB::TABLE('procurement_pengadaan')->leftjoin('procurement_mitra', 'procurement_pengadaan.id_mitra', '=', 'procurement_mitra.id')->select('procurement_mitra.*', 'procurement_mitra.id as kunci_mitra', 'procurement_pengadaan.*')->where('procurement_pengadaan.id', $id)->first();
	}

	public static function search_rfc($req) {
		return DB::SELECT("SELECT msr.*,
			SUM( CASE WHEN action = 1 THEN 1 ELSE 0 END) AS keluar_gudang,
			SUM( CASE WHEN action = 2 THEN 1 ELSE 0 END) AS terpakai,
			SUM( CASE WHEN action = 3 THEN 1 ELSE 0 END) AS kembali
			FROM maintenance_saldo_rfc msr
			WHERE msr.rfc = '$req->data' AND DATE_FORMAT(created_at, '%Y-%m-%d') >= '$req->m1' AND created_at <= '$req->m2' GROUP BY id_item
			");
	}

	public static function get_m($id, $no_id = false) {
		if ($no_id == true) {
			return DB::SELECT('SELECT *, uraian as nama_item, 0 as qty FROM procurement_material');
		} else {
			return DB::SELECT('SELECT *, uraian as nama_item, 0 qty
			FROM procurement_material LEFT JOIN procurement_material_dispt ON procurement_material.id = procurement_material_dispt.material_id WHERE jenis = "rekon" AND id_data =' . $id);
		}
	}

	public static function mitra_search($data, $witel = null)
	{
		$sql = DB::table('procurement_mitra')->where([
			['nama_company', 'like', '%' . $data . '%'],
			['active', 1]
		]);

		if($witel == 1)
		{
			$sql->where('witel', session('auth')->Witel_New);
		}

		return $sql->get();
	}

	public static function area_alamat($witel = null)
	{
		$sql = DB::Table('maintenance_datel')->select('sto as kode_area');

		if($witel)
		{
			$sql->where('witel', $witel);
		}
		else
		{
			$sql->where('witel', session('auth')->Witel_New);
		}

		return $sql->get();
	}

	public static function get_design($jns = 0, $data = null)
	{
		switch ($jns) {
			case 0:
				$sql = DB::table('procurement_designator As pd')
				->select(DB::RAW("(CASE WHEN design_mitra_id = 16 THEN 'Telkom Akses' ELSE 'Mitra' END) As namcomp"), 'pd.*')
				->where('pd.active', 1)
				->get();
			break;
			case 1:
				$sql = DB::table('procurement_designator As pd')
				->select(DB::RAW("(CASE WHEN design_mitra_id = 16 THEN 'Telkom Akses' ELSE 'Mitra' END) As namcomp"), 'pd.*')
				->where('pd.active', 1)
				->where('id', $data)->first();
			break;
			case 2:
				$sql = DB::TABLE('procurement_designator As pd')
				->select(DB::RAW("(CASE WHEN design_mitra_id = 16 THEN 'Telkom Akses' ELSE 'Mitra' END) As namcomp"), 'pd.*')
				->where([
					['witel', session('auth')->Witel_New],
					['pd.active', 1]
				])
				->orderBy('id', 'DESC')
				->get();
			break;
			case 3:
				$data = DB::Table('procurement_boq_upload')->where('id', $data)->first();

				$tgl_start = $data->bulan_pengerjaan .'-01';
				$tgl_end = $data->bulan_pengerjaan . '-' . date('t', strtotime(date($data->bulan_pengerjaan, strtotime('last day of this month') ) ) );
				$find_khs = self::get_pekerjaan($data->pekerjaan);
				// dd($tgl_start, $tgl_end);
				$sql = DB::table('procurement_designator As pd')
				->leftjoin('procurement_mitra As pm', 'pm.id', '=', 'pd.design_mitra_id')
				->select('pd.*', 'pd.id As id_design', DB::RAW("(CASE WHEN pd.design_mitra_id = 16 THEN 'Telkom Akses' ELSE 'Mitra' END) As namcomp"))
				->where([
					['pd.witel', $data->witel],
					['pd.active', 1]
				])
				->where(function($join) use($find_khs){
					$join->where('jenis', $find_khs->khs)
					->orwhere('jenis', 'NEW_ITEM');
				})
				// ->where(function($join) use($tgl_start, $tgl_end){
				// 	$join->where([
				// 		['tgl_start', '<=', $tgl_start],
				// 		['tgl_end', '>=', $tgl_end],
				// 	])
				// 	->orwhereNull('tgl_start');
				// })
				->get();
			break;
		}

		return $sql;
	}

	public static function get_mitra($id)
	{
		return DB::TABLE('procurement_mitra As pm')
		->leftjoin('mitra_amija As am', function($join){
			$join->on('am.mitra_amija_pt', '=', 'pm.nama_company')
			->Where('am.witel', '=', 'pm.witel')
			->Where('am.kat', '=', 'DELTA');
		})
		->select('pm.*', 'am.mitra_amija')
		->where('id', $id)
		->first();
	}

	public static function save_boq($req, $total_data, $id, $data_rekon, $data_lokasi, $status_pbd)
	{
		return DB::transaction(function () use ($req, $total_data, $id, $data_rekon, $data_lokasi, $status_pbd)
		{
			if(empty(json_decode($req->material_input) ) )
			{
				return [
					'alerts' => ['type' => 'warning', 'text' => 'Harap Masukkan Material!']
				];
			}

			if($status_pbd == 0)
			{
				$jenis_po  = null;

				if($req->jns_btn == 'submit')
				{
					$step_id = 1;
				}
				else
				{
					$step_id  = 33;
				}

				$get_mitra = AdminModel::get_mitra($req->mitra_id);

				if($req->pekerjaan == 'Construction')
				{
					$toc = $req->toc_edit;
					if($toc <= 0)
					{
						return [
							'alerts' => ['type' => 'warning', 'text' => 'TOC harap dimasukkan!']
						];
					}

					$jenis_po = $req->jenis_po;
				}
				else
				{
					$toc = date('t', strtotime($req->tahun.'-'. $req->bulan) );
				}

				foreach($data_lokasi as $v)
				{
					$sto[$v['sto'] ] = $v['sto'];
					$lokasi[$v['lokasi'] ] = $v['lokasi'];
				}

				if($id)
				{
					DB::table('procurement_boq_upload')->Where('id', $id)->update([
						'judul'               => trim(preg_replace('/[\t\n\r\s]+/', ' ', $req->judul) ),
						'witel'               => session('auth')->Witel_New,
						'jenis_kontrak'       => $req->jenis_kontrak,
						'jenis_po'            => $jenis_po,
						'total_material_sp'   => $total_data['total_sp_material'],
						'total_jasa_sp'       => $total_data['total_sp_jasa'],
						'total_sp'		        => $total_data['total_sp_material'] + $total_data['total_sp_jasa'],
						'pekerjaan'           => $req->pekerjaan,
						'lokasi_pekerjaan'    => implode(', ', $lokasi),
						'bulan_pengerjaan'    => $req->tahun.'-'. $req->bulan,
						'step_id'             => $step_id,
						'jenis_work'          => $req->jenis_work,
						'mitra_id'            => $req->mitra_id,
						'sto_pekerjaan'       => implode(', ', $sto),
						'mitra_nm'            => $get_mitra->nama_company,
						'toc'                 => $toc,
						'modified_by'         => session('auth')->id_user
					]);

					DB::SELECT(
						"DELETE pbd, pbl
						FROM procurement_Boq_lokasi pbl
						LEFT JOIN procurement_Boq_design pbd ON pbd.id_boq_lokasi = pbl.id
						WHERE pbl.id_upload = $id"
					);
				}
				else
				{
					$id = DB::table('procurement_boq_upload')->insertGetId([
						'judul'               => trim(preg_replace('/[\t\n\r\s]+/', ' ', $req->judul) ),
						'witel'               => session('auth')->Witel_New,
						'jenis_kontrak'       => $req->jenis_kontrak,
						'jenis_po'            => $jenis_po,
						'total_material_sp'   => $total_data['total_sp_material'],
						'total_jasa_sp'       => $total_data['total_sp_jasa'],
						'total_sp'		        => $total_data['total_sp_material'] + $total_data['total_sp_jasa'],
						'pekerjaan'           => $req->pekerjaan,
						'lokasi_pekerjaan'    => implode(', ', $lokasi),
						'bulan_pengerjaan'    => $req->tahun.'-'. $req->bulan,
						'step_id'             => $step_id,
						'jenis_work'          => $req->jenis_work,
						'mitra_id'            => $req->mitra_id,
						'sto_pekerjaan'       => implode(', ', $sto),
						'mitra_nm'            => $get_mitra->nama_company,
						'toc'                 => $toc,
						'created_by'          => session('auth')->id_user,
						'modified_by'         => session('auth')->id_user
					]);
				}

				$auto_next = 0;

				foreach($data_lokasi as $val_cl)
				{
					if($val_cl['jml_vol'] == 0)
					{
						return [
							'alerts' => ['type' => 'warning', 'text' => 'Jumlah Designator Tidak Boleh Kosong Semuanya!'],
						];
					}
					else
					{
						$id_lok = DB::table('procurement_Boq_lokasi')->insertGetId([
							'id_upload'   => $id,
							'lokasi'      => $val_cl['lokasi'],
							'sto'         => $val_cl['sto'],
							'sub_jenis_p' => $val_cl['sub_jenis'],
							'material_sp' => $val_cl['ttl_material_sp'],
							'jasa_sp'     => $val_cl['ttl_jasa_sp'],
						]);

						$get_sapid = DB::Table('proaktif_project')->where('id', $val_cl['id_proaktif'])->first();

						if($val_cl['id_proaktif'] != 0)
						{
							DB::Table('procurement_proaktif_boq')->insert([
								'id_pbu' => $id,
								'id_proaktif_project' => $val_cl['id_proaktif']
							]);
						}

						if($get_sapid && (isset($get_sapid->sapid) || $get_sapid->sapid != '') )
						{
							$auto_next = 2;
							$sapid = $get_sapid->sapid;

							$check_pid = DB::SELECT("SELECT (SELECT CONCAT_WS(', ',
								( CASE WHEN (SELECT SUM(pbd.material)
								FROM procurement_Boq_design pbd
								WHERE pbd.id_boq_lokasi = ".$id_lok." AND pbd.jenis_khs= 16) != 0 THEN CONCAT_WS('-', ppc.wbs, '01-01-01' ) ELSE NULL END),
								( CASE WHEN (SELECT SUM(pbd.material)
								FROM procurement_Boq_design pbd
								WHERE pbd.id_boq_lokasi = ".$id_lok." AND (pbd.jenis_khs != 16 OR pbd.jenis_khs IS NULL) ) != 0 THEN CONCAT_WS('-', ppc.wbs, '01-01-02' ) ELSE NULL END),
								( CASE WHEN (SELECT SUM(pbd.jasa)
								FROM procurement_Boq_design pbd
								WHERE pbd.id_boq_lokasi = ".$id_lok." ) != 0 THEN CONCAT_WS('-', ppc.wbs, '01-02-01' ) ELSE NULL END) ) ) As idp
								FROM budget_apm ppc
								WHERE ppc.wbs = '$sapid'")[0];

							$idp_pbl[$id_lok][] = $check_pid->idp;
							$idp[] = $check_pid->idp;

							$check_pp = DB::table('procurement_pid')->where('pid', $sapid)->first();

							if(!$check_pp)
							{
								$id_pp = DB::table('procurement_pid')->insertGetId([
									'pid'        => $sapid,
									'mitra_id'   => $req->mitra_id,
									'jenis_pid'  => 'NON GAMAS',
									'keterangan' => $req->jenis_work,
									'created_by' => session('auth')->id_user,
								]);
							}
							else
							{
								$id_pp = $check_pp->id;
							}

							$get_harga = DB::table('procurement_Boq_lokasi')->where('id', $id_lok)->first();

							DB::Table('procurement_req_pid')->insert([
								'id_upload'   => $id,
								'id_lokasi'   => $id_lok,
								'id_pid_req'  => $id_pp,
								'created_by'  => session('auth')->id_user,
								'budget'      => $get_harga->material_sp + $get_harga->jasa_sp
							]);

							foreach($idp_pbl as $k => $v)
							{
								DB::table('procurement_Boq_lokasi')->where('id', $k)->update([
									'pid_sto' => implode(', ', $v),
								]);
							}

							DB::table('procurement_boq_upload')->where('id', $id)->update([
								'id_project'  => implode(', ', $idp),
							]);
						}

						foreach($data_rekon as $val_rek)
						{
							if ($val_rek['urutan_sto'] == $val_cl['urutan_sto'] )
							{
								if($val_rek['sp'] != 0 || $val_rek['rekon'] != 0 )
								{
									DB::table('procurement_Boq_design')->insert([
										'id_boq_lokasi' => $id_lok,
										'id_design'     => $val_rek['id'],
										'designator'    => $val_rek['nama'],
										'material'      => $val_rek['material'],
										'jasa'          => $val_rek['jasa'],
										'sp'            => $val_rek['sp'],
										'jenis_khs'     => $val_rek['jenis_khs'],
										'rekon'         => 0,
										'tambah'        => 0,
										'kurang'        => 0,
										'status_design' => $status_pbd
									]);
								}
							}
						}
					}
				}

				self::save_log($step_id, $id);

				if($auto_next != 0)
				{
					self::save_log($auto_next, $id);
				}

				return 'ok';
			}
			else
			{
				$all_null = array_column($data_lokasi, 'lokasi_ada');

				if(count(array_filter($all_null) ) == 0 )
				{
					return [
						'alerts' => ['type' => 'warning', 'text' => 'Tidak Bisa Submit Karena Semua LOP Baru!']
					];
				}

				if($status_pbd == 2)
				{
					DB::SELECT(
						"DELETE pbd
						FROM procurement_Boq_lokasi pbl
						LEFT JOIN procurement_Boq_design pbd ON pbd.id_boq_lokasi = pbl.id
						WHERE pbl.id_upload = $id AND pbd.status_design = 1"
					);
				}

				DB::SELECT(
					"DELETE pbd
					FROM procurement_Boq_lokasi pbl
					LEFT JOIN procurement_Boq_design pbd ON pbd.id_boq_lokasi = pbl.id
					WHERE pbl.id_upload = $id AND pbd.status_design = $status_pbd"
				);

				$total_mat_rekon = $total_jasa_rekon = $total_mat_rekon_sp = $total_jasa_rekon_sp =  $total_mat_tmb = $total_mat_krg = $total_jasa_tmb = $total_jasa_krg = 0 ;

				foreach($data_lokasi as $klok => $val_cl)
				{
					$data_pbl = [];

					$sto[$val_cl['sto'] ] = $val_cl['sto'];
					$lokasi[$val_cl['lokasi'] ] = $val_cl['lokasi'];

					$total_mat_rekon_sp  += $val_cl['ttl_material_sp_rekon'];
					$total_jasa_rekon_sp += $val_cl['ttl_jasa_sp_rekon'];
					$total_mat_rekon     += $val_cl['ttl_material_rekon'];
					$total_jasa_rekon    += $val_cl['ttl_jasa_rekon'];

					$total_mat_tmb  += $val_cl['ttl_materialT_rekon'];
					$total_mat_krg += $val_cl['ttl_materialK_rekon'];
					$total_mat_krg  += $val_cl['ttl_jasaT_rekon'];
					$total_jasa_krg += $val_cl['ttl_jasaK_rekon'];

					if($status_pbd == 2)
					{
						$data_pbl['material_sp_rek_plan'] = $val_cl['ttl_material_sp_rekon'];
						$data_pbl['jasa_sp_rek_plan']     = $val_cl['ttl_jasa_sp_rekon'];
						$data_pbl['material_rek_plan']    = $val_cl['ttl_material_rekon'];
						$data_pbl['jasa_rek_plan']        = $val_cl['ttl_jasa_rekon'];

						$data_pbl['materialT_rek_plan']   = $val_cl['ttl_materialT_rekon'];
						$data_pbl['materialK_rek_plan']   = $val_cl['ttl_materialK_rekon'];
						$data_pbl['jasaT_rek_plan']       = $val_cl['ttl_jasaT_rekon'];
						$data_pbl['jasaK_rek_plan']       = $val_cl['ttl_jasaK_rekon'];
					}
					else
					{
						$data_pbl['material_sp_rek'] = $val_cl['ttl_material_sp_rekon'];
						$data_pbl['jasa_sp_rek']     = $val_cl['ttl_jasa_sp_rekon'];
						$data_pbl['material_rek']    = $val_cl['ttl_material_rekon'];
						$data_pbl['jasa_rek']        = $val_cl['ttl_jasa_rekon'];

						$data_pbl['materialT_rek']   = $val_cl['ttl_materialT_rekon'];
						$data_pbl['materialK_rek']   = $val_cl['ttl_materialK_rekon'];
						$data_pbl['jasaT_rek']       = $val_cl['ttl_jasaT_rekon'];
						$data_pbl['jasaK_rek']       = $val_cl['ttl_jasaK_rekon'];
					}

					if($val_cl['lokasi_ada'])
					{
						$data_pbl['lokasi']      = $val_cl['lokasi'];
						$data_pbl['sub_jenis_p'] = $val_cl['sub_jenis'];
						$data_pbl['sto']         = $val_cl['sto'];

						DB::table('procurement_Boq_lokasi')->where('id', $klok)->update($data_pbl);
					}
					else
					{
						$data_pbl['id_upload']   = $id;
						$data_pbl['lokasi']      = $val_cl['lokasi'];
						$data_pbl['sto']         = $val_cl['sto'];
						$data_pbl['sub_jenis_p'] = $val_cl['sub_jenis'];
						$data_pbl['new_boq']     = 1;

						$klok = DB::table('procurement_Boq_lokasi')->insertGetId($data_pbl);
					}

					// if($val_cl['jml_vol'] == 0)
					// {
					// 	return [
					// 		'alerts' => ['type' => 'warning', 'text' => 'Jumlah Designator Tidak Boleh Kosong Semuanya!'],
					// 	];
					// }
					// else
					// {
					// 	foreach($data_rekon as $val_rek)
					// 	{
					// 		if ($val_rek['urutan_sto'] == $val_cl['urutan_sto'] )
					// 		{
					// 			if($val_rek['sp'] != 0 || $val_rek['rekon'] != 0 )
					// 			{
					// 				$update = [
					// 					'id_boq_lokasi' => $klok,
					// 					'id_design'     => $val_rek['id'],
					// 					'designator'    => $val_rek['nama'],
					// 					'material'      => $val_rek['material'],
					// 					'jasa'          => $val_rek['jasa'],
					// 					'sp'            => $val_rek['sp'],
					// 					'jenis_khs'     => $val_rek['jenis_khs'],
					// 					'rekon'         => $val_rek['rekon'],
					// 					'tambah'        => $val_rek['tambah'],
					// 					'kurang'        => $val_rek['kurang'],
					// 					'status_design' => $status_pbd
					// 				];
					// 				DB::table('procurement_Boq_design')->insert($update);
					// 			}
					// 		}
					// 	}
					// }
					foreach($data_rekon as $val_rek)
					{
						if ($val_rek['urutan_sto'] == $val_cl['urutan_sto'] )
						{
							if($val_rek['sp'] != 0 || $val_rek['rekon'] != 0 )
							{
								$update = [
									'id_boq_lokasi' => $klok,
									'id_design'     => $val_rek['id'],
									'designator'    => $val_rek['nama'],
									'material'      => $val_rek['material'],
									'jasa'          => $val_rek['jasa'],
									'sp'            => $val_rek['sp'],
									'jenis_khs'     => $val_rek['jenis_khs'],
									'rekon'         => $val_rek['rekon'],
									'tambah'        => $val_rek['tambah'],
									'kurang'        => $val_rek['kurang'],
									'status_design' => $status_pbd
								];
								DB::table('procurement_Boq_design')->insert($update);
							}
						}
					}
				}

				// dd($total_mat_rekon_sp ,$total_jasa_rekon_sp,$total_mat_rekon ,$total_jasa_rekon ,$total_mat_tmb ,$total_jasa_tmb,$total_mat_krg ,$total_jasa_krg);

				DB::SELECT(
					"DELETE pbl
					FROM procurement_Boq_lokasi pbl
					LEFT JOIN procurement_Boq_design pbd ON pbd.id_boq_lokasi = pbl.id
					WHERE pbl.id_upload = $id AND pbd.id IS NULL "
				);

				$update = [
					'lokasi_pekerjaan' => implode(', ', $lokasi),
					'sto_pekerjaan'    => implode(', ', $sto),
					'modified_by'      => session('auth')->id_user
				];

				if($req->note)
				{
					$update['detail'] = $req->note;
				}

				if($status_pbd == 2)
				{
					$update['total_material_rek_sp_plan'] = $total_mat_rekon_sp;
					$update['total_jasa_rek_sp_plan']     = $total_jasa_rekon_sp;

					$update['total_material_rekon_plan'] = $total_mat_rekon;
					$update['total_jasa_rekon_plan']     = $total_jasa_rekon;
					$update['total_materialT_rek_plan']  = $total_mat_tmb;
					$update['total_materialK_rek_plan']  = $total_mat_krg;
					$update['total_jasaT_rek_plan']      = $total_jasa_tmb;
					$update['total_jasaK_rek_plan']      = $total_jasa_krg;
				}
				else
				{
					$update['total_material_rek_sp'] = $total_mat_rekon_sp;
					$update['total_jasa_rek_sp']     = $total_jasa_rekon_sp;

					$update['total_material_rekon'] = $total_mat_rekon;
					$update['total_jasa_rekon']     = $total_jasa_rekon;
					$update['total_materialT_rek']  = $total_mat_tmb;
					$update['total_materialK_rek']  = $total_mat_krg;
					$update['total_jasaT_rek']      = $total_jasa_tmb;
					$update['total_jasaK_rek']      = $total_jasa_krg;
				}

				DB::table('procurement_boq_upload')->where('id', $id)->update($update);

				return 'ok';
			}
		});
	}

	public static function get_data_final($id)
	{
		$AdminModel = new AdminModel();

		$date = DB::table('procurement_boq_upload')->where('id', $id)->first();

		$pph = $AdminModel->find_pph($date->tgl_ba_rekon);
		$pph_sp = $AdminModel->find_pph($date->tgl_sp);

		$query = DB::table("procurement_boq_upload AS pbu")
		->leftJoin("procurement_step AS ps", "ps.id","=","pbu.step_id")
		->leftJoin("procurement_mitra AS pm", "pm.id","=","pbu.mitra_id")
		->leftJoin("1_2_employee AS emp", "emp.nik","=","pbu.created_by")
		->leftjoin("procurement_nilai_psb AS pns", function($join){
			$join->on('pbu.id', '=', 'pns.id_dokumen')
			->Where('pns.desc', '=', 'BARIJ');
		})
		->leftJoin("bulan_format AS bf", "pns.periode_kegiatan", "=", "bf.id")
		->select("pbu.*", "ps.step_after", "pns.periode_kegiatan", "pns.flag_dok_psb", "ps.action AS action_ps", "ps.urutan", "emp.nama", "pm.id AS company_id","pm.nama_company","pm.alamat_company", "pm.telp", "pm.tgl_khs_maintenance", "bf.date_akhir_periode2", "pm.no_khs_maintenance AS no_khs", "pm.no_khs_maintenance", "pm.no_kontrak_ta", "pm.no_penetapan_khs", "pm.tgl_khs_penetapan", "pm.no_kesanggupan_khs", "pm.tgl_khs_kesanggupan", "bf.date_periode1", "pm.date_kontrak_ta", "pm.date_khs_penetapan", "pm.date_khs_kesanggupan",
		DB::raw("(CASE WHEN pbu.tgl_s_pen IS NOT NULL THEN DATE_ADD(pbu.tgl_s_pen, INTERVAL pbu.tgl_jatuh_tmp DAY) ELSE DATE_ADD(pbu.tgl_sp, INTERVAL pbu.tgl_jatuh_tmp DAY) END) AS durasi"),
		DB::raw("(CASE WHEN pbu.total_sp IS NULL THEN (pbu.total_material_sp + pbu.total_jasa_sp) ELSE pbu.total_sp END) as gd_sp"),
		DB::RAW("(SELECT nama FROM 1_2_employee WHERE nik = pbu.modified_by GROUP BY nik) As nama_modif"),
		DB::RAW("(SELECT nama FROM 1_2_employee WHERE nik = pbu.created_by GROUP BY nik) As nama_created"),
		DB::raw("(pbu.total_material_rekon + pbu.total_jasa_rekon) as gd_rekon "),
		DB::raw("( (CASE WHEN pbu.total_sp IS NULL THEN (pbu.total_material_sp + pbu.total_jasa_sp) ELSE pbu.total_sp END) * $pph_sp) as gd_ppn_sp"),
		DB::raw("( (CASE WHEN pbu.total_sp IS NULL THEN (pbu.total_material_sp + pbu.total_jasa_sp) ELSE pbu.total_sp END) + (CASE WHEN pbu.total_sp IS NULL THEN (pbu.total_material_sp + pbu.total_jasa_sp) ELSE pbu.total_sp END) * $pph_sp) as total_sp"),
		DB::raw("( (pbu.total_material_rekon + pbu.total_jasa_rekon) * $pph) as gd_ppn_rekon"),
		DB::raw("( (pbu.total_material_rekon + pbu.total_jasa_rekon) + ( (pbu.total_material_rekon + pbu.total_jasa_rekon) * $pph) ) as total_rekon") );

		$query->Where([
			["pbu.id", $id],
		])
		->GroupBy("pbu.id");

		return $query->first();
	}

	public static function check_step($data)
	{
		return DB::table('procurement_step As ps1')
		->leftjoin('procurement_step As ps2', 'ps1.step_after', '=', 'ps2.id')
		->select('ps2.*')
		->where('ps1.id', $data)
		->first();
	}

	public static function rollback_process($req, $id)
	{
		if($req->opsi_rollback == 'prev_loker')
		{
			$rollback_detail = DB::table('procurement_boq_upload As pbu')
			->leftjoin('procurement_step As ps', 'ps.step_after', '=', 'pbu.step_id')
			->leftjoin('procurement_step As ps2', 'ps2.id', '=', 'pbu.step_id')
			->select('ps.id', 'ps2.nama', 'ps2.aktor_nama', 'pbu.step_id as prev_step', 'ps2.action', 'ps2.urutan')
			->where('pbu.id', $id)
			->first();

			if($rollback_detail->action == 'stay')
			{
				$urut = (int)$rollback_detail->urutan;
				$find_prev = DB::table('procurement_step As ps')
				->leftjoin('procurement_step As ps2', 'ps2.step_after', '=', 'ps.id')
				->leftjoin('procurement_step As ps3', 'ps3.step_after', '=', 'ps2.id')
				->select('ps3.id', 'ps2.nama', 'ps2.aktor_nama', 'ps2.id as id_ps2')
				->where('ps.urutan', $urut)
				->first();

				$id_step                    = $find_prev->id;
				$nama                       = $find_prev->nama;
				$aktor_nm                   = $find_prev->aktor_nama;
				$rollback_detail->prev_step = $find_prev->id_ps2;
			}
			elseif($rollback_detail->action == 'backward')
			{
				$urut = (int)$rollback_detail->urutan;

			  $find_prev = DB::Table('procurement_step')->where('urutan', floor($urut) )->first();

				$id_step                    = $find_prev->id;
				$nama                       = $find_prev->nama;
				$aktor_nm                   = $find_prev->aktor_nama;
				$rollback_detail->prev_step = $find_prev->prev_step;
			}
			else
			{
				$id_step  = $rollback_detail->id;
				$nama     = $rollback_detail->nama;
				$aktor_nm = $rollback_detail->aktor_nama;
			}
		}
		else
		{
			$rollback_detail = DB::Table('procurement_step As ps')
			->leftjoin('procurement_step As ps2', 'ps2.id', '=', 'ps.step_after')
			->select('ps.id', 'ps2.nama', 'ps2.aktor_nama', 'ps.id as prev_step', 'ps2.action', 'ps2.urutan')
			->where('ps.id', $req->opsi_rollback)
			->first();

			$id_step  = $rollback_detail->id;
			$nama     = $rollback_detail->nama;
			$aktor_nm = $rollback_detail->aktor_nama;
		}

		DB::table('procurement_boq_upload')->where('id', $id)->update([
			'modified_by' => session('auth')->id_user,
			'step_id' => $id_step,
		]);

		if($rollback_detail->prev_step == 9)
		{
			DB::table('procurement_rfc_osp')->where('id_upload', $id)->update([
				'active' => 1
			]);
		}

		AdminModel::save_log_rollback($id_step, $id, 'Di Kembalikan Ke loker '. $nama .' ('. $aktor_nm .') Dengan Catatan: '.$req->ket);

		return $rollback_detail->prev_step;
	}

	public static function save_pelurusan($id, $req)
	{
		$nama_rfc = DB::table('inventory_material')->where('no_rfc', $req->rfc_id)->get();

		$rfc_old = '';
		$ld = [];
		if($req->rfc_old)
		{
			$rfc_old = implode(', ', $req->rfc_old);
			// DB::table('procurement_rfc_osp')->where('rfc', $rfc_old)->update([
			// 	'active' => 0
			// ]);

			foreach($req->rfc_old as $v)
			{
				$load_data[$v] = DB::table('procurement_rfc_osp')->where([
					['rfc', $v],
					['id_upload', $id]
				])->get();
			}

			foreach($load_data as $k => $v)
			{
				foreach($v as $vv)
				{
					$ld[$vv->material]['material'] = $vv->material;

					if(!isset($ld[$vv->material]['qty']) )
					{
						$ld[$vv->material]['qty'] = 0;
					}

					$ld[$vv->material]['qty'] += $vv->terpakai;
				}
			}
			$ld = array_values($ld);
			$msg_psn = 'Pelurusan PID dari '. $rfc_old .' Ke '. $req->rfc_id . ' Berhasil!';
		}
		else
		{
			$msg_psn = 'PID '. $req->rfc_id . ' Berhasil Ditambahkan!';
		}

		// if($req->jenis_grab != 'all')
		// {
		// 	$wbs = $nama_rfc[0]->wbs_element;
		// 	// dd($wbs, $sql);
		// 	$check = DB::table('procurement_boq_upload')
		// 	->where('id', $req->id_upload)
		// 	->where('id_project', 'like', '%'. $wbs . '%')
		// 	->first();
		// }

		// $stts_lurus = 'TIDAK LURUS';

		// if($check)
		// {
		// 	$stts_lurus = 'LURUS';
		// }

		$stts_lurus = 'LURUS';

		// DB::table('procurement_rfc_osp')->where([
		// 	['rfc', $req->rfc_id],
		// 	['id_upload', $id]
		// ])->delete();

		foreach($nama_rfc as $val)
		{
			$qty = 0;

			if($ld)
			{
				$find_k_material = array_search($val->material, array_column($ld, 'material') );

				if($find_k_material !== FALSE)
				{
					$qty = $ld[$find_k_material]['qty'];
				}
			}

			DB::table('procurement_rfc_osp')->insert([
				'id_upload'  => $id,
				'id_reserv'  => $val->no_reservasi,
				'rfc'        => $req->rfc_id,
				'rfc_lama'   => $rfc_old,
				'jenis_rfc'  => $val->type,
				'terpakai'   => $qty,
				'status_rfc' => $stts_lurus,
				'material'   => $val->material,
				'created_by' => session('auth')->id_user
			]);
		}

		$msg['direct'] = '/progress/'.$id;
		$msg['isi']['msg'] = [
			'type' => 'success',
			'text' => $msg_psn
		];

		return $msg;
	}

	public static function get_all_pdr()
	{
		return DB::Table('procurement_designator As pd')
		->leftjoin('procurement_design_rfc As pdr', 'pd.id', '=', 'pdr.id_design')
		->select('pdr.id_design', 'pdr.design_rfc', 'pd.id as id_design', 'pd.designator', 'pd.uraian', 'pd.material', 'pd.jasa', DB::RAW('0 As sp'), DB::RAW('0 As rekon'), 'pd.design_mitra_id', 'pd.jenis', DB::RAW("(CASE WHEN pd.design_mitra_id = 16 THEN 'Telkom Akses' ELSE 'Mitra' END) As namcomp"), 'pd.design_mitra_id As jenis_khs', 'pdr.free_use' )
		->where('pd.active', 1)
		->get();
	}

	public static function get_design_boq($id, $sd, $jns = 'one')
	{
		$sql = DB::table('procurement_boq_upload As pbu')
		->leftjoin('procurement_Boq_lokasi As pbl', 'pbu.id', '=', 'pbl.id_upload')
		->leftjoin('procurement_Boq_design As pbd', 'pbl.id', '=', 'pbd.id_boq_lokasi')
		->leftjoin('procurement_designator As pd', 'pd.id', '=', 'pbd.id_design')
		->leftjoin(DB::RAW("(SELECT id, sub_jenis, pekerjaan, khs FROM procurement_pekerjaan GROUP BY sub_jenis) ppe"), function($join){
			$join->on('pbu.pekerjaan', '=', 'ppe.pekerjaan')
			->on('pbu.jenis_work', '=', 'ppe.sub_jenis');
		})
		->where('pd.active', 1);

		switch ($jns) {
			case 'one':
				$sql->select('pbu.judul', 'pbu.mitra_nm', 'pbd.*', 'pd.jenis', 'pbd.jenis_khs As design_mitra_id', 'pbl.lokasi', 'pbd.id_boq_lokasi', 'pbl.sub_jenis_p', 'pbl.sto', 'pbl.new_boq', 'pd.uraian',
				DB::RAW("(CASE WHEN pbd.jenis_khs = 16 THEN 'Telkom Akses' ELSE 'Mitra' END) As namcomp"))
				->where([
					['pbu.id', $id],
				])
				->WhereIn('pbd.status_design', $sd)
				->groupBy('pbd.id');
			break;
			case 'all':
				$sql->select('pbu.id as id_pbu', 'pd.designator', DB::RAW("SUM(rekon) as jml_rekon"), 'pbd.rekon')
				->where([
					['pbu.id', $id],
					['pbd.status_design', 2]
				])
				->GroupBy('pbd.id');
			break;
			case 'dash_compare_rfc':
				$sql->select('pbu.judul', 'pbu.id as id_pbu', 'pbu.mitra_nm', 'pbd.*', 'pd.jenis', 'pbd.jenis_khs As design_mitra_id', 'pbl.lokasi', 'pbl.sub_jenis_p', 'pbl.sto', 'pbl.new_boq', 'pd.uraian',
				DB::RAW("(CASE WHEN pbd.jenis_khs = 16 THEN 'Telkom Akses' ELSE 'Mitra' END) As namcomp"),
				DB::RAW("SUM(rekon) as jml_rekon"),
				'pd.designator')
				->where([
					['pbu.active', 1],
					['pbd.status_design', 1],
					['pbu.step_id', '>', 8]
				])
				->WhereIn('pbd.status_design', $sd)
				->groupBy('pbd.id');
			break;
		}

		return $sql->get();
	}

	public static function get_rfc_boq($id, $jenis = 'all')
	{
		$sql = DB::table('procurement_rfc_osp As pro')
		->leftjoin('procurement_boq_upload As pbu', 'pbu.id', '=', 'pro.id_upload')
		->leftjoin('procurement_req_pid As prp', 'prp.id_upload', '=', 'pro.id_upload')
		->leftjoin('procurement_pid As pp', 'pp.id', '=', 'prp.id_pid_req')
		->leftJoin('inventory_material As im', function($join)
		{
			$join->on('im.no_rfc', '=', 'pro.rfc');
			$join->on('im.material', '=', 'pro.material');
		})
		->leftJoin('procurement_design_rfc As pdr', function($join)
		{
			$join->on('pdr.design_rfc', '=', 'pro.material');
			$join->on('pdr.pekerjaan', '=', 'pbu.pekerjaan');
		})
		->leftjoin('procurement_designator As pd', 'pd.id', '=', 'pdr.id_design')
		->leftjoin('1_2_employee As emp', 'emp.nik', '=', 'pro.created_by')
		->select('pbu.judul', 'pbu.id as id_pbu', 'pbu.mitra_nm', 'pro.*', 'im.wbs_element', 'im.no_reservasi', 'pro.id as id_pro', 'im.quantity', 'im.no_rfc', 'im.mitra', 'im.type', 'im.posting_datex', 'emp.nama', DB::RAW("(CASE WHEN pd.designator IS NULL THEN pro.material ELSE pd.designator END) As designator, (CASE WHEN pd.satuan IS NULL THEN '-' ELSE pd.satuan END) As satuan"))
		->where('pd.active', 1);

		if($id == 'all')
		{
			$sql->where([
				['pbu.active', 1],
				['pro.active', 1]
			])
			->WhereNotnull('im.wbs_element')
			->GroupBy('pro.id');
		}
		elseif($jenis == 'mitra')
		{
			$sql->where([
				['pbu.active', 1],
				['pro.active', 1],
				['pbu.mitra_nm', $id]
			])
			->WhereNotnull('im.wbs_element')
			->GroupBy('pro.id');
		}
		else
		{
			$sql->where([
				['prp.id_upload', $id],
				['pro.active', 1]
			])
			->WhereNotnull('im.wbs_element')
			->GroupBy('pro.id');
		}

		return $sql->OrderBy('pro.jenis_rfc', 'ASC')->get();
	}

	public static function get_pekerjaan($data, $jns = 'search')
  {
		$sql = DB::table('procurement_pekerjaan');
		switch ($jns) {
			case 'search':
				$sql->where('pekerjaan', $data);
				$sql = $sql->first();
			break;
			case 'sub_jenis_all_j':
				$sql->where('jenis', $data);
				$sql = $sql->get();
			break;
			case 'sub_jenis_all_sj':
				$sql->where('jenis', $data)->GroupBy('sub_jenis');
				$sql = $sql->get();
			break;
		}
    return $sql;
  }

	public static function check_downloadable($id)
	{
		return DB::table('procurement_rfc_osp As pro')
		->select('pro.*', 'im.no_reservasi',  'pro.id as id_pro', 'im.quantity', 'im.no_rfc', 'im.mitra', 'im.type', 'im.posting_datex', 'emp.nama')
		->leftjoin('procurement_req_pid As prp', 'prp.id_upload', '=', 'pro.id_upload')
		->leftjoin('procurement_pid As pp', 'pp.id', '=', 'prp.id_pid_req')
		->leftJoin('inventory_material As im', function($join)
			{
				$join->on('im.no_rfc', '=', 'pro.rfc');
				$join->on('im.material', '=', 'pro.material');
			})
		->leftjoin('1_2_employee As emp', 'emp.nik', '=', 'pro.created_by')
		->where([
			['pro.id_upload', $id],
			['pro.active', 1]
		])
		->WhereNotnull('im.wbs_element')
		->GroupBy('pro.id')
		->get();
	}

	public static function get_list_sp($id)
	{
		$sql = DB::table('procurement_boq_upload As pbu')
		->leftjoin('procurement_req_pid As prp', 'pbu.id', '=', 'prp.id_upload')
		->leftjoin('procurement_pid As pp', 'pp.id', '=', 'prp.id_pid_req')
		->leftjoin('procurement_mitra As pm', 'pm.id', '=', 'pbu.mitra_id')
		->select('pbu.*', 'pm.nama_company',
			DB::RAW("DATEDIFF(NOW(), pbu.tgl_last_update) as jml_hri"),
			DB::RAW("(SELECT nama FROM 1_2_employee WHERE nik = pbu.modified_by GROUP BY nik) As nama_modif")
		)->where([
			["pbu.active", 1],
			['pbu.step_id', $id]
		]);

		if(session('auth')->proc_level == 2 )
		{
			if(!in_array(session('auth')->id_user, [18940469, 20981020]))
			{
				$sql->where('pm.nama_company', session('auth')->mitra_amija_pt);
			}
		}

		if(session('auth')->proc_level == 44)
		{
			$find_witel = DB::Table('promise_witel As w1')
			->leftjoin('promise_witel As w2', 'w2.Regional', '=', 'w1.Regional')
			->where('w1.Witel', session('auth')->Witel_New)
			->get();

			foreach($find_witel as $vw)
			{
				$all_witel[] = $vw->Witel;
			}

			$sql->whereIn('pbu.witel', $all_witel);
		}
		elseif(session('auth')->proc_level != 99)
		{
			$sql->where('pbu.witel', session('auth')->Witel_New);
		}

		if(session('auth')->peker_pup)
		{
			$pekerjaan = explode(', ', session('auth')->peker_pup );
			$sql->whereIn('pbu.pekerjaan', $pekerjaan);
		}

		$sql->GroupBy('pbu.id')
		->orderBy('pbu.tgl_last_update', 'DESC')
		->orderBy('pbu.created_at', 'DESC');

		return $sql->get();
	}

	public static function convert_floor_ceil($tgl, $price)
	{
		$targetTimestamp = strtotime($tgl);
		$currentTimestamp = strtotime('2023-06-26');

		if ($currentTimestamp >= $targetTimestamp) {
			$data = ceil($price);
		} else {
			$data = floor($price);
		}

		return $data;
	}

	public static function create_zip($id)
	{
		$check_data = ReportModel::get_boq_data($id);
		$that = new \App\Http\Controllers\AdminController();
		$that_op = new \App\Http\Controllers\OperationController();
		$get_false = self::get_step_by_id($check_data->step_id);
		// dd($get_false);

		$that_op::download_boq_operation($id, 'save');

		if($get_false->action != 'forward')
		{
			$get_log = DB::table('procurement_rekon_log As prl')
			->leftjoin('procurement_step As ps', 'prl.step_id', '=', 'ps.id')
			->select('prl.*', 'ps.urutan')
			->where('id_upload', $id)
			->OrderBy('id', 'DESC')
			->get();

			$get_log = json_decode(json_encode($get_log), TRUE );
			// dd($get_log);
			foreach($get_log as $v)
			{
				if(!is_float($v['urutan']) )
				{
					$fn[] = $v;
				}
			}
			$check_data->step_id = $fn[0]['step_id'];
		}
		// dd($check_data->step_id);
		if($check_data->step_id >= 2 )
		{
			$that_op::download_justify($id, 'save');
		}

		if($check_data->step_id >= 5 )
		{
			self::download_doc('KESANGGUPAN', $id);
		}

		if($check_data->step_id >= 2)
		{
			$that->download_excel_ospfo($id, 'save');
		}

		if($check_data->bulan_pengerjaan >= '2021-11')
		{
			if($check_data->step_id >= 4)
			{
				self::download_doc('spen_2021', $id);
			}

			if($check_data->step_id >= 6)
			{
				self::download_doc('sp_2021', $id);
			}
		}

		if($check_data->step_id >= 10 )
		{
			if($check_data->bulan_pengerjaan >= '2021-11')
			{
				self::download_doc('BARM_2021', $id);

				if($check_data->BAUT)
				{
					self::download_doc('baut_2021', $id);
				}
				// dd($check_data->ba_abd);
				if($check_data->ba_abd)
				{
					self::download_doc('babd_2021', $id);
				}

				self::download_doc('bast_I_2021', $id);
				// self::download_doc('bast_II_2021', $id);

				self::download_doc('bal', $id);
				self::download_doc('ba_rek', $id);
			}
			else
			{
				self::download_doc('BARM', $id);
			}

			if($check_data->BAPP)
			{
				self::download_doc('BAPP', $id);
			}
			self::download_doc('FAKTUR', $id);
		}

		$check_path = public_path() . '/upload2/' . $id . '/dokumen_verifikasi/';
		$verifikasi_files = @preg_grep('~^\w.*$~', scandir($check_path) );

		if(count($verifikasi_files) != 0)
		{
			$files = array_values($verifikasi_files);

			foreach($files as $k => $v)
			{
				$path = public_path() . '/upload2/' . $id . '/dokumen_verifikasi/'. $v;

				if(file_exists($path) )
				{
					if (!file_exists(public_path() . '/upload2/' . $id . '/procurement/dokumen_verifikasi') ) {
						if (!mkdir(public_path() . '/upload2/' . $id . '/procurement/dokumen_verifikasi', 0770, true)) {
							return 'gagal menyiapkan folder ' . $v;
						}
					}
					\File::copy($path , (public_path() . '/upload2/' . $id . '/procurement/dokumen_verifikasi/'.$v) );
				}
			}
		}

		$rfc = [];

		$data_rfc = MitraModel::get_rfc_history($id);

		foreach($data_rfc as $kc1 => $valc1)
		{
			$rfc[$kc1]['rfc'] = $valc1->rfc;
		}

    foreach($rfc as $key => $val)
    {
      $check_path = '/mnt/hdd4/upload/storage/rfc_ttd/';
      $rfc_file = @preg_grep('~^'.$val['rfc'].'.*$~', scandir($check_path) );
			$path = '';
			$files = '';
			// if($files == '4902649916.pdf')
			// {
			// 	dd($rfc_file);
			// }
      if(count($rfc_file) != 0)
      {
        $files = array_values($rfc_file);
        $path = "/mnt/hdd4/upload/storage/rfc_ttd/$files[0]";
      }
      else
      {
        $check_path = public_path() . '/upload2/' . $id . '/dokumen_rfc_up/';
        $rfc_file = @preg_grep('~^'.$val['rfc'].'.*$~', scandir($check_path) );

        if(count($rfc_file) != 0)
        {
          $files = array_values($rfc_file);
          $path = public_path() . '/upload2/' . $id . '/dokumen_rfc_up/'.$files[0];
        }
      }

      if(file_exists($path) )
      {
				if (!file_exists(public_path() . '/upload2/' . $id . '/procurement/rfc')) {
					if (!mkdir(public_path() . '/upload2/' . $id . '/procurement/rfc', 0770, true)) {
						return 'gagal menyiapkan folder rfc';
					}
				}
        \File::copy($path , (public_path() . '/upload2/' . $id . '/procurement/rfc/'.$files[0]) );
      }
    }

    $path_npwp = public_path() . "/upload_mitra2/" . $check_data->mitra_id . "/";
		$npwp_file = @preg_grep('~^NPWP.*$~', scandir($path_npwp) );

		if(count($npwp_file) != 0)
		{
			$files_npwp = array_values($npwp_file);
			if (!file_exists(public_path() . '/upload2/' . $id . '/procurement/data_mitra/') )
			{
				if (!mkdir(public_path() . '/upload2/' . $id . '/procurement/data_mitra/', 0770, true) )
				{
					return 'gagal menyiapkan folder data_mitra';
				}
			}
			\File::copy( (public_path() . "/upload_mitra2/" . $check_data->mitra_id . "/" . $files_npwp[0]) , (public_path() . '/upload2/' . $id . '/procurement/data_mitra/'. $files_npwp[0]) );
		}

		\File::copyDirectory( (public_path() . '/upload2/' . $id) , (public_path() . '/upload2/' . $id . '_copyan_'));

		$rootPath      = public_path() . '/upload2/' . $id . '_copyan_';
		$namel         = str_replace('/', '-', "$check_data->judul");
		$new_name_file = $namel . '.zip';

		$zip = new ZipArchive();
		$zip->open($new_name_file, ZipArchive::CREATE | ZipArchive::OVERWRITE);

		$files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($rootPath) , RecursiveIteratorIterator::LEAVES_ONLY);

		foreach ($files as $name => $file)
		{
			if (!$file->isDir())
			{
				$filePath     = $file->getRealPath();
				$relativePath = substr($filePath, strlen($rootPath) + 1);
				$zip->addFile($filePath, $file->GetFileName());
			}
		}

		$zip->close();
		\File::deleteDirectory($rootPath);
		return $new_name_file;
	}

	public static function download_file_all_rar($id_boq_up)
	{
		$check_data = ReportModel::get_boq_data($id_boq_up);

		$all_data = DB::table('procurement_dok_ttd')
    ->where([
			['pekerjaan', $check_data->pekerjaan],
			['witel', $check_data->witel],
		])
		->GroupBy('rule_dok')
		->get();

		$class = [];

		foreach($all_data as $v)
		{
			$class[$v->rule_dok] = '';
		}

		$find_user = DB::table('procurement_dok_ttd As pdt')
		->leftjoin('procurement_user As ps', 'pdt.id_user', '=', 'ps.id')
		->where([
			['pdt.pekerjaan', $check_data->pekerjaan],
			['pdt.witel', $check_data->witel],
		])
		->get();

		$data = [];

		foreach($class as $k => $v)
		{
			$find_k = array_keys(array_column(json_decode(json_encode(@$find_user), TRUE ), 'rule_dok'), $k);

      $data[$k]['id'] = $k;

      if(!isset($data[$k]['avail']) )
      {
        $data[$k]['avail'] = 0;
        $data[$k]['expired'] = 0;
      }

      foreach($find_k as $vx)
      {
        $data_k = $find_user[$vx];

        if(date('Y-m-d', strtotime($data_k->tgl_start) ) <= date('Y-m-d') && date('Y-m-d', strtotime($data_k->tgl_end) ) >= date('Y-m-d') )
        {
          $data[$k]['avail'] += 1;
        }
        else
        {
          $data[$k]['expired'] += 1;
        }
      }
		}

    $find_avail = array_column($data, 'avail');

    // if(array_search(0, $find_avail) !== FALSE)
    // {
    //   return back()->with('alerts', [
    //     ['type' => 'warning', 'text' => "Dokumen tidak bisa diunduh karena terdeteksi user sudah melewati masa tenggang" ]
    //   ]);
    // }

		$delete_after_arr = [
			'S_PEN',
			'SP',
			'BAUT',
			'BABD',
			'BAST_I',
			'BAST_II',
			'ba_legal',
			'barek',
			'BAPP',
			'KESANGGUPAN_DOC',
			'Justif',
			'BARM',
			'FAKTUR',
			'excel_osp_FO',
			'ba_legal_2021',
			'sp_2021',
			's_pen_2021',
			'baut_2021',
			'bast_II_2021',
			'bast_I_2021',
			'barek_2021',
			'procurement/rfc',
			'procurement/data_mitra',
		];

		foreach ($delete_after_arr as $v) {
			register_shutdown_function('\File::deleteDirectory', public_path() .'/upload/' . $id_boq_up . '/'.$v);
		}

		$nama_file = self::create_zip($id_boq_up);

		$headers =['Content-type: application/zip'];

		$response = response()->download(public_path() .'/'. $nama_file, $nama_file, $headers);
		register_shutdown_function('unlink', public_path() .'/'. $nama_file);

		foreach ($delete_after_arr as $v) {
			register_shutdown_function('\File::deleteDirectory', public_path() .'/upload/' . $id_boq_up . '/'.$v);
		}

		return $response;
	}

	public static function download_zip_psb($version, $id)
	{
		$nama_file = self::create_zip_psb($version, $id);

		$headers = ['Content-type: application/zip'];

		$response = response()->download(public_path() . '/' . $nama_file, $nama_file, $headers);
		register_shutdown_function('unlink', public_path() . '/' . $nama_file);

		return $response;
	}

	public static function create_zip_psb($version, $id)
	{
		$time = round(microtime(true) * 1000);
		$that = new \App\Http\Controllers\AdminController();

		$data = self::download_filepsb('dokumen_pelengkap', $id);
		$judul = $data->mitra_nm.' (PSB PERIODE '.$data->bulan.' '.date('Y', strtotime($data->rekon_periode2)).')';

		switch ($version) {
			case 'khs_2021':
					\File::deleteDirectory(public_path().'/upload/'.$id.'/Justifikasi_dan_Rincian_SSL_(PSB)_'.str_replace(" ", "_", $data->mitra_nm).'.xlsx');
					$that->download_filepsb('justifikasi', $id, 'save');

					\File::deleteDirectory(public_path().'/upload/'.$id.'/Surat_Pesanan_PSB_'.str_replace(" ", "_", $data->mitra_nm).'.xlsx');
					$that->download_filepsb('surat_pesanan', $id, 'save');

					\File::deleteDirectory(public_path().'/upload/'.$id.'/Dokumen_Pelengkap_Lainnya_PSB_'.str_replace(" ", "_", $data->mitra_nm).'.xlsx');
					$that->download_filepsb('dokumen_pelengkap', $id, 'save');
				break;

			case 'beta_ho_v1':
					\File::deleteDirectory(public_path().'/upload/'.$id.'/DokPelengkap_Skema_OrderBased_'.str_replace(" ", "_", $data->mitra_nm).'.xlsx');
					$that->download_filepsb('dokpelengkap_skema_orderbased', $id, 'save');

					\File::deleteDirectory(public_path().'/upload/'.$id.'/Dok_Skema_OrderBased_'.str_replace(" ", "_", $data->mitra_nm).'.docx');
					self::word_skemaOrderBased($data, $version);
				break;

			case 'beta_ho_v2_manual':
					\File::deleteDirectory(public_path().'/upload/'.$id.'/DokPelengkap_Skema_OrderBasedxOrder'.str_replace(" ", "_", $data->mitra_nm).'.xlsx');
					$that->download_filepsb('dokpelengkap_skema_orderbasedxorder', $id, 'save');

					\File::deleteDirectory(public_path().'/upload/'.$id.'/Dok_Skema_OrderBased_'.str_replace(" ", "_", $data->mitra_nm).'.docx');
					self::word_skemaOrderBased($data, $version);
				break;

			case 'beta_kalimantan':
				\File::deleteDirectory(public_path().'/upload/'.$id.'/DokPelengkap_Skema_OrderBasedxKalimantan_'.str_replace(" ", "_", $data->mitra_nm).'.xlsx');
				$that->download_filepsb('dokpelengkap_skema_orderbasedxkalimantan', $id, 'save');

				\File::deleteDirectory(public_path().'/upload/'.$id.'/Dok_Skema_OrderBasedxKalimantan_'.str_replace(" ", "_", $data->mitra_nm).'.docx');
				self::word_skemaOrderBased($data, $version);
			break;
		}

		\File::copyDirectory(
			(public_path() . '/upload2/' . $id),
			(public_path() . '/upload2/' . $id . '_copyan_' . $time)
		);

		$rootPath = public_path() . '/upload2/' . $id . '_copyan_' . $time;
		$namel = str_replace('/', '-', "$judul");
		$new_name_file = $namel . '.zip';

		$zip = new ZipArchive();
		$zip->open($new_name_file, ZipArchive::CREATE | ZipArchive::OVERWRITE);

		$files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($rootPath), RecursiveIteratorIterator::LEAVES_ONLY);

		foreach ($files as $name => $file) {
			if (!$file->isDir()) {
				$filePath = $file->getRealPath();
				$relativePath = substr($filePath, strlen($rootPath) + 1);
				$zip->addFile($filePath, $file->GetFileName());
			}
		}
		$zip->close();
		\File::deleteDirectory($rootPath);
		return $new_name_file;
	}

	public static function save_data_s($req, $id)
	{
		$data = ReportModel::get_boq_data($id);

		DB::table('procurement_boq_upload')->where('id', $id)->update([
			'pks'             => $req->pks,
			'tgl_pks'         => $req->tgl_pks,
			'step_id'         => 4,
			'surat_penetapan' => $req->s_pen,
			'tgl_jatuh_tmp'   => $req->toc,
			'lokasi'          => $req->lokasi_project,
			'tgl_s_pen'       => $req->tgl_s_pen,
			'modified_by'     => session('auth')->id_user,
		]);

		$bulan = array (
			'01' =>'Januari',
			'02' =>'Februari',
			'03' =>'Maret',
			'04' =>'April',
			'05' =>'Mei',
			'06' =>'Juni',
			'07' =>'Juli',
			'08' =>'Agustus',
			'09' =>'September',
			'10' =>'Oktober',
			'11' =>'November',
			'12' =>'Desember'
		);

		$xp = explode('-', $req->tgl_pks);

		DB::Table('procurement_mitra')
		->where('id', $data->mitra_id)
		->update([
			'no_khs_maintenance'  => $req->pks,
			'tgl_khs_maintenance' => $xp[2] . ' '. $bulan[$xp[1] ] . ' ' . $xp[0]
		]);

		self::save_log(4, $id);

		$msg['direct'] = '/progressList/3';
		$msg['isi']['msg'] = [
			'type' => 'success',
			'text' => 'Dokumen Surat Penetapan berhasil Diolah!'
		];

		return $msg;
	}

  public static function download_doc($jenis, $id_boq)
  {
    //INI BIKIN TABLE DI DOC
		// dd($id_boq);
		$AdminModel  = new AdminModel();
		$data	       = self::get_data_final($id_boq);
		$pph_text    = $AdminModel->find_pph_text($data->tgl_bast);
		$mitra	     = AdminModel::get_mitra($data->mitra_id);
		$data_osp	   = reportmodel::get_boq_data($id_boq);
		$lokasi	     = MitraModel::get_lokasi($id_boq);
		$design	     = MitraModel::get_design($id_boq);
		$rfc	       = MitraModel::get_rfc_history($id_boq);
		$get_lok	   = DB::table('procurement_area')->where('witel', $data->witel)->first();

		$data_boq_rekon_raw = DB::SELECT("SELECT pbu.jenis_work, pbl.sub_jenis_p, pbd.*, pd.uraian, pd.satuan, pbd.material as material_default, pbd.jasa as jasa_default, pd.design_mitra_id, pbl.sto, pbl.lokasi, pbl.pid_sto, pm.nama_company
		FROM procurement_boq_upload pbu
		LEFT JOIN procurement_Boq_lokasi pbl ON pbu.id = pbl.id_upload
		LEFT JOIN procurement_Boq_design pbd ON pbl.id = pbd.id_boq_lokasi
		LEFT JOIN procurement_designator pd ON pd.id = pbd.id_design
		LEFT JOIN procurement_mitra pm ON pm.id = pbu.mitra_id
		WHERE pbu.id = ".$id_boq." AND pbd.status_design IN (0, 1) AND pd.active = 1 AND pbu.active = 1 GROUP BY pbd.id ORDER BY pbl.sub_jenis_p ASC, pbl.sto ASC, pbd.status_design ASC");

		$get_jenis_kerja = DB::table('procurement_pekerjaan')->where('jenis', $data_boq_rekon_raw[0]->jenis_work)->get();

		$order_sto = explode(', ', $data->sto_pekerjaan);
		$data_boq_rekon = [];

		// foreach($get_jenis_kerja as $gjk)
		// {
		// 	foreach($order_sto as $v)
		// 	{
		// 		$find_k = array_keys(array_column(json_decode(json_encode($data_boq_rekon_raw), 'TRUE'), 'sto'), $v);

		// 		foreach($find_k as $vv)
		// 		{
		// 			$dbr = $data_boq_rekon_raw[$vv];

		// 			if($gjk->sub_jenis == $dbr->sub_jenis_p)
		// 			{
		// 				$data_boq_rekon[] = $dbr;
		// 			}
		// 		}
		// 	}
		// }

		$data_boq_rekon = $data_boq_rekon_raw;

		$data_tot = [];

		$get_all_material = [];

		foreach($data_boq_rekon as $key => $val)
		{
			$get_all_material[$val->id_design .'-'. intval($val->material) .'-'. intval($val->jasa)]['id_design']  = $val->id_design;
			$get_all_material[$val->id_design .'-'. intval($val->material) .'-'. intval($val->jasa)]['jenis_khs']  = $val->design_mitra_id;
			$get_all_material[$val->id_design .'-'. intval($val->material) .'-'. intval($val->jasa)]['designator'] = $val->designator;
			$get_all_material[$val->id_design .'-'. intval($val->material) .'-'. intval($val->jasa)]['uraian']     = html_entity_decode($val->uraian, ENT_QUOTES, 'UTF-8');
			$get_all_material[$val->id_design .'-'. intval($val->material) .'-'. intval($val->jasa)]['satuan']     = $val->satuan;
			$get_all_material[$val->id_design .'-'. intval($val->material) .'-'. intval($val->jasa)]['material']   = intval($val->material);
			$get_all_material[$val->id_design .'-'. intval($val->material) .'-'. intval($val->jasa)]['jasa']       = intval($val->jasa);
			$get_all_material[$val->id_design .'-'. intval($val->material) .'-'. intval($val->jasa)]['sp']         = 0;
			$get_all_material[$val->id_design .'-'. intval($val->material) .'-'. intval($val->jasa)]['rekon']      = 0;
			$get_all_material[$val->id_design .'-'. intval($val->material) .'-'. intval($val->jasa)]['tambah']     = 0;
			$get_all_material[$val->id_design .'-'. intval($val->material) .'-'. intval($val->jasa)]['kurang']     = 0;
		}

		foreach($data_boq_rekon as $key => $val)
		{
			$data_bq['rekon'][$val->id_boq_lokasi]['lokasi'] = $val->lokasi;
			$data_bq['rekon'][$val->id_boq_lokasi]['sto']    = $val->sto;

			$split_pid_all = explode(', ', $val->pid_sto);
			$split_pid     = explode('-', $split_pid_all[0]);

			$data_bq['rekon'][$val->id_boq_lokasi]['pid_sto_lok'] = @$split_pid[0] .'-'. @$split_pid[1];

			$data_bq['rekon'][$val->id_boq_lokasi]['pid_sto_wbs'] = $val->pid_sto;

			$pid_lok = DB::Table('procurement_req_pid As prp')
			->leftjoin('procurement_pid As pp', 'pp.id', '=', 'prp.id_pid_req')
			->select('pp.pid')
			->where('id_lokasi', $val->id_boq_lokasi)
			->get();

			$pid_per_lok = [];

			foreach($pid_lok as $v)
			{
				$pid_per_lok[$v->pid] = $v->pid;
			}

			$data_bq['rekon'][$val->id_boq_lokasi]['pid_sto'] = implode(', ', $pid_per_lok);

			if(!isset($data_bq['rekon'][$val->id_boq_lokasi]['list']) )
			{
				$data_bq['rekon'][$val->id_boq_lokasi]['list']  = $get_all_material;
			}

			if($data_bq['rekon'][$val->id_boq_lokasi]['list'][$val->id_design .'-'. intval($val->material) .'-'. intval($val->jasa)])
			{
				$data_bq['rekon'][$val->id_boq_lokasi]['list'][$val->id_design .'-'. intval($val->material) .'-'. intval($val->jasa)]['sp']     = intval($val->sp);
				$data_bq['rekon'][$val->id_boq_lokasi]['list'][$val->id_design .'-'. intval($val->material) .'-'. intval($val->jasa)]['rekon']  = intval($val->rekon);
				$data_bq['rekon'][$val->id_boq_lokasi]['list'][$val->id_design .'-'. intval($val->material) .'-'. intval($val->jasa)]['tambah'] = intval($val->rekon) > intval($val->sp) ? intval($val->rekon) - intval($val->sp) : 0;
				$data_bq['rekon'][$val->id_boq_lokasi]['list'][$val->id_design .'-'. intval($val->material) .'-'. intval($val->jasa)]['kurang'] = intval($val->rekon) < intval($val->sp) ? intval($val->sp) - intval($val->rekon) : 0;
			}
		}

		if(@$data_bq['rekon'])
		{
			foreach($data_bq['rekon'] as $k => $v)
			{
				foreach($v['list'] as $kk => $vv)
				{
					$data_bq['rekon'][$k]['list'] = array_values($data_bq['rekon'][$k]['list']);
				}
			}

			foreach($data_bq['rekon'] as $k => $v)
			{
				foreach($v['list'] as $kk => $vv)
				{
					if(!isset($data_tot['rekon']['total']['material_sp']) )
					{
						$data_tot['rekon']['total']['material_sp']     = 0;
						$data_tot['rekon']['total']['jasa_sp']         = 0;
						$data_tot['rekon']['total']['material_rekon']  = 0;
						$data_tot['rekon']['total']['jasa_rekon']      = 0;
						$data_tot['rekon']['total']['material_tambah'] = 0;
						$data_tot['rekon']['total']['jasa_tambah']     = 0;
						$data_tot['rekon']['total']['material_kurang'] = 0;
						$data_tot['rekon']['total']['jasa_kurang']     = 0;
					}

					$data_tot['rekon']['total']['material_sp']     += intval($vv['material']) * intval($vv['sp']);
					$data_tot['rekon']['total']['jasa_sp']         += intval($vv['jasa']) * intval($vv['sp']);
					$data_tot['rekon']['total']['material_rekon']  += intval($vv['material']) * intval($vv['rekon']);
					$data_tot['rekon']['total']['jasa_rekon']      += intval($vv['jasa']) * intval($vv['rekon']);
					$data_tot['rekon']['total']['material_tambah'] += intval($vv['material']) * intval($vv['tambah']);
					$data_tot['rekon']['total']['jasa_tambah']     += intval($vv['jasa']) * intval($vv['tambah']);
					$data_tot['rekon']['total']['material_kurang'] += intval($vv['material']) * intval($vv['kurang']);
					$data_tot['rekon']['total']['jasa_kurang']     += intval($vv['jasa']) * intval($vv['kurang']);
				}
			}
		}

		$data_boq_sp = DB::SELECT("SELECT pbd.*, pd.uraian, pd.satuan, pbd.material as material_default, pbd.jasa as jasa_default, pbl.sto, pbl.lokasi, pbl.pid_sto
		FROM procurement_boq_upload pbu
		LEFT JOIN procurement_Boq_lokasi pbl ON pbu.id = pbl.id_upload
		LEFT JOIN procurement_Boq_design pbd ON pbl.id = pbd.id_boq_lokasi
		LEFT JOIN procurement_designator pd ON pd.id = pbd.id_design
		LEFT JOIN procurement_mitra pm ON pm.id = pbu.mitra_id
		WHERE pbu.id = ".$id_boq." AND pbd.status_design = 0 GROUP BY pbd.id");

		foreach($data_boq_sp as $key => $val)
		{
			if(!isset($data_tot['sp']['total']) )
			{
				$data_tot['sp']['total']['material_sp']    = 0;
				$data_tot['sp']['total']['jasa_sp']        = 0;
				$data_tot['sp']['total']['material_rekon'] = 0;
				$data_tot['sp']['total']['jasa_rekon']     = 0;
			}

			$data_tot['sp']['total']['material_sp']    += intval($val->material) * intval($val->sp);
			$data_tot['sp']['total']['jasa_sp']        += intval($val->jasa) * intval($val->sp);
			$data_tot['sp']['total']['material_rekon'] += intval($val->material) * intval($val->rekon);
			$data_tot['sp']['total']['jasa_rekon']     += intval($val->jasa) * intval($val->rekon);
		}

		$that = new \App\Http\Controllers\AdminController();

		$bulan_angk = array (
			'01' =>'Januari',
			'02' =>'Februari',
			'03' =>'Maret',
			'04' =>'April',
			'05' =>'Mei',
			'06' =>'Juni',
			'07' =>'Juli',
			'08' =>'Agustus',
			'09' =>'September',
			'10' =>'Oktober',
			'11' =>'November',
			'12' =>'Desember'
		);

    switch($jenis){
			case 'spen_2021':
				$template = public_path() . '/template_doc/maintenance_up_2021/2. Surat Penetapan.docx';
				$template_spen_2021 = new \PhpOffice\PhpWord\TemplateProcessor($template);

				$template_spen_2021->setValue('spen_no', htmlspecialchars($data->surat_penetapan));
				$template_spen_2021->setValue('spen_tgl_terbilang', htmlspecialchars($that->convert_month($data->tgl_s_pen) ) );

				$get_lok = DB::table('procurement_area')->where('witel', $data->witel)->first();

				$template_spen_2021->setValue('area', htmlspecialchars(ucwords($get_lok->area) ) );

				$pph_text_sp = $AdminModel->find_pph_text($data->tgl_s_pen);

				$template_spen_2021->setValue('pph_sp', htmlspecialchars($pph_text_sp));
				$template_spen_2021->setValue('judul', htmlspecialchars($data->judul ) );
				$template_spen_2021->setValue('pks_no', htmlspecialchars($data->pks ) );
				$template_spen_2021->setValue('pks_tgl_terbilang', htmlspecialchars($that->convert_month( ($data->tgl_pks) ) ) );

				$get_lokasi = DB::table('procurement_Boq_lokasi')->where('id_upload', $id_boq)->get();
				$new_lok = [];

				foreach($get_lokasi as $glok)
				{
					$new_lok[] = $glok->lokasi;
				}

				$template_spen_2021->setValue('lok_kerja', htmlspecialchars(implode(', ', $new_lok) ) );

				$tgl_start = $that->convert_month($data->tgl_sp ?? $data->tgl_s_pen);
				$tgl_end   = $that->convert_month( (date('d-m-Y', strtotime('+'. ($data->tgl_jatuh_tmp - 1). ' day', strtotime($data->tgl_sp ?? $data->tgl_s_pen ) ) ) ) );

				$template_spen_2021->setValue('tgl_spen_start', htmlspecialchars($tgl_start) );
				$template_spen_2021->setValue('tgl_spen_end', htmlspecialchars($tgl_end) );

				$template_spen_2021->setValue('nama_mitra', htmlspecialchars(preg_replace('/^PT/', 'PT.', $data->mitra_nm) ) );
				$template_spen_2021->setValue('sp_gd', htmlspecialchars(number_format($data->gd_sp, '0', '.', '.') ) );
				$template_spen_2021->setValue('sp_terbilang', htmlspecialchars(ucwords($that->terbilang(number_format($data->gd_sp, 0, '', '') ) . " rupiah" ) ) );

				$nilai_ttd = $data->gd_sp;

				if ($nilai_ttd >= 0 && $nilai_ttd <= 200000000)
				{
					$ttd = 'krglbh_dua_jt';
				} elseif ($nilai_ttd > 200000000 && $nilai_ttd <= 500000000) {
					$ttd = 'antr_dualima_jt';
				} else {
					$ttd = 'lbh_lima_jt';
				}

				$data_ttd = ReportModel::get_dokumen_id($ttd, $data->pekerjaan, $data->witel, $data->tgl_s_pen);
				$template_spen_2021->setValue('formula_ta_wakil_spen', htmlspecialchars($data_ttd->user) );
				$template_spen_2021->setValue('formula_ta_jabatan_spen', htmlspecialchars($data_ttd->jabatan) );

				//nama
				$file_name = 'template Surat Penetapan 2021.docx';

				$path =  public_path() . '/upload2/' . $id_boq . '/S_PEN';

				if (!file_exists($path)) {
					if (!mkdir($path, 0770, true)) {
						return 'gagal menyiapkan folder penetapan';
					}
				}

				$file =  $path .'/'. $file_name;

				$template_spen_2021->saveAS($file);
			break;
			case 'sp_2021':
				//AMD
				$template = public_path() . '/template_doc/maintenance_up_2021/3. Surat Pesanan.docx';
				$template_sp_2021 = new \PhpOffice\PhpWord\TemplateProcessor($template);

				$template_sp_2021->setValue('judul', htmlspecialchars($data->judul ) );
				$template_sp_2021->setValue('regional', htmlspecialchars('Kalimantan / 6' ) );

				$get_lok = DB::table('procurement_area')->where('witel', $data->witel)->first();

				$template_sp_2021->setValue('area', htmlspecialchars(ucwords($get_lok->area) ) );

				$pph_text_sp = $AdminModel->find_pph_text($data->tgl_sp);
				$split_pph = explode('%', $pph_text_sp);
				$terbilang_pph = $that->terbilang($split_pph[0]);
				$pph_sp = $AdminModel->find_pph($data->tgl_sp);
				// dd($terbilang_pph);
				$template_sp_2021->setValue('sp_no', htmlspecialchars($data->no_sp) );
				$template_sp_2021->setValue('sp_tgl', htmlspecialchars($that->convert_month($data->tgl_sp) ) );
				$template_sp_2021->setValue('mitra_nama', htmlspecialchars(preg_replace('/^PT/', 'PT.', $data->mitra_nm) ) );
				$template_sp_2021->setValue('pks_no', htmlspecialchars($data->pks ) );
				$template_sp_2021->setValue('pks_tgl', htmlspecialchars($that->convert_month( ($data->tgl_pks) ) ) );
				$template_sp_2021->setValue('spen_no', htmlspecialchars($data->surat_penetapan ) );
				$template_sp_2021->setValue('spen_tgl', htmlspecialchars($that->convert_month( ($data->tgl_s_pen) ) ) );
				$template_sp_2021->setValue('kesanggupan_no', htmlspecialchars($data->surat_kesanggupan ) );
				$template_sp_2021->setValue('kesanggupan_tgl_terbilang', htmlspecialchars($that->convert_month( ($data->tgl_surat_sanggup) ) ) );

				$sp_total = self::convert_floor_ceil($data->tgl_sp, $data->gd_sp * $pph_sp);

				$template_sp_2021->setValue('sp_total', htmlspecialchars(number_format($data->gd_sp + $sp_total, '0', '.', '.') ) );
				$template_sp_2021->setValue('sp_total_terbilang', htmlspecialchars(ucwords($that->terbilang(number_format($data->gd_sp + $sp_total, '0', '', '') ) . " rupiah" ) ) );
				$template_sp_2021->setValue('pph_sp', htmlspecialchars($pph_text_sp) );
				$template_sp_2021->setValue('pph_sp_terbilang', htmlspecialchars($terbilang_pph .' persen') );
				$template_sp_2021->setValue('sp_material', htmlspecialchars(number_format($data->total_material_sp, '0', '.', '.') ) );
				$template_sp_2021->setValue('sp_jasa', htmlspecialchars(number_format($data->total_jasa_sp, '0', '.', '.') ) );
				$template_sp_2021->setValue('sp_gd', htmlspecialchars(number_format($data->gd_sp, '0', '.', '.') ) );
				$template_sp_2021->setValue('sp_ppn', htmlspecialchars(number_format($sp_total, '0', '.', '.') ) );
				$template_sp_2021->setValue('sp_toc', htmlspecialchars(number_format($data->toc, '0', '.', '.') ) );
				$template_sp_2021->setValue('sp_toc_terbilang', htmlspecialchars($that->terbilang($data->toc) ) );

				$tgl_end = $that->convert_month( (date('d-m-Y', strtotime('+'. ($data->tgl_jatuh_tmp - 1). ' day', strtotime($data->tgl_sp ) ) ) ) );
				$tgl_start = $that->convert_month($data->tgl_sp);

				$template_sp_2021->setValue('sp_tgl_awal_terbilang', htmlspecialchars($tgl_start) );
				$template_sp_2021->setValue('sp_tgl_akhir_terbilang', htmlspecialchars($tgl_end) );

				$nilai_ttd = $data->gd_sp;

				if ($nilai_ttd >= 0 && $nilai_ttd <= 200000000)
				{
					$ttd = 'krglbh_dua_jt';
				} elseif ($nilai_ttd > 200000000 && $nilai_ttd <= 500000000) {
					$ttd = 'antr_dualima_jt';
				} else {
					$ttd = 'lbh_lima_jt';
				}

				$data_ttd = ReportModel::get_dokumen_id($ttd, $data->pekerjaan, $data->witel, $data->tgl_sp);

				$template_sp_2021->setValue('formula_ta_wakil_sp', htmlspecialchars($data_ttd->user) );
				$template_sp_2021->setValue('formula_ta_jabatan_sp', htmlspecialchars($data_ttd->jabatan) );

				$template_sp_2021->setValue('mitra_wakil', htmlspecialchars($mitra->wakil_mitra) );
				$template_sp_2021->setValue('mitra_jabatan', htmlspecialchars($mitra->jabatan_mitra) );

				//nama
				$file_name = 'template Surat Pesanan 2021.docx';

				$path =  public_path() . '/upload2/' . $id_boq . '/SP';

				if (!file_exists($path)) {
					if (!mkdir($path, 0770, true)) {
						return 'gagal menyiapkan folder sp';
					}
				}

				$file =  $path .'/'. $file_name;

				$template_sp_2021->saveAS($file);
			break;
			case 'baut_2021':
				//AMD
				$template = public_path() . '/template_doc/maintenance_up_2021/4. Berita Acara Uji Terima (BAUT).docx';
				$template_baut_2021 = new \PhpOffice\PhpWord\TemplateProcessor($template);
				$template_baut_2021->setValue('baut_no', htmlspecialchars($data->BAUT ) );
				$template_baut_2021->setValue('pks_no', htmlspecialchars($data->pks ) );
				$template_baut_2021->setValue('pks_tgl_terbilang', htmlspecialchars($that->convert_month( ($data->tgl_pks) ) ) );
				$template_baut_2021->setValue('sp_no', htmlspecialchars($data->no_sp) );
				$template_baut_2021->setValue('sp_tgl_terbilang', htmlspecialchars($that->convert_month($data->tgl_sp) ) );
				$template_baut_2021->setValue('judul', htmlspecialchars($data->judul ) );
				$template_baut_2021->setValue('mitra_nama', htmlspecialchars(preg_replace('/^PT/', 'PT.', $data->mitra_nm) ) );
				$template_baut_2021->setValue('regional', htmlspecialchars('Kalimantan / 6' ) );

				$get_lok = DB::table('procurement_area')->where('witel', $data->witel)->first();
				$template_baut_2021->setValue('area', htmlspecialchars(ucwords($get_lok->area) ) );
				$template_baut_2021->setValue('witel', htmlspecialchars(strtoupper($get_lok->witel) ) );

				$pid_lok = DB::Table('procurement_req_pid As prp')
				->leftjoin('procurement_pid As pp', 'pp.id', '=', 'prp.id_pid_req')
				->select('pp.pid')
				->where('id_upload', $id_boq)
				->get();

				$pid_per_lok = [];

				foreach($pid_lok as $v)
				{
					$pid_per_lok[] = $v->pid;
				}

				$id_project = implode('</w:t><w:br/><w:t>', $pid_per_lok);
				$new_line   = new \PhpOffice\PhpWord\Element\PreserveText('<w:br/>');

				$template_baut_2021->setValue('pid_sap', $id_project);
				// $template_baut_2021->setComplexValue('newline', $new_line);

				$template_baut_2021->setValue('baut_tgl', htmlspecialchars($that->convert_month(date('Y-m-d', strtotime($data->tgl_baut) ) ) ) );
				// dd(($that->convert_month(date('Y-m-d', strtotime($data->tgl_baut) ) ) ) );
				$template_baut_2021->setValue('baut_hari', htmlspecialchars ($that->only_day(date('Y-m-d', strtotime($data->tgl_baut) ) ) ) );
				$template_baut_2021->setValue('baut_hari_numerik_terbilang', htmlspecialchars ($that->terbilang(date('d', strtotime($data->tgl_baut) ) ) ) );
				$template_baut_2021->setValue('baut_bulan_terbilang', htmlspecialchars ($bulan_angk[date('m', strtotime($data->tgl_baut) )] ) );
				$template_baut_2021->setValue('baut_tahun_terbilang', htmlspecialchars ($that->terbilang(date('Y', strtotime($data->tgl_baut) ) ) ) );
				$template_baut_2021->setValue('baut_d', htmlspecialchars (date('d', strtotime($data->tgl_baut) ) ) );
				$template_baut_2021->setValue('baut_m', htmlspecialchars (date('m', strtotime($data->tgl_baut) ) ) );
				$template_baut_2021->setValue('baut_Y', htmlspecialchars (date('Y', strtotime($data->tgl_baut) ) ) );
				$data_waspang = ReportModel::get_proc_user($data->waspang_id);

				if($data_waspang)
				{
					$template_baut_2021->setValue('waspang_wakil', htmlspecialchars($data_waspang->user) );
					$template_baut_2021->setValue('waspang_jabatan', htmlspecialchars($data_waspang->jabatan) );
				}
				else
				{
					$template_baut_2021->setValue('waspang_wakil', htmlspecialchars('WASPANG') );
					$template_baut_2021->setValue('waspang_jabatan', htmlspecialchars('NIK') );
				}

				$template_baut_2021->setValue('mitra_wakil', htmlspecialchars($mitra->wakil_mitra));
				$template_baut_2021->setValue('mitra_jabatan', htmlspecialchars($mitra->jabatan_mitra));

				$get_data = ReportModel::get_dokumen_id('mngr_krj', $data->pekerjaan, $data->witel, $data->tgl_baut);

				$template_baut_2021->setValue('mngr_pekerjaan_wakil_baut', htmlspecialchars($get_data->user) );
				$template_baut_2021->setValue('mngr_pekerjaan_jabatan_baut', htmlspecialchars($get_data->jabatan) );


				//nama
				$file_name = 'template BAUT 2021.docx';

				$path =  public_path() . '/upload2/' . $id_boq . '/BAUT';

				if (!file_exists($path)) {
					if (!mkdir($path, 0770, true)) {
						return 'gagal menyiapkan folder baut';
					}
				}

				$file =  $path .'/'. $file_name;

				$template_baut_2021->saveAS($file);
			break;
			case 'babd_2021':
				//AMD
				$template = public_path() . '/template_doc/maintenance_up_2021/5. Berita Acara Serah Terima Gambar Akhir Pelaksanaan Pekerjaan (As-Built Drawing).docx';
				$template_ba_abd_2021 = new \PhpOffice\PhpWord\TemplateProcessor($template);
				$template_ba_abd_2021->setValue('babd_no', htmlspecialchars($data->ba_abd ) );
				$template_ba_abd_2021->setValue('pks_no', htmlspecialchars($data->pks ) );
				$template_ba_abd_2021->setValue('pks_tgl_terbilang', htmlspecialchars($that->convert_month( ($data->tgl_pks) ) ) );
				$template_ba_abd_2021->setValue('sp_no', htmlspecialchars($data->no_sp) );
				$template_ba_abd_2021->setValue('sp_tgl_terbilang', htmlspecialchars($that->convert_month($data->tgl_sp) ) );
				$template_ba_abd_2021->setValue('judul', htmlspecialchars($data->judul ) );
				$template_ba_abd_2021->setValue('mitra_nama', htmlspecialchars(preg_replace('/^PT/', 'PT.', $data->mitra_nm) ) );
				$template_ba_abd_2021->setValue('regional', htmlspecialchars('Kalimantan / 6' ) );

				$get_lok = DB::table('procurement_area')->where('witel', $data->witel)->first();
				$template_ba_abd_2021->setValue('area', htmlspecialchars(ucwords($get_lok->area) ) );

				$template_ba_abd_2021->setValue('mitra_wakil', htmlspecialchars($mitra->wakil_mitra));
				$template_ba_abd_2021->setValue('mitra_jabatan', htmlspecialchars($mitra->jabatan_mitra));

				$data_ttd = ReportModel::get_dokumen_id('mngr_krj', $data->pekerjaan, $data->witel, $data->tgl_ba_abd);

				$template_ba_abd_2021->setValue('mngr_pekerjaan_wakil_abd', htmlspecialchars($data_ttd->user) );
				$template_ba_abd_2021->setValue('mngr_pekerjaan_jabatan_abd', htmlspecialchars($data_ttd->jabatan) );

				$template_ba_abd_2021->setValue('babd_hari', htmlspecialchars ($that->only_day(date('Y-m-d', strtotime($data->tgl_ba_abd) ) ) ) );
				$template_ba_abd_2021->setValue('babd_hari_numerik_terbilang', htmlspecialchars ($that->terbilang(date('d', strtotime($data->tgl_ba_abd) ) ) ) );
				$template_ba_abd_2021->setValue('babd_bulan_terbilang', htmlspecialchars ($bulan_angk[date('m', strtotime($data->tgl_ba_abd) )] ) );
				$template_ba_abd_2021->setValue('babd_tahun_terbilang', htmlspecialchars ($that->terbilang(date('Y', strtotime($data->tgl_ba_abd) ) ) ) );
				$template_ba_abd_2021->setValue('babd_d', htmlspecialchars (date('d', strtotime($data->tgl_ba_abd) ) ) );
				$template_ba_abd_2021->setValue('babd_m', htmlspecialchars (date('m', strtotime($data->tgl_ba_abd) ) ) );
				$template_ba_abd_2021->setValue('babd_Y', htmlspecialchars (date('Y', strtotime($data->tgl_ba_abd) ) ) );
				// $template_ba_abd_2021->setValue('lok_kerja', htmlspecialchars ($data->lokasi_pekerjaan ) );

				$data_waspang = ReportModel::get_proc_user($data->waspang_id);

				$user_waspang = 'WASPANG';
				$jabatan_waspang  = 'JABATAN';

				if($data_waspang)
				{
					$user_waspang = $data_waspang->user;
					$jabatan_waspang = $data_waspang->jabatan;
				}

				$template_ba_abd_2021->setValue('nama_w', htmlspecialchars($user_waspang) );
				$template_ba_abd_2021->setValue('jabatan_w', htmlspecialchars($jabatan_waspang) );

				$arr = [
					'borderSize'  => 1,
					'borderColor' => 'none',
					'width'       => 9200,
					'unit'        => \PhpOffice\PhpWord\Style\Table::WIDTH_PERCENT,
					'width'       => 100 * 50,
					'alignment'   => \PhpOffice\PhpWord\SimpleType\JcTable::CENTER,
				];

				$tor = new \PhpOffice\PhpWord\Element\Table($arr);
				$fancyTableFontStyle = array('bold' => true);
				$fancyTableCellStyle = array('valign' => 'center');
				//table object rekonsiliasi
				$tor->addRow(900);
				$tor->addCell(100, $fancyTableCellStyle)->addText('No', $fancyTableFontStyle);
				$tor->addCell(2000, $fancyTableCellStyle)->addText('LOKASI', $fancyTableFontStyle);
				$tor->addCell(2000, $fancyTableCellStyle)->addText('STO', $fancyTableFontStyle);

				$no_tor = 0;

				$lok_rev = [];

				// foreach($lokasi as $v)
				// {
				// 	$lok_rev[$v->pid_sto]['lokasi'] = trim($v->lokasi);
				// 	$lok_rev[$v->pid_sto]['sto'][] = trim($v->sto);
				// }

				// foreach($lok_rev as $v)
				// {
				// 	$tor->addRow();
				// 	$tor->addCell(100)->addText(++$no_tor);
				// 	$tor->addCell(2000)->addText(ucwords($v['lokasi']) );
				// 	$tor->addCell(2000)->addText(implode(', ', $v['sto']) );
				// }

				foreach($lokasi as $k_lok => $v)
				{
					$lok_rev[$k_lok]['lokasi'] = trim($v->lokasi);
					$lok_rev[$k_lok]['sto'] = trim($v->sto);
				}

				foreach($lok_rev as $v)
				{
					$tor->addRow();
					$tor->addCell(100)->addText(++$no_tor);
					$tor->addCell(2000)->addText(ucwords($v['lokasi']) );
					$tor->addCell(2000)->addText(ucwords($v['sto']) );
				}

				$template_ba_abd_2021->setComplexBlock('{table_lokasi}', $tor);

				//nama
				$file_name = 'template BA ABD 2021.docx';

				$path =  public_path() . '/upload2/' . $id_boq . '/BABD';

				if (!file_exists($path)) {
					if (!mkdir($path, 0770, true)) {
						return 'gagal menyiapkan folder ba abd';
					}
				}

				$file =  $path .'/'. $file_name;

				$template_ba_abd_2021->saveAS($file);
			break;
			case 'bast_I_2021':
				$template = public_path() . '/template_doc/maintenance_up_2021/10. Berita Acara Serah Terima Pertama (BAST-I).docx';
				$template_bast_i_2021 = new \PhpOffice\PhpWord\TemplateProcessor($template);
				$template_bast_i_2021->setValue('BAST_no', htmlspecialchars($data->BAST ) );
				$template_bast_i_2021->setValue('pks_no', htmlspecialchars($data->pks ) );
				$template_bast_i_2021->setValue('pks_tgl_terbilang', htmlspecialchars($that->convert_month( ($data->tgl_pks) ) ) );
				$template_bast_i_2021->setValue('amd_1_pks_no', htmlspecialchars($mitra->no_amandemen1 ) );
				$template_bast_i_2021->setValue('amd_1_pks_tgl_terbilang', htmlspecialchars($that->convert_month( ($mitra->date_amandemen1) ) ) );
				$template_bast_i_2021->setValue('sp_no', htmlspecialchars($data->no_sp) );
				$template_bast_i_2021->setValue('sp_tgl_terbilang', htmlspecialchars($that->convert_month($data->tgl_sp) ) );
				$template_bast_i_2021->setValue('amd_sp_no', htmlspecialchars($data->amd_sp ?? '-') );
				$template_bast_i_2021->setValue('amd_sp_tgl_terbilang', htmlspecialchars($data->tgl_amd_sp ? $that->convert_month($data->tgl_amd_sp) : '-' ) );
				$template_bast_i_2021->setValue('judul', htmlspecialchars($data->judul ) );
				$template_bast_i_2021->setValue('mitra_nama', htmlspecialchars(preg_replace('/^PT/', 'PT.', $data->mitra_nm) ) );
				$template_bast_i_2021->setValue('regional', htmlspecialchars('Kalimantan / 6' ) );

				$get_lok = DB::table('procurement_area')->where('witel', $data->witel)->first();
				$template_bast_i_2021->setValue('area', htmlspecialchars(ucwords($get_lok->area) ) );

				$pid_lok = DB::Table('procurement_req_pid As prp')
				->leftjoin('procurement_pid As pp', 'pp.id', '=', 'prp.id_pid_req')
				->select('pp.pid')
				->where('id_upload', $id_boq)
				->get();

				$pid_per_lok = [];

				foreach($pid_lok as $v)
				{
					$pid_per_lok[] = $v->pid;
				}

				$id_project = implode('</w:t><w:br/><w:t>', $pid_per_lok);
				$new_line   = new \PhpOffice\PhpWord\Element\PreserveText('<w:br/>');
				$template_bast_i_2021->setValue('pid_sap', $id_project);
				// $template_bast_i_2021->setComplexValue('newline_baru', $new_line);

				$nilai_ttd = $data->gd_rekon;

				if ($nilai_ttd >= 0 && $nilai_ttd <= 200000000)
				{
					$ttd = 'krglbh_dua_jt';
				} elseif ($nilai_ttd > 200000000 && $nilai_ttd <= 500000000) {
					$ttd = 'antr_dualima_jt';
				} else {
					$ttd = 'lbh_lima_jt';
				}

				$data_ttd = ReportModel::get_dokumen_id($ttd, $data->pekerjaan, $data->witel, $data->tgl_bast);

				$template_bast_i_2021->setValue('formula_ta_wakil_rekon_bast', htmlspecialchars($data_ttd->user) );
				$template_bast_i_2021->setValue('formula_ta_jabatan_rekon_bast', htmlspecialchars($data_ttd->jabatan) );

				$template_bast_i_2021->setValue('mitra_wakil', htmlspecialchars($mitra->wakil_mitra) );
				$template_bast_i_2021->setValue('mitra_jabatan', htmlspecialchars($mitra->jabatan_mitra) );

				$template_bast_i_2021->setValue('bast_hari', htmlspecialchars ($that->only_day(date('Y-m-d', strtotime($data->tgl_bast) ) ) ) );
				$template_bast_i_2021->setValue('bast_hari_numerik_terbilang', htmlspecialchars ($that->terbilang(date('d', strtotime($data->tgl_bast) ) ) ) );
				$template_bast_i_2021->setValue('bast_bulan_terbilang', htmlspecialchars ($bulan_angk[date('m', strtotime($data->tgl_bast) )] ) );
				$template_bast_i_2021->setValue('bast_tahun_terbilang', htmlspecialchars ($that->terbilang(date('Y', strtotime($data->tgl_bast) ) ) ) );
				$template_bast_i_2021->setValue('bast_d', htmlspecialchars (date('d', strtotime($data->tgl_bast) ) ) );
				$template_bast_i_2021->setValue('bast_m', htmlspecialchars (date('m', strtotime($data->tgl_bast) ) ) );
				$template_bast_i_2021->setValue('bast_Y', htmlspecialchars (date('Y', strtotime($data->tgl_bast) ) ) );

				$template_bast_i_2021->setValue('rekon_gd', htmlspecialchars(number_format($data->gd_rekon, '0', '.', '.') ) );
				$template_bast_i_2021->setValue('rekon_gd_terbilang', htmlspecialchars(ucwords($that->terbilang(number_format($data->gd_rekon, 0, '', '') ) . " rupiah" ) ) );
				$template_bast_i_2021->setValue('pph', htmlspecialchars($pph_text) );

				$pph = $AdminModel->find_pph($data->tgl_bast);
				$rekon_total = self::convert_floor_ceil($data->tgl_bast, $data->gd_rekon + ($data->gd_rekon * $pph) );

				$template_bast_i_2021->setValue('rekon_total', htmlspecialchars(number_format($rekon_total, '0', '.', '.') ) );
				$template_bast_i_2021->setValue('rekon_total_terbilang', htmlspecialchars(ucwords($that->terbilang(number_format($rekon_total, '0', '', '') ) . " rupiah" ) ) );

				//nama
				$file_name = 'template BAST-I 2021.docx';

				$path =  public_path() . '/upload2/' . $id_boq . '/BAST_I';

				if (!file_exists($path)) {
					if (!mkdir($path, 0770, true)) {
						return 'gagal menyiapkan folder BAST I';
					}
				}

				$file =  $path .'/'. $file_name;

				$template_bast_i_2021->saveAS($file);
			break;
			case 'bast_II_2021':
				//AMD
				$template = public_path() . '/template_doc/maintenance_up_2021/11. Berita Acara Serah Terima Kedua (BAST-II).docx';
				$template_bast_ii_2021 = new \PhpOffice\PhpWord\TemplateProcessor($template);
				$template_bast_ii_2021->setValue('bast_no', htmlspecialchars($data->BAST ) );
				$template_bast_ii_2021->setValue('pks_no', htmlspecialchars($data->pks ) );
				$template_bast_ii_2021->setValue('pks_tgl_terbilang', htmlspecialchars($that->convert_month( ($data->tgl_pks) ) ) );
				$template_bast_ii_2021->setValue('sp_no', htmlspecialchars($data->no_sp) );
				$template_bast_ii_2021->setValue('sp_tgl_terbilang', htmlspecialchars($that->convert_month($data->tgl_sp) ) );
				$template_bast_ii_2021->setValue('judul', htmlspecialchars($data->judul ) );
				$template_bast_ii_2021->setValue('mitra_nama', htmlspecialchars(preg_replace('/^PT/', 'PT.', $data->mitra_nm) ) );
				$template_bast_ii_2021->setValue('regional', htmlspecialchars('Kalimantan / 6' ) );

				$get_lok = DB::table('procurement_area')->where('witel', $data->witel)->first();
				$template_bast_ii_2021->setValue('area', htmlspecialchars(ucwords($get_lok->area) ) );

				$pid_lok = DB::Table('procurement_req_pid As prp')
				->leftjoin('procurement_pid As pp', 'pp.id', '=', 'prp.id_pid_req')
				->select('pp.pid')
				->where('id_upload', $id_boq)
				->get();

				$pid_per_lok = [];

				foreach($pid_lok as $v)
				{
					$pid_per_lok[] = $v->pid;
				}

				$id_project = implode('</w:t><w:br/><w:t>', $pid_per_lok);
				$new_line   = new \PhpOffice\PhpWord\Element\PreserveText('<w:br/>');

				$template_bast_ii_2021->setValue('pid_sap', $id_project);
				// $template_bast_ii_2021->setComplexValue('newline', $new_line);

				$nilai_ttd = $data->gd_rekon;

				if ($nilai_ttd >= 0 && $nilai_ttd <= 200000000)
				{
					$ttd = 'krglbh_dua_jt';
				} elseif ($nilai_ttd > 200000000 && $nilai_ttd <= 500000000) {
					$ttd = 'antr_dualima_jt';
				} else {
					$ttd = 'lbh_lima_jt';
				}

				$data_ttd = ReportModel::get_dokumen_id($ttd, $data->pekerjaan, $data->witel, $data->tgl_bast);

				$template_bast_ii_2021->setValue('formula_ta_wakil_rekon_bast', htmlspecialchars($data_ttd->user) );
				$template_bast_ii_2021->setValue('formula_ta_jabatan_rekon_bast', htmlspecialchars($data_ttd->jabatan) );

				$template_bast_ii_2021->setValue('mitra_wakil', htmlspecialchars($mitra->wakil_mitra) );
				$template_bast_ii_2021->setValue('mitra_jabatan', htmlspecialchars($mitra->jabatan_mitra) );

        $template_bast_ii_2021->setValue('bast_hari', htmlspecialchars ($that->only_day(date('Y-m-d', strtotime($data->tgl_bast) ) ) ) );
				$template_bast_ii_2021->setValue('bast_hari_numerik_terbilang', htmlspecialchars ($that->terbilang(date('d', strtotime($data->tgl_bast) ) ) ) );
				$template_bast_ii_2021->setValue('bast_bulan_terbilang', htmlspecialchars ($bulan_angk[date('m', strtotime($data->tgl_bast) )] ) );
				$template_bast_ii_2021->setValue('bast_tahun_terbilang', htmlspecialchars ($that->terbilang(date('Y', strtotime($data->tgl_bast) ) ) ) );
				$template_bast_ii_2021->setValue('bast_d', htmlspecialchars (date('d', strtotime($data->tgl_bast) ) ) );
				$template_bast_ii_2021->setValue('bast_m', htmlspecialchars (date('m', strtotime($data->tgl_bast) ) ) );
				$template_bast_ii_2021->setValue('bast_Y', htmlspecialchars (date('Y', strtotime($data->tgl_bast) ) ) );

				$template_bast_ii_2021->setValue('bast_tgl', htmlspecialchars(date('d-m-Y', strtotime($data->tgl_bast) ) ) );

				//nama
				$file_name = 'template BAST-II 2021.docx';

				$path =  public_path() . '/upload2/' . $id_boq . '/BAST_II';

				if (!file_exists($path)) {
					if (!mkdir($path, 0770, true)) {
						return 'gagal menyiapkan folder BAST II';
					}
				}

				$file =  $path .'/'. $file_name;

				$template_bast_ii_2021->saveAS($file);
			break;
			case 'bal':
				//AMD
				$template = public_path() . '/template_doc/maintenance_up_2021/9. 16 BA Legalitas.docx';
				$template_bal_2021 = new \PhpOffice\PhpWord\TemplateProcessor($template);
				$template_bal_2021->setValue('pks_no', htmlspecialchars($data->pks ) );
				$template_bal_2021->setValue('pks_tgl_terbilang', htmlspecialchars($that->convert_month( ($data->tgl_pks) ) ) );
				$template_bal_2021->setValue('sp_no', htmlspecialchars($data->no_sp) );
				$template_bal_2021->setValue('sp_tgl_terbilang', htmlspecialchars($that->convert_month($data->tgl_sp) ) );
				$template_bal_2021->setValue('judul', htmlspecialchars($data->judul ) );
				$template_bal_2021->setValue('mitra_nama', htmlspecialchars(preg_replace('/^PT/', 'PT.', $data->mitra_nm) ) );
				$template_bal_2021->setValue('regional', htmlspecialchars('Kalimantan / 6' ) );

				$get_lok = DB::table('procurement_area')->where('witel', $data->witel)->first();
				$template_bal_2021->setValue('area', htmlspecialchars(ucwords($get_lok->area) ) );

				$template_bal_2021->setValue('barek_hari', htmlspecialchars ($that->only_day(date('Y-m-d', strtotime($data->tgl_ba_rekon) ) ) ) );
				$template_bal_2021->setValue('barek_hari_numerik_terbilang', htmlspecialchars ($that->terbilang(date('d', strtotime($data->tgl_ba_rekon) ) ) ) );
				$template_bal_2021->setValue('barek_bulan_terbilang', htmlspecialchars ($bulan_angk[date('m', strtotime($data->tgl_ba_rekon) )] ) );
				$template_bal_2021->setValue('barek_tahun_terbilang', htmlspecialchars ($that->terbilang(date('Y', strtotime($data->tgl_ba_rekon) ) ) ) );
				$template_bal_2021->setValue('barek_d', htmlspecialchars (date('d', strtotime($data->tgl_ba_rekon) ) ) );
				$template_bal_2021->setValue('barek_m', htmlspecialchars (date('m', strtotime($data->tgl_ba_rekon) ) ) );
				$template_bal_2021->setValue('barek_Y', htmlspecialchars (date('Y', strtotime($data->tgl_ba_rekon) ) ) );
				$template_bal_2021->setValue('lokasi_witel', htmlspecialchars($get_lok->witel) );

				$data_ttd = ReportModel::get_dokumen_id('mngr_krj', $data->pekerjaan, $data->witel, $data->tgl_ba_rekon);
				$project_nama = $data_ttd->user;
				$project_jab = $data_ttd->jabatan;

				$template_bal_2021->setValue('mngr_pekerjaan_wakil_rekon', htmlspecialchars($project_nama) );
				$template_bal_2021->setValue('mngr_pekerjaan_jabatan_rekon', htmlspecialchars($project_jab) );

				$template_bal_2021->setValue('mitra_wakil', htmlspecialchars($mitra->wakil_mitra) );
				$template_bal_2021->setValue('mitra_jabatan', htmlspecialchars($mitra->jabatan_mitra) );

				//nama
				$file_name = 'template BA Legalitas 2021.docx';

				$path =  public_path() . '/upload2/' . $id_boq . '/ba_legal';

				if (!file_exists($path)) {
					if (!mkdir($path, 0770, true)) {
						return 'gagal menyiapkan folder BA Legalitas';
					}
				}

				$file =  $path .'/'. $file_name;

				$template_bal_2021->saveAS($file);
			break;
			case 'ba_rek':
				$template = public_path() . '/template_doc/maintenance_up_2021/6. BA Rekonsiliasi Pekerjaan.docx';
				$template_barek_2021 = new \PhpOffice\PhpWord\TemplateProcessor($template);
				$template_barek_2021->setValue('bar_no', htmlspecialchars($data->ba_rekon ) );
				$template_barek_2021->setValue('pks_no', htmlspecialchars($data->pks ) );
				$template_barek_2021->setValue('pks_tgl_terbilang', htmlspecialchars($that->convert_month( ($data->tgl_pks) ) ) );
				$template_barek_2021->setValue('sp_no', htmlspecialchars($data->no_sp) );
				$template_barek_2021->setValue('sp_tgl_terbilang', htmlspecialchars($that->convert_month($data->tgl_sp) ) );
				$template_barek_2021->setValue('judul', htmlspecialchars($data->judul ) );
				$template_barek_2021->setValue('mitra_nama', htmlspecialchars(preg_replace('/^PT/', 'PT.', $data->mitra_nm) ) );
				$template_barek_2021->setValue('regional', htmlspecialchars('Kalimantan / 6' ) );

				$get_lok = DB::table('procurement_area')->where('witel', $data->witel)->first();
				$template_barek_2021->setValue('area', htmlspecialchars(ucwords($get_lok->area) ) );

				$template_barek_2021->setValue('barek_hari', htmlspecialchars ($that->only_day(date('Y-m-d', strtotime($data->tgl_ba_rekon) ) ) ) );
				$template_barek_2021->setValue('barek_hari_numerik_terbilang', htmlspecialchars ($that->terbilang(date('d', strtotime($data->tgl_ba_rekon) ) ) ) );
				$template_barek_2021->setValue('barek_bulan_terbilang', htmlspecialchars ($bulan_angk[date('m', strtotime($data->tgl_ba_rekon) )] ) );
				$template_barek_2021->setValue('barek_tahun_terbilang', htmlspecialchars ($that->terbilang(date('Y', strtotime($data->tgl_ba_rekon) ) ) ) );
				$template_barek_2021->setValue('barek_d', htmlspecialchars (date('d', strtotime($data->tgl_ba_rekon) ) ) );
				$template_barek_2021->setValue('barek_m', htmlspecialchars (date('m', strtotime($data->tgl_ba_rekon) ) ) );
				$template_barek_2021->setValue('barek_Y', htmlspecialchars (date('Y', strtotime($data->tgl_ba_rekon) ) ) );
				$template_barek_2021->setValue('lokasi_area', htmlspecialchars($get_lok->area) );

				$arr = [
					'borderSize'  => 1,
					'borderColor' => 'none',
					'width'       => 9200,
					'unit'        => \PhpOffice\PhpWord\Style\Table::WIDTH_PERCENT,
					'width'       => 100 * 50,
					'alignment'   => \PhpOffice\PhpWord\SimpleType\JcTable::CENTER,
				];

				$tor = new \PhpOffice\PhpWord\Element\Table($arr);
				$fancyTableFontStyle = array('bold' => true);
				$fancyTableCellStyle = array('valign' => 'center');
				//table object rekonsiliasi
				$tor->addRow(900);
				$tor->addCell(100, $fancyTableCellStyle)->addText('No', $fancyTableFontStyle);
				$tor->addCell(2000, $fancyTableCellStyle)->addText('LOKASI', $fancyTableFontStyle);
				$tor->addCell(2000, $fancyTableCellStyle)->addText('STO', $fancyTableFontStyle);
				$tor->addCell(2000, $fancyTableCellStyle)->addText('KETERANGAN', $fancyTableFontStyle);

				$no_tor = 0;

				$lok_rev = [];

				// foreach($lokasi as $v)
				// {
				// 	$lok_rev[$v->pid_sto]['lokasi'] = trim($v->lokasi);
				// 	$lok_rev[$v->pid_sto]['sto'][] = trim($v->sto);
				// }

				// foreach($lok_rev as $v)
				// {
				// 	$tor->addRow();
				// 	$tor->addCell(100)->addText(++$no_tor);
				// 	$tor->addCell(2000)->addText(ucwords($v['lokasi']) );
				// 	$tor->addCell(2000)->addText(implode(', ', $v['sto']) );
				// }

				foreach($lokasi as $k_lok => $v)
				{
					$lok_rev[$k_lok]['lokasi'] = trim($v->lokasi);
					$lok_rev[$k_lok]['sto'] = trim($v->sto);
					$lok_rev[$k_lok]['ket'] = trim($v->sub_jenis_p);
				}

				foreach($lok_rev as $v)
				{
					$tor->addRow();
					$tor->addCell(100)->addText(++$no_tor);
					$tor->addCell(2000)->addText(ucwords($v['lokasi']) );
					$tor->addCell(2000)->addText(ucwords($v['sto']) );
					$tor->addCell(2000)->addText(ucwords($v['ket']) );
				}

				$template_barek_2021->setComplexBlock('{table_lokasi}', $tor);

				$pph           = $AdminModel->find_pph($data->tgl_ba_rekon);
				$pph_text      = $AdminModel->find_pph_text($data->tgl_ba_rekon);
				$split_pph     = explode('%', $pph_text);
				$terbilang_pph = $that->terbilang($split_pph[0]);
				$pph_text_sp   = $AdminModel->find_pph_text($data->tgl_sp);
				$pph_sp        = $AdminModel->find_pph($data->tgl_sp);

				$rekon_total = self::convert_floor_ceil($data->tgl_ba_rekon, $data->gd_rekon + ($data->gd_rekon * $pph) );

				$template_barek_2021->setValue('rekon_total', htmlspecialchars(number_format($rekon_total, '0', '.', '.') ) );
				$template_barek_2021->setValue('rekon_total_terbilang', htmlspecialchars(ucwords($that->terbilang(number_format($rekon_total, '0', '', '') ) . " rupiah" ) ) );
				$template_barek_2021->setValue('pph', htmlspecialchars($pph_text) );
				// $template_barek_2021->setValue('pph', htmlspecialchars('') );
				$template_barek_2021->setValue('pph_terbilang', '('. htmlspecialchars($terbilang_pph) . ' persen)');
				// $template_barek_2021->setValue('pph_terbilang', htmlspecialchars('') );

				$template_barek_2021->setValue('sp_material', htmlspecialchars(number_format($data->total_material_sp, '0', '.', '.') ) );
				$template_barek_2021->setValue('sp_jasa', htmlspecialchars(number_format($data->total_jasa_sp, '0', '.', '.') ) );
				$template_barek_2021->setValue('sp_gd', htmlspecialchars(number_format($data->gd_sp, '0', '.', '.') ) );
				$template_barek_2021->setValue('pph_sp', htmlspecialchars($pph_text_sp) );

				$template_barek_2021->setValue('sp_gd_ppn', htmlspecialchars(number_format(self::convert_floor_ceil($data->tgl_ba_rekon, $data->gd_ppn_sp), '0', '.', '.') ) );
				$template_barek_2021->setValue('sp_total', htmlspecialchars(number_format(self::convert_floor_ceil($data->tgl_ba_rekon, $data->total_sp), '0', '.', '.') ) );

				$template_barek_2021->setValue('material_tambah', htmlspecialchars(number_format($data_tot['rekon']['total']['material_tambah'], '0', '.', '.') ) );
				$template_barek_2021->setValue('jasa_tambah', htmlspecialchars(number_format($data_tot['rekon']['total']['jasa_tambah'], '0', '.', '.') ) );

				$sum_material_jasa_tambah = $data_tot['rekon']['total']['material_tambah'] + $data_tot['rekon']['total']['jasa_tambah'];

				$template_barek_2021->setValue('tambah_gd', htmlspecialchars(number_format( ($sum_material_jasa_tambah), '0', '.', '.') ) );

				$ppn_tambah = self::convert_floor_ceil($data->tgl_ba_rekon, $sum_material_jasa_tambah * $pph);

				$template_barek_2021->setValue('tambah_ppn', htmlspecialchars(number_format($ppn_tambah, '0', '.', '.') ) );
				$template_barek_2021->setValue('tambah_total', htmlspecialchars(number_format($sum_material_jasa_tambah + $ppn_tambah, '0', '.', '.') ) );

				$template_barek_2021->setValue('kurang_material', htmlspecialchars(number_format($data_tot['rekon']['total']['material_kurang'], '0', '.', '.') ) );
				$template_barek_2021->setValue('kurang_jasa', htmlspecialchars(number_format($data_tot['rekon']['total']['jasa_kurang'], '0', '.', '.') ) );

				$sum_material_jasa_kurang = $data_tot['rekon']['total']['material_kurang'] + $data_tot['rekon']['total']['jasa_kurang'];

				$template_barek_2021->setValue('kurang_gd', htmlspecialchars(number_format( ($sum_material_jasa_kurang), '0', '.', '.') ) );

				$pph_kurang = self::convert_floor_ceil($data->tgl_ba_rekon, $sum_material_jasa_kurang * $pph);

				$template_barek_2021->setValue('kurang_ppn', htmlspecialchars(number_format($pph_kurang, '0', '.', '.') ) );
				$template_barek_2021->setValue('kurang_total', htmlspecialchars(number_format($sum_material_jasa_kurang + $pph_kurang, '0', '.', '.') ) );

				$template_barek_2021->setValue('rekon_material', htmlspecialchars(number_format($data->total_material_rekon, '0', '.', '.') ) );
				$template_barek_2021->setValue('rekon_jasa', htmlspecialchars(number_format($data->total_jasa_rekon, '0', '.', '.') ) );
				$template_barek_2021->setValue('rekon_gd', htmlspecialchars(number_format($data->gd_rekon, '0', '.', '.') ) );
				$template_barek_2021->setValue('rekon_gd_ppn', htmlspecialchars(number_format(self::convert_floor_ceil($data->tgl_ba_rekon, $data->gd_rekon * $pph), '0', '.', '.') ) );

				$harga_lok = new \PhpOffice\PhpWord\Element\Table($arr);
				$cellRowSpan = array('vMerge' => 'restart', 'valign' => 'center');
				$cellRowContinue = array('vMerge' => 'continue');
				$cellColSpan = array('gridSpan' => 2);
				$cellColSpan3 = array('gridSpan' => 3);

				$harga_lok->addRow();
				$harga_lok->addCell(2000, $cellRowSpan)->addText('Nomor');
				$harga_lok->addCell(2000, $cellRowSpan)->addText('Lokasi Pekerjaan');
				$harga_lok->addCell(2000, $cellRowSpan)->addText('STO');
				$harga_lok->addCell(4000, $cellColSpan)->addText('Harga');
				$harga_lok->addCell(2000, $cellRowSpan)->addText('Total');

				$harga_lok->addRow();
				$harga_lok->addCell(null, $cellRowContinue);
				$harga_lok->addCell(null, $cellRowContinue);
				$harga_lok->addCell(null, $cellRowContinue);
				$harga_lok->addCell(2000)->addText('Barang/Material');
				$harga_lok->addCell(2000)->addText('Jasa');
				$harga_lok->addCell(null, $cellRowContinue);

				$gd_material = $gd_jasa = 0;
				// dd($lokasi);
				$lok_rev = [];

				// foreach($lokasi as $v)
				// {
				// 	$lok_rev[$v->pid_sto]['lokasi'] = trim($v->lokasi);
				// 	$lok_rev[$v->pid_sto]['sto'][trim($v->sto)] = trim($v->sto);

				// 	if(!isset($lok_rev[$v->pid_sto]['total_material_rekon']) )
				// 	{
				// 		$lok_rev[$v->pid_sto]['total_material_rekon'] = 0;
				// 		$lok_rev[$v->pid_sto]['total_jasa_rekon'] = 0;
				// 		$lok_rev[$v->pid_sto]['sum_me'] = 0;
				// 	}

				// 	$lok_rev[$v->pid_sto]['total_material_rekon'] += $v->material_rek;
				// 	$lok_rev[$v->pid_sto]['total_jasa_rekon'] += $v->jasa_rek;
				// 	$lok_rev[$v->pid_sto]['sum_me'] += $v->material_rek + $v->jasa_rek;
				// }

				foreach($lokasi as $k_lok => $v)
				{
					$lok_rev[$k_lok]['lokasi'] = trim($v->lokasi);
					$lok_rev[$k_lok]['sto'][trim($v->sto)] = trim($v->sto);

					if(!isset($lok_rev[$k_lok]['total_material_rekon']) )
					{
						$lok_rev[$k_lok]['total_material_rekon'] = 0;
						$lok_rev[$k_lok]['total_jasa_rekon'] = 0;
						$lok_rev[$k_lok]['sum_me'] = 0;
					}

					$lok_rev[$k_lok]['total_material_rekon'] += $v->material_rek;
					$lok_rev[$k_lok]['total_jasa_rekon'] += $v->jasa_rek;
					$lok_rev[$k_lok]['sum_me'] += $v->material_rek + $v->jasa_rek;
				}

				$no_1 = 0;

				foreach($lok_rev as $k => $v)
				{
					$harga_lok->addRow();
					$harga_lok->addCell(100)->addText(++$no_1);
					$harga_lok->addCell(2000)->addText(ucwords($v['lokasi']) );
					$harga_lok->addCell(2000)->addText(implode(', ', $v['sto']) );
					$harga_lok->addCell(2000)->addText(number_format($v['total_material_rekon'], '0', '.', '.') );
					$harga_lok->addCell(2000)->addText(number_format($v['total_jasa_rekon'], '0', '.', '.') );
					$harga_lok->addCell(2000)->addText(number_format($v['sum_me'], '0', '.', '.') );

					$gd_material += $v['total_material_rekon'];
					$gd_jasa += $v['total_jasa_rekon'];
				}

				$harga_lok->addRow();
				$harga_lok->addCell(2000, $cellColSpan3)->addText('Barang/Material');
				$harga_lok->addCell(2000, $cellColSpan3)->addText(number_format($gd_material, '0', '.', '.') );

				$harga_lok->addRow();
				$harga_lok->addCell(2000, $cellColSpan3)->addText('Jasa');
				$harga_lok->addCell(2000, $cellColSpan3)->addText(number_format($gd_jasa, '0', '.', '.') );

				$harga_lok->addRow();
				$harga_lok->addCell(2000, $cellColSpan3)->addText('Total');
				$harga_lok->addCell(2000, $cellColSpan3)->addText(number_format($gd_material + $gd_jasa, '0', '.', '.') );

				$harga_lok->addRow();
				$harga_lok->addCell(2000, $cellColSpan3)->addText('PPN '.$pph_text);

				$pph_material_jasa = self::convert_floor_ceil($data->tgl_ba_rekon, ($gd_material + $gd_jasa) * $pph);

				$harga_lok->addCell(2000, $cellColSpan3)->addText( number_format($pph_material_jasa, '0', '.', '.') );

				$harga_lok->addRow();
				$harga_lok->addCell(2000, $cellColSpan3)->addText('Grand Total');

				$harga_lok->addCell(2000, $cellColSpan3)->addText( number_format(($gd_material + $gd_jasa) + $pph_material_jasa , '0', '.', '.') );

				$template_barek_2021->setComplexBlock('{harga_lokasi}', $harga_lok);

				$jwp = new \PhpOffice\PhpWord\Element\Table($arr);
				$fancyTableFontStyle = array('bold' => true);
				$fancyTableCellStyle = array('valign' => 'center');
				//table object rekonsiliasi
				$jwp->addRow(900);
				$jwp->addCell(100, $fancyTableCellStyle)->addText('No', $fancyTableFontStyle);
				$jwp->addCell(2000, $fancyTableCellStyle)->addText('LOKASI', $fancyTableFontStyle);
				$jwp->addCell(2000, $fancyTableCellStyle)->addText('STO', $fancyTableFontStyle);
				$jwp->addCell(2000, $fancyTableCellStyle)->addText('TOC', $fancyTableFontStyle);
				$jwp->addCell(2000, $fancyTableCellStyle)->addText('REALISASI UJI TERIMA (BAUT)', $fancyTableFontStyle);
				$jwp->addCell(2000, $fancyTableCellStyle)->addText('KETERLAMBATAN', $fancyTableFontStyle);

				$no_jwp = 0;

				$lok_rev = [];

				// foreach($lokasi as $v)
				// {
				// 	$lok_rev[$v->pid_sto]['lokasi'] = trim($v->lokasi);
				// 	$lok_rev[$v->pid_sto]['sto'][] = trim($v->sto);
				// }

				foreach($lokasi as $k_lok => $v)
				{
					$lok_rev[$k_lok]['lokasi'] = trim($v->lokasi);
					$lok_rev[$k_lok]['sto'] = trim($v->sto);
				}

				foreach($lok_rev as $v)
				{
					$jwp->addRow();
					$jwp->addCell(100)->addText(++$no_jwp);
					$jwp->addCell(2000)->addText(ucwords($v['lokasi']) );
					$jwp->addCell(2000)->addText(ucwords($v['sto']) );

					$toc_tgl = date('Y-m-d');

					if($data->tgl_jatuh_tmp)
					{
						// $toc_tgl = $tgl_ex[0] .'-'. $tgl_ex[1] . '-' . ($data->tgl_jatuh_tmp);
						$toc_tgl = date('Y-m-d', strtotime('+'. ($data->tgl_jatuh_tmp - 1). ' day', strtotime($data->tgl_sp ?? $data->tgl_s_pen ) ) );
					}
					// dd($toc_tgl);
					$jwp->addCell(2000)->addText($that->convert_month( (date('d-m-Y', strtotime($toc_tgl) ) ) ) );
					$jwp->addCell(2000)->addText($that->convert_month( (date('d-m-Y', strtotime($data->tgl_baut) ) ) ) );
					$jwp->addCell(2000)->addText('-');
				}

				foreach($lokasi as $k_lok => $v)
				{
					$lok_rev[$k_lok]['lokasi'] = trim($v->lokasi);
					$lok_rev[$k_lok]['sto'] = trim($v->sto);
				}

				foreach($lok_rev as $v)
				{
					$tor->addRow();
					$tor->addCell(100)->addText(++$no_tor);
					$tor->addCell(2000)->addText(ucwords($v['lokasi']) );
					$tor->addCell(2000)->addText(ucwords($v['sto']) );

					$toc_tgl = date('Y-m-d');

					if($data->tgl_jatuh_tmp)
					{
						// $toc_tgl = $tgl_ex[0] .'-'. $tgl_ex[1] . '-' . ($data->tgl_jatuh_tmp);
						$toc_tgl = date('Y-m-d', strtotime('+'. ($data->tgl_jatuh_tmp - 1). ' day', strtotime($data->tgl_sp ?? $data->tgl_s_pen ) ) );
					}
					// dd($toc_tgl);
					$tor->addCell(2000)->addText($that->convert_month( (date('d-m-Y', strtotime($toc_tgl) ) ) ) );
					$tor->addCell(2000)->addText($that->convert_month( (date('d-m-Y', strtotime($data->tgl_baut) ) ) ) );
					$tor->addCell(2000)->addText('-');
				}

				$data_ttd = ReportModel::get_dokumen_id('mngr_krj', $data->pekerjaan, $data->witel, $data->tgl_ba_rekon);
				// $data_ttd = ReportModel::get_dokumen_id('wh', $data->pekerjaan, $data->witel, $data->tgl_ba_rekon);
				$project_nama = $data_ttd->user;
				$project_jab = $data_ttd->jabatan;

				$template_barek_2021->setValue('mngr_pekerjaan_wakil_rekon', htmlspecialchars($project_nama) );
				$template_barek_2021->setValue('mngr_pekerjaan_jabatan_rekon', htmlspecialchars($project_jab) );

				$template_barek_2021->setValue('mitra_wakil', htmlspecialchars($mitra->wakil_mitra) );
				$template_barek_2021->setValue('mitra_jabatan', htmlspecialchars($mitra->jabatan_mitra) );

				$template_barek_2021->setComplexBlock('{table_jangka_waktu}', $jwp);

				$data_ttd = ReportModel::get_dokumen_id('gm', $data->pekerjaan, $data->witel, $data->tgl_ba_rekon);

				$template_barek_2021->setValue('gm_wakil_rekon', htmlspecialchars($data_ttd->user) );
				$template_barek_2021->setValue('gm_jabatan_rekon', htmlspecialchars($data_ttd->jabatan) );

				//nama
				$file_name = 'template BA Rekon 2021.docx';

				$path =  public_path() . '/upload2/' . $id_boq . '/barek';

				if (!file_exists($path)) {
					if (!mkdir($path, 0770, true)) {
						return 'gagal menyiapkan folder BA Rekon';
					}
				}

				$file =  $path .'/'. $file_name;

				$template_barek_2021->saveAS($file);
			break;
			case 'KESANGGUPAN':
				$template = public_path() . '/template_doc/maintenance_down_2021/template Kesanggupan.docx';
				$template_sanggup = new \PhpOffice\PhpWord\TemplateProcessor($template);

				$template_sanggup->setValue('mitra_nama', htmlspecialchars(preg_replace('/^PT/', 'PT.', $data->mitra_nm) ));
				$template_sanggup->setValue('mitra_alamat', htmlspecialchars($data->alamat_company));
				$template_sanggup->setValue('mitra_kontak', htmlspecialchars($data->telp));
				$template_sanggup->setValue('kesanggupan_no', htmlspecialchars($data->surat_kesanggupan));
				$template_sanggup->setValue('kesanggupan_tgl', htmlspecialchars(ucwords($get_lok->area).", ". $that->convert_month(date("d-m-Y", strtotime($data->tgl_surat_sanggup) ) ) ) );

				$data_ta = AdminModel::get_mitra(16);
				$nilai_ttd = $data->gd_sp;

				if ($nilai_ttd >= 0 && $nilai_ttd <= 200000000)
				{
					$ttd = 'krglbh_dua_jt';
				} elseif ($nilai_ttd > 200000000 && $nilai_ttd <= 500000000) {
					$ttd = 'antr_dualima_jt';
				} else {
					$ttd = 'lbh_lima_jt';
				}

				$data_ttd = ReportModel::get_dokumen_id($ttd, $data->pekerjaan, $data->witel, $data->tgl_s_pen);

				$template_sanggup->setValue('formula_ta_jabatan_spen', htmlspecialchars($data_ttd->jabatan) );

				$template_sanggup->setValue('nama_ta', htmlspecialchars(preg_replace('/^PT/', 'PT.', $data_ta->nama_company) ));
				$template_sanggup->setValue('alamat_ta', htmlspecialchars($data_ta->alamat_company));
				//  dd($data->tgl_s_pen);
				$template_sanggup->setValue('spen_no', htmlspecialchars("$data->surat_penetapan") );
				$template_sanggup->setValue('spen_tgl_terbilang', htmlspecialchars($that->convert_month(date("d-m-Y", strtotime($data->tgl_s_pen) ) ) ) );
				$template_sanggup->setValue('mitra_wakil', htmlspecialchars($mitra->wakil_mitra));
				$template_sanggup->setValue('mitra_jabatan', htmlspecialchars($mitra->jabatan_mitra));
				$template_sanggup->setValue('mitra_alamat', htmlspecialchars($mitra->alamat_company));
				$template_sanggup->setValue('judul', htmlspecialchars($data->judul));
				$template_sanggup->setValue('sp_gd', htmlspecialchars(number_format($data->gd_sp, 0, '.', '.') .",- (". ucwords($that->terbilang(number_format($data->gd_sp, 0, '', '') ) ." rupiah)") ) );

				$pph_text_sp = $AdminModel->find_pph_text($data->tgl_s_pen);
				$split_pph = explode('%', $pph_text_sp);
				$terbilang_pph = $that->terbilang($split_pph[0]);

				$template_sanggup->setValue('pph_sp', htmlspecialchars($pph_text_sp) );
				$template_sanggup->setValue('pph_sp_terbilang', htmlspecialchars($terbilang_pph .' persen') );
				//nama
				$file_name = 'template Kesanggupan.docx';

				$path =  public_path() . '/upload2/' . $id_boq . '/KESANGGUPAN_DOC';

				if (!file_exists($path)) {
					if (!mkdir($path, 0770, true)) {
						return 'gagal menyiapkan folder sanggup';
					}
				}

				$file =  $path .'/'. $file_name;
				$template_sanggup->saveAS($file);
			break;
      case 'BAPP':
         //BAPP
        $template = public_path() . '/template_doc/maintenance_down_2021/template BAPP.docx';
        $template_bapp = new \PhpOffice\PhpWord\TemplateProcessor($template);
				$template_bapp->setValue('bapp_no', htmlspecialchars($data->BAPP) );
        $template_bapp->setValue('judul', htmlspecialchars($data->judul));
        $template_bapp->setValue('mitra_nama', htmlspecialchars(preg_replace('/^PT/', 'PT.', $data->mitra_nm) ));
        $template_bapp->setValue('pks_no', htmlspecialchars($data->pks ) );
        $template_bapp->setValue('pks_tgl_terbilang', htmlspecialchars($that->convert_month(date("d-m-Y", strtotime($data->tgl_pks) ) ) ) );
        $template_bapp->setValue('sp_no', htmlspecialchars($data->no_sp) );
        $template_bapp->setValue('sp_tgl_terbilang', htmlspecialchars($that->convert_month(date("d-m-Y", strtotime($data->tgl_sp) ) ) ) );
        $template_bapp->setValue('mitra_wakil', htmlspecialchars($mitra->wakil_mitra) );
        $template_bapp->setValue('mitra_jabatan', htmlspecialchars($mitra->jabatan_mitra) );

				$nilai_ttd = ($data->gd_rekon == 0 ? $data->gd_sp : $data->gd_rekon);

				if ($nilai_ttd >= 0 && $nilai_ttd <= 200000000)
				{
					$ttd = 'krglbh_dua_jt';
				} elseif ($nilai_ttd > 200000000 && $nilai_ttd <= 500000000) {
					$ttd = 'antr_dualima_jt';
				} else {
					$ttd = 'lbh_lima_jt';
				}

				$data_ttd = ReportModel::get_dokumen_id($ttd, $data->pekerjaan, $data->witel, $data->tgl_bapp);

				$template_bapp->setValue('formula_ta_wakil_rekon_bapp', htmlspecialchars($data_ttd->user) );
        $template_bapp->setValue('formula_ta_jabatan_rekon_bapp', htmlspecialchars($data_ttd->jabatan) );

				$template_bapp->setValue('barek_hari', htmlspecialchars ($that->only_day(date('Y-m-d', strtotime($data->tgl_bapp) ) ) ) );
				$template_bapp->setValue('barek_hari_numerik_terbilang', htmlspecialchars ($that->terbilang(date('d', strtotime($data->tgl_bapp) ) ) ) );
				$template_bapp->setValue('barek_bulan_terbilang', htmlspecialchars ($bulan_angk[date('m', strtotime($data->tgl_bapp) )] ) );
				$template_bapp->setValue('barek_tahun_terbilang', htmlspecialchars ($that->terbilang(date('Y', strtotime($data->tgl_bapp) ) ) ) );
				$template_bapp->setValue('barek_tgl_terbilang', htmlspecialchars ($that->convert_month(date('Y-m-d', strtotime($data->tgl_bapp) ) ) ) );

				$pph_text_sp = $AdminModel->find_pph_text($data->tgl_sp);
				$pph = $AdminModel->find_pph($data->tgl_bapp);

        $template_bapp->setValue('pph', htmlspecialchars($pph_text) );

        $template_bapp->setValue('sp_gd', htmlspecialchars('Rp. '. number_format($data->gd_sp, '0', '.', '.') ) );
        $template_bapp->setValue('sp_total', htmlspecialchars('Rp. '. number_format(self::convert_floor_ceil($data->tgl_bapp, $data->gd_sp + ($data->gd_sp * $pph_text_sp) ), '0', '.', '.') ) );
        $template_bapp->setValue('rekon_gd', htmlspecialchars('Rp. '. number_format($data->gd_rekon, '0', '.', '.') ) );
        $template_bapp->setValue('rekon_gd_terbilang', htmlspecialchars(ucwords($that->terbilang(number_format($data->gd_rekon, 0, '', '') ) ) ) );
        $template_bapp->setValue('rekon_total', htmlspecialchars('Rp. '. number_format(self::convert_floor_ceil($data->tgl_bapp, $data->gd_rekon + ($data->gd_rekon * $pph) ) ), '0', '.', '.') );

        //nama
        $file_name ='template BAPP.docx';
				$path =  public_path() . '/upload2/' . $id_boq . '/BAPP';

				if (!file_exists($path)) {
					if (!mkdir($path, 0770, true)) {
						return 'gagal menyiapkan folder BAPP';
					}
				}

        $file = $path .'/'. $file_name;
        $template_bapp->saveAS($file);
      break;
      case 'BARM':
				$template = public_path() . '/template_doc/maintenance_down_2021/template BARM.docx';

				$template_barm = new \PhpOffice\PhpWord\TemplateProcessor($template);

				$data_ttd_ss = ReportModel::get_dokumen_id('mngr_ss', $data->pekerjaan, $data->witel, $data->tgl_ba_rekon);

				$template_barm->setValue('mgr_ss_barm', htmlspecialchars($data_ttd_ss->user));
				$template_barm->setValue('nik_mgr_ss_barm', htmlspecialchars($data_ttd_ss->nik));

				if($data_ttd_ss->reg == 6)
				{
					$template_barm->setValue('area_ss', htmlspecialchars('Banjarmasin') );
				}

				$data_ttd_ic = ReportModel::get_dokumen_id('invt_cntrl', $data->pekerjaan, $data->witel, $data->tgl_ba_rekon);

				$template_barm->setValue('nama_ic_barm', htmlspecialchars($data_ttd_ic->user));
				$template_barm->setValue('nik_ic_barm', htmlspecialchars($data_ttd_ic->nik));

				if($data_ttd_ic->reg == 6)
				{
					$template_barm->setValue('area_ss', htmlspecialchars('Banjarmasin') );
				}

				$data_waspang = ReportModel::get_proc_user($data->waspang_id);

				$user_waspang = 'WASPANG';
				$nik_waspang  = 'NIK';

				if($data_waspang)
				{
					$user_waspang = $data_waspang->user;
					$nik_waspang = $data_waspang->nik;
				}

				$template_barm->setValue('nama_w', htmlspecialchars($user_waspang) );
				$template_barm->setValue('nik_w', htmlspecialchars($nik_waspang) );

				$fancyTableFontStyle = array('bold' => true);
				$fancyTableCellStyle = array('valign' => 'center');

				$arr = [
					'borderSize'  => 1,
					'borderColor' => 'none',
					'width'       => 9200,
					'unit'        => \PhpOffice\PhpWord\Style\Table::WIDTH_PERCENT,
					'width'       => 100 * 50,
					'alignment'   => \PhpOffice\PhpWord\SimpleType\JcTable::CENTER,
				];

				$new_tbl_fst = new \PhpOffice\PhpWord\Element\Table([
					'borderColor' => 'none',
					'width'       => 9200,
					'unit'        => \PhpOffice\PhpWord\Style\Table::WIDTH_PERCENT,
					'width'       => 100 * 50,
					'alignment'   => \PhpOffice\PhpWord\SimpleType\JcTable::CENTER,
				]);
				//table object rekonsiliasi
				$tgl_amd_pks = $data_osp->amd_pks. ' Tanggal '. $that->convert_month(date("d-m-Y", strtotime($data->tgl_amd_pks) ) );
				$tgl_amd_sp = $data_osp->amd_sp. ' Tanggal '. $that->convert_month(date("d-m-Y", strtotime($data->tgl_amd_sp) ) );

				$new_tbl_fst->addRow(900);
				$new_tbl_fst->addCell(1200, [ 'borderTopSize' => 1, 'valign' => 'center' ])->addText('Pekerjaan', ['bold' => true]);
				$new_tbl_fst->addCell(100, [ 'borderTopSize' => 1, 'valign' => 'center' ])->addText(':');
				$new_tbl_fst->addCell(2500, [ 'borderTopSize' => 1, 'valign' => 'center' ])->addText(htmlspecialchars($data->judul), ['bold' => true]);
				$new_tbl_fst->addRow();
				$new_tbl_fst->addCell(1200, [ 'valign' => 'center' ])->addText('Perjanjian Kerjasama', ['bold' => true]);
				$new_tbl_fst->addCell(1, [ 'valign' => 'center' ])->addText(':');
				$new_tbl_fst->addCell(2500, [ 'valign' => 'center' ])->addText($data->pks. ' Tanggal '. $that->convert_month(date("d-m-Y", strtotime($data->tgl_pks) ) ), ['bold' => true] );

				if($data_osp->amd_pks)
				{
					$new_tbl_fst->addRow();
					$new_tbl_fst->addCell(1200, [ 'valign' => 'center' ])->addText('Amandemen Perjanjian Kerjasama', ['bold' => true]);
					$new_tbl_fst->addCell(100, [ 'valign' => 'center' ])->addText(':');
					$new_tbl_fst->addCell(2500, [ 'valign' => 'center' ])->addText( ($data_osp->amd_pks ? $tgl_amd_pks : ''), ['bold' => true] );
				}

				$new_tbl_fst->addRow();
				$new_tbl_fst->addCell(1200, [ 'valign' => 'center' ])->addText('Surat Pesanan', ['bold' => true]);
				$new_tbl_fst->addCell(100, [ 'valign' => 'center' ])->addText(':');
				$new_tbl_fst->addCell(2500, [ 'valign' => 'center' ])->addText($data->no_sp. ' Tanggal '. $that->convert_month(date("d-m-Y", strtotime($data->tgl_sp) ) ), ['bold' => true] );

				if($data_osp->amd_sp)
				{
					$new_tbl_fst->addRow();
					$new_tbl_fst->addCell(1200, [ 'valign' => 'center' ])->addText('Amandemen Surat Pesanan', ['bold' => true]);
					$new_tbl_fst->addCell(100, [ 'valign' => 'center' ])->addText(':');
					$new_tbl_fst->addCell(2500, [ 'valign' => 'center' ])->addText( ($data_osp->amd_sp ? $tgl_amd_sp : ''), ['bold' => true] );
				}

				$new_tbl_fst->addRow();
				$new_tbl_fst->addCell(1200, [ 'valign' => 'center' ])->addText('Pelaksana Pekerjaan', ['bold' => true]);
				$new_tbl_fst->addCell(100, [ 'valign' => 'center' ])->addText(':');
				$new_tbl_fst->addCell(2500, [ 'valign' => 'center' ])->addText(preg_replace('/^PT/', 'PT.', $data->mitra_nm), ['bold' => true] );
				$new_tbl_fst->addRow();
				$new_tbl_fst->addCell(1200, [ 'valign' => 'center' ])->addText('Regional', ['bold' => true]);
				$new_tbl_fst->addCell(100, [ 'valign' => 'center' ])->addText(':');
				$new_tbl_fst->addCell(2500, [ 'valign' => 'center' ])->addText('Kalimantan / 6', ['bold' => true]);
				$new_tbl_fst->addRow();
				$new_tbl_fst->addCell(1200, [ 'valign' => 'center' ])->addText('Area', ['bold' => true]);
				$new_tbl_fst->addCell(100, [ 'valign' => 'center' ])->addText(':');

				$new_tbl_fst->addCell(2500, [ 'valign' => 'center' ])->addText(ucwords($get_lok->area), ['bold' => true] );
				$new_tbl_fst->addRow();
				$new_tbl_fst->addCell(1200, [ 'valign' => 'center' ])->addText('ID Project', ['bold' => true]);
				$new_tbl_fst->addCell(100, [ 'valign' => 'center' ])->addText(':');

				$pid_lok = DB::Table('procurement_req_pid As prp')
				->leftjoin('procurement_pid As pp', 'pp.id', '=', 'prp.id_pid_req')
				->select('pp.pid')
				->where('id_upload', $id_boq)
				->get();

				$pid_per_lok = [];

				foreach($pid_lok as $v)
				{
					$pid_per_lok[] = $v->pid;
				}

				$id_project = implode('<w:br/>', $pid_per_lok);
				$new_tbl_fst->addCell(2500, [ 'valign' => 'center' ])->addText($id_project, ['bold' => true]);
				$new_tbl_fst->addRow();
				$new_tbl_fst->addCell(1200, [ 'borderBottomSize' => 1, 'valign' => 'center' ])->addText('Lokasi', ['bold' => true]);
				$new_tbl_fst->addCell(100, [ 'borderBottomSize' => 1, 'valign' => 'center' ])->addText(':');

				$get_lokasi = DB::table('procurement_Boq_lokasi')->where('id_upload', $id_boq)->get();
				$new_lok = [];

				foreach($get_lokasi as $glok)
				{
					$new_lok[] = $glok->lokasi;
				}

				$new_tbl_fst->addCell(2500, [ 'borderBottomSize' => 1, 'valign' => 'center' ])->addText(ucwords(implode(', ', $new_lok) ), ['bold' => true] );

				$template_barm->setComplexBlock('{table_detail}', $new_tbl_fst);

				$template_barm->setValue('tanggal_dibawah', htmlspecialchars($that->convert_month(date("d-m-Y", strtotime($data->tgl_ba_rekon) ), true ) ) );
				// dd($that->convert_month(date("d-m-Y", strtotime($data->tgl_ba_rekon) ), true ) );
				$template_barm->setValue('barek_hari', htmlspecialchars ($that->only_day(date('Y-m-d', strtotime($data->tgl_ba_rekon) ) ) ) );
				$template_barm->setValue('barek_hari_numerik_terbilang', htmlspecialchars ($that->terbilang(date('d', strtotime($data->tgl_ba_rekon) ) ) ) );
				$template_barm->setValue('barek_bulan_terbilang', htmlspecialchars ($bulan_angk[date('m', strtotime($data->tgl_ba_rekon) )] ) );
				$template_barm->setValue('barek_tahun_terbilang', htmlspecialchars ($that->terbilang(date('Y', strtotime($data->tgl_ba_rekon) ) ) ) );
				$template_barm->setValue('barek_d', htmlspecialchars (date('d', strtotime($data->tgl_ba_rekon) ) ) );
				$template_barm->setValue('barek_m', htmlspecialchars (date('m', strtotime($data->tgl_ba_rekon) ) ) );
				$template_barm->setValue('barek_Y', htmlspecialchars (date('Y', strtotime($data->tgl_ba_rekon) ) ) );

				$template_barm->setValue('mitra_comp', htmlspecialchars(preg_replace('/^PT/', 'PT.', $data->mitra_nm) ) );
				$template_barm->setValue('wakil_mitra', htmlspecialchars($mitra->wakil_mitra) );
				$template_barm->setValue('jabatan_mitra', htmlspecialchars($mitra->jabatan_mitra) );

				$tor = new \PhpOffice\PhpWord\Element\Table($arr);
				//table object rekonsiliasi
				$tor->addRow(900);
				$tor->addCell(100, $fancyTableCellStyle)->addText('No', $fancyTableFontStyle);
				$tor->addCell(2000, $fancyTableCellStyle)->addText('PID', $fancyTableFontStyle);
				$tor->addCell(2000, $fancyTableCellStyle)->addText('LOKASI', $fancyTableFontStyle);
				$tor->addCell(2000, $fancyTableCellStyle)->addText('STO', $fancyTableFontStyle);

				$no_tor = 0;

				$lok_rev = [];

				// foreach($lokasi as $v)
				// {
				// 	$lok_rev[$v->pid_sto]['lokasi'] = trim($v->lokasi);
				// 	$lok_rev[$v->pid_sto]['sto'][] = trim($v->sto);
				// }

				// foreach($lok_rev as $k=> $v)
				// {
				// 	$tor->addRow();
				// 	$tor->addCell(100)->addText(++$no_tor);
				// 	$tor->addCell(100)->addText($k);
				// 	$tor->addCell(2000)->addText(ucwords($v['lokasi']) );
				// 	$tor->addCell(2000)->addText(implode(', ', $v['sto']) );
				// }

				foreach($lokasi as $k_lok => $v)
				{
					$lok_rev[$k_lok]['lokasi'] = trim($v->lokasi);
					$lok_rev[$k_lok]['sto'] = trim($v->sto);
				}

				foreach($lok_rev as $k => $v)
				{
					$tor->addRow();
					$tor->addCell(100)->addText(++$no_tor);
					$tor->addCell(100)->addText($k);
					$tor->addCell(2000)->addText(ucwords($v['lokasi']) );
					$tor->addCell(2000)->addText(ucwords($v['sto']) );
				}

				$template_barm->setComplexBlock('{table_obyek_rek}', $tor);

				$all_rfc = AdminModel::find_all_rfc();
				$data_rfc = AdminModel::get_rfc_boq($id_boq);
				$all_design_rfc = json_decode(json_encode($all_rfc), TRUE);

				$rfc_out = $rfc_return = $rfc = [];

				foreach($data_rfc as $kc1 => $valc1)
				{
					$rfc_out[$valc1->material]['material'] = $valc1->material;

					$key_adr_pid = array_search($valc1->material, array_column($all_design_rfc, 'design_rfc') );

					$multiple = 1;

					if($key_adr_pid !== FALSE)
					{
						$multiple = $all_design_rfc[$key_adr_pid]['qty_per_item'];
					}

					if($valc1->type == 'Out')
					{
						$rfc_out[$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['jenis']        = 'Out';
						$rfc_out[$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['no_reservasi'] = $valc1->no_reservasi;
						$rfc_out[$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['nomor_rfc_gi'] = $valc1->no_rfc;
						$rfc_out[$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['vol_give']     = $valc1->quantity;
						$rfc_out[$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['use']          = $valc1->terpakai;
						$rfc_out[$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['id_pro']       = $valc1->id_pro;

						if(!isset($rfc_out[$valc1->material]['isi_m']['rumus_terpakai']) )
						{
							$rfc_out[$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['rumus_terpakai'] = $valc1->terpakai;
						}

						$rfc_out[$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['rumus_terpakai'] = ($valc1->terpakai * $multiple);

						$rfc_out[$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['status_rfc_gi'] = $valc1->status_rfc;
						$rfc_out[$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['id_pro'] = $valc1->id_pro;

						$rfc_out[$valc1->material]['keterangan'] = $valc1->keterangan;
					}

					if($valc1->type == 'Return')
					{
						$rfc_return[$valc1->material][$valc1->id_pro]['jenis']             = 'Return';
						$rfc_return[$valc1->material][$valc1->id_pro]['nomor_rfc_return']  = $valc1->no_rfc;
						$rfc_return[$valc1->material][$valc1->id_pro]['return']            = $valc1->quantity;
						$rfc_return[$valc1->material][$valc1->id_pro]['status_rfc_return'] = $valc1->status_rfc;
						$rfc_return[$valc1->material][$valc1->id_pro]['id_pro']            = $valc1->id_pro;
						$rfc_return[$valc1->material][$valc1->id_pro]['no_reservasi']      = $valc1->no_reservasi;
					}

					if(!isset($rfc_out[$valc1->material]['total_m']) )
					{
						$rfc_out[$valc1->material]['total_m'] = 0;
					}

					$rfc_out[$valc1->material]['total_m'] += ($valc1->type == 'Out' ? $valc1->quantity : 0);

					if(!isset($rfc_out[$valc1->material]['total_use']) )
					{
						$rfc_out[$valc1->material]['total_use'] = 0;
					}

					$rfc_out[$valc1->material]['total_use'] += ($valc1->type == 'Out' ? $valc1->terpakai * $multiple : 0);
				}

				$all_material = array_unique(array_merge(array_keys($rfc_out), array_keys($rfc_return) ) );
				$rfc = array_map(function($x){
					return $x = [];
				}, array_flip($all_material) );

				foreach($rfc_out as $k => $v)
				{
					$rfc[$k] = $v;
				}

				foreach($rfc_return as $k => $v)
				{
					$key_rfc = array_column($rfc, 'material');
					$find_k = array_keys($key_rfc, $k);

					foreach($find_k as $kk => $vv)
					{
						$data_inside_rfc = $rfc[$key_rfc[$vv] ]['isi_m'];

						foreach($data_inside_rfc as $k3 => $v3)
						{
							$matchingKeys = array_keys(array_filter($v, function($item) use ($k3) {
								return $item['no_reservasi'] == $k3;
							}) );

							foreach($matchingKeys as $k4 => $v4)
							{
								$rfc[$key_rfc[$vv] ]['isi_m'][$v[$v4]['no_reservasi'] ]['return'][$v4] = $v[$v4];
							}
						}
					}
				}

				// $tr = new \PhpOffice\PhpWord\Element\Table($arr);
				// //table hasil rekonsiliasi
				// $tr->addRow(900);
				// $tr->addCell(100, $fancyTableCellStyle)->addText('No', $fancyTableFontStyle);
				// $tr->addCell(2000, $fancyTableCellStyle)->addText('ID Material', $fancyTableFontStyle);
				// $tr->addCell(2000, $fancyTableCellStyle)->addText('Total Volume', $fancyTableFontStyle);
				// $tr->addCell(2000, $fancyTableCellStyle)->addText('Volume Material Telkom Akses', $fancyTableFontStyle);
				// $tr->addCell(2000, $fancyTableCellStyle)->addText('Volume Material Mitra', $fancyTableFontStyle);

				// $no_tr = 0;

				// $data_mat_rkon = [];

				// foreach($data_rfc as $v)
				// {
				// 	if(!isset($data_mat_rkon[$v['material'] ]) )
				// 	{
				// 		$data_mat_rkon[$v['material'] ]['material_ta'] = 0;
				// 	}

				// 	$data_mat_rkon[$v['material'] ]['nama']           = $v['material'];
				// 	$data_mat_rkon[$v['material'] ]['material_ta']   += $v['use_all'] - $v['return_all'];
				// 	$data_mat_rkon[$v['material'] ]['material_mitra'] = 0;
				// }

				// foreach($design as $valc1)
				// {
				// 	if($valc1->jenis_khs != 16 && $valc1->material != 0)
				// 	{
				// 		$data_mat_rkon[$valc1->designator.'m']['nama']           = $valc1->designator;
				// 		$data_mat_rkon[$valc1->designator.'m']['material_ta']    = 0;
				// 		$data_mat_rkon[$valc1->designator.'m']['material_mitra'] = $valc1->rekon;
				// 	}
				// }
				// // dd($data_mat_rkon, $rfc);
				// foreach($data_mat_rkon as $v)
				// {
				// 	$tr->addRow();
				// 	$tr->addCell(100)->addText(++$no_tr);
				// 	$tr->addCell(2000)->addText($v['nama']);
				// 	$tr->addCell(2000)->addText($v['material_ta'] + $v['material_mitra']);
				// 	$tr->addCell(2000)->addText($v['material_ta']);
				// 	$tr->addCell(2000)->addText($v['material_mitra']);
				// }

				// $template_barm->setComplexBlock('{table_rekon}', $tr);

				$dmu = new \PhpOffice\PhpWord\Element\Table($arr);

				$dmu->addRow();
				$dmu->addCell(500)->addText('#');
				$dmu->addCell(1500)->addText('ID Material');
				$dmu->addCell(1500)->addText('Total Material');
				$dmu->addCell(1500)->addText('NO. RFC/GI');
				$dmu->addCell(1500)->addText('Status');
				$dmu->addCell(1500)->addText('Volume Diberikan');
				$dmu->addCell(1500)->addText('Volume Pemakaian');
				$dmu->addCell(1500)->addText('Total Pemakaian');
				$dmu->addCell(1500)->addText('NO. RFC/Pengembalian');
				$dmu->addCell(1500)->addText('Status');
				$dmu->addCell(1500)->addText('Volume Dikembalian');
				$dmu->addCell(1500)->addText('Sisa Volume');
				$dmu->addCell(1500)->addText('Keterangan');

				$no = 0;

				foreach ($rfc as $key => $val)
				{
					$first = true;

					foreach ($val['isi_m'] as $k2 => $valc1_1)
					{
						$second = true;
						$no_row = 0;

						array_map(function ($x) use (&$no_row) {
							$no_row += !isset($x['return']) ? 1 : count($x['return']);
						}, $val['isi_m']);

						foreach ($valc1_1 as $kkk => $valc1)
						{
							if ($kkk == 'out')
							{
								$dmu->addRow();

								if ($first == true)
								{
									$dmu->addCell(500, ['vMerge' => 'restart'])->addText(++$no);
									$dmu->addCell(1500, ['vMerge' => 'restart'])->addText($key);
									$dmu->addCell(1500, ['vMerge' => 'restart'])->addText($val['total_m']);
								}
								else
								{
									$dmu->addCell(500, ['vMerge' => 'continue']);
									$dmu->addCell(1500, ['vMerge' => 'continue']);
									$dmu->addCell(1500, ['vMerge' => 'continue']);
								}

								$hitung_row_c = !isset($valc1_1['return']) ? 1 : count($valc1_1['return']);

								if ($second == true)
								{
									$dmu->addCell(1500, ['vMerge' => 'restart'])->addText($valc1['nomor_rfc_gi']);
									$dmu->addCell(1500, ['vMerge' => 'restart'])->addText($valc1['status_rfc_gi'], array('bgColor' => $valc1['status_rfc_gi'] == 'LURUS' ? '' : 'red', 'color' => $valc1['status_rfc_gi'] == 'LURUS' ? '' : 'black'));
									$dmu->addCell(1500, ['vMerge' => 'restart'])->addText($valc1['vol_give']);
									$dmu->addCell(1500, ['vMerge' => 'restart'])->addText(($valc1['use'] != 0 ? $valc1['use'] : ''));
								}
								else
								{
									$dmu->addCell(1500, ['vMerge' => 'continue']);
									$dmu->addCell(1500, ['vMerge' => 'continue']);
									$dmu->addCell(1500, ['vMerge' => 'continue']);
									$dmu->addCell(1500, ['vMerge' => 'continue']);
								}

								if ($first == true) {
									$dmu->addCell(1500, ['vMerge' => 'restart'])->addText($val['total_use']);
								}
								else
								{
									$dmu->addCell(1500, ['vMerge' => 'continue']);
								}

								if (isset($valc1_1['return']) )
								{
									$dmu->addCell(1500)->addText(current($valc1_1['return'])['nomor_rfc_return'] ?? '-');
									$dmu->addCell(1500, array('bgColor' => @current($valc1_1['return'])['status_rfc_return'] == 'TIDAK LURUS' ? 'red' : '', 'color' => @current($valc1_1['return'])['status_rfc_return'] == 'TIDAK LURUS' ? 'black' : ''))->addText(current($valc1_1['return'])['status_rfc_return'] ?? '-');
									$dmu->addCell(1500)->addText(current($valc1_1['return'])['return'] ?? '-');

									if ($second == true)
									{
										$used_rfc = 0;

										if (isset($valc1_1['return']) )
										{
											$r = 0;

											foreach ($valc1_1['return'] as $kx => $vx)
											{
												$r += $vx['return'];
											}

											$used_rfc = $valc1['vol_give'] - $r;
										}

										$dmu->addCell(1500, ['vMerge' => 'restart'])->addText($used_rfc);
										$second = false;
									}
									else
									{
										$dmu->addCell(1500, ['vMerge' => 'continue']);
									}
								}
								else
								{
									$dmu->addCell(1500)->addText('-');
									$dmu->addCell(1500)->addText('-');
									$dmu->addCell(1500)->addText('-');
									$dmu->addCell(1500)->addText('0');
								}

								if ($first == true)
								{
									$dmu->addCell(1500, ['vMerge' => 'restart'])->addText($val['keterangan'] ?? '');
									$first = false;
								}
								else
								{
									$dmu->addCell(1500, ['vMerge' => 'continue']);
								}
							}
							else
							{
								$new_valc1 = $valc1;
								array_shift($new_valc1);

								foreach (@$new_valc1 as $k4 => $v4)
								{
									$dmu->addRow();
									$dmu->addCell(1500)->addText($v4['nomor_rfc_return'] ?? '-');
									$dmu->addCell(1500, array('bgColor' => @$v4['status_rfc_return'] == 'TIDAK LURUS' ? 'red' : '', 'color' => @$v4['status_rfc_return'] == 'TIDAK LURUS' ? 'black' : ''))->addText($v4['status_rfc_return'] ?? '-');
									$dmu->addCell(1500)->addText($v4['return'] ?? '-');
								}
							}
						}
					}
				}

				$template_barm->setComplexBlock('{detail_material_use}', $dmu);

				$dmitra = new \PhpOffice\PhpWord\Element\Table($arr);
				//table detail Mitra
				$dmitra->addRow(900);
				$dmitra->addCell(100, $fancyTableCellStyle)->addText('No', $fancyTableFontStyle);
				$dmitra->addCell(2000, $fancyTableCellStyle)->addText('ID Material', $fancyTableFontStyle);
				$dmitra->addCell(2000, $fancyTableCellStyle)->addText('Volume Pemakaian', $fancyTableFontStyle);
				$dmitra->addCell(2000, $fancyTableCellStyle)->addText('Keterangan', $fancyTableFontStyle);

				$no_dmitra = 0;
				$get_only_mat = [];

				foreach($design as $valc1)
				{
					$get_only_mat[$valc1->designator][$valc1->material]['designator'] = $valc1->designator;
					$get_only_mat[$valc1->designator][$valc1->material]['rekon'] = $valc1->rekon;
					$get_only_mat[$valc1->designator][$valc1->material]['jenis_khs'] = $valc1->jenis_khs;
					$get_only_mat[$valc1->designator][$valc1->material]['material'] = $valc1->material;
				}

				foreach($get_only_mat as $valc1)
				{
					if($valc1['jenis_khs'] != 16 && $valc1['material'] != 0)
					{
						$dmitra->addRow();
						$dmitra->addCell(100)->addText(++$no_dmitra);
						$dmitra->addCell(2000)->addText($valc1['designator']);
						$dmitra->addCell(2000)->addText($valc1['rekon']);
						$dmitra->addCell(2000)->addText('');
					}
				}

				$template_barm->setComplexBlock('{detail_material_mitra}', $dmitra);
				//nama
				// $cleansing = preg_replace('/\W/', '_', $val_tc1['nama']);
				// $file_name = 'TEMPLATE DOKUMEN BARM '. $cleansing .'.docx';
				$file_name = 'TEMPLATE DOKUMEN BARM.docx';
				$path =   public_path() . '/upload2/' . $id_boq . '/BARM';

				if (!file_exists($path)) {
					if (!mkdir($path, 0770, true)) {
						return 'gagal menyiapkan folder barm';
					}
				}

				$file = $path .'/'. $file_name;
				$template_barm->saveAS($file);
      break;
      case 'BARM_2021':
				$template = public_path() . '/template_doc/maintenance_up_2021/7. BA Rekon Material.docx';

				$template_barm_2021 = new \PhpOffice\PhpWord\TemplateProcessor($template);

				$data_ttd_ss = ReportModel::get_dokumen_id('mngr_ss', $data->pekerjaan, $data->witel, $data->tgl_ba_rekon);

				$template_barm_2021->setValue('mgr_ss_barm_wakil', htmlspecialchars($data_ttd_ss->user) );
				$template_barm_2021->setValue('mgr_ss_barm_jabatan', htmlspecialchars($data_ttd_ss->jabatan) );

				// foreach($lokasi as $v)
				// {
				// 	$sto_comp[$v->lokasi .' '. $v->sto] = $v->sto;
				// 	$lokasi_comp[$v->lokasi .' '. $v->sto] = $v->lokasi;
				// }

				// if($data_ttd_ss->reg == 6)
				// {
				// 	$template_barm_2021->setValue('area_ss', htmlspecialchars('Banjarmasin') );
				// }

				// $data_ttd_ic = ReportModel::get_dokumen_id('invt_cntrl', $data->pekerjaan, $data->witel, $data->tgl_ba_rekon);

				// $template_barm_2021->setValue('nama_ic_barm', htmlspecialchars($data_ttd_ic->user));
				// $template_barm_2021->setValue('nik_ic_barm', htmlspecialchars($data_ttd_ic->nik));

				// if($data_ttd_ic->reg == 6)
				// {
				// 	$template_barm_2021->setValue('area_ss', htmlspecialchars('Banjarmasin') );
				// }

				// $data_ttd = ReportModel::get_dokumen_id('mngr_krj', $data->pekerjaan, $data->witel, $data->tgl_ba_rekon);
				$data_ttd = ReportModel::get_dokumen_id('wh', $data->pekerjaan, $data->witel, $data->tgl_ba_rekon);
				$project_nama = $data_ttd->user;
				$project_jab = $data_ttd->jabatan;

				$template_barm_2021->setValue('wh_nama', htmlspecialchars($project_nama) );
				$template_barm_2021->setValue('wh_jabatan', htmlspecialchars($project_jab) );

				$data_ttd = ReportModel::get_dokumen_id('mngr_krj', $data->pekerjaan, $data->witel, $data->tgl_ba_rekon);

				$project_nama = $data_ttd->user;
				$project_jab = $data_ttd->jabatan;

				$template_barm_2021->setValue('mngr_pekerjaan_wakil_rekon', htmlspecialchars($project_nama) );
				$template_barm_2021->setValue('mngr_pekerjaan_jabatan_rekon', htmlspecialchars($project_jab) );

				$data_waspang = ReportModel::get_proc_user($data->waspang_id);

				$user_waspang = 'WASPANG';
				$jabatan_waspang  = 'JABATAN';

				if($data_waspang)
				{
					$user_waspang = $data_waspang->user;
					$jabatan_waspang = $data_waspang->jabatan;
				}

				$template_barm_2021->setValue('nama_w', htmlspecialchars($user_waspang) );
				$template_barm_2021->setValue('jabatan_w', htmlspecialchars($jabatan_waspang) );

				$fancyTableFontStyle = array('bold' => true);
				$fancyTableCellStyle = array('valign' => 'center');

				$arr = [
					'borderSize'  => 1,
					'borderColor' => 'none',
					'width'       => 9200,
					'unit'        => \PhpOffice\PhpWord\Style\Table::WIDTH_PERCENT,
					'width'       => 100 * 50,
					'alignment'   => \PhpOffice\PhpWord\SimpleType\JcTable::CENTER,
				];

				$new_tbl_fst = new \PhpOffice\PhpWord\Element\Table([
					'borderColor' => 'none',
					'width'       => 9200,
					'unit'        => \PhpOffice\PhpWord\Style\Table::WIDTH_PERCENT,
					'width'       => 100 * 50,
					'alignment'   => \PhpOffice\PhpWord\SimpleType\JcTable::CENTER,
				]);
				//table object rekonsiliasi
				$tgl_amd_pks = $data_osp->amd_pks. ' Tanggal '. $that->convert_month(date("d-m-Y", strtotime($data->tgl_amd_pks) ) );
				$tgl_amd_sp = $data_osp->amd_sp. ' Tanggal '. $that->convert_month(date("d-m-Y", strtotime($data->tgl_amd_sp) ) );

				$new_tbl_fst->addRow(900);
				$new_tbl_fst->addCell(1200, [ 'borderTopSize' => 1, 'valign' => 'center' ])->addText('Pekerjaan', ['bold' => true]);
				$new_tbl_fst->addCell(100, [ 'borderTopSize' => 1, 'valign' => 'center' ])->addText(':');
				$new_tbl_fst->addCell(2500, [ 'borderTopSize' => 1, 'valign' => 'center' ])->addText(htmlspecialchars($data->judul), ['bold' => true]);
				$new_tbl_fst->addRow();
				$new_tbl_fst->addCell(1200, [ 'valign' => 'center' ])->addText('Perjanjian Kerjasama', ['bold' => true]);
				$new_tbl_fst->addCell(1, [ 'valign' => 'center' ])->addText(':');
				$new_tbl_fst->addCell(2500, [ 'valign' => 'center' ])->addText($data->pks. ' Tanggal '. $that->convert_month(date("d-m-Y", strtotime($data->tgl_pks) ) ), ['bold' => true] );

				if($data_osp->amd_pks)
				{
					$new_tbl_fst->addRow();
					$new_tbl_fst->addCell(1200, [ 'valign' => 'center' ])->addText('Amandemen Perjanjian Kerjasama', ['bold' => true]);
					$new_tbl_fst->addCell(100, [ 'valign' => 'center' ])->addText(':');
					$new_tbl_fst->addCell(2500, [ 'valign' => 'center' ])->addText( ($data_osp->amd_pks ? $tgl_amd_pks : ''), ['bold' => true] );
				}

				$new_tbl_fst->addRow();
				$new_tbl_fst->addCell(1200, [ 'valign' => 'center' ])->addText('Surat Pesanan', ['bold' => true]);
				$new_tbl_fst->addCell(100, [ 'valign' => 'center' ])->addText(':');
				$new_tbl_fst->addCell(2500, [ 'valign' => 'center' ])->addText($data->no_sp. ' Tanggal '. $that->convert_month(date("d-m-Y", strtotime($data->tgl_sp) ) ), ['bold' => true] );

				if($data_osp->amd_sp)
				{
					$new_tbl_fst->addRow();
					$new_tbl_fst->addCell(1200, [ 'valign' => 'center' ])->addText('Amandemen Surat Pesanan', ['bold' => true]);
					$new_tbl_fst->addCell(100, [ 'valign' => 'center' ])->addText(':');
					$new_tbl_fst->addCell(2500, [ 'valign' => 'center' ])->addText( ($data_osp->amd_sp ? $tgl_amd_sp : ''), ['bold' => true] );
				}

				$new_tbl_fst->addRow();
				$new_tbl_fst->addCell(1200, [ 'valign' => 'center' ])->addText('Pelaksana Pekerjaan', ['bold' => true]);
				$new_tbl_fst->addCell(100, [ 'valign' => 'center' ])->addText(':');
				$new_tbl_fst->addCell(2500, [ 'valign' => 'center' ])->addText(preg_replace('/^PT/', 'PT.', $data->mitra_nm), ['bold' => true] );
				$new_tbl_fst->addRow();
				$new_tbl_fst->addCell(1200, [ 'valign' => 'center' ])->addText('Regional', ['bold' => true]);
				$new_tbl_fst->addCell(100, [ 'valign' => 'center' ])->addText(':');
				$new_tbl_fst->addCell(2500, [ 'valign' => 'center' ])->addText('Kalimantan / 6', ['bold' => true]);
				$new_tbl_fst->addRow();
				$new_tbl_fst->addCell(1200, [ 'valign' => 'center' ])->addText('Area', ['bold' => true]);
				$new_tbl_fst->addCell(100, [ 'valign' => 'center' ])->addText(':');

				$new_tbl_fst->addCell(2500, [ 'valign' => 'center' ])->addText(ucwords($get_lok->area), ['bold' => true] );
				$new_tbl_fst->addRow();
				$new_tbl_fst->addCell(1200, [ 'valign' => 'center' ])->addText('ID Project', ['bold' => true]);
				$new_tbl_fst->addCell(100, [ 'valign' => 'center' ])->addText(':');

				$pid_lok = DB::Table('procurement_req_pid As prp')
				->leftjoin('procurement_pid As pp', 'pp.id', '=', 'prp.id_pid_req')
				->select('pp.pid')
				->where('id_upload', $id_boq)
				->get();

				$pid_per_lok = [];

				foreach($pid_lok as $v)
				{
					$pid_per_lok[] = $v->pid;
				}

				$id_project = implode('<w:br/>', $pid_per_lok);
				$new_tbl_fst->addCell(2500, [ 'valign' => 'center' ])->addText($id_project, ['bold' => true]);
				$new_tbl_fst->addRow();
				$new_tbl_fst->addCell(1200, [ 'borderBottomSize' => 1, 'valign' => 'center' ])->addText('Lokasi', ['bold' => true]);
				$new_tbl_fst->addCell(100, [ 'borderBottomSize' => 1, 'valign' => 'center' ])->addText(':');

				$get_lokasi = DB::table('procurement_Boq_lokasi')->where('id_upload', $id_boq)->get();
				$new_lok = [];

				foreach($get_lokasi as $glok)
				{
					$new_lok[] = $glok->lokasi;
				}

				$new_tbl_fst->addCell(2500, [ 'borderBottomSize' => 1, 'valign' => 'center' ])->addText(ucwords(implode(', ', $new_lok) ), ['bold' => true] );

				$template_barm_2021->setComplexBlock('{table_detail}', $new_tbl_fst);

				$template_barm_2021->setValue('tanggal_dibawah', htmlspecialchars($that->convert_month(date("d-m-Y", strtotime($data->tgl_ba_rekon) ), true ) ) );
				// dd($that->convert_month(date("d-m-Y", strtotime($data->tgl_ba_rekon) ), true ) );
				$template_barm_2021->setValue('barek_hari', htmlspecialchars ($that->only_day(date('Y-m-d', strtotime($data->tgl_ba_rekon) ) ) ) );
				$template_barm_2021->setValue('barek_hari_numerik_terbilang', htmlspecialchars ($that->terbilang(date('d', strtotime($data->tgl_ba_rekon) ) ) ) );
				$template_barm_2021->setValue('barek_bulan_terbilang', htmlspecialchars ($bulan_angk[date('m', strtotime($data->tgl_ba_rekon) )] ) );
				$template_barm_2021->setValue('barek_tahun_terbilang', htmlspecialchars ($that->terbilang(date('Y', strtotime($data->tgl_ba_rekon) ) ) ) );
				$template_barm_2021->setValue('barek_d', htmlspecialchars (date('d', strtotime($data->tgl_ba_rekon) ) ) );
				$template_barm_2021->setValue('barek_m', htmlspecialchars (date('m', strtotime($data->tgl_ba_rekon) ) ) );
				$template_barm_2021->setValue('barek_Y', htmlspecialchars (date('Y', strtotime($data->tgl_ba_rekon) ) ) );

				$template_barm_2021->setValue('mitra_comp', htmlspecialchars(preg_replace('/^PT/', 'PT.', $data->mitra_nm) ) );
				$template_barm_2021->setValue('wakil_mitra', htmlspecialchars($mitra->wakil_mitra) );
				$template_barm_2021->setValue('jabatan_mitra', htmlspecialchars($mitra->jabatan_mitra) );
				$template_barm_2021->setValue('judul', htmlspecialchars($data->judul) );

				$all_rfc = AdminModel::find_all_rfc();
				$data_rfc = AdminModel::get_rfc_boq($id_boq);
				$all_design_rfc = json_decode(json_encode($all_rfc), TRUE);

				$rfc_out = $rfc_return = $rfc = [];

				foreach($data_rfc as $kc1 => $valc1)
				{
					$rfc_out[$valc1->designator][$valc1->material]['material'] = $valc1->material;

					$key_adr_pid = array_search($valc1->material, array_column($all_design_rfc, 'design_rfc') );

					$multiple = 1;

					if($key_adr_pid !== FALSE)
					{
						$multiple = $all_design_rfc[$key_adr_pid]['qty_per_item'];
					}

					if($valc1->type == 'Out')
					{
						$rfc_out[$valc1->designator][$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['jenis']        = 'Out';
						$rfc_out[$valc1->designator][$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['no_reservasi'] = $valc1->no_reservasi;
						$rfc_out[$valc1->designator][$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['nomor_rfc_gi'] = $valc1->no_rfc;
						$rfc_out[$valc1->designator][$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['vol_give']     = $valc1->quantity;
						$rfc_out[$valc1->designator][$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['use']          = $valc1->terpakai;
						$rfc_out[$valc1->designator][$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['id_pro']       = $valc1->id_pro;

						if(!isset($rfc_out[$valc1->designator][$valc1->material]['isi_m']['rumus_terpakai']) )
						{
							$rfc_out[$valc1->designator][$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['rumus_terpakai'] = $valc1->terpakai * $multiple;
						}

						$rfc_out[$valc1->designator][$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['status_rfc_gi'] = $valc1->status_rfc;
						$rfc_out[$valc1->designator][$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['id_pro'] = $valc1->id_pro;

						$rfc_out[$valc1->designator][$valc1->material]['keterangan'] = $valc1->keterangan;
						$rfc_out[$valc1->designator][$valc1->material]['satuan'] = $valc1->satuan;
					}

					if($valc1->type == 'Return')
					{
						$rfc_return[$valc1->designator][$valc1->material][$valc1->id_pro]['jenis']             = 'Return';
						$rfc_return[$valc1->designator][$valc1->material][$valc1->id_pro]['nomor_rfc_return']  = $valc1->no_rfc;
						$rfc_return[$valc1->designator][$valc1->material][$valc1->id_pro]['return']            = $valc1->quantity;
						$rfc_return[$valc1->designator][$valc1->material][$valc1->id_pro]['status_rfc_return'] = $valc1->status_rfc;
						$rfc_return[$valc1->designator][$valc1->material][$valc1->id_pro]['id_pro']            = $valc1->id_pro;
						$rfc_return[$valc1->designator][$valc1->material][$valc1->id_pro]['satuan']       		 = $valc1->satuan;
						$rfc_return[$valc1->designator][$valc1->material][$valc1->id_pro]['no_reservasi']      = $valc1->no_reservasi;
					}

					if(!isset($rfc_out[$valc1->designator][$valc1->material]['total_m']) )
					{
						$rfc_out[$valc1->designator][$valc1->material]['total_m'] = 0;
						$rfc_out[$valc1->designator][$valc1->material]['return_m'] = 0;
					}

					$rfc_out[$valc1->designator][$valc1->material]['total_m'] += ($valc1->type == 'Out' ? $valc1->quantity : 0);
					$rfc_out[$valc1->designator][$valc1->material]['return_m'] += ($valc1->type == 'Return' ? $valc1->quantity : 0);

					if(!isset($rfc_out[$valc1->designator][$valc1->material]['total_use']) )
					{
						$rfc_out[$valc1->designator][$valc1->material]['total_use'] = 0;
						$rfc_out[$valc1->designator][$valc1->material]['total_return'] = 0;
					}

					$rfc_out[$valc1->designator][$valc1->material]['total_use'] += ($valc1->type == 'Out' ? $valc1->terpakai: 0);
					$rfc_out[$valc1->designator][$valc1->material]['total_return'] += ($valc1->type == 'Return' ? $valc1->quantity: 0);
				}

				$all_material = array_unique(array_merge(array_keys($rfc_out), array_keys($rfc_return) ) );
				$rfc = array_map(function($x){
					return $x = [];
				}, array_flip($all_material) );

				foreach($rfc_out as $k => $v)
				{
					$rfc[$k] = $v;
				}

				foreach($rfc_return as $k => $v)
				{
					foreach($v as $k2 => $v2)
					{
						$key_rfc = array_column($rfc[$k], 'material');
						$find_k = array_keys($key_rfc, $k2);

						foreach($find_k as $kk => $vv)
						{
							$data_inside_rfc = $rfc[$k][$key_rfc[$vv] ]['isi_m'];

							foreach($data_inside_rfc as $k3 => $v3)
							{
								$matchingKeys = array_keys(array_filter($v2, function($item) use ($k3) {
									return $item['no_reservasi'] == $k3;
								}) );

								foreach($matchingKeys as $k4 => $v4)
								{
									$rfc[$k][$key_rfc[$vv] ]['isi_m'][$v2[$v4]['no_reservasi'] ]['return'][$v4] = $v2[$v4];
								}
							}
						}
					}
				}

				$dmu = new \PhpOffice\PhpWord\Element\Table($arr);

				$dmu->addRow();
				$dmu->addCell(500, ['vMerge' => 'restart'])->addText('#');
				$dmu->addCell(1500, ['vMerge' => 'restart'])->addText('Nama Barang BOQ');
				$dmu->addCell(1500, ['vMerge' => 'restart'])->addText('Nama Barang ALISTA');
				$dmu->addCell(1500, ['vMerge' => 'restart'])->addText('Gudang');
				$dmu->addCell(1500, ['vMerge' => 'restart'])->addText('Satuan');
				$dmu->addCell(1500, ['vMerge' => 'restart'])->addText('Hasil Rekonsiliasi');
				$dmu->addCell(4500, ['gridSpan' => 3])->addText('Pemakaian Material');
				$dmu->addCell(1500, ['vMerge' => 'restart'])->addText('Keterangan');
				$dmu->addRow();
				$dmu->addCell(null, ['vMerge' => 'continue']);
				$dmu->addCell(null, ['vMerge' => 'continue']);
				$dmu->addCell(null, ['vMerge' => 'continue']);
				$dmu->addCell(null, ['vMerge' => 'continue']);
				$dmu->addCell(null, ['vMerge' => 'continue']);
				$dmu->addCell(null, ['vMerge' => 'continue']);
				$dmu->addCell(1500)->addText('PT. Telkom Akses');
				$dmu->addCell(1500)->addText($data_boq_rekon_raw[0]->nama_company);
				$dmu->addCell(1500)->addText('Pengembalian Material');
				$dmu->addCell(null, ['vMerge' => 'continue']);

				$no = 0;

				foreach ($rfc as $k1 => $v1)
				{
					$first = true;

					foreach($v1 as $k2 => $v2)
					{
						$dmu->addRow();

						if ($first == true)
						{
							$dmu->addCell(500, ['vMerge' => 'restart'])->addText(++$no);
							$dmu->addCell(1500, ['vMerge' => 'restart'])->addText($k1);

							$first = false;
						}
						else
						{
							$dmu->addCell(500, ['vMerge' => 'continue']);
							$dmu->addCell(1500, ['vMerge' => 'continue']);
						}

						$dmu->addCell(1500, ['vMerge' => 'restart'])->addText($k2);
						$dmu->addCell(1500, ['vMerge' => 'restart'])->addText($v2['total_m']);
						$dmu->addCell(1500, ['vMerge' => 'restart'])->addText($v2['satuan']);
						$dmu->addCell(1500, ['vMerge' => 'restart'])->addText($v2['total_use'] + $v2['total_return']);
						$dmu->addCell(1500, ['vMerge' => 'restart'])->addText($v2['total_use']);
						$dmu->addCell(1500, ['vMerge' => 'restart'])->addText(0);
						$dmu->addCell(1500, ['vMerge' => 'restart'])->addText($v2['total_return']);
						$dmu->addCell(1500, ['vMerge' => 'restart'])->addText('-');
					}
				}

				$template_barm_2021->setComplexBlock('{detail_material_use}', $dmu);

				$template_barm_2021->setValue('lokasi_pek', htmlspecialchars($data->lokasi_pekerjaan) );


				$file_name = 'TEMPLATE DOKUMEN BARM 2021.docx';
				$path =   public_path() . '/upload2/' . $id_boq . '/BARM';

				if (!file_exists($path)) {
					if (!mkdir($path, 0770, true)) {
						return 'gagal menyiapkan folder BARM';
					}
				}

				$file = $path .'/'. $file_name;
				$template_barm_2021->saveAS($file);
      break;
      default:
        //faktur pajak

        $template = public_path() . '/template_doc/maintenance_down_2021/template FAKTUR.docx';
        $template_frak = new \PhpOffice\PhpWord\TemplateProcessor($template);
        $template_frak->setValue('faktur_no', htmlspecialchars(($data->faktur ?? '')));
        $template_frak->setValue('mitra_nama', htmlspecialchars(preg_replace('/^PT/', 'PT.', $data->mitra_nm) ));
        $template_frak->setValue('mitra_alamat', htmlspecialchars($mitra->alamat_company));
        $template_frak->setValue('mitra_npwp', htmlspecialchars($mitra->NPWP));
        $template_frak->setValue('judul', htmlspecialchars($data->judul));
				$pph_text = $AdminModel->find_pph_text($data->tgl_faktur);
        $template_frak->setValue('pph', htmlspecialchars($pph_text));
        $template_frak->setValue('rekon_gd', htmlspecialchars(number_format($data->gd_rekon, 0, '.', '.') ) );
				$pph = $AdminModel->find_pph($data->tgl_faktur);
        $template_frak->setValue('rekon_gd_ppn', htmlspecialchars(number_format(self::convert_floor_ceil($data->tgl_faktur, $data->gd_rekon * $pph), 0, '.', '.') ) );
			 	$template_frak->setValue('mitra_wakil', htmlspecialchars($mitra->wakil_mitra));

        $template_frak->setValue('faktur_tgl_terbilang', htmlspecialchars(ucwords($mitra->lokasi_npwp).", ". $that->convert_month(date("d-m-Y", strtotime($data->tgl_faktur) ) ) ) );
        //nama
        $file_name = 'TEMPLATE DOKUMEN FAKTUR.docx';
				$path =   public_path() . '/upload2/' . $id_boq . '/FAKTUR';

				if (!file_exists($path)) {
					if (!mkdir($path, 0770, true)) {
						return 'gagal menyiapkan folder Faktur';
					}
				}

        $file = $path . '/' . $file_name;
        $template_frak->saveAS($file);
      break;
    }
  }

	public static function update_progress_sp($req, $id)
	{
		$check = ReportModel::get_boq_data($id);
		// dd($check, $req->all());

		// dd($check);
		// if($check->jenis_work == 'KONTRAK PUTUS')
		// {
		// 	$status = 'APPROVE_REKON';
		// 	$step_id = 8;
		// 	$nama_modifi = session('auth')->id_user;

		// 	DB::SELECT(
		// 		"UPDATE procurement_boq_upload pbu
		// 		LEFT JOIN procurement_Boq_lokasi pbl ON pbu.id = pbl.id_upload
		// 		SET pbu.status = '$status', pbl.status = '$status', pbl.step_id = 7, pbu.modified_by = '$nama_modifi'
		// 		WHERE pbu.id = $id"
		// 	);
		// }
		// else
		// {
		// 	$step_id = 6;
		// }

		if ($check->pekerjaan == "PSB")
		{
			$step_id = 7;
		} else {
			$step_id = 6;
		}

		$bulan = array (
			'01' =>'Januari',
			'02' =>'Februari',
			'03' =>'Maret',
			'04' =>'April',
			'05' =>'Mei',
			'06' =>'Juni',
			'07' =>'Juli',
			'08' =>'Agustus',
			'09' =>'September',
			'10' =>'Oktober',
			'11' =>'November',
			'12' =>'Desember'
		);

		$xp = explode('-', $req->tgl_pks);

		$data_j = ReportModel::get_boq_data($id);

		DB::Table('procurement_mitra')
		->where('id', $data_j->mitra_id)
		->update([
			'no_khs_maintenance'  => $req->pks,
			'tgl_khs_maintenance' => $xp[2] . ' '. $bulan[$xp[1] ] . ' ' . $xp[0]
		]);

		DB::table('procurement_boq_upload')->where('id', $id)->update([
			'tgl_jatuh_tmp'     => $req->toc,
			'no_sp'             => $req->no_sp,
			'tgl_sp'            => $req->tgl_sp,
			// 'total_sp'          => str_replace(".", "", $req->beforeppn),
			'total_sp'			=> $data_j->total_material_sp + $data_j->total_jasa_sp,
			'pks'               => $req->pks,
			'tgl_pks'           => $req->tgl_pks,
			'surat_penetapan'   => $req->s_pen,
			'tgl_s_pen'         => $req->tgl_s_pen,
			'surat_kesanggupan' => $req->no_kesanggupan_khs,
			'tgl_surat_sanggup' => $req->tgl_surat_sanggup,
			'step_id'           => $step_id,
			'modified_by'       => session('auth')->id_user,
		]);

		self::save_log($step_id, $id);

		$mitra 		= AdminModel::get_mitra($req->mitra_id);
		$botToken = '1902669622:AAHz0cl18e8-IF5waY1uQIj33Nxqq72EzFc';
		$website  = "https://api.telegram.org/bot".$botToken;

		$toc_tgl = date('Y-m-d', strtotime('+'. ($req->toc - 1). ' day', strtotime($req->tgl_sp) ) );
		$message = "<b>".preg_replace('/^PT/', 'PT.', $mitra->nama_company) ."</b>\n\n<b>Judul :</b> <i>$data_j->judul</i>\n<b>Pekerjaan :</b> <i>$data_j->pekerjaan - $data_j->jenis_work</i>\n<b>Tahapan :</b> <i>Surat Pesanan</i>\n\n<i>Diharapkan dapat segera mengerjakan sesuai TOC selama $req->toc Hari ($toc_tgl)</i>";

		if($data_j->mitra_id == 22)
		{
			$ci = '519446576';
		}
		else
		{
			$ci = '-1001727682139';
		}

		$params=[
			// 'chat_id' => $mitra->chat_id,
			'chat_id'    => $ci,
			'parse_mode' => 'html',
			'text'       => "$message",
		];
		$ch = curl_init();
		$optArray = array(
			CURLOPT_URL            => $website.'/sendMessage',
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_POST           => 1,
			CURLOPT_POSTFIELDS     => $params,
		);
		curl_setopt_array($ch, $optArray);
		$result = curl_exec($ch);
		curl_close($ch);
	}

	public static function alarm_sp()
	{
		$data = ReportModel::get_boq_data(2);

		if($data)
		{
			$arr_1 = $arr_2 = [];
			foreach($data as $val)
			{
				$botToken = '1902669622:AAHz0cl18e8-IF5waY1uQIj33Nxqq72EzFc';
				$jatuh_tmp = intval($val->tgl_jatuh_tmp);
				$toc = date('Y-m-d', strtotime( ($val->tgl_sp ?? date('Y-m-d') ). ' + ' . $jatuh_tmp . ' day') );
				$selisih = date_diff(date_create($toc), date_create(date('Y-m-d') ) );
				if($val->tgl_jatuh_tmp && $val->step_id == 6)
				{
					$date_form = $selisih->format('%a');

					if($date_form >= 1 && $date_form == 4)
					{
						$arr_1[] = "- $val->judul\nLama Pekerjaan: <b>$date_form hari ($toc)</b>\n";
					}

					if($date_form >= 7 )
					{
						$arr_2[] = "- $val->judul\nLama Pekerjaan: <b>$date_form hari ($toc)</b>\n";
					}
				}
			}
			// dd($arr_1, $arr_2);
			//JAM 8 PAGI CURL NYA
			$website="https://api.telegram.org/bot".$botToken;

			// if($val->mitra_id == 22)
			// {
			// 	$ci = '519446576';
			// }
			// else
			// {
			// 	$ci = '-1001727682139';
			// }
			$ci = '519446576';

			if($arr_1)
			{
				$chunck_ar1 = array_chunk($arr_1, 20);
				$add_msg = '';
				foreach($chunck_ar1 as $v)
				{
					$message_1 = "Pekerjaan Lebih Dari 1 Hari\n";

					foreach($v as $kk => $vv)
					{
						$add_msg .= ++$kk .' '. $vv;
					}

					$message_1 .= $add_msg;

					$params=[
						'chat_id' => $ci,
						// 'chat_id' => '-1001727682139' atau 519446576
						'parse_mode' => 'html',
						'text' => "$message_1",
					];

					$ch = curl_init();
					$optArray = array(
						CURLOPT_URL => $website.'/sendMessage',
						CURLOPT_RETURNTRANSFER => 1,
						CURLOPT_POST => 1,
						CURLOPT_POSTFIELDS => $params,
					);
					curl_setopt_array($ch, $optArray);
					$result = curl_exec($ch);
					curl_close($ch);
				}
			}

			if($arr_2)
			{
				$chunck_ar2 = array_chunk($arr_2, 20);
				$add_msg = '';
				foreach($chunck_ar2 as $v)
				{
					$message_2 = "Pekerjaan Lebih Dari 7 Hari\n";

					foreach($v as $kk => $vv)
					{
						$message_2 .= ++$kk .' '. $vv;
					}

					$params=[
						'chat_id' => $ci,
						// 'chat_id' => '-1001727682139' atau 519446576
						'parse_mode' => 'html',
						'text' => "$message_2",
					];

					$ch = curl_init();
					$optArray = array(
						CURLOPT_URL            => $website.'/sendMessage',
						CURLOPT_RETURNTRANSFER => 1,
						CURLOPT_POST           => 1,
						CURLOPT_POSTFIELDS     => $params,
					);
					curl_setopt_array($ch, $optArray);
					$result = curl_exec($ch);
					curl_close($ch);
				}
			}
		}
	}

	public static function reject_data($req, $id)
	{
		$step_id = 15;

		DB::Table('procurement_boq_upload')->where('id', $id)->update([
			'step_id'       => $step_id,
			'detail_reject' => $req->alasan_reject,
			'modified_by'   => session('auth')->id_user
		]);

		self::save_log($step_id, $id, $req->alasan_reject);
	}

	public static function get_data_rekon(...$id)
	{
		$arr_search[] = ['pbu.active', 1];

		if (!in_array(session('auth')->proc_level, [99, 6, 7, 44]) && !in_array(20, $id) && !in_array(13, $id) )
		{
			$arr_search[] = ['pbu.witel', session('auth')->Witel_New];
		}

		$sql = DB::table('procurement_boq_upload AS pbu')
		->leftjoin('procurement_mitra AS pm', 'pm.id', '=', 'pbu.mitra_id')
		->leftjoin('procurement_step AS ps', 'ps.id', '=', 'pbu.step_id')
		->leftjoin('procurement_nilai_psb AS pns', function($join) {
			$join->on('pbu.id', '=', 'pns.id_dokumen')
			->Where('pns.desc', '=', 'SP');
		})
		->select('pbu.*', 'ps.nama AS nama_step', 'pm.nama_company', 'pns.*',
			DB::RAW("DATEDIFF(NOW(), pbu.tgl_last_update) as jml_hri"),
			DB::RAW("(SELECT nama FROM 1_2_employee WHERE nik = pbu.modified_by GROUP BY nik) As nama_modif")
		)
		->WhereIn('pbu.step_id', $id)
		->where($arr_search);

		if(session('auth')->proc_level == 44)
		{
			$find_witel = DB::Table('promise_witel As w1')
			->leftjoin('promise_witel As w2', 'w2.Regional', '=', 'w1.Regional')
			->where('w1.Witel', session('auth')->Witel_New)
			->get();

			foreach($find_witel as $vw)
			{
				$all_witel[] = $vw->Witel;
			}

			$sql->whereIn('pbu.witel', $all_witel);
		}

		if(session('auth')->peker_pup)
		{
			$pekerjaan = explode(', ', session('auth')->peker_pup );
			$sql->whereIn('pbu.pekerjaan', $pekerjaan);
		}

		if(session('auth')->proc_level == 2 )
		{
			if(!in_array(session('auth')->id_user, [18940469, 20981020]))
			{
				$sql->where('pm.nama_company', session('auth')->mitra_amija_pt);
			}
		}

		$sql->OrderBy('pbu.id', 'DESC');

		return $sql->get();
	}

	// public static function pra_count_boq($id, $sd)
	// {
	// 	$AdminModel = new AdminModel();
	// 	$pph = $AdminModel->find_pph($id);

	// 	return DB::table("procurement_boq_upload As pbu")
	// 	->leftJoin("procurement_Boq_lokasi As pbl", "pbu.id","=","pbl.id_upload")
	// 	->leftJoin("procurement_Boq_design As pbd", "pbl.id","=","pbd.id_boq_lokasi")
	// 	->leftJoin("procurement_mitra As pm", "pm.id","=","pbu.mitra_id")
	// 	->leftJoin("1_2_employee As emp", "emp.nik","=","pbu.created_by")
	// 	->select(
	// 		DB::raw("SUM(CASE WHEN pbd.status_design = 0 THEN pbd.material * pbd.sp END) AS total_material_sp"),
	// 		DB::raw("SUM(CASE WHEN pbd.status_design = 0 THEN pbd.jasa * pbd.sp END) AS total_jasa_sp"),
	// 		DB::raw("SUM(CASE WHEN pbd.status_design = $sd THEN pbd.material * pbd.rekon END) AS total_material_rekon"),
	// 		DB::raw("SUM(CASE WHEN pbd.status_design = $sd THEN pbd.jasa * pbd.rekon END) AS total_jasa_rekon"),
	// 		DB::raw("SUM(CASE WHEN pbd.status_design = $sd THEN pbd.material * pbd.sp END) AS total_material_rek_sp"),
	// 		DB::raw("SUM(CASE WHEN pbd.status_design = $sd THEN pbd.jasa * pbd.sp END) AS total_jasa_rek_sp"),
	// 		DB::raw("(SUM(CASE WHEN pbd.status_design = 0 THEN pbd.material * pbd.sp END) + SUM(CASE WHEN pbd.status_design = 0 THEN pbd.jasa * pbd.sp END)) AS gd_sp"),
	// 		DB::raw("(SUM(CASE WHEN pbd.status_design = $sd THEN pbd.material * pbd.rekon END) + SUM(CASE WHEN pbd.status_design = $sd THEN pbd.jasa * pbd.rekon END)) AS gd_rekon"),
	// 		DB::raw("ROUND(( (SUM( CASE WHEN pbd.status_design = 0 THEN pbd.material * pbd.sp END) + SUM(CASE WHEN pbd.status_design = 0 THEN pbd.jasa * pbd.sp END) ) * $pph) ) AS gd_ppn_sp"),
	// 		DB::raw("ROUND(( (SUM( CASE WHEN pbd.status_design = $sd THEN pbd.material * pbd.rekon END) + SUM(CASE WHEN pbd.status_design = $sd THEN pbd.jasa * pbd.rekon END) ) * $pph) ) AS gd_ppn_rekon"),
	// 		DB::raw("ROUND(( (SUM( CASE WHEN pbd.status_design = 0 THEN pbd.material * pbd.sp END) + SUM(CASE WHEN pbd.status_design = 0 THEN pbd.jasa * pbd.sp END) ) + (SUM(CASE WHEN pbd.status_design = 0 THEN pbd.material * pbd.sp END) + SUM(CASE WHEN pbd.status_design = 0 THEN pbd.jasa * pbd.sp END)) * $pph) ) AS total_sp"),
	// 		DB::raw("ROUND(( (SUM( CASE WHEN pbd.status_design = $sd THEN pbd.material * pbd.rekon END) + SUM(CASE WHEN pbd.status_design = $sd THEN pbd.jasa * pbd.rekon END) ) + (SUM(CASE WHEN pbd.status_design = $sd THEN pbd.material * pbd.rekon END) + SUM(CASE WHEN pbd.status_design = $sd THEN pbd.jasa * pbd.rekon END)) * $pph) ) AS total_rekon") )
	// 	->where('pbu.id', $id)
	// 	->first();
	// }

	public static function save_rekon($id, $req)
	{
		// dd($req->all() );
		return DB::transaction(function () use ($id, $req)
		{
			if($req->status == 16)
			{
				DB::TABLE('procurement_boq_upload')->where('id', $id)->update([
					'step_id'     => $req->status,
					'detail'      => $req->detail,
					'modified_by' => session('auth')->id_user
				]);

				self::save_log($req->status, $id, $req->detail);

				$msg['direct'] = '/progressList/7';
				$msg['isi']['msg'] = [
					'type' => 'danger',
					'text' => 'Data Rekon Berhasil Ditolak!'
				];
			}
			else
			{
				$material = json_decode($req->material_input);

				foreach($material as $v)
				{
					$get_all_id[$v->id_design] = $v->id_design;
				}

				$get_all_id = "'". implode("', '", $get_all_id) . "'";

				$data_boq_plan = DB::SELECT("SELECT pbd.*, pd.uraian, pd.satuan, pbl.sub_jenis_p, pbd.material as material_default, pbd.jasa as jasa_default, pbl.lokasi, pbl.sto, pbl.pid_sto
				FROM procurement_boq_upload pbu
				LEFT JOIN procurement_Boq_lokasi pbl ON pbu.id = pbl.id_upload
				LEFT JOIN procurement_Boq_design pbd ON pbl.id = pbd.id_boq_lokasi
				LEFT JOIN procurement_designator pd ON pd.id = pbd.id_design
				WHERE pbu.id = ".$id." AND pbd.status_design = 0 AND pd.active = 1 AND pbu.active = 1 AND pbd.id_design NOT IN ($get_all_id) GROUP BY pbd.id");

				$data_boq_plan = json_decode(json_encode($data_boq_plan), TRUE);

				foreach($data_boq_plan as $v)
				{
					$dbp_mat[$v['id_design'] ] = $v;
				}

				$get_data_material = AdminModel::get_design();
				$find_material     = json_decode(json_encode($get_data_material), TRUE);

				foreach($material as $k => $val)
				{
					if(!isset($val->lokasi_id) )
					{
						$msg['direct'] = '/progress/'.$id;
						$msg['isi']['msg'] = [
							'type' => 'warning',
							'text' => 'Terjadi kesalahan! Silahkan input ulang!',
						];
						return $msg;
					}

					$check_lok_me = DB::table('procurement_Boq_lokasi')->where([
						['id', $val->lokasi_id],
						['id_upload', $id],
					])->first();

					$find_me = array_search($val->id, array_column($find_material, 'id') );
					$tambah  = ($val->rekon > $val->sp ? $val->rekon - $val->sp : 0);
					$kurang  = ($val->sp > $val->rekon ? $val->sp - $val->rekon : 0);

					$data_rekon[$k]['id']         = $val->id;
					$data_rekon[$k]['nama']       = $get_data_material[$find_me]->designator;
					$data_rekon[$k]['material']   = $val->material;
					$data_rekon[$k]['jasa']       = $val->jasa;
					$data_rekon[$k]['jenis_khs']  = $val->jenis_khs;
					$data_rekon[$k]['sp']         = $val->sp;
					$data_rekon[$k]['rekon']      = $val->rekon;
					$data_rekon[$k]['tambah']     = $tambah;
					$data_rekon[$k]['kurang']     = $kurang;
					$data_rekon[$k]['lokasi']     = $val->lokasi;
					$data_rekon[$k]['lokasi_ada'] = ($check_lok_me ? 'ada' : NULL);
					$data_rekon[$k]['sto']        = $val->sto;
					$data_rekon[$k]['urutan_sto'] = 'LOK'.$val->urutan;

					$count_khs[] = $val->jenis_khs;

					if(empty($val->sto) )
					{
						$msg['direct'] = '/progress/'.$id;
						$msg['isi']['msg'] = [
							'type' => 'warning',
							'text' => 'STO Tidak Boleh Kosong',
						];
						return $msg;
					}

					if(empty($val->sub_jenis) )
					{
						$msg['direct'] = '/progress/'.$id;
						$msg['isi']['msg'] = [
							'type' => 'warning',
							'text' => 'Sub Jenis Tidak Boleh Kosong',
						];
						return $msg;
					}

					if(!trim($val->lokasi))
					{
						$msg['direct'] = '/progress/'.$id;
						$msg['isi']['msg'] = [
							'type' => 'warning',
							'text' => 'Lokasi Tidak Boleh Kosong',
						];
						return $msg;
					}

					if($check_lok_me)
					{
						$id_li = $check_lok_me->id;
					}
					else
					{
						$id_li = 'LOK'.$val->urutan;
					}

					$data_lokasi[$id_li]['lokasi_ada'] = ($check_lok_me ? 'ada': NULL);
					$data_lokasi[$id_li]['urutan_sto'] = 'LOK'.$val->urutan;
					$data_lokasi[$id_li]['sto']        = $val->sto;
					$data_lokasi[$id_li]['lokasi']     = $val->lokasi;
					$data_lokasi[$id_li]['sub_jenis']  = $val->sub_jenis;

					if(!isset($data_lokasi[$id_li]['ttl_material_sp_rekon']) )
					{
						$data_lokasi[$id_li]['ttl_material_sp_rekon'] = 0;
						$data_lokasi[$id_li]['ttl_jasa_sp_rekon']     = 0;
						$data_lokasi[$id_li]['ttl_material_rekon']    = 0;
						$data_lokasi[$id_li]['ttl_jasa_rekon']        = 0;

						$data_lokasi[$id_li]['ttl_materialT_rekon'] = 0;
						$data_lokasi[$id_li]['ttl_materialK_rekon'] = 0;
						$data_lokasi[$id_li]['ttl_jasaT_rekon']     = 0;
						$data_lokasi[$id_li]['ttl_jasaK_rekon']     = 0;
					}

					$data_lokasi[$id_li]['ttl_material_sp_rekon'] += $val->material * $val->sp;
					$data_lokasi[$id_li]['ttl_jasa_sp_rekon']     += $val->jasa * $val->sp;
					$data_lokasi[$id_li]['ttl_material_rekon']    += $val->material * $val->rekon;
					$data_lokasi[$id_li]['ttl_jasa_rekon']        += $val->jasa * $val->rekon;

					$data_lokasi[$id_li]['ttl_materialT_rekon'] += $val->material * $tambah;
					$data_lokasi[$id_li]['ttl_materialK_rekon'] += $val->material * $kurang;
					$data_lokasi[$id_li]['ttl_jasaT_rekon']     += $val->jasa * $tambah;
					$data_lokasi[$id_li]['ttl_jasaK_rekon']     += $val->jasa * $kurang;

					$find_unused = array_keys(array_column($data_boq_plan, 'id_boq_lokasi'), $val->lokasi_id);
					// dd($data_boq_plan);
					foreach($find_unused as $v)
					{
						$data_dbp = $data_boq_plan[$v];

						if($data_dbp['id_boq_lokasi'] == $val->lokasi_id)
						{
							$data_rekon[$data_dbp['id_design'] . '_'. $val->urutan]['id']         = $data_dbp['id_design'];
							$data_rekon[$data_dbp['id_design'] . '_'. $val->urutan]['nama']       = $data_dbp['designator'];
							$data_rekon[$data_dbp['id_design'] . '_'. $val->urutan]['material']   = $data_dbp['material'];
							$data_rekon[$data_dbp['id_design'] . '_'. $val->urutan]['jasa']       = $data_dbp['jasa'];
							$data_rekon[$data_dbp['id_design'] . '_'. $val->urutan]['jenis_khs']  = $data_dbp['jenis_khs'];
							$data_rekon[$data_dbp['id_design'] . '_'. $val->urutan]['sp']         = $data_dbp['sp'];
							$data_rekon[$data_dbp['id_design'] . '_'. $val->urutan]['rekon']      = $data_dbp['rekon'];
							$data_rekon[$data_dbp['id_design'] . '_'. $val->urutan]['tambah']     = 0;
							$data_rekon[$data_dbp['id_design'] . '_'. $val->urutan]['kurang']     = $data_dbp['sp'];
							$data_rekon[$data_dbp['id_design'] . '_'. $val->urutan]['lokasi']     = $data_dbp['lokasi'];
							$data_rekon[$data_dbp['id_design'] . '_'. $val->urutan]['lokasi_ada'] = 'ada';
							$data_rekon[$data_dbp['id_design'] . '_'. $val->urutan]['sto']        = $data_dbp['sto'];
							$data_rekon[$data_dbp['id_design'] . '_'. $val->urutan]['urutan_sto'] = 'LOK'.$val->urutan;

							$data_lokasi[$id_li]['ttl_materialK_rekon'] += $data_dbp['material'] * $data_dbp['sp'];
							$data_lokasi[$id_li]['ttl_jasaK_rekon']     += $data_dbp['jasa'] * $data_dbp['sp'];
						}
					}

					// if($val->lokasi_id == 'NEW')
					// {
					// 	foreach($dbp_mat as $v)
					// 	{
					// 		$data_rekon[$v['id_design'] . '_'. $val->urutan]['id']         = $v['id_design'];
					// 		$data_rekon[$v['id_design'] . '_'. $val->urutan]['nama']       = $v['designator'];
					// 		$data_rekon[$v['id_design'] . '_'. $val->urutan]['material']   = $v['material'];
					// 		$data_rekon[$v['id_design'] . '_'. $val->urutan]['jasa']       = $v['jasa'];
					// 		$data_rekon[$v['id_design'] . '_'. $val->urutan]['jenis_khs']  = $v['jenis_khs'];
					// 		$data_rekon[$v['id_design'] . '_'. $val->urutan]['sp']         = 0;
					// 		$data_rekon[$v['id_design'] . '_'. $val->urutan]['rekon']      = 0;
					// 		$data_rekon[$v['id_design'] . '_'. $val->urutan]['tambah']     = 0;
					// 		$data_rekon[$v['id_design'] . '_'. $val->urutan]['kurang']     = 0;
					// 		$data_rekon[$v['id_design'] . '_'. $val->urutan]['lokasi']     = $v['lokasi'];
					// 		$data_rekon[$v['id_design'] . '_'. $val->urutan]['lokasi_ada'] = NULL;
					// 		$data_rekon[$v['id_design'] . '_'. $val->urutan]['sto']        = $v['sto'];
					// 		$data_rekon[$v['id_design'] . '_'. $val->urutan]['urutan_sto'] = 'LOK'.$val->urutan;
					// 	}
					// }

					if(!isset($data_lokasi[$id_li]['jml_vol']) )
					{
						$data_lokasi[$id_li]['jml_vol'] = 0;
					}

					$data_lokasi[$id_li]['jml_vol'] += $val->rekon;
				}

				$data_rekon = array_values($data_rekon);

				foreach($material as $v)
				{
					$find_me = array_search($v->id, array_column($find_material, 'id') );

					$check_dup[$v->id .'_'. $v->material .'_'. $v->jasa .'_'. $v->urutan]['mat'] = $find_material[$find_me]['designator'];

					if(!isset($check_dup[$v->id .'_'. $v->material .'_'. $v->jasa .'_'. $v->urutan]['jml']) )
					{
						$check_dup[$v->id .'_'. $v->material .'_'. $v->jasa .'_'. $v->urutan]['jml'] = 0;
					}

					$check_dup[$v->id .'_'. $v->material .'_'. $v->jasa .'_'. $v->urutan]['jml'] += 1;
				}

				foreach($check_dup as $k => $v)
				{
					if($v['jml'] > 1)
					{
						$msg['direct'] = '/progress/'.$id;
						$msg['isi']['msg'] = [
							'type' => 'danger',
							'text' => $v['mat'].' terdeteksi dimasukkan dengan menggunakan <b><u>material</u></b> dan <b><u>jasa</u></b> yang sama sebanyak 2 kali!'
						];
						return $msg;
					}
				}

				if(array_sum($count_khs) != 0)
				{
					$check_pelurusan = DB::table('procurement_rfc_osp As pro')->where([
						['id_upload', $id],
						['active', 1],
						['status_rfc', 'TIDAK LURUS']
					])->get();
				}

				$extra_msg = '';

				// if($req->jns_btn == 'sukses')
				// {
				// 	if(array_sum($count_khs) != 0 && count($check_pelurusan) != 0 )
				// 	{
				// 		$req->status = 8;
				// 		$extra_msg .= 'Tetapi masuk pelurusan RFC karena RFC tidak lurus';
				// 	}
				// 	else
				// 	{
				// 		$req->status = 9;
				// 	}
				// }
				// else
				// {
				// 	$req->status = 8;
				// 	$extra_msg .= 'Tetapi masuk pelurusan RFC karena Disubmit!';
				// }
				$req->status = 9;

				// dd($data_rekon, $data_lokasi);
				$pesan = self::save_boq($req, [], $id, $data_rekon, $data_lokasi, 1);

				self::verifikasi_dok($req, $id);

				if($pesan != 'ok')
				{
					$msg['direct'] = '/progress/'.$id;
					$msg['isi']['msg'] = [
						'type' => $pesan['alerts']['type'],
						'text' => $pesan['alerts']['text'],
					];

					return $msg;
				}

				DB::Table('procurement_boq_upload')->where('id', $id)->update([
					'step_id' => $req->status,
				]);

				self::save_log($req->status, $id);

				$msg['direct'] = '/progressList/7';
				$msg['isi']['msg'] = [
					'type' => 'success',
					'text' => 'Data Rekon Berhasil Disetujui! '.$extra_msg,
				];
			}

			return $msg;
		});
	}

	private static function verifikasi_dok($req, $id)
  {
    $path = public_path() . '/upload2/' . $id . '/dokumen_verifikasi/';

    if (!file_exists($path) ) {
      if (!mkdir($path, 0770, true)) {
        return 'gagal menyiapkan folder lampiran Amandemen';
      }
    }

		$nama_file_upload = [
			'ba_kpi',
			'ba_gr',
			'scan_boq_rek',
			'bar_telkom',
			'bar_mitratel',
			'npk',
			'ba_cut',
			'ba_ct',
			'ba_ut',
			'ba_redesign',
		];

		foreach ($nama_file_upload as $k => $nama_file) {
			if ($req->hasFile($nama_file)) {
				$file = $req->file($nama_file);
				try {
					$filename = strtolower( $file->getClientOriginalExtension() );
					$file->move("$path", "$filename");
				} catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
					return 'gagal menyimpan File Verifikasi';
				}
			}
		}
  }

	public static function save_log($id, $upload_id, $detail = null)
	{
		self::progress_save_log($id, $upload_id, $detail);
	}

	public static function save_log_rollback($id, $upload_id, $detail = null)
	{
		self::progress_save_log($id, $upload_id, $detail, 'rollback');
	}

	public static function progress_save_log($id, $upload_id, $detail, $status = 'normal')
	{
		$get           = self::get_step_by_id($id);
		$check_log     = DB::table('procurement_rekon_log')->where('id_upload', $upload_id)->get();
		$get_last_data = '';


		if(count($check_log) == 0 )
		{
			$created_at = date('Y-m-d H:i:s');
			$closed_at = date('Y-m-d H:i:s');
		}
		else
		{
			$get_last_data = DB::table('procurement_rekon_log')
			->where('id_upload', $upload_id)
			->OrderBy('id', 'DESC')
			->limit(1)
			->first();

			$created_at = $get_last_data->closed_at;
			$closed_at = date('Y-m-d H:i:s');
		}

		$isrollback = 0;

		if($status == 'normal')
		{
			$status_stp = $get->action;
		}
		else
		{
			$isrollback = 1;
			$status_stp = 'Rollback';
		}

		DB::table('procurement_boq_upload')->where('id', $upload_id)->update([
			'isrollback' => $isrollback,
			'tgl_last_update' => date('Y-m-d H:i:s'),
			'detail' => $detail
		]);

		DB::table('procurement_rekon_log')->Insert([
			'id_upload'   => $upload_id,
			'step_id'     => $get->id,
			'aktor'       => $get->aktor,
			'keterangan'  => $get->nama,
			'detail'      => $detail,
			'status_step' => $status_stp,
			'created_at'  => $created_at,
			'closed_at'   => $closed_at,
			'created_by'  => session('auth')->id_user
		]);

		//mehual
		$botToken = '1902669622:AAHz0cl18e8-IF5waY1uQIj33Nxqq72EzFc';
		$website="https://api.telegram.org/bot".$botToken;

		$data = ReportModel::get_boq_data($upload_id);
		// dd($data);

		$log_first = DB::table('procurement_rekon_log as prl')
		->leftjoin('procurement_step As ps', 'ps.id', '=', 'prl.step_id')
		->select('prl.*', 'ps.step_after', 'ps.action')
		->where('id_upload', $upload_id)
		->OrderBy('id', 'DESC')
		->limit(1)
		->first();

		$next_step = self::get_step_by_id($log_first->step_after);
		// dd($next_step,$log_first->step_after);
		if ($next_step == null)
		{
			$aktor_nama = 'FINISH';
			$nama = 'Selesai';
		} else {
			$aktor_nama = $next_step->aktor_nama;
			$nama = $next_step->nama;
		}

		// dd($aktor_nama, $nama, $data, $upload_id);

		$get_update = DB::table('1_2_employee As mp')->where('nik',  session('auth')->id_user)->first();
		$message = "<u>$aktor_nama</u> | <b>" .preg_replace('/^PT/', 'PT.', $data->mitra_nm) . "</b> : \n";
		$message .= "Judul Pekerjaan: $data->judul \n";
		$message .= "Loker: $get->nama \n";
		$message .= "Loker Selanjutnya: $nama \n";
		$message .= "User Update: $get_update->nama ($get_update->nik) \n";

		if($log_first->detail)
		{
			$message .= "Catatan: <b>$log_first->detail</b>\n";
		}

		if(in_array($id, [7, 9]) )
		{
			$message .="Berikut List RFC yang diinput:\n";
			$data_rfc =  DB::Table('procurement_rfc_osp')->where([
				['id_upload', $upload_id],
				['active', 1]
			])->GroupBy('rfc')
			->get();

			foreach($data_rfc as $no => $val)
			{
				$nomor = ++$no;
				$message .= $nomor .'. '. $val->rfc . "\n";
			}
		}

		if($log_first->action != 'stay')
		{
			$message .="Silahkan <b>$aktor_nama</b> Untuk Melanjutkan ketahap $nama \n";
		}
		else
		{
			$message .="Silahkan Menunggu <b>$aktor_nama</b> Untuk Memproses Pekerjaannya \n";
		}

		if($data->mitra_id == 22)
		{
			$ci = '519446576';
		}
		else
		{
			$ci = '-1001727682139';
		}

		$params=[
			'chat_id'    => $ci,
			'parse_mode' => 'html',
			'text'       => "$message",
		];

		$ch = curl_init();
		$optArray = array(
			CURLOPT_URL            => $website.'/sendMessage',
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_POST           => 1,
			CURLOPT_POSTFIELDS     => $params,
		);
		curl_setopt_array($ch, $optArray);
		$result = curl_exec($ch);
		curl_close($ch);
	}

	public static function save_rekon_final($req, $id)
	{
		$detail = $req->detail;
		if($req->jns_btn != 'save_me')
		{
			// if($req->status == 'REJECT_REG')
			// {
			// 	$step_id = 17;

			// 	$get_detail = ReportModel::get_boq_data($id);

			// 	$detail .= ' || '.$get_detail->detail;

			// 	$msg['msg'] = [
			// 		'type' => 'danger',
			// 		'text' => 'Evaluasi Rekon dari Regional Ke Mitra Berhasil Disubmit!'
			// 	];
			// }

			// if($req->status == 'REJECT')
			// {
			// 	$step_id = 17;

			// 	$msg['msg'] = [
			// 		'type' => 'danger',
			// 		'text' => 'Data Rekon Berhasil Ditolak!'
			// 	];
			// }

			if($req->status == 'APPROVE')
			{
				$step_id = 11;
				$msg['msg'] = [
					'type' => 'success',
					'text' => 'Data Rekon Berhasil Lanjutkan Ke Mitra Untuk Pelengkapan Dokumen!'
				];
			}
		}
		else
		{
			$step_id = 31;
			$msg['msg'] = [
				'type' => 'success',
				'text' => 'Data Rekon Berhasil Disimpan!'
			];
		}

		$data['BAUT']     = $req->baut;
		$data['tgl_baut'] = $req->tgl_baut;

		if($req->amd_pks)
		{
			$data['amd_pks']     = $req->amd_pks;
			$data['tgl_amd_pks'] = $req->tgl_amd_pks;
		}

		if($req->amd_sp)
		{
			$data['amd_sp']     = $req->amd_sp;
			$data['tgl_amd_sp'] = $req->tgl_amd_sp;
		}

		$data['detail']       = $detail;
		$data['step_id']      = $step_id;
		$data['BAST']         = $req->bast;
		$data['tgl_bast']     = $req->tgl_bast;
		$data['ba_abd']       = $req->ba_abd;
		$data['tgl_ba_abd']   = $req->tgl_ba_abd;
		$data['ba_rekon']     = $req->ba_rekon;
		$data['tgl_ba_rekon'] = $req->tgl_ba_rekon;
		$data['BAPP']         = $req->bapp;
		$data['tgl_bapp']     = $req->tgl_bapp;
		$data['modified_by']  = session('auth')->id_user;
		// dd($data);
		DB::table('procurement_boq_upload')->where('id', $id)->update($data);
		self::save_log($step_id, $id, $detail);

		return $msg;
	}

	public static function save_rekon_reg($req)
	{
		if($req->status == 'REJECT')
		{
			$step_id = 18;

			$msg['msg'] = [
				'type' => 'danger',
				'text' => 'Data Rekon Berhasil Ditolak!'
			];
		}
		else
		{
			$step_id = 13;
			$msg['msg'] = [
				'type' => 'success',
				'text' => 'Data Rekon Selesai!'
			];

			// $get_nilai = DB::SELECT("SELECT pp.*,
			// (SELECT SUM(budget_up) FROM procurement_req_pid
			//  LEFT JOIN procurement_boq_upload pbu ON pbu.id = procurement_req_pid.id_upload WHERE pbu.id = $req->id_boq_up GROUP BY pbu.id ) as budget_terpakai
			// FROM procurement_pid As pp
			// LEFT JOIN procurement_req_pid prp ON prp.id_pid_req = pp.id
			// WHERE prp.id_upload = $req->id_boq_up
			// GROUP BY pp.id, prp.id_upload");

			// foreach ($get_nilai as $val)
			// {
			// 	DB::Table('procurement_pid As pp')->where('id', $val->id)->update([
			// 		'budget' => $val->budget - $val->budget_terpakai
			// 	]);
			// }
		}

		$no_resi_return = $req->no_resi_return ?? null;
		$modified_by = session('auth')->id_user;

		DB::SELECT("UPDATE procurement_boq_upload pbu
			LEFT JOIN procurement_Boq_lokasi pbl ON pbu.id = pbl.id_upload
			SET detail = '$req->detail', pbu.step_id = $step_id, no_resi_return = '$no_resi_return', pbu.modified_by = '$modified_by'
			WHERE pbu.id = $req->id_boq_up"
		);

		self::save_log($step_id, $req->id_boq_up, $req->detail);

		return $msg;
	}

	public static function save_rekon_finance($req)
	{
		if($req->status == 'REJECT')
		{
			$step_id = 12;

			$msg['msg'] = [
				'type' => 'danger',
				'text' => 'Data Rekon Berhasil Ditolak!'
			];
		}
		else
		{
			$step_id = 14;
			$msg['msg'] = [
				'type' => 'success',
				'text' => 'Data Rekon Selesai!'
			];
		}

		$no_resi_return = $req->no_resi_return ?? null;
		$modified_by = session('auth')->id_user;

		DB::Table('procurement_boq_upload')->where('id', $req->id_boq_up)->update([
			'detail'         => $req->detail,
			'step_id'        => $step_id,
			'no_resi_return' => $no_resi_return,
			'modified_by'    => $modified_by,
			'tgl_selesai'    => date('Y-m-d H:i:s')
		]);

		self::save_log($step_id, $req->id_boq_up, $req->detail);

		return $msg;
	}

	// public static function amand()
	// {
	// 	return DB::table('procurement_req_pid As prp')
	// 	->leftjoin('procurement_pid As pp', 'pp.id_req', '=', 'prp.id')
	// 	->leftjoin('procurement_boq_upload As pbu', 'pbu.id', '=', 'prp.id_upload')
	// 	->leftjoin('procurement_Boq_lokasi As pbl', 'pbl.id_upload', '=', 'prp.id_upload')
	// 	->select('prp.*', DB::RAW("(SELECT GROUP_CONCAT(DISTINCT(procurement_pid.pid) ORDER BY procurement_pid.id DESC SEPARATOR ', ') As pid_get FROM procurement_pid WHERE id_req = pp.id_req) AS pid_get"), 'pbu.judul', 'pp.created_at as created_pp')
	// 	->where('pbl.status', '=', 'AMD')
	// 	->orderBy('pp.id', 'DESC')
	// 	->GroupBy('pp.id_req')
	// 	->get();
	// }

	// public static function upload_amand($req)
	// {
	// 	//id boq upload
	// 	DB::transaction(function () use ($req)
	// 	{
	// 		//GANTI JANGAN PP jadi utamanya
	// 		$data = DB::table('procurement_req_pid as prp')
	// 		->leftjoin('procurement_pid as pp', 'prp.id', '=', 'pp.id_req')
	// 		->select('prp.id_upload')
	// 		->where('prp.id', $req->id_req_pid)
	// 		->GroupBy('pp.id_req')
	// 		->first();

	// 		DB::SELECT(
	// 			"UPDATE procurement_boq_upload pbu
	// 			LEFT JOIN procurement_Boq_lokasi pbl ON pbu.id = pbl.id_upload
	// 			SET pbu.status = 'REQ_BUDGET', pbl.status = 'REQ_BUDGET'
	// 			WHERE pbu.id = $data->id_upload AND pbl.step_id = 7 "
	// 		);

	// 		DB::table('procurement_rekon_log')->insert([
	// 			'id_upload' => $data->id_upload,
	// 			'step' => 1,
	// 			'aktor' => 1,
	// 			'keterangan' => 'Upload Lampiran Amandemen',
	// 			'created_by' => session('auth')->id_user
	// 		]);

	// 		self::handleAmand_lamp($req, $data->id_upload, 'amandemen_lampiran');
	// 	});
	// }

	private static function handleAmand_lamp($req, $id, $path)
  {
    $path = public_path() . '/upload2/' . $id . '/' . $path . '/';

    if (!file_exists($path)) {
      if (!mkdir($path, 0770, true)) {
        return 'gagal menyiapkan folder lampiran Amandemen';
      }
    }

    if ($req->hasFile('upload_aman_lamp')) {
      $file = $req->file('upload_aman_lamp');
      try {
        $filename = 'File Lampiran.'.strtolower( $file->getClientOriginalExtension() );
        $file->move("$path", "$filename");
      } catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
        return 'gagal menyimpan sp';
      }
    }
  }

	public static function get_step_by_id($id)
	{
		return DB::table('procurement_step')->where('id', $id)->first();
	}

	public static function get_steps_by_urutan($id)
	{
		return DB::table('procurement_step')->select('id', 'select2_name As text', 'procurement_step.*')->where('urutan', 'like', $id.'%')->get();
	}

	// public static function request_ba()
	// {
	// 	return DB::table('procurement_boq_upload As pbu')
	// 	->select('pbu.*', 'pm.nama_company')
	// 	->leftjoin('procurement_mitra As pm', 'pm.id', '=', 'pbu.mitra_id')
	// 	->where([
	// 		['step_id', 12],
	// 		["pbu.active", 1]
	// 	])
	// 	->where(function($join){
	// 		$join->whereNull('BAUT')
	// 		->OrwhereNull('BAST')
	// 		->OrwhereNull('ba_abd')
	// 		->OrwhereNull('ba_rekon')
	// 		->OrwhereNull('surat_penetapan')
	// 		->OrwhereNull('tgl_s_pen')
	// 		->OrwhereNull('surat_kesanggupan')
	// 		->OrwhereNull('no_sp')
	// 		->OrwhereNull('tgl_sp')
	// 		->OrwhereNull('surat_penetapan');
	// 	})
	// 	->OrderBy('created_at', 'DESC')
	// 	->get();
	// }

	public static function excel_dashboard($witel, $mitra, $khs, $period, $jenis_tanggal, $tgl, $jenis)
	{
		if($jenis == 'data')
		{
			$isi = DB::table('procurement_boq_upload As pbu')
			->leftjoin('procurement_mitra As pm', 'pbu.mitra_id', '=', 'pm.id')
			->leftjoin('procurement_step As ps', 'pbu.step_id', '=', 'ps.id')
			->leftjoin('procurement_step As ps2', 'ps2.id', '=', 'ps.step_after')
			->leftjoin('procurement_step As ps3', 'ps3.id', '=', 'ps2.step_after')
			->select('pbu.*', 'ps.nama as step_before', 'ps2.nama as step_now', 'ps3.nama as step_after', 'pm.witel as witel_mitra', DB::RAW("DATEDIFF(NOW(), pbu.tgl_last_update) as jml_hri") )
			->where('pbu.active', 1);
		}
		else
		{
			$isi = DB::table('procurement_rekon_log As prl')
			->leftjoin('procurement_step As ps', 'ps.id', '=', 'prl.step_id')
			->leftjoin('procurement_boq_upload As pbu', 'pbu.id', '=', 'prl.id_upload')
			->leftjoin('procurement_mitra As pm', 'pbu.mitra_id', '=', 'pm.id')
			->select('pbu.judul', 'pbu.pekerjaan', 'pbu.jenis_work', 'pbu.mitra_nm', 'pm.witel as witel_mitra', 'prl.created_by', 'prl.detail', 'ps.*')
			->where('pbu.active', 1);
		}

		if(strcasecmp($witel, 'All') != 0)
		{
			$isi->where('pbu.witel', 'LIKE', '%'.$witel.'%');
		}

		if(strcasecmp($period, 'All') != 0)
		{
			if($period == 'sp')
			{
				switch ($jenis_tanggal) {
					case 'Tahun':
						$isi->where('pbu.created_at', 'LIKE', '%'.$tgl.'%');
					break;
					case 'Custom':
						$tgl = explode(' - ', $tgl);
						$isi->whereBetween(DB::raw("DATE_FORMAT(pbu.tgl_sp, '%Y-%m-%d')"), $tgl);
					break;
					case 'Bulan':
						$isi->whereBetween(DB::raw("MONTH(pbu.created_at)"), $tgl);
					break;
				}
			}
			else
			{
				$tgl_progress_kerja = explode(' - ', $tgl);
				$isi->whereBetween(DB::raw("DATE_FORMAT(pbu.tgl_last_update, '%Y-%m-%d')"), $tgl_progress_kerja);
			}
		}

		if(!empty($khs) && strcasecmp($khs, 'All') != 0)
		{
			$isi->where('pbu.pekerjaan', $khs);
		}

		if(!empty($mitra) && strcasecmp($mitra, 'All') != 0)
		{
			if(!is_numeric($mitra) )
			{
				$isi->where('mitra_nm', 'LIKE', '%'.$mitra.'%');
			}
			else
			{
				$isi->where('mitra_id', $mitra);
			}
		}
		$query = $isi->get();
		$data = [];

		if($jenis == 'data')
		{
			$head = [
				'Uraian',
				'Pekerjaan',
				'Total SP',
				'Total Rekon',
				'Jenis Pekerjaan',
				'Surat Kesanggupan',
				'Tanggal Surat Kesanggupan',
				'Surat Penetapan',
				'Tanggal Surat Penetapan',
				'Nama Mitra',
				'Witel Mitra',
				'PKS/Kontrak',
				'Amandemen PKS/Kontrak',
				'Tanggal Amandemen PKS/Kontrak',
				'Nomor Surat Pesanan',
				'Tanggal Surat Pesanan',
				'Amandemen Surat Pesanan',
				'Tanggal Amandemen Surat Pesanan',
				'Nomor SPP',
				'Nomor Receipt',
				'Nomor BAUT',
				'Nomor BAST',
				'Nomor BA ABD',
				'Nomor BA Rekon',
				'Tanggal Buat',
				'Tanggal Modifikasi',
				'Umur',
				'Loker Sebelum',
				'Loker Sekarang',
				'Loker Selanjutnya'
			];

			foreach ($query as $key => $val) {
				$data[$key]['judul']             = $val->judul;
				$data[$key]['pekerjaan']         = $val->pekerjaan;
				$data[$key]['total_sp']          = $val->total_material_sp + $val->total_jasa_sp;
				$data[$key]['total_rekon']       = $val->total_material_rekon + $val->total_jasa_rekon;
				$data[$key]['jenis_work']        = $val->jenis_work;
				$data[$key]['surat_kesanggupan'] = $val->surat_kesanggupan;
				$data[$key]['tgl_surat_sanggup'] = $val->tgl_surat_sanggup;
				$data[$key]['surat_penetapan']   = $val->surat_penetapan;
				$data[$key]['tgl_s_pen']         = $val->tgl_s_pen;
				$data[$key]['nama_company']      = $val->mitra_nm;
				$data[$key]['witel_mitra']       = $val->witel_mitra;
				$data[$key]['pks']               = $val->pks;
				$data[$key]['amd_pks']           = $val->amd_pks;
				$data[$key]['tgl_amd_pks']       = $val->tgl_amd_pks;
				$data[$key]['no_sp']             = $val->no_sp;
				$data[$key]['tgl_sp']            = $val->tgl_sp;
				$data[$key]['amd_sp']            = $val->amd_sp;
				$data[$key]['tgl_amd_sp']        = $val->tgl_amd_sp;
				$data[$key]['spp_num']           = $val->spp_num;
				$data[$key]['receipt_num']       = $val->receipt_num;
				$data[$key]['BAUT']              = $val->BAUT;
				$data[$key]['BAST']              = $val->BAST;
				$data[$key]['ba_abd']            = $val->ba_abd;
				$data[$key]['ba_rekon']          = $val->ba_rekon;
				$data[$key]['created_at']        = $val->created_at;
				$data[$key]['tgl_last_update']   = $val->tgl_last_update;
				$data[$key]['umur']              = $val->jml_hri;
				$data[$key]['step_before']       = $val->step_before;
				$data[$key]['step_now']          = $val->step_now;
				$data[$key]['step_after']        = $val->step_after;
			}
		}
		else
		{
			$head = [
				'Uraian',
				'Pekerjaan',
				'Jenis Pekerjaan',
				'Mitra',
				'Witel',
				'Aktor',
				'Keterangan',
				'created_by',

			];
			foreach ($query as $key => $val) {
				$data[$key]['judul']        = $val->judul;
				$data[$key]['pekerjaan']    = $val->pekerjaan;
				$data[$key]['jenis_work']   = $val->jenis_work;
				$data[$key]['nama_company'] = $val->mitra_nm;
				$data[$key]['witel_mitra']  = $val->witel_mitra;
				$data[$key]['aktor_nama']   = $val->aktor_nama;
				$data[$key]['nama']         = $val->nama;
				$data[$key]['detail']       = $val->detail;
				$data[$key]['created_by']   = $val->created_by;
			}
		}

		return ['head'=> $head, 'data' => $data];
	}

	public static function saveCreateJustifikasiPSB($req)
	{
		$bulan = DB::table('bulan_format')->where('id', $req->input('periode_kegiatan'))->first();
		$mitra = AdminModel::get_mitra($req->input('mitra') );

		$jml_pekerjaan_psb =
			($req->input('p1_survey_ssl') * str_replace(".", "", $req->input('p1_survey_hss'))) +
			($req->input('p2_tlpint_survey_ssl') * str_replace(".", "", $req->input('p2_tlpint_survey_hss'))) +
			($req->input('p2_intiptv_survey_ssl') * str_replace(".", "", $req->input('p2_intiptv_survey_hss'))) +
			($req->input('p3_survey_ssl') * str_replace(".", "", $req->input('p3_survey_hss'))) +
			($req->input('p1_ssl') * str_replace(".", "", $req->input('p1_hss'))) +
			// ($req->input('p2_ssl') * str_replace(".", "", $req->input('p2_hss'))) +
			($req->input('p2_tlpint_ssl') * str_replace(".", "", $req->input('p2_tlpint_hss'))) +
			($req->input('p2_intiptv_ssl') * str_replace(".", "", $req->input('p2_intiptv_hss'))) +
			($req->input('p3_ssl') * str_replace(".", "", $req->input('p3_hss')));

		$jml_pekerjaan_migrasi =
			($req->input('migrasi_service_1p2p_ssl') * str_replace(".", "", $req->input('migrasi_service_1p2p_hss'))) +
			($req->input('migrasi_service_1p3p_ssl') * str_replace(".", "", $req->input('migrasi_service_1p3p_hss'))) +
			($req->input('migrasi_service_2p3p_ssl') * str_replace(".", "", $req->input('migrasi_service_2p3p_hss')));

		$jml_pekerjaan_tambahan =
			($req->input('ikr_addon_stb_ssl') * str_replace(".", "", $req->input('ikr_addon_stb_hss'))) +
			($req->input('change_stb_ssl') * str_replace(".", "", $req->input('change_stb_hss'))) +
			($req->input('wifiextender_ssl') * str_replace(".", "", $req->input('wifiextender_hss'))) +
			($req->input('indihome_smart_ssl') * str_replace(".", "", $req->input('indihome_smart_hss'))) +
			($req->input('ont_premium_ssl') * str_replace(".", "", $req->input('ont_premium_hss'))) +
			($req->input('pu_s7_140_ssl') * str_replace(".", "", $req->input('pu_s7_140_hss'))) +
			($req->input('pu_s9_140_ssl') * str_replace(".", "", $req->input('pu_s9_140_hss'))) +
			($req->input('lme_wifi_pt1_ssl') * str_replace(".", "", $req->input('lme_wifi_pt1_hss'))) +
			($req->input('lme_ap_indoor_ssl') * str_replace(".", "", $req->input('lme_ap_indoor_hss'))) +
			($req->input('lme_ap_outdoor_ssl') * str_replace(".", "", $req->input('lme_ap_outdoor_hss')));

		// $total_sp = substr_replace(floor($jml_pekerjaan_psb + $jml_pekerjaan_tambahan), '000', -3, 3);
		$total_sp = ($jml_pekerjaan_psb + $jml_pekerjaan_migrasi +$jml_pekerjaan_tambahan);

		$id = DB::table('procurement_boq_upload')->insertGetId([
			'judul'       => "PEKERJAAN PASANG SAMBUNGAN BARU (PSB) PERIODE ".$bulan->bulan. " ".date('Y'). " WITEL ".strtoupper($mitra->witel)."",
			'pekerjaan'   => "PSB",
			'jenis_work'  => "Pasang Sambungan Baru (PSB)",
			'witel'       => session('auth')->Witel_New,
			'step_id'     => 5,
			'mitra_id'    => $req->input('mitra'),
			'mitra_nm'    => $mitra->nama_company,
			'tanggal_boq' => $req->input('tanggal_terbit'),
			'id_project'  => $req->input('id_project'),
			'created_by'  => session('auth')->id_user
		]);

		DB::table('procurement_nilai_psb')->insert([
			'id_dokumen'               => $id,
			'desc'                     => 'SP',
			'periode_kegiatan'         => $req->input('periode_kegiatan'),
			'rekon_periode1'           => $req->input('rekon_periode1'),
			'rekon_periode2'           => $req->input('rekon_periode2'),
			'ppn_numb'                 => $req->input('ppn_numb'),
			'total_nilai_sp'           => $total_sp,
			'persentase_7_kpi'         => 100,
			'p1_survey_hss'            => str_replace(".", "", $req->input('p1_survey_hss')),
			'p1_survey_ssl'            => $req->input('p1_survey_ssl'),
			'p2_tlpint_survey_hss'     => str_replace(".", "", $req->input('p2_tlpint_survey_hss')),
			'p2_tlpint_survey_ssl'     => $req->input('p2_tlpint_survey_ssl'),
			'p2_intiptv_survey_hss'    => str_replace(".", "", $req->input('p2_intiptv_survey_hss')),
			'p2_intiptv_survey_ssl'    => $req->input('p2_intiptv_survey_ssl'),
			'p3_survey_hss'            => str_replace(".", "", $req->input('p3_survey_hss')),
			'p3_survey_ssl'            => $req->input('p3_survey_ssl'),
			'p1_hss'                   => str_replace(".", "", $req->input('p1_hss')),
			'p1_ssl'                   => $req->input('p1_ssl'),
			// 'p2_hss'                => str_replace(".", "", $req->input('p2_hss')),
			// 'p2_ssl'                => $req->input('p2_ssl'),
			'p2_tlpint_hss'            => str_replace(".", "", $req->input('p2_tlpint_hss')),
			'p2_tlpint_ssl'            => $req->input('p2_tlpint_ssl'),
			'p2_intiptv_hss'           => str_replace(".", "", $req->input('p2_intiptv_hss')),
			'p2_intiptv_ssl'           => $req->input('p2_intiptv_ssl'),
			'p3_hss'                   => str_replace(".", "", $req->input('p3_hss')),
			'p3_ssl'                   => $req->input('p3_ssl'),
			'migrasi_service_1p2p_hss' => str_replace(".", "", $req->input('migrasi_service_1p2p_hss')),
			'migrasi_service_1p2p_ssl' => $req->input('migrasi_service_1p2p_ssl'),
			'migrasi_service_1p3p_hss' => str_replace(".", "", $req->input('migrasi_service_1p3p_hss')),
			'migrasi_service_1p3p_ssl' => $req->input('migrasi_service_1p3p_ssl'),
			'migrasi_service_2p3p_hss' => str_replace(".", "", $req->input('migrasi_service_2p3p_hss')),
			'migrasi_service_2p3p_ssl' => $req->input('migrasi_service_2p3p_ssl'),
			'lme_wifi_pt1_hss'         => str_replace(".", "", $req->input('lme_wifi_pt1_hss')),
			'lme_wifi_pt1_ssl'         => $req->input('lme_wifi_pt1_ssl'),
			'lme_ap_indoor_hss'        => str_replace(".", "", $req->input('lme_ap_indoor_hss')),
			'lme_ap_indoor_ssl'        => $req->input('lme_ap_indoor_ssl'),
			'lme_ap_outdoor_hss'       => str_replace(".", "", $req->input('lme_ap_outdoor_hss')),
			'lme_ap_outdoor_ssl'       => $req->input('lme_ap_outdoor_ssl'),
			'indihome_smart_hss'       => str_replace(".", "", $req->input('indihome_smart_hss')),
			'indihome_smart_ssl'       => $req->input('indihome_smart_ssl'),
			'plc_hss'                  => str_replace(".", "", $req->input('plc_hss')),
			'plc_ssl'                  => $req->input('plc_ssl'),
			'ikr_addon_stb_hss'        => str_replace(".", "", $req->input('ikr_addon_stb_hss')),
			'ikr_addon_stb_ssl'        => $req->input('ikr_addon_stb_ssl'),
			'change_stb_hss'           => str_replace(".", "", $req->input('change_stb_hss')),
			'change_stb_ssl'           => $req->input('change_stb_ssl'),
			'ont_premium_hss'          => str_replace(".", "", $req->input('ont_premium_hss')),
			'ont_premium_ssl'          => $req->input('ont_premium_ssl'),
			'wifiextender_hss'         => str_replace(".", "", $req->input('wifiextender_hss')),
			'wifiextender_ssl'         => $req->input('wifiextender_ssl'),
			'pu_s7_140_hss'            => str_replace(".", "", $req->input('pu_s7_140_hss')),
			'pu_s7_140_ssl'            => $req->input('pu_s7_140_ssl'),
			'pu_s9_140_hss'            => str_replace(".", "", $req->input('pu_s9_140_hss')),
			'pu_s9_140_ssl'            => $req->input('pu_s9_140_ssl'),
			'created_by'               => session('auth')->id_user
		]);

		self::save_log(5, $id);
	}

	public static function PengisianNomorSuratPSB($req)
	{
		switch ($req->input('step'))
		{
			case 'verif_boq':

				$step_id = 10;

				DB::table('procurement_boq_upload')->where('id', $req->input('id'))->update([
					'modified_by' => session('auth')->id_user,
					'step_id' => $step_id
				]);

				$jml_pekerjaan_psb =
					($req->input('p1_survey_ssl') * str_replace(".", "", $req->input('p1_survey_hss'))) +
					($req->input('p2_tlpint_survey_ssl') * str_replace(".", "", $req->input('p2_tlpint_survey_hss'))) +
					($req->input('p2_intiptv_survey_ssl') * str_replace(".", "", $req->input('p2_intiptv_survey_hss'))) +
					($req->input('p3_survey_ssl') * str_replace(".", "", $req->input('p3_survey_hss'))) +
					($req->input('p1_ssl') * str_replace(".", "", $req->input('p1_hss'))) +
					// ($req->input('p2_ssl') * str_replace(".", "", $req->input('p2_hss'))) +
					($req->input('p2_tlpint_ssl') * str_replace(".", "", $req->input('p2_tlpint_hss'))) +
					($req->input('p2_intiptv_ssl') * str_replace(".", "", $req->input('p2_intiptv_hss'))) +
					($req->input('p3_ssl') * str_replace(".", "", $req->input('p3_hss')));

				$jml_pekerjaan_migrasi =
					($req->input('migrasi_service_1p2p_ssl') * str_replace(".", "", $req->input('migrasi_service_1p2p_hss'))) +
					($req->input('migrasi_service_1p3p_ssl') * str_replace(".", "", $req->input('migrasi_service_1p3p_hss'))) +
					($req->input('migrasi_service_2p3p_ssl') * str_replace(".", "", $req->input('migrasi_service_2p3p_hss')));

				$jml_pekerjaan_tambahan =
					($req->input('ikr_addon_stb_ssl') * str_replace(".", "", $req->input('ikr_addon_stb_hss'))) +
					($req->input('change_stb_ssl') * str_replace(".", "", $req->input('change_stb_hss'))) +
					($req->input('wifiextender_ssl') * str_replace(".", "", $req->input('wifiextender_hss'))) +
					($req->input('indihome_smart_ssl') * str_replace(".", "", $req->input('indihome_smart_hss'))) +
					($req->input('ont_premium_ssl') * str_replace(".", "", $req->input('ont_premium_hss'))) +
					($req->input('pu_s7_140_ssl') * str_replace(".", "", $req->input('pu_s7_140_hss'))) +
					($req->input('pu_s9_140_ssl') * str_replace(".", "", $req->input('pu_s9_140_hss'))) +
					($req->input('lme_wifi_pt1_ssl') * str_replace(".", "", $req->input('lme_wifi_pt1_hss'))) +
					($req->input('lme_ap_indoor_ssl') * str_replace(".", "", $req->input('lme_ap_indoor_hss'))) +
					($req->input('lme_ap_outdoor_ssl') * str_replace(".", "", $req->input('lme_ap_outdoor_hss')));

				// $total_sp = substr_replace(floor($jml_pekerjaan_psb + $jml_pekerjaan_tambahan), '000', -3, 3);
				$total_sp = ($jml_pekerjaan_psb + $jml_pekerjaan_migrasi + $jml_pekerjaan_tambahan);

				DB::table('procurement_nilai_psb')->insert([
					'id_dokumen'               => $req->input('id'),
					'desc'                     => 'BARIJ',
					'periode_kegiatan'         => $req->input('periode_kegiatan'),
					'rekon_periode1'           => $req->input('rekon_periode1'),
					'rekon_periode2'           => $req->input('rekon_periode2'),
					'total_nilai_sp'           => $total_sp,
					'p1_survey_hss'            => str_replace(".", "", $req->input('p1_survey_hss')),
					'p1_survey_ssl'            => $req->input('p1_survey_ssl'),
					'p2_tlpint_survey_hss'     => str_replace(".", "", $req->input('p2_tlpint_survey_hss')),
					'p2_tlpint_survey_ssl'     => $req->input('p2_tlpint_survey_ssl'),
					'p2_intiptv_survey_hss'    => str_replace(".", "", $req->input('p2_intiptv_survey_hss')),
					'p2_intiptv_survey_ssl'    => $req->input('p2_intiptv_survey_ssl'),
					'p3_survey_hss'            => str_replace(".", "", $req->input('p3_survey_hss')),
					'p3_survey_ssl'            => $req->input('p3_survey_ssl'),
					'p1_hss'                   => str_replace(".", "", $req->input('p1_hss')),
					'p1_ssl'                   => $req->input('p1_ssl'),
					// 'p2_hss'                => str_replace(".", "", $req->input('p2_hss')),
					// 'p2_ssl'                => $req->input('p2_ssl'),
					'p2_tlpint_hss'            => str_replace(".", "", $req->input('p2_tlpint_hss')),
					'p2_tlpint_ssl'            => $req->input('p2_tlpint_ssl'),
					'p2_intiptv_hss'           => str_replace(".", "", $req->input('p2_intiptv_hss')),
					'p2_intiptv_ssl'           => $req->input('p2_intiptv_ssl'),
					'p3_hss'                   => str_replace(".", "", $req->input('p3_hss')),
					'p3_ssl'                   => $req->input('p3_ssl'),
					'migrasi_service_1p2p_hss' => str_replace(".", "", $req->input('migrasi_service_1p2p_hss')),
					'migrasi_service_1p2p_ssl' => $req->input('migrasi_service_1p2p_ssl'),
					'migrasi_service_1p3p_hss' => str_replace(".", "", $req->input('migrasi_service_1p3p_hss')),
					'migrasi_service_1p3p_ssl' => $req->input('migrasi_service_1p3p_ssl'),
					'migrasi_service_2p3p_hss' => str_replace(".", "", $req->input('migrasi_service_2p3p_hss')),
					'migrasi_service_2p3p_ssl' => $req->input('migrasi_service_2p3p_ssl'),
					'lme_wifi_pt1_hss'         => str_replace(".", "", $req->input('lme_wifi_pt1_hss')),
					'lme_wifi_pt1_ssl'         => $req->input('lme_wifi_pt1_ssl'),
					'lme_ap_indoor_hss'        => str_replace(".", "", $req->input('lme_ap_indoor_hss')),
					'lme_ap_indoor_ssl'        => $req->input('lme_ap_indoor_ssl'),
					'lme_ap_outdoor_hss'       => str_replace(".", "", $req->input('lme_ap_outdoor_hss')),
					'lme_ap_outdoor_ssl'       => $req->input('lme_ap_outdoor_ssl'),
					'indihome_smart_hss'       => str_replace(".", "", $req->input('indihome_smart_hss')),
					'indihome_smart_ssl'       => $req->input('indihome_smart_ssl'),
					'plc_hss'                  => str_replace(".", "", $req->input('plc_hss')),
					'plc_ssl'                  => $req->input('plc_ssl'),
					'ikr_addon_stb_hss'        => str_replace(".", "", $req->input('ikr_addon_stb_hss')),
					'ikr_addon_stb_ssl'        => $req->input('ikr_addon_stb_ssl'),
					'change_stb_hss'           => str_replace(".", "", $req->input('change_stb_hss')),
					'change_stb_ssl'           => $req->input('change_stb_ssl'),
					'ont_premium_hss'          => str_replace(".", "", $req->input('ont_premium_hss')),
					'ont_premium_ssl'          => $req->input('ont_premium_ssl'),
					'wifiextender_hss'         => str_replace(".", "", $req->input('wifiextender_hss')),
					'wifiextender_ssl'         => $req->input('wifiextender_ssl'),
					'pu_s7_140_hss'            => str_replace(".", "", $req->input('pu_s7_140_hss')),
					'pu_s7_140_ssl'            => $req->input('pu_s7_140_ssl'),
					'pu_s9_140_hss'            => str_replace(".", "", $req->input('pu_s9_140_hss')),
					'pu_s9_140_ssl'            => $req->input('pu_s9_140_ssl'),
					'created_by'               => session('auth')->id_user,
					'modified_by'              => session('auth')->id_user
				]);
			break;

			case 'verif_doc':

				$step_id = 11;

				DB::table('procurement_boq_upload')->where('id', $req->input('id'))->update([
					'surat_penetapan'    => $req->input('surat_penetapan'),
					'tgl_s_pen'          => $req->input('tgl_s_pen'),
					'surat_kesanggupan'  => $req->input('surat_kesanggupan'),
					'tgl_surat_sanggup'  => $req->input('tgl_surat_sanggup'),
					'amd_sp'             => $req->input('amd_sp'),
					'tgl_amd_sp'         => $req->input('rekon_periode2'),
					'no_baij'            => $req->input('no_baij'),
					'no_bap'             => $req->input('no_bap'),
					'BAPP'               => $req->input('BAPP'),
					'no_ba_pemeliharaan' => $req->input('no_ba_pemeliharaan'),
					'tgl_bapp'           => $req->input('rekon_periode2'),
					'BAST'               => $req->input('BAST'),
					'tgl_bast'           => $req->input('rekon_periode2'),
					'modified_by'        => session('auth')->id_user,
					'verificator_by'     => session('auth')->id_user,
					'verificator_at'     => date('Y-m-d H:i:s'),
					'step_id'            => $step_id
				]);

				DB::table('procurement_nilai_psb')->where('id_dokumen', $req->input('id'))->where('desc', 'BARIJ')->update([
					'persentase_7_kpi' => $req->input('persentase_7_kpi'),
					'ppn_numb'         => $req->input('ppn_numb'),
					'modified_by'      => session('auth')->id_user,
					'verificator_by'   => session('auth')->id_user,
					'verificator_at'   => date('Y-m-d H:i:s')
				]);
			break;
		}

		self::save_log($step_id, $req->input('id') );
	}

	public static function download_filepsb($type, $id)
	{
		if (in_array($type, ['justifikasi', 'surat_pesanan']))
		{
			$desc = 'SP';
		} else {
			$desc = 'BARIJ';
		}

		return DB::table('procurement_boq_upload as pbu')
		->leftJoin('procurement_mitra as pm', 'pbu.mitra_id', '=', 'pm.id')
		->leftJoin('procurement_hss_psb as phss', 'pbu.witel', '=', 'phss.witel')
		->leftJoin('procurement_nilai_psb as pns', 'pbu.id', '=', 'pns.id_dokumen')
		->leftJoin('bulan_format as bf', 'pns.periode_kegiatan', '=', 'bf.id')
		->select(
			'pbu.*',
			'pbu.masa_berlaku_pks AS expired_pks',
			'pm.nama_company',
			'pm.alamat_company',
			'pm.telp as telp_mitra',
			'pm.wakil_mitra',
			'pm.jabatan_mitra',
			'pm.rek',
			'pm.bank',
			'pm.atas_nama',
			'pm.cabang_bank',
			'pm.NPWP',
			'pm.lokasi_npwp',
			'pm.no_amandemen1',
			'pm.date_amandemen1',
			'pm.date_expired_amandemen1',
			'pm.no_amandemen2',
			'pm.date_amandemen2',
			'pm.date_expired_amandemen2',
			'pm.no_amandemen3',
			'pm.date_amandemen3',
			'pm.date_expired_amandemen3',
			'pm.no_amandemen4',
			'pm.date_amandemen4',
			'pm.date_expired_amandemen4',
			'pm.no_amandemen5',
			'pm.date_amandemen5',
			'pm.date_expired_amandemen5',
			'pm.no_kontrak_ta',
			'pm.date_kontrak_ta',
			'pm.no_penetapan_khs',
			'pm.date_khs_penetapan',
			'pm.no_kesanggupan_khs',
			'pm.date_khs_kesanggupan',
			'bf.bulan',
			'pm.area',
			'pm.area_sto',
			'pm.id_regional',
			'pm.regional',
			'pm.witel',
			'pm.pph as pph_mitra',
			'bf.month',
			'pns.*',
			'phss.p1 as phss_p1',
			'phss.p2 as phss_p2',
			'phss.p2_inet_voice as phss_p2_inet_voice',
			'phss.p2_inet_iptv as phss_p2_inet_iptv',
			'phss.p3 as phss_p3',
			'phss.p1_ku as phss_p1_ku',
			'phss.p2_inet_voice_ku as phss_p2_inet_voice_ku',
			'phss.p2_inet_iptv_ku as phss_p2_inet_iptv_ku',
			'phss.p3_ku as phss_p3_ku',
			'phss.migrasi_1p as phss_migrasi_1p',
			'phss.migrasi_2p as phss_migrasi_2p',
			'phss.migrasi_3p as phss_migrasi_3p',
			'phss.stb_tambahan as phss_stb_tambahan',
			'phss.change_stb as phss_change_stb',
			'phss.wifiext as phss_wifiext',
			'phss.ont_premium as phss_ont_premium',
			'phss.insert_tiang as phss_insert_tiang',
			'phss.insert_tiang_9 as phss_insert_tiang_9',
			'phss.tray_cable as phss_tray_cable',
			'phss.lme_wifi as phss_lme_wifi',
			'phss.lme_ap_indoor as phss_lme_ap_indoor',
			'phss.lme_ap_outdoor as phss_lme_ap_outdoor',
			'phss.smartcam_only as phss_smartcam_only',
			'phss.plc as phss_plc',
			'phss.rs_in_sc_1 as phss_rs_in_sc_1',
			'phss.s_clamp_spriner as phss_s_clamp_spriner',
			'phss.breket as phss_breket',
			'phss.utp_c6 as phss_utp_c6',
			'phss.precon_50 as phss_precon_50',
			'phss.precon_80 as phss_precon_80',
			'phss.precon_100 as phss_precon_100',
			'phss.precon_150 as phss_precon_150',
			'phss.patch_cord_2m as phss_patch_cord_2m',
			'phss.rj_45 as phss_rj_45',
			'phss.soc_ils as phss_soc_ils',
			'phss.soc_sum as phss_soc_sum',
			'phss.prekso_intra_15_rs as phss_prekso_intra_15_rs',
			'phss.prekso_intra_20_rs as phss_prekso_intra_20_rs',
			'phss.pu_s7_tiang as phss_pu_s7_tiang',
			'phss.otp_ftth_1 as phss_otp_ftth_1',
			'phss.tc_of_cr_200 as phss_tc_of_cr_200',
			'phss.ac_of_sm_1b as phss_ac_of_sm_1b')
		->where('pbu.id', $id)
		->where('pns.desc', $desc)
		->first();
	}

	public static function word_skemaOrderBased($data, $version)
	{
		$that = new \App\Http\Controllers\AdminController();
		switch ($version) {
			case 'beta_ho_v1':
				$template = public_path() . '/template_doc/psb/beta_ho/v1/Dok_Skema_OrderBased.docx';
				break;
			case 'beta_ho_v2_manual':
				$template = public_path() . '/template_doc/psb/beta_ho/v2_manual/Dok_Skema_OrderBased.docx';
				break;
			case 'beta_kalimantan':
				$template = public_path() . '/template_doc/psb/beta_kalimantan/Dok_Skema_OrderBased.docx';
				break;
		}
		$template_word = new \PhpOffice\PhpWord\TemplateProcessor($template);

		if (in_array(date('m'), [1, 2, 3]))
		{
			// Q1
			$kpi_target_pspi = 96;
			$kpi_target_tticomply = 91;
		}
		elseif (in_array(date('m'), [4, 5, 6]))
		{
			// Q2
			$kpi_target_pspi = 97;
			$kpi_target_tticomply = 95;
		}
		elseif (in_array(date('m'), [7, 8, 9, 10, 11, 12]))
		{
			// Q3 Q4
			$kpi_target_pspi = 98;
			$kpi_target_tticomply = 99;
		};

		$kpi_pspi             = @round((str_replace(',', '.', $data->kpi_real_pspi) / $kpi_target_pspi) * 100, 2);
		$kpi_tticomply        = @round((str_replace(',', '.', $data->kpi_real_tticomply) / $kpi_target_tticomply) * 100, 2);
		$kpi_ffg              = @round((str_replace(',', '.', $data->kpi_real_ffg) / 99) * 100, 2);
		$kpi_validasicore     = @round((str_replace(',', '.', $data->kpi_real_validasicore) / 100) * 100, 2);
		$kpi_ttrffg           = @round((str_replace(',', '.', $data->kpi_real_ttrffg) / 99) * 100, 2);
		$kpi_ujipetik         = @round((str_replace(',', '.', $data->kpi_real_ujipetik) / 99) * 100, 2);
		$kpi_qc2              = @round((str_replace(',', '.', $data->kpi_real_qc2) / 99) * 100, 2);

		$ach_kpi_pspi         = @round(($kpi_pspi * 25) / 100, 2);
		$ach_kpi_tticomply    = @round(($kpi_tticomply * 25) / 100, 2);
		$ach_kpi_ffg          = @round(($kpi_ffg * 15) / 100, 2);
		$ach_kpi_validasicore = @round(($kpi_validasicore * 5) / 100, 2);
		$ach_kpi_ttrffg       = @round(($kpi_ttrffg * 10) / 100, 2);
		$ach_kpi_ujipetik     = @round(($kpi_ujipetik * 10) / 100, 2);
		$ach_kpi_qc2          = @round(($kpi_qc2 * 10) / 100, 2);

		$ach_kpi = ($ach_kpi_pspi + $ach_kpi_tticomply + $ach_kpi_ffg + $ach_kpi_validasicore + $ach_kpi_ttrffg + $ach_kpi_ujipetik + $ach_kpi_qc2);

		if ($ach_kpi > 100)
		{
			$kpi = 100;
		}
		elseif ($ach_kpi < 90)
		{
			$kpi = 90;
		}
		else
		{
			$kpi = $ach_kpi;
		};

		$jml_pekerjaan_psb =
			($data->p1_survey_ssl * $data->p1_survey_hss) +
			($data->p2_tlpint_survey_ssl * $data->p2_tlpint_survey_hss) +
			($data->p2_intiptv_survey_ssl * $data->p2_intiptv_survey_hss) +
			($data->p3_survey_ssl * $data->p3_survey_hss) +

			($data->p1_ssl * $data->p1_hss) +
			($data->p2_ssl * $data->p2_hss) + // versi khs 2021
			($data->p2_tlpint_ssl * $data->p2_tlpint_hss) +
			($data->p2_intiptv_ssl * $data->p2_intiptv_hss) +
			($data->p3_ssl * $data->p3_hss);

		$jml_pekerjaan_migrasi =
			($data->migrasi_service_1p2p_ssl * $data->migrasi_service_1p2p_hss) +
			($data->migrasi_service_1p3p_ssl * $data->migrasi_service_1p3p_hss) +
			($data->migrasi_service_2p3p_ssl * $data->migrasi_service_2p3p_hss);

		$jml_pekerjaan_tambahan =
			($data->ikr_addon_stb_ssl * $data->ikr_addon_stb_hss) +
			($data->indihome_smart_ssl * $data->indihome_smart_hss) +
			($data->wifiextender_ssl * $data->wifiextender_hss) +
			($data->plc_ssl * $data->plc_hss) +
			($data->lme_wifi_pt1_ssl * $data->lme_wifi_pt1_hss) +
			($data->lme_ap_indoor_ssl * $data->lme_ap_indoor_hss) +
			($data->lme_ap_outdoor_ssl * $data->lme_ap_outdoor_hss) +
			($data->ont_premium_ssl * $data->ont_premium_hss) +
			($data->migrasi_stb_ssl * $data->migrasi_stb_hss) +
			($data->instalasi_ipcam_ssl * $data->instalasi_ipcam_hss) +
			($data->pu_s7_140_ssl * $data->pu_s7_140_hss) +
			($data->pu_s9_140_ssl * $data->pu_s9_140_hss);

		$jml_ssl =
			($data->p1_survey_ssl + $data->p2_tlpint_survey_ssl + $data->p2_intiptv_survey_ssl + $data->p3_survey_ssl + $data->p1_ssl + $data->p2_ssl + $data->p2_tlpint_ssl + $data->p2_intiptv_ssl + $data->p3_ssl + $data->migrasi_service_1p2p_ssl + $data->migrasi_service_1p3p_ssl + $data->migrasi_service_2p3p_ssl + $data->ikr_addon_stb_ssl + $data->indihome_smart_ssl + $data->wifiextender_ssl + $data->plc_ssl + $data->lme_wifi_pt1_ssl + $data->lme_ap_indoor_ssl + $data->lme_ap_outdoor_ssl + $data->ont_premium_ssl + $data->migrasi_stb_ssl + $data->instalasi_ipcam_ssl + $data->pu_s7_140_ssl + $data->pu_s9_140_ssl);

		$imbal_jasa = (($kpi / 100) * $jml_pekerjaan_psb) + ($jml_pekerjaan_migrasi + $jml_pekerjaan_tambahan);
		$nilai_dpp = substr_replace(floor($imbal_jasa), '000', -3, 3);
		$nilai_ppn = $nilai_dpp * (str_replace(',', '.', $data->ppn_numb) / 100);
		$nilai_pph = $nilai_dpp * (str_replace(',', '.', $data->pph_mitra) / 100);
		$nilai_kwitansi = ($nilai_dpp + $nilai_ppn);

		$jenis_pekerjaan = ['p1_survey_ssl', 'p1_survey_hss', 'p2_tlpint_survey_ssl', 'p2_tlpint_survey_hss', 'p2_intiptv_survey_ssl', 'p2_intiptv_survey_hss', 'p3_survey_ssl', 'p3_survey_hss', 'p1_ssl', 'p1_hss', 'p2_tlpint_ssl', 'p2_tlpint_hss', 'p2_intiptv_ssl', 'p2_intiptv_hss', 'p3_ssl', 'p3_hss', 'migrasi_service_1p2p_ssl', 'migrasi_service_1p2p_hss', 'migrasi_service_1p3p_ssl', 'migrasi_service_1p3p_hss', 'migrasi_service_2p3p_ssl', 'migrasi_service_2p3p_hss', 'ikr_addon_stb_ssl', 'ikr_addon_stb_hss', 'indihome_smart_ssl', 'indihome_smart_hss', 'wifiextender_ssl', 'wifiextender_hss', 'plc_ssl', 'plc_hss', 'lme_wifi_pt1_ssl', 'lme_wifi_pt1_hss', 'lme_ap_indoor_ssl', 'lme_ap_indoor_hss', 'lme_ap_outdoor_ssl', 'lme_ap_outdoor_hss', 'ont_premium_ssl', 'ont_premium_hss', 'migrasi_stb_ssl', 'migrasi_stb_hss', 'instalasi_ipcam_ssl', 'instalasi_ipcam_hss', 'pu_s7_140_ssl', 'pu_s7_140_hss', 'pu_s9_140_ssl', 'pu_s9_140_hss'];

		foreach ($jenis_pekerjaan as $pekerjaan)
		{
			$template_word->setValue($pekerjaan, htmlspecialchars(str_replace(',', '.', number_format($data->$pekerjaan))));
		}

		$template_word->setValue('psb_ssl', htmlspecialchars(str_replace(',', '.', number_format($data->p1_ssl + $data->p2_tlpint_ssl + $data->p2_intiptv_ssl + $data->p3_ssl))));
		$template_word->setValue('psb_survey_ssl', htmlspecialchars(str_replace(',', '.', number_format($data->p1_survey_ssl + $data->p2_tlpint_survey_ssl + $data->p2_intiptv_survey_ssl + $data->p3_survey_ssl))));
		$template_word->setValue('migrasi_ssl', htmlspecialchars(str_replace(',', '.', number_format($data->migrasi_service_1p2p_ssl + $data->migrasi_service_1p3p_ssl + $data->migrasi_service_2p3p_ssl))));
		$template_word->setValue('lme_ap_ssl', htmlspecialchars(str_replace(',', '.', number_format($data->lme_ap_indoor_ssl + $data->lme_ap_outdoor_ssl))));
		$template_word->setValue('pu_tiang_ssl', htmlspecialchars(str_replace(',', '.', number_format($data->pu_s7_140_ssl + $data->pu_s9_140_ssl))));
		$template_word->setValue('jumlah_ssl', htmlspecialchars(str_replace(',', '.', number_format($jml_ssl))));

		$template_word->setValue('p1_survey_ttl', htmlspecialchars(str_replace(',', '.', number_format($data->p1_survey_ssl * $data->p1_survey_hss))));
		$template_word->setValue('p2_tlpint_survey_ttl', htmlspecialchars(str_replace(',', '.', number_format($data->p2_tlpint_survey_ssl * $data->p2_tlpint_survey_hss))));
		$template_word->setValue('p2_intiptv_survey_ttl', htmlspecialchars(str_replace(',', '.', number_format($data->p2_intiptv_survey_ssl * $data->p2_intiptv_survey_hss))));
		$template_word->setValue('p3_survey_ttl', htmlspecialchars(str_replace(',', '.', number_format($data->p3_survey_ssl * $data->p3_survey_hss))));
		$template_word->setValue('p1_ttl', htmlspecialchars(str_replace(',', '.', number_format($data->p1_ssl * $data->p1_hss))));
		$template_word->setValue('p2_tlpint_ttl', htmlspecialchars(str_replace(',', '.', number_format($data->p2_tlpint_ssl * $data->p2_tlpint_hss))));
		$template_word->setValue('p2_intiptv_ttl', htmlspecialchars(str_replace(',', '.', number_format($data->p2_intiptv_ssl * $data->p2_intiptv_hss))));
		$template_word->setValue('p3_ttl', htmlspecialchars(str_replace(',', '.', number_format($data->p3_ssl * $data->p3_hss))));
		$template_word->setValue('migrasi_service_1p2p_ttl', htmlspecialchars(str_replace(',', '.', number_format($data->migrasi_service_1p2p_ssl * $data->migrasi_service_1p2p_hss))));
		$template_word->setValue('migrasi_service_1p3p_ttl', htmlspecialchars(str_replace(',', '.', number_format($data->migrasi_service_1p3p_ssl * $data->migrasi_service_1p3p_hss))));
		$template_word->setValue('migrasi_service_2p3p_ttl', htmlspecialchars(str_replace(',', '.', number_format($data->migrasi_service_2p3p_ssl * $data->migrasi_service_2p3p_hss))));
		$template_word->setValue('ikr_addon_stb_ttl', htmlspecialchars(str_replace(',', '.', number_format($data->ikr_addon_stb_ssl * $data->ikr_addon_stb_hss))));
		$template_word->setValue('indihome_smart_ttl', htmlspecialchars(str_replace(',', '.', number_format($data->indihome_smart_ssl * $data->indihome_smart_hss))));
		$template_word->setValue('wifiextender_ttl', htmlspecialchars(str_replace(',', '.', number_format($data->wifiextender_ssl * $data->wifiextender_hss))));
		$template_word->setValue('plc_ttl', htmlspecialchars(str_replace(',', '.', number_format($data->plc_ssl * $data->plc_hss))));
		$template_word->setValue('lme_wifi_pt1_ttl', htmlspecialchars(str_replace(',', '.', number_format($data->lme_wifi_pt1_ssl * $data->lme_wifi_pt1_hss))));
		$template_word->setValue('lme_ap_indoor_ttl', htmlspecialchars(str_replace(',', '.', number_format($data->lme_ap_indoor_ssl * $data->lme_ap_indoor_hss))));
		$template_word->setValue('lme_ap_outdoor_ttl', htmlspecialchars(str_replace(',', '.', number_format($data->lme_ap_outdoor_ssl * $data->lme_ap_outdoor_hss))));
		$template_word->setValue('ont_premium_ttl', htmlspecialchars(str_replace(',', '.', number_format($data->ont_premium_ssl * $data->ont_premium_hss))));
		$template_word->setValue('migrasi_stb_ttl', htmlspecialchars(str_replace(',', '.', number_format($data->migrasi_stb_ssl * $data->migrasi_stb_hss))));
		$template_word->setValue('instalasi_ipcam_ttl', htmlspecialchars(str_replace(',', '.', number_format($data->instalasi_ipcam_ssl * $data->instalasi_ipcam_hss))));
		$template_word->setValue('pu_s7_140_ttl', htmlspecialchars(str_replace(',', '.', number_format($data->pu_s7_140_ssl * $data->pu_s7_140_hss))));
		$template_word->setValue('pu_s9_140_ttl', htmlspecialchars(str_replace(',', '.', number_format($data->pu_s9_140_ssl * $data->pu_s9_140_ssl))));
		$template_word->setValue('harga_ttl', htmlspecialchars(str_replace(',', '.', number_format($jml_pekerjaan_psb + $jml_pekerjaan_migrasi + $jml_pekerjaan_tambahan))));
		$template_word->setValue('kpi', htmlspecialchars($kpi.'%'));
		$template_word->setValue('jmlp_psb', htmlspecialchars(str_replace(',', '.', number_format($jml_pekerjaan_psb))));
		$template_word->setValue('jmlp_migrasi', htmlspecialchars(str_replace(',', '.', number_format($jml_pekerjaan_migrasi))));
		$template_word->setValue('jmlp_tmbhn', htmlspecialchars(str_replace(',', '.', number_format($jml_pekerjaan_tambahan))));
		$template_word->setValue('nilai_dpp', htmlspecialchars(str_replace(',', '.', number_format($nilai_dpp))));
		$template_word->setValue('ppn_numb', htmlspecialchars($data->ppn_numb));
		$template_word->setValue('nilai_ppn', htmlspecialchars(str_replace(',', '.', number_format($nilai_ppn))));
		$template_word->setValue('nb_ppn', htmlspecialchars(str_replace(',', '.', number_format($imbal_jasa))));
		$template_word->setValue('penyebut_nb_ppn', htmlspecialchars(ucwords($that->terbilang($imbal_jasa))));
		$template_word->setValue('ns_ppn', htmlspecialchars(str_replace(',', '.', number_format($nilai_kwitansi))));
		$template_word->setValue('penyebut_ns_ppn', htmlspecialchars(ucwords($that->terbilang($nilai_kwitansi))));

		$template_word->setValue('witel', htmlspecialchars(ucfirst(strtolower($data->witel))));
		$template_word->setValue('area', htmlspecialchars($data->area));
		$template_word->setValue('nama_mitra', htmlspecialchars($data->nama_company));
		$template_word->setValue('alamat_mitra', htmlspecialchars($data->alamat_company));
		$template_word->setValue('pic_mitra', htmlspecialchars($data->wakil_mitra));
		$template_word->setValue('jabatan_pic_mitra', htmlspecialchars($data->jabatan_mitra));
		$template_word->setValue('no_kontrak_ta', htmlspecialchars($data->no_kontrak_ta));
		$template_word->setValue('tgl_kontrak_ta', htmlspecialchars($that->convert_month($data->date_kontrak_ta)));
		$template_word->setValue('masa_berlaku_pks', htmlspecialchars($that->convert_month($data->expired_pks)));
		// $template_word->setValue('no_amandemen1', htmlspecialchars($data->no_amandemen1));
		// $template_word->setValue('tgl_amandemen1', htmlspecialchars($that->convert_month($data->date_amandemen1)));
		// $template_word->setValue('no_amandemen2', htmlspecialchars($data->no_amandemen2));
		// $template_word->setValue('tgl_amandemen2', htmlspecialchars($that->convert_month($data->date_amandemen2)));
		// $template_word->setValue('no_amandemen3', htmlspecialchars($data->no_amandemen3));
		// $template_word->setValue('tgl_amandemen3', htmlspecialchars($that->convert_month($data->date_amandemen3)));
		// $template_word->setValue('no_amandemen4', htmlspecialchars($data->no_amandemen4));
		// $template_word->setValue('tgl_amandemen4', htmlspecialchars($that->convert_month($data->date_amandemen4)));
		// $template_word->setValue('no_amandemen5', htmlspecialchars($data->no_amandemen5));
		// $template_word->setValue('tgl_amandemen5', htmlspecialchars($that->convert_month($data->date_amandemen5)));
		$template_word->setValue('no_surat_penetapan', htmlspecialchars($data->surat_penetapan));
		$template_word->setValue('tgl_surat_penetapan', htmlspecialchars($that->convert_month($data->tgl_s_pen)));
		$template_word->setValue('no_surat_kesanggupan', htmlspecialchars($data->surat_kesanggupan));
		$template_word->setValue('tgl_surat_kesanggupan', htmlspecialchars($that->convert_month($data->tgl_surat_sanggup)));
		$template_word->setValue('no_sp', htmlspecialchars($data->no_sp));
		$template_word->setValue('tgl_sp', htmlspecialchars($that->convert_month($data->tgl_sp)));
		$template_word->setValue('toc', htmlspecialchars($data->toc));
		$template_word->setValue('toc_teks', htmlspecialchars(ucwords($that->terbilang($data->toc))));
		$template_word->setValue('no_surat_baut', htmlspecialchars($data->BAUT));
		$template_word->setValue('no_bast', htmlspecialchars($data->BAST));
		$template_word->setValue('periode_rekon', htmlspecialchars($data->month.' '.date('Y', strtotime($data->rekon_periode2))));
		$template_word->setValue('periode_rekonx', htmlspecialchars(strtoupper($data->month.' '.date('Y', strtotime($data->rekon_periode2)))));
		$template_word->setValue('rekon_periode1', htmlspecialchars($that->convert_month($data->rekon_periode1)));
		$template_word->setValue('rekon_periode2', htmlspecialchars($that->convert_month($data->rekon_periode2)));
		$template_word->setValue('hari_rekon_periode2', htmlspecialchars($that->only_day($data->rekon_periode2)));
		$template_word->setValue('tanggal_teks_rekon_periode2', htmlspecialchars(ucwords($that->terbilang(date('d', strtotime($data->rekon_periode2))))));
		$template_word->setValue('bulan_teks_rekon_periode2', htmlspecialchars($data->month));
		$template_word->setValue('tahun_teks_rekon_periode2', htmlspecialchars(ucwords($that->terbilang(date('Y', strtotime($data->rekon_periode2))))));
		$template_word->setValue('date_rekon_periode2', htmlspecialchars(date('d/m/Y', strtotime($data->rekon_periode2))));

		if ($nilai_dpp > 0 && $nilai_dpp <= 200000000)
		{
			$rule = 'krglbh_dua_jt';
		} elseif ($nilai_dpp > 200000000 && $nilai_dpp <= 500000000) {
			$rule = 'antr_dualima_jt';
		} else {
			$rule = 'lbh_lima_jt';
		}

		$template_word->setValue('pic_pejabat_ta', htmlspecialchars(strtoupper($that->ttd_user('PSB', $data->witel, $rule)->user)));
		$template_word->setValue('jabatan_pejabat_ta', htmlspecialchars(strtoupper($that->ttd_user('PSB', $data->witel, $rule)->jabatan)));
		$template_word->setValue('alamat_ta', htmlspecialchars(strtoupper($that->ttd_user('PSB', $data->witel, $rule)->alamat_kantor)));
		$template_word->setValue('pic_mgr_area', htmlspecialchars(strtoupper($that->ttd_user('PSB', $data->witel, 'mngr_krj')->user)));
		$template_word->setValue('jabatan_mgr_area', htmlspecialchars(strtoupper($that->ttd_user('PSB', $data->witel, 'mngr_krj')->jabatan)));
		$template_word->setValue('gm_area', htmlspecialchars(strtoupper($that->ttd_user('PSB', $data->witel, 'gm')->user)));
		$template_word->setValue('jabatan_gm_area', htmlspecialchars(strtoupper($that->ttd_user('PSB', $data->witel, 'gm')->jabatan)));



		$file_name = 'Dok_Skema_OrderBased_'.str_replace(" ", "_", $data->nama_company).'.docx';

		$path =  public_path() . '/upload2/' . $data->id . '/';

		if (!file_exists($path))
		{
			if (!mkdir($path, 0777, true))
			{
				return 'gagal menyiapkan folder rekon';
			}
		}

		$file =  $path .'/'. $file_name;

		$template_word->saveAS($file);
	}

	public static function get_last_work($data)
	{
		return DB::table('procurement_boq_upload')
		->whereIn('pekerjaan', $data)
		->where([
			['witel', session('auth')->Witel_New],
			['active', 1]
		])
		->OrderBy('id', 'DESC')
		->first();
	}

	public static function get_detail_boq($id, $sd)
	{
		$AdminModel = new AdminModel();
		$get_date = DB::Table('procurement_boq_upload')->where('id', $id)->first();

		$pph_sp = $AdminModel->find_pph($get_date->tgl_sp);
		$pph = $AdminModel->find_pph($get_date->tgl_ba_rekon);

		return DB::table('procurement_boq_upload As pbu')
		->leftjoin('procurement_Boq_lokasi As pbl', 'pbl.id_upload', '=', 'pbu.id')
		->leftjoin('procurement_req_pid As prp', 'prp.id_lokasi', '=', 'pbl.id')
		->leftjoin('procurement_Boq_design As pbd', 'pbl.id', '=', 'pbd.id_boq_lokasi')
		->leftjoin('procurement_pid As pp', 'pp.id', '=', 'prp.id_pid_req')
		->select('pbu.judul', 'prp.*', 'pp.pid', 'pbl.id As id_lokasi', 'pbl.pid_sto',
			DB::raw("SUM(CASE WHEN pbd.status_design = 0 THEN pbd.material * pbd.sp END) AS total_material_sp"),
			DB::raw("SUM(CASE WHEN pbd.status_design = 0 THEN pbd.jasa * pbd.sp END) AS total_jasa_sp"),
			DB::raw("SUM(CASE WHEN pbd.status_design = $sd THEN pbd.material * pbd.rekon END) AS total_material_rekon"),
			DB::raw("SUM(CASE WHEN pbd.status_design = $sd THEN pbd.jasa * pbd.rekon END) AS total_jasa_rekon"),
			DB::raw("SUM(CASE WHEN pbd.status_design = $sd THEN pbd.material * pbd.sp END) AS total_material_rek_sp"),
			DB::raw("SUM(CASE WHEN pbd.status_design = $sd THEN pbd.jasa * pbd.sp END) AS total_jasa_rek_sp"),
			DB::raw("(SUM(CASE WHEN pbd.status_design = 0 THEN pbd.material * pbd.sp END) + SUM(CASE WHEN pbd.status_design = 0 THEN pbd.jasa * pbd.sp END) ) AS gd_sp"),
			DB::raw("(SUM(CASE WHEN pbd.status_design = $sd THEN pbd.material * pbd.rekon END) + SUM(CASE WHEN pbd.status_design = $sd THEN pbd.jasa * pbd.rekon END) ) AS gd_rekon"),
			DB::raw("( (SUM(CASE WHEN pbd.status_design = 0 THEN pbd.material * pbd.sp END) + SUM(CASE WHEN pbd.status_design = 0 THEN pbd.jasa * pbd.sp END) ) * $pph_sp) AS gd_ppn_sp"),
			DB::raw("( (SUM(CASE WHEN pbd.status_design = $sd THEN pbd.material * pbd.rekon END) + SUM(CASE WHEN pbd.status_design = $sd THEN pbd.jasa * pbd.rekon END) ) * $pph_sp) AS gd_ppn_rekon"),
			DB::raw("( (SUM(CASE WHEN pbd.status_design = 0 THEN pbd.material * pbd.sp END) + SUM(CASE WHEN pbd.status_design = 0 THEN pbd.jasa * pbd.sp END) ) + (SUM(CASE WHEN pbd.status_design = 0 THEN pbd.material * pbd.sp END) + SUM(CASE WHEN pbd.status_design = 0 THEN pbd.jasa * pbd.sp END) ) * $pph) AS total_sp"),
			DB::raw("( (SUM(CASE WHEN pbd.status_design = $sd THEN pbd.material * pbd.rekon END) + SUM(CASE WHEN pbd.status_design = $sd THEN pbd.jasa * pbd.rekon END) ) + (SUM(CASE WHEN pbd.status_design = $sd THEN pbd.material * pbd.rekon END) + SUM(CASE WHEN pbd.status_design = $sd THEN pbd.jasa * pbd.rekon END) ) * $pph) AS total_rekon") )
		->where([
			['pbu.id', $id]
		])
		->GroupBy('prp.id')
		->get();
	}

	public static function get_pid_by_boq($id)
	{
		return DB::SELECT("SELECT pp.*
		FROM procurement_pid As pp
		LEFT JOIN procurement_req_pid prp ON prp.id_pid_req = pp.id
		WHERE prp.id_upload = $id
		GROUP BY pp.id");
	}

	public static function get_belum_lurus($id)
	{
		return DB::Table('procurement_rfc_osp As pro')
		->select("pro.*")
		->where([
			['pro.id_upload', $id],
			['pro.active', 1],
			['pro.status_rfc', 'TIDAK LURUS']
		])
		->GroupBy('rfc')
		->get();
	}

	public static function get_last_material_boq($id, $sd = 0)
	{
		$sql = DB::table('procurement_boq_upload As pbu')
		->leftjoin('procurement_Boq_lokasi As pbl', 'pbu.id', '=', 'pbl.id_upload')
		->leftjoin('procurement_Boq_design As pbd', 'pbl.id', '=', 'pbd.id_boq_lokasi')
		->leftjoin('procurement_designator As pd', 'pd.id', '=', 'pbd.id_design')
		->select('pbd.*', 'pbl.id', 'pbl.sto', 'pd.designator', 'pd.jenis', 'pbl.new_boq',
		DB::RAW("(CASE WHEN pbd.jenis_khs = 16 THEN 'Telkom Akses' ELSE 'Mitra' END) As namcomp"),
		'pd.material As material_pd', 'pd.jasa As jasa_pd', 'pbd.jenis_khs As design_mitra_id', 'pd.uraian', 'pbl.lokasi', 'pbl.sub_jenis_p', 'pbl.pid_sto')
		->where([
			['pbu.id', $id],
			['pbu.active', 1],
			['pd.active', 1]
		])
		->WhereNotNull('pbd.id');

		if(strcasecmp($sd, 'All') != 0)
		{
			$sql->where('pbd.status_design', $sd);
		}

		return $sql->OrderBy('pbd.status_design', 'ASC')->groupBy('pbd.id')->get();
	}

	public static function rest_step($id)
	{
		$check_stp = self::get_step_by_id($id->step_id);
		$find_step = $sisa = $lewat = [];

		if($check_stp->action == 'forward')
		{
			$find_step = self::get_step_by_id($id->step_id);
			$next_step = self::get_step_by_id($find_step->step_after);
		}
		elseif($check_stp->action == 'stay')
		{
			$get_stay_id = DB::Table('procurement_step')->where('id', $id->step_id)->first();
			$find_step_id = DB::Table('procurement_step')->where('step_after', $get_stay_id->step_after)->first();
			$find_step = self::get_step_by_id($find_step_id->id);
			$next_step = self::get_step_by_id($id->step_id);
		}
		else
		{
			$get_log = DB::Table('procurement_rekon_log')->where('id_upload', $id->id)->orderBy('id', 'ASC')->get();
			$find_k  = array_search($id->step_id, array_column(json_decode(json_encode($get_log, TRUE) ), 'step_id') );

			if($find_k !== FALSE)
			{
				$find_step = self::get_step_by_id($get_log[$find_k - 1]->step_id);
			}

			$next_step = self::get_step_by_id($id->step_id);
		}

		$next_next_step = self::get_step_by_id($next_step->step_after);

		$revisi = $next = $find_reject = [];

		$step_log = DB::Table('procurement_rekon_log As prl')
		->select('prl.*', 'ps.urutan', 'ps.id as id_ps', 'ps.action', 'ps.step_after')
		->leftjoin('procurement_step As ps', 'prl.step_id', '=', 'ps.id')
		->leftjoin('procurement_boq_upload As pbu', 'pbu.id', '=', 'prl.id_upload')
		->where([
			['pbu.id', $id->id]
		])
		->orderBy('prl.id', 'ASC')
		->get();

		foreach($step_log as $k => $val)
		{
			if($val->action == 'forward')
			{
				$next[$k] = $val;
			}

			if($val->status_step && !in_array($val->status_step, ['forward', 'stay']) )
			{
				$find_reject[$k] = $val;
			}
		}

		$revisi = $find_reject;

		if($find_step)
		{
			$fl = floor($find_step->urutan);

			$sisa = DB::table('procurement_step')->where([
				['action', 'forward'],
				['urutan', '>', $fl]
			])
			->OrderBy('urutan', 'ASC')
			->get();

			$lewat = DB::table('procurement_step')->where([
				['action', 'forward'],
				['urutan', '<=', $fl]
			])
			->OrderBy('urutan', 'ASC')
			->get();
		}
		else
		{
			$find_step = self::get_step_by_id($step_log->last()->step_id);
		}

		return ['current_step' => $find_step, 'remain_step' => $sisa, 'lewat' => $lewat, 'revisi' => $revisi, 'next' => $next, 'step_selanjutnya' => $next_step, 'next_next_step' => $next_next_step];
	}

	public static function alarm_expired_khs()
	{
		$data = DB::Table('procurement_mitra')->whereNotNull('tgl_khs_maintenance')->get();

		if($data)
		{
			$arr_1 =[];
			foreach($data as $val)
			{
				$botToken = '1902669622:AAHz0cl18e8-IF5waY1uQIj33Nxqq72EzFc';

				$bulan = array (
					'Januari'   => '01',
					'Februari'  => '02',
					'Maret'     => '03',
					'April'     => '04',
					'Mei'       => '05',
					'Juni'      => '06',
					'Juli'      => '07',
					'Agustus'   => '08',
					'September' => '09',
					'Oktober'   => '10',
					'November'  => '11',
					'Desember'  => '12'
				);

				$tgl = explode(' ', $val->tgl_khs_maintenance);
				$tgl_conv = $tgl[2] .'-'. $bulan[$tgl[1] ] .'-'. $tgl[0];

				if(date('Y-m-d') > $tgl_conv)
				{
					$arr_1[$val->nama_company] = "- $val->no_khs_maintenance ($val->tgl_khs_maintenance)\n";
				}
			}
			// dd($arr_1, $arr_2);
			//JAM 8 PAGI CURL NYA
			$website="https://api.telegram.org/bot".$botToken;

			// if($val->mitra_id == 22)
			// {
			// 	$ci = '519446576';
			// }
			// else
			// {
			// 	$ci = '-1001727682139';
			// }
			$ci = '519446576';

			if($arr_1)
			{
				$chunck_ar1 = array_chunk($arr_1, 20, TRUE);
				$add_msg = '';
				foreach($chunck_ar1 as $k => $v)
				{
					$message_1 = "Daftar Nomor KHS yang Expired\n";

					foreach($v as $kk => $vv)
					{
						$add_msg .= "<b>$kk</b>" .' '. $vv;
					}

					$message_1 .= $add_msg;

					$params=[
						'chat_id' => $ci,
						// 'chat_id' => '-1001727682139' atau 519446576
						'parse_mode' => 'html',
						'text' => "$message_1",
					];

					$ch = curl_init();
					$optArray = array(
						CURLOPT_URL => $website.'/sendMessage',
						CURLOPT_RETURNTRANSFER => 1,
						CURLOPT_POST => 1,
						CURLOPT_POSTFIELDS => $params,
					);
					curl_setopt_array($ch, $optArray);
					$result = curl_exec($ch);
					curl_close($ch);
				}
			}
		}
	}

	public static function find_all_rfc()
	{
		return DB::Table('procurement_designator As pd')
		->leftjoin('procurement_design_rfc As pdr', 'pd.id', '=', 'pdr.id_design')
		->select('pdr.id_design', 'pdr.design_rfc', 'pd.id as id_design', 'pd.designator', 'pd.uraian', 'pd.material', 'pd.jasa', DB::RAW('0 As sp'), DB::RAW('0 As rekon'), 'pd.design_mitra_id', 'pdr.qty_per_item', DB::RAW("(CASE WHEN pd.design_mitra_id = 16 THEN 'Telkom Akses' ELSE 'Mitra' END) As namcomp"), 'pd.design_mitra_id As jenis_khs' )
    ->whereNotNull('design_rfc')
		->where('pd.active', 1)
		->get();
	}

	public static function find_work($id, $jenis = 'kerjaan')
	{
		switch ($jenis) {
			case 'kerjaan':
				$sql = DB::Table('procurement_pekerjaan')->where('pekerjaan', $id)->first();
			break;
			case 'jenis':
				$sql = DB::Table('procurement_pekerjaan')
				->where([
					['jenis', trim($id)],
					['active', 1]
				])->get();
			break;
			default:
				$sql = DB::table('procurement_pekerjaan')->where('active', 1)->GroupBy('pekerjaan')->get();
			break;
		}

		return $sql;
	}

	public static function searching_material_complex($req)
	{
		$search = strtoupper($req->searchTerm);
		$tgl_start = $req->tahun .'-'. $req->bulan .'-01';
		$tgl_end = $req->tahun .'-'. $req->bulan . '-' . date('t', strtotime(date($req->tahun .'-'. $req->bulan, strtotime('last day of this month') ) ) );

		$find_khs = AdminModel::get_pekerjaan($req->pekerjaan);

		$check_data = DB::table('procurement_designator As pd')
		->leftjoin('procurement_mitra As pm', 'pm.id', '=', 'pd.design_mitra_id')
		->select('pd.*', DB::RAW("(CASE WHEN pd.design_mitra_id = 16 THEN 'Telkom Akses' ELSE 'Mitra' END) As namcomp"))
		->where([
			['pd.witel', $req->witel],
			['pd.active', 1],
		])
		->where(function($join) use($tgl_start, $tgl_end){
			$join->where([
				['tgl_start', '<=', $tgl_start],
				['tgl_end', '>=', $tgl_end],
			])
			->orwhereNull('tgl_start');
		})
		->where(function($join) use($search){
			$join->where(DB::RAW("REGEXP_REPLACE(TRIM(designator), '[[:space:]]+', '')"), 'LIKE', '%'. preg_replace('/\s+/', '', $search) .'%')
			->orwhere('jenis', 'LIKE', '%'.$search.'%');
		})
		->where(function($join) use($find_khs){
			$join->where('jenis', $find_khs->khs)
			->orwhere('jenis', 'NEW_ITEM');
		});

		switch ($req->jenis) {
			case 'telkom':
				$check_data->where('design_mitra_id', 16);
			break;
			case 'mitra':
				$check_data->where('design_mitra_id', 0);
			break;
		}

		$check_data = $check_data->get();

		return $check_data;
	}

	public static function timeline($witel, $tgl_hidden, $tgl, $mitra, $pekerjaan, $view_table)
	{
    $sql = '';

    if(!empty($witel) && $witel != 'All')
		{
			$sql .= "AND pbu.witel LIKE '%$witel%' ";
		}

		switch ($tgl_hidden) {
			case 'Tahun':
				$sql .=" AND pbu.created_at LIKE '$tgl%' ";
			break;
			case 'Custom':
				$tgl = explode(' - ', $tgl);
				$sql .=" AND DATE_FORMAT(pbu.tgl_sp, '%Y-%m-%d') BETWEEN '$tgl[0]' AND '$tgl[1]' ";
			break;
			case 'Bulan':
				$sql .=" AND MONTH(pbu.created_at) = $tgl";
			break;
		}

		if(session('auth')->proc_level == 2)
		{
			$sql .= " AND pbu.mitra_id = ".session('auth')->id_pm;
		}
		else
		{
			if(!empty($mitra) && $mitra != 'all')
			{
				$sql .= " AND pbu.mitra_id = $mitra";
			}
		}

		if(!empty($pekerjaan) && strcasecmp($pekerjaan, 'all') != 0)
		{
			$sql .= " AND pbu.pekerjaan = '$pekerjaan'";
		}

    if(session('auth')->peker_pup)
    {
      $pekerjaan = explode(', ', session('auth')->peker_pup );
			$imp_peker = "'". implode("', '", $pekerjaan) . "'";
      $sql .= " AND pbu.pekerjaan IN ($imp_peker) ";
    }

    if($view_table == 'mitra_only')
    {
      if(session('auth')->proc_level == 2)
      {
        $sql .= " AND pbu.mitra_id = ".session('auth')->id_pm;
      }

      $data = DB::SELECT("SELECT pbu.id as id_pbu, pbu.mitra_nm, pbu.tgl_last_update, pbu.step_id as step_pbu,
      ps1.aktor as aktor_clear, ps1.aktor_nama as nama_clear, ps1.action as action_skrg,
      ps2.aktor as aktor_next, ps2.aktor_nama as nama_next, ps2.action as action_next,
      DATEDIFF(NOW(), pbu.tgl_last_update) as jml_hri
      FROM procurement_boq_upload pbu
      LEFT JOIN procurement_step ps1 ON ps1.id = pbu.step_id
      LEFT JOIN procurement_step ps2 ON ps2.id = ps1.step_after
      WHERE pbu.active = 1 $sql
      GROUP BY pbu.id");

			$fn = $final_data =  [];

      foreach($data as $v)
      {
        $fn[$v->mitra_nm]['aktor']       = $v->mitra_nm;
        $fn[$v->mitra_nm]['close']       = 0;
        $fn[$v->mitra_nm]['trouble']     = 0;
        $fn[$v->mitra_nm]['kada_close']  = 0;
        $fn[$v->mitra_nm]['unclose_D1D'] = 0;
        $fn[$v->mitra_nm]['unclose_1D']  = 0;
        $fn[$v->mitra_nm]['unclose_2D']  = 0;
        $fn[$v->mitra_nm]['unclose_3D']  = 0;
        $fn[$v->mitra_nm]['unclose_3DD'] = 0;
        $fn[$v->mitra_nm]['unclose_7DD'] = 0;
      }

      foreach($data as $v)
      {
        if('Adm. Mitra' == $v->nama_clear)
        {
          if($v->action_skrg == 'forward')
          {
            $fn[$v->mitra_nm]['close'] += 1;

            $find_data = DB::SELECT("SELECT prl.* FROM procurement_rekon_log prl
            LEFT JOIN procurement_step ps ON ps.id = prl.step_id
            WHERE aktor_nama = '$v->nama_clear' AND id_upload = $v->id_pbu AND closed_at IS NOT NULL");

            foreach($find_data as $fda)
            {
              $date1 = strtotime($fda->created_at);
              $date2 = strtotime($fda->closed_at);

              $seconds = $date2 - $date1;

              $hour = abs($seconds / (60 * 60) );

              // if($hour > 0)
              // {
              //   if(!isset($fn[$v->mitra_nm]['jml_hari'][$v->id_pbu]) )
              //   {
              //     $fn[$v->mitra_nm]['jml_hari'][$v->id_pbu] = 0;
              //   }
              //     $fn[$v->mitra_nm]['jml_hari'][$v->id_pbu] += $hour;
              //   //  $fn[$v->mitra_nm]['tex'][$v->id_pbu] += $hour;
              // }

              if(!isset($fn[$v->mitra_nm]['jml_hari'][$v->id_pbu]) )
              {
                $fn[$v->mitra_nm]['jml_hari'][$v->id_pbu]['jam'] = 0;
                $fn[$v->mitra_nm]['jml_hari'][$v->id_pbu]['total_pekerjaan'] = 0;
              }
              $fn[$v->mitra_nm]['jml_hari'][$v->id_pbu]['jam'] += $hour;
              $fn[$v->mitra_nm]['jml_hari'][$v->id_pbu]['total_pekerjaan'] += 1;
            }
          }

          if($v->action_skrg != 'forward')
          {
            $fn[$v->mitra_nm]['trouble'] += 1;
          }
        }

        if('Adm. Mitra' == $v->nama_next && $v->action_skrg == 'forward')
        // if($v->action_skrg == 'forward')
        {
					if($v->jml_hri == 0)
					{
						$fn[$v->mitra_nm]['unclose_D1D'] += 1;
					}

          if($v->jml_hri == 1)
          {
            $fn[$v->mitra_nm]['unclose_1D'] += 1;
          }

          if($v->jml_hri == 2)
          {
            $fn[$v->mitra_nm]['unclose_2D'] += 1;
          }

          if($v->jml_hri == 3)
          {
            $fn[$v->mitra_nm]['unclose_3D'] += 1;
          }

          if($v->jml_hri < 7 && $v->jml_hri > 3)
          {
            $fn[$v->mitra_nm]['unclose_3DD'] += 1;
          }

          if($v->jml_hri >= 7)
          {
            $fn[$v->mitra_nm]['unclose_7DD'] += 1;
          }

					$find_data = DB::SELECT("SELECT prl.* FROM procurement_rekon_log prl WHERE id_upload = $v->id_pbu ORDER BY id DESC LIMIT 1");

					$date1 = strtotime($find_data[0]->closed_at);
					$date2 = strtotime(date('Y-m-d H:i:s') );

					$seconds = $date2 - $date1;

					$hour = abs($seconds / (60 * 60) );

					// if($hour > 0)
					// {
					//   if(!isset($fn[$v->mitra_nm]['jml_hari'][$v->id_pbu]) )
					//   {
					//     $fn[$v->mitra_nm]['jml_hari'][$v->id_pbu] = 0;
					//   }
					//   $fn[$v->mitra_nm]['jml_hari'][$v->id_pbu] += $hour;
					//   //  $fn[$v->mitra_nm]['tex'][$v->id_pbu] += $hour;
					// }

					if(!isset($fn[$v->mitra_nm]['jml_hari_open'][$v->id_pbu]) )
					{
						$fn[$v->mitra_nm]['jml_hari_open'][$v->id_pbu]['jam_open'] = 0;
						$fn[$v->mitra_nm]['jml_hari_open'][$v->id_pbu]['total_pekerjaan_open'] = 0;
					}

					$fn[$v->mitra_nm]['jml_hari_open'][$v->id_pbu]['jam_open'] += $hour;
					$fn[$v->mitra_nm]['jml_hari_open'][$v->id_pbu]['total_pekerjaan_open'] += 1;

          $fn[$v->mitra_nm]['kada_close'] += 1;
        }
      }

      foreach($fn as $k => $v)
      {
        $final_data[$k] = $v;

				if(isset($v['jml_hari']) )
				{
					foreach($v['jml_hari'] as $kk => $vv)
          {
            $final_data[$k]['jml_hari_conv'][$kk] = ROUND($vv['jam'] / $vv['total_pekerjaan'], 2);
            // $final_data[$k]['jml_hari_conv'][$kk] = ROUND($vv['jam'] / $v['close'] / $vv['total_pekerjaan'], 2);
            // $final_data[$k]['jml_hari_conv'][$kk] = ROUND($vv['jam'] / $v['close'], 2);
          }
        }

				if(isset($v['jml_hari_open']) )
				{
					foreach($v['jml_hari_open'] as $kk => $vv)
          {
            $final_data[$k]['jml_hari_conv_open'][$kk] = ROUND($vv['jam_open'] / $vv['total_pekerjaan_open'], 2);
            // $final_data[$k]['jml_hari_conv_open'][$kk] = ROUND($vv['jam_open'] / $v['kada_close'], 2);
          }
				}
      }
    }
    else
    {
      // dd($sql);
      $data = DB::SELECT("SELECT pbu.id as id_pbu, pbu.tgl_last_update, pbu.step_id as step_pbu,
      ps1.aktor as aktor_clear, ps1.aktor_nama as nama_clear, ps1.action as action_skrg,
      ps2.aktor as aktor_next, ps2.aktor_nama as nama_next, ps2.action as action_next,
      DATEDIFF(NOW(), pbu.tgl_last_update) as jml_hri
      FROM procurement_boq_upload pbu
      LEFT JOIN procurement_step ps1 ON ps1.id = pbu.step_id
      LEFT JOIN procurement_step ps2 ON ps2.id = ps1.step_after
      WHERE pbu.active = 1 AND pbu.step_id != 14 $sql
      GROUP BY pbu.id");

      $data_aktor = DB::table('procurement_step')->where('active', 1);

      if(!in_array(session('auth')->proc_level, [4, 99, 44]) )
      {
        $data_aktor->where('aktor', session('auth')->proc_level);
      }

      $data_aktor = $data_aktor->GroupBy('aktor_nama')->get();

			$fn = $final_data =  [];

      foreach($data_aktor as $v)
      {
        $fn[$v->aktor_nama]['aktor']       = $v->aktor;
        $fn[$v->aktor_nama]['close']       = 0;
        $fn[$v->aktor_nama]['trouble']     = 0;
        $fn[$v->aktor_nama]['kada_close']  = 0;
        $fn[$v->aktor_nama]['unclose_D1D'] = 0;
        $fn[$v->aktor_nama]['unclose_1D']  = 0;
        $fn[$v->aktor_nama]['unclose_2D']  = 0;
        $fn[$v->aktor_nama]['unclose_3D']  = 0;
        $fn[$v->aktor_nama]['unclose_3DD'] = 0;
        $fn[$v->aktor_nama]['unclose_7DD'] = 0;
      }

      foreach($data as $v)
      {
        if($v->action_skrg == 'forward')
				{
					$fn[$v->nama_clear]['close'] += 1;

					$find_data = DB::SELECT("SELECT prl.* FROM procurement_rekon_log prl
					LEFT JOIN procurement_step ps ON ps.id = prl.step_id
					WHERE aktor_nama = '$v->nama_clear' AND id_upload = $v->id_pbu AND closed_at IS NOT NULL");

					foreach($find_data as $fda)
					{
						$date1 = strtotime($fda->created_at);
						$date2 = strtotime($fda->closed_at);

						$seconds = $date2 - $date1;

						$hour = abs($seconds / (60 * 60) );

						// if($hour > 0)
						// {
						//   if(!isset($fn[$v->nama_clear]['jml_hari'][$v->id_pbu]) )
						//   {
						//     $fn[$v->nama_clear]['jml_hari'][$v->id_pbu] = 0;
						//   }
						//   $fn[$v->nama_clear]['jml_hari'][$v->id_pbu] += $hour;
						//   //  $fn[$v->nama_clear]['tex'][$v->id_pbu] += $hour;
						// }

						if(!isset($fn[$v->nama_clear]['jml_hari'][$v->id_pbu]) )
						{
							$fn[$v->nama_clear]['jml_hari'][$v->id_pbu]['jam'] = 0;
							$fn[$v->nama_clear]['jml_hari'][$v->id_pbu]['total_pekerjaan'] = 0;
						}

						$fn[$v->nama_clear]['jml_hari'][$v->id_pbu]['jam'] += $hour;
						$fn[$v->nama_clear]['jml_hari'][$v->id_pbu]['total_pekerjaan'] += 1;
					}
				}

				if($v->action_skrg != 'forward')
				{
					if(isset($fn[$v->nama_next]) )
					{
						$fn[$v->nama_next]['trouble'] += 1;
					}
				}

				if($v->action_skrg == 'forward')
				{
					if($v->jml_hri == 0)
					{
						$fn[$v->nama_next]['unclose_D1D'] += 1;
					}

					if($v->jml_hri == 1)
					{
						$fn[$v->nama_next]['unclose_1D'] += 1;
					}

					if($v->jml_hri == 2)
					{
						$fn[$v->nama_next]['unclose_2D'] += 1;
					}

					if($v->jml_hri == 3)
					{
						$fn[$v->nama_next]['unclose_3D'] += 1;
					}

					if($v->jml_hri < 7 && $v->jml_hri > 3)
					{
						$fn[$v->nama_next]['unclose_3DD'] += 1;
					}

					if($v->jml_hri >= 7)
					{
						$fn[$v->nama_next]['unclose_7DD'] += 1;
					}

					$find_data = DB::SELECT("SELECT prl.* FROM procurement_rekon_log prl WHERE id_upload = $v->id_pbu ORDER BY id DESC LIMIT 1");

					$date1 = strtotime($find_data[0]->closed_at);
					$date2 = strtotime(date('Y-m-d H:i:s') );

					$seconds = $date2 - $date1;

					$hour = abs($seconds / (60 * 60) );

					// if($hour > 0)
					// {
					//   if(!isset($fn[$v->nama_next]['jml_hari'][$v->id_pbu]) )
					//   {
					//     $fn[$v->nama_next]['jml_hari'][$v->id_pbu] = 0;
					//   }
					//   $fn[$v->nama_next]['jml_hari'][$v->id_pbu] += $hour;
					//   //  $fn[$v->nama_next]['tex'][$v->id_pbu] += $hour;
					// }

					if(!isset($fn[$v->nama_next]['jml_hari_open'][$v->id_pbu]) )
					{
						$fn[$v->nama_next]['jml_hari_open'][$v->id_pbu]['jam_open'] = 0;
						$fn[$v->nama_next]['jml_hari_open'][$v->id_pbu]['total_pekerjaan_open'] = 0;
					}

					$fn[$v->nama_next]['jml_hari_open'][$v->id_pbu]['jam_open'] += $hour;
					$fn[$v->nama_next]['jml_hari_open'][$v->id_pbu]['total_pekerjaan_open'] += 1;

					$fn[$v->nama_next]['kada_close'] += 1;
				}
      }

      foreach($fn as $k => $v)
      {
        $final_data[$k] = $v;

        if(isset($v['jml_hari']) )
				{
					foreach($v['jml_hari'] as $kk => $vv)
          {
            // $final_data[$k]['jml_hari_conv'][$kk] = ROUND($vv['jam'] / $v['close'] / $vv['total_pekerjaan'], 2);
            $final_data[$k]['jml_hari_conv'][$kk] = ROUND($vv['jam'] / $vv['total_pekerjaan'], 2);
            // $final_data[$k]['jml_hari_conv'][$kk] = ROUND($vv['jam'] / $v['close'], 2);
          }
        }

				if(isset($v['jml_hari_open']) )
				{
					foreach($v['jml_hari_open'] as $kk => $vv)
          {
            $final_data[$k]['jml_hari_conv_open'][$kk] = ROUND($vv['jam_open'] / $vv['total_pekerjaan_open'], 2);
            // $final_data[$k]['jml_hari_conv_open'][$kk] = ROUND($vv['jam_open'] / $v['kada_close'], 2);
          }
				}
      }
    }
    // dd($final_data);
    return $final_data;
	}

	public static function update_budget_sp($req, $jenis, $id)
	{
		$pid = $req->pid_be;

    if($req->status_pid == 'BE')
    {
      if($jenis == 'sp')
      {
        $step_id = 26;
      }
      else
      {
        $step_id = 32;
      }
      // dd($pid);
      foreach($pid as $v)
      {
        $arr['budget_exceeded'] = 1;
        $exp = explode('-', $v);
        $pid_s = $exp[0] .'-'. $exp[1];
        $wbs = $exp[2] .'-'. $exp[3] .'-'. $exp[4];

        if($wbs == '01-01-02')
        {
          $arr['be_material_mitra'] = 1;
        }

        if($wbs == '01-02-01')
        {
          $arr['be_jasa'] = 1;
        }

        if($wbs == '01-01-01')
        {
          $arr['be_material_ta'] = 1;
        }
        // dd($arr, $pid_s);
        DB::Table('procurement_pid')->where('pid', $pid_s)->update($arr);
      }
      $msg = 'Budget Exceeded';
    }
    else
    {
      $step_id = 27;
      foreach($pid as $v)
      {
        DB::table('procurement_req_pid')->where('id_pid_req', $v)->delete();
      }
      $msg = 'Salah PID';
    }

    DB::table('procurement_boq_upload')->where('id', $id)->update([
      'step_id' => $step_id,
      'detail' => $req->detail,
      'modified_by' => session('auth')->id_user
    ]);

    AdminModel::save_log($step_id, $id, $req->detail);
		return $msg;
	}

	public static function reset_tgl_terakhir_update()
	{
		$a = DB::table('procurement_boq_upload')->whereIn('pekerjaan', ['QE', 'Construction'])->get();
		$b = DB::table('procurement_rekon_log')->get();
		$b = json_decode(json_encode($b), TRUE);

		foreach($a as $v)
		{
			$find_k_1 = array_keys(array_column($b, 'id_upload'), $v->id);

			foreach($find_k_1 as $vv)
			{
				$b_new = $b[$vv];
				if($b_new['step_id'] == $v->step_id)
				{
					$c[$v->id]['step_id_pbu'] = $v->step_id;
					$c[$v->id]['step_id_prl'] = $b_new['step_id'];
					$c[$v->id]['closed_at_pbu'] = $v->tgl_last_update;
					$c[$v->id]['closed_at_prl'] = $b_new['closed_at'];
				}
			}
		}

		foreach($c as $k => $v)
		{
			DB::Table('procurement_boq_upload')->where('id', $k)->update([
				'tgl_last_update' => $v['closed_at_prl']
			]);
		}
	}

	public static function get_project_proaktif()
	{
		return DB::table('proaktif_project')
		->leftJoin('mpromise_lop', 'proaktif_project.proaktif_id','=', DB::raw('mpromise_lop.pid AND mpromise_lop.isHapus = 0') )
		->leftjoin('procurement_proaktif_boq As ppb' , 'ppb.id_proaktif_project', '=', 'proaktif_project.id')
		->leftJoin('procurement_boq_upload', 'procurement_boq_upload.id', '=', 'ppb.id_pbu' )
		->select('proaktif_project.*')
		->whereIn('proaktif_project.status_project', ['Active', 'Init'])
    ->where('proaktif_project.customer', 'like', '%'. session('auth')->Witel_New .'%')
		->where(DB::RAW("LENGTH(proaktif_project.sapid)"), '>', 5)
		->WhereNull('procurement_boq_upload.id')
		->GroupBy('proaktif_project.id')
		->get();
	}

	public static function get_detail_proaktif($id)
	{
		$data = DB::table('proaktif_project')->where('id', $id)->first();
		$grab =  GrabController::get_proaktif_boq($data->proaktif_id);

		foreach($grab as $k => $v)
		{
			$grab[$k]['nama_project'] = $data->nama_project;
		}
		return $grab;
	}

	public static function saveCutoffPsb($req)
	{
		$get_sc = str_replace(array("\r\n", "\n", " "), "", explode(';', $req->input('sc_cutoff')));
		$insert[] = [];

		foreach ($get_sc as $value)
		{
			if ($value != "")
			{
				$check = DB::table('procurement_cutoff_psb')->where('sc_id', $value)->first();

				if ($check == null)
				{
					$insert[] = [
						'tahun_kegiatan'   => date('Y'),
						'periode_kegiatan' => $req->input('periode_kegiatan'),
						'periode1'         => $req->input('periode1'),
						'periode2'         => $req->input('periode2'),
						'mitra_id'         => $req->input('mitra'),
						'sc_id'            => $value,
						'layanan'          => $req->input('layanan'),
						'created_by'       => session('auth')->id_user
					];
				}
			}
		}

		if (array_filter($insert))
		{
			$chunk = array_chunk(array_filter($insert), 500);
			foreach($chunk as $data)
			{
				DB::table('procurement_cutoff_psb')->insert($data);
			}
		}
	}

	public static function saveDataRekonPsb($req)
	{
		$id_mitra = $req->input('mitra');
		$is_layanan = $req->input('layanan');
		$order_id = str_replace(';', ',', str_replace(array('\r\n', '\r', '\n', ' ', '"'), '', json_encode($req->input('sc_cutoff'))));

		switch ($is_layanan) {
			case 'ont_premium':
				$result = DB::table('ont_premium_log')->where('kode', 'ps')->whereIn('wo_id', [$order_id])->get();

				foreach ($result as $key => $value)
				{
					$insert[] = [
						'id_mitra' => $id_mitra,
						'nik_teknisi1' => $value->nik_teknisi_1,
						'nik_teknisi2' => $value->nik_teknisi_2,
						'is_layanan' => $is_layanan,
						'regional' => $value->reg,
						'witel' => 'BANJARMASIN',
						'order_id' => $value->wo_id,
						'pots' => $value->no_telp,
						'speedy' => $value->no_internet,
						'last_updated_date' => $value->tanggal_usage,
						'last_updated_date_fix' => $value->tanggal_usage,
						'customer_name' => $value->nama_pelanggan,
						'no_hp' => $value->no_hp,
						'install_address' => $value->alamat_pelanggan,
						'customer_address' => $value->alamat_pelanggan,
						'serial_number_other' => $value->sn_ont_baru
					];
				}
				break;

			case 'migrasi_stb':
				$result = DB::table('migrasi_stb_premium_log')->where('kode', 'ps')->whereIn('wo_id', [$order_id])->get();

				foreach ($result as $key => $value)
				{
					$insert[] = [
						'id_mitra' => $id_mitra,
						'nik_teknisi1' => $value->nik_teknisi_1,
						'nik_teknisi2' => $value->nik_teknisi_2,
						'is_layanan' => $is_layanan,
						'regional' => $value->reg,
						'witel' => 'BANJARMASIN',
						'sto' => $value->sto,
						'order_id' => $value->wo_id,
						'pots' => $value->nd_pots,
						'speedy' => $value->no_internet,
						'last_updated_date' => $value->tanggal_usage,
						'last_updated_date_fix' => $value->tanggal_usage,
						'customer_name' => $value->nama_pelanggan,
						'no_hp' => $value->no_hp_pelanggan,
						'install_address' => $value->alamat_pelanggan,
						'customer_address' => $value->alamat_pelanggan,
						'serial_number_other' => $value->stb_id
					];
				}
				break;

			default:
				$result = DB::select('
					SELECT
						"'.$id_mitra.'" AS id_mitra,
						r.nik1 AS nik_teknisi1,
						r.nik2 AS nik_teknisi2,
						"'.$is_layanan.'" AS is_layanan,
						kpt.REGIONAL AS regional,
						kpt.WITEL AS witel,
						kpt.DATEL AS datel,
						kpt.STO AS sto,
						dps.orderIdInteger AS order_id,
						kpt.PACKAGE_NAME AS package_name,
						kpt.FLAG_DEPOSIT AS flag_deposit,
						kpt.TYPE_TRANSAKSI AS type_transaksi,
						kpt.TYPE_LAYANAN AS type_layanan,
						kpt.LOC_ID AS alpro_name,
						kpt.NCLI AS ncli,
						kpt.POTS AS pots,
						kpt.SPEEDY AS speedy,
						kpt.STATUS_RESUME AS status_resume,
						kpt.STATUS_MESSAGE AS status_message,
						kpt.ORDER_DATE AS order_date,
						dps.orderDate AS order_date2,
						kpt.LAST_UPDATED_DATE AS last_updated_date,
						dps.orderDatePs AS last_updated_date2,
						kpt.CUSTOMER_NAME AS customer_name,
						kpt.NO_HP AS no_hp,
						kpt.INSTALL_ADDRESS AS install_address,
						kpt.CUSTOMER_ADDRESS AS customer_address,
						kpt.KCONTACT AS kcontact,
						kpt.GPS_LATITUDE AS gps_latitude,
						kpt.GPS_LONGITUDE AS gps_longitude,
						utt.statusName AS status_utonline,
						pl.snont AS serial_number_ont,
						pl.snstb AS serial_number_stb,
						NULL AS serial_number_other,
						pmp.drop_core,
						pmp.precon,
						pmp.tiang,
						pmp.klem_ring,
						pmp.otp,
						pmp.prekso,
						pmp.s_clamp,
						pmp.soc,
						pmp.clamp_hook,
						pmp.tray_cable
					FROM Data_Pelanggan_Starclick dps
					LEFT JOIN kpro_tr6 kpt ON dps.orderIdInteger = kpt.ORDER_ID AND kpt.WITEL = "BANJARMASIN"
					LEFT JOIN utonline_tr6 utt ON dps.internet = utt.noInternet AND utt.witel IN ("KALSEL (BANJARMASIN)", "BANJARMASIN")
					LEFT JOIN dispatch_teknisi dt ON dps.orderIdInteger = dt.NO_ORDER
					LEFT JOIN pemakaian_material_psb pmp ON dps.orderIdInteger = pmp.sc_id
					LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
					LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
					LEFT JOIN regu r ON dt.id_regu = r.id_regu
					LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
					WHERE
						(dt.jenis_order IN ("SC", "MYIR") OR dt.jenis_order IS NULL) AND
						(dt.jenis_layanan NOT IN ("CABUT_NTE", "QC") OR dt.jenis_layanan IS NULL) AND
						dps.orderIdInteger IN ('.$order_id.')
					GROUP BY dps.orderIdInteger
				');

				foreach ($result as $key => $value)
				{
					$insert[] = [
						'id_mitra' => $value->id_mitra,
						'nik_teknisi1' => $value->nik_teknisi1,
						'nik_teknisi2' => $value->nik_teknisi2,
						'is_layanan' => $value->is_layanan,
						'regional' => $value->regional,
						'witel' => $value->witel,
						'datel' => $value->datel,
						'sto' => $value->sto,
						'order_id' => $value->order_id,
						'package_name' => $value->package_name,
						'flag_deposit' => $value->flag_deposit,
						'type_transaksi' => $value->type_transaksi,
						'type_layanan' => $value->type_layanan,
						'alpro_name' => $value->alpro_name,
						'ncli' => $value->ncli,
						'pots' => $value->pots,
						'speedy' => $value->speedy,
						'status_resume' => $value->status_resume,
						'status_message' => $value->status_message,
						'order_date' => $value->order_date ?? $value->order_date2,
						'last_updated_date' => $value->last_updated_date ?? $value->last_updated_date2,
						'last_updated_date_fix' => $value->last_updated_date ?? $value->last_updated_date2,
						'customer_name' => $value->customer_name,
						'no_hp' => $value->no_hp,
						'install_address' => $value->install_address,
						'customer_address' => $value->customer_address,
						'kcontact' => $value->kcontact,
						'gps_latitude' => $value->gps_latitude,
						'gps_longitude' => $value->gps_longitude,
						'status_utonline' => $value->status_utonline,
						'serial_number_ont' => $value->serial_number_ont,
						'serial_number_stb' => $value->serial_number_stb,
						'serial_number_other' => $value->serial_number_other,
						'drop_core' => $value->drop_core,
						'precon' => $value->precon,
						'tiang' => $value->tiang,
						'klem_ring' => $value->klem_ring,
						'otp' => $value->otp,
						'prekso' => $value->prekso,
						's_clamp' => $value->s_clamp,
						'soc' => $value->soc,
						'clamp_hook' => $value->clamp_hook,
						'tray_cable' => $value->tray_cable
					];
				}
				break;
		}

		if (count($insert) > 0)
		{
			DB::table('procurement_order_psb')->insert($insert);
		}
	}

	private static function save_order_addon($req)
	{
		$reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader('Xlsx');
		$reader->setReadDataOnly(true);
		$spreadSheet = $reader->load($req->file('upload_addon'));
		$sheet = $spreadSheet->getSheet($spreadSheet->getFirstSheetIndex());
		$data = $sheet->toArray();
		unset($data[0]);
		foreach ($data as $key => $value)
		{
			if (count($value) == 2)
			{
				$check = DB::table('procurement_order_psb')->where('speedy', $value['0'])->where('is_addon', 1)->first();
				if ($check != null)
				{
					DB::table('procurement_order_psb')
					->where('speedy', $value['0'])
					->where('is_addon', 1)
					->update([
						'id_mitra' => $req->input('id_mitra'),
						'is_addon' => 2,
						'dtm_is_addon' => date('Y-m-d H:i:s')
					]);
				} else {
					DB::table('procurement_order_psb')
					->insert([
						'id_mitra' => $req->input('id_mitra'),
						'speedy' => $value[0],
						'is_layanan' => $value[1],
						'is_addon' => 3,
						'dtm_is_addon' => date('Y-m-d H:i:s')
					]);
				}
			}
		}
	}

	public static function save_doc_generate_new($req)
	{
		$bulan = DB::table('bulan_format')->where('id', $req->input('periode_kegiatan'))->first();
		$mitra = DB::TABLE('procurement_mitra')->where('tacticalpro_mitra_id', $req->input('id_mitra'))->first();;
		$hss   = DB::table('procurement_hss_psb')->where('witel', $mitra->witel)->first();

		// dd($req->all(), $bulan, $mitra, $hss);

		$jml_pekerjaan_psb =
			($req->input('p1_survey_ssl') * str_replace('.', '', $req->input('p1_survey_hss') )) +
			($req->input('p2_tlpint_survey_ssl') * str_replace('.', '', $req->input('p2_tlpint_survey_hss') )) +
			($req->input('p2_intiptv_survey_ssl') * str_replace('.', '', $req->input('p2_intiptv_survey_hss') )) +
			($req->input('p3_survey_ssl') * str_replace('.', '', $req->input('p3_survey_hss') )) +
			($req->input('p1_ssl') * str_replace('.', '', $req->input('p1_hss') )) +
			($req->input('p2_tlpint_ssl') * str_replace('.', '', $req->input('p2_tlpint_hss') )) +
			($req->input('p2_intiptv_ssl') * str_replace('.', '', $req->input('p2_intiptv_hss') )) +
			($req->input('p3_ssl') * str_replace('.', '', $req->input('p3_hss') ));

		$jml_pekerjaan_migrasi =
			($req->input('migrasi_service_1p2p_ssl') * str_replace('.', '', $req->input('migrasi_service_1p2p_hss') )) +
			($req->input('migrasi_service_1p3p_ssl') * str_replace('.', '', $req->input('migrasi_service_1p3p_hss') )) +
			($req->input('migrasi_service_2p3p_ssl') * str_replace('.', '', $req->input('migrasi_service_2p3p_hss') ));

		$jml_pekerjaan_tambahan =
			($req->input('ikr_addon_stb_ssl') * str_replace('.', '', $req->input('ikr_addon_stb_hss') )) +
			($req->input('indihome_smart_ssl') * str_replace('.', '', $req->input('indihome_smart_hss') )) +
			($req->input('wifiextender_ssl') * str_replace('.', '', $req->input('wifiextender_hss') )) +
			($req->input('plc_ssl') * str_replace('.', '', $req->input('plc_hss') )) +
			($req->input('lme_wifi_pt1_ssl') * str_replace('.', '', $req->input('lme_wifi_pt1_hss') )) +
			($req->input('lme_ap_indoor_ssl') * str_replace('.', '', $req->input('lme_ap_indoor_hss') )) +
			($req->input('lme_ap_outdoor_ssl') * str_replace('.', '', $req->input('lme_ap_outdoor_hss') )) +
			($req->input('ont_premium_ssl') * str_replace('.', '', $req->input('ont_premium_hss') )) +
			($req->input('migrasi_stb_ssl') * str_replace('.', '', $req->input('migrasi_stb_hss') )) +
			($req->input('instalasi_ipcam_ssl') * str_replace('.', '', $req->input('instalasi_ipcam_hss') )) +
			($req->input('pu_s7_140_ssl') * str_replace('.', '', $req->input('pu_s7_140_hss') )) +
			($req->input('pu_s9_140_ssl') * str_replace('.', '', $req->input('pu_s9_140_hss') ));

		// $total_sp = substr_replace(floor($jml_pekerjaan_psb + $jml_pekerjaan_tambahan), '000', -3, 3);
		$total_sp = ($jml_pekerjaan_psb + $jml_pekerjaan_migrasi + $jml_pekerjaan_tambahan);

		if (in_array(date('m'), [1, 2, 3]))
		{
			// Q1
			$kpi_target_pspi = 96;
			$kpi_target_tticomply = 91;
		}
		elseif (in_array(date('m'), [4, 5, 6]))
		{
			// Q2
			$kpi_target_pspi = 97;
			$kpi_target_tticomply = 95;
		}
		elseif (in_array(date('m'), [7, 8, 9, 10, 11, 12]))
		{
			// Q3 Q4
			$kpi_target_pspi = 98;
			$kpi_target_tticomply = 99;
		};

		$kpi_pspi = @round( (str_replace(',', '.', $req->input('kpi_real_pspi')) / $kpi_target_pspi) * 100, 2);
		$kpi_tticomply = @round( (str_replace(',', '.', $req->input('kpi_real_tticomply')) / $kpi_target_tticomply) * 100, 2);
		$kpi_ffg = @round( (str_replace(',', '.', $req->input('kpi_real_ffg')) / 99) * 100, 2);
		$kpi_validasicore = @round( (str_replace(',', '.', $req->input('kpi_real_validasicore')) / 100) * 100, 2);
		$kpi_ttrffg = @round( (str_replace(',', '.', $req->input('kpi_real_ttrffg')) / 99) * 100, 2);
		$kpi_ujipetik = @round( (str_replace(',', '.', $req->input('kpi_real_ujipetik')) / 99) * 100, 2);
		$kpi_qc2 = @round( (str_replace(',', '.', $req->input('kpi_real_qc2')) / 99) * 100, 2);

		$ach_kpi_pspi = @round( ($kpi_pspi * 25) / 100, 2);
		$ach_kpi_tticomply = @round( ($kpi_tticomply * 25) / 100, 2);
		$ach_kpi_ffg = @round( ($kpi_ffg * 15) / 100, 2);
		$ach_kpi_validasicore = @round( ($kpi_validasicore * 5) / 100, 2);
		$ach_kpi_ttrffg = @round( ($kpi_ttrffg * 10) / 100, 2);
		$ach_kpi_ujipetik = @round( ($kpi_ujipetik * 10) / 100, 2);
		$ach_kpi_qc2 = @round( ($kpi_qc2 * 10) / 100, 2);

		$ach_kpi = ($ach_kpi_pspi + $ach_kpi_tticomply + $ach_kpi_ffg + $ach_kpi_validasicore + $ach_kpi_ttrffg + $ach_kpi_ujipetik + $ach_kpi_qc2);

		if ($ach_kpi > 100)
		{
			$kpi = 100;
		}
		elseif ($ach_kpi < 90)
		{
			$kpi = 90;
		}
		else
		{
			$kpi = $ach_kpi;
		};

		$id = DB::table('procurement_boq_upload')->insertGetId([
			'judul'              => "PEKERJAAN PASANG SAMBUNGAN BARU (PSB) PERIODE ".$bulan->bulan. " ".date('Y'). " WITEL ".strtoupper($mitra->witel)."",
			'pekerjaan'          => "PSB",
			'jenis_work'         => "Pasang Sambungan Baru (PSB)",
			'witel'              => strtoupper($mitra->witel),
			'step_id'            => 12,
			'mitra_id'           => $mitra->id,
			'mitra_nm'           => $mitra->nama_company,
			'tanggal_boq'        => $req->input('tanggal_terbit'),
			'toc'                => $req->input('toc'),
			'tgl_sp'             => $req->input('tgl_sp'),
			'no_sp'              => $req->input('no_sp'),
			'total_sp'           => $total_sp,
			'id_project'         => $req->input('id_project'),
			'pks'                => $req->input('pks'),
			'tgl_pks'            => $req->input('tgl_pks'),
			'masa_berlaku_pks'   => $req->input('masa_berlaku_pks'),
			'invoice'            => $req->input('invoice'),
			'spp_num'            => $req->input('spp_num'),
			'receipt_num'        => $req->input('receipt_num'),
			'faktur'             => $req->input('faktur'),
			'tgl_faktur'         => $req->input('tgl_faktur'),
			'surat_penetapan'    => $req->input('surat_penetapan'),
			'tgl_s_pen'          => $req->input('tgl_s_pen'),
			'surat_kesanggupan'  => $req->input('surat_kesanggupan'),
			'tgl_surat_sanggup'  => $req->input('tgl_surat_sanggup'),
			'no_baij'            => $req->input('no_baij'),
			'BAUT'               => $req->input('BAUT'),
			'tgl_baut'           => $req->input('rekon_periode2'),
			'BAST'               => $req->input('BAST'),
			'tgl_bast'           => $req->input('rekon_periode2'),
			'no_bap'             => $req->input('no_bap'),
			'BAPP'               => $req->input('BAPP'),
			'no_ba_pemeliharaan' => $req->input('no_ba_pemeliharaan'),
			'created_by'         => session('auth')->id_user,
			'modified_by'        => session('auth')->id_user,
			'verificator_by'     => session('auth')->id_user,
			'verificator_at'     => date('Y-m-d H:i:s')
		]);

		$desc = ['SP', 'BARIJ'];
		foreach ($desc as $value)
		{
			DB::table('procurement_nilai_psb')->insert([
				'id_dokumen'               => $id,
				'flag_dok_psb'             => $req->input('flag_dok_psb'),
				'desc'                     => $value,
				'periode_kegiatan'         => $req->input('periode_kegiatan'),
				'rekon_periode1'           => $req->input('rekon_periode1'),
				'rekon_periode2'           => $req->input('rekon_periode2'),
				'order_periode1'           => $req->input('order_periode1'),
				'order_periode2'           => $req->input('order_periode2'),
				'ppn_numb'                 => $req->input('ppn_numb'),
				'total_nilai_sp'           => $total_sp,
				'persentase_7_kpi'         => $kpi,
				'kpi_real_pspi'            => $req->input('kpi_real_pspi'),
				'kpi_real_tticomply'       => $req->input('kpi_real_tticomply'),
				'kpi_real_ffg'             => $req->input('kpi_real_ffg'),
				'kpi_real_validasicore'    => $req->input('kpi_real_validasicore'),
				'kpi_real_ttrffg'          => $req->input('kpi_real_ttrffg'),
				'kpi_real_ujipetik'        => $req->input('kpi_real_ujipetik'),
				'kpi_real_qc2'             => $req->input('kpi_real_qc2'),
				'p1_survey_hss'            => str_replace('.', '', $req->input('p1_survey_hss') ) ?? 0,
				'p1_survey_ssl'            => $req->input('p1_survey_ssl') ?? 0,
				'p2_tlpint_survey_hss'     => str_replace('.', '', $req->input('p2_tlpint_survey_hss') ) ?? 0,
				'p2_tlpint_survey_ssl'     => $req->input('p2_tlpint_survey_ssl') ?? 0,
				'p2_intiptv_survey_hss'    => str_replace('.', '', $req->input('p2_intiptv_survey_hss') ) ?? 0,
				'p2_intiptv_survey_ssl'    => $req->input('p2_intiptv_survey_ssl') ?? 0,
				'p3_survey_hss'            => str_replace('.', '', $req->input('p3_survey_hss') ) ?? 0,
				'p3_survey_ssl'            => $req->input('p3_survey_ssl') ?? 0,
				'p1_hss'                   => str_replace('.', '', $req->input('p1_hss') ) ?? 0,
				'p1_ssl'                   => $req->input('p1_ssl') ?? 0,
				'p2_tlpint_hss'            => str_replace('.', '', $req->input('p2_tlpint_hss') ) ?? 0,
				'p2_tlpint_ssl'            => $req->input('p2_tlpint_ssl') ?? 0,
				'p2_intiptv_hss'           => str_replace('.', '', $req->input('p2_intiptv_hss') ) ?? 0,
				'p2_intiptv_ssl'           => $req->input('p2_intiptv_ssl') ?? 0,
				'p3_hss'                   => str_replace('.', '', $req->input('p3_hss') ) ?? 0,
				'p3_ssl'                   => $req->input('p3_ssl') ?? 0,
				'migrasi_service_1p2p_hss' => str_replace('.', '', $req->input('migrasi_service_1p2p_hss') ) ?? 0,
				'migrasi_service_1p2p_ssl' => $req->input('migrasi_service_1p2p_ssl') ?? 0,
				'migrasi_service_1p3p_hss' => str_replace('.', '', $req->input('migrasi_service_1p3p_hss') ) ?? 0,
				'migrasi_service_1p3p_ssl' => $req->input('migrasi_service_1p3p_ssl') ?? 0,
				'migrasi_service_2p3p_hss' => str_replace('.', '', $req->input('migrasi_service_2p3p_hss') ) ?? 0,
				'migrasi_service_2p3p_ssl' => $req->input('migrasi_service_2p3p_ssl') ?? 0,
				'ikr_addon_stb_hss'        => str_replace('.', '', $req->input('ikr_addon_stb_hss') ) ?? 0,
				'ikr_addon_stb_ssl'        => $req->input('ikr_addon_stb_ssl') ?? 0,
				'indihome_smart_hss'       => str_replace('.', '', $req->input('indihome_smart_hss') ) ?? 0,
				'indihome_smart_ssl'       => $req->input('indihome_smart_ssl') ?? 0,
				'wifiextender_hss'         => str_replace('.', '', $req->input('wifiextender_hss') ) ?? 0,
				'wifiextender_ssl'         => $req->input('wifiextender_ssl') ?? 0,
				'plc_hss'                  => str_replace('.', '', $req->input('plc_hss') ) ?? 0,
				'plc_ssl'                  => $req->input('plc_ssl') ?? 0,
				'lme_wifi_pt1_hss'         => str_replace('.', '', $req->input('lme_wifi_pt1_hss') ) ?? 0,
				'lme_wifi_pt1_ssl'         => $req->input('lme_wifi_pt1_ssl') ?? 0,
				'lme_ap_indoor_hss'        => str_replace('.', '', $req->input('lme_ap_indoor_hss') ) ?? 0,
				'lme_ap_indoor_ssl'        => $req->input('lme_ap_indoor_ssl') ?? 0,
				'lme_ap_outdoor_hss'       => str_replace('.', '', $req->input('lme_ap_outdoor_hss') ) ?? 0,
				'lme_ap_outdoor_ssl'       => $req->input('lme_ap_outdoor_ssl') ?? 0,
				'ont_premium_hss'          => str_replace('.', '', $req->input('ont_premium_hss') ) ?? 0,
				'ont_premium_ssl'          => $req->input('ont_premium_ssl') ?? 0,
				'migrasi_stb_hss'          => str_replace('.', '', $req->input('migrasi_stb_hss') ) ?? 0,
				'migrasi_stb_ssl'          => $req->input('migrasi_stb_ssl') ?? 0,
				'instalasi_ipcam_hss'      => str_replace('.', '', $req->input('instalasi_ipcam_hss') ) ?? 0,
				'instalasi_ipcam_ssl'      => $req->input('instalasi_ipcam_ssl') ?? 0,
				'pu_s7_140_hss'            => str_replace('.', '', $req->input('pu_s7_140_hss') ) ?? 0,
				'pu_s7_140_ssl'            => $req->input('pu_s7_140_ssl') ?? 0,
				'pu_s9_140_hss'            => str_replace('.', '', $req->input('pu_s9_140_hss') ) ?? 0,
				'pu_s9_140_ssl'            => $req->input('pu_s9_140_ssl') ?? 0,
				'created_by'               => session('auth')->id_user,
				'modified_by'              => session('auth')->id_user,
				'verificator_by'           => session('auth')->id_user,
				'verificator_at'           => date('Y-m-d H:i:s')
			]);
		}

		$that = new \App\Http\Controllers\AdminController();
        $that->download_zip_psb('beta_ho_v2_manual', $id);

		self::save_log(12, $id);
	}

	public static function save_doc_generate($req)
	{
		$bulan = DB::table('bulan_format')->where('id', $req->input('periode_kegiatan'))->first();
		$mitra = AdminModel::get_mitra($req->input('id_mitra'));
		$hss = DB::table('procurement_hss_psb')->where('witel', $mitra->witel)->first();

		$jml_pekerjaan_psb =
			($req->input('p1_survey_ssl') * $hss->p1) +
			($req->input('p2_tlpint_survey_ssl') * $hss->p2_inet_voice) +
			($req->input('p2_intiptv_survey_ssl') * $hss->p2_inet_iptv) +
			($req->input('p3_survey_ssl') * $hss->p3) +
			($req->input('p1_ssl') * $hss->p1_ku) +
			($req->input('p2_tlpint_ssl') * $hss->p2_inet_voice_ku) +
			($req->input('p2_intiptv_ssl') * $hss->p2_inet_iptv_ku) +
			($req->input('p3_ssl') * $hss->p3_ku);

		$jml_pekerjaan_migrasi =
			($req->input('migrasi_service_1p2p_ssl') * $hss->migrasi_1p) +
			($req->input('migrasi_service_1p3p_ssl') * $hss->migrasi_2p) +
			($req->input('migrasi_service_2p3p_ssl') * $hss->migrasi_3p);

		$jml_pekerjaan_tambahan =
			($req->input('ikr_addon_stb_ssl') * $hss->stb_tambahan) +
			($req->input('indihome_smart_ssl') * $hss->smartcam_only) +
			($req->input('wifiextender_ssl') * $hss->wifiext) +
			($req->input('plc_ssl') * $hss->plc) +
			($req->input('lme_wifi_pt1_ssl') * $hss->lme_wifi) +
			($req->input('lme_ap_indoor_ssl') * $hss->lme_ap_indoor) +
			($req->input('lme_ap_outdoor_ssl') * $hss->lme_ap_outdoor) +
			($req->input('ont_premium_ssl') * $hss->ont_premium) +
			($req->input('migrasi_stb_ssl') * $hss->migrasi_stb) +
			($req->input('instalasi_ipcam_ssl') * $hss->instalasi_ipcam) +
			($req->input('pu_s7_140_ssl') * $hss->insert_tiang) +
			($req->input('pu_s9_140_ssl') * $hss->insert_tiang_9);

		// $total_sp = substr_replace(floor($jml_pekerjaan_psb + $jml_pekerjaan_tambahan), '000', -3, 3);
		$total_sp = ($jml_pekerjaan_psb + $jml_pekerjaan_migrasi + $jml_pekerjaan_tambahan);

		if (in_array(date('m'), [1, 2, 3]))
		{
			// Q1
			$kpi_target_pspi = 96;
			$kpi_target_tticomply = 91;
		} elseif (in_array(date('m'), [4, 5, 6])) {
			// Q2
			$kpi_target_pspi = 97;
			$kpi_target_tticomply = 95;
		} elseif (in_array(date('m'), [7, 8, 9, 10, 11, 12])) {
			// Q3 Q4
			$kpi_target_pspi = 98;
			$kpi_target_tticomply = 99;
		};

		$kpi_pspi = @round( (str_replace(',', '.', $req->input('kpi_real_pspi')) / $kpi_target_pspi) * 100, 2);
		$kpi_tticomply = @round( (str_replace(',', '.', $req->input('kpi_real_tticomply')) / $kpi_target_tticomply) * 100, 2);
		$kpi_ffg = @round( (str_replace(',', '.', $req->input('kpi_real_ffg')) / 99) * 100, 2);
		$kpi_validasicore = @round( (str_replace(',', '.', $req->input('kpi_real_validasicore')) / 100) * 100, 2);
		$kpi_ttrffg = @round( (str_replace(',', '.', $req->input('kpi_real_ttrffg')) / 99) * 100, 2);
		$kpi_ujipetik = @round( (str_replace(',', '.', $req->input('kpi_real_ujipetik')) / 99) * 100, 2);
		$kpi_qc2 = @round( (str_replace(',', '.', $req->input('kpi_real_qc2')) / 99) * 100, 2);

		$ach_kpi_pspi = @round( ($kpi_pspi * 25) / 100, 2);
		$ach_kpi_tticomply = @round( ($kpi_tticomply * 25) / 100, 2);
		$ach_kpi_ffg = @round( ($kpi_ffg * 15) / 100, 2);
		$ach_kpi_validasicore = @round( ($kpi_validasicore * 5) / 100, 2);
		$ach_kpi_ttrffg = @round( ($kpi_ttrffg * 10) / 100, 2);
		$ach_kpi_ujipetik = @round( ($kpi_ujipetik * 10) / 100, 2);
		$ach_kpi_qc2 = @round( ($kpi_qc2 * 10) / 100, 2);

		$ach_kpi = ($ach_kpi_pspi + $ach_kpi_tticomply + $ach_kpi_ffg + $ach_kpi_validasicore + $ach_kpi_ttrffg + $ach_kpi_ujipetik + $ach_kpi_qc2);

		if ($ach_kpi > 100)
		{
			$kpi = 100;
		} elseif ($ach_kpi < 90) {
			$kpi = 90;
		} else {
			$kpi = $ach_kpi;
		};

		$id = DB::table('procurement_boq_upload')->insertGetId([
			'judul'              => "PEKERJAAN PASANG SAMBUNGAN BARU (PSB) PERIODE ".$bulan->bulan. " ".date('Y'). " WITEL ".strtoupper($mitra->witel)."",
			'pekerjaan'          => "PSB",
			'jenis_work'         => "Pasang Sambungan Baru (PSB)",
			'witel'              => strtoupper($mitra->witel),
			'step_id'            => 12,
			'mitra_id'           => $req->input('id_mitra'),
			'mitra_nm'           => $mitra->nama_company,
			'tanggal_boq'        => $req->input('tanggal_terbit'),
			'toc'                => $req->input('toc'),
			'tgl_sp'             => $req->input('tgl_sp'),
			'no_sp'              => $req->input('no_sp'),
			'total_sp'           => $total_sp,
			'id_project'         => $req->input('id_project'),
			'pks'                => $req->input('pks'),
			'tgl_pks'            => $req->input('tgl_pks'),
			'masa_berlaku_pks'   => $req->input('masa_berlaku_pks'),
			'invoice'            => $req->input('invoice'),
			'spp_num'            => $req->input('spp_num'),
			'receipt_num'        => $req->input('receipt_num'),
			'faktur'             => $req->input('faktur'),
			'tgl_faktur'         => $req->input('tgl_faktur'),
			'surat_penetapan'    => $req->input('surat_penetapan'),
			'tgl_s_pen'          => $req->input('tgl_s_pen'),
			'surat_kesanggupan'  => $req->input('surat_kesanggupan'),
			'tgl_surat_sanggup'  => $req->input('tgl_surat_sanggup'),
			'no_baij'            => $req->input('no_baij'),
			'no_ba_pemeriksaan'  => $req->input('no_ba_pemeriksaan'),
			'BAUT'               => $req->input('BAUT'),
			'tgl_baut'           => $req->input('rekon_periode2'),
			'BAST'               => $req->input('BAST'),
			'tgl_bast'           => $req->input('rekon_periode2'),
			'no_bap'             => $req->input('no_bap'),
			'BAPP'               => $req->input('BAPP'),
			'no_ba_pemeliharaan' => $req->input('no_ba_pemeliharaan'),
			'created_by'         => session('auth')->id_user,
			'modified_by'        => session('auth')->id_user,
			'verificator_by'     => session('auth')->id_user,
			'verificator_at'     => date('Y-m-d H:i:s')
		]);

		$desc = ['SP', 'BARIJ'];
		foreach ($desc as $value)
		{
			DB::table('procurement_nilai_psb')->insert([
				'id_dokumen'               => $id,
				'flag_dok_psb'             => $req->input('flag_dok_psb'),
				'desc'                     => $value,
				'periode_kegiatan'         => $req->input('periode_kegiatan'),
				'rekon_periode1'           => $req->input('rekon_periode1'),
				'rekon_periode2'           => $req->input('rekon_periode2'),
				'order_periode1'           => $req->input('order_periode1'),
				'order_periode2'           => $req->input('order_periode2'),
				'ppn_numb'                 => $req->input('ppn_numb'),
				'total_nilai_sp'           => $total_sp,
				'persentase_7_kpi'         => $kpi,
				'kpi_real_pspi'            => $req->input('kpi_real_pspi'),
				'kpi_real_tticomply'       => $req->input('kpi_real_tticomply'),
				'kpi_real_ffg'             => $req->input('kpi_real_ffg'),
				'kpi_real_validasicore'    => $req->input('kpi_real_validasicore'),
				'kpi_real_ttrffg'          => $req->input('kpi_real_ttrffg'),
				'kpi_real_ujipetik'        => $req->input('kpi_real_ujipetik'),
				'kpi_real_qc2'             => $req->input('kpi_real_qc2'),
				'p1_survey_hss'            => $hss->p1,
				'p1_survey_ssl'            => $req->input('p1_survey_ssl'),
				'p2_tlpint_survey_hss'     => $hss->p2_inet_voice,
				'p2_tlpint_survey_ssl'     => $req->input('p2_tlpint_survey_ssl'),
				'p2_intiptv_survey_hss'    => $hss->p2_inet_iptv,
				'p2_intiptv_survey_ssl'    => $req->input('p2_intiptv_survey_ssl'),
				'p3_survey_hss'            => $hss->p3,
				'p3_survey_ssl'            => $req->input('p3_survey_ssl'),
				'p1_hss'                   => $hss->p1_ku,
				'p1_ssl'                   => $req->input('p1_ssl'),
				'p2_tlpint_hss'            => $hss->p2_inet_voice_ku,
				'p2_tlpint_ssl'            => $req->input('p2_tlpint_ssl'),
				'p2_intiptv_hss'           => $hss->p2_inet_iptv_ku,
				'p2_intiptv_ssl'           => $req->input('p2_intiptv_ssl'),
				'p3_hss'                   => $hss->p3_ku,
				'p3_ssl'                   => $req->input('p3_ssl'),
				'migrasi_service_1p2p_hss' => $hss->migrasi_1p,
				'migrasi_service_1p2p_ssl' => $req->input('migrasi_service_1p2p_ssl'),
				'migrasi_service_1p3p_hss' => $hss->migrasi_2p,
				'migrasi_service_1p3p_ssl' => $req->input('migrasi_service_1p3p_ssl'),
				'migrasi_service_2p3p_hss' => $hss->migrasi_3p,
				'migrasi_service_2p3p_ssl' => $req->input('migrasi_service_2p3p_ssl'),
				'ikr_addon_stb_hss'        => $hss->stb_tambahan,
				'ikr_addon_stb_ssl'        => $req->input('ikr_addon_stb_ssl'),
				'indihome_smart_hss'       => $hss->smartcam_only,
				'indihome_smart_ssl'       => $req->input('indihome_smart_ssl'),
				'wifiextender_hss'         => $hss->wifiext,
				'wifiextender_ssl'         => $req->input('wifiextender_ssl'),
				'plc_hss'                  => $hss->plc,
				'plc_ssl'                  => $req->input('plc_ssl'),
				'lme_wifi_pt1_hss'         => $hss->lme_wifi,
				'lme_wifi_pt1_ssl'         => $req->input('lme_wifi_pt1_ssl'),
				'lme_ap_indoor_hss'        => $hss->lme_ap_indoor,
				'lme_ap_indoor_ssl'        => $req->input('lme_ap_indoor_ssl'),
				'lme_ap_outdoor_hss'       => $hss->lme_ap_outdoor,
				'lme_ap_outdoor_ssl'       => $req->input('lme_ap_outdoor_ssl'),
				'ont_premium_hss'          => $hss->ont_premium,
				'ont_premium_ssl'          => $req->input('ont_premium_ssl'),
				'migrasi_stb_hss'          => $hss->migrasi_stb,
				'migrasi_stb_ssl'          => $req->input('migrasi_stb_ssl'),
				'instalasi_ipcam_hss'      => $hss->instalasi_ipcam,
				'instalasi_ipcam_ssl'      => $req->input('instalasi_ipcam_ssl'),
				'pu_s7_140_hss'            => $hss->insert_tiang,
				'pu_s7_140_ssl'            => $req->input('pu_s7_140_ssl'),
				'pu_s9_140_hss'            => $hss->insert_tiang_9,
				'pu_s9_140_ssl'            => $req->input('pu_s9_140_ssl'),
				'created_by'               => session('auth')->id_user,
				'modified_by'              => session('auth')->id_user,
				'verificator_by'           => session('auth')->id_user,
				'verificator_at'           => date('Y-m-d H:i:s')
			]);
		}

		DB::statement('UPDATE procurement_order_psb SET is_rekon = 1, dtm_is_rekon = "'.date('Y-m-d H:i:s').'" WHERE id_mitra = '.$mitra->tacticalpro_mitra_id.' AND (DATE(last_updated_date) BETWEEN "'.$req->input('order_periode1').'" AND "'.$req->input('order_periode2').'")');

		// self::save_order_addon($req);

		$that = new \App\Http\Controllers\AdminController();
        $that->download_zip_psb('beta_ho_v1', $id);

		self::save_log(12, $id);
	}
}