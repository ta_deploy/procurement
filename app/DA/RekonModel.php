<?php

namespace App\DA;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use ZipArchive;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use App\DA\AdminModel;

date_default_timezone_set("Asia/Makassar");

class RekonModel {

    public static function dashboardRekon($startDate, $endDate)
    {
        $mitra = DB::select('SELECT mitra_amija_pt as mitra FROM mitra_amija WHERE mitra_status = "ACTIVE" AND witel = "KALSEL" GROUP BY mitra_amija_pt');

        $query1 = DB::select('
            SELECT
                ma.mitra_amija_pt as mitra,
                SUM(CASE WHEN pl.status_laporan != 1 OR pl.status_laporan IS NULL THEN 1 ELSE 0 END) as jumlah_dps_nok,
                SUM(CASE WHEN pl.status_laporan = "1" THEN 1 ELSE 0 END) as jumlah_dps_ok
            FROM Data_Pelanggan_Starclick dps
            LEFT JOIN dispatch_teknisi dt ON dps.orderIdInteger = dt.NO_ORDER
            LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
            LEFT JOIN regu r ON dt.id_regu = r.id_regu
            LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
            WHERE
                dt.jenis_order IN ("SC","MYIR") AND
                dt.jenis_layanan NOT IN ("CABUT_NTE", "QC") AND
                (DATE(dps.orderDate) BETWEEN "'.$startDate.'" AND "'.$endDate.'")
            GROUP BY ma.mitra_amija_pt
        ');

        $query2 = DB::select('
            SELECT
                ma.mitra_amija_pt as mitra,
                SUM(CASE WHEN pl.status_laporan != 1 OR pl.status_laporan IS NULL THEN 1 ELSE 0 END) as jumlah_kpt_nok,
                SUM(CASE WHEN pl.status_laporan = "1" THEN 1 ELSE 0 END) as jumlah_kpt_ok,
                SUM(CASE WHEN kpt.PRODUCT IN ("WMS", "WMS LITE") THEN 1 ELSE 0 END) as jumlah_kpt_wms
            FROM kpro_tr6 kpt
            LEFT JOIN dispatch_teknisi dt ON kpt.ORDER_ID = dt.NO_ORDER
            LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
            LEFT JOIN regu r ON dt.id_regu = r.id_regu
            LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
            WHERE
                (dt.jenis_order IN ("SC", "MYIR") OR dt.jenis_order IS NULL) AND
                (dt.jenis_layanan NOT IN ("CABUT_NTE", "QC") OR dt.jenis_layanan IS NULL) AND
                (DATE(kpt.LAST_UPDATED_DATE) BETWEEN "' . $startDate . '" AND "' . $endDate . '") AND
                (kpt.STATUS_RESUME = "Completed (PS)" OR kpt.STATUS_MESSAGE = "Completed") AND
                kpt.WITEL = "BANJARMASIN"
            GROUP BY ma.mitra_amija_pt
        ');

        $query3 = DB::select('
            SELECT
                ma.mitra_amija_pt as mitra,
                SUM(CASE WHEN utt.scId IS NULL AND kpt.PROVIDER LIKE "DCS%" AND kpt.JENIS_PSB = "AO" THEN 1 ELSE 0 END) as nok_ut
            FROM kpro_tr6 kpt
            LEFT JOIN dispatch_teknisi dt ON kpt.ORDER_ID = dt.NO_ORDER
            LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
            LEFT JOIN regu r ON dt.id_regu = r.id_regu
            LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
            LEFT JOIN utonline_tr6 utt ON kpt.SPEEDY = utt.noInternet AND utt.witel IN ("KALSEL (BANJARMASIN)", "BANJARMASIN")
            WHERE
                (dt.jenis_order IN ("SC", "MYIR") OR dt.jenis_order IS NULL) AND
                (dt.jenis_layanan NOT IN ("CABUT_NTE", "QC") OR dt.jenis_layanan IS NULL) AND
                (DATE(kpt.LAST_UPDATED_DATE) BETWEEN "' . $startDate . '" AND "' . $endDate . '") AND
                (kpt.STATUS_RESUME = "Completed (PS)" OR kpt.STATUS_MESSAGE = "Completed") AND
                kpt.PRODUCT = "INDIHOME" AND
                kpt.WITEL = "BANJARMASIN"
            GROUP BY ma.mitra_amija_pt
        ');

        $query4 = DB::select('
            SELECT
                ma.mitra_amija_pt as mitra,
                SUM(CASE WHEN mttr.id_item = "PU-S7.0-140" THEN mttr.qty ELSE 0 END) as jml_tiang
            FROM maintaince mt
            LEFT JOIN mitra_amija ma ON mt.nama_mitra = ma.mitra_marina AND ma.kat = "DELTA" AND ma.witel = "KALSEL"
            LEFT JOIN maintaince_mtr mttr ON mt.id = mttr.maintaince_id
            WHERE
                mt.status IN ("close", "sudah tanam") AND
                mt.nama_order = "INSERT TIANG" AND
                (DATE(mt.tgl_selesai) BETWEEN "'.$startDate.'" AND "'.$endDate.'")
            GROUP BY ma.mitra_amija_pt
        ');

        $query = [];

        foreach($mitra as $val)
        {
            foreach($query1 as $val_c1)
            {
                if (!empty($val_c1->mitra))
                {
                    if ($val_c1->mitra == $val->mitra)
                    {
                        $query[$val->mitra]['jumlah_dps_nok'] = $val_c1->jumlah_dps_nok;
                        $query[$val->mitra]['jumlah_dps_ok'] = $val_c1->jumlah_dps_ok;
                    }
                } else {
                    $query['NONE']['jumlah_dps_nok'] = $val_c1->jumlah_dps_nok;
                    $query['NONE']['jumlah_dps_ok'] = $val_c1->jumlah_dps_ok;
                }
            }
            foreach($query2 as $val_c2)
            {
                if (!empty($val_c2->mitra))
                {
                    if ($val_c2->mitra == $val->mitra)
                    {
                        $query[$val->mitra]['jumlah_kpt_nok'] = $val_c2->jumlah_kpt_nok;
                        $query[$val->mitra]['jumlah_kpt_ok'] = $val_c2->jumlah_kpt_ok;
                        $query[$val->mitra]['jumlah_kpt_wms'] = $val_c2->jumlah_kpt_wms;
                    }
                } else {
                    $query['NONE']['jumlah_kpt_nok'] = $val_c2->jumlah_kpt_nok;
                    $query['NONE']['jumlah_kpt_ok'] = $val_c2->jumlah_kpt_ok;
                    $query['NONE']['jumlah_kpt_wms'] = $val_c2->jumlah_kpt_wms;
                }
            }
            foreach ($query3 as $val_c3)
            {
                if (!empty($val_c3->mitra))
                {
                    if ($val_c3->mitra == $val->mitra)
                    {
                        $query[$val->mitra]['nok_ut'] = $val_c3->nok_ut;
                    }
                } else {
                    $query['NONE']['nok_ut'] = $val_c3->nok_ut;
                }
            }
            foreach ($query4 as $val_c4)
            {
                if (!empty($val_c4->mitra))
                {
                    if ($val_c4->mitra == $val->mitra)
                    {
                        $query[$val->mitra]['jml_tiang'] = $val_c4->jml_tiang;
                    }
                } else {
                    $query['NONE']['jml_tiang'] = $val_c4->jml_tiang;
                }
            }
        }

        return $query;
    }

    public static function dashboardRekonProvisioning($source, $mitra, $status, $startDate, $endDate)
    {
        switch ($mitra) {
            case 'NONE':
                $where_mitra = 'AND ma.mitra_amija_pt IS NULL';
                break;
            case 'ALL':
                $where_mitra = '';
                break;
            default:
                $where_mitra = 'AND ma.mitra_amija_pt = "' . $mitra . '"';
                break;
        }

        switch ($status) {
            case 'OK':
                $where_status = 'AND pl.status_laporan = "1"';
                break;
            case 'NOK':
                $where_status = 'AND (pl.status_laporan != 1 OR pl.status_laporan IS NULL)';
                break;
            case 'UTONLINENOK':
                $where_status = 'AND utt.scId IS NULL AND kpt.PROVIDER LIKE "DCS%" AND kpt.JENIS_PSB = "AO"';
                break;
            case 'WMS':
                $where_status = 'AND kpt.PRODUCT IN ("WMS", "WMS LITE")';
                break;
            case 'ALL':
                $where_status = '';
                break;
        }

        switch ($source) {
            case 'starclick':
                return DB::select('
                    SELECT
                        gt.SM,
                        gt.TL,
                        ma.mitra_amija_pt,
                        r.uraian as tim,
                        dps.orderId,
                        dps.myir,
                        kpt.DEVICE_ID as kpt_myir,
                        dt.tgl as tgl_dispatch,
                        pl.modified_at,
                        dps.orderDate,
                        dps.orderDatePs,
                        kpt.NPER as kpt_nper,
                        dps.orderAddr,
                        dps.sto,
                        dps.internet,
                        dps.noTelp,
                        dps.orderName,
                        dps.orderStatus,
                        dps.agent_id,
                        dps.jenisPsb,
                        dps.provider as dps_provider,
                        kpt.PROVIDER as kpt_provider,
                        kpt.GROUP_CHANNEL as kpt_group_channel,
                        kpt.PRODUCT as kpt_product,
                        kpt.FLAG_DEPOSIT as kpt_flag_deposit,
                        dtjl.jenis_layanan as dtjl_jenis_layanan,
                        kpt.TYPE_TRANSAKSI,
                        kpt.TYPE_LAYANAN,
                        pl.rfc_number,
                        pl.typeont,
                        pl.snont,
                        pl.typestb,
                        pl.snstb,
                        dtjl.mode as dtjl_mode,
                        SUM(CASE WHEN plm.id_item = "AD-SC" THEN plm.qty ELSE 0 END) as adsc,
                        SUM(CASE WHEN plm.id_item IN ("RS-IN-SC-1","RS-IN-SC-11") THEN plm.qty ELSE 0 END) as roset,
                        SUM(CASE WHEN plm.id_item IN ("AC-OF-SM-1B","DC-ROLL") THEN plm.qty ELSE 0 END) as dc_roll,
                        SUM(CASE WHEN plm.id_item IN ("Preconnectorized-1C-50-NonAcc","preconnectorized 50 M") THEN plm.qty ELSE 0 END) as precon50,
                        SUM(CASE WHEN plm.id_item = "Preconnectorized-1C-75-NonAcc" THEN plm.qty ELSE 0 END) as precon75,
                        SUM(CASE WHEN plm.id_item IN ("Preconnectorized-1C-80-NonAcc","Preconnectorized-1C-80") THEN plm.qty ELSE 0 END) as precon80,
                        SUM(CASE WHEN plm.id_item = "Preconnectorized-1C-100-NonAcc" THEN plm.qty ELSE 0 END) as precon100,
                        SUM(CASE WHEN plm.id_item = "Preconnectorized-1C-150-NonAcc" THEN plm.qty ELSE 0 END) as precon150,
                        SUM(CASE WHEN plm.id_item = "PC-SC-SC-5" THEN plm.qty ELSE 0 END) as patchcore_5,
                        SUM(CASE WHEN plm.id_item = "PC-SC-SC-10" THEN plm.qty ELSE 0 END) as patchcore_10,
                        SUM(CASE WHEN plm.id_item = "PC-SC-SC-15" THEN plm.qty ELSE 0 END) as patchcore_15,
                        SUM(CASE WHEN plm.id_item = "PC-SC-SC-20" THEN plm.qty ELSE 0 END) as patchcore_20,
                        SUM(CASE WHEN plm.id_item = "PC-SC-SC-30" THEN plm.qty ELSE 0 END) as patchcore_30,
                        SUM(CASE WHEN plm.id_item IN ("UTP-CAT5", "UTP-CAT6","UTP-C5", "UTP-C6") THEN plm.qty ELSE 0 END) as utp,
                        SUM(CASE WHEN plm.id_item IN ("PU-S7.0-140","PU-S9.0-140") THEN plm.qty ELSE 0 END) as tiang,
                        pl.jml_tiang,
                        SUM(CASE WHEN plm.id_item = "TC-OF-CR-200" THEN plm.qty ELSE 0 END) as tray_cable,
                        SUM(CASE WHEN plm.id_item IN ("BREKET","BREKET-A") THEN plm.qty ELSE 0 END) as breket,
                        SUM(CASE WHEN plm.id_item IN ("S-Clamp-Spriner","S-Clamp-Sprinerr") THEN plm.qty ELSE 0 END) as clamps,
                        SUM(CASE WHEN plm.id_item = "PS-TIANG" THEN plm.qty ELSE 0 END) as pulstrap,
                        SUM(CASE WHEN plm.id_item = "RJ45-5" THEN plm.qty ELSE 0 END) as rj45,
                        SUM(CASE WHEN plm.id_item = "SOC-ILS" THEN plm.qty ELSE 0 END) as soc_ils,
                        SUM(CASE WHEN plm.id_item = "SOC-SUM" THEN plm.qty ELSE 0 END) as soc_sum,
                        SUM(CASE WHEN plm.id_item = "PREKSO-INTRA-15-RS" THEN plm.qty ELSE 0 END) as prekso_15,
                        SUM(CASE WHEN plm.id_item = "PREKSO-INTRA-20-RS" THEN plm.qty ELSE 0 END) as prekso_20,
                        SUBSTRING_INDEX(pl.kordinat_pelanggan, ",", 1) AS lat_pelanggan,
                        SUBSTRING_INDEX(SUBSTRING_INDEX(pl.kordinat_pelanggan, ",", -1), ",", 1) AS long_pelanggan,
                        kpt.LOC_ID as loc_id,
                        pl.nama_odp,
                        SUBSTRING_INDEX(pl.kordinat_odp, ",", 1) AS lat_odp,
                        SUBSTRING_INDEX(SUBSTRING_INDEX(pl.kordinat_odp, ",", -1), ",", 1) AS long_odp,
                        m.datel,
                        pls.laporan_status,
                        pl.catatan,
                        utt.statusName as utt_statusName,
                        utt.qcStatusName as utt_qcStatusName,
                        utt.create_dtm as utt_create_dtm,
                        pcp.periode1,
                        pcp.periode2,
                        bskt.track_id_int
                    FROM Data_Pelanggan_Starclick dps
                    LEFT JOIN kpro_tr6 kpt ON dps.orderIdInteger = kpt.ORDER_ID AND kpt.WITEL = "BANJARMASIN"
                    LEFT JOIN utonline_tr6 utt ON dps.internet = utt.noInternet AND utt.witel IN ("KALSEL (BANJARMASIN)", "BANJARMASIN")
                    LEFT JOIN dispatch_teknisi dt ON dps.orderIdInteger = dt.NO_ORDER
                    LEFT JOIN dispatch_teknisi_jenis_layanan dtjl ON dt.jenis_layanan = dtjl.jenis_layanan
                    LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
                    LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
                    LEFT JOIN psb_laporan_material plm ON pl.id = plm.psb_laporan_id
                    LEFT JOIN rfc_sal i on plm.id_item_bantu = i.id_item_bantu
                    LEFT JOIN regu r ON dt.id_regu = r.id_regu
                    LEFT JOIN maintenance_datel m ON dps.sto = m.sto
                    LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
                    LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
                    LEFT JOIN procurement_cutoff_psb pcp ON dps.orderIdInteger = pcp.sc_id
                    LEFT JOIN byod_survey_kpro_tr6 bskt ON dps.orderIdInteger = bskt.track_id_int
                    WHERE
                        dt.jenis_order IN ("SC","MYIR") AND
                        dt.jenis_layanan NOT IN ("CABUT_NTE", "QC") AND
                        DATE(dps.orderDate) BETWEEN "' . $startDate . '" AND "' . $endDate . '"
                        ' . $where_status . '
                        ' . $where_mitra . '
                    GROUP BY dt.id
                ');
                break;
            case 'kpro':
                return DB::select('
                    SELECT
                        gt.SM,
                        gt.TL,
                        ma.mitra_amija_pt,
                        r.uraian as tim,
                        kpt.ORDER_ID as kpt_orderId,
                        kpt.DEVICE_ID as kpt_myir,
                        dt.tgl as tgl_dispatch,
                        pl.modified_at,
                        kpt.ORDER_DATE as orderDate,
                        kpt.LAST_UPDATED_DATE as orderDatePs,
                        kpt.NPER as kpt_nper,
                        kpt.STO as kpt_sto,
                        dps.orderAddr,
                        kpt.SPEEDY as kpt_internet,
                        kpt.POTS as kpt_noTelp,
                        kpt.CUSTOMER_NAME as kpt_orderName,
                        kpt.STATUS_RESUME as kpt_orderStatus,
                        kpt.AGENT_ID as kpt_agent_id,
                        kpt.JENIS_PSB as kpt_jenisPsb,
                        kpt.PROVIDER as kpt_provider,
                        kpt.GROUP_CHANNEL as kpt_group_channel,
                        kpt.PRODUCT as kpt_product,
                        kpt.FLAG_DEPOSIT as kpt_flag_deposit,
                        dtjl.jenis_layanan as dtjl_jenis_layanan,
                        kpt.TYPE_TRANSAKSI,
                        kpt.TYPE_LAYANAN,
                        pl.rfc_number,
                        pl.typeont,
                        pl.snont,
                        pl.typestb,
                        pl.snstb,
                        dtjl.mode as dtjl_mode,
                        SUM(CASE WHEN plm.id_item = "AD-SC" THEN plm.qty ELSE 0 END) as adsc,
                        SUM(CASE WHEN plm.id_item IN ("RS-IN-SC-1","RS-IN-SC-11") THEN plm.qty ELSE 0 END) as roset,
                        SUM(CASE WHEN plm.id_item IN ("AC-OF-SM-1B","DC-ROLL") THEN plm.qty ELSE 0 END) as dc_roll,
                        SUM(CASE WHEN plm.id_item IN ("Preconnectorized-1C-50-NonAcc","preconnectorized 50 M") THEN plm.qty ELSE 0 END) as precon50,
                        SUM(CASE WHEN plm.id_item = "Preconnectorized-1C-75-NonAcc" THEN plm.qty ELSE 0 END) as precon75,
                        SUM(CASE WHEN plm.id_item IN ("Preconnectorized-1C-80-NonAcc","Preconnectorized-1C-80") THEN plm.qty ELSE 0 END) as precon80,
                        SUM(CASE WHEN plm.id_item = "Preconnectorized-1C-100-NonAcc" THEN plm.qty ELSE 0 END) as precon100,
                        SUM(CASE WHEN plm.id_item = "Preconnectorized-1C-150-NonAcc" THEN plm.qty ELSE 0 END) as precon150,
                        SUM(CASE WHEN plm.id_item = "PC-SC-SC-5" THEN plm.qty ELSE 0 END) as patchcore_5,
                        SUM(CASE WHEN plm.id_item = "PC-SC-SC-10" THEN plm.qty ELSE 0 END) as patchcore_10,
                        SUM(CASE WHEN plm.id_item = "PC-SC-SC-15" THEN plm.qty ELSE 0 END) as patchcore_15,
                        SUM(CASE WHEN plm.id_item = "PC-SC-SC-20" THEN plm.qty ELSE 0 END) as patchcore_20,
                        SUM(CASE WHEN plm.id_item = "PC-SC-SC-30" THEN plm.qty ELSE 0 END) as patchcore_30,
                        SUM(CASE WHEN plm.id_item IN ("UTP-CAT5", "UTP-CAT6","UTP-C5", "UTP-C6") THEN plm.qty ELSE 0 END) as utp,
                        SUM(CASE WHEN plm.id_item IN ("PU-S7.0-140","PU-S9.0-140") THEN plm.qty ELSE 0 END) as tiang,
                        pl.jml_tiang,
                        SUM(CASE WHEN plm.id_item = "TC-OF-CR-200" THEN plm.qty ELSE 0 END) as tray_cable,
                        SUM(CASE WHEN plm.id_item IN ("BREKET","BREKET-A") THEN plm.qty ELSE 0 END) as breket,
                        SUM(CASE WHEN plm.id_item IN ("S-Clamp-Spriner","S-Clamp-Sprinerr") THEN plm.qty ELSE 0 END) as clamps,
                        SUM(CASE WHEN plm.id_item = "PS-TIANG" THEN plm.qty ELSE 0 END) as pulstrap,
                        SUM(CASE WHEN plm.id_item = "RJ45-5" THEN plm.qty ELSE 0 END) as rj45,
                        SUM(CASE WHEN plm.id_item = "SOC-ILS" THEN plm.qty ELSE 0 END) as soc_ils,
                        SUM(CASE WHEN plm.id_item = "SOC-SUM" THEN plm.qty ELSE 0 END) as soc_sum,
                        SUM(CASE WHEN plm.id_item = "PREKSO-INTRA-15-RS" THEN plm.qty ELSE 0 END) as prekso_15,
                        SUM(CASE WHEN plm.id_item = "PREKSO-INTRA-20-RS" THEN plm.qty ELSE 0 END) as prekso_20,
                        SUBSTRING_INDEX(pl.kordinat_pelanggan, ",", 1) AS lat_pelanggan,
                        SUBSTRING_INDEX(SUBSTRING_INDEX(pl.kordinat_pelanggan, ",", -1), ",", 1) AS long_pelanggan,
                        kpt.LOC_ID as loc_id,
                        pl.nama_odp,
                        SUBSTRING_INDEX(pl.kordinat_odp, ",", 1) AS lat_odp,
                        SUBSTRING_INDEX(SUBSTRING_INDEX(pl.kordinat_odp, ",", -1), ",", 1) AS long_odp,
                        m.datel,
                        pls.laporan_status,
                        pl.catatan,
                        utt.statusName as utt_statusName,
                        utt.qcStatusName as utt_qcStatusName,
                        utt.create_dtm as utt_create_dtm,
                        pcp.periode1,
                        pcp.periode2,
                        bskt.track_id_int
                    FROM kpro_tr6 kpt
                    LEFT JOIN Data_Pelanggan_Starclick dps ON kpt.ORDER_ID = dps.orderIdInteger
                    LEFT JOIN utonline_tr6 utt ON kpt.SPEEDY = utt.noInternet AND utt.witel IN ("KALSEL (BANJARMASIN)", "BANJARMASIN")
                    LEFT JOIN dispatch_teknisi dt ON kpt.ORDER_ID = dt.NO_ORDER
                    LEFT JOIN dispatch_teknisi_jenis_layanan dtjl ON dt.jenis_layanan = dtjl.jenis_layanan
                    LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
                    LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
                    LEFT JOIN psb_laporan_material plm ON pl.id = plm.psb_laporan_id
                    LEFT JOIN rfc_sal i on plm.id_item_bantu = i.id_item_bantu
                    LEFT JOIN regu r ON dt.id_regu = r.id_regu
                    LEFT JOIN maintenance_datel m ON kpt.STO = m.sto
                    LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
                    LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
                    LEFT JOIN procurement_cutoff_psb pcp ON kpt.ORDER_ID = pcp.sc_id
                    LEFT JOIN byod_survey_kpro_tr6 bskt ON kpt.ORDER_ID = bskt.track_id_int
                    WHERE
                        (dt.jenis_order IN ("SC", "MYIR") OR dt.jenis_order IS NULL) AND
                        (dt.jenis_layanan NOT IN ("CABUT_NTE", "QC") OR dt.jenis_layanan IS NULL) AND
                        (DATE(kpt.LAST_UPDATED_DATE) BETWEEN "' . $startDate . '" AND "' . $endDate . '") AND
                        (kpt.STATUS_RESUME = "Completed (PS)" OR kpt.STATUS_MESSAGE = "Completed") AND
                        kpt.WITEL = "BANJARMASIN"
                        ' . $where_status . '
                        ' . $where_mitra . '
                    GROUP BY kpt.ID
                ');
                break;
        }
    }

    public static function dashboardRekonTiang($mitra, $startDate, $endDate)
    {
        switch ($mitra) {
            case 'NONE':
                $where_mitra = 'AND ma.mitra_amija_pt IS NULL';
                break;
            case 'ALL':
                $where_mitra = '';
                break;
            default:
                $where_mitra = 'AND ma.mitra_amija_pt = "' . $mitra . '"';
                break;
        }

        return DB::select('
            SELECT
                mt.verify,
                mt.order_from,
                mt.no_tiket,
                dps.orderId,
                SUM(CASE WHEN mttr.id_item = "PU-S7.0-140" THEN mttr.qty ELSE 0 END) as jml_tiang,
                dps.orderName as nama_pelanggan,
                pmw.customer as nama_pelanggan2,
                dps.internet as inet,
                pmw.no_internet as inet2,
                dps.orderStatus,
                kpt.STATUS_RESUME,
                pmw.orderDate as pmw_orderDate,
                dps.orderDate,
                dps.orderDatePs,
                kpt.ORDER_DATE,
                kpt.LAST_UPDATED_DATE,
                dps.orderAddr as alamat_pelanggan,
                SUBSTRING_INDEX(dps.jenisPsb, "|", 1) as transaksi,
                SUBSTRING_INDEX(SUBSTRING_INDEX(dps.jenisPsb, "|", -1), ",", 1) AS layanan,
                mt.dispatch_regu_name as tim,
                ma.mitra_amija_pt as mitra
            FROM maintaince mt
            LEFT JOIN maintaince_mtr mttr ON mt.id = mttr.maintaince_id
            LEFT JOIN mitra_amija ma ON mt.nama_mitra = ma.mitra_marina AND (ma.kat = "DELTA" AND ma.witel = "KALSEL")
            LEFT JOIN psb_myir_wo pmw ON (mt.no_tiket = pmw.myir OR mt.no_tiket = pmw.sc)
            LEFT JOIN Data_Pelanggan_Starclick dps ON (pmw.sc = dps.orderId OR mt.no_tiket = dps.orderId)
            LEFT JOIN kpro_tr6 kpt ON dps.orderIdInteger = kpt.ORDER_ID
            WHERE
                mt.status IN ("close", "sudah tanam") AND
                mt.nama_order = "INSERT TIANG" AND
                (DATE(mt.tgl_selesai) BETWEEN "'.$startDate.'" AND "'.$endDate.'")
                '.$where_mitra.'
            GROUP BY mt.no_tiket
        ');
    }

    public static function log_kpro()
    {
        return DB::table('kpro_tr6')->orderBy('LAST_SYNC', 'desc')->select('LAST_SYNC')->first();
    }

    public static function log_utonline()
    {
        return DB::table('utonline_tr6')->orderBy('last_updated_at', 'desc')->select('last_updated_at')->first();
    }

    public static function ps_kpro($type, $witel)
    {
        switch ($type) {
            case 'bulan':
                $months = ['januari' => '01', 'februari' => '02', 'maret' => '03', 'april' => '04', 'mei' => '05', 'juni' => '06', 'juli' => '07', 'agustus' => '08', 'september' => '09', 'oktober' => '10', 'november' => '11', 'desember' => '12'];
                $sums = '';
                foreach ($months as $key => $value) {
                    $nper = date('Y').''.$value;
                    $sums .= ',SUM(CASE WHEN kpt.NPER = "'.$nper.'" THEN 1 ELSE 0 END) as '.$key.'';
                }
                return DB::select('
                    SELECT
                        kpt.WITEL as area
                        '.$sums.'
                    FROM kpro_tr6 kpt
                    WHERE kpt.WITEL = "'.$witel.'"
                    GROUP BY kpt.WITEL
                ');
                break;
            
            case 'hari':
                $days = '';
                for ($i = 1; $i < date('t') + 1; $i ++) {
                    if ($i < 10)
                    {
                        $keys = '0'.$i;
                    } else {
                        $keys = $i;
                    }

                    $days .= ',SUM(CASE WHEN (DATE(kpt.LAST_UPDATED_DATE) = "'.date('Y-m-').''.$keys.'") THEN 1 ELSE 0 END) as ps_kpro_d'.$keys.'';
                }
                return DB::select('
                    SELECT
                        kpt.WITEL as area
                        '.$days.'
                    FROM kpro_tr6 kpt
                    WHERE kpt.WITEL = "'.$witel.'"
                    GROUP BY kpt.WITEL
                ');
                break;
        }
    }

    public static function dashboard_order($regional, $witel, $mitra, $startDate, $endDate)
    {
        switch ($regional) {
            case 'ALL':
                $regional = '';
                break;
            
            default:
                $regional = 'pop.regional = "'.str_replace('REG', '', $regional).'" AND';
                break;
        }

        switch ($witel) {
            case 'ALL':
                $witel = '';
                break;
            
            default:
                $witel = 'pop.witel = "'.$witel.'" AND';
                break;
        }

        switch ($mitra) {
            case 'ALL':
                $mitra = 'pm.nama_company IS NOT NULL AND';
                break;
            
            default:
                $mitra = 'pm.nama_company = "'.$mitra.'" AND';
                break;
        }

        return DB::select('
            SELECT
                pm.id as pm_id_mitra,
                pm.tacticalpro_mitra_id as id_mitra,
                pm.id_regional as regional,
                pop.witel,
                pm.nama_company as mitra,
                pm.no_kontrak_ta,
                pm.date_kontrak_ta,
                SUM(CASE WHEN pop.is_layanan = "p1_survey" THEN 1 ELSE 0 END) as p1_survey,
                SUM(CASE WHEN pop.is_layanan = "p2_tlpint_survey" THEN 1 ELSE 0 END) as p2_tlpint_survey,
                SUM(CASE WHEN pop.is_layanan = "p2_intiptv_survey" THEN 1 ELSE 0 END) as p2_intiptv_survey,
                SUM(CASE WHEN pop.is_layanan = "p3_survey" THEN 1 ELSE 0 END) as p3_survey,
                SUM(CASE WHEN pop.is_layanan = "p1" THEN 1 ELSE 0 END) as p1,
                SUM(CASE WHEN pop.is_layanan = "p2_tlpint" THEN 1 ELSE 0 END) as p2_tlpint,
                SUM(CASE WHEN pop.is_layanan = "p2_intiptv" THEN 1 ELSE 0 END) as p2_intiptv,
                SUM(CASE WHEN pop.is_layanan = "p3" THEN 1 ELSE 0 END) as p3,
                SUM(CASE WHEN pop.is_layanan = "migrasi_service_1p2p" THEN 1 ELSE 0 END) as migrasi_service_1p2p,
                SUM(CASE WHEN pop.is_layanan = "migrasi_service_1p3p" THEN 1 ELSE 0 END) as migrasi_service_1p3p,
                SUM(CASE WHEN pop.is_layanan = "migrasi_service_2p3p" THEN 1 ELSE 0 END) as migrasi_service_2p3p,
                SUM(CASE WHEN pop.is_layanan NOT IN ("p1_survey", "p2_tlpint_survey", "p2_intiptv_survey", "p3_survey", "p1", "p2_tlpint", "p2_intiptv", "p3", "migrasi_service_1p2p", "migrasi_service_1p3p", "migrasi_service_2p3p") AND pop.is_addon = 3 THEN 1 ELSE 0 END) as addon_nok,
                SUM(CASE WHEN pop.is_layanan NOT IN ("p1_survey", "p2_tlpint_survey", "p2_intiptv_survey", "p3_survey", "p1", "p2_tlpint", "p2_intiptv", "p3", "migrasi_service_1p2p", "migrasi_service_1p3p", "migrasi_service_2p3p") AND pop.is_addon IN (0, 1, 2) THEN 1 ELSE 0 END) as addon_ok,
                SUM(CASE WHEN pop.is_layanan = "ikr_addon_stb" AND pop.is_addon IN (0, 1, 2) THEN 1 ELSE 0 END) as ikr_addon_stb,
                SUM(CASE WHEN pop.is_layanan = "indihome_smart" AND pop.is_addon IN (0, 1, 2) THEN 1 ELSE 0 END) as indihome_smart,
                SUM(CASE WHEN pop.is_layanan = "wifiextender" AND pop.is_addon IN (0, 1, 2) THEN 1 ELSE 0 END) as wifiextender,
                SUM(CASE WHEN pop.is_layanan = "plc" AND pop.is_addon IN (0, 1, 2) THEN 1 ELSE 0 END) as plc,
                SUM(CASE WHEN pop.is_layanan = "lme_wifi_pt1" AND pop.is_addon IN (0, 1, 2) THEN 1 ELSE 0 END) as lme_wifi_pt1,
                SUM(CASE WHEN pop.is_layanan = "lme_ap_indoor" AND pop.is_addon IN (0, 1, 2) THEN 1 ELSE 0 END) as lme_ap_indoor,
                SUM(CASE WHEN pop.is_layanan = "lme_ap_outdoor" AND pop.is_addon IN (0, 1, 2) THEN 1 ELSE 0 END) as lme_ap_outdoor,
                SUM(CASE WHEN pop.is_layanan = "migrasi_stb" AND pop.is_addon IN (0, 1, 2) THEN 1 ELSE 0 END) as migrasi_stb,
                SUM(CASE WHEN pop.is_layanan = "instalasi_ipcam" AND pop.is_addon IN (0, 1, 2) THEN 1 ELSE 0 END) as instalasi_ipcam,
                SUM(CASE WHEN pop.is_layanan = "pu_s7_140" AND pop.is_addon IN (0, 1, 2) THEN 1 ELSE 0 END) as pu_s7_140,
                SUM(CASE WHEN pop.is_layanan = "pu_s9_140" AND pop.is_addon IN (0, 1, 2) THEN 1 ELSE 0 END) as pu_s9_140,
                SUM(CASE WHEN pop.is_rekon = 0 THEN 1 ELSE 0 END) as belum_rekon,
                SUM(CASE WHEN pop.is_rekon = 1 THEN 1 ELSE 0 END) as sudah_rekon
            FROM procurement_order_psb pop
            JOIN procurement_mitra pm ON pop.id_mitra = pm.tacticalpro_mitra_id AND pm.id_regional = pop.regional AND pop.witel = UPPER(pm.area)
            WHERE
                '.$regional.'
                '.$witel.'
                '.$mitra.'
                (DATE(pop.last_updated_date) BETWEEN "'.$startDate.'" AND "'.$endDate.'")
            GROUP BY pm.tacticalpro_mitra_id
            ORDER BY pm.witel, pm.nama_company ASC
        ');
    }

    public static function dashboard_order_detail($regional, $witel, $mitra, $startDate, $endDate, $layanan)
    {
        switch ($regional) {
            case 'ALL':
                $regional = '';
                break;
            
            default:
                $regional = 'pop.regional = "'.str_replace('REG', '', $regional).'" AND';
                break;
        }

        switch ($witel) {
            case 'ALL':
                $witel = '';
                break;
            
            default:
                $witel = 'pop.witel = "'.$witel.'" AND';
                break;
        }

        switch ($mitra) {
            case 'ALL':
                $mitra = 'pm.nama_company IS NOT NULL AND';
                break;
            
            default:
                $mitra = 'pm.nama_company = "'.$mitra.'" AND';
                break;
        }

        switch ($layanan) {
            case 'ALL':
                $layanan = '';
                break;

            case 'belum_rekon':
                $layanan = 'pop.is_rekon = 0 AND';
                break;

            case 'sudah_rekon':
                $layanan = 'pop.is_rekon = 1 AND';
                break;
            
            case 'p1_survey':
                $layanan = 'pop.is_layanan = "p1_survey" AND';
                break;
            
            case 'p2_tlpint_survey':
                $layanan = 'pop.is_layanan = "p2_tlpint_survey" AND';
                break;

            case 'p2_intiptv_survey':
                $layanan = 'pop.is_layanan = "p2_intiptv_survey" AND';
                break;
            
            case 'p3_survey':
                $layanan = 'pop.is_layanan = "p3_survey_ssl" AND';
                break;

            case 'p1':
                $layanan = 'pop.is_layanan = "p1" AND';
                break;
            
            case 'p2_tlpint':
                $layanan = 'pop.is_layanan = "p2_tlpint" AND';
                break;

            case 'p2_intiptv':
                $layanan = 'pop.is_layanan = "p2_intiptv" AND';
                break;
            
            case 'p3':
                $layanan = 'pop.is_layanan = "p3_ssl" AND';
                break;

            case 'migrasi_service_1p2p':
                $layanan = 'pop.is_layanan = "migrasi_service_1p2p" AND';
                break;

            case 'migrasi_service_1p3p':
                $layanan = 'pop.is_layanan = "migrasi_service_1p3p" AND';
                break;
            
            case 'migrasi_service_2p3p':
                $layanan = 'pop.is_layanan = "migrasi_service_2p3p" AND';
                break;

            case 'ikr_addon_stb':
                $layanan = 'pop.is_layanan = "ikr_addon_stb" AND pop.is_addon IN (0, 1, 2) AND';
                break;

            case 'indihome_smart':
                $layanan = 'pop.is_layanan = "indihome_smart" AND pop.is_addon IN (0, 1, 2) AND';
                break;

            case 'wifiextender':
                $layanan = 'pop.is_layanan = "wifiextender" AND pop.is_addon IN (0, 1, 2) AND';
                break;

            case 'plc':
                $layanan = 'pop.is_layanan = "plc" AND pop.is_addon IN (0, 1, 2) AND';
                break;

            case 'lme_wifi_pt1':
                $layanan = 'pop.is_layanan = "lme_wifi_pt1" AND pop.is_addon IN (0, 1, 2) AND';
                break;

            case 'lme_ap_indoor':
                $layanan = 'pop.is_layanan = "lme_ap_indoor" AND pop.is_addon IN (0, 1, 2) AND';
                break;

            case 'lme_ap_outdoor':
                $layanan = 'pop.is_layanan = "lme_ap_outdoor" AND pop.is_addon IN (0, 1, 2) AND';
                break;
            
            case 'migrasi_stb':
                $layanan = 'pop.is_layanan = "migrasi_stb" AND pop.is_addon IN (0, 1, 2) AND';
                break;

            case 'instalasi_ipcam':
                $layanan = 'pop.is_layanan = "instalasi_ipcam" AND pop.is_addon IN (0, 1, 2) AND';
                break;

            case 'pu_s7_140':
                $layanan = 'pop.is_layanan = "pu_s7_140" AND pop.is_addon IN (0, 1, 2) AND';
                break;

            case 'pu_s9_140':
                $layanan = 'pop.is_layanan = "pu_s9_140" AND pop.is_addon IN (0, 1, 2) AND';
                break;

            case 'addon_nok':
                $layanan = 'pop.is_layanan NOT IN ("p1_survey", "p2_tlpint_survey", "p2_intiptv_survey", "p3_survey", "p1", "p2_tlpint", "p2_intiptv", "p3", "migrasi_service_1p2p", "migrasi_service_1p3p", "migrasi_service_2p3p") AND pop.is_addon = 3 AND';
                break;

            case 'addon_ok':
                $layanan = 'pop.is_layanan NOT IN ("p1_survey", "p2_tlpint_survey", "p2_intiptv_survey", "p3_survey", "p1", "p2_tlpint", "p2_intiptv", "p3", "migrasi_service_1p2p", "migrasi_service_1p3p", "migrasi_service_2p3p") AND pop.is_addon IN (0, 1, 2) AND';
                break;
        }

        return DB::select('
            SELECT
                pm.nama_company as mitra,
                plp.layanan as plp_layanan,
                pop.*
            FROM procurement_order_psb pop
            JOIN procurement_layanan_psb plp ON pop.is_layanan = plp.alias_layanan
            JOIN procurement_mitra pm ON pop.id_mitra = pm.tacticalpro_mitra_id AND pm.id_regional = pop.regional AND pop.witel = UPPER(pm.area)
            WHERE
                '.$regional.'
                '.$witel.'
                '.$mitra.'
                '.$layanan.'
                (DATE(pop.last_updated_date) BETWEEN "'.$startDate.'" AND "'.$endDate.'")
            ORDER BY pop.last_updated_date ASC
        ');
    }

    public static function rekonProvisioningKPRO($view, $witel, $startDate, $endDate)
    {
        switch ($view) {
            case 'DAY':
                $days = '';
                for ($i = 1; $i < date('t') + 1; $i ++) {
                    if ($i < 10)
                    {
                        $keys = '0'.$i;
                    } else {
                        $keys = $i;
                    }

                    $days .= ',SUM(CASE WHEN (DATE(kpt.LAST_UPDATED_DATE) = "'.date('Y-m-').''.$keys.'") THEN 1 ELSE 0 END) as ps_kpro_d'.$keys.'';
                }
                return DB::select('
                    SELECT
                        kpt.WITEL as area
                        '.$days.'
                    FROM kpro_tr6 kpt
                    WHERE kpt.WITEL = "'.$witel.'"
                    GROUP BY kpt.WITEL
                ');
                break;
            case 'MONTH':
                $months = ['januari' => '01', 'februari' => '02', 'maret' => '03', 'april' => '04', 'mei' => '05', 'juni' => '06', 'juli' => '07', 'agustus' => '08', 'september' => '09', 'oktober' => '10', 'november' => '11', 'desember' => '12'];
                $sums = '';
                foreach ($months as $key => $value) {
                    $nper = date('Y').''.$value;
                    $sums .= ',SUM(CASE WHEN kpt.NPER = "'.$nper.'" THEN 1 ELSE 0 END) as '.$key.'';
                }
                return DB::select('
                    SELECT
                        kpt.WITEL as area
                        '.$sums.'
                    FROM kpro_tr6 kpt
                    WHERE kpt.WITEL = "'.$witel.'"
                    GROUP BY kpt.WITEL
                ');
                break;
            case 'ORDER':
                switch ($witel) {
                    case 'BANJARMASIN':
                        return DB::select('
                            SELECT
                                kpt.REGIONAL AS reg,
                                kpt.WITEL AS witel,
                                ma.mitra_amija_pt AS area,
                                SUM(CASE WHEN kpt.TYPE_LAYANAN = "1P" AND kpt.FLAG_DEPOSIT != "Deposit BYOD" AND kpt.PRODUCT = "INDIHOME" THEN 1 ELSE 0 END) AS p1,
                                SUM(CASE WHEN kpt.TYPE_LAYANAN = "2P" AND kpt.FLAG_DEPOSIT != "Deposit BYOD" AND kpt.PRODUCT = "INDIHOME" AND kpt.POTS != "" AND kpt.SPEEDY != "" THEN 1 ELSE 0 END) AS p2_tlpint,
                                SUM(CASE WHEN kpt.TYPE_LAYANAN = "2P" AND kpt.FLAG_DEPOSIT != "Deposit BYOD" AND kpt.PRODUCT = "INDIHOME" AND kpt.POTS = "" AND kpt.SPEEDY != "" THEN 1 ELSE 0 END) AS p2_intiptv,
                                SUM(CASE WHEN kpt.TYPE_LAYANAN = "3P" AND kpt.FLAG_DEPOSIT != "Deposit BYOD" AND kpt.PRODUCT = "INDIHOME" THEN 1 ELSE 0 END) AS p3,
                                SUM(CASE WHEN kpt.TYPE_LAYANAN = "1P" AND kpt.FLAG_DEPOSIT = "Deposit BYOD" AND kpt.PRODUCT = "INDIHOME" THEN 1 ELSE 0 END) AS p1_survey,
                                SUM(CASE WHEN kpt.TYPE_LAYANAN = "2P" AND kpt.FLAG_DEPOSIT = "Deposit BYOD" AND kpt.PRODUCT = "INDIHOME" AND kpt.POTS != "" AND kpt.SPEEDY != "" THEN 1 ELSE 0 END) AS p2_tlpint_survey,
                                SUM(CASE WHEN kpt.TYPE_LAYANAN = "2P" AND kpt.FLAG_DEPOSIT = "Deposit BYOD" AND kpt.PRODUCT = "INDIHOME" AND kpt.POTS = "" AND kpt.SPEEDY != "" THEN 1 ELSE 0 END) AS p2_intiptv_survey,
                                SUM(CASE WHEN kpt.TYPE_LAYANAN = "3P" AND kpt.FLAG_DEPOSIT = "Deposit BYOD" AND kpt.PRODUCT = "INDIHOME" THEN 1 ELSE 0 END) AS p3_survey,
                                SUM(CASE WHEN kpt.PRODUCT = "WMS" THEN 1 ELSE 0 END) AS wms,
                                SUM(CASE WHEN kpt.PRODUCT = "WMS LITE" THEN 1 ELSE 0 END) AS wms_lite,
                                SUM(CASE WHEN kpt.JENIS_PSB = "MO" AND kpt.KCONTACT LIKE "%ADDONSTB%" THEN 1 ELSE 0 END) AS addon_stb,
                                SUM(CASE WHEN kpt.JENIS_PSB = "MO" AND kpt.PACKAGE_NAME LIKE "%Biaya Sewa Second Set Top Box%" THEN 1 ELSE 0 END) AS second_stb,
                                SUM(CASE WHEN kpt.JENIS_PSB = "MO" AND kpt.PACKAGE_NAME LIKE "%Biaya Sewa ONT Premium%" THEN 1 ELSE 0 END) AS ont_premium,
                                (SELECT COUNT(opl.wo_id) FROM ont_premium_log opl LEFT JOIN data_teknisi dtt ON opl.nik_teknisi_1 = dtt.nik WHERE opl.kode = "ps" AND (DATE(opl.tanggal_usage) BETWEEN "'.$startDate.'" AND "'.$endDate.'") AND dtt.mitra = ma.mitra_amija_pt) AS ont_premium_dns,
                                (SELECT COUNT(mspl.wo_id) FROM migrasi_stb_premium_log mspl LEFT JOIN data_teknisi dtt ON mspl.nik_teknisi_1 = dtt.nik WHERE mspl.kode = "ps" AND (DATE(mspl.tanggal_usage) BETWEEN "'.$startDate.'" AND "'.$endDate.'") AND dtt.mitra = ma.mitra_amija_pt) AS migrasi_stb_dns,
                                SUM(CASE WHEN kpt.JENIS_PSB = "MO" AND kpt.PACKAGE_NAME LIKE "%SWWIFIEXT%" THEN 1 ELSE 0 END) AS wifiext,
                                SUM(CASE WHEN kpt.JENIS_PSB = "MO" AND kpt.PACKAGE_NAME LIKE "%Sewa Mesh WiFi%" THEN 1 ELSE 0 END) AS meshwifi,
                                SUM(CASE WHEN kpt.JENIS_PSB = "MO" AND kpt.PACKAGE_NAME LIKE "%SWPLC%" THEN 1 ELSE 0 END) AS plc,
                                SUM(CASE WHEN kpt.JENIS_PSB = "MO" AND kpt.PACKAGE_NAME LIKE "%Sewa Perangkat Smart Camera%" THEN 1 ELSE 0 END) AS indihome_smart,
                                (SELECT SUM(CASE WHEN mttr.id_item = "PU-S7.0-140" THEN mttr.qty ELSE 0 END) FROM maintaince mt LEFT JOIN maintaince_mtr mttr ON mt.id = mttr.maintaince_id LEFT JOIN mitra_amija maa ON mt.nama_mitra = maa.mitra_marina AND maa.kat = "DELTA" AND maa.witel = "KALSEL" WHERE mt.status IN ("close", "sudah tanam") AND mt.nama_order = "INSERT TIANG" AND (DATE(mt.tgl_selesai) BETWEEN "'.$startDate.'" AND "'.$endDate.'") AND maa.mitra_amija_pt = ma.mitra_amija_pt) AS tiang
                            FROM kpro_tr6 kpt
                            LEFT JOIN dispatch_teknisi dt ON kpt.ORDER_ID = dt.NO_ORDER
                            LEFT JOIN regu r ON dt.id_regu = r.id_regu
                            LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
                            LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
                            WHERE
                                (DATE(kpt.LAST_UPDATED_DATE) BETWEEN "'.$startDate.'" AND "'.$endDate.'") AND
                                (kpt.STATUS_RESUME = "Completed (PS)" OR kpt.STATUS_MESSAGE = "Completed") AND
                                kpt.WITEL = "BANJARMASIN" AND
                                (dt.jenis_order IN ("SC", "MYIR") OR dt.jenis_order IS NULL) AND
                                (dt.jenis_layanan NOT IN ("CABUT_NTE", "QC") OR dt.jenis_layanan IS NULL)
                            GROUP BY ma.mitra_amija_pt
                        ');
                        break;
                    
                    case 'BALIKPAPAN':
                        return DB::select('
                            SELECT
                                kpt.REGIONAL AS reg,
                                kpt.WITEL AS witel,
                                md.mitra_prov AS area,
                                SUM(CASE WHEN kpt.TYPE_LAYANAN = "1P" AND kpt.FLAG_DEPOSIT != "Deposit BYOD" AND kpt.PRODUCT = "INDIHOME" THEN 1 ELSE 0 END) AS p1,
                                SUM(CASE WHEN kpt.TYPE_LAYANAN = "2P" AND kpt.FLAG_DEPOSIT != "Deposit BYOD" AND kpt.PRODUCT = "INDIHOME" AND kpt.POTS != "" AND kpt.SPEEDY != "" THEN 1 ELSE 0 END) AS p2_tlpint,
                                SUM(CASE WHEN kpt.TYPE_LAYANAN = "2P" AND kpt.FLAG_DEPOSIT != "Deposit BYOD" AND kpt.PRODUCT = "INDIHOME" AND kpt.POTS = "" AND kpt.SPEEDY != "" THEN 1 ELSE 0 END) AS p2_intiptv,
                                SUM(CASE WHEN kpt.TYPE_LAYANAN = "3P" AND kpt.FLAG_DEPOSIT != "Deposit BYOD" AND kpt.PRODUCT = "INDIHOME" THEN 1 ELSE 0 END) AS p3,
                                SUM(CASE WHEN kpt.TYPE_LAYANAN = "1P" AND kpt.FLAG_DEPOSIT = "Deposit BYOD" AND kpt.PRODUCT = "INDIHOME" THEN 1 ELSE 0 END) AS p1_survey,
                                SUM(CASE WHEN kpt.TYPE_LAYANAN = "2P" AND kpt.FLAG_DEPOSIT = "Deposit BYOD" AND kpt.PRODUCT = "INDIHOME" AND kpt.POTS != "" AND kpt.SPEEDY != "" THEN 1 ELSE 0 END) AS p2_tlpint_survey,
                                SUM(CASE WHEN kpt.TYPE_LAYANAN = "2P" AND kpt.FLAG_DEPOSIT = "Deposit BYOD" AND kpt.PRODUCT = "INDIHOME" AND kpt.POTS = "" AND kpt.SPEEDY != "" THEN 1 ELSE 0 END) AS p2_intiptv_survey,
                                SUM(CASE WHEN kpt.TYPE_LAYANAN = "3P" AND kpt.FLAG_DEPOSIT = "Deposit BYOD" AND kpt.PRODUCT = "INDIHOME" THEN 1 ELSE 0 END) AS p3_survey,
                                SUM(CASE WHEN kpt.PRODUCT = "WMS" THEN 1 ELSE 0 END) AS wms,
                                SUM(CASE WHEN kpt.PRODUCT = "WMS LITE" THEN 1 ELSE 0 END) AS wms_lite,
                                SUM(CASE WHEN kpt.JENIS_PSB = "MO" AND kpt.KCONTACT LIKE "%ADDONSTB%" THEN 1 ELSE 0 END) AS addon_stb,
                                SUM(CASE WHEN kpt.JENIS_PSB = "MO" AND kpt.PACKAGE_NAME LIKE "%Biaya Sewa Second Set Top Box%" THEN 1 ELSE 0 END) AS second_stb,
                                SUM(CASE WHEN kpt.JENIS_PSB = "MO" AND kpt.PACKAGE_NAME LIKE "%Biaya Sewa ONT Premium%" THEN 1 ELSE 0 END) AS ont_premium,
                                "0" AS ont_premium_dns,
                                "0" AS migrasi_stb_dns,
                                SUM(CASE WHEN kpt.JENIS_PSB = "MO" AND kpt.PACKAGE_NAME LIKE "%SWWIFIEXT%" THEN 1 ELSE 0 END) AS wifiext,
                                SUM(CASE WHEN kpt.JENIS_PSB = "MO" AND kpt.PACKAGE_NAME LIKE "%Sewa Mesh WiFi%" THEN 1 ELSE 0 END) AS meshwifi,
                                SUM(CASE WHEN kpt.JENIS_PSB = "MO" AND kpt.PACKAGE_NAME LIKE "%SWPLC%" THEN 1 ELSE 0 END) AS plc,
                                SUM(CASE WHEN kpt.JENIS_PSB = "MO" AND kpt.PACKAGE_NAME LIKE "%Sewa Perangkat Smart Camera%" THEN 1 ELSE 0 END) AS indihome_smart,
                                "0" AS tiang
                            FROM kpro_tr6 kpt
                            LEFT JOIN maintenance_datel md ON kpt.STO = md.sto AND md.witel = "BALIKPAPAN"
                            WHERE
                                (DATE(kpt.LAST_UPDATED_DATE) BETWEEN "'.$startDate.'" AND "'.$endDate.'") AND
                                (kpt.STATUS_RESUME = "Completed (PS)" OR kpt.STATUS_MESSAGE = "Completed") AND
                                kpt.WITEL = "BALIKPAPAN"
                            GROUP BY md.mitra_prov
                        ');
                        break;
                }
                break;
        }
    }

    public static function rekonOrderKproDetail($view, $witel, $area, $layanan, $startDate, $endDate, $nper)
    {
        switch ($layanan) {
            case 'p1':
                $is_layanan = '0P - 1P (Tlp/Int)';
                $layananx = 'AND kpt.TYPE_LAYANAN = "1P" AND kpt.FLAG_DEPOSIT != "Deposit BYOD" AND kpt.PRODUCT = "INDIHOME"';
                break;
            case 'p2_tlpint':
                $is_layanan = '0P – 2P (Tlp+Int)';
                $layananx = 'AND kpt.TYPE_LAYANAN = "2P" AND kpt.FLAG_DEPOSIT != "Deposit BYOD" AND kpt.PRODUCT = "INDIHOME" AND kpt.POTS != "" AND kpt.SPEEDY != ""';
                break;
            case 'p2_intiptv':
                $is_layanan = '0P – 2P (Int+IPTV)';
                $layananx = 'AND kpt.TYPE_LAYANAN = "2P" AND kpt.FLAG_DEPOSIT != "Deposit BYOD" AND kpt.PRODUCT = "INDIHOME" AND kpt.POTS = "" AND kpt.SPEEDY != ""';
                break;
            case 'p3':
                $is_layanan = '0P - 3P (Tlp+Int+IPTV)';
                $layananx = 'AND kpt.TYPE_LAYANAN = "3P" AND kpt.FLAG_DEPOSIT != "Deposit BYOD" AND kpt.PRODUCT = "INDIHOME"';
                break;
            case 'p1_survey':
                $is_layanan = '0P - 1P (Tlp/Int)';
                $layananx = 'AND kpt.TYPE_LAYANAN = "1P" AND kpt.FLAG_DEPOSIT = "Deposit BYOD" AND kpt.PRODUCT = "INDIHOME"';
                break;
            case 'p2_tlpint_survey':
                $is_layanan = '0P – 2P (Tlp+Int)';
                $layananx = 'AND kpt.TYPE_LAYANAN = "2P" AND kpt.FLAG_DEPOSIT = "Deposit BYOD" AND kpt.PRODUCT = "INDIHOME" AND kpt.POTS != "" AND kpt.SPEEDY != ""';
                break;
            case 'p2_intiptv_survey':
                $is_layanan = '0P – 2P (Int+IPTV)';
                $layananx = 'AND kpt.TYPE_LAYANAN = "2P" AND kpt.FLAG_DEPOSIT = "Deposit BYOD" AND kpt.PRODUCT = "INDIHOME" AND kpt.POTS = "" AND kpt.SPEEDY != ""';
                break;
            case 'p3_survey':
                $is_layanan = '0P - 3P (Tlp+Int+IPTV)';
                $layananx = 'AND kpt.TYPE_LAYANAN = "3P" AND kpt.FLAG_DEPOSIT = "Deposit BYOD" AND kpt.PRODUCT = "INDIHOME"';
                break;
            case 'wms':
                $is_layanan = 'WMS';
                $layananx = 'AND kpt.PRODUCT = "WMS"';
                break;
            case 'wms_lite':
                $is_layanan = 'WMS LITE';
                $layananx = 'AND kpt.PRODUCT = "WMS LITE"';
                break;
            case 'addon_stb':
                $is_layanan = 'ADDON STB';
                $layananx = 'AND kpt.JENIS_PSB = "MO" AND kpt.KCONTACT LIKE "%ADDONSTB%"';
                break;
            case 'second_stb':
                $is_layanan = 'SECOND STB';
                $layananx = 'AND kpt.JENIS_PSB = "MO" AND kpt.PACKAGE_NAME LIKE "%Biaya Sewa Second Set Top Box%"';
                break;
            case 'ont_premium':
                $is_layanan = 'ONT PREMIUM';
                $layananx = 'AND kpt.JENIS_PSB = "MO" AND kpt.PACKAGE_NAME LIKE "%Biaya Sewa ONT Premium%"';
                break;
            case 'wifiext':
                $is_layanan = 'WIFI EXT';
                $layananx = 'AND kpt.JENIS_PSB = "MO" AND kpt.PACKAGE_NAME LIKE "%SWWIFIEXT%"';
                break;
            case 'meshwifi':
                $is_layanan = 'MESH WIFI';
                $layananx = 'AND kpt.JENIS_PSB = "MO" AND kpt.PACKAGE_NAME LIKE "%Sewa Mesh WiFi%"';
                break;
            case 'plc':
                $is_layanan = 'PLC';
                $layananx = 'AND kpt.JENIS_PSB = "MO" AND kpt.PACKAGE_NAME LIKE "%SWPLC%"';
                break;
            case 'indihome_smart':
                $is_layanan = 'INDIHOME SMART';
                $layananx = 'AND kpt.JENIS_PSB = "MO" AND kpt.PACKAGE_NAME LIKE "%Sewa Perangkat Smart Camera%"';
                break;
            case 'tiang':
                $is_layanan = 'TIANG';
                $layananx = '';
                break;
        }

        switch ($view) {
            case 'MONTH':
                return DB::select('
                    SELECT
                        kpt.REGIONAL AS reg,
                        kpt.WITEL AS witel,
                        null AS area,
                        null AS is_layanan,
                        kpt.*,
                        pmp.drop_core,
                        pmp.precon,
                        pmp.tiang,
                        pmp.klem_ring,
                        pmp.otp,
                        pmp.prekso,
                        pmp.roset,
                        pmp.s_clamp,
                        pmp.soc,
                        pmp.clamp_hook,
                        pmp.segitiga_bracket,
                        pmp.tray_cable,
                        utt.create_dtm AS utt_create_dtm,
                        utt.statusName AS utt_statusName
                    FROM kpro_tr6 kpt
                    LEFT JOIN pemakaian_material_psb pmp ON kpt.ORDER_ID = pmp.sc_id
                    LEFT JOIN utonline_tr6 utt ON kpt.SPEEDY = utt.noInternet
                    WHERE
                        kpt.WITEL = "'.$witel.'" AND
                        kpt.NPER = '.$nper.'
                    GROUP BY kpt.ORDER_ID
                ');
                break;
            
            case 'DAY':
                return DB::select('
                    SELECT
                        kpt.REGIONAL AS reg,
                        kpt.WITEL AS witel,
                        null AS area,
                        null AS is_layanan,
                        kpt.*,
                        pmp.drop_core,
                        pmp.precon,
                        pmp.tiang,
                        pmp.klem_ring,
                        pmp.otp,
                        pmp.prekso,
                        pmp.roset,
                        pmp.s_clamp,
                        pmp.soc,
                        pmp.clamp_hook,
                        pmp.segitiga_bracket,
                        pmp.tray_cable,
                        utt.create_dtm AS utt_create_dtm,
                        utt.statusName AS utt_statusName
                    FROM kpro_tr6 kpt
                    LEFT JOIN pemakaian_material_psb pmp ON kpt.ORDER_ID = pmp.sc_id
                    LEFT JOIN utonline_tr6 utt ON kpt.SPEEDY = utt.noInternet
                    WHERE
                        kpt.WITEL = "'.$witel.'" AND
                        (DATE(kpt.LAST_UPDATED_DATE) = "'.$nper.'")
                    GROUP BY kpt.ORDER_ID
                ');
                break;
        }

        switch ($witel) {
            case 'BANJARMASIN':
                switch ($area) {
                    case 'ALL':
                        $areax = '';
                        break;
                    case 'NON AREA':
                        $areax = 'AND ma.mitra_amija_pt IS NULL';
                        break;
                    default:
                        $areax = 'AND ma.mitra_amija_pt = "'.$area.'"';
                        break;
                }

                switch ($view) {
                    case 'ORDER':
                        return DB::select('
                            SELECT
                                kpt.REGIONAL AS reg,
                                kpt.WITEL AS witel,
                                ma.mitra_amija_pt AS area,
                                "'.$is_layanan.'" AS is_layanan,
                                kpt.*,
                                pmp.drop_core,
                                pmp.precon,
                                pmp.tiang,
                                pmp.klem_ring,
                                pmp.otp,
                                pmp.prekso,
                                pmp.roset,
                                pmp.s_clamp,
                                pmp.soc,
                                pmp.clamp_hook,
                                pmp.segitiga_bracket,
                                pmp.tray_cable,
                                utt.create_dtm AS utt_create_dtm,
                                utt.statusName AS utt_statusName
                            FROM kpro_tr6 kpt
                            LEFT JOIN dispatch_teknisi dt ON kpt.ORDER_ID = dt.NO_ORDER
                            LEFT JOIN pemakaian_material_psb pmp ON kpt.ORDER_ID = pmp.sc_id
                            LEFT JOIN utonline_tr6 utt ON kpt.SPEEDY = utt.noInternet
                            LEFT JOIN regu r ON dt.id_regu = r.id_regu
                            LEFT JOIN group_telegram gt ON r.mainsector = gt.chat_id
                            LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija AND ma.witel = "KALSEL"
                            WHERE
                                (DATE(kpt.LAST_UPDATED_DATE) BETWEEN "'.$startDate.'" AND "'.$endDate.'") AND
                                (kpt.STATUS_RESUME = "Completed (PS)" OR kpt.STATUS_MESSAGE = "Completed") AND
                                kpt.WITEL = "BANJARMASIN" AND
                                (dt.jenis_order IN ("SC", "MYIR") OR dt.jenis_order IS NULL) AND
                                (dt.jenis_layanan NOT IN ("CABUT_NTE", "QC") OR dt.jenis_layanan IS NULL)
                                '.$layananx.'
                                '.$areax.'
                        ');
                        break;
                }
                break;
            
            case 'BALIKPAPAN':
                switch ($area) {
                    case 'ALL':
                        $areax = '';
                        break;
                    case 'NON AREA':
                        $areax = 'AND md.mitra_prov IS NULL';
                        break;
                    default:
                        $areax = 'AND md.mitra_prov = "'.$area.'"';
                        break;
                }

                switch ($view) {
                    case 'ORDER':
                        return DB::select('
                            SELECT
                                kpt.REGIONAL AS reg,
                                kpt.WITEL AS witel,
                                md.mitra_prov AS area,
                                "'.$is_layanan.'" AS is_layanan,
                                kpt.*,
                                pmp.drop_core,
                                pmp.precon,
                                pmp.tiang,
                                pmp.klem_ring,
                                pmp.otp,
                                pmp.prekso,
                                pmp.roset,
                                pmp.s_clamp,
                                pmp.soc,
                                pmp.clamp_hook,
                                pmp.segitiga_bracket,
                                pmp.tray_cable,
                                utt.create_dtm AS utt_create_dtm,
                                utt.statusName AS utt_statusName
                            FROM kpro_tr6 kpt
                            LEFT JOIN pemakaian_material_psb pmp ON kpt.ORDER_ID = pmp.sc_id
                            LEFT JOIN utonline_tr6 utt ON kpt.SPEEDY = utt.noInternet
                            LEFT JOIN maintenance_datel md ON kpt.STO = md.sto AND md.witel = "BALIKPAPAN"
                            WHERE
                                (DATE(kpt.LAST_UPDATED_DATE) BETWEEN "'.$startDate.'" AND "'.$endDate.'") AND
                                (kpt.STATUS_RESUME = "Completed (PS)" OR kpt.STATUS_MESSAGE = "Completed") AND
                                kpt.WITEL = "BALIKPAPAN"
                                '.$layananx.'
                                '.$areax.'
                        ');
                        break;
                }
                break;
        }
    }

    public static function rekonOrderDnsDetail($witel, $area, $layanan, $startDate, $endDate)
    {
        switch ($area) {
            case 'NON AREA':
                $areax = 'AND dtt.mitra IS NULL';
                break;
            case 'ALL':
                $areax = '';
                break;
            default:
                $areax = 'AND dtt.mitra = "'.$area.'"';
                break;
        }
        switch ($layanan) {
            case 'ont_premium_dns':
                return DB::select('
                    SELECT
                    *
                    FROM ont_premium_log opl
                    LEFT JOIN data_teknisi dtt ON opl.nik_teknisi_1 = dtt.nik
                    WHERE
                        opl.kode = "ps" AND
                        (DATE(opl.tanggal_usage) BETWEEN "'.$startDate.'" AND "'.$endDate.'")
                        '.$areax.'
                ');
                break;
            
            case 'migrasi_stb_dns':
                return DB::select('
                    SELECT
                    *
                    FROM migrasi_stb_premium_log mspl
                    LEFT JOIN data_teknisi dtt ON mspl.nik_teknisi_1 = dtt.nik
                    WHERE
                        mspl.kode = "ps" AND
                        (DATE(mspl.tanggal_usage) BETWEEN "'.$startDate.'" AND "'.$endDate.'")
                        '.$areax.'
                ');
                break;
        }
    }

    public static function provDashboardOrder($view, $startDate, $endDate)
    {
        switch ($view)
        {
            case 'MONTH':

                $months = ['januari' => '01', 'februari' => '02', 'maret' => '03', 'april' => '04', 'mei' => '05', 'juni' => '06', 'juli' => '07', 'agustus' => '08', 'september' => '09', 'oktober' => '10', 'november' => '11', 'desember' => '12'];

                $month = '';
                
                foreach ($months as $key => $value)
                {
                    $nper = date('Y').''.$value;
                    $month .= ',SUM(CASE WHEN kpt.NPER = "'.$nper.'" THEN 1 ELSE 0 END) as '.$key.'';
                }
                
                return DB::table("kpro_tr6 AS kpt")
                ->leftJoin("procurement_psb_cutoff AS psc", "kpt.ORDER_ID", "=", "psc.sc_id")
                ->select(DB::raw("
                    kpt.REGIONAL AS reg,
                    kpt.WITEL AS witel,
                    null AS mitra
                    $month
                "))
                ->where(function ($query) {
                    $query->where("kpt.STATUS_RESUME", "Completed (PS)")
                    ->orWhere("kpt.STATUS_MESSAGE", "Completed");
                })
                ->whereNull("psc.sc_id")
                ->groupBy("kpt.WITEL")
                ->get();

            break;

            case 'DAY':

                $days = '';
                
                for ($i = 1; $i < date('t') + 1; $i ++)
                {
                    if ($i < 10)
                    {
                        $keys = '0'.$i;
                    }
                    else
                    {
                        $keys = $i;
                    }

                    $days .= ',SUM(CASE WHEN (DATE(kpt.LAST_UPDATED_DATE) = "'.date('Y-m-').''.$keys.'") THEN 1 ELSE 0 END) as ps_kpro_d'.$keys.'';
                }

                return DB::table("kpro_tr6 AS kpt")
                ->leftJoin("procurement_psb_cutoff AS psc", "kpt.ORDER_ID", "=", "psc.sc_id")
                ->select(DB::raw("
                    kpt.REGIONAL AS reg,
                    kpt.WITEL AS witel,
                    null AS mitra
                    $days
                "))
                ->where(function ($query) {
                    $query->where("kpt.STATUS_RESUME", "Completed (PS)")
                    ->orWhere("kpt.STATUS_MESSAGE", "Completed");
                })
                ->whereNull("psc.sc_id")
                ->groupBy("kpt.WITEL")
                ->get();

            break;

            case 'ORDER':

                return DB::table("kpro_tr6 AS kpt")
                ->leftJoin("procurement_hss_psb AS phss", "kpt.WITEL", "=", "phss.witel_ms2n")
                ->leftJoin("procurement_psb_cutoff AS psc", "kpt.ORDER_ID", "=", "psc.sc_id")
                ->select(DB::raw('
                    kpt.REGIONAL AS reg,
                    kpt.WITEL AS witel,
                    null AS mitra,
                    SUM(CASE WHEN kpt.JENIS_PSB IN ("AO", "PDA", "PDA_LOCAL") AND kpt.TYPE_LAYANAN = "1P" AND kpt.FLAG_DEPOSIT != "Deposit BYOD" AND kpt.PRODUCT = "INDIHOME" THEN 1 ELSE 0 END) AS p1,
                    phss.p1_ku as hss_p1,
                    SUM(CASE WHEN kpt.JENIS_PSB IN ("AO", "PDA", "PDA_LOCAL") AND kpt.TYPE_LAYANAN = "2P" AND kpt.FLAG_DEPOSIT != "Deposit BYOD" AND kpt.PRODUCT = "INDIHOME" AND kpt.POTS != "" AND kpt.SPEEDY != "" THEN 1 ELSE 0 END) AS p2_tlpint,
                    phss.p2_inet_voice_ku as hss_p2_tlpint,
                    SUM(CASE WHEN kpt.JENIS_PSB IN ("AO", "PDA", "PDA_LOCAL") AND kpt.TYPE_LAYANAN = "2P" AND kpt.FLAG_DEPOSIT != "Deposit BYOD" AND kpt.PRODUCT = "INDIHOME" AND kpt.POTS = "" AND kpt.SPEEDY != "" THEN 1 ELSE 0 END) AS p2_intiptv,
                    phss.p2_inet_iptv_ku as hss_p2_intiptv,
                    SUM(CASE WHEN kpt.JENIS_PSB IN ("AO", "PDA", "PDA_LOCAL") AND kpt.TYPE_LAYANAN = "3P" AND kpt.FLAG_DEPOSIT != "Deposit BYOD" AND kpt.PRODUCT = "INDIHOME" THEN 1 ELSE 0 END) AS p3,
                    phss.p3_ku as hss_p3,
                    SUM(CASE WHEN kpt.JENIS_PSB IN ("AO", "PDA", "PDA_LOCAL") AND kpt.TYPE_LAYANAN = "1P" AND kpt.FLAG_DEPOSIT = "Deposit BYOD" AND kpt.PRODUCT = "INDIHOME" THEN 1 ELSE 0 END) AS p1_survey,
                    phss.p1 as hss_p1_survey,
                    SUM(CASE WHEN kpt.JENIS_PSB IN ("AO", "PDA", "PDA_LOCAL") AND kpt.TYPE_LAYANAN = "2P" AND kpt.FLAG_DEPOSIT = "Deposit BYOD" AND kpt.PRODUCT = "INDIHOME" AND kpt.POTS != "" AND kpt.SPEEDY != "" THEN 1 ELSE 0 END) AS p2_tlpint_survey,
                    phss.p2_inet_voice as hss_p2_tlpint_survey,
                    SUM(CASE WHEN kpt.JENIS_PSB IN ("AO", "PDA", "PDA_LOCAL") AND kpt.TYPE_LAYANAN = "2P" AND kpt.FLAG_DEPOSIT = "Deposit BYOD" AND kpt.PRODUCT = "INDIHOME" AND kpt.POTS = "" AND kpt.SPEEDY != "" THEN 1 ELSE 0 END) AS p2_intiptv_survey,
                    phss.p2_inet_iptv as hss_p2_intiptv_survey,
                    SUM(CASE WHEN kpt.JENIS_PSB IN ("AO", "PDA", "PDA_LOCAL") AND kpt.TYPE_LAYANAN = "3P" AND kpt.FLAG_DEPOSIT = "Deposit BYOD" AND kpt.PRODUCT = "INDIHOME" THEN 1 ELSE 0 END) AS p3_survey,
                    phss.p3 as hss_p3_survey,
                    SUM(CASE WHEN kpt.PRODUCT = "WMS" THEN 1 ELSE 0 END) AS wms,
                    SUM(CASE WHEN kpt.PRODUCT = "WMS LITE" THEN 1 ELSE 0 END) AS wms_lite,
                    phss.lme_wifi as hss_lme_wifi,
                    phss.lme_ap_indoor as hss_lme_ap_indoor,
                    phss.lme_ap_outdoor as hss_lme_ap_outdoor,
                    SUM(CASE WHEN kpt.JENIS_PSB = "MO" AND (kpt.KCONTACT LIKE "%ADDONSTB%" OR kpt.KCONTACT LIKE "%ADD STB%") THEN 1 ELSE 0 END) AS addon_stb,
                    SUM(CASE WHEN kpt.JENIS_PSB = "MO" AND kpt.PACKAGE_NAME LIKE "%Biaya Sewa Second Set Top Box%" THEN 1 ELSE 0 END) AS second_stb,
                    phss.stb_tambahan as hss_stb_tambahan,
                    phss.migrasi_stb as hss_migrasi_stb,
                    SUM(CASE WHEN kpt.JENIS_PSB = "MO" AND kpt.KCONTACT LIKE "%CHANGE STB%" THEN 1 ELSE 0 END) AS change_stb,
                    phss.change_stb as hss_change_stb,
                    SUM(CASE WHEN kpt.JENIS_PSB = "MO" AND kpt.PACKAGE_NAME LIKE "%Biaya Sewa ONT Premium%" THEN 1 ELSE 0 END) AS ont_premium,
                    phss.ont_premium as hss_ont_premium,
                    SUM(CASE WHEN kpt.JENIS_PSB = "MO" AND (kpt.PACKAGE_NAME LIKE "%SWWIFIEXT%" OR kpt.KCONTACT LIKE "%Wifi Extender%") THEN 1 ELSE 0 END) AS wifiext,
                    SUM(CASE WHEN kpt.JENIS_PSB = "MO" AND kpt.PACKAGE_NAME LIKE "%Sewa Mesh WiFi%" THEN 1 ELSE 0 END) AS meshwifi,
                    phss.wifiext as hss_wifiext,
                    SUM(CASE WHEN kpt.JENIS_PSB = "MO" AND kpt.PACKAGE_NAME LIKE "%SWPLC%" THEN 1 ELSE 0 END) AS plc,
                    phss.plc as hss_plc,
                    SUM(CASE WHEN kpt.JENIS_PSB = "MO" AND kpt.PACKAGE_NAME LIKE "%Sewa Perangkat Smart Camera%" THEN 1 ELSE 0 END) AS indihome_smart,
                    phss.smartcam_only as hss_indihome_smart
                '))
                ->whereBetween("kpt.LAST_UPDATED_DATE", [$startDate, $endDate])
                ->where(function ($query) {
                    $query->where("kpt.STATUS_RESUME", "Completed (PS)")
                    ->orWhere("kpt.STATUS_MESSAGE", "Completed");
                })
                ->whereNull("psc.sc_id")
                ->groupBy("kpt.WITEL")
                ->get();

            break;

            case 'ORDER_CUTOFF':
                
                return DB::table("kpro_tr6 AS kpt")
                ->leftJoin("procurement_hss_psb AS phss", "kpt.WITEL", "=", "phss.witel_ms2n")
                ->leftJoin("procurement_psb_cutoff AS psc", "kpt.ORDER_ID", "=", "psc.sc_id")
                ->leftJoin("procurement_mitra AS pm", "psc.mitra_id", "=", "pm.id")
                ->select(DB::raw('
                    kpt.REGIONAL AS reg,
                    kpt.WITEL AS witel,
                    pm.nama_company AS mitra,
                    SUM(CASE WHEN psc.layanan_fix = "0P - 1P" THEN 1 ELSE 0 END) AS p1,
                    phss.p1_ku as hss_p1,
                    SUM(CASE WHEN psc.layanan_fix = "0P - 2P TELP + INET" THEN 1 ELSE 0 END) AS p2_tlpint,
                    phss.p2_inet_voice_ku as hss_p2_tlpint,
                    SUM(CASE WHEN psc.layanan_fix = "0P - 2P INET + IPTV" THEN 1 ELSE 0 END) AS p2_intiptv,
                    phss.p2_inet_iptv_ku as hss_p2_intiptv,
                    SUM(CASE WHEN psc.layanan_fix = "0P - 3P" THEN 1 ELSE 0 END) AS p3,
                    phss.p3_ku as hss_p3,
                    SUM(CASE WHEN psc.layanan_fix = "0P - 1P (BYOD)" THEN 1 ELSE 0 END) AS p1_survey,
                    phss.p1 as hss_p1_survey,
                    SUM(CASE WHEN psc.layanan_fix = "0P - 2P TELP + INET (BYOD)" THEN 1 ELSE 0 END) AS p2_tlpint_survey,
                    phss.p2_inet_voice as hss_p2_tlpint_survey,
                    SUM(CASE WHEN psc.layanan_fix = "0P - 2P INET + IPTV (BYOD)" THEN 1 ELSE 0 END) AS p2_intiptv_survey,
                    phss.p2_inet_iptv as hss_p2_intiptv_survey,
                    SUM(CASE WHEN psc.layanan_fix = "0P - 3P (BYOD)" THEN 1 ELSE 0 END) AS p3_survey,
                    phss.p3 as hss_p3_survey,
                    SUM(CASE WHEN psc.layanan_fix = "WMS" THEN 1 ELSE 0 END) AS wms,
                    SUM(CASE WHEN psc.layanan_fix = "WMS LITE" THEN 1 ELSE 0 END) AS wms_lite,
                    phss.lme_wifi as hss_lme_wifi,
                    phss.lme_ap_indoor as hss_lme_ap_indoor,
                    phss.lme_ap_outdoor as hss_lme_ap_outdoor,
                    SUM(CASE WHEN psc.layanan_fix = "ADDON STB" THEN 1 ELSE 0 END) AS addon_stb,
                    SUM(CASE WHEN psc.layanan_fix = "SECOND STB" THEN 1 ELSE 0 END) AS second_stb,
                    phss.stb_tambahan as hss_stb_tambahan,
                    phss.migrasi_stb as hss_migrasi_stb,
                    SUM(CASE WHEN psc.layanan_fix = "CHANGE STB" THEN 1 ELSE 0 END) AS change_stb,
                    phss.change_stb as hss_change_stb,
                    SUM(CASE WHEN psc.layanan_fix = "ONT PREMIUM" THEN 1 ELSE 0 END) AS ont_premium,
                    phss.ont_premium as hss_ont_premium,
                    SUM(CASE WHEN psc.layanan_fix = "WIFI EXTENDER" THEN 1 ELSE 0 END) AS wifiext,
                    SUM(CASE WHEN psc.layanan_fix = "MESH WIFI" THEN 1 ELSE 0 END) AS meshwifi,
                    phss.wifiext as hss_wifiext,
                    SUM(CASE WHEN psc.layanan_fix = "PLC" THEN 1 ELSE 0 END) AS plc,
                    phss.plc as hss_plc,
                    SUM(CASE WHEN psc.layanan_fix = "INDIHOME SMART CAMERA" THEN 1 ELSE 0 END) AS indihome_smart,
                    phss.smartcam_only as hss_indihome_smart
                '))
                ->whereBetween("kpt.LAST_UPDATED_DATE", [$startDate, $endDate])
                ->where(function ($query) {
                    $query->where("kpt.STATUS_RESUME", "Completed (PS)")
                    ->orWhere("kpt.STATUS_MESSAGE", "Completed");
                })
                ->whereNotNull("psc.sc_id")
                ->groupBy("pm.nama_company")
                ->get();

            break;
        }
    }

    public static function provDashboardOrderDetail($view, $startDate, $endDate, $witel, $mitra, $month, $day, $layanan)
    {
        switch ($view) {
            case 'MONTH':

                $data = DB::table("kpro_tr6 AS kpt")
                ->leftJoin("utonline_tr6 AS utt", "kpt.SPEEDY", "=", "utt.noInternet")
                ->leftJoin("utonline_material AS utm", "utt.order_id", "=", "utm.id_order")
                ->leftJoin("procurement_psb_cutoff AS psc", "kpt.ORDER_ID", "=", "psc.sc_id")
                ->select(DB::raw("
                    kpt.*,
                    null AS mitra,
                    utt.statusName as utt_statusName,
                    utt.qcStatusName as utt_qcStatusName,
                    utt.create_dtm as utt_create_dtm,
                    (
                        CASE
                            WHEN (JENIS_PSB IN ('AO', 'PDA', 'PDA_LOCAL') AND TYPE_LAYANAN = '1P' AND FLAG_DEPOSIT != 'Deposit BYOD' AND PRODUCT = 'INDIHOME') THEN '0P - 1P'
                            WHEN (JENIS_PSB IN ('AO', 'PDA', 'PDA_LOCAL') AND TYPE_LAYANAN = '2P' AND FLAG_DEPOSIT != 'Deposit BYOD' AND PRODUCT = 'INDIHOME' AND POTS != '' AND SPEEDY != '') THEN '0P - 2P TELP + INET'
                            WHEN (JENIS_PSB IN ('AO', 'PDA', 'PDA_LOCAL') AND TYPE_LAYANAN = '2P' AND FLAG_DEPOSIT != 'Deposit BYOD' AND PRODUCT = 'INDIHOME' AND POTS = '' AND SPEEDY != '') THEN '0P - 2P INET + IPTV'
                            WHEN (JENIS_PSB IN ('AO', 'PDA', 'PDA_LOCAL') AND TYPE_LAYANAN = '3P' AND FLAG_DEPOSIT != 'Deposit BYOD' AND PRODUCT = 'INDIHOME') THEN '0P - 3P'
                            WHEN (JENIS_PSB IN ('AO', 'PDA', 'PDA_LOCAL') AND TYPE_LAYANAN = '1P' AND FLAG_DEPOSIT = 'Deposit BYOD' AND PRODUCT = 'INDIHOME') THEN '0P - 1P (BYOD)'
                            WHEN (JENIS_PSB IN ('AO', 'PDA', 'PDA_LOCAL') AND TYPE_LAYANAN = '2P' AND FLAG_DEPOSIT = 'Deposit BYOD' AND PRODUCT = 'INDIHOME' AND POTS != '' AND SPEEDY != '') THEN '0P - 2P TELP + INET (BYOD)'
                            WHEN (JENIS_PSB IN ('AO', 'PDA', 'PDA_LOCAL') AND TYPE_LAYANAN = '2P' AND FLAG_DEPOSIT = 'Deposit BYOD' AND PRODUCT = 'INDIHOME' AND POTS = '' AND SPEEDY != '') THEN '0P - 2P INET + IPTV (BYOD)'
                            WHEN (JENIS_PSB IN ('AO', 'PDA', 'PDA_LOCAL') AND TYPE_LAYANAN = '3P' AND FLAG_DEPOSIT = 'Deposit BYOD' AND PRODUCT = 'INDIHOME') THEN '0P - 3P (BYOD)'
                            WHEN (PRODUCT = 'WMS') THEN 'WMS'
                            WHEN (PRODUCT = 'WMS LITE') THEN 'WMS LITE'
                            WHEN (JENIS_PSB = 'MO' AND (KCONTACT LIKE '%ADDONSTB%' OR KCONTACT LIKE '%ADD STB%')) THEN 'ADDON STB'
                            WHEN (JENIS_PSB = 'MO' AND PACKAGE_NAME LIKE '%Biaya Sewa Second Set Top Box%') THEN 'SECOND STB'
                            WHEN (JENIS_PSB = 'MO' AND KCONTACT LIKE '%CHANGE STB%') THEN 'CHANGE STB'
                            WHEN (JENIS_PSB = 'MO' AND PACKAGE_NAME LIKE '%Biaya Sewa ONT Premium%') THEN 'ONT PREMIUM'
                            WHEN (JENIS_PSB = 'MO' AND (PACKAGE_NAME LIKE '%SWWIFIEXT%' OR KCONTACT LIKE '%Wifi Extender%')) THEN 'WIFI EXTENDER'
                            WHEN (JENIS_PSB = 'MO' AND PACKAGE_NAME LIKE '%Sewa Mesh WiFi%') THEN 'MESH WIFI'
                            WHEN (JENIS_PSB = 'MO' AND PACKAGE_NAME LIKE '%SWPLC%') THEN 'PLC'
                            WHEN (JENIS_PSB = 'MO' AND PACKAGE_NAME LIKE '%Sewa Perangkat Smart Camera%') THEN 'INDIHOME SMART CAMERA'
                        ELSE 'Layanan Tidak Diketahui' END
                    ) AS is_layanan,
                    SUM(CASE WHEN utm.id_barang IN ('AC-OF-SM-1B', 'AC-OF-SM-12-SC') THEN utm.volume ELSE 0 END) as dc_roll,
                    SUM(CASE WHEN utm.id_barang = 'Preconnectorized-1C-50-NonAcc' THEN utm.volume ELSE 0 END) as precon50,
                    SUM(CASE WHEN utm.id_barang = 'Preconnectorized-1C-70-NonAcc' THEN utm.volume ELSE 0 END) as precon70,
                    SUM(CASE WHEN utm.id_barang = 'Preconnectorized-1C-80-NonAcc' THEN utm.volume ELSE 0 END) as precon80,
                    SUM(CASE WHEN utm.id_barang = 'AD-SC' THEN utm.volume ELSE 0 END) as adaptor_sc,
                    SUM(CASE WHEN utm.id_barang = 'OTP-FTTH-1' THEN utm.volume ELSE 0 END) as otp_ftth,
                    SUM(CASE WHEN utm.id_barang = 'PREKSO-INTRA-15-RS' THEN utm.volume ELSE 0 END) as prekso15,
                    SUM(CASE WHEN utm.id_barang = 'PREKSO-INTRA-20-RS' THEN utm.volume ELSE 0 END) as prekso20,
                    SUM(CASE WHEN utm.id_barang = 'PC-SC-SC-10' THEN utm.volume ELSE 0 END) as patchcord10,
                    SUM(CASE WHEN utm.id_barang = 'PC-SC-SC-15' THEN utm.volume ELSE 0 END) as patchcord15,
                    SUM(CASE WHEN utm.id_barang = 'UTP-C6' THEN utm.volume ELSE 0 END) as utpc6,
                    SUM(CASE WHEN utm.id_barang = 'PU-S7.0-140' THEN utm.volume ELSE 0 END) as tiang7,
                    SUM(CASE WHEN utm.id_barang = 'PU-S9.0-140' THEN utm.volume ELSE 0 END) as tiang9,
                    SUM(CASE WHEN utm.id_barang = 'TC-2-160' THEN utm.volume ELSE 0 END) as traycable,
                    SUM(CASE WHEN utm.id_barang = 'BREKET-A' THEN utm.volume ELSE 0 END) as breket,
                    SUM(CASE WHEN utm.id_barang = 'S-Clamp-Spriner' THEN utm.volume ELSE 0 END) as sclamp,
                    SUM(CASE WHEN utm.id_barang = 'RJ45-5' THEN utm.volume ELSE 0 END) as rj45t5,
                    SUM(CASE WHEN utm.id_barang = 'RJ45-6' THEN utm.volume ELSE 0 END) as rj45t6,
                    SUM(CASE WHEN utm.id_barang = 'SOC-ILS' THEN utm.volume ELSE 0 END) as soc_ils,
                    SUM(CASE WHEN utm.id_barang = 'SOC-SUM' THEN utm.volume ELSE 0 END) as soc_sum
                "))
                ->where(function ($query) {
                    $query->where("kpt.STATUS_RESUME", "Completed (PS)")
                    ->orWhere("kpt.STATUS_MESSAGE", "Completed");
                })
                ->whereNull("psc.sc_id");

                if ($witel == 'ALL')
                {
                    $data->where('kpt.NPER', $month);
                }
                else if ($month == 'ALL')
                {
                    $data->where([
                        ['kpt.NPER', 'LIKE', date('Y').'%'],
                        ['kpt.WITEL', $witel]
                    ]);
                }
                else
                {
                    $data->where([
                        ['kpt.WITEL', $witel],
                        ['kpt.NPER', $month]
                    ]);
                }

                return $data->groupBy("kpt.ORDER_ID")->get();;
                
            break;

            case 'DAY':

                return DB::table("kpro_tr6 AS kpt")
                ->leftJoin("utonline_tr6 AS utt", "kpt.SPEEDY", "=", "utt.noInternet")
                ->leftJoin("utonline_material AS utm", "utt.order_id", "=", "utm.id_order")
                ->leftJoin("procurement_psb_cutoff AS psc", "kpt.ORDER_ID", "=", "psc.sc_id")
                ->select(DB::raw("
                    kpt.*,
                    null AS mitra,
                    utt.statusName as utt_statusName,
                    utt.qcStatusName as utt_qcStatusName,
                    utt.create_dtm as utt_create_dtm,
                    (
                        CASE
                            WHEN (JENIS_PSB IN ('AO', 'PDA', 'PDA_LOCAL') AND TYPE_LAYANAN = '1P' AND FLAG_DEPOSIT != 'Deposit BYOD' AND PRODUCT = 'INDIHOME') THEN '0P - 1P'
                            WHEN (JENIS_PSB IN ('AO', 'PDA', 'PDA_LOCAL') AND TYPE_LAYANAN = '2P' AND FLAG_DEPOSIT != 'Deposit BYOD' AND PRODUCT = 'INDIHOME' AND POTS != '' AND SPEEDY != '') THEN '0P - 2P TELP + INET'
                            WHEN (JENIS_PSB IN ('AO', 'PDA', 'PDA_LOCAL') AND TYPE_LAYANAN = '2P' AND FLAG_DEPOSIT != 'Deposit BYOD' AND PRODUCT = 'INDIHOME' AND POTS = '' AND SPEEDY != '') THEN '0P - 2P INET + IPTV'
                            WHEN (JENIS_PSB IN ('AO', 'PDA', 'PDA_LOCAL') AND TYPE_LAYANAN = '3P' AND FLAG_DEPOSIT != 'Deposit BYOD' AND PRODUCT = 'INDIHOME') THEN '0P - 3P'
                            WHEN (JENIS_PSB IN ('AO', 'PDA', 'PDA_LOCAL') AND TYPE_LAYANAN = '1P' AND FLAG_DEPOSIT = 'Deposit BYOD' AND PRODUCT = 'INDIHOME') THEN '0P - 1P (BYOD)'
                            WHEN (JENIS_PSB IN ('AO', 'PDA', 'PDA_LOCAL') AND TYPE_LAYANAN = '2P' AND FLAG_DEPOSIT = 'Deposit BYOD' AND PRODUCT = 'INDIHOME' AND POTS != '' AND SPEEDY != '') THEN '0P - 2P TELP + INET (BYOD)'
                            WHEN (JENIS_PSB IN ('AO', 'PDA', 'PDA_LOCAL') AND TYPE_LAYANAN = '2P' AND FLAG_DEPOSIT = 'Deposit BYOD' AND PRODUCT = 'INDIHOME' AND POTS = '' AND SPEEDY != '') THEN '0P - 2P INET + IPTV (BYOD)'
                            WHEN (JENIS_PSB IN ('AO', 'PDA', 'PDA_LOCAL') AND TYPE_LAYANAN = '3P' AND FLAG_DEPOSIT = 'Deposit BYOD' AND PRODUCT = 'INDIHOME') THEN '0P - 3P (BYOD)'
                            WHEN (PRODUCT = 'WMS') THEN 'WMS'
                            WHEN (PRODUCT = 'WMS LITE') THEN 'WMS LITE'
                            WHEN (JENIS_PSB = 'MO' AND (KCONTACT LIKE '%ADDONSTB%' OR KCONTACT LIKE '%ADD STB%')) THEN 'ADDON STB'
                            WHEN (JENIS_PSB = 'MO' AND PACKAGE_NAME LIKE '%Biaya Sewa Second Set Top Box%') THEN 'SECOND STB'
                            WHEN (JENIS_PSB = 'MO' AND KCONTACT LIKE '%CHANGE STB%') THEN 'CHANGE STB'
                            WHEN (JENIS_PSB = 'MO' AND PACKAGE_NAME LIKE '%Biaya Sewa ONT Premium%') THEN 'ONT PREMIUM'
                            WHEN (JENIS_PSB = 'MO' AND (PACKAGE_NAME LIKE '%SWWIFIEXT%' OR KCONTACT LIKE '%Wifi Extender%')) THEN 'WIFI EXTENDER'
                            WHEN (JENIS_PSB = 'MO' AND PACKAGE_NAME LIKE '%Sewa Mesh WiFi%') THEN 'MESH WIFI'
                            WHEN (JENIS_PSB = 'MO' AND PACKAGE_NAME LIKE '%SWPLC%') THEN 'PLC'
                            WHEN (JENIS_PSB = 'MO' AND PACKAGE_NAME LIKE '%Sewa Perangkat Smart Camera%') THEN 'INDIHOME SMART CAMERA'
                        ELSE 'Layanan Tidak Diketahui' END
                    ) AS is_layanan,
                    SUM(CASE WHEN utm.id_barang IN ('AC-OF-SM-1B', 'AC-OF-SM-12-SC') THEN utm.volume ELSE 0 END) as dc_roll,
                    SUM(CASE WHEN utm.id_barang = 'Preconnectorized-1C-50-NonAcc' THEN utm.volume ELSE 0 END) as precon50,
                    SUM(CASE WHEN utm.id_barang = 'Preconnectorized-1C-70-NonAcc' THEN utm.volume ELSE 0 END) as precon70,
                    SUM(CASE WHEN utm.id_barang = 'Preconnectorized-1C-80-NonAcc' THEN utm.volume ELSE 0 END) as precon80,
                    SUM(CASE WHEN utm.id_barang = 'AD-SC' THEN utm.volume ELSE 0 END) as adaptor_sc,
                    SUM(CASE WHEN utm.id_barang = 'OTP-FTTH-1' THEN utm.volume ELSE 0 END) as otp_ftth,
                    SUM(CASE WHEN utm.id_barang = 'PREKSO-INTRA-15-RS' THEN utm.volume ELSE 0 END) as prekso15,
                    SUM(CASE WHEN utm.id_barang = 'PREKSO-INTRA-20-RS' THEN utm.volume ELSE 0 END) as prekso20,
                    SUM(CASE WHEN utm.id_barang = 'PC-SC-SC-10' THEN utm.volume ELSE 0 END) as patchcord10,
                    SUM(CASE WHEN utm.id_barang = 'PC-SC-SC-15' THEN utm.volume ELSE 0 END) as patchcord15,
                    SUM(CASE WHEN utm.id_barang = 'UTP-C6' THEN utm.volume ELSE 0 END) as utpc6,
                    SUM(CASE WHEN utm.id_barang = 'PU-S7.0-140' THEN utm.volume ELSE 0 END) as tiang7,
                    SUM(CASE WHEN utm.id_barang = 'PU-S9.0-140' THEN utm.volume ELSE 0 END) as tiang9,
                    SUM(CASE WHEN utm.id_barang = 'TC-2-160' THEN utm.volume ELSE 0 END) as traycable,
                    SUM(CASE WHEN utm.id_barang = 'BREKET-A' THEN utm.volume ELSE 0 END) as breket,
                    SUM(CASE WHEN utm.id_barang = 'S-Clamp-Spriner' THEN utm.volume ELSE 0 END) as sclamp,
                    SUM(CASE WHEN utm.id_barang = 'RJ45-5' THEN utm.volume ELSE 0 END) as rj45t5,
                    SUM(CASE WHEN utm.id_barang = 'RJ45-6' THEN utm.volume ELSE 0 END) as rj45t6,
                    SUM(CASE WHEN utm.id_barang = 'SOC-ILS' THEN utm.volume ELSE 0 END) as soc_ils,
                    SUM(CASE WHEN utm.id_barang = 'SOC-SUM' THEN utm.volume ELSE 0 END) as soc_sum
                "))
                ->where('kpt.WITEL', $witel)
                ->whereDate('kpt.LAST_UPDATED_DATE', $day)
                ->where(function ($query) {
                    $query->where("kpt.STATUS_RESUME", "Completed (PS)")
                    ->orWhere("kpt.STATUS_MESSAGE", "Completed");
                })
                ->whereNull("psc.sc_id")
                ->groupBy("kpt.ORDER_ID")
                ->get();

            break;

            case 'ORDER':

                $data = DB::table("kpro_tr6 AS kpt")
                ->leftJoin('procurement_hss_psb AS phss', 'kpt.WITEL', '=', 'phss.witel_ms2n')
                ->leftJoin("utonline_tr6 AS utt", "kpt.SPEEDY", "=", "utt.noInternet")
                ->leftJoin("utonline_material AS utm", "utt.order_id", "=", "utm.id_order")
                ->leftJoin("procurement_psb_cutoff AS psc", "kpt.ORDER_ID", "=", "psc.sc_id")
                ->select(DB::raw("
                    kpt.*,
                    null AS mitra,
                    utt.statusName as utt_statusName,
                    utt.qcStatusName as utt_qcStatusName,
                    utt.create_dtm as utt_create_dtm,
                    (
                        CASE
                            WHEN (JENIS_PSB IN ('AO', 'PDA', 'PDA_LOCAL') AND TYPE_LAYANAN = '1P' AND FLAG_DEPOSIT != 'Deposit BYOD' AND PRODUCT = 'INDIHOME') THEN '0P - 1P'
                            WHEN (JENIS_PSB IN ('AO', 'PDA', 'PDA_LOCAL') AND TYPE_LAYANAN = '2P' AND FLAG_DEPOSIT != 'Deposit BYOD' AND PRODUCT = 'INDIHOME' AND POTS != '' AND SPEEDY != '') THEN '0P - 2P TELP + INET'
                            WHEN (JENIS_PSB IN ('AO', 'PDA', 'PDA_LOCAL') AND TYPE_LAYANAN = '2P' AND FLAG_DEPOSIT != 'Deposit BYOD' AND PRODUCT = 'INDIHOME' AND POTS = '' AND SPEEDY != '') THEN '0P - 2P INET + IPTV'
                            WHEN (JENIS_PSB IN ('AO', 'PDA', 'PDA_LOCAL') AND TYPE_LAYANAN = '3P' AND FLAG_DEPOSIT != 'Deposit BYOD' AND PRODUCT = 'INDIHOME') THEN '0P - 3P'
                            WHEN (JENIS_PSB IN ('AO', 'PDA', 'PDA_LOCAL') AND TYPE_LAYANAN = '1P' AND FLAG_DEPOSIT = 'Deposit BYOD' AND PRODUCT = 'INDIHOME') THEN '0P - 1P (BYOD)'
                            WHEN (JENIS_PSB IN ('AO', 'PDA', 'PDA_LOCAL') AND TYPE_LAYANAN = '2P' AND FLAG_DEPOSIT = 'Deposit BYOD' AND PRODUCT = 'INDIHOME' AND POTS != '' AND SPEEDY != '') THEN '0P - 2P TELP + INET (BYOD)'
                            WHEN (JENIS_PSB IN ('AO', 'PDA', 'PDA_LOCAL') AND TYPE_LAYANAN = '2P' AND FLAG_DEPOSIT = 'Deposit BYOD' AND PRODUCT = 'INDIHOME' AND POTS = '' AND SPEEDY != '') THEN '0P - 2P INET + IPTV (BYOD)'
                            WHEN (JENIS_PSB IN ('AO', 'PDA', 'PDA_LOCAL') AND TYPE_LAYANAN = '3P' AND FLAG_DEPOSIT = 'Deposit BYOD' AND PRODUCT = 'INDIHOME') THEN '0P - 3P (BYOD)'
                            WHEN (PRODUCT = 'WMS') THEN 'WMS'
                            WHEN (PRODUCT = 'WMS LITE') THEN 'WMS LITE'
                            WHEN (JENIS_PSB = 'MO' AND (KCONTACT LIKE '%ADDONSTB%' OR KCONTACT LIKE '%ADD STB%')) THEN 'ADDON STB'
                            WHEN (JENIS_PSB = 'MO' AND PACKAGE_NAME LIKE '%Biaya Sewa Second Set Top Box%') THEN 'SECOND STB'
                            WHEN (JENIS_PSB = 'MO' AND KCONTACT LIKE '%CHANGE STB%') THEN 'CHANGE STB'
                            WHEN (JENIS_PSB = 'MO' AND PACKAGE_NAME LIKE '%Biaya Sewa ONT Premium%') THEN 'ONT PREMIUM'
                            WHEN (JENIS_PSB = 'MO' AND (PACKAGE_NAME LIKE '%SWWIFIEXT%' OR KCONTACT LIKE '%Wifi Extender%')) THEN 'WIFI EXTENDER'
                            WHEN (JENIS_PSB = 'MO' AND PACKAGE_NAME LIKE '%Sewa Mesh WiFi%') THEN 'MESH WIFI'
                            WHEN (JENIS_PSB = 'MO' AND PACKAGE_NAME LIKE '%SWPLC%') THEN 'PLC'
                            WHEN (JENIS_PSB = 'MO' AND PACKAGE_NAME LIKE '%Sewa Perangkat Smart Camera%') THEN 'INDIHOME SMART CAMERA'
                        ELSE 'Layanan Tidak Diketahui' END
                    ) AS is_layanan,
                    SUM(CASE WHEN utm.id_barang IN ('AC-OF-SM-1B', 'AC-OF-SM-12-SC') THEN utm.volume ELSE 0 END) as dc_roll,
                    SUM(CASE WHEN utm.id_barang = 'Preconnectorized-1C-50-NonAcc' THEN utm.volume ELSE 0 END) as precon50,
                    SUM(CASE WHEN utm.id_barang = 'Preconnectorized-1C-70-NonAcc' THEN utm.volume ELSE 0 END) as precon70,
                    SUM(CASE WHEN utm.id_barang = 'Preconnectorized-1C-80-NonAcc' THEN utm.volume ELSE 0 END) as precon80,
                    SUM(CASE WHEN utm.id_barang = 'AD-SC' THEN utm.volume ELSE 0 END) as adaptor_sc,
                    SUM(CASE WHEN utm.id_barang = 'OTP-FTTH-1' THEN utm.volume ELSE 0 END) as otp_ftth,
                    SUM(CASE WHEN utm.id_barang = 'PREKSO-INTRA-15-RS' THEN utm.volume ELSE 0 END) as prekso15,
                    SUM(CASE WHEN utm.id_barang = 'PREKSO-INTRA-20-RS' THEN utm.volume ELSE 0 END) as prekso20,
                    SUM(CASE WHEN utm.id_barang = 'PC-SC-SC-10' THEN utm.volume ELSE 0 END) as patchcord10,
                    SUM(CASE WHEN utm.id_barang = 'PC-SC-SC-15' THEN utm.volume ELSE 0 END) as patchcord15,
                    SUM(CASE WHEN utm.id_barang = 'UTP-C6' THEN utm.volume ELSE 0 END) as utpc6,
                    SUM(CASE WHEN utm.id_barang = 'PU-S7.0-140' THEN utm.volume ELSE 0 END) as tiang7,
                    SUM(CASE WHEN utm.id_barang = 'PU-S9.0-140' THEN utm.volume ELSE 0 END) as tiang9,
                    SUM(CASE WHEN utm.id_barang = 'TC-2-160' THEN utm.volume ELSE 0 END) as traycable,
                    SUM(CASE WHEN utm.id_barang = 'BREKET-A' THEN utm.volume ELSE 0 END) as breket,
                    SUM(CASE WHEN utm.id_barang = 'S-Clamp-Spriner' THEN utm.volume ELSE 0 END) as sclamp,
                    SUM(CASE WHEN utm.id_barang = 'RJ45-5' THEN utm.volume ELSE 0 END) as rj45t5,
                    SUM(CASE WHEN utm.id_barang = 'RJ45-6' THEN utm.volume ELSE 0 END) as rj45t6,
                    SUM(CASE WHEN utm.id_barang = 'SOC-ILS' THEN utm.volume ELSE 0 END) as soc_ils,
                    SUM(CASE WHEN utm.id_barang = 'SOC-SUM' THEN utm.volume ELSE 0 END) as soc_sum
                "))
                ->whereBetween("kpt.LAST_UPDATED_DATE", [$startDate, $endDate])
                ->where(function ($query) {
                    $query->where("kpt.STATUS_RESUME", "Completed (PS)")
                    ->orWhere("kpt.STATUS_MESSAGE", "Completed");
                })
                ->whereNull("psc.sc_id");

                switch ($witel) {
                    case 'ALL':
                        # code...
                        break;
                    
                    default:
                            $data->where('kpt.WITEL', $witel);
                        break;
                }

                switch ($layanan)
                {
                    case 'p1':
                            $data->whereRaw('kpt.JENIS_PSB IN ("AO", "PDA", "PDA_LOCAL") AND kpt.TYPE_LAYANAN = "1P" AND kpt.FLAG_DEPOSIT != "Deposit BYOD" AND kpt.PRODUCT = "INDIHOME"');
                        break;
                    case 'p2_tlpint':
                            $data->whereRaw('kpt.JENIS_PSB IN ("AO", "PDA", "PDA_LOCAL") AND kpt.TYPE_LAYANAN = "2P" AND kpt.FLAG_DEPOSIT != "Deposit BYOD" AND kpt.PRODUCT = "INDIHOME" AND kpt.POTS != "" AND kpt.SPEEDY != ""');
                        break;
                    case 'p2_intiptv':
                            $data->whereRaw('kpt.JENIS_PSB IN ("AO", "PDA", "PDA_LOCAL") AND kpt.TYPE_LAYANAN = "2P" AND kpt.FLAG_DEPOSIT != "Deposit BYOD" AND kpt.PRODUCT = "INDIHOME" AND kpt.POTS = "" AND kpt.SPEEDY != ""');
                        break;
                    case 'p3':
                            $data->whereRaw('kpt.JENIS_PSB IN ("AO", "PDA", "PDA_LOCAL") AND kpt.TYPE_LAYANAN = "3P" AND kpt.FLAG_DEPOSIT != "Deposit BYOD" AND kpt.PRODUCT = "INDIHOME"');
                        break;
                    case 'p1_survey':
                            $data->whereRaw('kpt.JENIS_PSB IN ("AO", "PDA", "PDA_LOCAL") AND kpt.TYPE_LAYANAN = "1P" AND kpt.FLAG_DEPOSIT = "Deposit BYOD" AND kpt.PRODUCT = "INDIHOME"');
                        break;
                    case 'p2_tlpint_survey':
                            $data->whereRaw('kpt.JENIS_PSB IN ("AO", "PDA", "PDA_LOCAL") AND kpt.TYPE_LAYANAN = "2P" AND kpt.FLAG_DEPOSIT = "Deposit BYOD" AND kpt.PRODUCT = "INDIHOME" AND kpt.POTS != "" AND kpt.SPEEDY != ""');
                        break;
                    case 'p2_intiptv_survey':
                            $data->whereRaw('kpt.JENIS_PSB IN ("AO", "PDA", "PDA_LOCAL") AND kpt.TYPE_LAYANAN = "2P" AND kpt.FLAG_DEPOSIT = "Deposit BYOD" AND kpt.PRODUCT = "INDIHOME" AND kpt.POTS = "" AND kpt.SPEEDY != ""');
                        break;
                    case 'p3_survey':
                            $data->whereRaw('kpt.JENIS_PSB IN ("AO", "PDA", "PDA_LOCAL") AND kpt.TYPE_LAYANAN = "3P" AND kpt.FLAG_DEPOSIT = "Deposit BYOD" AND kpt.PRODUCT = "INDIHOME"');
                        break;
                    case 'wms':
                            $data->whereRaw('kpt.PRODUCT = "WMS"');
                        break;
                    case 'wms_lite':
                            $data->whereRaw('kpt.PRODUCT = "WMS LITE"');
                        break;
                    case 'addon_stb':
                            $data->whereRaw('kpt.JENIS_PSB = "MO" AND (kpt.KCONTACT LIKE "%ADDONSTB%" OR kpt.KCONTACT LIKE "%ADD STB%")');
                        break;
                    case 'second_stb':
                            $data->whereRaw('kpt.JENIS_PSB = "MO" AND kpt.PACKAGE_NAME LIKE "%Biaya Sewa Second Set Top Box%"');
                        break;
                    case 'change_stb':
                            $data->whereRaw('kpt.JENIS_PSB = "MO" AND kpt.KCONTACT LIKE "%CHANGE STB%"');
                        break;
                    case 'ont_premium':
                            $data->whereRaw('kpt.JENIS_PSB = "MO" AND kpt.PACKAGE_NAME LIKE "%Biaya Sewa ONT Premium%"');
                        break;
                    case 'wifiext':
                            $data->whereRaw('kpt.JENIS_PSB = "MO" AND (kpt.PACKAGE_NAME LIKE "%SWWIFIEXT%" OR kpt.KCONTACT LIKE "%Wifi Extender%")');
                        break;
                    case 'meshwifi':
                            $data->whereRaw('kpt.JENIS_PSB = "MO" AND kpt.PACKAGE_NAME LIKE "%Sewa Mesh WiFi%"');
                        break;
                    case 'plc':
                            $data->whereRaw('kpt.JENIS_PSB = "MO" AND kpt.PACKAGE_NAME LIKE "%SWPLC%"');
                        break;
                    case 'indihome_smart':
                            $data->whereRaw('kpt.JENIS_PSB = "MO" AND kpt.PACKAGE_NAME LIKE "%Sewa Perangkat Smart Camera%"');
                        break;
                }

                return $data->groupBy("kpt.ORDER_ID")->get();
                    
            break;

            case 'ORDER_CUTOFF':

                $data = DB::table("kpro_tr6 AS kpt")
                ->leftJoin('procurement_hss_psb AS phss', 'kpt.WITEL', '=', 'phss.witel_ms2n')
                ->leftJoin("utonline_tr6 AS utt", "kpt.SPEEDY", "=", "utt.noInternet")
                ->leftJoin("utonline_material AS utm", "utt.order_id", "=", "utm.id_order")
                ->leftJoin("procurement_psb_cutoff AS psc", "kpt.ORDER_ID", "=", "psc.sc_id")
                ->leftJoin("procurement_mitra AS pm", "psc.mitra_id", "=", "pm.id")
                ->select(DB::raw("
                    kpt.*,
                    pm.nama_company AS mitra,
                    utt.statusName as utt_statusName,
                    utt.qcStatusName as utt_qcStatusName,
                    utt.create_dtm as utt_create_dtm,
                    (
                        CASE
                            WHEN (JENIS_PSB IN ('AO', 'PDA', 'PDA_LOCAL') AND TYPE_LAYANAN = '1P' AND FLAG_DEPOSIT != 'Deposit BYOD' AND PRODUCT = 'INDIHOME') THEN '0P - 1P'
                            WHEN (JENIS_PSB IN ('AO', 'PDA', 'PDA_LOCAL') AND TYPE_LAYANAN = '2P' AND FLAG_DEPOSIT != 'Deposit BYOD' AND PRODUCT = 'INDIHOME' AND POTS != '' AND SPEEDY != '') THEN '0P - 2P TELP + INET'
                            WHEN (JENIS_PSB IN ('AO', 'PDA', 'PDA_LOCAL') AND TYPE_LAYANAN = '2P' AND FLAG_DEPOSIT != 'Deposit BYOD' AND PRODUCT = 'INDIHOME' AND POTS = '' AND SPEEDY != '') THEN '0P - 2P INET + IPTV'
                            WHEN (JENIS_PSB IN ('AO', 'PDA', 'PDA_LOCAL') AND TYPE_LAYANAN = '3P' AND FLAG_DEPOSIT != 'Deposit BYOD' AND PRODUCT = 'INDIHOME') THEN '0P - 3P'
                            WHEN (JENIS_PSB IN ('AO', 'PDA', 'PDA_LOCAL') AND TYPE_LAYANAN = '1P' AND FLAG_DEPOSIT = 'Deposit BYOD' AND PRODUCT = 'INDIHOME') THEN '0P - 1P (BYOD)'
                            WHEN (JENIS_PSB IN ('AO', 'PDA', 'PDA_LOCAL') AND TYPE_LAYANAN = '2P' AND FLAG_DEPOSIT = 'Deposit BYOD' AND PRODUCT = 'INDIHOME' AND POTS != '' AND SPEEDY != '') THEN '0P - 2P TELP + INET (BYOD)'
                            WHEN (JENIS_PSB IN ('AO', 'PDA', 'PDA_LOCAL') AND TYPE_LAYANAN = '2P' AND FLAG_DEPOSIT = 'Deposit BYOD' AND PRODUCT = 'INDIHOME' AND POTS = '' AND SPEEDY != '') THEN '0P - 2P INET + IPTV (BYOD)'
                            WHEN (JENIS_PSB IN ('AO', 'PDA', 'PDA_LOCAL') AND TYPE_LAYANAN = '3P' AND FLAG_DEPOSIT = 'Deposit BYOD' AND PRODUCT = 'INDIHOME') THEN '0P - 3P (BYOD)'
                            WHEN (PRODUCT = 'WMS') THEN 'WMS'
                            WHEN (PRODUCT = 'WMS LITE') THEN 'WMS LITE'
                            WHEN (JENIS_PSB = 'MO' AND (KCONTACT LIKE '%ADDONSTB%' OR KCONTACT LIKE '%ADD STB%')) THEN 'ADDON STB'
                            WHEN (JENIS_PSB = 'MO' AND PACKAGE_NAME LIKE '%Biaya Sewa Second Set Top Box%') THEN 'SECOND STB'
                            WHEN (JENIS_PSB = 'MO' AND KCONTACT LIKE '%CHANGE STB%') THEN 'CHANGE STB'
                            WHEN (JENIS_PSB = 'MO' AND PACKAGE_NAME LIKE '%Biaya Sewa ONT Premium%') THEN 'ONT PREMIUM'
                            WHEN (JENIS_PSB = 'MO' AND (PACKAGE_NAME LIKE '%SWWIFIEXT%' OR KCONTACT LIKE '%Wifi Extender%')) THEN 'WIFI EXTENDER'
                            WHEN (JENIS_PSB = 'MO' AND PACKAGE_NAME LIKE '%Sewa Mesh WiFi%') THEN 'MESH WIFI'
                            WHEN (JENIS_PSB = 'MO' AND PACKAGE_NAME LIKE '%SWPLC%') THEN 'PLC'
                            WHEN (JENIS_PSB = 'MO' AND PACKAGE_NAME LIKE '%Sewa Perangkat Smart Camera%') THEN 'INDIHOME SMART CAMERA'
                        ELSE 'Layanan Tidak Diketahui' END
                    ) AS is_layanan,
                    SUM(CASE WHEN utm.id_barang IN ('AC-OF-SM-1B', 'AC-OF-SM-12-SC') THEN utm.volume ELSE 0 END) as dc_roll,
                    SUM(CASE WHEN utm.id_barang = 'Preconnectorized-1C-50-NonAcc' THEN utm.volume ELSE 0 END) as precon50,
                    SUM(CASE WHEN utm.id_barang = 'Preconnectorized-1C-70-NonAcc' THEN utm.volume ELSE 0 END) as precon70,
                    SUM(CASE WHEN utm.id_barang = 'Preconnectorized-1C-80-NonAcc' THEN utm.volume ELSE 0 END) as precon80,
                    SUM(CASE WHEN utm.id_barang = 'AD-SC' THEN utm.volume ELSE 0 END) as adaptor_sc,
                    SUM(CASE WHEN utm.id_barang = 'OTP-FTTH-1' THEN utm.volume ELSE 0 END) as otp_ftth,
                    SUM(CASE WHEN utm.id_barang = 'PREKSO-INTRA-15-RS' THEN utm.volume ELSE 0 END) as prekso15,
                    SUM(CASE WHEN utm.id_barang = 'PREKSO-INTRA-20-RS' THEN utm.volume ELSE 0 END) as prekso20,
                    SUM(CASE WHEN utm.id_barang = 'PC-SC-SC-10' THEN utm.volume ELSE 0 END) as patchcord10,
                    SUM(CASE WHEN utm.id_barang = 'PC-SC-SC-15' THEN utm.volume ELSE 0 END) as patchcord15,
                    SUM(CASE WHEN utm.id_barang = 'UTP-C6' THEN utm.volume ELSE 0 END) as utpc6,
                    SUM(CASE WHEN utm.id_barang = 'PU-S7.0-140' THEN utm.volume ELSE 0 END) as tiang7,
                    SUM(CASE WHEN utm.id_barang = 'PU-S9.0-140' THEN utm.volume ELSE 0 END) as tiang9,
                    SUM(CASE WHEN utm.id_barang = 'TC-2-160' THEN utm.volume ELSE 0 END) as traycable,
                    SUM(CASE WHEN utm.id_barang = 'BREKET-A' THEN utm.volume ELSE 0 END) as breket,
                    SUM(CASE WHEN utm.id_barang = 'S-Clamp-Spriner' THEN utm.volume ELSE 0 END) as sclamp,
                    SUM(CASE WHEN utm.id_barang = 'RJ45-5' THEN utm.volume ELSE 0 END) as rj45t5,
                    SUM(CASE WHEN utm.id_barang = 'RJ45-6' THEN utm.volume ELSE 0 END) as rj45t6,
                    SUM(CASE WHEN utm.id_barang = 'SOC-ILS' THEN utm.volume ELSE 0 END) as soc_ils,
                    SUM(CASE WHEN utm.id_barang = 'SOC-SUM' THEN utm.volume ELSE 0 END) as soc_sum
                "))
                ->whereBetween("kpt.LAST_UPDATED_DATE", [$startDate, $endDate])
                ->where(function ($query) {
                    $query->where("kpt.STATUS_RESUME", "Completed (PS)")
                    ->orWhere("kpt.STATUS_MESSAGE", "Completed");
                })
                ->whereNotNull("psc.sc_id");

                switch ($witel) {
                    case 'ALL':
                        # code...
                        break;
                    
                    default:
                            $data->where('kpt.WITEL', $witel);
                        break;
                }

                switch ($mitra) {
                    case 'ALL':
                        # code...
                        break;
                    
                    default:
                            $data->where('pm.nama_company', $mitra);
                        break;
                }

                switch ($layanan)
                {
                    case 'p1':
                        $data->whereRaw('psc.layanan_fix = "0P - 1P"');
                    break;
                    case 'p2_tlpint':
                            $data->whereRaw('psc.layanan_fix = "0P - 2P TELP + INET"');
                        break;
                    case 'p2_intiptv':
                            $data->whereRaw('psc.layanan_fix = "0P - 2P INET + IPTV"');
                        break;
                    case 'p3':
                            $data->whereRaw('psc.layanan_fix = "0P - 3P"');
                        break;
                    case 'p1_survey':
                            $data->whereRaw('psc.layanan_fix = "0P - 1P (BYOD)"');
                        break;
                    case 'p2_tlpint_survey':
                            $data->whereRaw('psc.layanan_fix = "0P - 2P TELP + INET (BYOD)"');
                        break;
                    case 'p2_intiptv_survey':
                            $data->whereRaw('psc.layanan_fix = "0P - 2P INET + IPTV (BYOD)"');
                        break;
                    case 'p3_survey':
                            $data->whereRaw('psc.layanan_fix = "0P - 3P (BYOD)"');
                        break;
                    case 'wms':
                            $data->whereRaw('psc.layanan_fix = "WMS"');
                        break;
                    case 'wms_lite':
                            $data->whereRaw('psc.layanan_fix = "WMS LITE"');
                        break;
                    case 'addon_stb':
                            $data->whereRaw('psc.layanan_fix = "ADDON STB"');
                        break;
                    case 'second_stb':
                            $data->whereRaw('psc.layanan_fix = "SECOND STB"');
                        break;
                    case 'change_stb':
                            $data->whereRaw('psc.layanan_fix = "CHANGE STB"');
                        break;
                    case 'ont_premium':
                            $data->whereRaw('psc.layanan_fix = "ONT PREMIUM"');
                        break;
                    case 'wifiext':
                            $data->whereRaw('psc.layanan_fix = "WIFI EXTENDER"');
                        break;
                    case 'meshwifi':
                            $data->whereRaw('psc.layanan_fix = "MESH WIFI"');
                        break;
                    case 'plc':
                            $data->whereRaw('psc.layanan_fix = "PLC"');
                        break;
                    case 'indihome_smart':
                            $data->whereRaw('psc.layanan_fix = "INDIHOME SMART CAMERA"');
                        break;
                }

                return $data->groupBy("kpt.ORDER_ID")->get();
                    
            break;
        }
    }

    public static function provCutoffOrderBasedSave($req)
    {
        $periode_kegiatan = $req->input('periode_kegiatan');
        $rekon_periode1   = $req->input('rekon_periode1');
        $rekon_periode2   = $req->input('rekon_periode2');
        $regional         = str_replace('REG','', $req->input('regional'));
        $witel            = $req->input('witel');
        $id_mitra         = $req->input('id_mitra');
        $mitra            = DB::table('procurement_mitra')->where([ ['id_regional', $regional], ['tacticalpro_mitra_id', $id_mitra] ])->first();

        $null_kpro = $duplicate_cutoff = null;
        $insert[] = $message_response[] = [];

        $load  = Excel::toArray([], $req->file('file_cutoff'));
        if (count($load) == 1)
        {
            $get_sc = $load[0];
            unset($get_sc[0]);

            foreach ($get_sc as $k => $v)
            {
                $order_id = str_replace('.0', '', $v[0]);
                $layanan  = $v[1];

                if ($order_id != "")
                {
                    $check_layanan = self::provCheckOrder($order_id);
                    if ($check_layanan == null)
                    {
                        $layanan_fix = 'Layanan Tidak Diketahui';
                    }
                    else
                    {
                        $layanan_fix = $check_layanan->is_layanan;
                    }

                    $check_kpro = DB::table('kpro_tr6')->where('ORDER_ID', $order_id)->first();
                    if ($check_kpro == null)
                    {
                        $null_kpro .= "$order_id; ";
                        $message_response[] = ['order_id' => $order_id, 'is_check' => 2, 'layanan_upload' => $layanan, 'layanan_fix' => $layanan_fix];
                    }
                    else
                    {
                        $check = DB::table('procurement_psb_cutoff')->where('sc_id', $order_id)->first();

                        if ($check == null)
                        {
                            $insert[] = [
                                'id_regional'      => $regional,
                                'witel'            => $witel,
                                'mitra_id'         => $mitra->id,
                                'tahun_kegiatan'   => date('Y'),
                                'periode_kegiatan' => $periode_kegiatan,
                                'periode1'         => $rekon_periode1,
                                'periode2'         => $rekon_periode2,
                                'mitra_id'         => $mitra->id,
                                'sc_id'            => $order_id,
                                'layanan'          => $layanan,
                                'layanan_fix'      => $layanan_fix,
                                'tgl_ps'           => $check_kpro->LAST_UPDATED_DATE,
                                'created_by'       => session('auth')->id_user
                            ];

                            $message_response[] = ['order_id' => $order_id, 'is_check' => 1, 'layanan_upload' => $layanan, 'layanan_fix' => $layanan_fix];
                        }
                        else
                        {
                            $duplicate_cutoff .= "$order_id; ";
                            $message_response[] = ['order_id' => $order_id, 'is_check' => 3, 'layanan_upload' => $layanan, 'layanan_fix' => $layanan_fix];
                        }
                    }
                }
            }
        }

        if ($null_kpro == null && $duplicate_cutoff == null)
        {
            if (array_filter($insert))
            {
                $chunk = array_chunk(array_filter($insert), 500);
                foreach($chunk as $data)
                {
                    DB::table('procurement_psb_cutoff')->insert($data);
                }
            }
        }

        return $message_response;
    }

    public static function provCreateOrderBasedSave($req)
    {
        $bulan     = DB::table('bulan_format')->where('id', $req->input('periode_kegiatan'))->first();
		$mitra     = DB::table('procurement_mitra')->where([ ['id_regional', $req->input('id_regional')], ['tacticalpro_mitra_id', $req->input('id_mitra')] ])->first();
		$hss_witel = DB::table('procurement_hss_psb')->where('witel_ms2n', strtoupper($mitra->area))->first();
        $fix_rekon = self::provRekonCutoffOrderBased($req->input('id_regional'), strtoupper($mitra->area), $mitra->id, date('Y'), $req->input('periode_kegiatan'));

		// dd($req->all(), $bulan, $mitra, $hss_witel, $fix_rekon);

		$jml_pekerjaan_psb =
			($fix_rekon->p1 * $hss_witel->p1) +
			($fix_rekon->p2_inet_voice * $hss_witel->p2_inet_voice) +
			($fix_rekon->p2_inet_iptv * $hss_witel->p2_inet_iptv) +
			($fix_rekon->p3 * $hss_witel->p3) +
			($fix_rekon->p1_ku * $hss_witel->p1_ku) +
			($fix_rekon->p2_inet_voice_ku * $hss_witel->p2_inet_voice_ku) +
			($fix_rekon->p2_inet_iptv_ku * $hss_witel->p2_inet_iptv_ku) +
			($fix_rekon->p3_ku * $hss_witel->p3_ku);

		$jml_pekerjaan_migrasi =
			(0 * $hss_witel->migrasi_1p) +
			(0 * $hss_witel->migrasi_2p) +
			(0 * $hss_witel->migrasi_3p);

		$jml_pekerjaan_tambahan =
			(($fix_rekon->wms + $fix_rekon->wms_lite) * $hss_witel->lme_wifi) +
			($fix_rekon->wms * $hss_witel->lme_ap_indoor) +
			(0 * $hss_witel->lme_ap_outdoor) +
			($fix_rekon->smartcam_only * $hss_witel->smartcam_only) +
			($fix_rekon->plc * $hss_witel->plc) +
			(($fix_rekon->addon_stb + $fix_rekon->second_stb) * $hss_witel->stb_tambahan) +
			($fix_rekon->ont_premium * $hss_witel->ont_premium) +
			($fix_rekon->wifiext * $hss_witel->wifiext) +
			(0 * $hss_witel->insert_tiang) +
			(0 * $hss_witel->insert_tiang_9);

		$total_sp = ($jml_pekerjaan_psb + $jml_pekerjaan_migrasi + $jml_pekerjaan_tambahan);

		if (in_array(date('m'), [1, 2, 3]))
		{
			// Q1
			$kpi_target_pspi = 96;
			$kpi_target_tticomply = 91;
		}
		elseif (in_array(date('m'), [4, 5, 6]))
		{
			// Q2
			$kpi_target_pspi = 97;
			$kpi_target_tticomply = 95;
		}
		elseif (in_array(date('m'), [7, 8, 9, 10, 11, 12]))
		{
			// Q3 Q4
			$kpi_target_pspi = 98;
			$kpi_target_tticomply = 99;
		};

		$kpi_pspi             = @round((str_replace(',', '.', $req->input('kpi_real_pspi')) / $kpi_target_pspi) * 100, 2);
		$kpi_tticomply        = @round((str_replace(',', '.', $req->input('kpi_real_tticomply')) / $kpi_target_tticomply) * 100, 2);
		$kpi_ffg              = @round((str_replace(',', '.', $req->input('kpi_real_ffg')) / 99) * 100, 2);
		$kpi_validasicore     = @round((str_replace(',', '.', $req->input('kpi_real_validasicore')) / 100) * 100, 2);
		$kpi_ttrffg           = @round((str_replace(',', '.', $req->input('kpi_real_ttrffg')) / 99) * 100, 2);
		$kpi_ujipetik         = @round((str_replace(',', '.', $req->input('kpi_real_ujipetik')) / 99) * 100, 2);
		$kpi_qc2              = @round((str_replace(',', '.', $req->input('kpi_real_qc2')) / 99) * 100, 2);

		$ach_kpi_pspi         = @round(($kpi_pspi * 25) / 100, 2);
		$ach_kpi_tticomply    = @round(($kpi_tticomply * 25) / 100, 2);
		$ach_kpi_ffg          = @round(($kpi_ffg * 15) / 100, 2);
		$ach_kpi_validasicore = @round(($kpi_validasicore * 5) / 100, 2);
		$ach_kpi_ttrffg       = @round(($kpi_ttrffg * 10) / 100, 2);
		$ach_kpi_ujipetik     = @round(($kpi_ujipetik * 10) / 100, 2);
		$ach_kpi_qc2          = @round(($kpi_qc2 * 10) / 100, 2);

		$ach_kpi              = ($ach_kpi_pspi + $ach_kpi_tticomply + $ach_kpi_ffg + $ach_kpi_validasicore + $ach_kpi_ttrffg + $ach_kpi_ujipetik + $ach_kpi_qc2);

		if ($ach_kpi > 100)
		{
			$kpi = 100;
		}
		elseif ($ach_kpi < 90)
		{
			$kpi = 90;
		}
		else
		{
			$kpi = $ach_kpi;
		};

		$id = DB::table('procurement_boq_upload')->insertGetId([
			'judul'              => "PEKERJAAN PASANG SAMBUNGAN BARU (PSB) PERIODE ".$bulan->bulan. " ".date('Y'). " WITEL ".strtoupper($mitra->witel)."",
			'pekerjaan'          => "PSB",
			'jenis_work'         => "Pasang Sambungan Baru (PSB)",
			'witel'              => strtoupper($mitra->witel),
			'step_id'            => 12,
			'mitra_id'           => $mitra->id,
			'mitra_nm'           => $mitra->nama_company,
			'tanggal_boq'        => $req->input('tanggal_terbit'),
			'toc'                => $req->input('toc'),
			'tgl_sp'             => $req->input('tgl_sp'),
			'no_sp'              => $req->input('no_sp'),
			'total_sp'           => $total_sp,
			'id_project'         => $req->input('id_project'),
			'pks'                => $req->input('pks'),
			'tgl_pks'            => $req->input('tgl_pks'),
			'masa_berlaku_pks'   => $req->input('masa_berlaku_pks'),
			'invoice'            => $req->input('invoice'),
			'spp_num'            => $req->input('spp_num'),
			'receipt_num'        => $req->input('receipt_num'),
			'faktur'             => $req->input('faktur'),
			'tgl_faktur'         => $req->input('tgl_faktur'),
			'surat_penetapan'    => $req->input('surat_penetapan'),
			'tgl_s_pen'          => $req->input('tgl_s_pen'),
			'surat_kesanggupan'  => $req->input('surat_kesanggupan'),
			'tgl_surat_sanggup'  => $req->input('tgl_surat_sanggup'),
			'no_baij'            => $req->input('no_baij'),
			'BAUT'               => $req->input('BAUT'),
			'tgl_baut'           => $req->input('rekon_periode2'),
			'BAST'               => $req->input('BAST'),
			'tgl_bast'           => $req->input('rekon_periode2'),
			'no_bap'             => $req->input('no_bap'),
			'BAPP'               => $req->input('BAPP'),
			'no_ba_pemeliharaan' => $req->input('no_ba_pemeliharaan'),
			'created_by'         => session('auth')->id_user,
			'modified_by'        => session('auth')->id_user,
			'verificator_by'     => session('auth')->id_user,
			'verificator_at'     => date('Y-m-d H:i:s')
		]);

		$desc = ['SP', 'BARIJ'];
		foreach ($desc as $value)
		{
			DB::table('procurement_nilai_psb')->insert([
				'id_dokumen'               => $id,
				'flag_dok_psb'             => 'beta_kalimantan',
				'desc'                     => $value,
				'periode_kegiatan'         => $req->input('periode_kegiatan'),
				'rekon_periode1'           => $req->input('rekon_periode1'),
				'rekon_periode2'           => $req->input('rekon_periode2'),
				'order_periode1'           => $req->input('order_periode1'),
				'order_periode2'           => $req->input('order_periode2'),
				'ppn_numb'                 => $req->input('ppn_numb'),
				'total_nilai_sp'           => $total_sp,
				'persentase_7_kpi'         => $kpi,
				'kpi_real_pspi'            => $req->input('kpi_real_pspi'),
				'kpi_real_tticomply'       => $req->input('kpi_real_tticomply'),
				'kpi_real_ffg'             => $req->input('kpi_real_ffg'),
				'kpi_real_validasicore'    => $req->input('kpi_real_validasicore'),
				'kpi_real_ttrffg'          => $req->input('kpi_real_ttrffg'),
				'kpi_real_ujipetik'        => $req->input('kpi_real_ujipetik'),
				'kpi_real_qc2'             => $req->input('kpi_real_qc2'),
				'p1_survey_hss'            => $hss_witel->p1 ?? 0,
				'p1_survey_ssl'            => $fix_rekon->p1 ?? 0,
				'p2_tlpint_survey_hss'     => $hss_witel->p2_inet_voice ?? 0,
				'p2_tlpint_survey_ssl'     => $fix_rekon->p2_inet_voice ?? 0,
				'p2_intiptv_survey_hss'    => $hss_witel->p2_inet_iptv ?? 0,
				'p2_intiptv_survey_ssl'    => $fix_rekon->p2_inet_iptv ?? 0,
				'p3_survey_hss'            => $hss_witel->p3 ?? 0,
				'p3_survey_ssl'            => $fix_rekon->p3 ?? 0,
				'p1_hss'                   => $hss_witel->p1_ku ?? 0,
				'p1_ssl'                   => $fix_rekon->p1_ku ?? 0,
				'p2_tlpint_hss'            => $hss_witel->p2_inet_voice_ku ?? 0,
				'p2_tlpint_ssl'            => $fix_rekon->p2_inet_voice_ku ?? 0,
				'p2_intiptv_hss'           => $hss_witel->p2_inet_iptv_ku ?? 0,
				'p2_intiptv_ssl'           => $fix_rekon->p2_inet_iptv_ku ?? 0,
				'p3_hss'                   => $hss_witel->p3_ku ?? 0,
				'p3_ssl'                   => $fix_rekon->p3_ku ?? 0,
				'migrasi_service_1p2p_hss' => $hss_witel->migrasi_1p ?? 0,
				'migrasi_service_1p2p_ssl' => 0,
				'migrasi_service_1p3p_hss' => $hss_witel->migrasi_2p ?? 0,
				'migrasi_service_1p3p_ssl' => 0,
				'migrasi_service_2p3p_hss' => $hss_witel->migrasi_3p ?? 0,
				'migrasi_service_2p3p_ssl' => 0,
				'ikr_addon_stb_hss'        => $hss_witel->stb_tambahan ?? 0,
				'ikr_addon_stb_ssl'        => ($fix_rekon->addon_stb + $fix_rekon->second_stb) ?? 0,
				'change_stb_hss'           => $hss_witel->change_stb ?? 0,
				'change_stb_ssl'           => $fix_rekon->change_stb ?? 0,
				'indihome_smart_hss'       => $hss_witel->smartcam_only ?? 0,
				'indihome_smart_ssl'       => $fix_rekon->smartcam_only ?? 0,
				'wifiextender_hss'         => $hss_witel->wifiext ?? 0,
				'wifiextender_ssl'         => $fix_rekon->wifiext ?? 0,
				'plc_hss'                  => $hss_witel->plc ?? 0,
				'plc_ssl'                  => $fix_rekon->plc ?? 0,
				'lme_wifi_pt1_hss'         => $hss_witel->lme_wifi ?? 0,
				'lme_wifi_pt1_ssl'         => ($fix_rekon->wms + $fix_rekon->wms_lite) ?? 0,
				'lme_ap_indoor_hss'        => $hss_witel->lme_ap_indoor ?? 0,
				'lme_ap_indoor_ssl'        => $fix_rekon->wms ?? 0,
				'lme_ap_outdoor_hss'       => $hss_witel->lme_ap_outdoor ?? 0,
				'lme_ap_outdoor_ssl'       => 0,
				'ont_premium_hss'          => $hss_witel->ont_premium ?? 0,
				'ont_premium_ssl'          => $fix_rekon->ont_premium ?? 0,
				'migrasi_stb_hss'          => 0,
				'migrasi_stb_ssl'          => 0,
				'instalasi_ipcam_hss'      => 0,
				'instalasi_ipcam_ssl'      => 0,
				'pu_s7_140_hss'            => $hss_witel->insert_tiang ?? 0,
				'pu_s7_140_ssl'            => 0,
				'pu_s9_140_hss'            => $hss_witel->insert_tiang_9 ?? 0,
				'pu_s9_140_ssl'            => 0,
				'created_by'               => session('auth')->id_user,
				'modified_by'              => session('auth')->id_user,
				'verificator_by'           => session('auth')->id_user,
				'verificator_at'           => date('Y-m-d H:i:s')
			]);
		}

		AdminModel::save_log(12, $id);

        return $id;
    }

    public static function provRekonCutoffOrderBased($regional, $witel, $mitra, $tahun, $periode)
    {
        return DB::table('procurement_psb_cutoff')
        ->select(DB::raw('
            SUM(CASE WHEN layanan_fix = "0P - 1P (BYOD)" THEN 1 ELSE 0 END) AS p1,
            SUM(CASE WHEN layanan_fix = "0P - 2P TELP + INET (BYOD)" THEN 1 ELSE 0 END) AS p2_inet_voice,
            SUM(CASE WHEN layanan_fix = "0P - 2P INET + IPTV (BYOD)" THEN 1 ELSE 0 END) AS p2_inet_iptv,
            SUM(CASE WHEN layanan_fix = "0P - 3P (BYOD)" THEN 1 ELSE 0 END) AS p3,
            SUM(CASE WHEN layanan_fix = "0P - 1P" THEN 1 ELSE 0 END) AS p1_ku,
            SUM(CASE WHEN layanan_fix = "0P - 2P TELP + INET" THEN 1 ELSE 0 END) AS p2_inet_voice_ku,
            SUM(CASE WHEN layanan_fix = "0P - 2P INET + IPTV" THEN 1 ELSE 0 END) AS p2_inet_iptv_ku,
            SUM(CASE WHEN layanan_fix = "0P - 3P" THEN 1 ELSE 0 END) AS p3_ku,
            SUM(CASE WHEN layanan_fix = "WMS" THEN 1 ELSE 0 END) AS wms,
            SUM(CASE WHEN layanan_fix = "WMS LITE" THEN 1 ELSE 0 END) AS wms_lite,
            SUM(CASE WHEN layanan_fix = "ADDON STB" THEN 1 ELSE 0 END) AS addon_stb,
            SUM(CASE WHEN layanan_fix = "SECOND STB" THEN 1 ELSE 0 END) AS second_stb,
            SUM(CASE WHEN layanan_fix = "CHANGE STB" THEN 1 ELSE 0 END) AS change_stb,
            SUM(CASE WHEN layanan_fix = "ONT PREMIUM" THEN 1 ELSE 0 END) AS ont_premium,
            SUM(CASE WHEN layanan_fix = "WIFI EXTENDER" THEN 1 ELSE 0 END) AS wifiext,
            SUM(CASE WHEN layanan_fix = "MESH WIFI" THEN 1 ELSE 0 END) AS meshwifi,
            SUM(CASE WHEN layanan_fix = "PLC" THEN 1 ELSE 0 END) AS plc,
            SUM(CASE WHEN layanan_fix = "INDIHOME SMART CAMERA" THEN 1 ELSE 0 END) AS smartcam_only
        '))
        ->where([
            ['id_regional', $regional],
            ['witel', $witel],
            ['mitra_id', $mitra],
            ['tahun_kegiatan', $tahun],
            ['periode_kegiatan', $periode]
        ])
        ->first();
    }

    public static function provCheckOrder($id)
    {
        return DB::table('kpro_tr6')
        ->select(DB::raw('
        (
            CASE
                WHEN (JENIS_PSB IN ("AO", "PDA", "PDA_LOCAL") AND TYPE_LAYANAN = "1P" AND FLAG_DEPOSIT != "Deposit BYOD" AND PRODUCT = "INDIHOME") THEN "0P - 1P"
                WHEN (JENIS_PSB IN ("AO", "PDA", "PDA_LOCAL") AND TYPE_LAYANAN = "2P" AND FLAG_DEPOSIT != "Deposit BYOD" AND PRODUCT = "INDIHOME" AND POTS != "" AND SPEEDY != "") THEN "0P - 2P TELP + INET"
                WHEN (JENIS_PSB IN ("AO", "PDA", "PDA_LOCAL") AND TYPE_LAYANAN = "2P" AND FLAG_DEPOSIT != "Deposit BYOD" AND PRODUCT = "INDIHOME" AND POTS = "" AND SPEEDY != "") THEN "0P - 2P INET + IPTV"
                WHEN (JENIS_PSB IN ("AO", "PDA", "PDA_LOCAL") AND TYPE_LAYANAN = "3P" AND FLAG_DEPOSIT != "Deposit BYOD" AND PRODUCT = "INDIHOME") THEN "0P - 3P"
                WHEN (JENIS_PSB IN ("AO", "PDA", "PDA_LOCAL") AND TYPE_LAYANAN = "1P" AND FLAG_DEPOSIT = "Deposit BYOD" AND PRODUCT = "INDIHOME") THEN "0P - 1P (BYOD)"
                WHEN (JENIS_PSB IN ("AO", "PDA", "PDA_LOCAL") AND TYPE_LAYANAN = "2P" AND FLAG_DEPOSIT = "Deposit BYOD" AND PRODUCT = "INDIHOME" AND POTS != "" AND SPEEDY != "") THEN "0P - 2P TELP + INET (BYOD)"
                WHEN (JENIS_PSB IN ("AO", "PDA", "PDA_LOCAL") AND TYPE_LAYANAN = "2P" AND FLAG_DEPOSIT = "Deposit BYOD" AND PRODUCT = "INDIHOME" AND POTS = "" AND SPEEDY != "") THEN "0P - 2P INET + IPTV (BYOD)"
                WHEN (JENIS_PSB IN ("AO", "PDA", "PDA_LOCAL") AND TYPE_LAYANAN = "3P" AND FLAG_DEPOSIT = "Deposit BYOD" AND PRODUCT = "INDIHOME") THEN "0P - 3P (BYOD)"
                WHEN (PRODUCT = "WMS") THEN "WMS"
                WHEN (PRODUCT = "WMS LITE") THEN "WMS LITE"
                WHEN (JENIS_PSB = "MO" AND (KCONTACT LIKE "%ADDONSTB%" OR KCONTACT LIKE "%ADD STB%")) THEN "ADDON STB"
                WHEN (JENIS_PSB = "MO" AND PACKAGE_NAME LIKE "%Biaya Sewa Second Set Top Box%") THEN "SECOND STB"
                WHEN (JENIS_PSB = "MO" AND KCONTACT LIKE "%CHANGE STB%") THEN "CHANGE STB"
                WHEN (JENIS_PSB = "MO" AND PACKAGE_NAME LIKE "%Biaya Sewa ONT Premium%") THEN "ONT PREMIUM"
                WHEN (JENIS_PSB = "MO" AND (PACKAGE_NAME LIKE "%SWWIFIEXT%" OR KCONTACT LIKE "%Wifi Extender%")) THEN "WIFI EXTENDER"
                WHEN (JENIS_PSB = "MO" AND PACKAGE_NAME LIKE "%Sewa Mesh WiFi%") THEN "MESH WIFI"
                WHEN (JENIS_PSB = "MO" AND PACKAGE_NAME LIKE "%SWPLC%") THEN "PLC"
                WHEN (JENIS_PSB = "MO" AND PACKAGE_NAME LIKE "%Sewa Perangkat Smart Camera%") THEN "INDIHOME SMART CAMERA"
            ELSE "Layanan Tidak Diketahui" END
        ) AS is_layanan
        '))
        ->where('ORDER_ID', $id)
        ->first();
    }

}
