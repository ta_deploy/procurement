<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;

date_default_timezone_set('Asia/Makassar');

class BotModel
{
    public static function sendMessage($chatID, $msg)
    {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://api.telegram.org/bot1902669622:AAHz0cl18e8-IF5waY1uQIj33Nxqq72EzFc/sendmessage?chat_id=' . $chatID . '&text=' . urlencode("$msg") . '&parse_mode=HTML',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;
    }
}
