<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DA\OperationModel;
use App\DA\CommerceModel;
use App\DA\AdminModel;
use App\DA\MitraModel;
use App\DA\ReportModel;
use App\DA\ToolsModel;
use DB;

date_default_timezone_set("Asia/Makassar");

class ProgressController extends Controller
{
  protected $lampiran_dokumen = ['Rekap_Tagihan' => 'Rekap Tagihan / Pekerjaan', 'Rekapitulasi_BA_Instalasi' => 'Rekapitulasi BA Instalasi', 'Laporan_Penyelesaian' => 'Laporan Penyelesaian / BA Digital', 'KPI' => 'KPI', 'KPI_Addons' => 'KPI Addons', 'BARM' => 'BARM', 'ENOFA' => 'E-NOFA', 'Pivot_RFC' => 'Pivot RFC', 'Checklist_RFC' => 'Checklist RFC', 'RFC' => 'RFC'];

  public function key_data()
  {
    $key = 'n%&^*'. (date('i') + date('d')) .'@'. date('Y$m#d&H%i') .'&'. (date('d') + date('H')) . 'n%&^*';
    return md5($key);
  }

	public function progress_step($id)
	{
    $data = ReportModel::get_boq_data($id);
    // $check_budget = ReportModel::get_pid_single_BE($id);
    $akses = false;
    $auth = session('auth')->proc_level;
    $check_validate = [];

    if($data)
    {
      $check_validate = AdminModel::check_step($data->step_id);
      $akses = in_array($auth, explode(',', $check_validate->aksesible) );

      if($auth == 2 && $data->mitra_nm != session('auth')->mitra_amija_pt)
      {
        $akses = false;
      }
    }
    else
    {
      if(in_array($auth, [3, 4, 44, 99] ) )
      {
        if(session('auth')->proc_level == 44)
        {
          $find_witel = DB::Table('promise_witel As w1')
          ->leftjoin('promise_witel As w2', 'w2.Regional', '=', 'w1.Regional')
          ->where('w1.Witel', session('auth')->Witel_New)
          ->get();

          foreach($find_witel as $vw)
          {
            $all_witel[] = $vw->Witel;
          }

          if(!in_array($data->witel, $all_witel) )
          {
            return back()->with('alerts', [
              ['type' => 'danger', 'text' => 'Gagal Karena Berbeda Regional!']
            ]);
          }
        }

        if($data && session('auth')->Witel_New != $data->witel && in_array(session('auth')->proc_level, [4]) )
        {
          return back()->with('alerts', [
            ['type' => 'danger', 'text' => 'Gagal Karena Berbeda Witel!']
          ]);
        }

        $akses = true;
      }
    }

    if( (is_null($data) || $data->active == 1 ) && $akses)
    {
      switch ($id) {
        case (preg_match('/[a-zA-Z]/', $id) && $id == 'input' ? TRUE : FALSE):
          $kerjaan        = ToolsModel::list_pekerjaan();
          $all_sto        = AdminModel::area_alamat();
          $design         = AdminModel::get_design(2);
          $last_data_work = AdminModel::get_last_work(['QE', 'Maintenance']);
          $last_data      = AdminModel::get_last_material_boq($last_data_work->id);
          $proaktif_all   = AdminModel::get_project_proaktif();
          $data_item = $data_material = $data_item_rw = [];

          // foreach($last_data as $val)
          // {
          //   $data_item[$val->id_boq_lokasi][] = $val;
          // }

          foreach($last_data as $k => $val)
          {
            $data_material[$val->id_design .'_'. $val->design_mitra_id]['id_design']       = $val->id_design;
            $data_material[$val->id_design .'_'. $val->design_mitra_id]['design_mitra_id'] = $val->design_mitra_id;
            $data_material[$val->id_design .'_'. $val->design_mitra_id]['jenis']           = $val->jenis;
            $data_material[$val->id_design .'_'. $val->design_mitra_id]['designator']      = $val->designator;
            $data_material[$val->id_design .'_'. $val->design_mitra_id]['uraian']          = html_entity_decode($val->uraian, ENT_QUOTES, 'UTF-8');
            $data_material[$val->id_design .'_'. $val->design_mitra_id]['jenis_khs']       = $val->jenis_khs;
            $data_material[$val->id_design .'_'. $val->design_mitra_id]['material']        = $val->material;
            $data_material[$val->id_design .'_'. $val->design_mitra_id]['jasa']            = $val->jasa;
            $data_material[$val->id_design .'_'. $val->design_mitra_id]['namcomp']         = $val->namcomp;
            $data_material[$val->id_design .'_'. $val->design_mitra_id]['sp']              = $val->sp;
            $data_material[$val->id_design .'_'. $val->design_mitra_id]['rekon']           = $val->rekon;
          }

          $data_material = array_values($data_material);

          foreach($last_data as $val)
          {
            if(!isset($data_item_rw[$val->id_boq_lokasi]) )
            {
              foreach($data_material as $k => $v)
              {
                $data_item_rw[$val->id_boq_lokasi][$v['id_design'] .'-'. $v['jenis_khs'] ] = (object)[
                  'id'              => 0,
                  'id_boq_lokasi'   => $val->id_boq_lokasi,
                  'id_design'       => $v['id_design'],
                  'jenis_khs'       => $v['jenis_khs'],
                  'designator'      => $v['designator'],
                  'material'        => $v['material'],
                  'jasa'            => $v['jasa'],
                  'sp'              => 0,
                  'rekon'           => 0,
                  'tambah'          => 0,
                  'kurang'          => 0,
                  'status_design'   => 0,
                  'jenis'           => $v['jenis'],
                  'namcomp'         => $v['namcomp'],
                  'design_mitra_id' => $v['design_mitra_id'],
                  'uraian'          => $v['uraian'],
                ];
              }
            }

            if($data_item_rw[$val->id_boq_lokasi][$val->id_design .'-'. $val->jenis_khs])
            {
              if($data_item_rw[$val->id_boq_lokasi][$val->id_design .'-'. $val->jenis_khs]->material != 0)
              {
                $data_item_rw[$val->id_boq_lokasi][$val->id_design .'-'. $val->jenis_khs]->material = $val->material_pd;
              }

              if($data_item_rw[$val->id_boq_lokasi][$val->id_design .'-'. $val->jenis_khs]->jasa != 0)
              {
                $data_item_rw[$val->id_boq_lokasi][$val->id_design .'-'. $val->jenis_khs]->jasa = $val->jasa_pd;
              }

              $data_item_rw[$val->id_boq_lokasi][$val->id_design .'-'. $val->jenis_khs]->id              = $val->id;
              $data_item_rw[$val->id_boq_lokasi][$val->id_design .'-'. $val->jenis_khs]->sp              = $val->sp;
              $data_item_rw[$val->id_boq_lokasi][$val->id_design .'-'. $val->jenis_khs]->rekon           = $val->rekon;
              $data_item_rw[$val->id_boq_lokasi][$val->id_design .'-'. $val->jenis_khs]->tambah          = $val->tambah;
              $data_item_rw[$val->id_boq_lokasi][$val->id_design .'-'. $val->jenis_khs]->kurang          = $val->kurang;
              $data_item_rw[$val->id_boq_lokasi][$val->id_design .'-'. $val->jenis_khs]->jenis           = $val->jenis;
              $data_item_rw[$val->id_boq_lokasi][$val->id_design .'-'. $val->jenis_khs]->namcomp         = $val->namcomp;
              $data_item_rw[$val->id_boq_lokasi][$val->id_design .'-'. $val->jenis_khs]->design_mitra_id = $val->design_mitra_id;
            }
          }

          foreach($data_item_rw as $k => $v)
          {
            $data_item[] = array_values($v);
          }

          $data_item = array_values($data_item);

          $split_bulan = explode('-', $last_data_work->bulan_pengerjaan);
          $mitra = ReportModel::active_mitra();
          return view('Operation.manual_boq', compact('kerjaan', 'all_sto', 'design', 'mitra', 'data_item', 'data_material', 'proaktif_all'), ['bulan' => $split_bulan[1], 'tahun' => $split_bulan[0]] );
        break;
        case (in_array($data->step_id, [33]) ? TRUE : FALSE):
          $kerjaan        = ToolsModel::list_pekerjaan();
          $all_sto        = AdminModel::area_alamat();
          $design         = AdminModel::get_design(2);
          $last_data_work = AdminModel::get_data_final($id);
          $last_data      = AdminModel::get_last_material_boq($id);
          $proaktif_all   = AdminModel::get_project_proaktif();
          $data_item = $data_material = $data_item_rw = [];

          // foreach($last_data as $val)
          // {
          //   $data_item[$val->id_boq_lokasi][] = $val;
          // }

          foreach($last_data as $k => $val)
          {
            $data_material[$val->id_design .'_'. $val->design_mitra_id]['id_design']       = $val->id_design;
            $data_material[$val->id_design .'_'. $val->design_mitra_id]['design_mitra_id'] = $val->design_mitra_id;
            $data_material[$val->id_design .'_'. $val->design_mitra_id]['jenis']           = $val->jenis;
            $data_material[$val->id_design .'_'. $val->design_mitra_id]['designator']      = $val->designator;
            $data_material[$val->id_design .'_'. $val->design_mitra_id]['uraian']          = html_entity_decode($val->uraian, ENT_QUOTES, 'UTF-8');
            $data_material[$val->id_design .'_'. $val->design_mitra_id]['jenis_khs']       = $val->jenis_khs;
            $data_material[$val->id_design .'_'. $val->design_mitra_id]['material']        = $val->material;
            $data_material[$val->id_design .'_'. $val->design_mitra_id]['jasa']            = $val->jasa;
            $data_material[$val->id_design .'_'. $val->design_mitra_id]['namcomp']         = $val->namcomp;
            $data_material[$val->id_design .'_'. $val->design_mitra_id]['sp']              = $val->sp;
            $data_material[$val->id_design .'_'. $val->design_mitra_id]['rekon']           = $val->rekon;
          }

          $data_material = array_values($data_material);

          foreach($last_data as $val)
          {
            if(!isset($data_item_rw[$val->id_boq_lokasi]) )
            {
              foreach($data_material as $k => $v)
              {
                $data_item_rw[$val->id_boq_lokasi][$v['id_design'] .'-'. $v['jenis_khs'] ] = (object)[
                  'id'              => 0,
                  'id_boq_lokasi'   => $val->id_boq_lokasi,
                  'lokasi'          => $val->lokasi,
                  'id_design'       => $v['id_design'],
                  'jenis_khs'       => $v['jenis_khs'],
                  'designator'      => $v['designator'],
                  'material'        => $v['material'],
                  'jasa'            => $v['jasa'],
                  'sp'              => 0,
                  'rekon'           => 0,
                  'tambah'          => 0,
                  'kurang'          => 0,
                  'status_design'   => 0,
                  'jenis'           => $v['jenis'],
                  'namcomp'         => $v['namcomp'],
                  'design_mitra_id' => $v['design_mitra_id'],
                  'uraian'          => $v['uraian'],
                ];
              }
            }

            if($data_item_rw[$val->id_boq_lokasi][$val->id_design .'-'. $val->jenis_khs])
            {
              if($data_item_rw[$val->id_boq_lokasi][$val->id_design .'-'. $val->jenis_khs]->material != 0)
              {
                $data_item_rw[$val->id_boq_lokasi][$val->id_design .'-'. $val->jenis_khs]->material = $val->material_pd;
              }

              if($data_item_rw[$val->id_boq_lokasi][$val->id_design .'-'. $val->jenis_khs]->jasa != 0)
              {
                $data_item_rw[$val->id_boq_lokasi][$val->id_design .'-'. $val->jenis_khs]->jasa = $val->jasa_pd;
              }

              $data_item_rw[$val->id_boq_lokasi][$val->id_design .'-'. $val->jenis_khs]->id              = $val->id;
              $data_item_rw[$val->id_boq_lokasi][$val->id_design .'-'. $val->jenis_khs]->sp              = $val->sp;
              $data_item_rw[$val->id_boq_lokasi][$val->id_design .'-'. $val->jenis_khs]->rekon           = $val->rekon;
              $data_item_rw[$val->id_boq_lokasi][$val->id_design .'-'. $val->jenis_khs]->tambah          = $val->tambah;
              $data_item_rw[$val->id_boq_lokasi][$val->id_design .'-'. $val->jenis_khs]->kurang          = $val->kurang;
              $data_item_rw[$val->id_boq_lokasi][$val->id_design .'-'. $val->jenis_khs]->jenis           = $val->jenis;
              $data_item_rw[$val->id_boq_lokasi][$val->id_design .'-'. $val->jenis_khs]->namcomp         = $val->namcomp;
              $data_item_rw[$val->id_boq_lokasi][$val->id_design .'-'. $val->jenis_khs]->design_mitra_id = $val->design_mitra_id;
            }
          }

          foreach($data_item_rw as $k => $v)
          {
            $data_item[] = array_values($v);
          }

          $data_item = array_values($data_item);

          $split_bulan = explode('-', $last_data_work->bulan_pengerjaan);
          $mitra = ReportModel::active_mitra();
          $jenis = 'load';

          return view('Operation.manual_boq', compact('kerjaan', 'all_sto', 'design', 'mitra', 'data_item', 'data_material', 'proaktif_all', 'jenis', 'last_data_work'), ['bulan' => $split_bulan[1], 'tahun' => $split_bulan[0]] );
        break;
        case (in_array($data->step_id, [1, 24, 27]) ? TRUE : FALSE):
          $data    = AdminModel::get_data_final($id);
          $lokasi  = CommerceModel::get_lokasi($id);
          $ld      = AdminModel::get_detail_boq($id, 0);
          $apm_all = ReportModel::get_apm_budget_all();

          foreach($ld as $k => $v)
          {
            $load_data[] = $v;
            $find_k = array_search($v->pid, array_column($apm_all, 'wbs') );

            if($find_k !== FALSE)
            {
              $be = $apm_all[$find_k];
              $load_data[$k]->mns = $be['keperluan']['Material : Non Stock'];
              $load_data[$k]->ms = $be['keperluan']['Material : Stock'];
            }
          }

          return view('Commerce.praKerja.req_pid', compact('data', 'lokasi', 'load_data') );
        break;
        case ($data->step_id == 2 ? TRUE : FALSE):
          $data         = AdminModel::get_data_final($id);
          $find_pid     = OperationModel::find_pid_byUp($id);
          $find_job_dok = ReportModel::get_proc_user($data->pekerjaan, 'job');
          return view('Operation.create_justif', compact('data', 'find_job_dok') );
        break;
        case (in_array($data->step_id, [3]) ? TRUE : FALSE):
          $data      = AdminModel::get_data_final($id);
          $get_mitra = ToolsModel::find_mitra(Session('auth')->mitra_amija_pt);
          $check_data =[];
          $get_pid = explode(', ', $data->id_project);
          return view('Area.Rekon.PraKerja.input_spen', compact('get_mitra', 'data', 'check_data', 'get_pid') );
        break;
        case (in_array($data->step_id, [4, 19, 15]) ? TRUE : FALSE):
          $data      = AdminModel::get_data_final($id);
          $get_mitra = ToolsModel::find_mitra($data->nama_company);
          $ta        = ToolsModel::find_mitra('TELKOM AKSES');
          return view('Mitra.Rekon.PraKerja.input_surat_sanggup', compact('data', 'get_mitra', 'ta'));
        break;
        case ($data->step_id == 5 ? TRUE : FALSE):
          $data      = AdminModel::get_data_final($id);
          $get_khs   = AdminModel::find_work($data->pekerjaan);
          $get_mitra = ToolsModel::find_mitra(Session('auth')->mitra_amija_pt);
          $check_data =[];
          return view('Area.Rekon.PraKerja.input_sp', compact('get_mitra', 'data', 'check_data', 'get_khs'));
        break;
        case (in_array($data->step_id, [6, 16]) ? TRUE : FALSE):
          $kerjaan       = ToolsModel::list_pekerjaan();
          $design        = AdminModel::get_design(3, $id);
          $data_pbu      = AdminModel::get_data_final($id);
          $all_sto       = AdminModel::area_alamat($data_pbu->witel);
          $sub_jenis_all = AdminModel::get_pekerjaan($data_pbu->jenis_work, 'sub_jenis_all_j');

          $data_pid_all = MitraModel::get_all_pid($id);
          $valid = null;

          foreach($data_pid_all as $k => $val)
          {
            $pid_all[$val->pid]['pid']             = $val->pid;
            $pid_all[$val->pid]['budget_new']      = $val->budget_up;
            $pid_all[$val->pid]['created_at_new']  = $val->tgl_last_update;
            $pid_all[$val->pid]['budget_lama']     = $val->budget;
            $pid_all[$val->pid]['created_at_lama'] = $val->created_at;
          }

          $data_rfc = AdminModel::get_rfc_boq($id);
          $data_pbl = $history_rfc = [];
          $count_download = 0;

          $all_design_rfc = AdminModel::find_all_rfc();
          $all_design_rfc = json_decode(json_encode($all_design_rfc), TRUE);
          $get_all_lok = DB::table('procurement_Boq_lokasi As pbl')
          ->leftjoin('procurement_req_pid As prp', 'pbl.id', '=', 'prp.id_lokasi')
          ->leftjoin('procurement_pid As pp', 'pp.id', '=', 'prp.id_pid_req')
          ->select('pbl.*', 'pp.pid')
          ->where('pbl.id_upload', $id)
          ->WhereNotNull('pp.id')
          ->get();

          $tanpa_rfc = $rfc_out = $rfc_return = $rfc_lop_out = $rfc_lop_return =  $rfc = $rfc_lop = [];

          foreach($data_rfc as $kc1 => $valc1)
					{
            $find_k_pbl = array_search($valc1->wbs_element, array_column(json_decode(json_encode($get_all_lok, TRUE) ), 'pid') );

            if($find_k_pbl !== FALSE)
            {
              $data_pbl = $get_all_lok[$find_k_pbl];
            }

            $history_rfc[$valc1->rfc] = $valc1;
            $check_path = '/mnt/hdd4/upload/storage/rfc_ttd/';
            $rfc_file = @preg_grep('~^'.$valc1->rfc.'.*$~', scandir($check_path) );

            $key_adr_pid = array_search($valc1->material, array_column($all_design_rfc, 'design_rfc') );

            if(count($rfc_file) != 0)
            {
              $history_rfc[$valc1->rfc]->download = 'ada';
              $count_download += 1;
            }
            else
            {
              $check_path = public_path() . '/upload2/' . $id . '/dokumen_rfc_up/';
              $rfc_file = @preg_grep('~^'.$valc1->rfc.'.*$~', scandir($check_path) );
              if(count($rfc_file) != 0)
              {
                $history_rfc[$valc1->rfc]->download = 'ada';
                $count_download += 1;
              }
              else
              {
                $history_rfc[$valc1->rfc]->download = 'kdd';
              }
            }

            $rfc_out[$valc1->material]['material'] = $valc1->material;

            if($data_pbl)
            {
              $rfc_lop_out[$data_pbl->id][$valc1->material]['material'] = $valc1->material;
            }

            $multiple = 1;

            if($key_adr_pid !== FALSE)
            {
              $multiple = $all_design_rfc[$key_adr_pid]['qty_per_item'];
            }

            if($valc1->type == 'Out')
            {
              $rfc_out[$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['jenis']        = 'Out';
              $rfc_out[$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['no_reservasi'] = $valc1->no_reservasi;
              $rfc_out[$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['nomor_rfc_gi'] = $valc1->no_rfc;
              $rfc_out[$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['vol_give']     = $valc1->quantity;
              $rfc_out[$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['use']          = $valc1->terpakai;
              $rfc_out[$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['id_pro']       = $valc1->id_pro;

              if(!isset($rfc_out[$valc1->material]['isi_m']['rumus_terpakai']) )
              {
                $rfc_out[$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['rumus_terpakai'] = $valc1->terpakai;
              }

              $rfc_out[$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['rumus_terpakai'] = ($valc1->terpakai * $multiple);

              $rfc_out[$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['status_rfc_gi'] = $valc1->status_rfc;
              $rfc_out[$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['id_pro'] = $valc1->id_pro;

              $rfc_out[$valc1->material]['keterangan'] = $valc1->keterangan;

              if($data_pbl)
              {
                $rfc_lop_out[$data_pbl->id][$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['jenis']        = 'Out';
                $rfc_lop_out[$data_pbl->id][$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['nomor_rfc_gi'] = $valc1->no_rfc;
                $rfc_lop_out[$data_pbl->id][$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['vol_give']     = $valc1->quantity;
                $rfc_lop_out[$data_pbl->id][$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['use']          = $valc1->terpakai;
                $rfc_lop_out[$data_pbl->id][$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['no_reservasi'] = $valc1->no_reservasi;

                if(!isset($rfc_lop_out[$data_pbl->id][$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['rumus_terpakai']) )
                {
                  $rfc_lop_out[$data_pbl->id][$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['rumus_terpakai'] = $valc1->terpakai;
                }

                $rfc_lop_out[$data_pbl->id][$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['rumus_terpakai'] = ($valc1->terpakai * $multiple);

                $rfc_lop_out[$data_pbl->id][$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['status_rfc_gi'] = $valc1->status_rfc;
                $rfc_lop_out[$data_pbl->id][$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['id_pro'] = $valc1->id_pro;

                $rfc_lop_out[$data_pbl->id][$valc1->material]['keterangan'] = $valc1->keterangan;
              }
            }

            if($valc1->type == 'Return')
            {
              $rfc_return[$valc1->material][$valc1->id_pro]['jenis']             = 'Return';
              $rfc_return[$valc1->material][$valc1->id_pro]['nomor_rfc_return']  = $valc1->no_rfc;
              $rfc_return[$valc1->material][$valc1->id_pro]['return']            = $valc1->quantity;
              $rfc_return[$valc1->material][$valc1->id_pro]['status_rfc_return'] = $valc1->status_rfc;
              $rfc_return[$valc1->material][$valc1->id_pro]['id_pro']            = $valc1->id_pro;
              $rfc_return[$valc1->material][$valc1->id_pro]['no_reservasi']      = $valc1->no_reservasi;

              if($data_pbl)
              {
                $rfc_lop_return[$data_pbl->id][$valc1->material][$valc1->id_pro]['jenis']             = 'return';
                $rfc_lop_return[$data_pbl->id][$valc1->material][$valc1->id_pro]['nomor_rfc_return']  = $valc1->no_rfc;
                $rfc_lop_return[$data_pbl->id][$valc1->material][$valc1->id_pro]['return']            = $valc1->quantity;
                $rfc_lop_return[$data_pbl->id][$valc1->material][$valc1->id_pro]['no_reservasi']      = $valc1->no_reservasi;
                $rfc_lop_return[$data_pbl->id][$valc1->material][$valc1->id_pro]['id_pro']            = $valc1->id_pro;
                $rfc_lop_return[$data_pbl->id][$valc1->material][$valc1->id_pro]['status_rfc_return'] = $valc1->status_rfc;
              }
            }

            if(!isset($rfc_out[$valc1->material]['total_m']) )
            {
              $rfc_out[$valc1->material]['total_m'] = 0;
            }

            $rfc_out[$valc1->material]['total_m'] += ($valc1->type == 'Out' ? $valc1->quantity : 0);

            if(!isset($rfc_out[$valc1->material]['total_use']) )
            {
              $rfc_out[$valc1->material]['total_use'] = 0;
            }

            $rfc_out[$valc1->material]['total_use'] += ($valc1->type == 'Out' ? $valc1->terpakai * $multiple : 0);

            if($rfc_lop_out)
            {
              if(!isset($rfc_lop_out[$data_pbl->id][$valc1->material]['total_m']) )
              {
                $rfc_lop_out[$data_pbl->id][$valc1->material]['total_m'] = 0;
              }

              $rfc_lop_out[$data_pbl->id][$valc1->material]['total_m'] += ($valc1->type == 'Out' ? $valc1->quantity : 0);

              if(!isset($rfc_lop_out[$data_pbl->id][$valc1->material]['total_use']) )
              {
                $rfc_lop_out[$data_pbl->id][$valc1->material]['total_use'] = 0;
              }

              $rfc_lop_out[$data_pbl->id][$valc1->material]['total_use'] += ($valc1->type == 'Out' ? $valc1->terpakai * $multiple : 0);
            }
					}

          $all_material = array_unique(array_merge(array_keys($rfc_out), array_keys($rfc_return) ) );
          $rfc = array_map(function($x){
            return $x = [];
          }, array_flip($all_material) );

          foreach($rfc_out as $k => $v)
          {
            $rfc[$k] = $v;
          }

          foreach($rfc_return as $k => $v)
          {
            $key_rfc = array_column($rfc, 'material');
            $find_k = array_keys($key_rfc, $k);

            foreach($find_k as $kk => $vv)
            {
              $data = $rfc[$key_rfc[$vv] ]['isi_m'];

              foreach($data as $k3 => $v3)
              {
                $matchingKeys = array_keys(array_filter($v, function($item) use ($k3) {
                  return $item['no_reservasi'] == $k3;
                }) );

                foreach($matchingKeys as $k4 => $v4)
                {
                  $rfc[$key_rfc[$vv] ]['isi_m'][$v[$v4]['no_reservasi'] ]['return'][$v4] = $v[$v4];
                }
              }
            }
          }

          foreach($rfc_lop_out as $k0 => $v0)
          {
            if(@$rfc_lop_return[$k0])
            {
              $rfc_return = $rfc_lop_return[$k0];
              $all_material = array_unique(array_merge(array_keys($v0), array_keys($rfc_return ?? []) ) );
              $rfc_lop_raw = array_map(function($x){
                return $x = [];
              }, array_flip($all_material) );
              $rfc_lop = [];

              foreach($v0 as $k => $v)
              {
                $rfc_lop_raw[$k] = $v;
              }

              foreach($rfc_return as $k => $v)
              {
                $key_rfc = array_column($rfc, 'material');
                $find_k = array_keys($key_rfc, $k);

                foreach($find_k as $kk => $vv)
                {
                  $data = $rfc_lop_raw[$key_rfc[$vv] ]['isi_m'];

                  foreach($data as $k3 => $v3)
                  {
                    $matchingKeys = array_keys(array_filter($v, function($item) use ($k3) {
                      return $item['no_reservasi'] == $k3;
                    }) );

                    foreach($matchingKeys as $k4 => $v4)
                    {
                      $rfc_lop_raw[$key_rfc[$vv] ]['isi_m'][$v[$v4]['no_reservasi'] ]['return'][$v4] = $v[$v4];
                    }
                  }
                }
              }

              $rfc_lop[$k0] = $rfc_lop_raw;
            }
          }

          $key_khs = $data_item = [];

          // foreach($last_data as $val)
          // {
          //   $data_item[$val->id_boq_lokasi][] = $val;
          //   $key_khs[] =$val->jenis_khs;
          // }

          // $data_item = array_values($data_item);

		      $last_data_mat = AdminModel::get_last_material_boq($id, 'ALL');
          $order_mat_1 = $order_mat_0 = $order_mat_2 = [];

          foreach($last_data_mat as $v)
          {
            if($v->status_design == 1)
            {
              $order_mat_1[] = $v;
            }

            if($v->status_design == 2)
            {
              $order_mat_2[] = $v;
            }

            if($v->status_design == 0)
            {
              $order_mat_0[] = $v;
            }
          }

          if($order_mat_1)
          {
            $last_data_mat = $order_mat_1;
            $ibd = 1;
          }
          elseif($order_mat_2)
          {
            $last_data_mat = $order_mat_2;
            $ibd = 2;
          }
          elseif($order_mat_0)
          {
            $last_data_mat = $order_mat_0;
            $ibd = 0;
          }

          $last_data = AdminModel::get_design_boq($id, [$ibd]);

          foreach($last_data_mat as $k => $val)
          {
            $data_material[$val->id_design .'_'. $val->design_mitra_id]['id_design']       = $val->id_design;
            $data_material[$val->id_design .'_'. $val->design_mitra_id]['design_mitra_id'] = $val->design_mitra_id;
            $data_material[$val->id_design .'_'. $val->design_mitra_id]['jenis']           = $val->jenis;
            $data_material[$val->id_design .'_'. $val->design_mitra_id]['designator']      = $val->designator;
            $data_material[$val->id_design .'_'. $val->design_mitra_id]['uraian']          = html_entity_decode($val->uraian, ENT_QUOTES, 'UTF-8');
            $data_material[$val->id_design .'_'. $val->design_mitra_id]['jenis_khs']       = $val->jenis_khs;
            $data_material[$val->id_design .'_'. $val->design_mitra_id]['material']        = $val->material;
            $data_material[$val->id_design .'_'. $val->design_mitra_id]['jasa']            = $val->jasa;
            $data_material[$val->id_design .'_'. $val->design_mitra_id]['namcomp']         = $val->namcomp;
            $data_material[$val->id_design .'_'. $val->design_mitra_id]['sp']              = $val->sp;
            $data_material[$val->id_design .'_'. $val->design_mitra_id]['rekon']           = $val->rekon;
          }

          $data_material = array_values($data_material);

          foreach($last_data as $val)
          {
            if(!isset($data_item_rw[$val->id_boq_lokasi]) )
            {
              foreach($data_material as $k => $v)
              {
                $data_item_rw[$val->id_boq_lokasi][$v['id_design'] .'-'. $v['jenis_khs'] ] = (object)[
                  'id'              => 0,
                  'id_boq_lokasi'   => $val->id_boq_lokasi,
                  'sto'             => $val->sto,
                  'lokasi'          => $val->lokasi,
                  'sub_jenis_p'     => $val->sub_jenis_p,
                  'id_design'       => $v['id_design'],
                  'jenis_khs'       => $v['jenis_khs'],
                  'designator'      => $v['designator'],
                  'material'        => $v['material'],
                  'jasa'            => $v['jasa'],
                  'sp'              => 0,
                  'rekon'           => 0,
                  'tambah'          => 0,
                  'kurang'          => 0,
                  'status_design'   => 0,
                  'jenis'           => $v['jenis'],
                  'namcomp'         => $v['namcomp'],
                  'design_mitra_id' => $v['design_mitra_id'],
                  'uraian'          => $v['uraian'],
                  'new_boq'         => $val->new_boq,
                ];
              }
            }

            if($data_item_rw[$val->id_boq_lokasi][$val->id_design .'-'. $val->jenis_khs])
            {
              $data_item_rw[$val->id_boq_lokasi][$val->id_design .'-'. $val->jenis_khs]->id              = $val->id;
              $data_item_rw[$val->id_boq_lokasi][$val->id_design .'-'. $val->jenis_khs]->sp              = $val->sp;
              $data_item_rw[$val->id_boq_lokasi][$val->id_design .'-'. $val->jenis_khs]->rekon           = $val->rekon;
              $data_item_rw[$val->id_boq_lokasi][$val->id_design .'-'. $val->jenis_khs]->tambah          = $val->tambah;
              $data_item_rw[$val->id_boq_lokasi][$val->id_design .'-'. $val->jenis_khs]->kurang          = $val->kurang;
              $data_item_rw[$val->id_boq_lokasi][$val->id_design .'-'. $val->jenis_khs]->material        = $val->material;
              $data_item_rw[$val->id_boq_lokasi][$val->id_design .'-'. $val->jenis_khs]->jasa            = $val->jasa;
              $data_item_rw[$val->id_boq_lokasi][$val->id_design .'-'. $val->jenis_khs]->jenis           = $val->jenis;
              $data_item_rw[$val->id_boq_lokasi][$val->id_design .'-'. $val->jenis_khs]->namcomp         = $val->namcomp;
              $data_item_rw[$val->id_boq_lokasi][$val->id_design .'-'. $val->jenis_khs]->design_mitra_id = $val->design_mitra_id;
            }
          }

          foreach($data_item_rw as $k => $v)
          {
            $data_item[] = array_values($v);
          }

          foreach($rfc as $k => &$v)
          {
            if(!isset($v['isi_m']) )
            {
              unset($rfc[$k]);
            }
          }
          unset($v);

          $find_rfr = [];

          foreach($data_rfc as $k => $v)
          {
            $find_rfr[$v->id_reserv][$k]['type'] = $v->type;
            $find_rfr[$v->id_reserv][$k]['no_rfc'] = $v->no_rfc;
            $find_rfr[$v->id_reserv][$k]['material'] = $v->material;
          }

          foreach($find_rfr as $k => $v)
          {
            $v =  array_values($v);

            if(!in_array('Out', array_column($v, 'type') ) )
            {
              $find_return = array_keys(array_column($v,'type'), 'Return');

              foreach($find_return as $vv)
              {
                $tanpa_rfc[] = $v[$vv]['no_rfc'] .' ('. $v[$vv]['material'] .')';
              }
            }
          }

          $save_pid = $save_pid_per_lok = [];

      		$find_pid = OperationModel::find_pid_byUp($id);

          foreach($find_pid as $v)
          {
            $check_pid = OperationModel::check_pid($v);

            if($check_pid->idp)
            {
              $idp[$check_pid->idp] = $check_pid->idp;
            }
            else
            {
              $wbs = explode(',', $v->idp);

              foreach($wbs as $vv)
              {
                $idp[$v->pid .'-'. $vv] = $v->pid .'-'. $vv;
              }
            }
          }

      		$ll = OperationModel::get_lokasi($id);

          foreach($ll as $k => $v)
          {
            if($v->pid_sto)
            {
              $exp = explode(',', $v->pid_sto);

              foreach($exp as $vvv)
              {
                $save_pid_per_lok[$v->id][] = trim($vvv);
              }
            }
            else
            {
              $get_list_pid = DB::table('procurement_Boq_lokasi As pbl')
              ->LeftJoin('procurement_req_pid As prp', 'pbl.id', '=', 'prp.id_lokasi')
              ->select('pbl.*', 'prp.id_pid_req As id_pid')
              ->where('pbl.id', $v->id)->get();

              foreach($get_list_pid as $glp)
              {
                if($glp->id_pid)
                {
                  $data = DB::SELECT("SELECT (SELECT CONCAT_WS(',',
                  ( CASE WHEN (SELECT SUM(pbd.material)
                  FROM procurement_Boq_design pbd
                  WHERE pbd.id_boq_lokasi = ".$glp->id." AND pbd.jenis_khs= 16) != 0 THEN CONCAT_WS('-', ppc.pid, '01-01-01' ) ELSE NULL END),
                  ( CASE WHEN (SELECT SUM(pbd.material)
                  FROM procurement_Boq_design pbd
                  WHERE pbd.id_boq_lokasi = ".$glp->id." AND (pbd.jenis_khs != 16 OR pbd.jenis_khs IS NULL)) != 0 THEN CONCAT_WS('-', ppc.pid, '01-01-02' ) ELSE NULL END),
                  ( CASE WHEN (SELECT SUM(pbd.jasa)
                  FROM procurement_Boq_design pbd
                  WHERE pbd.id_boq_lokasi = ".$glp->id.") != 0 THEN CONCAT_WS('-', ppc.pid, '01-02-01' ) ELSE NULL END) ) ) As idp
                  FROM procurement_pid ppc
                  WHERE ppc.id = ".$glp->id_pid)[0];

                  if($data)
                  {
                    $exp = explode(',', $data->idp);

                    foreach($exp as $vvv)
                    {
                      $save_pid_per_lok[$glp->id][] = trim($vvv);
                    }
                  }
                }
              }
            }
          }

          foreach($idp as $val)
          {
            $exp = explode(',', $val);

            foreach($exp as $v)
            {
              $save_pid[] = $v;
            }
          }

          if(!$save_pid)
          {
            $find_id_p = ReportModel::get_boq_data($id);
            if($find_id_p->id_project)
            {
              $save_pid = explode(', ', $find_id_p->id_project);
            }
          }

          $save_pid = array_unique(array_map(function($x){
            return trim($x);
          }, $save_pid) );

          return view('Mitra.Rekon.Finish.input_material_rekon', compact('kerjaan', 'ibd', 'data_material', 'all_sto', 'design', 'data_item', 'data_pbu', 'pid_all', 'history_rfc', 'count_download', 'rfc', 'rfc_lop', 'sub_jenis_all', 'save_pid_per_lok', 'tanpa_rfc'), ['total_rfc' => count($data_rfc), 'witho_rfc' => array_sum($key_khs), 'pid' => $save_pid]);
        break;
        case ($data->step_id == 7 ? TRUE : FALSE):
          $kerjaan        = ToolsModel::list_pekerjaan();
          $design         = AdminModel::get_design(3, $id);
          $data_pbu       = AdminModel::get_data_final($id);
          $all_sto        = AdminModel::area_alamat($data_pbu->witel);
          $sub_jenis_all  = AdminModel::get_pekerjaan($data_pbu->jenis_work, 'sub_jenis_all_j');

          $last_data_mat = AdminModel::get_last_material_boq($id, 'all');

          $order_mat_1 = $order_mat_0 = $order_mat_2 = $load_sp = [];

          foreach($last_data_mat as $v)
          {
            if($v->status_design == 1)
            {
              $order_mat_1[] = $v;
            }

            if($v->status_design == 2)
            {
              $order_mat_2[] = $v;
            }

            if($v->status_design == 0)
            {
              $order_mat_0[] = $v;
            }
          }

          if($order_mat_1)
          {
            $last_data_mat = $order_mat_1;
            $id_design = 1;
          }
          elseif($order_mat_2)
          {
            $last_data_mat = $order_mat_2;
            $id_design = 2;
          }
          elseif($order_mat_0)
          {
            $last_data_mat = $order_mat_0;
            $id_design = 0;
          }

          $last_data      = AdminModel::get_design_boq($id, [$id_design]);
          $design_rfc     = AdminModel::get_design_boq($id, $id_design, 'all');
          $all_pdr        = AdminModel::get_all_pdr();
          $data_rfc       = AdminModel::get_rfc_boq($id);
          $rfc = $rfc_lop = $data_pbl = $volum_design = $collect_submit = [];
          $count = 0;

          $all_rfc = AdminModel::find_all_rfc();
          $all_design_rfc = json_decode(json_encode($all_rfc), TRUE);
          $get_all_lok = DB::table('procurement_Boq_lokasi As pbl')
          ->leftjoin('procurement_req_pid As prp', 'pbl.id', '=', 'prp.id_lokasi')
          ->leftjoin('procurement_pid As pp', 'pp.id', '=', 'prp.id_pid_req')
          ->select('pbl.*', 'pp.pid')
          ->where('pbl.id_upload', $id)
          ->WhereNotNull('pp.id')
          ->get();

          $tanpa_rfc = $rfc_out = $rfc_return = $rfc_lop_out = $rfc_lop_return =  $rfc = $rfc_lop = [];

          foreach($data_rfc as $kc1 => $valc1)
					{
            $find_k_pbl = array_search($valc1->wbs_element, array_column(json_decode(json_encode($get_all_lok, TRUE) ), 'pid') );

            if($find_k_pbl !== FALSE)
            {
              $data_pbl = $get_all_lok[$find_k_pbl];
            }

            $rfc_out[$valc1->material]['material'] = $valc1->material;

            $key_adr_pid = array_search($valc1->material, array_column($all_design_rfc, 'design_rfc') );

            if($data_pbl)
            {
              $rfc_lop_out[$data_pbl->id][$valc1->material]['material'] = $valc1->material;
            }

            $multiple = 1;

            if($key_adr_pid !== FALSE)
            {
              $multiple = $all_design_rfc[$key_adr_pid]['qty_per_item'];
            }

            if($valc1->type == 'Out')
            {
              $rfc_out[$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['jenis']        = 'Out';
              $rfc_out[$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['no_reservasi'] = $valc1->no_reservasi;
              $rfc_out[$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['nomor_rfc_gi'] = $valc1->no_rfc;
              $rfc_out[$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['vol_give']     = $valc1->quantity;
              $rfc_out[$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['use']          = $valc1->terpakai;
              $rfc_out[$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['id_pro']       = $valc1->id_pro;

              if(!isset($rfc_out[$valc1->material]['isi_m']['rumus_terpakai']) )
              {
                $rfc_out[$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['rumus_terpakai'] = $valc1->terpakai;
              }

              $rfc_out[$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['rumus_terpakai'] = ($valc1->terpakai * $multiple);

              $rfc_out[$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['status_rfc_gi'] = $valc1->status_rfc;
              $rfc_out[$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['id_pro'] = $valc1->id_pro;

              $rfc_out[$valc1->material]['keterangan'] = $valc1->keterangan;

              if($data_pbl)
              {
                $rfc_lop_out[$data_pbl->id][$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['jenis']        = 'Out';
                $rfc_lop_out[$data_pbl->id][$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['nomor_rfc_gi'] = $valc1->no_rfc;
                $rfc_lop_out[$data_pbl->id][$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['vol_give']     = $valc1->quantity;
                $rfc_lop_out[$data_pbl->id][$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['use']          = $valc1->terpakai;
                $rfc_lop_out[$data_pbl->id][$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['no_reservasi'] = $valc1->no_reservasi;

                if(!isset($rfc_lop_out[$data_pbl->id][$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['rumus_terpakai']) )
                {
                  $rfc_lop_out[$data_pbl->id][$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['rumus_terpakai'] = $valc1->terpakai;
                }

                $rfc_lop_out[$data_pbl->id][$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['rumus_terpakai'] = ($valc1->terpakai * $multiple);

                $rfc_lop_out[$data_pbl->id][$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['status_rfc_gi'] = $valc1->status_rfc;
                $rfc_lop_out[$data_pbl->id][$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['id_pro'] = $valc1->id_pro;

                $rfc_lop_out[$data_pbl->id][$valc1->material]['keterangan'] = $valc1->keterangan;
              }
            }

            if($valc1->type == 'Return')
            {
              $rfc_return[$valc1->material][$valc1->id_pro]['jenis']             = 'Return';
              $rfc_return[$valc1->material][$valc1->id_pro]['nomor_rfc_return']  = $valc1->no_rfc;
              $rfc_return[$valc1->material][$valc1->id_pro]['return']            = $valc1->quantity;
              $rfc_return[$valc1->material][$valc1->id_pro]['status_rfc_return'] = $valc1->status_rfc;
              $rfc_return[$valc1->material][$valc1->id_pro]['id_pro']            = $valc1->id_pro;
              $rfc_return[$valc1->material][$valc1->id_pro]['no_reservasi']      = $valc1->no_reservasi;

              if($data_pbl)
              {
                $rfc_lop_return[$data_pbl->id][$valc1->material][$valc1->id_pro]['jenis']             = 'return';
                $rfc_lop_return[$data_pbl->id][$valc1->material][$valc1->id_pro]['nomor_rfc_return']  = $valc1->no_rfc;
                $rfc_lop_return[$data_pbl->id][$valc1->material][$valc1->id_pro]['return']            = $valc1->quantity;
                $rfc_lop_return[$data_pbl->id][$valc1->material][$valc1->id_pro]['no_reservasi']      = $valc1->no_reservasi;
                $rfc_lop_return[$data_pbl->id][$valc1->material][$valc1->id_pro]['id_pro']            = $valc1->id_pro;
                $rfc_lop_return[$data_pbl->id][$valc1->material][$valc1->id_pro]['status_rfc_return'] = $valc1->status_rfc;
              }
            }

            if(!isset($rfc_out[$valc1->material]['total_m']) )
            {
              $rfc_out[$valc1->material]['total_m'] = 0;
            }

            $rfc_out[$valc1->material]['total_m'] += ($valc1->type == 'Out' ? $valc1->quantity : 0);

            if(!isset($rfc_out[$valc1->material]['total_use']) )
            {
              $rfc_out[$valc1->material]['total_use'] = 0;
            }

            $rfc_out[$valc1->material]['total_use'] += ($valc1->type == 'Out' ? $valc1->terpakai * $multiple : 0);

            if($rfc_lop_out)
            {
              if(!isset($rfc_lop_out[$data_pbl->id][$valc1->material]['total_m']) )
              {
                $rfc_lop_out[$data_pbl->id][$valc1->material]['total_m'] = 0;
              }

              $rfc_lop_out[$data_pbl->id][$valc1->material]['total_m'] += ($valc1->type == 'Out' ? $valc1->quantity : 0);

              if(!isset($rfc_lop_out[$data_pbl->id][$valc1->material]['total_use']) )
              {
                $rfc_lop_out[$data_pbl->id][$valc1->material]['total_use'] = 0;
              }

              $rfc_lop_out[$data_pbl->id][$valc1->material]['total_use'] += ($valc1->type == 'Out' ? $valc1->terpakai * $multiple : 0);
            }

            foreach($all_pdr as $k3 => $v3)
            {
              if($v3->design_rfc == $valc1->material)
              {
                $collect_submit[$k3] = $v3;

                if($data_pbl)
                {
                  $rfc_lop_out[$data_pbl->id][$valc1->material]['design'][$k3]['id_design'] = $v3->id_design;
                  $rfc_lop_out[$data_pbl->id][$valc1->material]['design'][$k3]['nm_design'] = $v3->designator;

                  $rfc_lop_out[$data_pbl->id][$valc1->material]['free_use'] = $v3->free_use;
                }
              }
            }
					}

          $all_material = array_unique(array_merge(array_keys($rfc_out), array_keys($rfc_return) ) );
          $rfc = array_map(function($x){
            return $x = [];
          }, array_flip($all_material) );

          foreach($rfc_out as $k => $v)
          {
            $rfc[$k] = $v;
          }

          foreach($rfc_return as $k => $v)
          {
            $key_rfc = array_column($rfc, 'material');
            $find_k = array_keys($key_rfc, $k);

            foreach($find_k as $kk => $vv)
            {
              $data = $rfc[$key_rfc[$vv] ]['isi_m'];

              foreach($data as $k3 => $v3)
              {
                $matchingKeys = array_keys(array_filter($v, function($item) use ($k3) {
                  return $item['no_reservasi'] == $k3;
                }) );

                foreach($matchingKeys as $k4 => $v4)
                {
                  $rfc[$key_rfc[$vv] ]['isi_m'][$v[$v4]['no_reservasi'] ]['return'][$v4] = $v[$v4];
                }
              }
            }
          }

          foreach($data_rfc as $kc1 => $valc1)
					{
            if($valc1->type == 'Out')
            {
              foreach($all_pdr as $k3 => $v3)
              {
                if($v3->design_rfc == $valc1->material)
                {
                  $rfc[$valc1->material]['design'][$k3]['id_design'] = $v3->id_design;
                  $rfc[$valc1->material]['design'][$k3]['nm_design'] = $v3->designator;

                  $rfc[$valc1->material]['free_use'] = $v3->free_use;
                }

                foreach ($design_rfc as $kc2 => $valc2)
                {
                  if($valc2->id_pbu == $valc1->id_pbu)
                  {
                    if($valc2->designator == $v3->designator)
                    {
                      if($v3->design_rfc == $valc1->material)
                      {
                        $key_adr_pid = array_search($valc1->material, array_column($all_design_rfc, 'design_rfc') );

                        $multiple = 1;

                        if($key_adr_pid !== FALSE)
                        {
                          $multiple = $all_design_rfc[$key_adr_pid]['qty_per_item'];
                        }

                        $volum_design[$valc2->designator]['material'] = $valc2->designator;

                        $volum_design[$valc2->designator]['nama_material_rfc'][$valc1->material]['nama'] = $valc1->material;

                        if($valc1->keterangan)
                        {
                          $volum_design[$valc2->designator]['nama_material_rfc'][$valc1->material]['keterangan'][$valc1->keterangan] = $valc1->keterangan;
                        }

                        $volum_design[$valc2->designator]['total_volum_design'][$valc2->id_pbu] = $valc2->rekon;

                        if(!isset($volum_design[$valc2->designator]['total_volum_rfc'] ) )
                        {
                          $volum_design[$valc2->designator]['total_volum_rfc'] = 0;
                        }

                        $volum_design[$valc2->designator]['total_volum_rfc'] += $valc1->terpakai * $multiple;
                      }
                    }
                  }
                }
              }
            }
          }

          foreach($rfc_lop_out as $k0 => $v0)
          {
            if(@$rfc_lop_return[$k0])
            {
              $rfc_return = $rfc_lop_return[$k0];
              $all_material = array_unique(array_merge(array_keys($v0), array_keys($rfc_return ?? []) ) );
              $rfc_lop_raw = array_map(function($x){
                return $x = [];
              }, array_flip($all_material) );
              $rfc_lop = [];

              foreach($v0 as $k => $v)
              {
                $rfc_lop_raw[$k] = $v;
              }

              foreach($rfc_return as $k => $v)
              {
                $key_rfc = array_column($rfc, 'material');
                $find_k = array_keys($key_rfc, $k);

                foreach($find_k as $kk => $vv)
                {
                  $data = $rfc_lop_raw[$key_rfc[$vv] ]['isi_m'];

                  foreach($data as $k3 => $v3)
                  {
                    $matchingKeys = array_keys(array_filter($v, function($item) use ($k3) {
                      return $item['no_reservasi'] == $k3;
                    }) );

                    foreach($matchingKeys as $k4 => $v4)
                    {
                      $rfc_lop_raw[$key_rfc[$vv] ]['isi_m'][$v[$v4]['no_reservasi'] ]['return'][$v4] = $v[$v4];
                    }
                  }
                }
              }

              $rfc_lop[$k0] = $rfc_lop_raw;
            }
          }

          foreach($volum_design as $k => $v)
          {
            $volum_design[$k]['total_volum_design'] = array_sum($v['total_volum_design']);
          }

          $count_download = 0;
          $history_rfc = [];
          $check_download = AdminModel::check_downloadable($id);

          foreach($check_download as $key => $val)
          {
            $history_rfc[$val->rfc] = $val;
            $check_path = '/mnt/hdd4/upload/storage/rfc_ttd/';
            $rfc_file = @preg_grep('~^'.$val->rfc.'.*$~', scandir($check_path) );
            // dd($rfc_file);
            if(count($rfc_file) != 0)
            {
              $history_rfc[$val->rfc]->download = 'ada';
              $count_download += 1;
            }
            else
            {
              $check_path = public_path() . '/upload2/' . $id . '/dokumen_rfc_up/';
              $rfc_file = @preg_grep('~^'.$val->rfc.'.*$~', scandir($check_path) );
              if(count($rfc_file) != 0)
              {
                $history_rfc[$val->rfc]->download = 'ada';
                $count_download += 1;
              }
              else
              {
                $history_rfc[$val->rfc]->download = 'kdd';
              }
            }
          }

          $data_sp = AdminModel::get_data_final($id);
          $data_item = $data_item_rw = [];

          // foreach($last_data as $val)
          // {
          //   $data_item[$val->id_boq_lokasi][] = $val;
          // }

          // $data_item = array_values($data_item);
          $select2 = AdminModel::get_steps_by_urutan(8);

          // $collect_submit = json_decode(json_encode($collect_submit), true);

          // foreach ($data_item as $k => $v)
          // {
          //   foreach ($v as $kk => $vv)
          //   {
          //     if (array_search($vv->id_design, array_column($collect_submit, 'id_design')) !== FALSE) {
          //       $xyz['ada'][] = $vv;
          //     } else {
          //       $xyz['tada'][] = $vv;
          //     }
          //   }
          // }

          // foreach ($collect_submit as $k => $v)
          // {
          //   foreach ($data_item as $kk => $vv)
          //   {
          //     if (array_search($v['id_design'], array_column($vv, 'id_design') ) !== FALSE) {
          //       $xyz['ada'][] = $v;
          //     } else {
          //       $xyz['tada'][] = $v;
          //       $data_item[$kk][] = $v;
          //     }
          //   }
          // }

          // $data_item = json_decode(json_encode($data_item), FALSE);
          // $data_item = array_values($data_item);

          foreach($last_data_mat as $k => $val)
          {
            $data_material[$val->id_design .'_'. $val->design_mitra_id]['id_design']       = $val->id_design;
            $data_material[$val->id_design .'_'. $val->design_mitra_id]['design_mitra_id'] = $val->design_mitra_id;
            $data_material[$val->id_design .'_'. $val->design_mitra_id]['jenis']           = $val->jenis;
            $data_material[$val->id_design .'_'. $val->design_mitra_id]['designator']      = $val->designator;
            $data_material[$val->id_design .'_'. $val->design_mitra_id]['uraian']          = html_entity_decode($val->uraian, ENT_QUOTES, 'UTF-8');
            $data_material[$val->id_design .'_'. $val->design_mitra_id]['jenis_khs']       = $val->jenis_khs;
            $data_material[$val->id_design .'_'. $val->design_mitra_id]['material']        = $val->material;
            $data_material[$val->id_design .'_'. $val->design_mitra_id]['jasa']            = $val->jasa;
            $data_material[$val->id_design .'_'. $val->design_mitra_id]['namcomp']         = $val->namcomp;
            $data_material[$val->id_design .'_'. $val->design_mitra_id]['sp']              = $val->sp;
            $data_material[$val->id_design .'_'. $val->design_mitra_id]['rekon']           = $val->rekon;
            $data_material[$val->id_design .'_'. $val->design_mitra_id]['status_design']   = $val->status_design;
          }

          $data_material = array_values($data_material);

          foreach($last_data as $val)
          {
            if(!isset($data_item_rw[$val->id_boq_lokasi]) )
            {
              foreach($data_material as $k => $v)
              {
                $data_item_rw[$val->id_boq_lokasi][$v['id_design'] .'-'. $v['jenis_khs'] ] = (object)[
                  'id'              => 0,
                  'id_boq_lokasi'   => $val->id_boq_lokasi,
                  'sto'             => $val->sto,
                  'lokasi'          => $val->lokasi,
                  'sub_jenis_p'     => $val->sub_jenis_p,
                  'id_design'       => $v['id_design'],
                  'jenis_khs'       => $v['jenis_khs'],
                  'designator'      => $v['designator'],
                  'material'        => $v['material'],
                  'jasa'            => $v['jasa'],
                  'sp'              => 0,
                  'rekon'           => 0,
                  'tambah'          => 0,
                  'kurang'          => 0,
                  'status_design'   => 0,
                  'jenis'           => $v['jenis'],
                  'namcomp'         => $v['namcomp'],
                  'design_mitra_id' => $v['design_mitra_id'],
                  'uraian'          => $v['uraian'],
                  'new_boq'         => $val->new_boq,
                ];
              }
            }

            if($data_item_rw[$val->id_boq_lokasi][$val->id_design .'-'. $val->jenis_khs])
            {
              $data_item_rw[$val->id_boq_lokasi][$val->id_design .'-'. $val->jenis_khs ]->id              = $val->id;
              $data_item_rw[$val->id_boq_lokasi][$val->id_design .'-'. $val->jenis_khs ]->sp              = $val->sp;
              $data_item_rw[$val->id_boq_lokasi][$val->id_design .'-'. $val->jenis_khs ]->rekon           = $val->rekon;
              $data_item_rw[$val->id_boq_lokasi][$val->id_design .'-'. $val->jenis_khs ]->tambah          = $val->tambah;
              $data_item_rw[$val->id_boq_lokasi][$val->id_design .'-'. $val->jenis_khs ]->kurang          = $val->kurang;
              $data_item_rw[$val->id_boq_lokasi][$val->id_design .'-'. $val->jenis_khs ]->material        = $val->material;
              $data_item_rw[$val->id_boq_lokasi][$val->id_design .'-'. $val->jenis_khs ]->jasa            = $val->jasa;
              $data_item_rw[$val->id_boq_lokasi][$val->id_design .'-'. $val->jenis_khs ]->jenis           = $val->jenis;
              $data_item_rw[$val->id_boq_lokasi][$val->id_design .'-'. $val->jenis_khs ]->namcomp         = $val->namcomp;
              $data_item_rw[$val->id_boq_lokasi][$val->id_design .'-'. $val->jenis_khs ]->design_mitra_id = $val->design_mitra_id;
            }
          }

          foreach($data_item_rw as $k => $v)
          {
            $data_item[] = array_values($v);
          }
          // dd($data_item, $last_data, $rfc_lop, $rfc);

          foreach($rfc as $k => &$v)
          {
            if(!isset($v['isi_m']) )
            {
              unset($rfc[$k]);
            }
          }
          unset($v);

          $find_rfr = [];

          foreach($data_rfc as $k => $v)
          {
            $find_rfr[$v->id_reserv][$k]['type'] = $v->type;
            $find_rfr[$v->id_reserv][$k]['no_rfc'] = $v->no_rfc;
            $find_rfr[$v->id_reserv][$k]['material'] = $v->material;
          }

          foreach($find_rfr as $k => $v)
          {
            $v =  array_values($v);

            if(!in_array('Out', array_column($v, 'type') ) )
            {
              $find_return = array_keys(array_column($v,'type'), 'Return');

              foreach($find_return as $vv)
              {
                $tanpa_rfc[] = $v[$vv]['no_rfc'] .' ('. $v[$vv]['material'] .')';
              }
            }
          }

          $save_pid = $save_pid_per_lok = [];

      		$find_pid = OperationModel::find_pid_byUp($id);

          foreach($find_pid as $v)
          {
            $check_pid = OperationModel::check_pid($v);

            if($check_pid->idp)
            {
              $idp[$check_pid->idp] = $check_pid->idp;
            }
            else
            {
              $wbs = explode(',', $v->idp);

              foreach($wbs as $vv)
              {
                $idp[$v->pid .'-'. $vv] = $v->pid .'-'. $vv;
              }
            }
          }

      		$ll = OperationModel::get_lokasi($id);

          foreach($ll as $k => $v)
          {
            if($v->pid_sto)
            {
              $exp = explode(',', $v->pid_sto);

              foreach($exp as $vvv)
              {
                $save_pid_per_lok[$v->id][] = trim($vvv);
              }
            }
            else
            {
              $get_list_pid = DB::table('procurement_Boq_lokasi As pbl')
              ->LeftJoin('procurement_req_pid As prp', 'pbl.id', '=', 'prp.id_lokasi')
              ->select('pbl.*', 'prp.id_pid_req As id_pid')
              ->where('pbl.id', $v->id)->get();

              foreach($get_list_pid as $glp)
              {
                if($glp->id_pid)
                {
                  $data = DB::SELECT("SELECT (SELECT CONCAT_WS(',',
                  ( CASE WHEN (SELECT SUM(pbd.material)
                  FROM procurement_Boq_design pbd
                  WHERE pbd.id_boq_lokasi = ".$glp->id." AND pbd.jenis_khs= 16) != 0 THEN CONCAT_WS('-', ppc.pid, '01-01-01' ) ELSE NULL END),
                  ( CASE WHEN (SELECT SUM(pbd.material)
                  FROM procurement_Boq_design pbd
                  WHERE pbd.id_boq_lokasi = ".$glp->id." AND (pbd.jenis_khs != 16 OR pbd.jenis_khs IS NULL)) != 0 THEN CONCAT_WS('-', ppc.pid, '01-01-02' ) ELSE NULL END),
                  ( CASE WHEN (SELECT SUM(pbd.jasa)
                  FROM procurement_Boq_design pbd
                  WHERE pbd.id_boq_lokasi = ".$glp->id.") != 0 THEN CONCAT_WS('-', ppc.pid, '01-02-01' ) ELSE NULL END) ) ) As idp
                  FROM procurement_pid ppc
                  WHERE ppc.id = ".$glp->id_pid)[0];

                  if($data)
                  {
                    $exp = explode(',', $data->idp);

                    foreach($exp as $vvv)
                    {
                      $save_pid_per_lok[$glp->id][] = trim($vvv);
                    }
                  }
                }
              }
            }
          }

          foreach($idp as $val)
          {
            $exp = explode(',', $val);

            foreach($exp as $v)
            {
              $save_pid[] = $v;
            }
          }

          if(!$save_pid)
          {
            $find_id_p = ReportModel::get_boq_data($id);
            if($find_id_p->id_project)
            {
              $save_pid = explode(', ', $find_id_p->id_project);
            }
          }

          $save_pid = array_unique(array_map(function($x){
            return trim($x);
          }, $save_pid) );

          return view('Operation.validasi_rfc_material', compact('data', 'kerjaan', 'all_sto', 'data_material', 'design', 'data_item', 'rfc', 'data_sp', 'data_pbu', 'tanpa_rfc', 'select2', 'count_download', 'save_pid_per_lok', 'sub_jenis_all', 'history_rfc', 'volum_design', 'load_sp', 'rfc_lop' , 'ibd'), ['pid' => $save_pid]);
        break;
        case ($data->step_id == 8 ? TRUE : FALSE):
          $kerjaan       = ToolsModel::list_pekerjaan();
          $design        = AdminModel::get_design(3, $id);
          $data_pbu      = AdminModel::get_data_final($id);
          $all_sto       = AdminModel::area_alamat($data_pbu->witel);
          $sub_jenis_all = AdminModel::get_pekerjaan($data_pbu->jenis_work, 'sub_jenis_all_j');
          $last_data     = AdminModel::get_design_boq($id, [1]);
          $data_pid_all  = MitraModel::get_all_pid($id);
          $valid = null;

          foreach($data_pid_all as $k => $val)
          {
            $pid_all[$val->pid]['pid']             = $val->pid;
            $pid_all[$val->pid]['budget_new']      = $val->budget_up;
            $pid_all[$val->pid]['created_at_new']  = $val->tgl_last_update;
            $pid_all[$val->pid]['budget_lama']     = $val->budget;
            $pid_all[$val->pid]['created_at_lama'] = $val->created_at;
          }

          $data_sp = AdminModel::get_data_final($id);
          $data_rfc = MitraModel::get_rfc_history($id);
          $data_rfc_tidk_lurus = MitraModel::get_rfc_history($id, 'TIDAK LURUS');
          $count_download = 0;
          $history_rfc = $unclear_rfc_raw = $unclear_rfc = [];

          foreach ($data_rfc_tidk_lurus as $key => $val) {
            $find_rfc = MitraModel::find_rfc('old_rfc', $val->rfc);

            if(!$find_rfc)
            {
              $unclear_rfc_raw[$val->rfc] = $val;
              $check_path = '/mnt/hdd4/upload/storage/rfc_ttd/';
              $rfc_file = @preg_grep('~^'.$val->rfc.'.*$~', scandir($check_path) );
              // dd($rfc_file);
              if(count($rfc_file) != 0)
              {
                $unclear_rfc_raw[$val->rfc]->download = 'ada';
                $count_download += 1;
              }
              else
              {
                $check_path = public_path() . '/upload2/' . $id . '/dokumen_rfc_up/';
                $rfc_file = @preg_grep('~^'.$val->rfc.'.*$~', scandir($check_path) );
                if(count($rfc_file) != 0)
                {
                  $unclear_rfc_raw[$val->rfc]->download = 'ada';
                  $count_download += 1;
                }
                else
                {
                  $unclear_rfc_raw[$val->rfc]->download = 'kdd';
                }
              }
            }
          }

          foreach($unclear_rfc_raw as $k => $v)
          {
            $unclear_rfc[$k] = $v;
            $sd = MitraModel::find_rfc('old_rfc', $k);
            $unclear_rfc[$k]->pelurusan = 'nok';

            if($sd)
            {
              $unclear_rfc[$k]->pelurusan = 'ok';
            }
          }
          // dd($unclear_rfc);
          foreach($data_rfc as $key => $val)
          {
            $history_rfc[$val->rfc] = $val;
            $check_path = '/mnt/hdd4/upload/storage/rfc_ttd/';
            $rfc_file = @preg_grep('~^'.$val->rfc.'.*$~', scandir($check_path) );
            // dd($rfc_file);
            if(count($rfc_file) != 0)
            {
              $history_rfc[$val->rfc]->download = 'ada';
              $count_download += 1;
            }
            else
            {
              $check_path = public_path() . '/upload2/' . $id . '/dokumen_rfc_up/';
              $rfc_file = @preg_grep('~^'.$val->rfc.'.*$~', scandir($check_path) );
              if(count($rfc_file) != 0)
              {
                $history_rfc[$val->rfc]->download = 'ada';
                $count_download += 1;
              }
              else
              {
                $history_rfc[$val->rfc]->download = 'kdd';
              }
            }
          }

          foreach($last_data as $val)
          {
            $data_item[$val->id_boq_lokasi][] = $val;
          }

          usort($history_rfc, function($a, $b) {return strcmp($b->download, $a->download);});

          $get_belum_lurus = AdminModel::get_belum_lurus($id);
          // dd(count($data_rfc) + count($data_rfc_tidk_lurus), $count_download );
          $data_item = array_values($data_item);
          // dd(count($data_rfc), $count_download);
          return view('Mitra.Rekon.Finish.pelurusan_rfc', compact('kerjaan', 'all_sto', 'design', 'sub_jenis_all', 'data_item', 'data_pbu', 'get_belum_lurus', 'pid_all', 'data_sp', 'history_rfc', 'unclear_rfc', 'count_download'), ['total_rfc' => (count($data_rfc) ) ]);
        break;
        case (in_array($data->step_id, [9, 28, 32]) ? TRUE : FALSE):
          $data_sp = AdminModel::get_data_final($id);
          $lokasi  = CommerceModel::get_lokasi($id, 1);
          $ld      = AdminModel::get_detail_boq($id, 1);
          $apm_all = ReportModel::get_apm_budget_all();

          foreach($ld as $k => $v)
          {
            $load_data[] = $v;
            $find_k = array_search($v->pid, array_column($apm_all, 'wbs') );

            if($find_k !== FALSE)
            {
              $be = $apm_all[$find_k];
              $load_data[$k]->mns = $be['keperluan']['Material : Non Stock'];
              $load_data[$k]->ms = $be['keperluan']['Material : Stock'];
            }
          }

          $get_wbs = DB::SELECT("SELECT pbl.id, (SELECT CONCAT_WS(',',
          ( CASE WHEN (SELECT SUM(pbd.material)
          FROM procurement_Boq_design pbd
          WHERE pbd.jenis_khs= 16) != 0 THEN '01-01-01' ELSE NULL END),
          ( CASE WHEN (SELECT SUM(pbd.material)
          FROM procurement_Boq_design pbd
          WHERE (pbd.jenis_khs != 16 OR pbd.jenis_khs IS NULL) ) != 0 THEN '01-01-02' ELSE NULL END),
          ( CASE WHEN (SELECT SUM(pbd.jasa)
          FROM procurement_Boq_design pbd ) != 0 THEN '01-02-01' ELSE NULL END) ) ) As idp
          FROM procurement_pid ppc
          LEFT JOIN procurement_req_pid prp ON ppc.id = prp.id_pid_req
          LEFT JOIN procurement_Boq_lokasi pbl ON pbl.id_upload = prp.id_upload
          WHERE prp.id_upload = $id GROUP BY pbl.id");

          return view('Commerce.Finish.update_pid', compact('data_sp', 'lokasi', 'load_data', 'get_pid', 'get_wbs') );
        break;
        case (in_array($data->step_id, [10, 31]) ? TRUE : FALSE):
          $data    = AdminModel::get_data_final($id);
          $get_pid = explode(', ', $data->id_project);
          return view('Area.Rekon.Finish.input_BA', compact('get_mitra', 'data', 'get_pid'));
        break;
        case ($data->step_id == 11 ? TRUE : FALSE):
          $data             = AdminModel::get_data_final($id);
          $title            = "Nomor Dokumen";
          $sub_title        = "Pengisian Nomor Dokumen Pelengkap";
          $data_osp         = Reportmodel::get_boq_data($id);
          $data_mitra       = AdminModel::get_mitra($data->mitra_id);
          $get_me           = ToolsModel::find_mitra(Session('auth')->mitra_amija_pt);
          $lampiran_dokumen = $this->lampiran_dokumen;
          $hss_witel        = DB::table('procurement_hss_psb')->where('witel', session('auth')->Witel_New)->first();
          return view('Mitra.Rekon.Finish.input_nomor_harga', compact('id', 'data', 'data_osp', 'data_mitra', 'get_me', 'lampiran_dokumen', 'title', 'sub_title', 'hss_witel') );
        break;
        case (in_array($data->step_id, [12, 17, 18, 29, 30]) ? TRUE : FALSE):
          $data_sp = AdminModel::get_data_final($id);
          return view('Mitra.Rekon.Finish.upload_ttd', compact('data_sp'));
        break;
      }
    }
    else
    {
      if(@$data->active == 0)
      {
        return back()->with('alerts', [
          ['type' => 'warning', 'text' => 'Pekerjaan Tidak Ada, Silahkan Contact Admin!']
        ]);
      }

      if(!$akses)
      {
        return back()->with('alerts', [
          ['type' => 'warning', 'text' => 'Anda Bukan Aktor Yang Mengerjakan Langkah Pekerjaan Ini!']
        ]);
      }
    }
	}

	public function crud_step($id, Request $req)
	{
    $data = Reportmodel::get_boq_data($id);
    $akses = false;
    $auth = session('auth')->proc_level;
    $check_validate = [];

    if($data)
    {
      $check_validate = AdminModel::check_step($data->step_id);
      $akses = in_array($auth, explode(',', $check_validate->aksesible));

      if($auth == 2 && $data->mitra_nm != session('auth')->mitra_amija_pt)
      {
        $akses = false;
      }
    }
    else
    {
      if(in_array($auth, [3, 4, 44, 99] ) )
      {
        if(session('auth')->proc_level == 44)
        {
          $find_witel = DB::Table('promise_witel As w1')
          ->leftjoin('promise_witel As w2', 'w2.Regional', '=', 'w1.Regional')
          ->where('w1.Witel', session('auth')->Witel_New)
          ->get();

          foreach($find_witel as $vw)
          {
            $all_witel[] = $vw->Witel;
          }

          if(!in_array($data->witel, $all_witel) )
          {
            return back()->with('alerts', [
              ['type' => 'danger', 'text' => 'Gagal Karena Berbeda Regional!']
            ]);
          }
        }

        if($data && session('auth')->Witel_New != $data->witel && in_array(session('auth')->proc_level, [4]) )
        {
          return back()->with('alerts', [
            ['type' => 'danger', 'text' => 'Gagal Karena Berbeda Witel!']
          ]);
        }

        $akses = true;
      }
    }
    // dd($check_validate, $id, $akses, $data);
    if( (is_null($data) || $data->active == 1 ) && $akses)
    {
      switch ($id) {
        case (preg_match('/[a-zA-Z]/', $id) && $id == 'input' ? TRUE : FALSE):
          return OperationModel::save_req_pid($req);
        break;
        case (in_array($data->step_id, [33]) ? TRUE : FALSE):
          return OperationModel::update_req_pid($req, $id);
        break;
        case (in_array($data->step_id, [1, 24, 27]) ? TRUE : FALSE):
          return CommerceModel::save_pid($req, $id);
        break;
        case ($data->step_id == 2 ? TRUE : FALSE):
          $msg = OperationModel::upload_justif_ttd($id, $req);
          return redirect($msg['direct'])->with('alerts', $msg['isi']);
        break;
        case (in_array($data->step_id, [3]) ? TRUE : FALSE):
          if($req->s_pen)
          {
            $split_tgl  = explode('-', $req->tgl_s_pen);
            $str        = preg_split("/[\/.-]/", $req->s_pen);
            $split_spen = array_reverse($str);

            if($split_spen[1] .'-'. $split_spen[0] != $split_tgl[1].'-'.$split_tgl[0])
            {
              return back()->with('alerts', [
                ['type' => 'danger', 'text' => 'Kode Surat Penetapan Tidak Sesuai!']
              ]);
            }
          }

          $msg = AdminModel::save_data_s($req, $id);
          return redirect($msg['direct'])->with('alerts', $msg['isi']);
        break;
        case (in_array($data->step_id, [4, 19, 15]) ? TRUE : FALSE):
          if($req->surat_kesanggupan)
          {
            $array_bln = array(1 => "I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
            $split_tgl = explode('-', $req->tgl_sggp);
            $str       = preg_split("/[\/.-]/", $req->surat_kesanggupan);
            $chck_sk   = array_reverse($str);

            if(preg_match("/[A-Za-z]/", $chck_sk[1]) )
            {
              $tgl_chck = $array_bln[(int)$split_tgl[1] ].'/'.$split_tgl[0];
              $nomor_s  = $chck_sk[1].'/'.$chck_sk[0];
            }
            else {
              $tgl_chck = $split_tgl[1] .'/'.$split_tgl[0];
              $nomor_s  = sprintf('%02d', $chck_sk[1]).'/'.$chck_sk[0];
            }

            if($tgl_chck != $nomor_s)
            {
              return back()->with('alerts', [
                ['type' => 'danger', 'text' => 'Kode Kesanggupan Tidak Sesuai!']
              ]);
            }
          }

          if($req->s_pen)
          {
            $split_tgl  = explode('-', $req->tgl_s_pen);
            $str        = preg_split("/[\/.-]/", $req->s_pen);
            $split_spen = array_reverse($str);

            if($split_spen[1] .'-'. $split_spen[0] != $split_tgl[1].'-'.$split_tgl[0])
            {
              return back()->with('alerts', [
                ['type' => 'danger', 'text' => 'Kode Surat Penetapan Tidak Sesuai!']
              ]);
            }
          }

          $msg = MitraModel::update_kesanggupan($req, $id);
          return redirect('/progressList/4')->with('alerts', $msg);
        break;
        case ($data->step_id == 5 ? TRUE : FALSE):
          if ($data->pekerjaan <> 'PSB')
          {
            if ($req->no_sp) {
              $split_tgl = explode('-', $req->tgl_sp);
              $str       = preg_split("/[\/.-]/", $req->no_sp);
              $split_sp  = array_reverse($str);

              if($split_sp[1] .'-'. $split_sp[0] != $split_tgl[1].'-'.$split_tgl[0])
              {
                return back()->with('alerts', [
                  ['type' => 'danger', 'text' => 'Kode Surat Pesanan Tidak Sesuai!']
                ]);
              }
            }

            if ($req->pks) {
              $split_tgl = explode('-', $req->tgl_pks);
              $str       = preg_split("/[\/.-]/", $req->pks);
              $split_pks  = array_reverse($str);

              if($split_pks[1] .'-'. $split_pks[0] != $split_tgl[1].'-'.$split_tgl[0])
              {
                return back()->with('alerts', [
                  ['type' => 'danger', 'text' => 'Kode PKS Tidak Sesuai!']
                ]);
              }
            }
          }

          AdminModel::update_progress_sp($req, $id);
          return redirect('/progressList/5')->with('alerts', [
            ['type' => 'success', 'text' => 'Dokumen SP berhasil Diperbaharui!']
          ]);
        break;
        case (in_array($data->step_id, [6, 16]) ? TRUE : FALSE):
          return MitraModel::save_rfc($req, $id);
        break;
        case ($data->step_id == 7 ? TRUE : FALSE):
          $msg = AdminModel::save_rekon($id, $req);
          return redirect($msg['direct'])->with('alerts', $msg['isi'])->withInput();
        break;
        case ($data->step_id == 8 ? TRUE : FALSE):
          $msg = AdminModel::save_pelurusan($id, $req);
          return redirect($msg['direct'])->with('alerts', $msg['isi']);
        break;
        case (in_array($data->step_id, [9, 28]) ? TRUE : FALSE):
          return CommerceModel::update_pid($id, $req);
        break;
        case (in_array($data->step_id, [10, 31]) ? TRUE : FALSE):
          if($req->amd_sp)
          {
            $split_tgl    = explode('-', $req->tgl_amd_sp);
            $str          = preg_split("/[\/.-]/", $req->amd_sp);
            $split_amd_sp = array_reverse($str);

            if($split_amd_sp[1].'/'.$split_amd_sp[0] != $split_tgl[1].'/'.$split_tgl[0])
            {
              return redirect('/progress/'.$id)->with('alerts', [
                ['type' => 'danger', 'text' => 'Kode Amandemen SP Tidak Sesuai!']
              ]);
            }
          }

          if($req->amd_pks)
          {
            $split_tgl     = explode('-', $req->tgl_amd_pks);
            $str           = preg_split("/[\/.-]/", $req->amd_pks);
            $split_amd_pks = array_reverse($str);

            if($split_amd_pks[1].'/'.$split_amd_pks[0] != $split_tgl[1].'/'.$split_tgl[0])
            {
              return redirect('/progress/'.$id)->with('alerts', [
                ['type' => 'danger', 'text' => 'Kode Amandemen PKS Tidak Sesuai!']
              ]);
            }
          }

          if($req->baut)
          {
            $split_tgl  = explode('-', $req->tgl_baut);
            $str        = preg_split("/[\/.-]/", $req->baut);
            $split_baut = array_reverse($str);

            if($split_baut[1] .'-'. $split_baut[0] != $split_tgl[1].'-'.$split_tgl[0])
            {
              return redirect('/progress/'.$id)->with('alerts', [
                ['type' => 'danger', 'text' => 'Kode BAUT Tidak Sesuai!']
              ]);
            }
          }

          if($req->bast)
          {
            $split_tgl  = explode('-', $req->tgl_bast);
            $str        = preg_split("/[\/.-]/", $req->bast);
            $split_bast = array_reverse($str);

            if($split_bast[1] .'-'. $split_bast[0] != $split_tgl[1].'-'.$split_tgl[0])
            {
              return redirect('/progress/'.$id)->with('alerts', [
                ['type' => 'danger', 'text' => 'Kode BAST Tidak Sesuai!']
              ]);
            }
          }

          if($req->ba_abd)
          {
            $split_tgl    = explode('-', $req->tgl_ba_abd);
            $str          = preg_split("/[\/.-]/", $req->ba_abd);
            $split_ba_abd = array_reverse($str);

            if($split_ba_abd[1] .'-'. $split_ba_abd[0] != $split_tgl[1].'-'.$split_tgl[0])
            {
              return redirect('/progress/'.$id)->with('alerts', [
                ['type' => 'danger', 'text' => 'Kode BA ABD Tidak Sesuai!']
              ]);
            }
          }

          if($req->ba_rekon)
          {
            $split_tgl      = explode('-', $req->tgl_ba_rekon);
            $str            = preg_split("/[\/.-]/", $req->ba_rekon);
            $split_ba_rekon = array_reverse($str);

            if($split_ba_rekon[1] .'-'. $split_ba_rekon[0] != $split_tgl[1].'-'.$split_tgl[0])
            {
              return redirect('/progress/'.$id)->with('alerts', [
                ['type' => 'danger', 'text' => 'Kode BA REKON Tidak Sesuai!']
              ]);
            }
          }

          if($req->bapp)
          {
            $split_tgl  = explode('-', $req->tgl_bapp);
            $str        = preg_split("/[\/.-]/", $req->bapp);
            $split_bapp = array_reverse($str);

            if($split_bapp[1] .'-'. $split_bapp[0] != $split_tgl[1].'-'.$split_tgl[0])
            {
              return redirect('/progress/'.$id)->with('alerts', [
                ['type' => 'danger', 'text' => 'Kode BAPP Tidak Sesuai!']
              ]);
            }
          }

          $msg = AdminModel::save_rekon_final($req, $id);
          return redirect('/progressList/10')->with('alerts', $msg);
        break;
        case ($data->step_id == 11 ? TRUE : FALSE):
          // dd($req->all());
          if ($req->pekerjaan <> "PSB")
          {
            $array_bln = array(1 => "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X", "XI", "XII");
            $split_tgl = explode('-', $req->tgl_faktur);

            if ($req->spp_num) {
              $str      = preg_split("/[\/.-]/", $req->spp_num);
              $chck_spp = array_reverse($str);

              if(strlen($chck_spp[0]) != 4)
              {
                $year = date('y', strtotime($req->tgl_faktur) );
              }
              else
              {
                $year = $split_tgl[0];
              }

              $tgl_im = $array_bln[(int)$split_tgl[1]] . '/' . $year;

              if (!isset($chck_spp[1]) || $tgl_im != $chck_spp[1] . '/' . $chck_spp[0]) {
                return back()->with('alerts', [
                  ['type' => 'danger', 'text' => 'Kode SPP Tidak Sesuai!']
                ])->withInput();
              }
            }

            if ($req->receipt_num) {
              $str          = preg_split("/[\/.-]/", $req->receipt_num);
              $chck_receipt = array_reverse($str);

              if(strlen($chck_receipt[0]) != 4)
              {
                $year = date('y', strtotime($req->tgl_faktur) );
              }
              else
              {
                $year = $split_tgl[0];
              }

              $tgl_im = $array_bln[(int)$split_tgl[1]] . '/' . $year;

              if (!isset($chck_receipt[1]) || $tgl_im != $chck_receipt[1] . '/' . $chck_receipt[0]) {
                return back()->with('alerts', [
                  ['type' => 'danger', 'text' => 'Kode Kwitansi Tidak Sesuai!']
                ])->withInput();
              }
            }

            if ($req->no_invoice) {
              $str          = preg_split("/[\/.-]/", $req->no_invoice);
              $chck_invoice = array_reverse($str);

              if(strlen($chck_invoice[0]) != 4)
              {
                $year = date('y', strtotime($req->tgl_faktur) );
              }
              else
              {
                $year = $split_tgl[0];
              }

              $tgl_im = $array_bln[(int)$split_tgl[1]] . '/' . $year;

              if (!isset($chck_invoice[1]) || $tgl_im != $chck_invoice[1] . '/' . $chck_invoice[0]) {
                return back()->with('alerts', [
                  ['type' => 'danger', 'text' => 'Kode Invoice Tidak Sesuai!']
                ])->withInput();
              }
            }

            // if ($req->no_faktur) {
            //   $str         = preg_split("/[\/.-]/", $req->no_faktur);
            //   $chck_faktur = array_reverse($str);

            //   if (!isset($chck_faktur[1]) || $tgl_im != $chck_faktur[1] . '/' . $chck_faktur[0]) {
            //     return back()->with('alerts', [
            //       ['type' => 'danger', 'text' => 'Kode Faktur Tidak Sesuai!']
            //     ])->withInput();
            //   }
            // }
          }

          MitraModel::save_ops($req, $id);
          if ($req->pekerjaan == "PSB")
          {
            return redirect('/progressList/12')->with('alerts', [
              ['type' => 'success', 'text' => 'Berhasil Melengkapi Nomor Surat dan Dokumen Lainnya!']
            ]);
          } else {
            return redirect('/progressList/11')->with('alerts', [
              ['type' => 'success', 'text' => 'Berhasil Tambah Nomor Mitra!']
            ]);
          }
        break;
        case (in_array($data->step_id, [12, 17, 18, 29, 30]) ? TRUE : FALSE):
          $msg = MitraModel::save_ttd($req, $id);
          return redirect('/progressList/12')->with('alerts', $msg);
        break;
        default:
        break;
      }
    }
    else
    {
      if(@$data->active == 0)
      {
        return back()->with('alerts', [
          ['type' => 'warning', 'text' => 'Pekerjaan Tidak Ada, Silahkan Contact Admin!']
        ]);
      }

      if(!$akses)
      {
        return back()->with('alerts', [
          ['type' => 'warning', 'text' => 'Anda Bukan Aktor Yang Mengerjakan Langkah Pekerjaan Ini!']
        ]);
      }
    }
	}

	public function list_step($id)
	{
    $akses = null;
    // $check_budget   = ReportModel::get_pid_single_BE($id);
    $auth           = session('auth')->proc_level;
    $check_validate = AdminModel::check_step($id);

    if($check_validate)
    {
      $akses = in_array($auth, explode(',', $check_validate->aksesible) );
    }

    if(in_array($auth, [3, 4, 44, 99] ) )
    {
      $akses = true;
    }

    if($akses)
    {
      switch ($id) {
        case (preg_match('/[a-zA-Z]/', $id) && $id == 'input' ? TRUE : FALSE):
          $data_active = OperationModel::get_data_req('awal', 1);
          $data_delete = OperationModel::get_data_req('all', 0);
          $all_data    = OperationModel::get_data_req('all', 1);
          $draft_data  = OperationModel::get_data_req(33, 1);
          return view('Report.Operation.ListRequest', compact('data_active', 'data_delete', 'all_data', 'draft_data') );
        break;
        case 1:
          $data_siap      = CommerceModel::get_data_req(1);
          $revok          = CommerceModel::get_list_bex([27]);
          $data_budget_ex = CommerceModel::get_list_bex([24, 26]);
          $apm_all        = ReportModel::get_apm_budget_all();
          $data_ex = $data_revok = [];

          foreach ($revok as $key => $v) {
            $data_revok[$v->id]['jenis_work']      = $v->jenis_work;
            $data_revok[$v->id]['judul']           = $v->judul;
            $data_revok[$v->id]['id_project']      = $v->id_project;
            $data_revok[$v->id]['created_at']      = $v->created_at;
            $data_revok[$v->id]['detail']          = $v->detail;
            $data_revok[$v->id]['jml_hri']         = $v->jml_hri;
            $data_revok[$v->id]['created_by']      = $v->created_by;
            $data_revok[$v->id]['tgl_last_update'] = $v->tgl_last_update;
            $data_revok[$v->id]['modified_by']     = $v->nama_modif .'('. $v->modified_by .')';

            if(!isset($data_revok[$v->id]['total_stock']) )
            {
              $data_revok[$v->id]['total_stock'] = [];
              $data_revok[$v->id]['total_non_stock'] = [];
            }

            $find_k = array_search($v->pid, array_column(json_decode(json_encode($apm_all, TRUE) ), 'wbs') );

            if($find_k !== FALSE)
            {
              $data_apm = $apm_all[$find_k];
              $data_revok[$v->id]['apm'][$find_k]['wbs'] = $data_apm['wbs'];
              $data_revok[$v->id]['apm'][$find_k]['keperluan'] = $data_apm['keperluan'];

              $data_revok[$v->id]['total_stock'][$find_k] = str_replace(',', '', $data_apm['keperluan']['Material : Stock']);
              $data_revok[$v->id]['total_non_stock'][$find_k] = str_replace(',', '', $data_apm['keperluan']['Material : Non Stock']);
            }
            else
            {
              $data_revok[$v->id]['apm'][0]['wbs'] = null;
              $data_revok[$v->id]['apm'][0]['keperluan'] = null;
            }
          }

          foreach ($data_budget_ex as $key => $v) {
            $data_ex[$v->id]['jenis_work']  = $v->jenis_work;
            $data_ex[$v->id]['judul']       = $v->judul;
            $data_ex[$v->id]['id_project']  = $v->id_project;
            $data_ex[$v->id]['created_at']  = $v->created_at;
            $data_ex[$v->id]['jml_hri']     = $v->jml_hri;
            $data_ex[$v->id]['created_by']  = $v->created_by;
            $data_ex[$v->id]['modified_by'] = $v->nama_modif .'('. $v->modified_by .')';
            $data_ex[$v->id]['detail']      = $v->detail;

            if(array_filter([$v->be_mt, $v->be_j, $v->be_mm] ) )
            {
              $data_ex[$v->id]['exceeded'][$v->id_pid] = array_filter([$v->be_mt, $v->be_j, $v->be_mm] );

              $find_k = array_search($v->pid, array_column(json_decode(json_encode($apm_all, TRUE) ), 'wbs') );

              if($find_k !== FALSE)
              {
                $data_apm = $apm_all[$find_k];
                $data_ex[$v->id]['apm'][$find_k]['wbs'] = $data_apm['wbs'];
                $data_ex[$v->id]['apm'][$find_k]['keperluan'] = $data_apm['keperluan'];
              }
              else
              {
                $data_ex[$v->id]['apm'][0]['wbs'] = null;
                $data_ex[$v->id]['apm'][0]['keperluan'] = null;
              }
            }
          }

          return view('Report.Commerce.list_pid', compact('data_siap', 'data_revok'), ['data_budget_ex' => $data_ex] );
        break;
        case 2:
          $data_ready = OperationModel::ready_justif(2);
          $proses     = OperationModel::ready_justif(3);
          return view('Report.Operation.list_justif', compact('data_ready', 'proses')) ;
        break;
        case 3:
          $new  = AdminModel::get_list_sp(3);
          $wait = AdminModel::get_list_sp(4);
          $be   = AdminModel::get_list_sp(26);
          return view('Report.Area.Rekon.PraKerja.list_spen', compact('new', 'wait') );
        break;
        case 4:
          $avail   = AdminModel::get_list_sp(4);
          $sanggup = AdminModel::get_list_sp(5);
          $save    = AdminModel::get_list_sp(19);
          $reject  = AdminModel::get_list_sp(15);
          return view('Report.Mitra.list_spen', compact('avail', 'sanggup', 'save', 'reject') );
        break;
        case (in_array($id, [5, 15]) ? TRUE : FALSE):
          $sanggup = AdminModel::get_list_sp(5);
          $approve = AdminModel::get_list_sp(6);
          $bulan_format = DB::SELECT('SELECT id, bulan as text, date_periode1, date_periode2 FROM bulan_format');
          $hss_witel = DB::table('procurement_hss_psb')->where('witel', session('auth')->Witel_New)->first();
          $get_mitra = DB::SELECT('SELECT id, REPLACE(nama_company,"PT","PT.") as text FROM procurement_mitra WHERE witel = "' . session('auth')->Witel_New . '" GROUP BY nama_company');
          return view('Report.Area.Rekon.PraKerja.list_sp', compact('sanggup', 'approve' , 'bulan_format', 'get_mitra', 'hss_witel') );
        break;
        case 6:
          //submit satu paket sama list
          $siap_berkas   = AdminModel::get_data_rekon(6);
          $wait_berkas   = AdminModel::get_data_rekon(7);
          $reject_berkas = AdminModel::get_data_rekon(16);
          return view('Report.Mitra.list_rekon_prog', compact('siap_berkas', 'wait_berkas', 'reject_berkas') );
        break;
        case 7:
          //submit satu paket sama list
          $ready_rekon  = AdminModel::get_data_rekon(7);
          $reject_rekon = AdminModel::get_data_rekon(16);
          $select2      = AdminModel::get_steps_by_urutan(8);
          $hss_witel = DB::table('procurement_hss_psb')->where('witel', session('auth')->Witel_New)->first();
          return view('Report.Area.Rekon.Finish.list_Verifyrekon', compact('ready_rekon', 'reject_rekon', 'select2', 'hss_witel') );
        break;
        case 8:
          $data_ready = MitraModel::get_ready_pelurusan(8);
          return view('Mitra.Rekon.Finish.pelurusan_material', compact('data_ready') );
        break;
        case 9:
          $data_avail     = CommerceModel::get_ready_budget(9);
          $data_budget_ex = CommerceModel::get_list_bex([28, 32]);
          $apm_all        = ReportModel::get_apm_budget_all();

          $data_ex = $pid_list = $data_ada = [];

          foreach ($data_avail as $key => $v) {
            $data_ada[$v->id]['jenis_work']      = $v->jenis_work;
            $data_ada[$v->id]['judul']           = $v->judul;
            $data_ada[$v->id]['id_project']      = $v->id_project;
            $data_ada[$v->id]['created_at']      = $v->created_at;
            $data_ada[$v->id]['detail']          = $v->detail;
            $data_ada[$v->id]['jml_hri']         = $v->jml_hri;
            $data_ada[$v->id]['created_by']      = $v->created_by;
            $data_ada[$v->id]['tgl_last_update'] = $v->tgl_last_update;
            $data_ada[$v->id]['modified_by']     = $v->nama_modif .'('. $v->modified_by .')';

            $find_k = array_search($v->pid, array_column(json_decode(json_encode($apm_all, TRUE) ), 'wbs') );

            if(!isset($data_ada[$v->id]['total_stock']) )
            {
              $data_ada[$v->id]['total_stock'] = [];
              $data_ada[$v->id]['total_non_stock'] = [];
            }

            if($find_k !== FALSE)
            {
              $data_apm = $apm_all[$find_k];
              $data_ada[$v->id]['apm'][$find_k]['wbs'] = $data_apm['wbs'];
              $data_ada[$v->id]['apm'][$find_k]['keperluan'] = $data_apm['keperluan'];

              $data_ada[$v->id]['total_stock'][$find_k] = str_replace(',', '', $data_apm['keperluan']['Material : Stock']);
              $data_ada[$v->id]['total_non_stock'][$find_k] = str_replace(',', '', $data_apm['keperluan']['Material : Non Stock']);
            }
            else
            {
              $data_ada[$v->id]['apm'][0]['wbs'] = null;
              $data_ada[$v->id]['apm'][0]['keperluan'] = null;
            }
          }
          foreach ($data_budget_ex as $key => $v) {
            $data_ex[$v->id]['jenis_work']  = $v->jenis_work;
            $data_ex[$v->id]['judul']       = $v->judul;
            $data_ex[$v->id]['id_project']  = $v->id_project;
            $data_ex[$v->id]['created_at']  = $v->created_at;
            $data_ex[$v->id]['detail']      = $v->detail;
            $data_ex[$v->id]['jml_hri']     = $v->jml_hri;
            $data_ex[$v->id]['created_by']  = $v->created_by;
            $data_ex[$v->id]['modified_by'] = $v->nama_modif .'('. $v->modified_by .')';

            if(array_filter([$v->be_mt, $v->be_j, $v->be_mm] ) )
            {
              $data_ex[$v->id]['exceeded'][$v->id_pid] = array_filter([$v->be_mt, $v->be_j, $v->be_mm] );

              $find_k = array_search($v->pid, array_column(json_decode(json_encode($apm_all, TRUE) ), 'wbs') );

              if($find_k !== FALSE)
              {
                $data_apm = $apm_all[$find_k];
                $data_ex[$v->id]['apm'][$find_k]['wbs'] = $data_apm['wbs'];
                $data_ex[$v->id]['apm'][$find_k]['keperluan'] = $data_apm['keperluan'];
              }
              else
              {
                $data_ex[$v->id]['apm'][0]['wbs'] = null;
                $data_ex[$v->id]['apm'][0]['keperluan'] = null;
              }
            }
          }

          return view('Report.Commerce.list_req_budget', compact('data_ada', 'data_ex') );
        break;
        case 10:
          $ready_rekon  = AdminModel::get_data_rekon(10);
          $proses_rekon = AdminModel::get_data_rekon(11, 29, 30);
          $reject_rekon = AdminModel::get_data_rekon(18);
          $select2      = AdminModel::get_steps_by_urutan(11);
          $save_rekon   = AdminModel::get_data_rekon(31);
          $hss_witel = DB::table('procurement_hss_psb')->where('witel', session('auth')->Witel_New)->first();
          return view('Report.Area.Rekon.Finish.list_verify_boqRekon', compact('ready_rekon', 'save_rekon', 'proses_rekon', 'reject_rekon', 'select2', 'hss_witel'));
        break;
        case 11:
          $berkas_get         = AdminModel::get_data_rekon(11);
          $reject_berkas_area = AdminModel::get_data_rekon(17);
          return view('Report.Area.Rekon.Finish.list_rekon_kelengkapan', compact('berkas_get', 'reject_berkas_area'));
        break;
        case (in_array($id, [12, 17, 18, 29, 30]) ? TRUE : FALSE):
          $ready_rekon  = AdminModel::get_data_rekon(12, 17, 29, 30);
          $reject_rekon = AdminModel::get_data_rekon(18);
          return view('Report.Mitra.list_ttd', compact('ready_rekon', 'reject_rekon') );
        break;
        case 20:
          //submit satu paket sama list
          $ready_rekon  = AdminModel::get_data_rekon(20);
          $reject_rekon = AdminModel::get_data_rekon(18);
          return view('Report.Regional.list_rekon_regional', compact('ready_rekon', 'reject_rekon') );
        break;
        case 13:
          //submit satu paket sama list
          $ready_rekon  = AdminModel::get_data_rekon(13);
          $reject_rekon = AdminModel::get_data_rekon(18);
          return view('Report.finance.list_rekon_finish', compact('ready_rekon', 'reject_rekon') );
        break;
        default:
        break;
      }
    }
    else
    {
      if(!$akses)
      {
        return back()->with('alerts', [
          ['type' => 'warning', 'text' => 'Anda Bukan Aktor Yang Bisa Melihat List Pekerjaan Ini!']
        ]);
      }
    }
	}

	public function crud_list_step($id, Request $req)
	{
		switch ($id) {
      case 13:
        $msg = AdminModel::save_rekon_finance($req);
        return redirect('/progressList/13')->with('alerts', $msg);
      break;
      case 20:
        $msg = AdminModel::save_rekon_reg($req);
        return redirect('/progressList/20')->with('alerts', $msg);
      break;
      default:
      break;
    }
	}

  public function rollback_me($id)
  {
    $data_sp = ReportModel::get_boq_data($id, 3);

    if($data_sp->urutan >= 13)
    {
      return redirect("/home")->with('alerts', [
        ['type' => 'warning', 'text' => 'Pekerjaan Sedang Dievaluasi, Tidak Bisa Dirollback!']
      ]);
    }

    if($data_sp->urutan == 1.1)
    {
      return redirect("/home")->with('alerts', [
        ['type' => 'warning', 'text' => 'Tidak Bisa Dipaksa Rollback!']
      ]);
    }
    // elseif($data_sp->action == 'backward')
    // {
      // return redirect("/home")->with('alerts', [
      //   ['type' => 'warning', 'text' => 'Pekerjaan Harus Yang Lurus!']
      // ]);
    // }
    else
    {
      if($data_sp->action == 'stay')
      {
        $get_stay_id = DB::Table('procurement_step')->where('id', $data_sp->step_id)->first();
			  $find_step_id = DB::Table('procurement_step')->where('step_after', $get_stay_id->step_after)->first();

        $find_prev = DB::table('procurement_step')->where([
          ['action', 'forward'],
          ['step_after', $find_step_id->id]
        ])
        ->orderBy('urutan', 'ASC')
        ->first();
        // dd($find_prev, $urut, $data_sp);
        $data_sp->nama       = $find_prev->nama;
        $data_sp->aktor_nama = $find_prev->aktor_nama;
        $data_sp->action     = $find_prev->action;
      }

      if($data_sp->action == 'backward')
      {
        $get_stay_id = DB::Table('procurement_step')->where('id', $data_sp->step_id)->first();
			  $find_step_id = DB::Table('procurement_step')->where('urutan', floor($get_stay_id->urutan) )->first();
        // dd($find_prev, $urut, $data_sp);
        $data_sp->nama       = $find_step_id->nama;
        $data_sp->aktor_nama = $find_step_id->aktor_nama;
        $data_sp->action     = $find_step_id->action;
      }

      if(empty($data_sp->nama) )
      {
        return back()->with('alerts', [
					['type' => 'warning', 'text' => 'Loker Sebelumnya Tidak Ditemukan!']
				]);
      }

      $get_prev_step_first = DB::table('procurement_step As a')
      ->leftjoin('procurement_step As b', 'b.id', '=', 'a.step_after')
      ->select('a.id As true_id', 'b.*')
      ->Where('a.step_after', $data_sp->step_id)
      ->first();

      $get_prev_step_all = DB::table('procurement_step As a')
      ->leftjoin('procurement_step As b', 'b.id', '=', 'a.step_after')
      ->select('a.id As true_id', 'b.*')
      ->Where([
        ['a.urutan', '<', $data_sp->urutan],
        ['a.action', 'forward']
      ])
      ->orderBy('b.urutan', 'DESC')
      ->GroupBy('b.urutan')
      ->get();


      return view('Tools.rollback', compact('data_sp', 'get_prev_step_all') );
    }
  }

  public function update_rollback(Request $req, $id)
	{
    AdminModel::rollback_process($req, $id);
    return redirect("/home")->with('alerts', [
      ['type' => 'warning', 'text' => 'Pekerjaan Berhasil Dikembalikan!']
    ]);
	}

	public function edit_pekerjaan($id)
	{
		$data = AdminModel::get_data_final($id);
    $get_pid = explode(', ', $data->id_project);

    if($data->step_id <= 5)
    {
      $get_mitra = ToolsModel::find_mitra(Session('auth')->mitra_amija_pt);
      return view('Tools.edit_pekerjaan_pra', compact('data', 'get_mitra', 'get_pid') );
    }
    else
    {
      $data_mitra       = AdminModel::get_mitra($data->mitra_id);
      $get_me           = ToolsModel::find_mitra(Session('auth')->mitra_amija_pt);
      $lampiran_dokumen = $this->lampiran_dokumen;
      return view('Tools.edit_pekerjaan_pasca', compact('data', 'get_mitra', 'get_pid') );
    }
	}

	public function update_pekerjaan(Request $req, $id)
	{
    return ReportModel::update_pekerjaan($req, $id);
	}

  public static function get_detail_proaktif(Request $req)
  {
    return AdminModel::get_detail_proaktif($req->id);
  }
}