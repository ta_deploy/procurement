<?php

namespace App\Http\Controllers;

use App\DA\AdminModel;
use App\DA\CommerceModel;
use App\DA\ReportModel;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Input;

date_default_timezone_set("Asia/Makassar");

class CommerceController extends Controller
{
	public function search_pid(Request $req)
	{
		// $data_pid = CommerceModel::get_pid_ajx($req->searchTerm, $req->id_upload);
    $data = $result = [];
		// foreach($data_pid as $d)
		// {
		// 	$pid = $d->pid;
		// 	if(!isset($result[$pid]) )
		// 	{
		// 		$result[$pid]['id']         = $d->id;
		// 		$result[$pid]['pid']        = $pid;
		// 		$result[$pid]['keterangan'] = $d->keterangan;
		// 		$result[$pid]['budget']     = $d->budget;
		// 		$result[$pid]['created_at'] = $d->created_at;
		// 		$result[$pid]['created_by'] = $d->created_by;
		// 		$result[$pid]['budget_all'] =  0;
		// 	}
		// 	$result[$pid]['budget_all'] += $d->budget_up;

		// 	if($d->active == 1)
		// 	{
		// 		$result[$pid]['judul'][] = $d->judul;
		// 	}
		// }

    // foreach($result as $row)
    // {
		// 	$data[] = array("id" => $row['id'], "text" => $row['pid'] );
    // }
		$data_apm = ReportModel::get_apm_budget_all($req->searchTerm);
		$get_all_pp = CommerceModel::all_procurement_pid();

		foreach($data_apm as $k => $v)
		{
			$find_k = array_search($v['wbs'], array_column(json_decode(json_encode($get_all_pp, TRUE) ), 'pid') );

			$data[$k]['id'] = $v['wbs'];
			$data[$k]['text'] = $v['wbs'];
			$data[$k]['keperluan'] = $v['keperluan'];

			if($find_k !== FALSE)
			{
				// $pp = $get_all_pp[$find_k];
				// $data[$k]['id'] = $pp->id;
			}
		}

    return \Response::json(array_slice($data, 0, 20, true) );
	}

	public function rekon_be(Request $req, $id)
	{
		$data = CommerceModel::update_rekon_be($req, $id);

		return redirect('/progressList/9')->with('alerts', [
      ['type' => 'warning', 'text' => "Pekerjaan $data->judul Berhasil Dipindah Ke Loker Budget Exceeded"]
    ]);
	}

	public function upload_po()
	{
		$data = CommerceModel::List_PO();
		return view("Commerce.upload_po", compact('data') );
	}

	public function insert_po()
	{
		return view("Commerce.insert_po");
	}

	public function save_po(Request $req)
	{
		return CommerceModel::insert_po($req);
	}

	public function get_proaktif_tematik(Request $req)
	{
		$data = CommerceModel::get_proaktif($req);
		return \Response::json($data);
	}

	public function edit_po($id)
	{
		$data = CommerceModel::find_po($id);
		return view("Commerce.insert_po", compact('data') );
	}

	public function update_po(Request $req, $id)
	{
		return CommerceModel::update_po($req, $id);
	}

}