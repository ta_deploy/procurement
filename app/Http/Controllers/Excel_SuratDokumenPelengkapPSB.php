<?php

namespace App\Http\Controllers;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Events\BeforeSheet;
use Maatwebsite\Excel\Events\BeforeWriting;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use Maatwebsite\Excel\Excel;
use DB;
class Excel_SuratDokumenPelengkapPSB implements  WithEvents
{
    use Exportable;

    public function __construct($data, $output_file)
    {
        $this->data = $data;
        $this->output_file = $output_file;
    }

    public function convert_month($date, $cetak_hari = false)
    {
        $bulan = array (1 =>   'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );

        $hari = array ( 1 => 'Senin',
            'Selasa',
            'Rabu',
            'Kamis',
            'Jumat',
            'Sabtu',
            'Minggu'
        );

        $result = date('d m Y', strtotime($date) );
        $split = explode(' ', $result);
        $hasilnya = $split[0] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[2];

        if($cetak_hari)
        {
            $num = date('N', strtotime($date) );
            return $hari[$num].' '.$hasilnya;
        }

        return $hasilnya;
    }

    public function only_day($date)
    {
        $hari = array ( 1 => 'Senin',
            'Selasa',
            'Rabu',
            'Kamis',
            'Jumat',
            'Sabtu',
            'Minggu'
        );

        $num = date('N', strtotime($date) );
        return $hari[$num];
    }

    public function terbilang($nilai)
    {
        if ($nilai < 0)
        {
            $hasil = "minus " . trim($this->penyebut($nilai) );
        }
        else
        {
            $hasil = trim($this->penyebut($nilai) );
        }

        return $hasil;
    }

    public function penyebut($nilai)
    {
        $nilai = abs($nilai);
        $huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
        $temp = "";

        if($nilai < 12)
        {
            $temp = " " . $huruf[$nilai];
        }
        else if($nilai < 20)
        {
            $temp = $this->penyebut($nilai - 10) . " belas";
        }
        else if($nilai < 100)
        {
            $temp = $this->penyebut($nilai / 10) . " puluh" . $this->penyebut($nilai % 10);
        }
        else if($nilai < 200)
        {
            $temp = " seratus" . $this->penyebut($nilai - 100);
        }
        else if($nilai < 1000)
        {
            $temp = $this->penyebut($nilai / 100) . " ratus" . $this->penyebut($nilai % 100);
        }
        else if($nilai < 2000)
        {
            $temp = " seribu" . $this->penyebut($nilai - 1000);
        }
        else if($nilai < 1000000)
        {
            $temp = $this->penyebut($nilai / 1000) . " ribu" . $this->penyebut($nilai % 1000);
        }
        else if($nilai < 1000000000)
        {
            $temp = $this->penyebut($nilai / 1000000) . " juta" . $this->penyebut($nilai % 1000000);
        }
        else if($nilai < 1000000000000)
        {
            $temp = $this->penyebut($nilai / 1000000000) . " milyar" . $this->penyebut(fmod($nilai, 1000000000) );
        }
        else if($nilai < 1000000000000000)
        {
            $temp = $this->penyebut($nilai / 1000000000000) . " trilyun" . $this->penyebut(fmod($nilai, 1000000000000) );
        }

        return$temp;
    }

    public function ttd_user($rule)
    {
        return DB::table('procurement_dok_ttd')
        ->leftJoin('procurement_user', 'procurement_dok_ttd.id_user', '=', 'procurement_user.id')
        ->select('procurement_user.*')
        ->where('procurement_dok_ttd.witel', $this->data->witel)
        ->where('procurement_dok_ttd.pekerjaan', 'PSB')
        ->where('procurement_dok_ttd.rule_dok', $rule)
        ->first();
    }

    public function verificator_proc($nik)
    {
        return DB::table('1_2_employee')->where('nik', $nik)->first();
    }

    public function registerEvents(): array
    {
        return [
            BeforeExport::class  => function(BeforeExport $event) {

                $reader = new Xlsx();
                \PhpOffice\PhpSpreadsheet\Shared\File::setUseUploadTempDirectory(true);
                $spreadSheet = $reader->load(public_path(). '/template_doc/psb/khs_2021/Dokumen_Pelengkap_Lainnya.xlsx');

                $spreadSheet->getSheet(0)->getCell('B1')->SetValue($this->data->nama_company);
                $spreadSheet->getSheet(0)->getCell('B2')->SetValue('Pekerjaan '.$this->data->jenis_work.' Periode '.$this->data->month.' '.date('Y', strtotime($this->data->tgl_amd_sp)).' Witel '. ucfirst(strtolower($this->data->witel)));
                $spreadSheet->getSheet(0)->getCell('B3')->SetValue($this->data->alamat_company);
                $spreadSheet->getSheet(0)->getCell('B4')->SetValue($this->data->telp_mitra);
                $spreadSheet->getSheet(0)->getCell('B5')->SetValue($this->data->no_sp);
                $spreadSheet->getSheet(0)->getCell('B6')->SetValue($this->convert_month($this->data->tgl_sp));
                $spreadSheet->getSheet(0)->getCell('B7')->SetValue($this->data->amd_sp);
                $spreadSheet->getSheet(0)->getCell('B8')->SetValue($this->data->invoice);
                $spreadSheet->getSheet(0)->getCell('B9')->SetValue($this->data->no_kontrak_ta);
                $spreadSheet->getSheet(0)->getCell('B10')->SetValue($this->data->id_project);
                $spreadSheet->getSheet(0)->getCell('B11')->SetValue($this->data->NPWP);
                $spreadSheet->getSheet(0)->getCell('B12')->SetValue($this->data->rek);
                $spreadSheet->getSheet(0)->getCell('B13')->SetValue($this->data->bank);
                $spreadSheet->getSheet(0)->getCell('B14')->SetValue($this->data->cabang_bank);
                $spreadSheet->getSheet(0)->getCell('B15')->SetValue($this->data->atas_nama);
                $spreadSheet->getSheet(0)->getCell('B16')->SetValue(ucwords(strtolower($this->data->witel)));
                $spreadSheet->getSheet(0)->getCell('B17')->SetValue($this->data->area);
                $spreadSheet->getSheet(0)->getCell('B18')->SetValue($this->data->bulan);
                $spreadSheet->getSheet(0)->getCell('B19')->SetValue($this->data->spp_num);
                $spreadSheet->getSheet(0)->getCell('B20')->SetValue($this->convert_month($this->data->date_kontrak_ta));
                $spreadSheet->getSheet(0)->getCell('B21')->SetValue($this->data->wakil_mitra);
                $spreadSheet->getSheet(0)->getCell('B22')->SetValue($this->data->jabatan_mitra);
                $spreadSheet->getSheet(0)->getCell('B23')->SetValue($this->data->receipt_num);
                $spreadSheet->getSheet(0)->getCell('B24')->SetValue($this->convert_month($this->data->tgl_faktur));
                $spreadSheet->getSheet(0)->getCell('B25')->SetValue($this->data->faktur);
                $spreadSheet->getSheet(0)->getCell('B26')->SetValue($this->data->no_baij);
                $spreadSheet->getSheet(0)->getCell('B27')->SetValue($this->data->persentase_7_kpi);
                $spreadSheet->getSheet(0)->getCell('B28')->SetValue($this->data->BAST);
                $spreadSheet->getSheet(0)->getCell('B29')->SetValue($this->only_day($this->data->rekon_periode2));
                $spreadSheet->getSheet(0)->getCell('B30')->SetValue(ucwords($this->terbilang(date('d', strtotime($this->data->rekon_periode2)))));
                $spreadSheet->getSheet(0)->getCell('B31')->SetValue($this->data->month);
                $spreadSheet->getSheet(0)->getCell('B32')->SetValue(date('d/m/Y', strtotime($this->data->rekon_periode2)));
                $spreadSheet->getSheet(0)->getCell('B33')->SetValue(ucwords($this->terbilang(date('Y', strtotime($this->data->rekon_periode2)))));
                $spreadSheet->getSheet(0)->getCell('B34')->SetValue($this->data->no_bap);
                $spreadSheet->getSheet(0)->getCell('B35')->SetValue($this->convert_month($this->data->rekon_periode1));
                $spreadSheet->getSheet(0)->getCell('B36')->SetValue($this->convert_month($this->data->rekon_periode2));
                $spreadSheet->getSheet(0)->getCell('B37')->SetValue($this->data->BAPP);
                $spreadSheet->getSheet(0)->getCell('B38')->SetValue($this->verificator_proc($this->data->verificator_by)->nama);
                $spreadSheet->getSheet(0)->getCell('B39')->SetValue($this->verificator_proc($this->data->verificator_by)->nik);
                $spreadSheet->getSheet(0)->getCell('B42')->SetValue($this->ttd_user('mngr_krj')->user);
                $spreadSheet->getSheet(0)->getCell('B43')->SetValue($this->ttd_user('mngr_krj')->nik);
                $spreadSheet->getSheet(0)->getCell('B44')->SetValue($this->ttd_user('gm')->user);
                $spreadSheet->getSheet(0)->getCell('B45')->SetValue($this->ttd_user('gm')->jabatan);
                $spreadSheet->getSheet(0)->getCell('B46')->SetValue($this->data->p1_survey_ssl);
                $spreadSheet->getSheet(0)->getCell('B47')->SetValue($this->data->p2_ssl);
                $spreadSheet->getSheet(0)->getCell('B48')->SetValue($this->data->p3_survey_ssl);
                $spreadSheet->getSheet(0)->getCell('B49')->SetValue($this->data->lme_wifi_pt1_ssl);
                $spreadSheet->getSheet(0)->getCell('B50')->SetValue($this->data->indihome_smart_ssl);
                $spreadSheet->getSheet(0)->getCell('B51')->SetValue($this->data->plc_ssl);
                $spreadSheet->getSheet(0)->getCell('B52')->SetValue($this->data->ikr_addon_stb_ssl);
                $spreadSheet->getSheet(0)->getCell('B53')->SetValue($this->data->wifiextender_ssl);
                $spreadSheet->getSheet(0)->getCell('B54')->SetValue($this->data->ont_premium_ssl);
                $spreadSheet->getSheet(0)->getCell('B55')->SetValue($this->data->total_nilai_sp);
                $spreadSheet->getSheet(0)->getCell('B58')->SetValue($this->data->pu_s7_140_hss);
                $spreadSheet->getSheet(0)->getCell('B59')->SetValue($this->data->pu_s7_140_ssl);
                $spreadSheet->getSheet(0)->getCell('B60')->SetValue($this->data->lme_ap_indoor_ssl);
                $spreadSheet->getSheet(0)->getCell('B61')->SetValue($this->data->lme_ap_outdoor_ssl);
                $spreadSheet->getSheet(0)->getCell('B62')->SetValue($this->data->ppn_numb);
                $spreadSheet->getSheet(0)->getCell('B63')->SetValue($this->terbilang($this->data->ppn_numb)." persen");
                $spreadSheet->getSheet(0)->getCell('B64')->SetValue($this->data->p1_survey_hss);
                $spreadSheet->getSheet(0)->getCell('B65')->SetValue($this->data->p2_hss);
                $spreadSheet->getSheet(0)->getCell('B66')->SetValue($this->data->p3_survey_hss);
                $spreadSheet->getSheet(0)->getCell('B67')->SetValue($this->data->ikr_addon_stb_hss);
                $spreadSheet->getSheet(0)->getCell('B68')->SetValue($this->data->wifiextender_hss);
                $spreadSheet->getSheet(0)->getCell('B69')->SetValue($this->data->ont_premium_hss);
                $spreadSheet->getSheet(0)->getCell('B70')->SetValue($this->data->pu_s7_140_hss);
                $spreadSheet->getSheet(0)->getCell('B71')->SetValue($this->data->lme_wifi_pt1_hss);
                $spreadSheet->getSheet(0)->getCell('B72')->SetValue($this->data->lme_ap_indoor_hss);
                $spreadSheet->getSheet(0)->getCell('B73')->SetValue($this->data->lme_ap_outdoor_hss);
                $spreadSheet->getSheet(0)->getCell('B74')->SetValue($this->data->indihome_smart_hss);
                $spreadSheet->getSheet(0)->getCell('B75')->SetValue($this->data->plc_hss);
                $spreadSheet->getSheet(0)->getCell('B76')->SetValue($this->data->area_sto);
                $spreadSheet->getSheet(0)->getCell('B77')->SetValue($this->data->no_ba_pemeliharaan);
                $spreadSheet->getSheet(0)->getCell('B78')->SetValue($this->data->pph_mitra);
                $spreadSheet->getSheet(0)->getCell('B79')->SetValue(''); // uraian NPK
                $spreadSheet->getSheet(0)->getCell('B80')->SetValue(''); // total BA Pemotongan
                $spreadSheet->getSheet(0)->getCell('B81')->SetValue($this->data->pu_s9_140_hss);
                $spreadSheet->getSheet(0)->getCell('B82')->SetValue($this->data->pu_s9_140_ssl);
                $spreadSheet->getSheet(0)->getCell('B83')->SetValue('0'); // traycable hss
                $spreadSheet->getSheet(0)->getCell('B84')->SetValue('0'); // traycable ssl
                $spreadSheet->getSheet(0)->getCell('B85')->SetValue($this->data->migrasi_service_1p2p_hss);
                $spreadSheet->getSheet(0)->getCell('B86')->SetValue($this->data->migrasi_service_1p2p_ssl);
                $spreadSheet->getSheet(0)->getCell('B87')->SetValue($this->data->migrasi_service_1p3p_hss);
                $spreadSheet->getSheet(0)->getCell('B88')->SetValue($this->data->migrasi_service_1p3p_ssl);
                $spreadSheet->getSheet(0)->getCell('B89')->SetValue($this->data->migrasi_service_2p3p_hss);
                $spreadSheet->getSheet(0)->getCell('B90')->SetValue($this->data->migrasi_service_2p3p_ssl);
                $spreadSheet->getSheet(0)->getCell('B92')->SetValue($this->data->p2_tlpint_survey_hss);
                $spreadSheet->getSheet(0)->getCell('B93')->SetValue($this->data->p2_tlpint_survey_ssl);
                $spreadSheet->getSheet(0)->getCell('B94')->SetValue($this->data->p2_intiptv_survey_hss);
                $spreadSheet->getSheet(0)->getCell('B95')->SetValue($this->data->p2_intiptv_survey_ssl);
                $spreadSheet->getSheet(0)->getCell('B96')->SetValue($this->data->p2_tlpint_hss);
                $spreadSheet->getSheet(0)->getCell('B97')->SetValue($this->data->p2_tlpint_ssl);
                $spreadSheet->getSheet(0)->getCell('B98')->SetValue($this->data->p2_intiptv_hss);
                $spreadSheet->getSheet(0)->getCell('B99')->SetValue($this->data->p2_intiptv_ssl);
                $spreadSheet->getSheet(0)->getCell('B100')->SetValue($this->data->p3_hss);
                $spreadSheet->getSheet(0)->getCell('B101')->SetValue($this->data->p3_ssl);
                $spreadSheet->getSheet(0)->getCell('B102')->SetValue($this->data->p1_hss);
                $spreadSheet->getSheet(0)->getCell('B103')->SetValue($this->data->p1_ssl);
                $spreadSheet->getSheet(0)->getCell('B104')->SetValue($this->data->change_stb_hss);
                $spreadSheet->getSheet(0)->getCell('B105')->SetValue($this->data->change_stb_ssl);
                $spreadSheet->getSheet(0)->getCell('B106')->SetValue($this->data->tgl_amd_sp);
                $spreadSheet->getSheet(0)->getCell('B107')->SetValue($this->convert_month($this->data->tgl_amd_sp));
                $spreadSheet->getSheet(0)->getCell('B108')->SetValue($this->data->tgl_faktur);
                $spreadSheet->getSheet(0)->getCell('B109')->SetValue($this->convert_month($this->data->tgl_faktur));
                $spreadSheet->getSheet(0)->getCell('B110')->SetValue($this->data->tgl_bast);
                $spreadSheet->getSheet(0)->getCell('B111')->SetValue($this->convert_month($this->data->tgl_bast));

                $jml_pekerjaan_psb = 
                    ($this->data->p1_survey_ssl * $this->data->p1_survey_hss) +
                    ($this->data->p2_tlpint_survey_ssl * $this->data->p2_tlpint_survey_hss) +
                    ($this->data->p2_intiptv_survey_ssl * $this->data->p2_intiptv_survey_hss) + 
                    ($this->data->p3_survey_ssl * $this->data->p3_survey_hss) +

                    ($this->data->p1_ssl * $this->data->p1_hss) +
                    ($this->data->p2_ssl * $this->data->p2_hss) + // versi khs 2021
                    ($this->data->p2_tlpint_ssl * $this->data->p2_tlpint_hss) +
                    ($this->data->p2_intiptv_ssl * $this->data->p2_intiptv_hss) + 
                    ($this->data->p3_ssl * $this->data->p3_hss);

                $jml_pekerjaan_migrasi =
                    ($this->data->migrasi_service_1p2p_ssl * $this->data->migrasi_service_1p2p_hss) +
                    ($this->data->migrasi_service_1p3p_ssl * $this->data->migrasi_service_1p3p_hss) +
                    ($this->data->migrasi_service_2p3p_ssl * $this->data->migrasi_service_2p3p_hss);

                $jml_pekerjaan_tambahan =
                    ($this->data->ikr_addon_stb_ssl * $this->data->ikr_addon_stb_hss) +
                    ($this->data->change_stb_ssl * $this->data->change_stb_hss) +
                    ($this->data->indihome_smart_ssl * $this->data->indihome_smart_hss) +
                    ($this->data->wifiextender_ssl * $this->data->wifiextender_hss) +
                    ($this->data->plc_ssl * $this->data->plc_hss) +
                    ($this->data->lme_wifi_pt1_ssl * $this->data->lme_wifi_pt1_hss) +
                    ($this->data->lme_ap_indoor_ssl * $this->data->lme_ap_indoor_hss) +
                    ($this->data->lme_ap_outdoor_ssl * $this->data->lme_ap_outdoor_hss) +
                    ($this->data->ont_premium_ssl * $this->data->ont_premium_hss) +
                    ($this->data->pu_s7_140_ssl * $this->data->pu_s7_140_hss) +
                    ($this->data->pu_s9_140_ssl * $this->data->pu_s9_140_hss);

                $imbal_jasa1 = ($jml_pekerjaan_psb + $jml_pekerjaan_migrasi) * (str_replace(',', '.', $this->data->persentase_7_kpi) / 100);
                $imbal_jasa2 = $jml_pekerjaan_tambahan;
                $total_imbal_jasa = $imbal_jasa1 + $imbal_jasa2;

                $nilai_dpp = substr_replace(floor($total_imbal_jasa), '000', -3, 3);
                $nilai_ppn = $nilai_dpp * (str_replace(',', '.', $this->data->ppn_numb) / 100);
                $nilai_kwitansi = ($nilai_dpp + $nilai_ppn);

                // dd($this->data->perfomansi_psb, $imbal_jasa1, $imbal_jasa2, $total_imbal_jasa, $nilai_dpp, $nilai_ppn, $nilai_kwitansi);

                $spreadSheet->getSheet(0)->getCell('B56')->SetValue(ucwords($this->terbilang($nilai_kwitansi)));
                $spreadSheet->getSheet(0)->getCell('B57')->SetValue(ucwords($this->terbilang($nilai_dpp)));

                if ($this->data->p1_survey_ssl == 0)
                {
                    // bairj
                    $spreadSheet->getSheet(8)->getCell('D19')->SetValue("");
                    $spreadSheet->getSheet(8)->getCell('E19')->SetValue("");
                    $spreadSheet->getSheet(8)->getCell('F19')->SetValue("");

                    // amd sp
                    $spreadSheet->getSheet(10)->getCell('E23')->SetValue("");
                    $spreadSheet->getSheet(10)->getCell('F23')->SetValue("");
                    $spreadSheet->getSheet(10)->getCell('G23')->SetValue("");
                }

                if ($this->data->p2_tlpint_survey_ssl == 0)
                {
                    // bairj
                    $spreadSheet->getSheet(8)->getCell('D20')->SetValue("");
                    $spreadSheet->getSheet(8)->getCell('E20')->SetValue("");
                    $spreadSheet->getSheet(8)->getCell('F20')->SetValue("");

                    // amd sp
                    $spreadSheet->getSheet(10)->getCell('E24')->SetValue("");
                    $spreadSheet->getSheet(10)->getCell('F24')->SetValue("");
                    $spreadSheet->getSheet(10)->getCell('G24')->SetValue("");
                }

                if ($this->data->p2_intiptv_survey_ssl == 0)
                {
                    // bairj
                    $spreadSheet->getSheet(8)->getCell('D21')->SetValue("");
                    $spreadSheet->getSheet(8)->getCell('E21')->SetValue("");
                    $spreadSheet->getSheet(8)->getCell('F21')->SetValue("");

                    // amd sp
                    $spreadSheet->getSheet(10)->getCell('E25')->SetValue("");
                    $spreadSheet->getSheet(10)->getCell('F25')->SetValue("");
                    $spreadSheet->getSheet(10)->getCell('G25')->SetValue("");
                }

                if ($this->data->p3_survey_ssl == 0)
                {
                    // bairj
                    $spreadSheet->getSheet(8)->getCell('D22')->SetValue("");
                    $spreadSheet->getSheet(8)->getCell('E22')->SetValue("");
                    $spreadSheet->getSheet(8)->getCell('F22')->SetValue("");

                    // amd sp
                    $spreadSheet->getSheet(10)->getCell('E26')->SetValue("");
                    $spreadSheet->getSheet(10)->getCell('F26')->SetValue("");
                    $spreadSheet->getSheet(10)->getCell('G26')->SetValue("");
                }

                if ($this->data->p1_ssl == 0)
                {
                    // bairj
                    $spreadSheet->getSheet(8)->getCell('D23')->SetValue("");
                    $spreadSheet->getSheet(8)->getCell('E23')->SetValue("");
                    $spreadSheet->getSheet(8)->getCell('F23')->SetValue("");

                    // amd sp
                    $spreadSheet->getSheet(10)->getCell('E27')->SetValue("");
                    $spreadSheet->getSheet(10)->getCell('F27')->SetValue("");
                    $spreadSheet->getSheet(10)->getCell('G27')->SetValue("");
                }

                if ($this->data->p2_ssl == 0)
                {
                    // bairj
                    $spreadSheet->getSheet(8)->getCell('D24')->SetValue("");
                    $spreadSheet->getSheet(8)->getCell('E24')->SetValue("");
                    $spreadSheet->getSheet(8)->getCell('F24')->SetValue("");

                    // amd sp
                    $spreadSheet->getSheet(10)->getCell('E28')->SetValue("");
                    $spreadSheet->getSheet(10)->getCell('F28')->SetValue("");
                    $spreadSheet->getSheet(10)->getCell('G28')->SetValue("");
                }

                if ($this->data->p2_tlpint_ssl == 0)
                {
                    // bairj
                    $spreadSheet->getSheet(8)->getCell('D25')->SetValue("");
                    $spreadSheet->getSheet(8)->getCell('E25')->SetValue("");
                    $spreadSheet->getSheet(8)->getCell('F25')->SetValue("");

                    // amd sp
                    $spreadSheet->getSheet(10)->getCell('E29')->SetValue("");
                    $spreadSheet->getSheet(10)->getCell('F29')->SetValue("");
                    $spreadSheet->getSheet(10)->getCell('G29')->SetValue("");
                }

                if ($this->data->p2_intiptv_ssl == 0)
                {
                    // bairj
                    $spreadSheet->getSheet(8)->getCell('D26')->SetValue("");
                    $spreadSheet->getSheet(8)->getCell('E26')->SetValue("");
                    $spreadSheet->getSheet(8)->getCell('F26')->SetValue("");

                    // amd sp
                    $spreadSheet->getSheet(10)->getCell('E30')->SetValue("");
                    $spreadSheet->getSheet(10)->getCell('F30')->SetValue("");
                    $spreadSheet->getSheet(10)->getCell('G30')->SetValue("");
                }

                if ($this->data->p3_ssl == 0)
                {
                    // bairj
                    $spreadSheet->getSheet(8)->getCell('D27')->SetValue("");
                    $spreadSheet->getSheet(8)->getCell('E27')->SetValue("");
                    $spreadSheet->getSheet(8)->getCell('F27')->SetValue("");

                    // amd sp
                    $spreadSheet->getSheet(10)->getCell('E31')->SetValue("");
                    $spreadSheet->getSheet(10)->getCell('F31')->SetValue("");
                    $spreadSheet->getSheet(10)->getCell('G31')->SetValue("");
                }

                if ($this->data->migrasi_service_1p2p_ssl == 0)
                {
                    // bairj
                    $spreadSheet->getSheet(8)->getCell('D44')->SetValue("");
                    $spreadSheet->getSheet(8)->getCell('E44')->SetValue("");
                    $spreadSheet->getSheet(8)->getCell('F44')->SetValue("");

                    // amd sp
                    $spreadSheet->getSheet(10)->getCell('E51')->SetValue("");
                    $spreadSheet->getSheet(10)->getCell('F51')->SetValue("");
                    $spreadSheet->getSheet(10)->getCell('G51')->SetValue("");
                }

                if ($this->data->migrasi_service_1p3p_ssl == 0)
                {
                    // bairj
                    $spreadSheet->getSheet(8)->getCell('D45')->SetValue("");
                    $spreadSheet->getSheet(8)->getCell('E45')->SetValue("");
                    $spreadSheet->getSheet(8)->getCell('F45')->SetValue("");

                    // amd sp
                    $spreadSheet->getSheet(10)->getCell('E52')->SetValue("");
                    $spreadSheet->getSheet(10)->getCell('F52')->SetValue("");
                    $spreadSheet->getSheet(10)->getCell('G52')->SetValue("");
                }

                if ($this->data->migrasi_service_2p3p_ssl == 0)
                {
                    // bairj
                    $spreadSheet->getSheet(8)->getCell('D46')->SetValue("");
                    $spreadSheet->getSheet(8)->getCell('E46')->SetValue("");
                    $spreadSheet->getSheet(8)->getCell('F46')->SetValue("");

                    // amd sp
                    $spreadSheet->getSheet(10)->getCell('E53')->SetValue("");
                    $spreadSheet->getSheet(10)->getCell('F53')->SetValue("");
                    $spreadSheet->getSheet(10)->getCell('G53')->SetValue("");
                }

                if ($this->data->lme_wifi_pt1_ssl == 0)
                {
                    // bairj
                    $spreadSheet->getSheet(8)->getCell('D49')->SetValue("");
                    $spreadSheet->getSheet(8)->getCell('E49')->SetValue("");
                    $spreadSheet->getSheet(8)->getCell('F49')->SetValue("");

                    // amd sp
                    $spreadSheet->getSheet(10)->getCell('E56')->SetValue("");
                    $spreadSheet->getSheet(10)->getCell('F56')->SetValue("");
                    $spreadSheet->getSheet(10)->getCell('G56')->SetValue("");
                }

                if ($this->data->lme_ap_indoor_ssl == 0)
                {
                    // bairj
                    $spreadSheet->getSheet(8)->getCell('D50')->SetValue("");
                    $spreadSheet->getSheet(8)->getCell('E50')->SetValue("");
                    $spreadSheet->getSheet(8)->getCell('F50')->SetValue("");

                    // amd sp
                    $spreadSheet->getSheet(10)->getCell('E57')->SetValue("");
                    $spreadSheet->getSheet(10)->getCell('F57')->SetValue("");
                    $spreadSheet->getSheet(10)->getCell('G57')->SetValue("");
                }

                if ($this->data->lme_ap_outdoor_ssl == 0)
                {
                    // bairj
                    $spreadSheet->getSheet(8)->getCell('D51')->SetValue("");
                    $spreadSheet->getSheet(8)->getCell('E51')->SetValue("");    
                    $spreadSheet->getSheet(8)->getCell('F51')->SetValue("");

                    // amd sp
                    $spreadSheet->getSheet(10)->getCell('E58')->SetValue("");
                    $spreadSheet->getSheet(10)->getCell('F58')->SetValue("");
                    $spreadSheet->getSheet(10)->getCell('G58')->SetValue("");
                }

                if ($this->data->indihome_smart_ssl == 0)
                {
                    // bairj
                    $spreadSheet->getSheet(8)->getCell('D52')->SetValue("");
                    $spreadSheet->getSheet(8)->getCell('E52')->SetValue("");
                    $spreadSheet->getSheet(8)->getCell('F52')->SetValue("");

                    //amd sp
                    $spreadSheet->getSheet(10)->getCell('E60')->SetValue("");
                    $spreadSheet->getSheet(10)->getCell('F60')->SetValue("");
                    $spreadSheet->getSheet(10)->getCell('G60')->SetValue("");
                }

                if ($this->data->plc_ssl == 0)
                {
                    // bairj
                    $spreadSheet->getSheet(8)->getCell('D53')->SetValue("");
                    $spreadSheet->getSheet(8)->getCell('E53')->SetValue("");
                    $spreadSheet->getSheet(8)->getCell('F53')->SetValue("");

                    // amd sp
                    $spreadSheet->getSheet(10)->getCell('E63')->SetValue("");
                    $spreadSheet->getSheet(10)->getCell('F63')->SetValue("");
                    $spreadSheet->getSheet(10)->getCell('G63')->SetValue("");
                }

                if ($this->data->ikr_addon_stb_ssl == 0)
                {
                    // bairj
                    $spreadSheet->getSheet(8)->getCell('D54')->SetValue("");
                    $spreadSheet->getSheet(8)->getCell('E54')->SetValue("");
                    $spreadSheet->getSheet(8)->getCell('F54')->SetValue("");

                    // amd sp
                    $spreadSheet->getSheet(10)->getCell('E64')->SetValue("");
                    $spreadSheet->getSheet(10)->getCell('F64')->SetValue("");
                    $spreadSheet->getSheet(10)->getCell('G64')->SetValue("");
                }

                if ($this->data->change_stb_ssl == 0)
                {
                    // bairj
                    $spreadSheet->getSheet(8)->getCell('D55')->SetValue("");
                    $spreadSheet->getSheet(8)->getCell('E55')->SetValue("");
                    $spreadSheet->getSheet(8)->getCell('F55')->SetValue("");

                    // amd sp
                    $spreadSheet->getSheet(10)->getCell('E65')->SetValue("");
                    $spreadSheet->getSheet(10)->getCell('F65')->SetValue("");
                    $spreadSheet->getSheet(10)->getCell('G65')->SetValue("");
                }

                if ($this->data->ont_premium_ssl == 0)
                {
                    // bairj
                    $spreadSheet->getSheet(8)->getCell('D56')->SetValue("");
                    $spreadSheet->getSheet(8)->getCell('E56')->SetValue("");
                    $spreadSheet->getSheet(8)->getCell('F56')->SetValue("");

                    // amd sp
                    $spreadSheet->getSheet(10)->getCell('E66')->SetValue("");
                    $spreadSheet->getSheet(10)->getCell('F66')->SetValue("");
                    $spreadSheet->getSheet(10)->getCell('G66')->SetValue("");
                }

                if ($this->data->wifiextender_ssl == 0)
                {
                    // bairj
                    $spreadSheet->getSheet(8)->getCell('D57')->SetValue("");
                    $spreadSheet->getSheet(8)->getCell('E57')->SetValue("");
                    $spreadSheet->getSheet(8)->getCell('F57')->SetValue("");

                    // amd sp
                    $spreadSheet->getSheet(10)->getCell('E67')->SetValue("");
                    $spreadSheet->getSheet(10)->getCell('F67')->SetValue("");
                    $spreadSheet->getSheet(10)->getCell('G67')->SetValue("");
                }

                if ($this->data->pu_s7_140_ssl == 0)
                {
                    // bairj
                    $spreadSheet->getSheet(8)->getCell('D63')->SetValue("");
                    $spreadSheet->getSheet(8)->getCell('E63')->SetValue("");
                    $spreadSheet->getSheet(8)->getCell('F63')->SetValue("");

                    // amd sp
                    $spreadSheet->getSheet(10)->getCell('E73')->SetValue("");
                    $spreadSheet->getSheet(10)->getCell('F73')->SetValue("");
                    $spreadSheet->getSheet(10)->getCell('G73')->SetValue("");
                }

                if ($this->data->pu_s9_140_ssl == 0)
                {
                    // bairj
                    $spreadSheet->getSheet(8)->getCell('D64')->SetValue("");
                    $spreadSheet->getSheet(8)->getCell('E64')->SetValue("");
                    $spreadSheet->getSheet(8)->getCell('F64')->SetValue("");

                    // amd sp
                    $spreadSheet->getSheet(10)->getCell('E74')->SetValue("");
                    $spreadSheet->getSheet(10)->getCell('F74')->SetValue("");
                    $spreadSheet->getSheet(10)->getCell('G74')->SetValue("");
                }

                // if ($this->data->traycable_ssl == 0)
                // {
                    // bairj
                    $spreadSheet->getSheet(8)->getCell('D65')->SetValue("");
                    $spreadSheet->getSheet(8)->getCell('E65')->SetValue("");
                    $spreadSheet->getSheet(8)->getCell('F65')->SetValue("");

                    // amd sp
                    $spreadSheet->getSheet(10)->getCell('E75')->SetValue("");
                    $spreadSheet->getSheet(10)->getCell('F75')->SetValue("");
                    $spreadSheet->getSheet(10)->getCell('G75')->SetValue("");
                // }
                

                if ($nilai_dpp > 0 && $nilai_dpp <= 200000000)
                {    
                    $rule = 'krglbh_dua_jt';

                    // form_tagihan_mitra
                    $spreadSheet->getSheet(1)->getCell('H35')->SetValue("___________________");
                    $spreadSheet->getSheet(1)->getCell('H36')->SetValue("NIK.");

                    // checklist_dokumen_mitra
                    $spreadSheet->getSheet(13)->getCell('A65')->SetValue("___________________");
                } elseif ($nilai_dpp > 200000000 && $nilai_dpp <= 500000000) {
                    $rule = 'antr_dualima_jt';
                } else {
                    $rule = 'lbh_lima_jt';

                    // form_tagihan_mitra
                    $spreadSheet->getSheet(1)->getCell('H35')->SetValue("___________________");
                    $spreadSheet->getSheet(1)->getCell('H36')->SetValue("NIK.");

                    // checklist_dokumen_mitra
                    $spreadSheet->getSheet(13)->getCell('A65')->SetValue("___________________");

                    $spreadSheet->getSheet(1)->getCell('E35')->SetValue(strtoupper($this->ttd_user('mngr_krj')->user));
                    $spreadSheet->getSheet(1)->getCell('E36')->SetValue('NIK. ' . $this->ttd_user('mngr_krj')->nik);
                }

                $spreadSheet->getSheet(0)->getCell('B40')->SetValue(strtoupper($this->ttd_user($rule)->user));
                $spreadSheet->getSheet(0)->getCell('B41')->SetValue($this->ttd_user($rule)->jabatan);
                $spreadSheet->getSheet(0)->getCell('B91')->SetValue($this->ttd_user($rule)->nik);



                $writer = IOFactory::createWriter($spreadSheet, 'Xlsx');

                switch ($this->output_file)
                {
                    case 'save':

                            $path = public_path() . "/upload2/" . $this->data->id . '/';

                            if (!file_exists($path))
                            {
                                if (!mkdir($path, 0777, true))
                                {
                                    return 'gagal menyiapkan folder sanggup';
                                }
                            }

                            $writer->save($path . "Dokumen_Pelengkap_Lainnya_PSB_" . str_replace(" ", "_", $this->data->nama_company) . ".xlsx");
                        break;
                    
                    default:
                            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                            header('Content-Disposition: attachment;filename="Dokumen_Pelengkap_Lainnya_PSB_'.str_replace(" ", "_", $this->data->nama_company).'.xlsx"');
            
                            $writer->save('php://output');
                            die;
                        break;
                }
            },
            BeforeWriting::class => function (BeforeWriting $event) {
                $event->writer->setActiveSheetIndex(0);
            }
        ];
    }
}