<?php

namespace App\Http\Controllers;

use App\DA\AdminModel;
use App\DA\ToolsModel;
use App\DA\ReportModel;
use Excel;
use Illuminate\Http\Request;

date_default_timezone_set("Asia/Makassar");
class ToolsController extends Controller
{
  protected $file_mitra = [
    'PKS_PSB_MIGRASI_2020'             => 'PKS PSB & MIGRASI 2020',
    'BAK_PEKERJAAN_PSB_MIGRASI'        => 'BAK PEKERJAAN PSB & MIGRASI',
    'AMANDEMEN_PERTAMA_PEMBAYARAN_PSB' => 'AMANDEMEN PERTAMA PEMBAYARAN PSB',
    'KHS_PSB_MIGRASI_2020'             => 'KHS PSB MIGRASI 2020',
    'SIUJK_OSS'                        => 'SIUJK OSS',
    'SURAT_KUASA'                      => 'SURAT KUASA',
    'SBU'                              => 'SBU',
    'NPWP'                             => 'NPWP',
    'SPT_PPN'                          => 'SPT PPN'
  ];

  public function edit_mitra_sh()
  {
    $AM = new AdminModel();
    $file_mitra = $AM->file_mitra;
    $data       = ToolsModel::find_mitra(session('auth')->mitra_amija_pt);
    $all_mitra  = ToolsModel::show_mitra();
    $mitra_me   = [];
    $id         = null;
    $get_witel  = ToolsModel::get_witel();
    $get_pph = [
      (object)['id' => '1,75%', 'text' => 'Kecil ( 1,75% )'],
      (object)['id' => '2,65%', 'text' => 'Menengah ( 2,65% )'],
      (object)['id' => '4%', 'text' => 'Tanpa SIUJK ( 4% )'],
    ];

    if($data)
    {
      $mitra_me = AdminModel::get_mitra($data->id);
      $id       = $data->id;
    }
    return view('Area.mitra_edit', compact('mitra_me', 'id', 'file_mitra', 'all_mitra', 'get_witel', 'get_pph') );
  }

	public function save_mitra_sh(Request $req)
  {
    $id_m = '';

    if(session('auth')->mitra_amija_pt)
    {
      $data = ToolsModel::find_mitra(session('auth')->mitra_amija_pt);
      if($data)
      {
        $id_m = $data->id;
      }
    }

    if($req->no_khs_maintenance)
    {
      $split_tgl = explode('-', $req->tgl_khs_maintenance);
      $str       = preg_split("/[\/.-]/", $req->no_khs_maintenance);
      $split_kh  = array_reverse($str);

      // if($split_kh[1] .'-'. $split_kh[0] != $split_tgl[1].'-'.$split_tgl[0])
      // {
      //   return redirect('/tools/edit/mitra')->with('alerts', [
      //     ['type' => 'danger', 'text' => 'Kode KHS Tidak Sesuai!']
      //   ]);
      // }
    }
    ToolsModel::save_mitra($req, $id_m);
    return redirect('/')->with('alerts', [
      ['type' => 'success', 'text' => 'Profil Mitra Berhasil Diubah!']
    ]);
  }

	public function show_mitra()
  {
    $data = ToolsModel::show_mitra();
    return view('Report.Area.list_mitra', ['data' => $data]);
  }

	public function list_user_dok()
	{
    $data_main = ReportModel::get_list_user_dok('QE', session('auth')->Witel_New);
    $data_psb  = ReportModel::get_list_user_dok('PSB', session('auth')->Witel_New);
    $data_cons = ReportModel::get_list_user_dok('Construction', session('auth')->Witel_New);
    $data_all  = ReportModel::get_list_user_dok('all', session('auth')->Witel_New);
    return view('Report.list_bank_ttd', compact('data_main', 'data_psb', 'data_cons', 'data_all') );
	}

  public function create_user_mitra($id)
	{
    $data    = ReportModel::get_proc_user($id);
    $kerjaan = ToolsModel::list_pekerjaan();
    $status_dok["Construction"][] = (object) ['id' => 'Waspang', 'text' => 'Waspang Construction'];
    $status_dok["OSPFO"][] = (object) ['id' => 'Waspang', 'text' => 'Waspang Maintenance'];

		return view('Tools.form_user_ttd', compact('kerjaan', 'status_dok', 'data') );
	}

	public function save_orUp_user(Request $req, $id)
	{
		$msg = ToolsModel::save_or_update_user_dok($req, $id);
    return redirect($msg['direct'])->with('alerts', $msg['isi']);
	}

	public function input_mitra()
  {
    $id = 'input';
    $mitra_me   = [];
    $file_mitra = $this->file_mitra;
    $all_mitra  = ToolsModel::show_mitra();
    $get_witel = ToolsModel::get_witel();
    $get_pph = [
      (object)['id' => '1,75%', 'text' => 'Kecil ( 1,75% )'],
      (object)['id' => '2,65%', 'text' => 'Menengah ( 2,65% )'],
      (object)['id' => '4%', 'text' => 'Tanpa SIUJK ( 4% )'],
    ];
    return view('Area.mitra_edit', compact('mitra_me', 'id', 'file_mitra', 'all_mitra', 'get_witel', 'get_pph') );
  }

	public function save_mitra(Request $req, $id)
  {
    if($req->no_khs_maintenance)
    {
      $split_tgl = explode('-', $req->tgl_khs_maintenance);
      $str       = preg_split("/[\/.-]/", $req->no_khs_maintenance);
      $split_kh  = array_reverse($str);

      // if($split_kh[1] .'-'. $split_kh[0] != $split_tgl[1].'-'.$split_tgl[0])
      // {
      //   return back()->with('alerts', [
      //     ['type' => 'danger', 'text' => 'Kode KHS Tidak Sesuai!']
      //   ])->withInput();
      // }
    }

    $msg = ToolsModel::save_mitra($req, $id);
    return redirect($msg['direct'])->with('alerts', $msg['isi'])->withInput();
  }

  public function edit_mitra($id)
  {
    $file_mitra = $this->file_mitra;
    $mitra_me   = AdminModel::get_mitra($id);
    $all_mitra  = ToolsModel::show_mitra();
    $get_witel  = ToolsModel::get_witel();
    $get_pph = [
      (object)['id' => '1,75%', 'text' => 'Kecil ( 1,75% )'],
      (object)['id' => '2,65%', 'text' => 'Menengah ( 2,65% )'],
      (object)['id' => '4%', 'text' => 'Tanpa SIUJK ( 4% )'],
    ];

    return view('Area.mitra_edit', compact('mitra_me', 'id', 'file_mitra', 'all_mitra', 'get_witel', 'get_pph') );
  }

	public function create_pid_utama()
	{
		return view('Commerce.create_pid', ['data' => [], 'history_be' => [] ]);
	}

	public function save_pid(Request $req)
	{
		$msg = ToolsModel::save_pid_main($req);
		return redirect($msg['direct'])->with('alerts', $msg['isi']);
	}

	public function delete_user_mitra($id)
	{
		$msg = ToolsModel::delete_user_ttd($id);
		return redirect($msg['direct'])->with('alerts', $msg['isi']);
	}

  public function get_hss_psb()
  {
    $data = ToolsModel::get_hss_psb();

    return view('Tools.hss_psb', compact('data'));
  }

  public function save_hss_psb_witel(Request $req, $witel)
  {
    ToolsModel::save_hss_psb_witel($req, $witel);

    return redirect('/tools/hss_psb')->with('alerts', [
      ['type' => 'success', 'text' => 'Berhasil Perbaharui HSS Witel '.$witel],
    ]);
  }

	public function planning_fixed()
	{
    $data = ToolsModel::get_distinct_material();
    return view('Tools.form_boq', ['material' => $data]);
	}

	public function get_tiket(Request $req)
	{
		$search = strtoupper($req->searchTerm);
    $check_data = ToolsModel::get_tiket($search, $req->design);

    $data = [];

    foreach ($check_data as $row)
    {
      $data[] = array("id" => $row->id, "text" => $row->no_tiket);
    }
    return \Response::json($data);
	}

	public function save_planning_fixed(Request $req)
	{
    ToolsModel::save_planning($req);
    return redirect('/tools/list_planning_fixed')->with('alerts', [
      ['type' => 'success', 'text' => 'Planning Berhasil Dibuat'],
    ]);
	}

	public function list_planning_fixed()
	{
    $data_raw = ToolsModel::list_planning();
    $data = [];

    foreach($data_raw as $v)
    {
      $data[$v->id]['id'] = $v->id;
      $data[$v->id]['judul'] = $v->judul;
      $data[$v->id]['design'][] = $v->designator;
    }
    return view('Tools.view_planning_fixed', compact('data') );
	}

	public function planning_rfc($id)
	{
		$data = ToolsModel::get_planning_id($id);
    $arr_tiket = [];

    foreach($data as $v)
    {
      $tiket = json_decode($v->id_tiket);

      foreach($tiket as $vv)
      {
        $get_tiket = ToolsModel::find_tiket($vv);
        $arr_tiket[$vv] = $get_tiket->no_tiket;
      }
    }

    $data_tiket = ToolsModel::get_tiket_with_rfc($id);
    $rfc_tiket = [];

    foreach ($data_tiket as $k => $v)
    {
      $rfc_tiket[$v->rfc]['jenis'] = $v->type;
      $rfc_tiket[$v->rfc]['tiket'][] = $v->no_tiket;
    }

    return view('Tools.planning_rfc', compact('data', 'arr_tiket', 'rfc_tiket') );
	}

	public function save_planning_rfc(Request $req, $id)
	{
		ToolsModel::save_planning_rfc($req, $id);
    return redirect('/tools/list_planning_fixed')->with('alerts', [
      ['type' => 'success', 'text' => 'RFC Berhasil Ditambah!!'],
    ]);
	}

	public function generate_planning($id)
	{
    $data = ToolsModel::get_download_planning($id);
    return Excel::download(new ExcelCommon([$data], 'excl_planning'), 'Planning Data.xlsx');
	}

	public function delete_planning($id)
	{
		ToolsModel::delete_planning_rfc($id);
    return redirect('/tools/list_planning_fixed')->with('alerts', [
      ['type' => 'danger', 'text' => 'Pekerjana Berhasil Dihapus!!'],
    ]);
	}

	public function summary_rfc_rfr()
	{
    return view('Tools.summary_rfc_rfr');
	}

	public function get_search_inventory_pid(Request $req)
	{
		$search = strtoupper($req->searchTerm);
    $check_data = ToolsModel::search_wbs_pid($search, $req->design);

    $data = [];

    foreach ($check_data as $row)
    {
      $data[] = array("id" => $row->wbs_element, "text" => $row->wbs_element);
    }
    return \Response::json($data);
	}

	public function get_collect_inventory_material(Request $req)
	{
		$data = ToolsModel::get_inventory_material_by_wbs($req->id);
    return \Response::json($data);
	}
}