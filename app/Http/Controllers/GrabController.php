<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use DB;
use App\DA\BotModel;

date_default_timezone_set("Asia/Makassar");
setlocale(LC_ALL, 'IND', 'id_ID');
define('USER', '935508');
define('PWD', 'dalapa135');

class GrabController extends Controller
{
  public function grab_listdalapa_by_id_mitra()
	{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://dalapa.id/login');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);
    curl_setopt($ch, CURLOPT_COOKIEJAR, 'dalapa.jar');  //could be empty, but cause problems on some hosts
    curl_setopt($ch, CURLOPT_COOKIEFILE, '/');
    $result = curl_exec($ch);
    $dom = new \DOMDocument();
    $dom = $dom->loadHTML($result);
    $token = $dom->getElementsByTagName('input')[0]->getAttribute("value");
    curl_setopt($ch, CURLOPT_URL, 'https://dalapa.id/login');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);
    curl_setopt($ch, CURLOPT_COOKIEJAR, 'dalapa.jar');  //could be empty, but cause problems on some hosts
    curl_setopt($ch, CURLOPT_COOKIEFILE, '/');
    curl_setopt($ch, CURLOPT_POSTFIELDS, "email=".USER."&password=".PWD."&_token=".$token);
    $rough_content = curl_exec($ch);

    $err = curl_errno($ch);
    $errmsg = curl_error($ch);
    $header = curl_getinfo($ch);
    $header_content = substr($rough_content, 0, $header['header_size']);
    $body_content = trim(str_replace($header_content, '', $rough_content));
    $pattern = "#Set-Cookie:\\s+(?<cookie>[^=]+=[^;]+)#m";
    preg_match_all ($pattern, $header_content, $matches);
    $cookiesOut = implode("; ", $matches['cookie']);

    $mitra = DB::table('procurement_dalapa_mitra')->get();
    $toinsert=[];
    foreach($mitra as $m){
      for($i=1;$i<=date('m');$i++){
        echo $i.$m->id."\n";
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authentication: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzZXNzaW9uX2lkX3dmIjoxMDQxLCJzZXNzaW9uX3dpdGVsX3dmIjoiQkpNIn0.lEs9m1qAmYXkHDgKVrXj3A2J2SJBW1Rgnq7_GnolkfE','Authorization: 1041','AuthWitel: BJM'));
        curl_setopt($ch, CURLOPT_URL, 'https://dalapa.id/api/boq/get?month='.$i.'&year='.date('Y').'&witel=BJM&mitra='.$m->id);
        curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, false);
        curl_setopt($ch, CURLOPT_COOKIE, $cookiesOut);
        $result = curl_exec($ch);
        foreach(json_decode($result)->data as $o){
          $toinsert[]=[
            "pekerjaan_id"          => $o->id_list_odp_sehat,
            "witel_id"              => $o->id_witel_data,
            "nama_mitra"            => $o->nama_mitra,
            "date_listing"          => $o->date_listing,
            "alpro_name"            => $o->odp_location,
            "total_harga"           => $o->total_harga,
            "total_harga_mitra"     => $o->total_harga_mitra,
            "total_harga_kopegtel"  => $o->total_harga_kopegtel,
            "workforce_name"        => $o->workforce_name,
            "on_progress"           => $o->on_progress,
            "approval_id"           => $o->id_approval_telkom_history_listing,
            "nama_team"             => $o->nama_team,
            "workforce_name_conf"   => $o->workforce_name_conf,
            "sector_name"           => $o->sector_name,
            "odp_id"                => $o->odp_id,
            "date_complete"         => $o->date_complete,
            "jenis_pekerjaan"       => "ODP SEHAT",
            "mitra_id_dalapa"       => $m->id,
            "tahun_bulan_grab"      => date('Y')."-".$i
          ];
        }
        //download BOQ odp sehat
        if(count(json_decode($result)->data)){
          $path = public_path().'/excel/';
          if (!file_exists($path)) {
            if (!mkdir($path, 0770, true))
              return 'gagal menyiapkan folder foto evidence';
          }

          curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authentication: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzZXNzaW9uX2lkX3dmIjoxMDQxLCJzZXNzaW9uX3dpdGVsX3dmIjoiQkpNIn0.lEs9m1qAmYXkHDgKVrXj3A2J2SJBW1Rgnq7_GnolkfE','Authorization: 1041','AuthWitel: BJM'));
          curl_setopt($ch, CURLOPT_URL, 'https://dalapa.id/api/print/boq_odp_sehat_bulk');
          curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch, CURLOPT_HEADER, false);
          curl_setopt($ch, CURLOPT_POST, true);
          curl_setopt($ch, CURLOPT_POSTFIELDS, 'type=EXCEL&month='.$i.'&year='.date('Y').'&witel=BJM&mitra='.$m->id);
          curl_setopt($ch, CURLOPT_COOKIE, $cookiesOut);
          $result = curl_exec($ch);

          $link_excel = json_decode($result)->link;

          curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authentication: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzZXNzaW9uX2lkX3dmIjoxMDQxLCJzZXNzaW9uX3dpdGVsX3dmIjoiQkpNIn0.lEs9m1qAmYXkHDgKVrXj3A2J2SJBW1Rgnq7_GnolkfE','Authorization: 1041','AuthWitel: BJM'));
          curl_setopt($ch, CURLOPT_URL, $link_excel);
          curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch, CURLOPT_HEADER, false);
          curl_setopt($ch, CURLOPT_POST, false);
          curl_setopt($ch, CURLOPT_COOKIE, $cookiesOut);
          $result = curl_exec($ch);
          // curl_close($ch);
          $downloaded_file = fopen($path."ODP_SEHAT_".$m->id."_".date('Y')."_".$i.".xlsx", 'w');
          fwrite($downloaded_file, $result);
          fclose($downloaded_file);
        }
        // $odp=array_merge($odp,json_decode($result)->data);
        // $odp[]=json_decode($result)->data;
        // dd(json_decode($result)->data);

        //list pekerjaan odc
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authentication: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzZXNzaW9uX2lkX3dmIjoxMDQxLCJzZXNzaW9uX3dpdGVsX3dmIjoiQkpNIn0.lEs9m1qAmYXkHDgKVrXj3A2J2SJBW1Rgnq7_GnolkfE','Authorization: 1041','AuthWitel: BJM'));
        curl_setopt($ch, CURLOPT_URL, 'https://dalapa.id/api/boq_odc/get');
        curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "month=".$i."&year=".date('Y')."&witel=BJM&mitra=".$m->id);
        curl_setopt($ch, CURLOPT_COOKIE, $cookiesOut);
        $result = curl_exec($ch);
        foreach(json_decode($result)->data as $o){
          $toinsert[]=[
            "pekerjaan_id"        => $o->id_list_odc_sehat,
            "witel_id"            => $o->id_witel_data,
            "nama_mitra"          => $o->nama_mitra,
            "date_listing"        => $o->date_listing,
            "alpro_name"          => $o->odc_name,
            "total_harga"         => $o->total_harga,
            "total_harga_mitra"   => $o->total_harga_mitra,
            "total_harga_kopegtel"=> $o->total_harga_kopegtel,
            "workforce_name"      => $o->workforce_name,
            "on_progress"         => $o->on_progress,
            "approval_id"         => $o->id_approval_telkom_history_listing_odc,
            "nama_team"           => $o->nama_team,
            "workforce_name_conf" => $o->workforce_name_conf,

            "sector_name"         => null,
            "odp_id"              => null,
            "date_complete"       => null,
            "jenis_pekerjaan"     => "ODC SEHAT",
            "mitra_id_dalapa"     => $m->id,
            "tahun_bulan_grab"    => date('Y')."-".$i
          ];
        }
        // $odc[]=json_decode($result)->data;
        //download BOQ odc sehat
        if(count(json_decode($result)->data)){
          $path = public_path().'/excel/';
          if (!file_exists($path)) {
            if (!mkdir($path, 0770, true))
              return 'gagal menyiapkan folder foto evidence';
          }

          curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authentication: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzZXNzaW9uX2lkX3dmIjoxMDQxLCJzZXNzaW9uX3dpdGVsX3dmIjoiQkpNIn0.lEs9m1qAmYXkHDgKVrXj3A2J2SJBW1Rgnq7_GnolkfE','Authorization: 1041','AuthWitel: BJM'));
          curl_setopt($ch, CURLOPT_URL, 'https://dalapa.id/api/print/boq_odc_sehat_bulk');
          curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch, CURLOPT_HEADER, false);
          curl_setopt($ch, CURLOPT_POST, true);
          curl_setopt($ch, CURLOPT_POSTFIELDS, 'type=EXCEL&month='.$i.'&year='.date('Y').'&witel=BJM&mitra='.$m->id);
          curl_setopt($ch, CURLOPT_COOKIE, $cookiesOut);
          $result = curl_exec($ch);

          $link_excel = json_decode($result)->link;

          curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authentication: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzZXNzaW9uX2lkX3dmIjoxMDQxLCJzZXNzaW9uX3dpdGVsX3dmIjoiQkpNIn0.lEs9m1qAmYXkHDgKVrXj3A2J2SJBW1Rgnq7_GnolkfE','Authorization: 1041','AuthWitel: BJM'));
          curl_setopt($ch, CURLOPT_URL, $link_excel);
          curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch, CURLOPT_HEADER, false);
          curl_setopt($ch, CURLOPT_POST, false);
          curl_setopt($ch, CURLOPT_COOKIE, $cookiesOut);
          $result = curl_exec($ch);
          // curl_close($ch);
          $downloaded_file = fopen($path."ODC_SEHAT_".$m->id."_".date('Y')."_".$i.".xlsx", 'w');
          fwrite($downloaded_file, $result);
          fclose($downloaded_file);
        }

      }
    }
    curl_close($ch);

    // dd($toinsert);
    $srcarr=array_chunk($toinsert,500);
    DB::table('procurement_dalapa_pekerjaan')->truncate();
    foreach($srcarr as $item) {
        DB::table('procurement_dalapa_pekerjaan')->insert($item);
    }
		// dd(json_decode($result)->data);
	}

  public static function get_proaktif()
  {
    $akun = DB::table('akun')->where('user', '955943')->first();
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://apps.telkomakses.co.id/proactive/index.php');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);
    curl_setopt($ch, CURLOPT_COOKIEJAR, 'proaktif.jar');  //could be empty, but cause problems on some hosts
    curl_setopt($ch, CURLOPT_COOKIEFILE, '/');
    curl_setopt($ch, CURLOPT_POSTFIELDS, "username=".$akun->user."&password=".$akun->pwd."&btn_login=Log+In");
    $rough_content = curl_exec($ch);

    curl_setopt($ch, CURLOPT_URL, 'https://apps.telkomakses.co.id/proactive/all_project_server.php');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);
    curl_setopt($ch, CURLOPT_COOKIEJAR, 'proaktif.jar');  //could be empty, but cause problems on some hosts
    curl_setopt($ch, CURLOPT_COOKIEFILE, '/');
    $rough_content_token = curl_exec($ch);

    $dom = new \DOMDocument();
    @$dom->loadHTML($rough_content_token);
    $token = $dom->getElementById('csrf_token')->getAttribute("value");

    $search_key = ["Witel+Kalsel+(Banjarmasin)", "Balikpapan", "Divisi+Regional+6", "MITRATEL+-+REGIONAL", "WITEL+BANJARMASIN"];
    $toinsert = [];
    foreach($search_key as $sk){
      curl_setopt($ch, CURLOPT_URL, 'https://apps.telkomakses.co.id/proactive/all_project_server_data.php');
      curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_HEADER, false);
      curl_setopt($ch, CURLOPT_COOKIESESSION, true);
      curl_setopt($ch, CURLOPT_COOKIEJAR, 'proaktif.jar');  //could be empty, but cause problems on some hosts
      curl_setopt($ch, CURLOPT_COOKIEFILE, '/');
      curl_setopt($ch, CURLOPT_POSTFIELDS, "draw=0&columns%5B0%5D%5Bdata%5D=0&columns%5B0%5D%5Bname%5D=&columns%5B0%5D%5Bsearchable%5D=true&columns%5B0%5D%5Borderable%5D=true&columns%5B0%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B0%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B1%5D%5Bdata%5D=1&columns%5B1%5D%5Bname%5D=&columns%5B1%5D%5Bsearchable%5D=true&columns%5B1%5D%5Borderable%5D=true&columns%5B1%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B1%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B2%5D%5Bdata%5D=2&columns%5B2%5D%5Bname%5D=&columns%5B2%5D%5Bsearchable%5D=true&columns%5B2%5D%5Borderable%5D=true&columns%5B2%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B2%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B3%5D%5Bdata%5D=3&columns%5B3%5D%5Bname%5D=&columns%5B3%5D%5Bsearchable%5D=true&columns%5B3%5D%5Borderable%5D=true&columns%5B3%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B3%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B4%5D%5Bdata%5D=4&columns%5B4%5D%5Bname%5D=&columns%5B4%5D%5Bsearchable%5D=true&columns%5B4%5D%5Borderable%5D=true&columns%5B4%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B4%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B5%5D%5Bdata%5D=5&columns%5B5%5D%5Bname%5D=&columns%5B5%5D%5Bsearchable%5D=true&columns%5B5%5D%5Borderable%5D=true&columns%5B5%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B5%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B6%5D%5Bdata%5D=6&columns%5B6%5D%5Bname%5D=&columns%5B6%5D%5Bsearchable%5D=true&columns%5B6%5D%5Borderable%5D=true&columns%5B6%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B6%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B7%5D%5Bdata%5D=7&columns%5B7%5D%5Bname%5D=&columns%5B7%5D%5Bsearchable%5D=true&columns%5B7%5D%5Borderable%5D=true&columns%5B7%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B7%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B8%5D%5Bdata%5D=8&columns%5B8%5D%5Bname%5D=&columns%5B8%5D%5Bsearchable%5D=true&columns%5B8%5D%5Borderable%5D=true&columns%5B8%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B8%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B9%5D%5Bdata%5D=9&columns%5B9%5D%5Bname%5D=&columns%5B9%5D%5Bsearchable%5D=true&columns%5B9%5D%5Borderable%5D=true&columns%5B9%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B9%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B10%5D%5Bdata%5D=10&columns%5B10%5D%5Bname%5D=&columns%5B10%5D%5Bsearchable%5D=true&columns%5B10%5D%5Borderable%5D=true&columns%5B10%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B10%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B11%5D%5Bdata%5D=11&columns%5B11%5D%5Bname%5D=&columns%5B11%5D%5Bsearchable%5D=true&columns%5B11%5D%5Borderable%5D=true&columns%5B11%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B11%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B12%5D%5Bdata%5D=12&columns%5B12%5D%5Bname%5D=&columns%5B12%5D%5Bsearchable%5D=true&columns%5B12%5D%5Borderable%5D=false&columns%5B12%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B12%5D%5Bsearch%5D%5Bregex%5D=false&order%5B0%5D%5Bcolumn%5D=12&order%5B0%5D%5Bdir%5D=desc&start=0&length=10&search%5Bvalue%5D=$sk&search%5Bregex%5D=false&csrf_token=$token&regional=&witel=&kategori_project=after_sap");
      $rough_content = curl_exec($ch);

      $filter_records = json_decode($rough_content)->recordsFiltered;

      curl_setopt($ch, CURLOPT_URL, 'https://apps.telkomakses.co.id/proactive/all_project_server_data.php');
      curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_HEADER, false);
      curl_setopt($ch, CURLOPT_COOKIESESSION, true);
      curl_setopt($ch, CURLOPT_COOKIEJAR, 'proaktif.jar');  //could be empty, but cause problems on some hosts
      curl_setopt($ch, CURLOPT_COOKIEFILE, '/');
      curl_setopt($ch, CURLOPT_POSTFIELDS, "draw=0&columns%5B0%5D%5Bdata%5D=0&columns%5B0%5D%5Bname%5D=&columns%5B0%5D%5Bsearchable%5D=true&columns%5B0%5D%5Borderable%5D=true&columns%5B0%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B0%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B1%5D%5Bdata%5D=1&columns%5B1%5D%5Bname%5D=&columns%5B1%5D%5Bsearchable%5D=true&columns%5B1%5D%5Borderable%5D=true&columns%5B1%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B1%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B2%5D%5Bdata%5D=2&columns%5B2%5D%5Bname%5D=&columns%5B2%5D%5Bsearchable%5D=true&columns%5B2%5D%5Borderable%5D=true&columns%5B2%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B2%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B3%5D%5Bdata%5D=3&columns%5B3%5D%5Bname%5D=&columns%5B3%5D%5Bsearchable%5D=true&columns%5B3%5D%5Borderable%5D=true&columns%5B3%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B3%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B4%5D%5Bdata%5D=4&columns%5B4%5D%5Bname%5D=&columns%5B4%5D%5Bsearchable%5D=true&columns%5B4%5D%5Borderable%5D=true&columns%5B4%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B4%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B5%5D%5Bdata%5D=5&columns%5B5%5D%5Bname%5D=&columns%5B5%5D%5Bsearchable%5D=true&columns%5B5%5D%5Borderable%5D=true&columns%5B5%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B5%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B6%5D%5Bdata%5D=6&columns%5B6%5D%5Bname%5D=&columns%5B6%5D%5Bsearchable%5D=true&columns%5B6%5D%5Borderable%5D=true&columns%5B6%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B6%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B7%5D%5Bdata%5D=7&columns%5B7%5D%5Bname%5D=&columns%5B7%5D%5Bsearchable%5D=true&columns%5B7%5D%5Borderable%5D=true&columns%5B7%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B7%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B8%5D%5Bdata%5D=8&columns%5B8%5D%5Bname%5D=&columns%5B8%5D%5Bsearchable%5D=true&columns%5B8%5D%5Borderable%5D=true&columns%5B8%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B8%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B9%5D%5Bdata%5D=9&columns%5B9%5D%5Bname%5D=&columns%5B9%5D%5Bsearchable%5D=true&columns%5B9%5D%5Borderable%5D=true&columns%5B9%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B9%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B10%5D%5Bdata%5D=10&columns%5B10%5D%5Bname%5D=&columns%5B10%5D%5Bsearchable%5D=true&columns%5B10%5D%5Borderable%5D=true&columns%5B10%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B10%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B11%5D%5Bdata%5D=11&columns%5B11%5D%5Bname%5D=&columns%5B11%5D%5Bsearchable%5D=true&columns%5B11%5D%5Borderable%5D=true&columns%5B11%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B11%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B12%5D%5Bdata%5D=12&columns%5B12%5D%5Bname%5D=&columns%5B12%5D%5Bsearchable%5D=true&columns%5B12%5D%5Borderable%5D=false&columns%5B12%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B12%5D%5Bsearch%5D%5Bregex%5D=false&order%5B0%5D%5Bcolumn%5D=12&order%5B0%5D%5Bdir%5D=desc&start=0&length=$filter_records&search%5Bvalue%5D=$sk&search%5Bregex%5D=false&csrf_token=$token&regional=&witel=&kategori_project=after_sap");

      $rough_content = curl_exec($ch);

      foreach(json_decode($rough_content)->data as $d){
        $proaktif_id = str_replace("<a href='show_initiation.php?project_id=", "", str_replace("' class='btn btn-default btn-circle'><i class='icon-check'></i></a>", "", $d[12]));
        $toinsert[] = [
          'pid'             => $d[0],
          'sapid'           => $d[1],
          'nama_project'    => $d[2],
          'portofolio'      => $d[3],
          'nomor_kontrak'   => $d[4],
          'pm'              => $d[5],
          'tgl_mulai'       => $d[6],
          'tgl_selesai'     => $d[7],
          'tipe_project'    => $d[8],
          'fase'            => $d[9],
          'customer'        => $d[10],
          'status_project'  => $d[11],
          'proaktif_id'     => $proaktif_id
        ];
      }
    }
    curl_close($ch);
    DB::table('proaktif_project')->truncate();

    foreach(array_chunk($toinsert, 1000) as $v){
      DB::table('proaktif_project')->insert($v);
    }
  }

  public static function inventoryOutMaterial($witel)
  {
    $startDate = "01-01-2022";
    // $startDate = date('m-d-Y', strtotime('-1 days'));
    $endDate = date("m-d-Y");

    $cookies = "ci_session=a%3A6%3A%7Bs%3A10%3A%22session_id%22%3Bs%3A32%3A%228b1528a36b0c24881e229160e5989eec%22%3Bs%3A10%3A%22ip_address%22%3Bs%3A12%3A%2261.94.172.11%22%3Bs%3A10%3A%22user_agent%22%3Bs%3A111%3A%22Mozilla%2F5.0+%28Windows+NT+10.0%3B+Win64%3B+x64%29+AppleWebKit%2F537.36+%28KHTML%2C+like+Gecko%29+Chrome%2F111.0.0.0+Safari%2F537.36%22%3Bs%3A13%3A%22last_activity%22%3Bi%3A1680671153%3Bs%3A9%3A%22user_data%22%3Bs%3A0%3A%22%22%3Bs%3A9%3A%22logged_in%22%3Ba%3A5%3A%7Bs%3A12%3A%22is_logged_in%22%3Bi%3A1%3Bs%3A6%3A%22userid%22%3Bs%3A6%3A%22840034%22%3Bs%3A8%3A%22username%22%3Bs%3A6%3A%22840034%22%3Bs%3A10%3A%22profile_id%22%3Bi%3A1%3Bs%3A12%3A%22profile_name%22%3Bs%3A4%3A%22USER%22%3B%7D%7D2f207e8e438565f920c83701b28816e1";

    ini_set("memory_limit", "-1");
    ini_set("max_execution_time", "-1");

    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => "https://dashboard.telkomakses.co.id/inventory/index.php/report/download_out_material/ALL/ALL/ALL/$startDate/$endDate/AREA/$witel",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
      CURLOPT_HTTPHEADER => array(
        "Cookie: $cookies"
      ),
    ));

    $response = str_replace('"', '', curl_exec($curl));

    curl_close($curl);

    $result = [];
    foreach (explode("\n", $response) as $val)
    {
      $result[] = explode(',', $val);
    }
    unset($result[0]);

    if (count($result) > 0)
    {
      DB::transaction(function () use ($result, $startDate, $endDate, $witel) {
        $expStartDate = explode("-", $startDate);
        $expEndDate   = explode("-", $endDate);
        $start        = $expStartDate[2] . '-' . $expStartDate[0] . '-' . $expStartDate[1];
        $end          = $expEndDate[2] . '-' . $expEndDate[0] . '-' . $expEndDate[1];

        DB::table('inventory_material_log')->where('witel', $witel)->delete();

        $insert = [];
        foreach ($result as $data)
        {
          if ($data[0] != 0)
          {
            DB::table('inventory_material_log')->insert([
              'no_reservasi'   => @$data[0],
              'no_rfc'         => @$data[1],
              'po_numb'        => @$data[2],
              'plant'          => @$data[3],
              'project_id'     => @$data[4],
              'wbs_element'    => @$data[5],
              'description'    => @$data[6],
              'type'           => @$data[7],
              'program'        => @$data[8],
              'regional'       => @$data[9],
              'witel'          => @$data[10],
              'mitra'          => @$data[11],
              'requester'      => @$data[12],
              'nama_requester' => @$data[13],
              'nik_pemakai'    => @$data[14],
              'nama_pemakai'   => @$data[15],
              'posting_date'   => @$data[16],
              'material'       => @$data[17],
              'satuan'         => @$data[18],
              'quantity'       => @$data[19],
              'posting_datex'  => date('Y-m-d', strtotime(@$data[16]))
            ]);
          }
        }

        $total = count($result);

        //insert to data master
        sleep(5);
        DB::table('inventory_material')->where('witel', $witel)->delete();
        DB::statement('INSERT INTO `inventory_material` SELECT * FROM `inventory_material_log` WHERE `witel` = "'.$witel.'"');

        print_r("Finish Syncron Dashboard Inventory $start $end Witel $witel Total $total");
      });
    }
  }

  public static function get_proaktif_boq($id)
  {
    $akun = DB::table('akun')->where('user', '18950728')->first();
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://apps.telkomakses.co.id/proactive/index.php');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);
    curl_setopt($ch, CURLOPT_COOKIEJAR, 'proaktif.jar');  //could be empty, but cause problems on some hosts
    curl_setopt($ch, CURLOPT_COOKIEFILE, '/');
    curl_setopt($ch, CURLOPT_POSTFIELDS, "username=".$akun->user."&password=".$akun->pwd."&btn_login=Log+In");
    $rough_content = curl_exec($ch);

    curl_setopt($ch, CURLOPT_URL, 'https://apps.telkomakses.co.id/proactive/show_initiation.php?project_id='.$id);
    $result = curl_exec($ch);
    $dom = @\DOMDocument::loadHTML(trim($result) );
    $table = $dom->getElementsByTagName('table')->item(4);
    $rows = $table->getElementsByTagName('tr');
    $columns = array(
      0 =>'designator',
      'uraian',
      'satuan',
      'telkom_material',
      'telkom_jasa',
      'mitra_material',
      'mitra_jasa',
      'volume',
      'telkom_total',
      'mitra_total'
    );
    for ($i = 2, $count = $rows->length; $i < $count; $i++)
    {
      $cells = $rows->item($i)->getElementsByTagName('td');
      $data = array();
      for ($j = 0, $jcount = count($columns); $j < $jcount; $j++)
      {
        $td = $cells->item($j);

        if($j == 0 )
        {
          $optionTags = $td->getElementsByTagName('option');
          foreach ($optionTags as $tag){
            if ($tag->hasAttribute("selected")){
              $isi = $tag->nodeValue;
            }
          }
        }
        else
        {
          $isi = $td->getElementsByTagName('input')->item(0)->getAttribute('value');
        }

        $data[$columns[$j] ] = trim(@$isi);
      }
      // dd($data);
      $rs[] = $data;
    }
    return $rs;
  }

  public static function outMaterialPsb($witel)
  {
    $startDate = date('m-d-Y', strtotime('-2 days'));
    $endDate = date('m-d-Y', strtotime('-1 days'));

    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL            => "https://dashboard.telkomakses.co.id/provisioning/index.php/report/download_out_material/ALL/ALL/ALL/$startDate/$endDate/ALL/$witel",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING       => "",
        CURLOPT_MAXREDIRS      => 10,
        CURLOPT_TIMEOUT        => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST  => "GET",
    ));

    $response = curl_exec($curl);
    curl_close($curl);

    $result = explode("\n", str_replace(array('"'), '', $response));

    foreach ($result as $value)
    {
      $not_value = ['', 'NO RESERVASI,NO RFC,WH,WBS ELEMENT,DESCRIPTION,TYPE,PROGRAM,REGIONAL,WITEL,MITRA,REQUESTER,NAMA REQUESTER,NIK PEMAKAI,NAMA PEMAKAI,POSTING DATE,MATERIAL,SATUAN,QUANTITY'];
      if (!in_array($value, $not_value))
      {
        $rows = explode(',', $value);

        if (count($rows) == 18)
        {
          $insert[] = [
            'no_reservasi'   => @$rows[0],
            'no_rfc'         => @$rows[1],
            'wh'             => @$rows[2],
            'wbs_element'    => @$rows[3],
            'description'    => @$rows[4],
            'type'           => @$rows[5],
            'program'        => @$rows[6],
            'regional'       => @$rows[7],
            'witel'          => @$rows[8],
            'mitra'          => @$rows[9],
            'requester'      => @$rows[10],
            'nama_requester' => @$rows[11],
            'nik_pemakai'    => @$rows[12],
            'nama_pemakai'   => @$rows[13],
            'posting_date'   => @$rows[14],
            'posting_datex'  => date('Y-m-d', strtotime(@$rows[14])),
            'material'       => @$rows[15],
            'satuan'         => @$rows[16],
            'quantity'       => @$rows[17]
          ];
        }
      }
    }

    DB::table('out_material_psb')->whereBetween('posting_datex', [$startDate, $endDate])->where('witel', $witel)->delete();

    $total = count($insert);

    $array_chunk = array_chunk($insert, 500);

    foreach ($array_chunk as $numb => $chunk)
    {
      DB::table('out_material_psb')->insert($chunk);

      print_r("saved page $numb and sleep (1)\n");
      sleep(1);
    }

    print_r("\nFinish Syncron Dashboard Out Material PSB $startDate $endDate Witel $witel Total $total\n");
  }
}
