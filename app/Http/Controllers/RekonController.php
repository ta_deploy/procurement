<?php

namespace App\Http\Controllers;

use App\DA\AdminModel;
use App\DA\MitraModel;
use App\DA\UploadModel;
use App\DA\RekonModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Maatwebsite\Excel\Facades\Excel;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use DB;

date_default_timezone_set("Asia/Makassar");
class RekonController extends Controller
{

  // public function uploadBOQ()
  // {
  //   $data = AdminModel::get_upload_BOQ();
  //   // dd($data);
  //   return view('Mitra.Rekon.Finish.upload_boq', compact('data'));
  // }

  // public function save_BOQ_SP(Request $req)
  // {
  //   if($req->hasFile('upload_boq'))
  //   {
  //     $get_data = Excel::toArray(new UploadModel, $req->file('upload_boq'));
  //     $get_sheet1 = $get_data[0];
  //     $key = array_search('DESIGNATOR', array_column($get_sheet1, 1));
  //     $new_data = [];
  //     // dd($key, $get_sheet1);
  //     if($key)
  //     {
  //       if(in_array($req->jenis_up, ['Maintenance', 'QE']) )
  //       {
  //         $nosp = trim($get_sheet1[1][1], ' ');
  //         $pks = trim($get_sheet1[2][1], ' ');
  //         $witel = trim($get_sheet1[4][1], ' ');
  //         $project = trim($get_sheet1[5][1], ' ');
  //         $lokasi = trim($get_sheet1[6][1], ' ');
  //         $sto = preg_replace('/[^A-Za-z0-9\-]/', '', trim($get_sheet1[7][1], ' ') );

  //         $data = [
  //           'nosp' => $nosp,
  //           'pks' => $pks,
  //           'project' => $project,
  //           'lokasi' => $lokasi,
  //           'sto' => $sto,
  //           'witel' => $witel
  //         ];
  //         //TODO: MITRANYA JANGAN DIAMBIL DULU LEWAT EXCEL, TAKUTNYA TIDAK SAMA DENGAN DTABASE
  //         // $mitra = $get_sheet1[8][1];

  //         foreach ($get_sheet1 as $key => $val)
  //         {
  //           if(!in_array('', [strval($val[4]), strval($val[1]), strval($val[2])]))
  //           {
  //             if($key < 13) continue;

  //             if($val[6] != 0){
  //               $new_data[$key]['nomor'] = $val[0];
  //               $new_data[$key]['signature'] = $val[1];
  //               $new_data[$key]['material'] = $val[4];
  //               $new_data[$key]['jasa'] = $val[5];
  //               $new_data[$key]['total'] = $val[6];
  //             }

  //             foreach ($val as $key_c1 => $val_c1)
  //             {
  //               if($key_c1 <= 6) continue;
  //               if ( $val[$key_c1] != 0)
  //               {
  //                 // dd($key_c1, $val[$key_c1], $val);
  //                 if(!in_array($val[$key_c1], [0, NULL]) && $get_sheet1[9][$key_c1] != NULL)
  //                 {
  //                   $data_lokasi[$key_c1]['sto'] = preg_replace('/[^A-Za-z0-9\-]/', '', $get_sheet1[9][$key_c1]);
  //                   $data_lokasi[$key_c1]['lokasi'] = trim($get_sheet1[10][$key_c1]," ");
  //                   $data_lokasi[$key_c1]['PID'] = trim($get_sheet1[11][$key_c1],"" );

  //                   $new_data[$key]['data_rekon'][$key_c1]['sto'] = preg_replace('/[^A-Za-z0-9\-]/', '', $get_sheet1[9][$key_c1]);
  //                   $new_data[$key]['data_rekon'][$key_c1]['lokasi'] = trim($get_sheet1[10][$key_c1]," ");
  //                   $new_data[$key]['data_rekon'][$key_c1]['PID'] = trim($get_sheet1[11][$key_c1],"" );
  //                   $new_data[$key]['data_rekon'][$key_c1]['rekon'] = 0;
  //                   $new_data[$key]['data_rekon'][$key_c1]['tambah'] = 0;
  //                   $new_data[$key]['data_rekon'][$key_c1]['kurang'] = 0;
  //                   $new_data[$key]['data_rekon'][$key_c1]['sp'] = trim($val[$key_c1]," ");
  //                 }
  //               }
  //             }
  //           }
  //         }
  //       }
  //       else
  //       {
  //         $project = preg_replace('/:/', '', trim($get_sheet1[1][1], ' ') );
  //         $tanggal_boq = preg_replace('/:/', '', trim($get_sheet1[4][1], ' ') );
  //         $lokasi = preg_replace('/:/', '', trim($get_sheet1[3][1], ' ') );

  //         $data = [
  //           'project' => $project,
  //           'witel' => $lokasi,
  //           'lokasi' => $lokasi,
  //           'tanggal_boq' => $tanggal_boq,
  //           'pks' => null,
  //           'nosp' => null,
  //           'sto' => null
  //         ];

  //         foreach ($get_sheet1 as $key => $val)
  //         {
  //           if($key < 10) continue;
  //           // dd($key, $val);
  //           if($val[5] != 0){
  //             $new_data[$key]['nomor'] = $val[0];
  //             $new_data[$key]['signature'] = $val[1];
  //             $new_data[$key]['material'] = $val[4] ?? 0;
  //             $new_data[$key]['jasa'] = $val[5] ?? 0;
  //           }

  //           if( $val[5] != 0)
  //           {
  //             // dd(5, $val[5], $val);
  //             if(!in_array($val[5], [0, NULL]) && $get_sheet1[9][5] != NULL)
  //             {
  //               $data_lokasi[5]['sto'] = null;
  //               $data_lokasi[5]['lokasi'] = $lokasi;
  //               $data_lokasi[5]['PID'] = null;

  //               $new_data[$key]['data_rekon'][5]['sto'] = null;
  //               $new_data[$key]['data_rekon'][5]['lokasi'] = $lokasi;
  //               $new_data[$key]['data_rekon'][5]['PID'] = null;
  //               $new_data[$key]['data_rekon'][5]['sp'] = $val[6];
  //               $new_data[$key]['data_rekon'][5]['rekon'] = $val[7];
  //               $new_data[$key]['data_rekon'][5]['tambah'] = 0;
  //               $new_data[$key]['data_rekon'][5]['kurang'] = 0;
  //             }
  //           }
  //         }
  //       }
  //       // dd($new_data);
  //       // $pesan = AdminModel::save_boq($req, $data, $new_data, $data_lokasi, 'BOQ_REKON');

          // if($pesan != 'ok')
          //   {
          //     return redirect('/Rekon/upload/boq')->with('alerts', [
          //       ['type' => $pesan['alerts']['type'], 'text' => $pesan['alerts']['text'] ]
          //     ]);
          //   }
  //     }
  //     else
  //     {
  //       return redirect('/Rekon/upload/boq')->with('alerts', [
  //         ['type' => 'danger', 'text' => 'Tidak ditemukan DESIGNATOR!']
  //       ]);
  //     }
  //   }
  //   return redirect('/Rekon/upload/boq')->with('alerts', [
  //     ['type' => 'success', 'text' => 'BOQ Berhasil Ditambahkan!']
  //   ]);
  // }

  public function rekonDashboardOrder(Request $req)
  {
    $load_data = [];

    if($req)
    {
      $load_data = $req->all();
    }

    $regional = Input::get('regional');
    $witel = Input::get('witel');
    $mitra = Input::get('mitra');
    $startDate = Input::get('startDate');
    $endDate = Input::get('endDate');

    if ($regional == null)
    {
      $regional = 'REG6';
    }
    if ($witel == null)
    {
      $witel = 'ALL';
    }
    if ($mitra == null)
    {
      $mitra = 'ALL';
    }
    if ($startDate == null)
    {
      $startDate = date('Y-m-01');
    }
    if ($endDate == null)
    {
      $endDate = date('Y-m-d');
    }

    
    $get_mitra = DB::table('procurement_mitra')->where('witel', session('auth')->Witel_New)->where('active', 1)->where('tacticalpro_mitra_id', '!=', 0)->select('tacticalpro_mitra_id AS id', 'nama_company AS text')->groupBy('nama_company')->get();
    $get_layanan = DB::table('procurement_layanan_psb')->where('aktif', 1)->select('alias_layanan AS id', 'layanan AS text')->get();
    $bulan_format = DB::table('bulan_format')->select('id', 'bulan AS text', 'date_periode1', 'date_periode2')->get();
    $get_regional = DB::table('procurement_mitra_nasional')->select('reg', 'witel', 'mitra')->orderBy('reg', 'ASC')->groupBy('mitra')->get();

    return view('Rekon.dashboard_order', compact('regional', 'witel', 'mitra', 'startDate', 'endDate', 'get_regional', 'bulan_format', 'load_data', 'get_mitra', 'get_layanan'));
  }

  public function ajx_rekonDashboardOrder()
  {
    $regional = Input::get('regional');
    $witel = Input::get('witel');
    $mitra = Input::get('mitra');
    $periode = Input::get('periode');
    $startDate = Input::get('startDate');
    $endDate = Input::get('endDate');

    if ($regional == null)
    {
      $regional = 'REG6';
    }
    if ($witel == null)
    {
      $witel = 'ALL';
    }
    if ($mitra == null)
    {
      $mitra = 'ALL';
    }
    if ($periode == null)
    {
      $periode = date('Ym');
    }

    $data = RekonModel::dashboard_order($regional, $witel, $mitra, $startDate, $endDate);
    $bulan_format = DB::SELECT('SELECT id, bulan as text, date_periode1, date_periode2 FROM bulan_format');

    $txt_bf = '';
    foreach ($bulan_format as $value)
    {
      $txt_bf .= '<option value="'.$value->id.'">'.$value->text.'</option>';
    }

    $total_p1_survey = $total_p2_tlpint_survey = $total_p2_intiptv_survey = $total_p3_survey = $total_p1 = $total_p2_tlpint = $total_p2_intiptv = $total_p3 = $total_addon_nok = $total_addon_ok = $total_migrasi_service_1p2p = $total_migrasi_service_1p3p = $total_migrasi_service_2p3p = $total_belum_rekon = $total_sudah_rekon = $grand_total = 0;

    $result['data'] = $result['footer'] = [];
    foreach ($data as $key => $value)
    {
      $total_p1 += $value->p1;
      $total_p2_tlpint += $value->p2_tlpint;
      $total_p2_intiptv += $value->p2_intiptv;
      $total_p3 += $value->p3;
      $total_p1_survey += $value->p1_survey;
      $total_p2_tlpint_survey += $value->p2_tlpint_survey;
      $total_p2_intiptv_survey += $value->p2_intiptv_survey;
      $total_p3_survey += $value->p3_survey;
      $total_addon_nok += $value->addon_nok;
      $total_addon_ok += $value->addon_ok;
      $total_migrasi_service_1p2p += $value->migrasi_service_1p2p;
      $total_migrasi_service_1p3p += $value->migrasi_service_1p3p;
      $total_migrasi_service_2p3p += $value->migrasi_service_2p3p;
      $total_belum_rekon += $value->belum_rekon;
      $total_sudah_rekon += $value->sudah_rekon;

      $result['data'][] = [
        $value->regional,
        $value->witel,
        $value->mitra,
        '<a href="/Rekon/rekonDashboardOrderDetail?regional='.$value->regional.'&witel='.$value->witel.'&mitra='.$value->mitra.'&startDate='.$startDate.'&endDate='.$endDate.'&layanan=p1" style="color: #6c757d;">'.str_replace(',', '.', number_format($value->p1)).'</a>',
        '<a href="/Rekon/rekonDashboardOrderDetail?regional='.$value->regional.'&witel='.$value->witel.'&mitra='.$value->mitra.'&startDate='.$startDate.'&endDate='.$endDate.'&layanan=p2_tlpint" style="color: #6c757d;">'.str_replace(',', '.', number_format($value->p2_tlpint)).'</a>',
        '<a href="/Rekon/rekonDashboardOrderDetail?regional='.$value->regional.'&witel='.$value->witel.'&mitra='.$value->mitra.'&startDate='.$startDate.'&endDate='.$endDate.'&layanan=p2_intiptv" style="color: #6c757d;">'.str_replace(',', '.', number_format($value->p2_intiptv)).'</a>',
        '<a href="/Rekon/rekonDashboardOrderDetail?regional='.$value->regional.'&witel='.$value->witel.'&mitra='.$value->mitra.'&startDate='.$startDate.'&endDate='.$endDate.'&layanan=p3" style="color: #6c757d;">'.str_replace(',', '.', number_format($value->p3)).'</a>',
        '<a href="/Rekon/rekonDashboardOrderDetail?regional='.$value->regional.'&witel='.$value->witel.'&mitra='.$value->mitra.'&startDate='.$startDate.'&endDate='.$endDate.'&layanan=p1_survey" style="color: #6c757d;">'.str_replace(',', '.', number_format($value->p1_survey)).'</a>',
        '<a href="/Rekon/rekonDashboardOrderDetail?regional='.$value->regional.'&witel='.$value->witel.'&mitra='.$value->mitra.'&startDate='.$startDate.'&endDate='.$endDate.'&layanan=p2_tlpint_survey" style="color: #6c757d;">'.str_replace(',', '.', number_format($value->p2_tlpint_survey)).'</a>',
        '<a href="/Rekon/rekonDashboardOrderDetail?regional='.$value->regional.'&witel='.$value->witel.'&mitra='.$value->mitra.'&startDate='.$startDate.'&endDate='.$endDate.'&layanan=p2_intiptv_survey" style="color: #6c757d;">'.str_replace(',', '.', number_format($value->p2_intiptv_survey)).'</a>',
        '<a href="/Rekon/rekonDashboardOrderDetail?regional='.$value->regional.'&witel='.$value->witel.'&mitra='.$value->mitra.'&startDate='.$startDate.'&endDate='.$endDate.'&layanan=p3_survey" style="color: #6c757d;">'.str_replace(',', '.', number_format($value->p3_survey)).'</a>',
        '<a href="/Rekon/rekonDashboardOrderDetail?regional='.$value->regional.'&witel='.$value->witel.'&mitra='.$value->mitra.'&startDate='.$startDate.'&endDate='.$endDate.'&layanan=migrasi_service_1p2p" style="color: #6c757d;">'.str_replace(',', '.', number_format($value->migrasi_service_1p2p)).'</a>',
        '<a href="/Rekon/rekonDashboardOrderDetail?regional='.$value->regional.'&witel='.$value->witel.'&mitra='.$value->mitra.'&startDate='.$startDate.'&endDate='.$endDate.'&layanan=migrasi_service_1p3p" style="color: #6c757d;">'.str_replace(',', '.', number_format($value->migrasi_service_1p3p)).'</a>',
        '<a href="/Rekon/rekonDashboardOrderDetail?regional='.$value->regional.'&witel='.$value->witel.'&mitra='.$value->mitra.'&startDate='.$startDate.'&endDate='.$endDate.'&layanan=migrasi_service_2p3p" style="color: #6c757d;">'.str_replace(',', '.', number_format($value->migrasi_service_2p3p)).'</a>',
        '<a href="/Rekon/rekonDashboardOrderDetail?regional='.$value->regional.'&witel='.$value->witel.'&mitra='.$value->mitra.'&startDate='.$startDate.'&endDate='.$endDate.'&layanan=addon_nok" style="color: #6c757d;">'.str_replace(',', '.', number_format($value->addon_nok)).'</a>',
        '<a href="/Rekon/rekonDashboardOrderDetail?regional='.$value->regional.'&witel='.$value->witel.'&mitra='.$value->mitra.'&startDate='.$startDate.'&endDate='.$endDate.'&layanan=addon_ok" style="color: #6c757d;">'.str_replace(',', '.', number_format($value->addon_ok)).'</a>',
        '<a href="/Rekon/rekonDashboardOrderDetail?regional='.$value->regional.'&witel='.$value->witel.'&mitra='.$value->mitra.'&startDate='.$startDate.'&endDate='.$endDate.'&layanan=belum_rekon" style="color: #6c757d;">'.str_replace(',', '.', number_format($value->belum_rekon)).'</a>',
        '<a href="/Rekon/rekonDashboardOrderDetail?regional='.$value->regional.'&witel='.$value->witel.'&mitra='.$value->mitra.'&startDate='.$startDate.'&endDate='.$endDate.'&layanan=sudah_rekon" style="color: #6c757d;">'.str_replace(',', '.', number_format($value->sudah_rekon)).'</a>',
        '
        <div class="modal fade" id="verticalModal-'.$value->pm_id_mitra.'-'.$value->witel.'" tabindex="-1" role="dialog" aria-labelledby="verticalModalTitle-'.$value->pm_id_mitra.'-'.$value->witel.'" aria-hidden="true">

          <div class="modal-dialog modal-dialog-centered modal-xl" role="document">

            <div class="modal-content">

              <div class="modal-header">
                <h5 class="modal-title" id="verticalModalTitle-'.$value->pm_id_mitra.'-'.$value->witel.'">Pengisian Nomor Surat dan Lainnya ('.$value->pm_id_mitra.'-'.$value->witel.')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
              </div>

              <div class="modal-body">
                <form id="form-'.$value->pm_id_mitra.'-'.$value->witel.'" method="post" action="/Admin/save/filepsb" enctype="multipart/form-data" autocomplete="off">

                  '.csrf_field().'
                  <input type="hidden" name="id_file" value="doc_generate">
                  <input type="hidden" name="flag_dok_psb" value="beta_ho_v1">
                  <input type="hidden" name="id_mitra" value="'.$value->pm_id_mitra.'">
                  <input type="hidden" name="witel" value="'.$value->witel.'">
                  <input type="hidden" name="order_periode1" value="'.$startDate.'">
                  <input type="hidden" name="order_periode2" value="'.$endDate.'">
                  <input type="hidden" name="pks" value="'.$value->no_kontrak_ta.'">
                  <input type="hidden" name="tgl_pks" value="'.$value->date_kontrak_ta.'">
                  <input type="hidden" name="p1_survey_ssl" value="'.$value->p1_survey.'">
                  <input type="hidden" name="p2_tlpint_survey_ssl" value="'.$value->p2_tlpint_survey.'">
                  <input type="hidden" name="p2_intiptv_survey_ssl" value="'.$value->p2_intiptv_survey.'">
                  <input type="hidden" name="p3_survey_ssl" value="'.$value->p3_survey.'">
                  <input type="hidden" name="p1_ssl" value="'.$value->p1.'">
                  <input type="hidden" name="p2_tlpint_ssl" value="'.$value->p2_tlpint.'">
                  <input type="hidden" name="p2_intiptv_ssl" value="'.$value->p2_intiptv.'">
                  <input type="hidden" name="p3_ssl" value="'.$value->p3.'">
                  <input type="hidden" name="migrasi_service_1p2p_ssl" value="'.$value->migrasi_service_1p2p.'">
                  <input type="hidden" name="migrasi_service_1p3p_ssl" value="'.$value->migrasi_service_1p3p.'">
                  <input type="hidden" name="migrasi_service_2p3p_ssl" value="'.$value->migrasi_service_2p3p.'">
                  <input type="hidden" name="ikr_addon_stb_ssl" value="'.$value->ikr_addon_stb.'">
                  <input type="hidden" name="indihome_smart_ssl" value="'.$value->indihome_smart.'">
                  <input type="hidden" name="wifiextender_ssl" value="'.$value->wifiextender.'">
                  <input type="hidden" name="plc_ssl" value="'.$value->plc.'">
                  <input type="hidden" name="lme_wifi_pt1_ssl" value="'.$value->lme_wifi_pt1.'">
                  <input type="hidden" name="lme_ap_indoor_ssl" value="'.$value->lme_ap_indoor.'">
                  <input type="hidden" name="lme_ap_outdoor_ssl" value="'.$value->lme_ap_outdoor.'">
                  <input type="hidden" name="migrasi_stb_ssl" value="'.$value->migrasi_stb.'">
                  <input type="hidden" name="instalasi_ipcam_ssl" value="'.$value->instalasi_ipcam.'">
                  <input type="hidden" name="pu_s7_140_ssl" value="'.$value->pu_s7_140.'">
                  <input type="hidden" name="pu_s9_140_ssl" value="'.$value->pu_s9_140.'">

                  <div class="col-md-12">

                    <h3>Periode</h3>
                    <section class="form-row">
                        <div class="form-group col-md-2">
                            <label class="col-form-label pull-right" for="tanggal_terbit">Tanggal Terbit</label>
                            <input type="text" class="form-control text-center required" id="tgl-terbit-'.$value->pm_id_mitra.'-'.$value->witel.'" name="tanggal_terbit" value="'.date('Y-m-d').'">
                        </div>
                        <div class="form-group col-md-3">
                          <label class="col-form-label pull-right" for="periode_kegiatan">Periode Kegiatan</label>
                            <select class="text-center required" id="periode-kegiatan-'.$value->pm_id_mitra.'-'.$value->witel.'" name="periode_kegiatan">
                            '.$txt_bf.'
                            </select>
                        </div>
                        <div class="form-group col-md-2">
                          <label class="col-form-label pull-right" for="date_periode1">Pelaksanaan Awal</label>
                            <input type="text" class="form-control input-transparent text-center" id="date-periode1-'.$value->pm_id_mitra.'-'.$value->witel.'" name="rekon_periode1" readonly>
                        </div>
                        <div class="form-group col-md-2">
                          <label class="col-form-label pull-right" for="date_periode2">Pelaksanaan Akhir</label>
                            <input type="text" class="form-control input-transparent text-center" id="date-periode2-'.$value->pm_id_mitra.'-'.$value->witel.'" name="rekon_periode2" readonly>
                        </div>
                        <div class="form-group col-md-1">
                            <label class="col-form-label pull-right" for="ppn_numb">PPN (%)</label>
                            <input type="text" class="form-control input-transparent required my-input-'.$value->pm_id_mitra.'-'.$value->witel.'" name="ppn_numb" value="11" readonly>
                        </div>
                        <div class="form-group col-md-2">
                            <label class="col-form-label pull-right" for="masa_berlaku_pks">Masa Berlaku PKS</label>
                            <input type="text" class="form-control date-picker text-center required" id="masa-berlaku-pks-'.$value->pm_id_mitra.'-'.$value->witel.'" name="masa_berlaku_pks" value="'.date('Y-m-d').'">
                            <span class="help-block"><small>Amandemen Terakhir</small></span>
                        </div>
                        <div class="form-group col-md-3">
                            <label class="col-form-label" for="toc">TOC</label>
                            <input type="text" id="toc-'.$value->pm_id_mitra.'-'.$value->witel.'" name="toc" class="form-control input-transparent required" autocomplete="off">
                            <span id="durasi-'.$value->pm_id_mitra.'-'.$value->witel.'"></span>
                        </div>
                        <div class="form-group col-md-3">
                            <label class="col-form-label pull-right" for="tgl_sp">Tanggal Surat Pesanan</label>
                            <input type="text" class="form-control date-picker text-center required" id="tgl-sp-'.$value->pm_id_mitra.'-'.$value->witel.'" name="tgl_sp" value="'.date('Y-m-d').'">
                        </div>
                        <div class="form-group col-md-3">
                            <label class="col-form-label pull-right" for="no_sp">Nomor Surat Pesanan</label>
                            <input type="text" class="form-control input-transparent text-center required" name="no_sp">
                        </div>
                        <div class="form-group col-md-3">
                            <label class="col-form-label pull-right" for="id_project">ID Project</label>
                            <input type="text" class="form-control input-transparent text-center required" name="id_project">
                        </div>
                    </section>

                    <h3>KPI 7 PSB</h3>
                    <section class="form-row">
                        <div class="form-group col-md-3">
                            <label class="col-form-label pull-right" for="kpi_real_pspi">KPI Real PS/PI (%)</label>
                            <input type="text" class="form-control input-transparent required" name="kpi_real_pspi" minlength="2" maxlength="5">
                        </div>
                        <div class="form-group col-md-3">
                            <label class="col-form-label pull-right" for="kpi_real_tticomply">KPI Real TTI Comply (%)</label>
                            <input type="text" class="form-control input-transparent required" name="kpi_real_tticomply" minlength="2" maxlength="5">
                        </div>
                        <div class="form-group col-md-3">
                            <label class="col-form-label pull-right" for="kpi_real_ffg">KPI Real FFG (%)</label>
                            <input type="text" class="form-control input-transparent required" name="kpi_real_ffg" minlength="2" maxlength="5">
                        </div>
                        <div class="form-group col-md-3">
                            <label class="col-form-label pull-right" for="kpi_real_validasicore">KPI Real Validasi Core (%)</label>
                            <input type="text" class="form-control input-transparent required" name="kpi_real_validasicore" minlength="2" maxlength="5">
                        </div>
                        <div class="form-group col-md-3">
                            <label class="col-form-label pull-right" for="kpi_real_ttrffg">KPI Real TTR FFG (%)</label>
                            <input type="text" class="form-control input-transparent required" name="kpi_real_ttrffg" minlength="2" maxlength="5">
                        </div>
                        <div class="form-group col-md-3">
                            <label class="col-form-label pull-right" for="kpi_real_ujipetik">KPI Real Uji Petik (%)</label>
                            <input type="text" class="form-control input-transparent required" name="kpi_real_ujipetik" minlength="2" maxlength="5">
                        </div>
                        <div class="form-group col-md-3">
                            <label class="col-form-label pull-right" for="kpi_real_qc2">KPI Real QC2 (%)</label>
                            <input type="text" class="form-control input-transparent required" name="kpi_real_qc2" minlength="2" maxlength="5">
                        </div>
                    </section>

                    <h3>Nomor Surat Dokumen</h3>
                    <section class="form-row">
                        <div class="form-group col-md-4">
                            <label class="col-form-label pull-right" for="invoice">Nomor Invoice</label>
                            <input type="text" class="form-control input-transparent text-center required" name="invoice">
                        </div>
                        <div class="form-group col-md-4">
                            <label class="col-form-label pull-right" for="spp_num">Nomor SPP</label>
                            <input type="text" class="form-control input-transparent text-center required" name="spp_num">
                        </div>
                        <div class="form-group col-md-4">
                            <label class="col-form-label pull-right" for="receipt_num">Nomor Kwitansi</label>
                            <input type="text" class="form-control input-transparent text-center required" name="receipt_num">
                        </div>
                        <div class="form-group col-md-4">
                            <label class="col-form-label pull-right" for="faktur">Nomor Seri Faktur Pajak</label>
                            <input type="text" class="form-control input-transparent text-center required" name="faktur">
                        </div>
                        <div class="form-group col-md-4">
                          <label class="col-form-label pull-right" for="surat_penetapan">Nomor Surat Penetapan</label>
                          <input type="text" class="form-control input-transparent text-center required" name="surat_penetapan">
                        </div>
                        <div class="form-group col-md-4">
                          <label class="col-form-label pull-right" for="surat_kesanggupan">Nomor Surat Kesanggupan</label>
                          <input type="text" class="form-control input-transparent text-center required" name="surat_kesanggupan">
                        </div>
                        <div class="form-group col-md-4">
                            <label class="col-form-label pull-right" for="tgl_faktur">Tanggal Faktur</label>
                            <input type="text" class="form-control date-picker text-center required" id="tgl-faktur-'.$value->pm_id_mitra.'-'.$value->witel.'" name="tgl_faktur" value="'.date('Y-m-d').'">
                        </div>
                        <div class="form-group col-md-4">
                            <label class="col-form-label pull-right" for="tgl_s_pen">Tanggal Surat Penetapan</label>
                            <input type="text" class="form-control date-picker text-center required" id="tgl-s-pen-'.$value->pm_id_mitra.'-'.$value->witel.'" name="tgl_s_pen" value="'.date('Y-m-d').'">
                        </div>
                        <div class="form-group col-md-4">
                            <label class="col-form-label pull-right" for="tgl_surat_sanggup">Tanggal Surat Kesanggupan</label>
                            <input type="text" class="form-control date-picker text-center required" id="tgl-surat-sanggup-'.$value->pm_id_mitra.'-'.$value->witel.'" name="tgl_surat_sanggup" value="'.date('Y-m-d').'">
                        </div>
                        <div class="form-group col-md-4">
                          <label class="col-form-label pull-right" for="no_baij">Nomor Surat BAR&IJ</label>
                          <input type="text" class="form-control input-transparent text-center required" name="no_baij">
                        </div>
                        <div class="form-group col-md-4">
                            <label class="col-form-label pull-right" for="no_ba_pemeriksaan">Nomor Surat BA Pemeriksaan</label>
                            <input type="text" class="form-control input-transparent text-center required" name="no_ba_pemeriksaan">
                        </div>
                        <div class="form-group col-md-4">
                            <label class="col-form-label pull-right" for="BAUT">Nomor Surat BAUT</label>
                            <input type="text" class="form-control input-transparent text-center required" name="BAUT">
                        </div>
                        <div class="form-group col-md-4">
                            <label class="col-form-label pull-right" for="BAST">Nomor Surat BAST</label>
                            <input type="text" class="form-control input-transparent text-center required" name="BAST">
                        </div>
                        <div class="form-group col-md-4">
                            <label class="col-form-label pull-right" for="no_bap">Nomor Surat BA Performansi</label>
                            <input type="text" class="form-control input-transparent text-center required" name="no_bap">
                        </div>
                        <div class="form-group col-md-4">
                            <label class="col-form-label pull-right" for="no_ba_pemeliharaan">Nomor BA Pemeliharaan</label>
                            <input type="text" class="form-control input-transparent text-center required" name="no_ba_pemeliharaan">
                        </div>
                    </section>

                    <h3>Finish</h3>
                    <section>
                        <label for="acceptTerms" style="text-align: left"> <input id="acceptTerms" name="acceptTerms" type="checkbox" class="required" /> Saya sebagai perwakilan mitra <strong>'.$value->mitra.'</strong> dari witel <strong>'.$value->witel.'</strong> mengkonfirmasi bahwa data yang dimasukan ke dalam aplikasi ini <strong>PROMISE</strong> benar dan dapat dipertanggungjawabkan kebenarannya.</label>
                    </section>

                  </div>
                </form>
              </div>

            </div>

          </div>

        </div>

        <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" style="margin-bottom: 5px;" data-target="#verticalModal-'.$value->pm_id_mitra.'-'.$value->witel.'"><i class="fe fe-edit"></i>&nbsp;Rekon</button>

        <script type="text/javascript">
        $(document).ready(function () {

          $(document).on("show.bs.modal", "#verticalModal-'.$value->pm_id_mitra.'-'.$value->witel.'", function (e) {

          var form = $("#form-'.$value->pm_id_mitra.'-'.$value->witel.'");
          form.length &&
            (form.children("div").steps({
            headerTag: "h3",
            bodyTag: "section",
            transitionEffect: "slideLeft",
            onStepChanging: function (e, a, o) {
              return (form.validate().settings.ignore = ":disabled,:hidden"), form.valid();
            },
            onFinishing: function (e, a) {
              return (form.validate().settings.ignore = ":disabled"), form.valid();
            },
            onFinished: function (e, a) {
              alert("Submitted!");
              form.submit()
            },
            }));

            $("#tgl-terbit-'.$value->pm_id_mitra.'-'.$value->witel.'").daterangepicker(
              {
                  singleDatePicker: true,
                  timePicker: false,
                  showDropdowns: true,
                  locale:
                  {
                      format: "YYYY-MM-DD",
                  }
              }
            );

            $("#tgl-sp-'.$value->pm_id_mitra.'-'.$value->witel.'").daterangepicker(
              {
                  singleDatePicker: true,
                  timePicker: false,
                  showDropdowns: true,
                  locale:
                  {
                      format: "YYYY-MM-DD",
                  }
              }
            );

            $("#masa-berlaku-pks-'.$value->pm_id_mitra.'-'.$value->witel.'").daterangepicker(
              {
                  singleDatePicker: true,
                  timePicker: false,
                  showDropdowns: true,
                  locale:
                  {
                      format: "YYYY-MM-DD",
                  }
              }
            );

            $("#tgl-faktur-'.$value->pm_id_mitra.'-'.$value->witel.'").daterangepicker(
              {
                  singleDatePicker: true,
                  timePicker: false,
                  showDropdowns: true,
                  locale:
                  {
                      format: "YYYY-MM-DD",
                  }
              }
            );

            $("#tgl-s-pen-'.$value->pm_id_mitra.'-'.$value->witel.'").daterangepicker(
              {
                  singleDatePicker: true,
                  timePicker: false,
                  showDropdowns: true,
                  locale:
                  {
                      format: "YYYY-MM-DD",
                  }
              }
            );

            $("#tgl-surat-sanggup-'.$value->pm_id_mitra.'-'.$value->witel.'").daterangepicker(
              {
                  singleDatePicker: true,
                  timePicker: false,
                  showDropdowns: true,
                  locale:
                  {
                      format: "YYYY-MM-DD",
                  }
              }
            );

            $("#periode-kegiatan-'.$value->pm_id_mitra.'-'.$value->witel.'").val(null).change();

            $("#periode-kegiatan-'.$value->pm_id_mitra.'-'.$value->witel.'").select2({
                placeholder: "Pilih Periode Kegiatan",
                width: "100%"
            });

            var periode = '.json_encode($bulan_format).'

            $("#periode-kegiatan-'.$value->pm_id_mitra.'-'.$value->witel.'").on( "select2 change", function(){
                var perd = this.value,
                d = new Date(),
                years = d.getFullYear()
                $.each(periode, function(key, index) {
                    if(index.id == perd ) {
                        console.log(index)
                        if(perd == "01") {
                            year = d.getFullYear() - 1
                        } else {
                            year = d.getFullYear()
                        }
                        $("#date-periode1-'.$value->pm_id_mitra.'-'.$value->witel.'").val(years + "-" + index.date_periode1)
                        $("#date-periode2-'.$value->pm_id_mitra.'-'.$value->witel.'").val(years + "-" + index.date_periode2)
                    }
                })
            })

            const myInput = document.querySelector(".my-input-'.$value->pm_id_mitra.'-'.$value->witel.'");
              myInput.addEventListener("input", function () {
                  this.value = this.value.replace(/[^0-9.]/g, "").replace(/(\..*?)\..*/g, "$1");
              })

              $("#tgl-sp-'.$value->pm_id_mitra.'-'.$value->witel.'").on("change", function(e) {

                var kalkulasi_date = new Date(new Date($("#tgl-sp-'.$value->pm_id_mitra.'-'.$value->witel.'").val()).setDate(new Date($("#tgl-sp-'.$value->pm_id_mitra.'-'.$value->witel.'").val()).getDate() + parseInt($("#toc-'.$value->pm_id_mitra.'-'.$value->witel.'").val().length != 0 ? parseInt($("#toc-'.$value->pm_id_mitra.'-'.$value->witel.'").val() ) - 1 : 0))).toISOString().slice(0, 10)
                $("#durasi-'.$value->pm_id_mitra.'-'.$value->witel.'").html("<code>*Tanggal TOC Adalah "+kalkulasi_date+"</code>");
              });

            $("#toc-'.$value->pm_id_mitra.'-'.$value->witel.'").on("keypress keyup", function(e) {
                var isi = $(this).val()
                if (/\D/g.test(isi))
                {
                  isi = isi.replace(/\D/g, "");
                }

                var kalkulasi_date = new Date(new Date($("#tgl-sp-'.$value->pm_id_mitra.'-'.$value->witel.'").val()).setDate(new Date($("#tgl-sp-'.$value->pm_id_mitra.'-'.$value->witel.'").val()).getDate() + parseInt(isi.length != 0 ? parseInt(isi )  - 1 : 0))).toISOString().slice(0, 10)
                $("#durasi-'.$value->pm_id_mitra.'-'.$value->witel.'").html("<code>*Tanggal TOC Adalah "+kalkulasi_date+"</code>");
            });

            if (/\D/g.test($("#toc-'.$value->pm_id_mitra.'-'.$value->witel.'").val()))
            {
              $("#toc-'.$value->pm_id_mitra.'-'.$value->witel.'").val() = $("#toc-'.$value->pm_id_mitra.'-'.$value->witel.'").val().replace(/\D/g, "");
            }
          });
        });
        </script>
        '
      ];
    }

    $result['footer'] = [
      '<a href="/Rekon/rekonDashboardOrderDetail?regional=ALL&witel=ALL&mitra=ALL&startDate='.$startDate.'&endDate='.$endDate.'&layanan=p1" style="color: #6c757d;">'.str_replace(',', '.', number_format($total_p1)).'</a>',
      '<a href="/Rekon/rekonDashboardOrderDetail?regional=ALL&witel=ALL&mitra=ALL&startDate='.$startDate.'&endDate='.$endDate.'&layanan=p2_tlpint" style="color: #6c757d;">'.str_replace(',', '.', number_format($total_p2_tlpint)).'</a>',
      '<a href="/Rekon/rekonDashboardOrderDetail?regional=ALL&witel=ALL&mitra=ALL&startDate='.$startDate.'&endDate='.$endDate.'&layanan=p2_intiptv" style="color: #6c757d;">'.str_replace(',', '.', number_format($total_p2_intiptv)).'</a>',
      '<a href="/Rekon/rekonDashboardOrderDetail?regional=ALL&witel=ALL&mitra=ALL&startDate='.$startDate.'&endDate='.$endDate.'&layanan=p3" style="color: #6c757d;">'.str_replace(',', '.', number_format($total_p3)).'</a>',
      '<a href="/Rekon/rekonDashboardOrderDetail?regional=ALL&witel=ALL&mitra=ALL&startDate='.$startDate.'&endDate='.$endDate.'&layanan=p1_survey" style="color: #6c757d;">'.str_replace(',', '.', number_format($total_p1_survey)).'</a>',
      '<a href="/Rekon/rekonDashboardOrderDetail?regional=ALL&witel=ALL&mitra=ALL&startDate='.$startDate.'&endDate='.$endDate.'&layanan=p2_tlpint_survey" style="color: #6c757d;">'.str_replace(',', '.', number_format($total_p2_tlpint_survey)).'</a>',
      '<a href="/Rekon/rekonDashboardOrderDetail?regional=ALL&witel=ALL&mitra=ALL&startDate='.$startDate.'&endDate='.$endDate.'&layanan=p2_intiptv_survey" style="color: #6c757d;">'.str_replace(',', '.', number_format($total_p2_intiptv_survey)).'</a>',
      '<a href="/Rekon/rekonDashboardOrderDetail?regional=ALL&witel=ALL&mitra=ALL&startDate='.$startDate.'&endDate='.$endDate.'&layanan=p3_survey" style="color: #6c757d;">'.str_replace(',', '.', number_format($total_p3_survey)).'</a>',
      '<a href="/Rekon/rekonDashboardOrderDetail?regional=ALL&witel=ALL&mitra=ALL&startDate='.$startDate.'&endDate='.$endDate.'&layanan=migrasi_service_1p2p" style="color: #6c757d;">'.str_replace(',', '.', number_format($total_migrasi_service_1p2p)).'</a>',
      '<a href="/Rekon/rekonDashboardOrderDetail?regional=ALL&witel=ALL&mitra=ALL&startDate='.$startDate.'&endDate='.$endDate.'&layanan=migrasi_service_1p3p" style="color: #6c757d;">'.str_replace(',', '.', number_format($total_migrasi_service_1p3p)).'</a>',
      '<a href="/Rekon/rekonDashboardOrderDetail?regional=ALL&witel=ALL&mitra=ALL&startDate='.$startDate.'&endDate='.$endDate.'&layanan=migrasi_service_2p3p" style="color: #6c757d;">'.str_replace(',', '.', number_format($total_migrasi_service_2p3p)).'</a>',
      '<a href="/Rekon/rekonDashboardOrderDetail?regional=ALL&witel=ALL&mitra=ALL&startDate='.$startDate.'&endDate='.$endDate.'&layanan=addon_nok" style="color: #6c757d;">'.str_replace(',', '.', number_format($total_addon_nok)).'</a>',
      '<a href="/Rekon/rekonDashboardOrderDetail?regional=ALL&witel=ALL&mitra=ALL&startDate='.$startDate.'&endDate='.$endDate.'&layanan=addon_ok" style="color: #6c757d;">'.str_replace(',', '.', number_format($total_addon_ok)).'</a>',
      '<a href="/Rekon/rekonDashboardOrderDetail?regional=ALL&witel=ALL&mitra=ALL&startDate='.$startDate.'&endDate='.$endDate.'&layanan=belum_rekon" style="color: #6c757d;">'.str_replace(',', '.', number_format($total_belum_rekon)).'</a>',
      '<a href="/Rekon/rekonDashboardOrderDetail?regional=ALL&witel=ALL&mitra=ALL&startDate='.$startDate.'&endDate='.$endDate.'&layanan=sudah_rekon" style="color: #6c757d;">'.str_replace(',', '.', number_format($total_sudah_rekon)).'</a>',
      ''
    ];

    return \Response::json($result);
  }

  public function rekonDashboardOrderDetail()
  {
    $regional = Input::get('regional');
    $witel = Input::get('witel');
    $mitra = Input::get('mitra');
    $periode = Input::get('periode');
    $startDate = Input::get('startDate');
    $endDate = Input::get('endDate');
    $layanan = Input::get('layanan');

    return view('Rekon.dashboard_order_detail', compact('regional', 'witel', 'mitra', 'startDate', 'endDate', 'layanan'));
  }

  public function ajx_rekonDashboardOrderDetail()
  {
    $regional = Input::get('regional');
    $witel = Input::get('witel');
    $mitra = Input::get('mitra');
    $startDate = Input::get('startDate');
    $endDate = Input::get('endDate');
    $layanan = Input::get('layanan');
    $key = 1;

    if ($regional == null)
    {
      $regional = 'REG6';
    }
    if ($witel == null)
    {
      $witel = 'ALL';
    }
    if ($mitra == null)
    {
      $mitra = 'ALL';
    }
    if ($startDate == null)
    {
      $startDate = date('Y-m-01');
    }
    if ($endDate == null)
    {
      $endDate = date('Y-m-d');
    }
    if ($layanan == null)
    {
      $layanan = 'ALL';
    }

    $data = RekonModel::dashboard_order_detail($regional, $witel, $mitra, $startDate, $endDate, $layanan);

    $result['data'] = [];
    foreach ($data as $key => $value) {
      $result['data'][] = [
        $key++,
        $value->nik_teknisi1,
        $value->nik_teknisi2,
        $value->plp_layanan,
        $value->regional,
        $value->witel,
        $value->mitra,
        $value->datel,
        $value->sto,
        $value->order_id,
        $value->package_name,
        $value->flag_deposit,
        $value->type_transaksi,
        $value->type_layanan,
        $value->ncli,
        $value->pots,
        $value->speedy,
        $value->status_resume,
        $value->status_message,
        $value->order_date,
        $value->last_updated_date,
        $value->customer_name,
        $value->no_hp,
        $value->install_address,
        $value->kcontact,
        $value->gps_longitude,
        $value->gps_latitude,
        $value->status_utonline,
        $value->serial_number_ont,
        $value->serial_number_stb,
        $value->serial_number_other,
        $value->drop_core,
        $value->precon,
        $value->tiang,
        $value->klem_ring,
        $value->otp,
        $value->prekso,
        $value->s_clamp,
        $value->soc,
        $value->clamp_hook,
        $value->tray_cable
      ];
    }

    return \Response::json($result);
  }

  public function dashboard()
  {
    $startDate  = Input::input('startDate');
    $endDate    = Input::input('endDate');
    $witel_ms2n = session('auth')->Witel_New;

    $get_mitra = DB::table('procurement_mitra')->where('witel', $witel_ms2n)->where('active', 1)->where('tacticalpro_mitra_id', '!=', 0)->select('tacticalpro_mitra_id AS id', 'nama_company AS text')->groupBy('nama_company')->get();
    $get_layanan = DB::table('procurement_layanan_psb')->where('aktif', 1)->select('alias_layanan AS id', 'layanan AS text')->get();
    $bulan_format = DB::table('bulan_format')->select('id', 'bulan AS text', 'date_periode1', 'date_periode2')->get();

    $log_kpro = RekonModel::log_kpro();
    $log_utonline = RekonModel::log_utonline();

    switch ($witel_ms2n) {
      case 'KALSEL':
        $witel = 'BANJARMASIN';
        break;
      case 'BALIKPAPAN':
        $witel = $witel_ms2n;
        break;
    }

    return view('Rekon.dashboard', compact('startDate', 'endDate', 'witel_ms2n', 'witel', 'get_layanan', 'log_kpro', 'log_utonline', 'bulan_format', 'get_mitra'));
  }

  public function ajx_rekonDashboard()
  {
    $startDate = Input::input('startDate');
    $endDate = Input::input('endDate');

    $total_dps_nok = $total_dps_ok = $jumlah_dps = $total_dps = $total_kpt_nok = $total_kpt_ok = $jumlah_kpt = $total_kpt_wms = $total_kpt = $total_nok_ut = $total_jml_tiang = 0;

    $dashboardRekon = RekonModel::dashboardRekon($startDate, $endDate);

    foreach ($dashboardRekon as $mitra => $value) {
      // STARCLICK NCX
      $total_dps_nok += @$value['jumlah_dps_nok'];
      $total_dps_ok += @$value['jumlah_dps_ok'];
      $jumlah_dps = (@$value['jumlah_dps_nok'] + @$value['jumlah_dps_ok']);
      $total_dps += $jumlah_dps;
      // PS K-PRO
      $total_kpt_nok += @$value['jumlah_kpt_nok'];
      $total_kpt_ok += @$value['jumlah_kpt_ok'];
      $jumlah_kpt = (@$value['jumlah_kpt_nok'] + @$value['jumlah_kpt_ok']);
      $total_kpt += $jumlah_kpt;
      $total_kpt_wms += @$value['jumlah_kpt_wms'];
      $total_nok_ut += @$value['nok_ut'];

      // INSERT TIANG
      $total_jml_tiang += @$value['jml_tiang'];

      $result['data'][] = [
        $mitra,
        // STARCLICK NCX
        '<a href="/Rekon/dashboardRekonProvisioning?source=starclick&mitra='.$mitra.'&status=NOK&startDate='.$startDate.'&endDate='.$endDate.'" target="_blank" style="color: #6c757d;">'.str_replace(',', '.', number_format(@$value['jumlah_dps_nok'] ? : '0')).'</a>',
        '<a href="/Rekon/dashboardRekonProvisioning?source=starclick&mitra='.$mitra.'&status=OK&startDate='.$startDate.'&endDate='.$endDate.'" target="_blank" style="color: #6c757d;">'.str_replace(',', '.', number_format(@$value['jumlah_dps_ok'] ? : '0')).'</a>',
        '<a href="/Rekon/dashboardRekonProvisioning?source=starclick&mitra='.$mitra.'&status=ALL&startDate='.$startDate.'&endDate='.$endDate.'" target="_blank" style="color: #6c757d;">'.str_replace(',', '.', number_format($jumlah_dps)).'</a>',
        // PS K-PRO
        '<a href="/Rekon/dashboardRekonProvisioning?source=kpro&mitra='.$mitra.'&status=NOK&startDate='.$startDate.'&endDate='.$endDate.'" target="_blank" style="color: #6c757d;">'.str_replace(',', '.', number_format(@$value['jumlah_kpt_nok'] ? : '0')).'</a>',
        '<a href="/Rekon/dashboardRekonProvisioning?source=kpro&mitra='.$mitra.'&status=OK&startDate='.$startDate.'&endDate='.$endDate.'" target="_blank" style="color: #6c757d;">'.str_replace(',', '.', number_format(@$value['jumlah_kpt_ok'] ? : '0')).'</a>',
        '<a href="/Rekon/dashboardRekonProvisioning?source=kpro&mitra='.$mitra.'&status=ALL&startDate='.$startDate.'&endDate='.$endDate.'" target="_blank" style="color: #6c757d;">'.str_replace(',', '.', number_format($jumlah_kpt)).'</a>',
        '<a href="/Rekon/dashboardRekonProvisioning?source=kpro&mitra='.$mitra.'&status=WMS&startDate='.$startDate.'&endDate='.$endDate.'" target="_blank" style="color: #6c757d;">'.str_replace(',', '.', number_format(@$value['jumlah_kpt_wms'] ? : '0')).'</a>',
        '<a href="/Rekon/dashboardRekonProvisioning?source=kpro&mitra='.$mitra.'&status=UTONLINENOK&startDate='.$startDate.'&endDate='.$endDate.'" target="_blank" style="color: #6c757d;">'.str_replace(',', '.', number_format(@$value['nok_ut'] ? : '0')).'</a>',
        // INSERT TIANG
        '<a href="/Rekon/dashboardRekonTiang?mitra='.$mitra.'&startDate='.$startDate.'&endDate='.$endDate.'" target="_blank" style="color: #6c757d;">'.str_replace(',', '.', number_format(@$value['jml_tiang'] ? : '0')).'</a>'
      ];
    }

    $result['footer']= [
    // STARCLICK NCX
    '<a href="/Rekon/dashboardRekonProvisioning?source=starclick&mitra=ALL&status=NOK&startDate='.$startDate.'&endDate='.$endDate.'" target="_blank" style="color: #6c757d;">'.str_replace(',', '.', number_format($total_dps_nok)).'</a>',
    '<a href="/Rekon/dashboardRekonProvisioning?source=starclick&mitra=ALL&status=OK&startDate='.$startDate.'&endDate='.$endDate.'" target="_blank" style="color: #6c757d;">'.str_replace(',', '.', number_format($total_dps_ok)).'</a>',
    '<a href="/Rekon/dashboardRekonProvisioning?source=starclick&mitra=ALL&status=ALL&startDate='.$startDate.'&endDate='.$endDate.'" target="_blank" style="color: #6c757d;">'.str_replace(',', '.', number_format($total_dps)).'</a>',
    // PS K-PRO
    '<a href="/Rekon/dashboardRekonProvisioning?source=kpro&mitra=ALL&status=NOK&startDate='.$startDate.'&endDate='.$endDate.'" target="_blank" style="color: #6c757d;">'.str_replace(',', '.', number_format($total_kpt_nok)).'</a>',
    '<a href="/Rekon/dashboardRekonProvisioning?source=kpro&mitra=ALL&status=OK&startDate='.$startDate.'&endDate='.$endDate.'" target="_blank" style="color: #6c757d;">'.str_replace(',', '.', number_format($total_kpt_ok)).'</a>',
    '<a href="/Rekon/dashboardRekonProvisioning?source=kpro&mitra=ALL&status=ALL&startDate='.$startDate.'&endDate='.$endDate.'" target="_blank" style="color: #6c757d;">'.str_replace(',', '.', number_format($total_kpt)).'</a>',
    '<a href="/Rekon/dashboardRekonProvisioning?source=kpro&mitra=ALL&status=WMS&startDate='.$startDate.'&endDate='.$endDate.'" target="_blank" style="color: #6c757d;">'.str_replace(',', '.', number_format($total_kpt_wms)).'</a>',
    '<a href="/Rekon/dashboardRekonProvisioning?source=kpro&mitra=ALL&status=UTONLINENOK&startDate='.$startDate.'&endDate='.$endDate.'" target="_blank" style="color: white !important;">'.str_replace(',', '.', number_format($total_nok_ut)).'</a>',
    // INSERT TIANG
    '<a href="/Rekon/dashboardRekonTiang?mitra=ALL&startDate='.$startDate.'&endDate='.$endDate.'" target="_blank" style="color: #6c757d;">'.str_replace(',', '.', number_format($total_jml_tiang)).'</a>'
    ];

    return \Response::json($result);
  }

  public function dashboardRekonProvisioning()
  {
    $source = Input::get('source');
    $mitra = Input::get('mitra');
    $status = Input::get('status');
    $startDate = Input::input('startDate');
    $endDate = Input::input('endDate');

    return view('Rekon.dashboardRekonProvisioning', compact('source','mitra','status','startDate','endDate','nper'));
  }

  public function ajx_rekonProvisioning()
  {
    $source = Input::get('source');
    $mitra = Input::get('mitra');
    $status = Input::get('status');
    $startDate = Input::input('startDate');
    $endDate = Input::input('endDate');

    $data = RekonModel::dashboardRekonProvisioning($source, $mitra, $status, $startDate, $endDate);

    $totalPrecon100 = $totalPrecon80 = $totalPrecon75 = $totalPrecon50 = $totalPrecon150nonAcc = $totalPreconn = 0;
    $a = 100; $b = 80; $c = 75; $d = 50; $f = 150; $pc5 = 5; $pc10 = 10; $pc15 = 15; $pc20 = 20; $pc30 = 30;

    $result = [];
    $numb = 1;

    foreach ($data as $key => $value) {

      $total_pc5 = ($value->patchcore_5 * $pc5);
      $total_pc10 = ($value->patchcore_10 * $pc10);
      $total_pc15 = ($value->patchcore_15 * $pc15);
      $total_pc20 = ($value->patchcore_20 * $pc20);
      $total_pc30 = ($value->patchcore_30 * $pc30);
      $total_pc_all = $total_pc5 + $total_pc10 + $total_pc15 + $total_pc20 + $total_pc30;
      $totalPrecon100 = ($value->precon100 * $a);
      $totalPrecon80 = ($value->precon80 * $b);
      $totalPrecon75 = ($value->precon75 * $c);
      $totalPrecon50 = ($value->precon50 * $d);
      $totalPrecon150nonAcc = ($value->precon150 * $f);
      $totalPreconn = $totalPrecon100 + $totalPrecon80 + $totalPrecon75 + $totalPrecon50 + $totalPrecon150nonAcc + $value->dc_roll;

      switch ($value->dtjl_mode) {
        case '1':
            $dtjl_mode = 'KU';
          break;

        default:
            $dtjl_mode = 'KT';
          break;
      }

      $detail_precon = "DC_ROLL : ".$value->dc_roll." \nPRECONN : ".($totalPrecon100 + $totalPrecon80 + $totalPrecon75 + $totalPrecon50 + $totalPrecon150nonAcc)."";

      if ($value->track_id_int)
      {
        $flag_byod = 'Order BYOD';
      } else {
        $flag_byod = '-';
      }

      $periode_rekon = $value->periode1.' - '.$value->periode2;

      $result['data'][] = [
        $numb++,
        $value->SM ?? '-',
        $value->TL ?? '-',
        $value->mitra_amija_pt ?? '-',
        $value->tim ?? '-',
        $value->orderName ?? @$value->kpt_orderName ?? '-',
        $value->orderId ?? @$value->kpt_orderId ?? '-',
        $value->myir ?? @$value->kpt_myir ?? '-',
        $value->orderDate ?? '-',
        $value->orderDatePs ?? '-',
        // $periode_rekon ?? '-',
        $flag_byod ?? '-',
        @$value->kpt_nper ?? '-',
        $value->tgl_dispatch ?? '-',
        $value->modified_at ?? '-',
        $value->sto ?? @$value->kpt_sto ?? '-',
        $value->internet ?? @$value->kpt_internet ?? '-',
        $value->noTelp ?? @$value->kpt_noTelp ?? '-',
        $value->orderStatus ?? @$value->kpt_orderStatus ?? '-',
        $value->utt_statusName ?? '-',
        $value->utt_create_dtm ?? '-',
        $value->orderAddr ?? '-',
        $value->lat_pelanggan ?? '-',
        $value->long_pelanggan ?? '-',
        strtoupper($value->loc_id) ?? '-',
        strtoupper($value->nama_odp) ?? '-',
        $value->lat_odp ?? '-',
        $value->long_odp ?? '-',
        $value->datel ?? '-',
        $value->laporan_status ?? "NO UPDATE" ?? '-',
        $value->catatan ?? '-',
        $value->agent_id ?? @$value->kpt_agent_id ?? '-',
        $value->jenisPsb ?? @$value->kpt_jenisPsb ?? '-',
        $value->dps_provider ?? @$value->kpt_provider ?? '-',
        strtoupper($value->dtjl_jenis_layanan) ?? '-',
        $value->TYPE_TRANSAKSI ?? '-',
        $value->TYPE_LAYANAN ?? '-',
        $value->kpt_group_channel ?? '-',
        $value->kpt_product ?? '-',
        $value->kpt_flag_deposit ?? '-',
        $value->rfc_number ?? '-',
        $value->typeont ?? '-',
        $value->snont ?? '-',
        $value->typestb ?? '-',
        $value->snstb ?? '-',
        $dtjl_mode ?? '-',
        $value->adsc ?? '0',
        $value->roset ?? '0',
        $value->prekso_15 ?? '0',
        $value->prekso_20 ?? '0',
        $value->dc_roll ?? '0',
        $value->precon50 ?? '0',
        $value->precon75 ?? '0',
        $value->precon80 ?? '0',
        $value->precon100 ?? '0',
        $value->precon150 ?? '0',
        $totalPreconn ?? '0',
        $detail_precon ?? '-',
        $total_pc_all ?? '0',
        $value->utp ?? '0',
        $value->tiang ?? '0',
        $value->jml_tiang ?? '0',
        $value->tray_cable ?? '0',
        $value->breket ?? '0',
        $value->clamps ?? '0',
        $value->pulstrap ?? '0',
        $value->rj45 ?? '0',
        $value->soc_ils ?? '0',
        $value->soc_sum ?? '0'
      ];
    }

    return \Response::json($result);
  }

  public function dashboardRekonTiang()
  {
    $mitra = Input::get('mitra');
    $start = Input::get('startDate');
    $end = Input::get('endDate');

    return view('Rekon.dashboardRekonTiang', compact('mitra', 'start', 'end'));
  }

  public function ajx_rekonTiang()
  {
    $mitra = Input::get('mitra');
    $start = Input::get('startDate');
    $end = Input::get('endDate');

    $data = RekonModel::dashboardRekonTiang($mitra, $start, $end);

    $result = [];
    $numb = 1;

    foreach ($data as $key => $value)
    {
      if ($value->verify == null)
      {
      $verify = 'Perlu Verif TA (Teknisi)';
      } elseif ($value->verify == 1) {
        $verify = 'Verif TA';
      } elseif ($value->verify == 2) {
        $verify = 'Verif Telkom';
      } elseif ($value->verify == 3) {
        $verify = 'Siap Rekon';
      } elseif ($value->verify == 4) {
        $verify = 'Decline dari TA';
      } elseif ($value->verify == 5) {
        $verify = 'Decline dari Telkom';
      }

      $result['data'][] = [
        $numb++,
        $value->mitra,
        $value->tim,
        $verify,
        $value->order_from,
        $value->no_tiket,
        $value->orderId ?? '-',
        $value->nama_pelanggan ?? $value->nama_pelanggan2 ?? '-',
        $value->inet ?? $value->inet2 ?? '-',
        $value->STATUS_RESUME ?? $value->orderStatus ?? '-',
        $value->ORDER_DATE ?? $value->orderDate ?? $value->pmw_orderDate ??'-',
        $value->LAST_UPDATED_DATE ?? $value->orderDatePs ?? '-',
        $value->alamat_pelanggan ?? '-',
        $value->transaksi ?? '-',
        $value->layanan ?? '-',
        $value->jml_tiang
      ];
    }

    return \Response::json($result);
  }

  public function ajx_rekonProvisioningKPRO()
  {
    $view      = Input::get('view');
    $witel     = Input::get('witel');
    $startDate = Input::input('startDate');
    $endDate   = Input::input('endDate');

    $data = RekonModel::rekonProvisioningKPRO($view, $witel, $startDate, $endDate);
    $hss_witel = DB::table('procurement_hss_psb')->where('witel_ms2n', $witel)->first();

    $total_p1 = $total_p2_tlpint = $total_p2_intiptv = $total_p3 = $total_p1_survey = $total_p2_tlpint_survey = $total_p2_intiptv_survey = $total_p3_survey = $total_wms = $total_wms_lite = $total_addon_stb = $total_second_stb = $total_ont_premium = $total_ont_premium_dns = $total_migrasi_stb_dns = $total_wifiext = $total_meshwifi = $total_plc = $total_indihome_smart = $total_tiang = $grand_total = $grand_total_plus = 0;

    foreach ($data as $key => $value)
    {
      switch ($view) {
        case 'MONTH':
          $result['data'][$value->area][] = $value->area;

          $months = ['januari' => '01', 'februari' => '02', 'maret' => '03', 'april' => '04', 'mei' => '05', 'juni' => '06', 'juli' => '07', 'agustus' => '08', 'september' => '09', 'oktober' => '10', 'november' => '11', 'desember' => '12'];

          foreach ($months as $key => $value_m)
          {
            $nper = date('Y').''.$value_m;
            $result['data'][$value->area][] = '<a href="/Rekon/rekonOrderKproDetail?view=MONTH&witel='.$value->area.'&area=null&layanan=p1&startDate=null&endDate=null&nper='.$nper.'" target="_blank" style="color: #6c757d">'.str_replace(',', '.', number_format($value->$key)).'</a>';
          }

          $result['data'][$value->area][] = number_format($value->januari + $value->februari + $value->maret + $value->april + $value->mei + $value->juni + $value->juli + $value->agustus + $value->september + $value->oktober + $value->november + $value->desember);

          $result['data'] = array_values($result['data']);
          break;

        case 'DAY':
          $result['data'][$value->area][] = $value->area;

          for ($i = 1; $i < date('t') + 1; $i ++)
          { 
            if ($i < 10)
            {
              $keys = '0'.$i;
            } else {
              $keys = $i;
            }
            $txt_ps_kpro = 'ps_kpro_d'.$keys;
            $result['data'][$value->area][] = '<a href="/Rekon/rekonOrderKproDetail?view=DAY&witel='.$value->area.'&area=null&layanan=p1&startDate=null&endDate=null&nper='.date('Y-m-'.$keys.'').'" target="_blank" style="color: #6c757d;">'.str_replace(',', '.', number_format($value->$txt_ps_kpro)).'</a>';
          }

          $result['data'] = array_values($result['data']);
          break;

        case 'ORDER':
          $total_p1                += $value->p1;
          $total_p2_tlpint         += $value->p2_tlpint;
          $total_p2_intiptv        += $value->p2_intiptv;
          $total_p3                += $value->p3;
          $total_p1_survey         += $value->p1_survey;
          $total_p2_tlpint_survey  += $value->p2_tlpint_survey;
          $total_p2_intiptv_survey += $value->p2_intiptv_survey;
          $total_p3_survey         += $value->p3_survey;
          $total_wms               += $value->wms;
          $total_wms_lite          += $value->wms_lite;
          $total_addon_stb         += $value->addon_stb;
          $total_second_stb        += $value->second_stb;
          $total_ont_premium       += $value->ont_premium;
          $total_ont_premium_dns   += $value->ont_premium_dns;
          $total_migrasi_stb_dns   += $value->migrasi_stb_dns;
          $total_wifiext           += $value->wifiext;
          $total_meshwifi          += $value->meshwifi;
          $total_plc               += $value->plc;
          $total_indihome_smart    += $value->indihome_smart;
          $total_tiang             += $value->tiang;
          $grand_total             =  ($value->p1 + $value->p2_tlpint + $value->p2_intiptv + $value->p3) + ($value->p1_survey + $value->p2_tlpint_survey + $value->p2_intiptv_survey + $value->p3_survey) + ($value->wms + $value->wms_lite + $value->addon_stb + $value->second_stb + $value->ont_premium + $value->ont_premium_dns + $value->migrasi_stb_dns + $value->wifiext + $value->plc + $value->indihome_smart + $value->tiang);
          $grand_total_plus        += $grand_total;

          $area = $value->area ?? 'NON AREA';

          if ($witel == 'BANJARMASIN')
          {
            $tbody_ontpremium = '<a href="/Rekon/rekonOrderDnsDetail?witel='.$value->witel.'&area='.$area.'&layanan=ont_premium_dns&startDate='.$startDate.'&endDate='.$endDate.'" target="_blank" style="color: #6c757d;">'.str_replace(',', '.', number_format($value->ont_premium_dns ? : '0')).'</a>';
            $tfoot_ontpremium = '<a href="/Rekon/rekonOrderDnsDetail?witel='.$value->witel.'&area=ALL&layanan=ont_premium_dns&startDate='.$startDate.'&endDate='.$endDate.'" target="_blank" style="color: #6c757d;">'.str_replace(',', '.', number_format($total_ont_premium_dns ? : '0')).'</a>';

            $tbody_migrasistb = '<a href="/Rekon/rekonOrderDnsDetail?witel='.$value->witel.'&area='.$area.'&layanan=migrasi_stb_dns&startDate='.$startDate.'&endDate='.$endDate.'" target="_blank" style="color: #6c757d;">'.str_replace(',', '.', number_format($value->migrasi_stb_dns ? : '0')).'</a>';
            $tfoot_migrasistb = '<a href="/Rekon/rekonOrderDnsDetail?witel='.$value->witel.'&area=ALL&layanan=migrasi_stb_dns&startDate='.$startDate.'&endDate='.$endDate.'" target="_blank" style="color: #6c757d;">'.str_replace(',', '.', number_format($total_migrasi_stb_dns ? : '0')).'</a>';

            $tbody_tiang = '<a href="/Rekon/dashboardRekonTiang?mitra='.$value->area.'&startDate='.$startDate.'&endDate='.$endDate.'" target="_blank" style="color: #6c757d;">'.str_replace(',', '.', number_format($value->tiang ? : '0')).'</a>';
            $tbody_tiang_jasa = str_replace(',', '.', number_format(($value->tiang * $hss_witel->insert_tiang) ? : '0'));
            $jml_tiang_jasa = ($value->tiang * $hss_witel->insert_tiang);
            $tfoot_tiang = '<a href="/Rekon/dashboardRekonTiang?mitra=ALL&startDate='.$startDate.'&endDate='.$endDate.'" target="_blank" style="color: #6c757d;">'.str_replace(',', '.', number_format($total_tiang ? : '0')).'</a>';
            $tfoot_tiang_jasa = str_replace(',', '.', number_format(($total_tiang * $hss_witel->insert_tiang) ? : '0'));
            $total_tiang_jasa = ($total_tiang * $hss_witel->insert_tiang);
          } else {
            $tbody_tiang = $tbody_tiang_jasa = $tfoot_tiang = $tfoot_tiang_jasa = $tbody_ontpremium = $tfoot_ontpremium = $tbody_migrasistb = $tfoot_migrasistb = '-';
            $jml_tiang_jasa = $total_tiang_jasa = 0;
          };

          $result['data'][] = [
            $value->reg,
            $value->witel,
            $area,
            '<a href="/Rekon/rekonOrderKproDetail?view=ORDER&witel='.$value->witel.'&area='.$area.'&layanan=p1&startDate='.$startDate.'&endDate='.$endDate.'&nper=null" target="_blank" style="color: #6c757d;">'.str_replace(',', '.', number_format($value->p1 ? : '0')).'</a>',
            str_replace(',', '.', number_format(($value->p1 * $hss_witel->p1_ku) ? : '0')),
            '<a href="/Rekon/rekonOrderKproDetail?view=ORDER&witel='.$value->witel.'&area='.$area.'&layanan=p2_tlpint&startDate='.$startDate.'&endDate='.$endDate.'&nper=null" target="_blank" style="color: #6c757d;">'.str_replace(',', '.', number_format($value->p2_tlpint ? : '0')).'</a>',
            str_replace(',', '.', number_format(($value->p2_tlpint * $hss_witel->p2_inet_voice_ku) ? : '0')),
            '<a href="/Rekon/rekonOrderKproDetail?view=ORDER&witel='.$value->witel.'&area='.$area.'&layanan=p2_intiptv&startDate='.$startDate.'&endDate='.$endDate.'&nper=null" target="_blank" style="color: #6c757d;">'.str_replace(',', '.', number_format($value->p2_intiptv ? : '0')).'</a>',
            str_replace(',', '.', number_format(($value->p2_intiptv * $hss_witel->p2_inet_iptv_ku) ? : '0')),
            '<a href="/Rekon/rekonOrderKproDetail?view=ORDER&witel='.$value->witel.'&area='.$area.'&layanan=p3&startDate='.$startDate.'&endDate='.$endDate.'&nper=null" target="_blank" style="color: #6c757d;">'.str_replace(',', '.', number_format($value->p3 ? : '0')).'</a>',
            str_replace(',', '.', number_format(($value->p3 * $hss_witel->p3_ku) ? : '0')),
            '<a href="/Rekon/rekonOrderKproDetail?view=ORDER&witel='.$value->witel.'&area='.$area.'&layanan=p1_survey&startDate='.$startDate.'&endDate='.$endDate.'&nper=null" target="_blank" style="color: #6c757d;">'.str_replace(',', '.', number_format($value->p1_survey ? : '0')).'</a>',
            str_replace(',', '.', number_format(($value->p1_survey * $hss_witel->p1) ? : '0')),
            '<a href="/Rekon/rekonOrderKproDetail?view=ORDER&witel='.$value->witel.'&area='.$area.'&layanan=p2_tlpint_survey&startDate='.$startDate.'&endDate='.$endDate.'&nper=null" target="_blank" style="color: #6c757d;">'.str_replace(',', '.', number_format($value->p2_tlpint_survey ? : '0')).'</a>',
            str_replace(',', '.', number_format(($value->p2_tlpint_survey * $hss_witel->p2_inet_voice) ? : '0')),
            '<a href="/Rekon/rekonOrderKproDetail?view=ORDER&witel='.$value->witel.'&area='.$area.'&layanan=p2_intiptv_survey&startDate='.$startDate.'&endDate='.$endDate.'&nper=null" target="_blank" style="color: #6c757d;">'.str_replace(',', '.', number_format($value->p2_intiptv_survey ? : '0')).'</a>',
            str_replace(',', '.', number_format(($value->p2_intiptv_survey * $hss_witel->p2_inet_iptv) ? : '0')),
            '<a href="/Rekon/rekonOrderKproDetail?view=ORDER&witel='.$value->witel.'&area='.$area.'&layanan=p3_survey&startDate='.$startDate.'&endDate='.$endDate.'&nper=null" target="_blank" style="color: #6c757d;">'.str_replace(',', '.', number_format($value->p3_survey ? : '0')).'</a>',
            str_replace(',', '.', number_format(($value->p3_survey * $hss_witel->p3) ? : '0')),
            '<a href="/Rekon/rekonOrderKproDetail?view=ORDER&witel='.$value->witel.'&area='.$area.'&layanan=wms&startDate='.$startDate.'&endDate='.$endDate.'&nper=null" target="_blank" style="color: #6c757d;">'.str_replace(',', '.', number_format($value->wms ? : '0')).'</a>',
            str_replace(',', '.', number_format((($value->wms * $hss_witel->lme_wifi) + ($value->wms * $hss_witel->lme_ap_indoor)) ? : '0')),
            '<a href="/Rekon/rekonOrderKproDetail?view=ORDER&witel='.$value->witel.'&area='.$area.'&layanan=wms_lite&startDate='.$startDate.'&endDate='.$endDate.'&nper=null" target="_blank" style="color: #6c757d;">'.str_replace(',', '.', number_format($value->wms_lite ? : '0')).'</a>',
            str_replace(',', '.', number_format(($value->wms_lite * $hss_witel->lme_wifi) ? : '0')),
            '<a href="/Rekon/rekonOrderKproDetail?view=ORDER&witel='.$value->witel.'&area='.$area.'&layanan=addon_stb&startDate='.$startDate.'&endDate='.$endDate.'&nper=null" target="_blank" style="color: #6c757d;">'.str_replace(',', '.', number_format($value->addon_stb ? : '0')).'</a>',
            str_replace(',', '.', number_format(($value->addon_stb * $hss_witel->stb_tambahan) ? : '0')),
            '<a href="/Rekon/rekonOrderKproDetail?view=ORDER&witel='.$value->witel.'&area='.$area.'&layanan=second_stb&startDate='.$startDate.'&endDate='.$endDate.'&nper=null" target="_blank" style="color: #6c757d;">'.str_replace(',', '.', number_format($value->second_stb ? : '0')).'</a>',
            str_replace(',', '.', number_format(($value->second_stb * $hss_witel->stb_tambahan) ? : '0')),
            '<a href="/Rekon/rekonOrderKproDetail?view=ORDER&witel='.$value->witel.'&area='.$area.'&layanan=ont_premium&startDate='.$startDate.'&endDate='.$endDate.'&nper=null" target="_blank" style="color: #6c757d;">'.str_replace(',', '.', number_format($value->ont_premium ? : '0')).'</a>',
            str_replace(',', '.', number_format(($value->ont_premium * $hss_witel->ont_premium) ? : '0')),
            $tbody_ontpremium,
            $tbody_migrasistb,
            '<a href="/Rekon/rekonOrderKproDetail?view=ORDER&witel='.$value->witel.'&area='.$area.'&layanan=wifiext&startDate='.$startDate.'&endDate='.$endDate.'&nper=null" target="_blank" style="color: #6c757d;">'.str_replace(',', '.', number_format($value->wifiext ? : '0')).'</a>',
            str_replace(',', '.', number_format(($value->wifiext * $hss_witel->wifiext) ? : '0')),
            '<a href="/Rekon/rekonOrderKproDetail?view=ORDER&witel='.$value->witel.'&area='.$area.'&layanan=meshwifi&startDate='.$startDate.'&endDate='.$endDate.'&nper=null" target="_blank" style="color: #6c757d;">'.str_replace(',', '.', number_format($value->meshwifi ? : '0')).'</a>',
            str_replace(',', '.', number_format(($value->meshwifi * $hss_witel->wifiext) ? : '0')),
            '<a href="/Rekon/rekonOrderKproDetail?view=ORDER&witel='.$value->witel.'&area='.$area.'&layanan=plc&startDate='.$startDate.'&endDate='.$endDate.'&nper=null" target="_blank" style="color: #6c757d;">'.str_replace(',', '.', number_format($value->plc ? : '0')).'</a>',
            str_replace(',', '.', number_format(($value->plc * $hss_witel->plc) ? : '0')),
            '<a href="/Rekon/rekonOrderKproDetail?view=ORDER&witel='.$value->witel.'&area='.$area.'&layanan=indihome_smart&startDate='.$startDate.'&endDate='.$endDate.'&nper=null" target="_blank" style="color: #6c757d;">'.str_replace(',', '.', number_format($value->indihome_smart ? : '0')).'</a>',
            str_replace(',', '.', number_format(($value->indihome_smart * $hss_witel->smartcam_only) ? : '0')),
            $tbody_tiang,
            $tbody_tiang_jasa,
            str_replace(',', '.', number_format($grand_total ? : '0')),
            str_replace(',', '.', number_format(($value->p1 * $hss_witel->p1_ku) +  ($value->p2_tlpint * $hss_witel->p2_inet_voice_ku) + ($value->p2_intiptv * $hss_witel->p2_inet_iptv_ku) + ($value->p3 * $hss_witel->p3_ku) + ($value->p1_survey * $hss_witel->p1) + ($value->p2_tlpint_survey * $hss_witel->p2_inet_voice) + ($value->p2_intiptv_survey * $hss_witel->p2_inet_iptv) + ($value->p3_survey * $hss_witel->p3) + (($value->wms * $hss_witel->lme_wifi) + ($value->wms * $hss_witel->lme_ap_indoor)) + ($value->wms_lite * $hss_witel->lme_wifi) + ($value->addon_stb * $hss_witel->stb_tambahan) + ($value->ont_premium * $hss_witel->ont_premium) + ($value->wifiext * $hss_witel->wifiext) + ($value->meshwifi * $hss_witel->wifiext) + ($value->plc * $hss_witel->plc) + ($value->indihome_smart * $hss_witel->smartcam_only) + $jml_tiang_jasa ? : '0'))
          ];

          $result['footer'] = [
            '<a href="/Rekon/rekonOrderKproDetail?view=ORDER&witel='.$value->witel.'&area=ALL&layanan=p1&startDate='.$startDate.'&endDate='.$endDate.'&nper=null" target="_blank" style="color: #6c757d;">'.str_replace(',', '.', number_format($total_p1 ? : '0')).'</a>',
            str_replace(',', '.', number_format(($total_p1 * $hss_witel->p1_ku) ? : '0')),
            '<a href="/Rekon/rekonOrderKproDetail?view=ORDER&witel='.$value->witel.'&area=ALL&layanan=p2_tlpint&startDate='.$startDate.'&endDate='.$endDate.'&nper=null" target="_blank" style="color: #6c757d;">'.str_replace(',', '.', number_format($total_p2_tlpint ? : '0')).'</a>',
            str_replace(',', '.', number_format(($total_p2_tlpint * $hss_witel->p2_inet_voice_ku) ? : '0')),
            '<a href="/Rekon/rekonOrderKproDetail?view=ORDER&witel='.$value->witel.'&area=ALL&layanan=p2_intiptv&startDate='.$startDate.'&endDate='.$endDate.'&nper=null" target="_blank" style="color: #6c757d;">'.str_replace(',', '.', number_format($total_p2_intiptv ? : '0')).'</a>',
            str_replace(',', '.', number_format(($total_p2_intiptv * $hss_witel->p2_inet_iptv_ku) ? : '0')),
            '<a href="/Rekon/rekonOrderKproDetail?view=ORDER&witel='.$value->witel.'&area=ALL&layanan=p3&startDate='.$startDate.'&endDate='.$endDate.'&nper=null" target="_blank" style="color: #6c757d;">'.str_replace(',', '.', number_format($total_p3 ? : '0')).'</a>',
            str_replace(',', '.', number_format(($total_p3 * $hss_witel->p3_ku) ? : '0')),
            '<a href="/Rekon/rekonOrderKproDetail?view=ORDER&witel='.$value->witel.'&area=ALL&layanan=p1_survey&startDate='.$startDate.'&endDate='.$endDate.'&nper=null" target="_blank" style="color: #6c757d;">'.str_replace(',', '.', number_format($total_p1_survey ? : '0')).'</a>',
            str_replace(',', '.', number_format(($total_p1_survey * $hss_witel->p1) ? : '0')),
            '<a href="/Rekon/rekonOrderKproDetail?view=ORDER&witel='.$value->witel.'&area=ALL&layanan=p2_tlpint_survey&startDate='.$startDate.'&endDate='.$endDate.'&nper=null" target="_blank" style="color: #6c757d;">'.str_replace(',', '.', number_format($total_p2_tlpint_survey ? : '0')).'</a>',
            str_replace(',', '.', number_format(($total_p2_tlpint_survey * $hss_witel->p2_inet_voice) ? : '0')),
            '<a href="/Rekon/rekonOrderKproDetail?view=ORDER&witel='.$value->witel.'&area=ALL&layanan=p2_intiptv_survey&startDate='.$startDate.'&endDate='.$endDate.'&nper=null" target="_blank" style="color: #6c757d;">'.str_replace(',', '.', number_format($total_p2_intiptv_survey ? : '0')).'</a>',
            str_replace(',', '.', number_format(($total_p2_intiptv_survey * $hss_witel->p2_inet_iptv) ? : '0')),
            '<a href="/Rekon/rekonOrderKproDetail?view=ORDER&witel='.$value->witel.'&area=ALL&layanan=p3_survey&startDate='.$startDate.'&endDate='.$endDate.'&nper=null" target="_blank" style="color: #6c757d;">'.str_replace(',', '.', number_format($total_p3_survey ? : '0')).'</a>',
            str_replace(',', '.', number_format(($total_p3_survey * $hss_witel->p3) ? : '0')),
            '<a href="/Rekon/rekonOrderKproDetail?view=ORDER&witel='.$value->witel.'&area=ALL&layanan=wms&startDate='.$startDate.'&endDate='.$endDate.'&nper=null" target="_blank" style="color: #6c757d;">'.str_replace(',', '.', number_format($total_wms ? : '0')).'</a>',
            str_replace(',', '.', number_format((($total_wms * $hss_witel->lme_wifi) + ($value->wms * $hss_witel->lme_ap_indoor)) ? : '0')),
            '<a href="/Rekon/rekonOrderKproDetail?view=ORDER&witel='.$value->witel.'&area=ALL&layanan=wms_lite&startDate='.$startDate.'&endDate='.$endDate.'&nper=null" target="_blank" style="color: #6c757d;">'.str_replace(',', '.', number_format($total_wms_lite ? : '0')).'</a>',
            str_replace(',', '.', number_format(($total_wms_lite * $hss_witel->lme_wifi) ? : '0')),
            '<a href="/Rekon/rekonOrderKproDetail?view=ORDER&witel='.$value->witel.'&area=ALL&layanan=addon_stb&startDate='.$startDate.'&endDate='.$endDate.'&nper=null" target="_blank" style="color: #6c757d;">'.str_replace(',', '.', number_format($total_addon_stb ? : '0')).'</a>',
            str_replace(',', '.', number_format(($total_addon_stb * $hss_witel->stb_tambahan) ? : '0')),
            '<a href="/Rekon/rekonOrderKproDetail?view=ORDER&witel='.$value->witel.'&area=ALL&layanan=second_stb&startDate='.$startDate.'&endDate='.$endDate.'&nper=null" target="_blank" style="color: #6c757d;">'.str_replace(',', '.', number_format($total_second_stb ? : '0')).'</a>',
            str_replace(',', '.', number_format(($total_second_stb * $hss_witel->stb_tambahan) ? : '0')),
            '<a href="/Rekon/rekonOrderKproDetail?view=ORDER&witel='.$value->witel.'&area=ALL&layanan=ont_premium&startDate='.$startDate.'&endDate='.$endDate.'&nper=null" target="_blank" style="color: #6c757d;">'.str_replace(',', '.', number_format($total_ont_premium ? : '0')).'</a>',
            str_replace(',', '.', number_format(($total_ont_premium * $hss_witel->ont_premium) ? : '0')),
              $tfoot_ontpremium,
              $tfoot_migrasistb,
            '<a href="/Rekon/rekonOrderKproDetail?view=ORDER&witel='.$value->witel.'&area=ALL&layanan=wifiext&startDate='.$startDate.'&endDate='.$endDate.'&nper=null" target="_blank" style="color: #6c757d;">'.str_replace(',', '.', number_format($total_wifiext ? : '0')).'</a>',
            str_replace(',', '.', number_format(($total_wifiext * $hss_witel->wifiext) ? : '0')),
            '<a href="/Rekon/rekonOrderKproDetail?view=ORDER&witel='.$value->witel.'&area=ALL&layanan=meshwifi&startDate='.$startDate.'&endDate='.$endDate.'&nper=null" target="_blank" style="color: #6c757d;">'.str_replace(',', '.', number_format($total_meshwifi ? : '0')).'</a>',
            str_replace(',', '.', number_format(($total_meshwifi * $hss_witel->wifiext) ? : '0')),
            '<a href="/Rekon/rekonOrderKproDetail?view=ORDER&witel='.$value->witel.'&area=ALL&layanan=plc&startDate='.$startDate.'&endDate='.$endDate.'&nper=null" target="_blank" style="color: #6c757d;">'.str_replace(',', '.', number_format($total_plc ? : '0')).'</a>',
            str_replace(',', '.', number_format(($total_plc * $hss_witel->plc) ? : '0')),
            '<a href="/Rekon/rekonOrderKproDetail?view=ORDER&witel='.$value->witel.'&area=ALL&layanan=indihome_smart&startDate='.$startDate.'&endDate='.$endDate.'&nper=null" target="_blank" style="color: #6c757d;">'.str_replace(',', '.', number_format($total_indihome_smart ? : '0')).'</a>',
            str_replace(',', '.', number_format(($total_indihome_smart * $hss_witel->smartcam_only) ? : '0')),
              $tfoot_tiang,
              $tfoot_tiang_jasa,
              str_replace(',', '.', number_format($grand_total_plus ? : '0')),
              str_replace(',', '.', number_format(($total_p1 * $hss_witel->p1_ku) + ($total_p2_tlpint * $hss_witel->p2_inet_voice_ku) + ($total_p2_intiptv * $hss_witel->p2_inet_iptv_ku) + ($total_p3 * $hss_witel->p3_ku) + ($total_p1_survey * $hss_witel->p1) + ($total_p2_tlpint_survey * $hss_witel->p2_inet_voice) + ($total_p2_intiptv_survey * $hss_witel->p2_inet_iptv) + ($total_p3_survey * $hss_witel->p3) + (($total_wms * $hss_witel->lme_wifi) + ($value->wms * $hss_witel->lme_ap_indoor)) + ($total_wms_lite * $hss_witel->lme_wifi) + ($total_addon_stb * $hss_witel->stb_tambahan) + ($total_second_stb * $hss_witel->stb_tambahan) + ($total_ont_premium * $hss_witel->ont_premium) + ($total_wifiext * $hss_witel->wifiext) + ($total_meshwifi * $hss_witel->wifiext) + ($total_plc * $hss_witel->plc) + ($total_indihome_smart * $hss_witel->smartcam_only) + $total_tiang_jasa? : '0'))
          ];
          break;
      }
    }
    // dd($result);

    return \Response::json($result);
  }

  public static function rekonOrderKproDetail()
  {
    $view = Input::get('view');
    $witel = Input::get('witel');
    $area = Input::get('area');
    $layanan = Input::get('layanan');
    $startDate = Input::input('startDate');
    $endDate = Input::input('endDate');
    $nper = Input::input('nper');

    return view('Rekon.rekonOrderKproDetail', compact('view', 'witel', 'area', 'layanan', 'startDate', 'endDate', 'nper'));
  }

  public static function ajx_rekonOrderKproDetail()
  {
    $view = Input::get('view');
    $witel = Input::get('witel');
    $area = Input::get('area');
    $layanan = Input::get('layanan');
    $startDate = Input::input('startDate');
    $endDate = Input::input('endDate');
    $nper = Input::input('nper');
    
    $data = RekonModel::rekonOrderKproDetail($view, $witel, $area, $layanan, $startDate, $endDate, $nper);

    $result['data'] = [];
    $numb = 1;

    foreach ($data as $key => $value) {
      $result['data'][] = [
        $numb++,
        $value->area ?? 'NON AREA',
        $value->is_layanan,
        $value->ORDER_ID,
        $value->REGIONAL,
        $value->WITEL,
        $value->DATEL,
        $value->STO,
        $value->EXTERN_ORDER_ID,
        $value->JENIS_PSB,
        $value->TYPE_TRANSAKSI,
        $value->FLAG_DEPOSIT,
        $value->STATUS_RESUME,
        $value->STATUS_MESSAGE,
        $value->KCONTACT,
        $value->ORDER_DATE,
        $value->NCLI,
        $value->NDEM,
        $value->SPEEDY,
        $value->POTS,
        $value->CUSTOMER_NAME,
        $value->NO_HP,
        $value->EMAIL,
        $value->INSTALL_ADDRESS,
        $value->CUSTOMER_ADDRESS,
        $value->CITY_NAME,
        $value->GPS_LATITUDE,
        $value->GPS_LONGITUDE,
        $value->PACKAGE_NAME,
        $value->LOC_ID,
        $value->DEVICE_ID,
        $value->AGENT_ID,
        $value->WFM_ID,
        $value->SCHEDSTART,
        $value->SCHEDFINISH,
        $value->ACTSTART,
        $value->ACTFINISH,
        $value->SCHEDULE_LABOR,
        $value->FINISH_LABOR,
        $value->LAST_UPDATED_DATE,
        $value->TYPE_LAYANAN,
        $value->ISI_COMMENT,
        $value->TINDAK_LANJUT,
        $value->USER_ID_TL,
        $value->TL_DATE,
        $value->TANGGAL_PROSES,
        $value->TANGGAL_MANJA,
        $value->HIDE,
        $value->CATEGORY,
        $value->PROVIDER,
        $value->NPER,
        $value->AMCREW,
        $value->STATUS_WO,
        $value->STATUS_TASK,
        $value->CHANNEL,
        $value->GROUP_CHANNEL,
        $value->PRODUCT,
        $value->JENIS_ORDER,
        $value->DEVICE_ID_INT,
        $value->drop_core ?? 0,
        $value->precon ?? 0,
        $value->tiang ?? 0,
        $value->klem_ring ?? 0,
        $value->otp ?? 0,
        $value->prekso ?? 0,
        $value->roset ?? 0,
        $value->s_clamp ?? 0,
        $value->soc ?? 0,
        $value->clamp_hook ?? 0,
        $value->segitiga_bracket ?? 0,
        $value->tray_cable ?? 0,
        $value->utt_statusName ?? '-',
        $value->utt_create_dtm ?? '-'
      ];
    }

    return \Response::json($result);
  }

  public function createProvOrderBased()
  {
    $get_regional = DB::table('procurement_mitra_nasional')->select('id_reg', 'reg', 'witel', 'id_mitra', 'mitra')->orderBy('reg', 'ASC')->groupBy('witel', 'id_mitra')->get();
    $bulan_format = DB::table('bulan_format')->select('id', 'bulan AS text', 'date_periode1', 'date_periode2')->get();
    $hss_witel = DB::table('procurement_hss_psb')->where('witel', session('auth')->Witel_New)->first();
    $get_layanan = DB::table('procurement_layanan_psb')->where('aktif', 1)->select('alias_layanan AS id', 'layanan AS text')->get();
    
    return view('Rekon.createProvOrderBased', compact('get_regional', 'bulan_format', 'hss_witel', 'get_layanan'));
  }

  public static function rekonOrderDnsDetail()
  {
    $witel = Input::get('witel');
    $area = Input::get('area');
    $layanan = Input::get('layanan');
    $startDate = Input::input('startDate');
    $endDate = Input::input('endDate');

    return view('Rekon.rekonOrderDnsDetail', compact('witel', 'area', 'layanan', 'startDate', 'endDate'));
  }

  public static function ajx_rekonOrderDnsDetail()
  {
    $witel = Input::get('witel');
    $area = Input::get('area');
    $layanan = Input::get('layanan');
    $startDate = Input::input('startDate');
    $endDate = Input::input('endDate');

    $result['data'] = [];
    $numb = 1;
    
    $data = RekonModel::rekonOrderDnsDetail($witel, $area, $layanan, $startDate, $endDate);

    switch ($layanan) {
      case 'ont_premium_dns':
        foreach ($data as $key => $value) {
          $result['data'][] = [
            $numb++,
            $value->mitra ?? 'NON AREA',
            $value->wo_id,
            $value->nama_pelanggan,
            $value->alamat_pelanggan,
            $value->no_telp,
            $value->no_internet,
            $value->ont_type_eksisting,
            $value->sn_ont_eksisting,
            $value->ont_type_baru,
            $value->sn_ont_baru,
            $value->type,
            $value->helpdesk_approve,
            $value->amcrew,
            $value->nik_teknisi_1,
            $value->nik_teknisi_2,
            $value->jenis_dapros,
            $value->tanggal_usage,
            $value->nik,
            $value->nama,
            $value->tim,
            $value->sektor
          ];
        }
        break;
      
      case 'migrasi_stb_dns':
        foreach ($data as $key => $value) {
          $result['data'][] = [
            $numb++,
            $value->mitra ?? 'NON AREA',
            $value->wo_id,
            $value->sto,
            $value->no_internet,
            $value->nd_pots,
            $value->mac_address,
            $value->nama_pelanggan,
            $value->alamat_pelanggan,
            $value->no_hp_pelanggan,
            $value->helpdesk_assign,
            $value->amcrew,
            $value->nik_teknisi_1,
            $value->nik_teknisi_2,
            $value->merk,
            $value->stb_id,
            $value->mac_address_baru,
            $value->tanggal_assign,
            $value->tanggal_visited,
            $value->tanggal_usage,
            $value->nik,
            $value->nama,
            $value->tim,
            $value->sektor
          ];
        }
        break;
    }

    return \Response::json($result);
  }

  public function provCreateOrderBased()
  {
    $bulan_format = DB::table('bulan_format')->select('id', 'bulan AS text', 'date_periode1', 'date_periode2')->get();
    $get_regional = DB::table('procurement_mitra_nasional')->select('id_reg', 'reg', 'witel', 'id_mitra', 'mitra')->orderBy('reg', 'ASC')->groupBy('witel', 'id_mitra')->get();
    $is_response  = false;

    return view('Rekon.provisioning.create_order_based', compact('bulan_format', 'get_regional', 'is_response'));
  }

  public function provCreateOrderBasedPost(Request $req)
  {
    $bulan_format     = DB::table('bulan_format')->select('id', 'bulan AS text', 'date_periode1', 'date_periode2')->get();
    $get_regional     = DB::table('procurement_mitra_nasional')->select('id_reg', 'reg', 'witel', 'id_mitra', 'mitra')->orderBy('reg', 'ASC')->groupBy('witel', 'id_mitra')->get();
    
    $regional         = str_replace('REG','', $req->input('regional'));
    $witel            = $req->input('witel');
    $id_mitra         = $req->input('id_mitra');
    $periode_kegiatan = $req->input('periode_kegiatan');
    $rekon_periode1   = $req->input('rekon_periode1');
    $rekon_periode2   = $req->input('rekon_periode2');

    $mitra            = DB::table('procurement_mitra')->where([ ['id_regional', $regional], ['tacticalpro_mitra_id', $id_mitra] ])->first();
    $hss_witel        = DB::table('procurement_hss_psb')->where('witel_ms2n', $witel)->first();
    $fix_rekon        = RekonModel::provRekonCutoffOrderBased($regional, $witel, $mitra->id, date('Y'), $periode_kegiatan);

    $is_response      = true;

    return view('Rekon.provisioning.create_order_based', compact('bulan_format', 'get_regional', 'regional', 'witel', 'id_mitra', 'periode_kegiatan', 'rekon_periode1', 'rekon_periode2', 'mitra', 'hss_witel', 'fix_rekon', 'is_response'));
  }

  public function provCreateOrderBasedSave(Request $req)
  {
    $startDate = $req->input('rekon_periode1');
    $endDate   = $req->input('rekon_periode2');

    $id        = RekonModel::provCreateOrderBasedSave($req);

    $that      = new \App\Http\Controllers\AdminController();
    $that->download_zip_psb('beta_kalimantan', $id);

    return redirect('/Rekon/provisioning/dashboard/order?startDate='.$startDate.'&endDate='.$endDate)->with('alerts', [
      ['type' => 'success', 'text' => 'Success Generate Document (Rekon Order Based)!']
    ]);
  }

  public function provCutoffOrderBased()
  {
    $get_regional     = DB::table('procurement_mitra_nasional')->select('id_reg', 'reg', 'witel', 'id_mitra', 'mitra')->orderBy('reg', 'ASC')->groupBy('witel', 'id_mitra')->get();
    $bulan_format     = DB::table('bulan_format')->select('id', 'bulan AS text', 'date_periode1', 'date_periode2')->get();
    $response_rekon[] = [];

    return view('Rekon.provisioning.cutoff_order_based', compact('get_regional', 'bulan_format', 'response_rekon'));
  }

  public function provCutoffOrderBasedSave(Request $req)
  {
    $get_regional   = DB::table('procurement_mitra_nasional')->select('id_reg', 'reg', 'witel', 'id_mitra', 'mitra')->orderBy('reg', 'ASC')->groupBy('witel', 'id_mitra')->get();
    $bulan_format   = DB::table('bulan_format')->select('id', 'bulan AS text', 'date_periode1', 'date_periode2')->get();
    $response_rekon = RekonModel::provCutoffOrderBasedSave($req);

    return view('Rekon.provisioning.cutoff_order_based', compact('get_regional', 'bulan_format', 'response_rekon'));

    return back()->with('alerts', [
      ['type' => 'success', 'text' => 'Data Cutoff PSB Periode '.$req->input('periode1').' s/d '.$req->input('periode2').' Berhasil di Simpan']
    ]);
  }

  public function provDashboardOrder()
  {
    $startDate    = Input::get('startDate');
    $endDate      = Input::get('endDate');

    $log_kpro     = RekonModel::log_kpro();
    $log_utonline = RekonModel::log_utonline();

    $get_regional = DB::table('procurement_mitra_nasional')->select('id_reg', 'reg', 'witel', 'id_mitra', 'mitra')->orderBy('reg', 'ASC')->groupBy('witel', 'id_mitra')->get();
    $bulan_format = DB::table('bulan_format')->select('id', 'bulan AS text', 'date_periode1', 'date_periode2')->get();

    return view('Rekon.provisioning.dashboard_order', compact('startDate', 'endDate', 'log_kpro', 'log_utonline', 'get_regional', 'bulan_format'));
  }

  public function ajx_provDashboardOrder()
  {
    $view      = Input::get('view');
    $startDate = Input::get('startDate');
    $endDate   = Input::get('endDate');

    $data = RekonModel::provDashboardOrder($view, $startDate, $endDate);

    switch ($view)
    {
      case 'MONTH':

            $jml_januari = $jml_februari = $jml_maret = $jml_april = $jml_mei = $jml_juni = $jml_juli = $jml_agustus = $jml_september = $jml_oktober = $jml_november = $jml_desember = $jml_all = $total = 0;

            foreach ($data as $key => $value)
            {
              $jml_januari   += $value->januari;
              $jml_februari  += $value->februari;
              $jml_maret     += $value->maret;
              $jml_april     += $value->april;
              $jml_mei       += $value->mei;
              $jml_juni      += $value->juni;
              $jml_juli      += $value->juli;
              $jml_agustus   += $value->agustus;
              $jml_september += $value->september;
              $jml_oktober   += $value->oktober;
              $jml_november  += $value->november;
              $jml_desember  += $value->desember;
              $jml_all       = ($value->januari + $value->februari + $value->maret + $value->april + $value->mei + $value->juni + $value->juli + $value->agustus + $value->september + $value->oktober + $value->november + $value->desember);
              $total         += $jml_all;

              $result['data'][$value->witel][] = $value->reg;
              $result['data'][$value->witel][] = $value->witel;

              $months = ['januari' => '01', 'februari' => '02', 'maret' => '03', 'april' => '04', 'mei' => '05', 'juni' => '06', 'juli' => '07', 'agustus' => '08', 'september' => '09', 'oktober' => '10', 'november' => '11', 'desember' => '12'];

              foreach ($months as $key => $value_m)
              {
                $nper = date('Y').''.$value_m;
                $result['data'][$value->witel][] = '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=MONTH&witel='.$value->witel.'&month='.$nper.'">'.str_replace(',', '.', number_format($value->$key)).'</a>';
              }

              $result['data'][$value->witel][] = '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=MONTH&witel='.$value->witel.'&month=ALL">'.str_replace(',', '.', number_format($jml_all)).'</a>';

              $result['data'] = array_values($result['data']);
            }

            $result['footer'] = [
              '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=MONTH&witel=ALL&month='.date('Y').'01">'.str_replace(',', '.', number_format($jml_januari)).'</a>',
              '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=MONTH&witel=ALL&month='.date('Y').'02">'.str_replace(',', '.', number_format($jml_februari)).'</a>',
              '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=MONTH&witel=ALL&month='.date('Y').'03">'.str_replace(',', '.', number_format($jml_maret)).'</a>',
              '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=MONTH&witel=ALL&month='.date('Y').'04">'.str_replace(',', '.', number_format($jml_april)).'</a>',
              '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=MONTH&witel=ALL&month='.date('Y').'05">'.str_replace(',', '.', number_format($jml_mei)).'</a>',
              '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=MONTH&witel=ALL&month='.date('Y').'06">'.str_replace(',', '.', number_format($jml_juni)).'</a>',
              '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=MONTH&witel=ALL&month='.date('Y').'07">'.str_replace(',', '.', number_format($jml_juli)).'</a>',
              '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=MONTH&witel=ALL&month='.date('Y').'08">'.str_replace(',', '.', number_format($jml_agustus)).'</a>',
              '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=MONTH&witel=ALL&month='.date('Y').'09">'.str_replace(',', '.', number_format($jml_september)).'</a>',
              '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=MONTH&witel=ALL&month='.date('Y').'10">'.str_replace(',', '.', number_format($jml_oktober)).'</a>',
              '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=MONTH&witel=ALL&month='.date('Y').'11">'.str_replace(',', '.', number_format($jml_november)).'</a>',
              '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=MONTH&witel=ALL&month='.date('Y').'12">'.str_replace(',', '.', number_format($jml_desember)).'</a>',
              str_replace(',', '.', number_format($total))
            ];

        break;

      case 'DAY':

            foreach ($data as $key => $value)
            {
              $result['data'][$value->witel][] = $value->reg;
              $result['data'][$value->witel][] = $value->witel;

              for ($i = 1; $i < date('t') + 1; $i ++)
              { 
                if ($i < 10)
                {
                  $keys = '0'.$i;
                  $days = date('Y-m-').$keys;
                }
                else
                {
                  $keys = $i;
                  $days = date('Y-m-').$keys;
                }
                $txt_ps_kpro = 'ps_kpro_d'.$keys;
                $result['data'][$value->witel][] = '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=DAY&witel='.$value->witel.'&day='.$days.'">'.str_replace(',', '.', number_format($value->$txt_ps_kpro)).'</a>';
              }

              $result['data'] = array_values($result['data']);
            }

        break;
        
      case 'ORDER':

            $jml_p1 = $jasa_p1 = $jml_jasa_p1 = $jml_p2_tlpint = $jasa_p2_tlpint = $jml_jasa_p2_tlpint = $jml_p2_intiptv = $jasa_p2_intiptv = $jml_jasa_p2_intiptv = $jml_p3 = $jasa_p3 = $jml_jasa_p3 = $jml_p1_survey = $jasa_p1_survey = $jml_jasa_p1_survey = $jml_p2_tlpint_survey = $jasa_p2_tlpint_survey = $jml_jasa_p2_tlpint_survey = $jml_p2_intiptv_survey = $jasa_p2_intiptv_survey = $jml_jasa_p2_intiptv_survey = $jml_p3_survey = $jasa_p3_survey = $jml_jasa_p3_survey = $jml_wms = $jasa_wms = $jml_jasa_wms = $jml_wms_lite = $jasa_wms_lite = $jml_jasa_wms_lite = $jml_addon_stb = $jasa_addon_stb = $jml_jasa_addon_stb = $jml_second_stb = $jasa_second_stb = $jml_jasa_second_stb = $jml_change_stb = $jasa_change_stb = $jml_jasa_change_stb = $jml_ont_premium = $jasa_ont_premium = $jml_jasa_ont_premium = $jml_wifiext = $jasa_wifiext = $jml_jasa_wifiext = $jml_meshwifi = $jasa_meshwifi = $jml_jasa_meshwifi = $jml_plc = $jasa_plc = $jml_jasa_plc = $jml_indihome_smart = $jasa_indihome_smart = $jml_jasa_indihome_smart = $jml_ssl = $jasa_jml = $total_ssl = $total_jasa = 0;

            foreach ($data as $key => $value)
            {
              $jml_p1                     += $value->p1;
              $jasa_p1                    = ($value->p1 * $value->hss_p1);
              $jml_jasa_p1                += ($value->p1 * $value->hss_p1);

              $jml_p2_tlpint              += $value->p2_tlpint;
              $jasa_p2_tlpint             = ($value->p2_tlpint * $value->hss_p2_tlpint);
              $jml_jasa_p2_tlpint         += ($value->p2_tlpint * $value->hss_p2_tlpint);

              $jml_p2_intiptv             += $value->p2_intiptv;
              $jasa_p2_intiptv            = ($value->p2_intiptv * $value->hss_p2_intiptv);
              $jml_jasa_p2_intiptv        += ($value->p2_intiptv * $value->hss_p2_intiptv);

              $jml_p3                     += $value->p3;
              $jasa_p3                    = ($value->p3 * $value->hss_p3);
              $jml_jasa_p3                += ($value->p3 * $value->hss_p3);

              $jml_p1_survey              += $value->p1_survey;
              $jasa_p1_survey             = ($value->p1_survey * $value->hss_p1_survey);
              $jml_jasa_p1_survey         += ($value->p1_survey * $value->hss_p1_survey);

              $jml_p2_tlpint_survey       += $value->p2_tlpint_survey;
              $jasa_p2_tlpint_survey      = ($value->p2_tlpint_survey * $value->hss_p2_tlpint_survey);
              $jml_jasa_p2_tlpint_survey  += ($value->p2_tlpint_survey * $value->hss_p2_tlpint_survey);

              $jml_p2_intiptv_survey      += $value->p2_intiptv_survey;
              $jasa_p2_intiptv_survey     = ($value->p2_intiptv_survey * $value->hss_p2_intiptv_survey);
              $jml_jasa_p2_intiptv_survey += ($value->p2_intiptv_survey * $value->hss_p2_intiptv_survey);

              $jml_p3_survey              += $value->p3_survey;
              $jasa_p3_survey             = ($value->p3_survey * $value->hss_p3_survey);
              $jml_jasa_p3_survey         += ($value->p3_survey * $value->hss_p3_survey);

              $jml_wms                    += $value->wms;
              $jasa_wms                   = (($value->wms * $value->hss_lme_wifi) + ($value->wms * $value->hss_lme_ap_indoor));
              $jml_jasa_wms               += (($value->wms * $value->hss_lme_wifi) + ($value->wms * $value->hss_lme_ap_indoor));

              $jml_wms_lite               += $value->wms_lite;
              $jasa_wms_lite              = ($value->wms_lite * $value->hss_lme_wifi);
              $jml_jasa_wms_lite          += ($value->wms_lite * $value->hss_lme_wifi);

              $jml_addon_stb              += $value->addon_stb;
              $jasa_addon_stb             = ($value->addon_stb * $value->hss_stb_tambahan);
              $jml_jasa_addon_stb         += ($value->addon_stb * $value->hss_stb_tambahan);

              $jml_second_stb             += $value->second_stb;
              $jasa_second_stb            = ($value->second_stb * $value->hss_stb_tambahan);
              $jml_jasa_second_stb        += ($value->second_stb * $value->hss_stb_tambahan);

              $jml_change_stb             += $value->change_stb;
              $jasa_change_stb            = ($value->change_stb * $value->hss_change_stb);
              $jml_jasa_change_stb        += ($value->change_stb * $value->hss_change_stb);

              $jml_ont_premium            += $value->ont_premium;
              $jasa_ont_premium           = ($value->ont_premium * $value->hss_ont_premium);
              $jml_jasa_ont_premium       += ($value->ont_premium * $value->hss_ont_premium);

              $jml_wifiext                += $value->wifiext;
              $jasa_wifiext               = ($value->wifiext * $value->hss_wifiext);
              $jml_jasa_wifiext           += ($value->wifiext * $value->hss_wifiext);

              $jml_meshwifi               += $value->meshwifi;
              $jasa_meshwifi              = ($value->meshwifi * $value->hss_wifiext);
              $jml_jasa_meshwifi          += ($value->meshwifi * $value->hss_wifiext);

              $jml_plc                    += $value->plc;
              $jasa_plc                   = ($value->plc * $value->hss_plc);
              $jml_jasa_plc               += ($value->plc * $value->hss_plc);

              $jml_indihome_smart         += $value->indihome_smart;
              $jasa_indihome_smart        = ($value->indihome_smart * $value->hss_indihome_smart);
              $jml_jasa_indihome_smart    += ($value->indihome_smart * $value->hss_indihome_smart);

              $jml_ssl                    = ($value->p1 + $value->p2_tlpint + $value->p2_intiptv + $value->p3 + $value->p1_survey + $value->p2_tlpint_survey + $value->p2_intiptv_survey + $value->p3_survey + $value->wms + $value->wms_lite + $value->addon_stb + $value->second_stb + $value->change_stb + $value->ont_premium + $value->wifiext + $value->meshwifi + $value->plc + $value->indihome_smart);
              $jasa_jml                   = ($jasa_p1 + $jasa_p2_tlpint + $jasa_p2_intiptv + $jasa_p3 + $jasa_p1_survey + $jasa_p2_tlpint_survey + $jasa_p2_intiptv_survey + $jasa_p3_survey + $jasa_wms + $jasa_wms_lite + $jasa_addon_stb + $jasa_second_stb + $jasa_change_stb + $jasa_ont_premium + $jasa_wifiext + $jasa_meshwifi + $jasa_plc + $jasa_indihome_smart);
              $total_ssl                  += $jml_ssl;
              $total_jasa                 += $jasa_jml;

              $result['data'][] = [
                $value->reg,
                $value->witel,

                '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=ORDER&witel='.$value->witel.'&startDate='.$startDate.'&endDate='.$endDate.'&layanan=p1">'.str_replace(',', '.', number_format($value->p1 ? : '0')).'</a>',
                str_replace(',', '.', number_format($jasa_p1 ? : '0')),

                '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=ORDER&witel='.$value->witel.'&startDate='.$startDate.'&endDate='.$endDate.'&layanan=p2_tlpint">'.str_replace(',', '.', number_format($value->p2_tlpint ? : '0')).'</a>',
                str_replace(',', '.', number_format($jasa_p2_tlpint ? : '0')),

                '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=ORDER&witel='.$value->witel.'&startDate='.$startDate.'&endDate='.$endDate.'&layanan=p2_intiptv">'.str_replace(',', '.', number_format($value->p2_intiptv ? : '0')).'</a>',
                str_replace(',', '.', number_format($jasa_p2_intiptv ? : '0')),

                '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=ORDER&witel='.$value->witel.'&startDate='.$startDate.'&endDate='.$endDate.'&layanan=p3">'.str_replace(',', '.', number_format($value->p3 ? : '0')),
                str_replace(',', '.', number_format($jasa_p3 ? : '0')),

                '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=ORDER&witel='.$value->witel.'&startDate='.$startDate.'&endDate='.$endDate.'&layanan=p1_survey">'.str_replace(',', '.', number_format($value->p1_survey ? : '0')).'</a>',
                str_replace(',', '.', number_format($jasa_p1_survey ? : '0')),

                '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=ORDER&witel='.$value->witel.'&startDate='.$startDate.'&endDate='.$endDate.'&layanan=p2_tlpint_survey">'.str_replace(',', '.', number_format($value->p2_tlpint_survey ? : '0')).'</a>',
                str_replace(',', '.', number_format($jasa_p2_tlpint_survey ? : '0')),

                '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=ORDER&witel='.$value->witel.'&startDate='.$startDate.'&endDate='.$endDate.'&layanan=p2_intiptv_survey">'.str_replace(',', '.', number_format($value->p2_intiptv_survey ? : '0')).'</a>',
                str_replace(',', '.', number_format($jasa_p2_intiptv_survey ? : '0')),

                '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=ORDER&witel='.$value->witel.'&startDate='.$startDate.'&endDate='.$endDate.'&layanan=p3_survey">'.str_replace(',', '.', number_format($value->p3_survey ? : '0')).'</a>',
                str_replace(',', '.', number_format($jasa_p3_survey ? : '0')),

                '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=ORDER&witel='.$value->witel.'&startDate='.$startDate.'&endDate='.$endDate.'&layanan=wms">'.str_replace(',', '.', number_format($value->wms ? : '0')).'</a>',
                str_replace(',', '.', number_format($jasa_wms ? : '0')),

                '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=ORDER&witel='.$value->witel.'&startDate='.$startDate.'&endDate='.$endDate.'&layanan=wms_lite">'.str_replace(',', '.', number_format($value->wms_lite ? : '0')).'</a>',
                str_replace(',', '.', number_format($jasa_wms_lite ? : '0')),

                '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=ORDER&witel='.$value->witel.'&startDate='.$startDate.'&endDate='.$endDate.'&layanan=addon_stb">'.str_replace(',', '.', number_format($value->addon_stb ? : '0')).'</a>',
                str_replace(',', '.', number_format($jasa_addon_stb ? : '0')),

                '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=ORDER&witel='.$value->witel.'&startDate='.$startDate.'&endDate='.$endDate.'&layanan=second_stb">'.str_replace(',', '.', number_format($value->second_stb ? : '0')).'</a>',
                str_replace(',', '.', number_format($jasa_second_stb ? : '0')),

                '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=ORDER&witel='.$value->witel.'&startDate='.$startDate.'&endDate='.$endDate.'&layanan=change_stb">'.str_replace(',', '.', number_format($value->change_stb ? : '0')).'</a>',
                str_replace(',', '.', number_format($jasa_change_stb ? : '0')),

                '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=ORDER&witel='.$value->witel.'&startDate='.$startDate.'&endDate='.$endDate.'&layanan=ont_premium">'.str_replace(',', '.', number_format($value->ont_premium ? : '0')).'</a>',
                str_replace(',', '.', number_format($jasa_ont_premium ? : '0')),

                '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=ORDER&witel='.$value->witel.'&startDate='.$startDate.'&endDate='.$endDate.'&layanan=wifiext">'.str_replace(',', '.', number_format($value->wifiext ? : '0')).'</a>',
                str_replace(',', '.', number_format($jasa_wifiext ? : '0')),

                '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=ORDER&witel='.$value->witel.'&startDate='.$startDate.'&endDate='.$endDate.'&layanan=meshwifi">'.str_replace(',', '.', number_format($value->meshwifi ? : '0')).'</a>',
                str_replace(',', '.', number_format($jasa_meshwifi ? : '0')),

                '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=ORDER&witel='.$value->witel.'&startDate='.$startDate.'&endDate='.$endDate.'&layanan=plc">'.str_replace(',', '.', number_format($value->plc ? : '0')).'</a>',
                str_replace(',', '.', number_format($jasa_plc ? : '0')),

                '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=ORDER&witel='.$value->witel.'&startDate='.$startDate.'&endDate='.$endDate.'&layanan=indihome_smart">'.str_replace(',', '.', number_format($value->indihome_smart ? : '0')).'</a>',
                str_replace(',', '.', number_format($jasa_indihome_smart ? : '0')),

                str_replace(',', '.', number_format($jml_ssl ? : '0')),
                str_replace(',', '.', number_format($jasa_jml ? : '0'))
              ];
            }

            $result['footer'] = [
              '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=ORDER&witel=ALL&startDate='.$startDate.'&endDate='.$endDate.'&layanan=p1">'.str_replace(',', '.', number_format($jml_p1 ? : '0')).'</a>',
              str_replace(',', '.', number_format($jml_jasa_p1 ? : '0')),

              '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=ORDER&witel=ALL&startDate='.$startDate.'&endDate='.$endDate.'&layanan=p2_tlpint">'.str_replace(',', '.', number_format($jml_p2_tlpint ? : '0')).'</a>',
              str_replace(',', '.', number_format($jml_jasa_p2_tlpint ? : '0')),

              '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=ORDER&witel=ALL&startDate='.$startDate.'&endDate='.$endDate.'&layanan=p2_intiptv">'.str_replace(',', '.', number_format($jml_p2_intiptv ? : '0')).'</a>',
              str_replace(',', '.', number_format($jml_jasa_p2_intiptv ? : '0')),

              '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=ORDER&witel=ALL&startDate='.$startDate.'&endDate='.$endDate.'&layanan=p3">'.str_replace(',', '.', number_format($jml_p3 ? : '0')).'</a>',
              str_replace(',', '.', number_format($jml_jasa_p3 ? : '0')),

              '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=ORDER&witel=ALL&startDate='.$startDate.'&endDate='.$endDate.'&layanan=p1_survey">'.str_replace(',', '.', number_format($jml_p1_survey ? : '0')).'</a>',
              str_replace(',', '.', number_format($jml_jasa_p1_survey ? : '0')),

              '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=ORDER&witel=ALL&startDate='.$startDate.'&endDate='.$endDate.'&layanan=p2_tlpint_survey">'.str_replace(',', '.', number_format($jml_p2_tlpint_survey ? : '0')).'</a>',
              str_replace(',', '.', number_format($jml_jasa_p2_tlpint_survey ? : '0')),

              '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=ORDER&witel=ALL&startDate='.$startDate.'&endDate='.$endDate.'&layanan=p2_intiptv_survey">'.str_replace(',', '.', number_format($jml_p2_intiptv_survey ? : '0')).'</a>',
              str_replace(',', '.', number_format($jml_jasa_p2_intiptv_survey ? : '0')),

              '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=ORDER&witel=ALL&startDate='.$startDate.'&endDate='.$endDate.'&layanan=p3_survey">'.str_replace(',', '.', number_format($jml_p3_survey ? : '0')).'</a>',
              str_replace(',', '.', number_format($jml_jasa_p3_survey ? : '0')),

              '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=ORDER&witel=ALL&startDate='.$startDate.'&endDate='.$endDate.'&layanan=wms">'.str_replace(',', '.', number_format($jml_wms ? : '0')).'</a>',
              str_replace(',', '.', number_format($jml_jasa_wms ? : '0')),

              '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=ORDER&witel=ALL&startDate='.$startDate.'&endDate='.$endDate.'&layanan=wms_lite">'.str_replace(',', '.', number_format($jml_wms_lite ? : '0')).'</a>',
              str_replace(',', '.', number_format($jml_jasa_wms_lite ? : '0')),

              '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=ORDER&witel=ALL&startDate='.$startDate.'&endDate='.$endDate.'&layanan=addon_stb">'.str_replace(',', '.', number_format($jml_addon_stb ? : '0')).'</a>',
              str_replace(',', '.', number_format($jml_jasa_addon_stb ? : '0')),

              '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=ORDER&witel=ALL&startDate='.$startDate.'&endDate='.$endDate.'&layanan=second_stb">'.str_replace(',', '.', number_format($jml_second_stb ? : '0')).'</a>',
              str_replace(',', '.', number_format($jml_jasa_second_stb ? : '0')),

              '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=ORDER&witel=ALL&startDate='.$startDate.'&endDate='.$endDate.'&layanan=change_stb">'.str_replace(',', '.', number_format($jml_change_stb ? : '0')).'</a>',
              str_replace(',', '.', number_format($jml_jasa_change_stb ? : '0')),

              '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=ORDER&witel=ALL&startDate='.$startDate.'&endDate='.$endDate.'&layanan=ont_premium">'.str_replace(',', '.', number_format($jml_ont_premium ? : '0')).'</a>',
              str_replace(',', '.', number_format($jml_jasa_ont_premium ? : '0')),

              '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=ORDER&witel=ALL&startDate='.$startDate.'&endDate='.$endDate.'&layanan=wifiext">'.str_replace(',', '.', number_format($jml_wifiext ? : '0')).'</a>',
              str_replace(',', '.', number_format($jml_jasa_wifiext ? : '0')),

              '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=ORDER&witel=ALL&startDate='.$startDate.'&endDate='.$endDate.'&layanan=meshwifi">'.str_replace(',', '.', number_format($jml_meshwifi ? : '0')).'</a>',
              str_replace(',', '.', number_format($jml_jasa_meshwifi ? : '0')),

              '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=ORDER&witel=ALL&startDate='.$startDate.'&endDate='.$endDate.'&layanan=plc">'.str_replace(',', '.', number_format($jml_plc ? : '0')).'</a>',
              str_replace(',', '.', number_format($jml_jasa_plc ? : '0')),

              '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=ORDER&witel=ALL&startDate='.$startDate.'&endDate='.$endDate.'&layanan=indihome_smart">'.str_replace(',', '.', number_format($jml_indihome_smart ? : '0')).'</a>',
              str_replace(',', '.', number_format($jml_jasa_indihome_smart ? : '0')),

              str_replace(',', '.', number_format($total_ssl ? : '0')),
              str_replace(',', '.', number_format($total_jasa ? : '0'))
            ];

        break;

      case 'ORDER_CUTOFF':

        $jml_p1 = $jasa_p1 = $jml_jasa_p1 = $jml_p2_tlpint = $jasa_p2_tlpint = $jml_jasa_p2_tlpint = $jml_p2_intiptv = $jasa_p2_intiptv = $jml_jasa_p2_intiptv = $jml_p3 = $jasa_p3 = $jml_jasa_p3 = $jml_p1_survey = $jasa_p1_survey = $jml_jasa_p1_survey = $jml_p2_tlpint_survey = $jasa_p2_tlpint_survey = $jml_jasa_p2_tlpint_survey = $jml_p2_intiptv_survey = $jasa_p2_intiptv_survey = $jml_jasa_p2_intiptv_survey = $jml_p3_survey = $jasa_p3_survey = $jml_jasa_p3_survey = $jml_wms = $jasa_wms = $jml_jasa_wms = $jml_wms_lite = $jasa_wms_lite = $jml_jasa_wms_lite = $jml_addon_stb = $jasa_addon_stb = $jml_jasa_addon_stb = $jml_second_stb = $jasa_second_stb = $jml_jasa_second_stb = $jml_change_stb = $jasa_change_stb = $jml_jasa_change_stb = $jml_ont_premium = $jasa_ont_premium = $jml_jasa_ont_premium = $jml_wifiext = $jasa_wifiext = $jml_jasa_wifiext = $jml_meshwifi = $jasa_meshwifi = $jml_jasa_meshwifi = $jml_plc = $jasa_plc = $jml_jasa_plc = $jml_indihome_smart = $jasa_indihome_smart = $jml_jasa_indihome_smart = $jml_ssl = $jasa_jml = $total_ssl = $total_jasa = 0;

          foreach ($data as $key => $value)
          {
            $jml_p1                     += $value->p1;
            $jasa_p1                    = ($value->p1 * $value->hss_p1);
            $jml_jasa_p1                += ($value->p1 * $value->hss_p1);

            $jml_p2_tlpint              += $value->p2_tlpint;
            $jasa_p2_tlpint             = ($value->p2_tlpint * $value->hss_p2_tlpint);
            $jml_jasa_p2_tlpint         += ($value->p2_tlpint * $value->hss_p2_tlpint);

            $jml_p2_intiptv             += $value->p2_intiptv;
            $jasa_p2_intiptv            = ($value->p2_intiptv * $value->hss_p2_intiptv);
            $jml_jasa_p2_intiptv        += ($value->p2_intiptv * $value->hss_p2_intiptv);

            $jml_p3                     += $value->p3;
            $jasa_p3                    = ($value->p3 * $value->hss_p3);
            $jml_jasa_p3                += ($value->p3 * $value->hss_p3);

            $jml_p1_survey              += $value->p1_survey;
            $jasa_p1_survey             = ($value->p1_survey * $value->hss_p1_survey);
            $jml_jasa_p1_survey         += ($value->p1_survey * $value->hss_p1_survey);

            $jml_p2_tlpint_survey       += $value->p2_tlpint_survey;
            $jasa_p2_tlpint_survey      = ($value->p2_tlpint_survey * $value->hss_p2_tlpint_survey);
            $jml_jasa_p2_tlpint_survey  += ($value->p2_tlpint_survey * $value->hss_p2_tlpint_survey);

            $jml_p2_intiptv_survey      += $value->p2_intiptv_survey;
            $jasa_p2_intiptv_survey     = ($value->p2_intiptv_survey * $value->hss_p2_intiptv_survey);
            $jml_jasa_p2_intiptv_survey += ($value->p2_intiptv_survey * $value->hss_p2_intiptv_survey);

            $jml_p3_survey              += $value->p3_survey;
            $jasa_p3_survey             = ($value->p3_survey * $value->hss_p3_survey);
            $jml_jasa_p3_survey         += ($value->p3_survey * $value->hss_p3_survey);

            $jml_wms                    += $value->wms;
            $jasa_wms                   = (($value->wms * $value->hss_lme_wifi) + ($value->wms * $value->hss_lme_ap_indoor));
            $jml_jasa_wms               += (($value->wms * $value->hss_lme_wifi) + ($value->wms * $value->hss_lme_ap_indoor));

            $jml_wms_lite               += $value->wms_lite;
            $jasa_wms_lite              = ($value->wms_lite * $value->hss_lme_wifi);
            $jml_jasa_wms_lite          += ($value->wms_lite * $value->hss_lme_wifi);

            $jml_addon_stb              += $value->addon_stb;
            $jasa_addon_stb             = ($value->addon_stb * $value->hss_stb_tambahan);
            $jml_jasa_addon_stb         += ($value->addon_stb * $value->hss_stb_tambahan);

            $jml_second_stb             += $value->second_stb;
            $jasa_second_stb            = ($value->second_stb * $value->hss_stb_tambahan);
            $jml_jasa_second_stb        += ($value->second_stb * $value->hss_stb_tambahan);

            $jml_change_stb             += $value->change_stb;
            $jasa_change_stb            = ($value->change_stb * $value->hss_change_stb);
            $jml_jasa_change_stb        += ($value->change_stb * $value->hss_change_stb);

            $jml_ont_premium            += $value->ont_premium;
            $jasa_ont_premium           = ($value->ont_premium * $value->hss_ont_premium);
            $jml_jasa_ont_premium       += ($value->ont_premium * $value->hss_ont_premium);

            $jml_wifiext                += $value->wifiext;
            $jasa_wifiext               = ($value->wifiext * $value->hss_wifiext);
            $jml_jasa_wifiext           += ($value->wifiext * $value->hss_wifiext);

            $jml_meshwifi               += $value->meshwifi;
            $jasa_meshwifi              = ($value->meshwifi * $value->hss_wifiext);
            $jml_jasa_meshwifi          += ($value->meshwifi * $value->hss_wifiext);

            $jml_plc                    += $value->plc;
            $jasa_plc                   = ($value->plc * $value->hss_plc);
            $jml_jasa_plc               += ($value->plc * $value->hss_plc);

            $jml_indihome_smart         += $value->indihome_smart;
            $jasa_indihome_smart        = ($value->indihome_smart * $value->hss_indihome_smart);
            $jml_jasa_indihome_smart    += ($value->indihome_smart * $value->hss_indihome_smart);

            $jml_ssl                    = ($value->p1 + $value->p2_tlpint + $value->p2_intiptv + $value->p3 + $value->p1_survey + $value->p2_tlpint_survey + $value->p2_intiptv_survey + $value->p3_survey + $value->wms + $value->wms_lite + $value->addon_stb + $value->second_stb + $value->ont_premium + $value->wifiext + $value->meshwifi + $value->plc + $value->indihome_smart);
            $jasa_jml                   = ($jasa_p1 + $jasa_p2_tlpint + $jasa_p2_intiptv + $jasa_p3 + $jasa_p1_survey + $jasa_p2_tlpint_survey + $jasa_p2_intiptv_survey + $jasa_p3_survey + $jasa_wms + $jasa_wms_lite + $jasa_addon_stb + $jasa_second_stb + $jasa_change_stb + $jasa_ont_premium + $jasa_wifiext + $jasa_meshwifi + $jasa_plc + $jasa_indihome_smart);
            $total_ssl                  += $jml_ssl;
            $total_jasa                 += $jasa_jml;

            $result['data'][] = [
              $value->reg,
              $value->witel,
              $value->mitra,

              '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=ORDER_CUTOFF&witel='.$value->witel.'&mitra='.$value->mitra.'&startDate='.$startDate.'&endDate='.$endDate.'&layanan=p1">'.str_replace(',', '.', number_format($value->p1 ? : '0')).'</a>',
              str_replace(',', '.', number_format($jasa_p1 ? : '0')),

              '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=ORDER_CUTOFF&witel='.$value->witel.'&mitra='.$value->mitra.'&startDate='.$startDate.'&endDate='.$endDate.'&layanan=p2_tlpint">'.str_replace(',', '.', number_format($value->p2_tlpint ? : '0')).'</a>',
              str_replace(',', '.', number_format($jasa_p2_tlpint ? : '0')),

              '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=ORDER_CUTOFF&witel='.$value->witel.'&mitra='.$value->mitra.'&startDate='.$startDate.'&endDate='.$endDate.'&layanan=p2_intiptv">'.str_replace(',', '.', number_format($value->p2_intiptv ? : '0')).'</a>',
              str_replace(',', '.', number_format($jasa_p2_intiptv ? : '0')),

              '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=ORDER_CUTOFF&witel='.$value->witel.'&mitra='.$value->mitra.'&startDate='.$startDate.'&endDate='.$endDate.'&layanan=p3">'.str_replace(',', '.', number_format($value->p3 ? : '0')),
              str_replace(',', '.', number_format($jasa_p3 ? : '0')),

              '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=ORDER_CUTOFF&witel='.$value->witel.'&mitra='.$value->mitra.'&startDate='.$startDate.'&endDate='.$endDate.'&layanan=p1_survey">'.str_replace(',', '.', number_format($value->p1_survey ? : '0')).'</a>',
              str_replace(',', '.', number_format($jasa_p1_survey ? : '0')),

              '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=ORDER_CUTOFF&witel='.$value->witel.'&mitra='.$value->mitra.'&startDate='.$startDate.'&endDate='.$endDate.'&layanan=p2_tlpint_survey">'.str_replace(',', '.', number_format($value->p2_tlpint_survey ? : '0')).'</a>',
              str_replace(',', '.', number_format($jasa_p2_tlpint_survey ? : '0')),

              '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=ORDER_CUTOFF&witel='.$value->witel.'&mitra='.$value->mitra.'&startDate='.$startDate.'&endDate='.$endDate.'&layanan=p2_intiptv_survey">'.str_replace(',', '.', number_format($value->p2_intiptv_survey ? : '0')).'</a>',
              str_replace(',', '.', number_format($jasa_p2_intiptv_survey ? : '0')),

              '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=ORDER_CUTOFF&witel='.$value->witel.'&mitra='.$value->mitra.'&startDate='.$startDate.'&endDate='.$endDate.'&layanan=p3_survey">'.str_replace(',', '.', number_format($value->p3_survey ? : '0')).'</a>',
              str_replace(',', '.', number_format($jasa_p3_survey ? : '0')),

              '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=ORDER_CUTOFF&witel='.$value->witel.'&mitra='.$value->mitra.'&startDate='.$startDate.'&endDate='.$endDate.'&layanan=wms">'.str_replace(',', '.', number_format($value->wms ? : '0')).'</a>',
              str_replace(',', '.', number_format($jasa_wms ? : '0')),

              '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=ORDER_CUTOFF&witel='.$value->witel.'&mitra='.$value->mitra.'&startDate='.$startDate.'&endDate='.$endDate.'&layanan=wms_lite">'.str_replace(',', '.', number_format($value->wms_lite ? : '0')).'</a>',
              str_replace(',', '.', number_format($jasa_wms_lite ? : '0')),

              '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=ORDER_CUTOFF&witel='.$value->witel.'&mitra='.$value->mitra.'&startDate='.$startDate.'&endDate='.$endDate.'&layanan=addon_stb">'.str_replace(',', '.', number_format($value->addon_stb ? : '0')).'</a>',
              str_replace(',', '.', number_format($jasa_addon_stb ? : '0')),

              '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=ORDER_CUTOFF&witel='.$value->witel.'&mitra='.$value->mitra.'&startDate='.$startDate.'&endDate='.$endDate.'&layanan=second_stb">'.str_replace(',', '.', number_format($value->second_stb ? : '0')).'</a>',
              str_replace(',', '.', number_format($jasa_second_stb ? : '0')),

              '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=ORDER_CUTOFF&witel='.$value->witel.'&mitra='.$value->mitra.'&startDate='.$startDate.'&endDate='.$endDate.'&layanan=change_stb">'.str_replace(',', '.', number_format($value->change_stb ? : '0')).'</a>',
              str_replace(',', '.', number_format($jasa_change_stb ? : '0')),

              '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=ORDER_CUTOFF&witel='.$value->witel.'&mitra='.$value->mitra.'&startDate='.$startDate.'&endDate='.$endDate.'&layanan=ont_premium">'.str_replace(',', '.', number_format($value->ont_premium ? : '0')).'</a>',
              str_replace(',', '.', number_format($jasa_ont_premium ? : '0')),

              '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=ORDER_CUTOFF&witel='.$value->witel.'&mitra='.$value->mitra.'&startDate='.$startDate.'&endDate='.$endDate.'&layanan=wifiext">'.str_replace(',', '.', number_format($value->wifiext ? : '0')).'</a>',
              str_replace(',', '.', number_format($jasa_wifiext ? : '0')),

              '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=ORDER_CUTOFF&witel='.$value->witel.'&mitra='.$value->mitra.'&startDate='.$startDate.'&endDate='.$endDate.'&layanan=meshwifi">'.str_replace(',', '.', number_format($value->meshwifi ? : '0')).'</a>',
              str_replace(',', '.', number_format($jasa_meshwifi ? : '0')),

              '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=ORDER_CUTOFF&witel='.$value->witel.'&mitra='.$value->mitra.'&startDate='.$startDate.'&endDate='.$endDate.'&layanan=plc">'.str_replace(',', '.', number_format($value->plc ? : '0')).'</a>',
              str_replace(',', '.', number_format($jasa_plc ? : '0')),

              '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=ORDER_CUTOFF&witel='.$value->witel.'&mitra='.$value->mitra.'&startDate='.$startDate.'&endDate='.$endDate.'&layanan=indihome_smart">'.str_replace(',', '.', number_format($value->indihome_smart ? : '0')).'</a>',
              str_replace(',', '.', number_format($jasa_indihome_smart ? : '0')),

              str_replace(',', '.', number_format($jml_ssl ? : '0')),
              str_replace(',', '.', number_format($jasa_jml ? : '0'))
            ];
          }

          $result['footer'] = [
            '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=ORDER_CUTOFF&witel=ALL&mitra=ALL&startDate='.$startDate.'&endDate='.$endDate.'&layanan=p1">'.str_replace(',', '.', number_format($jml_p1 ? : '0')).'</a>',
            str_replace(',', '.', number_format($jml_jasa_p1 ? : '0')),

            '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=ORDER_CUTOFF&witel=ALL&mitra=ALL&startDate='.$startDate.'&endDate='.$endDate.'&layanan=p2_tlpint">'.str_replace(',', '.', number_format($jml_p2_tlpint ? : '0')).'</a>',
            str_replace(',', '.', number_format($jml_jasa_p2_tlpint ? : '0')),

            '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=ORDER_CUTOFF&witel=ALL&mitra=ALL&startDate='.$startDate.'&endDate='.$endDate.'&layanan=p2_intiptv">'.str_replace(',', '.', number_format($jml_p2_intiptv ? : '0')).'</a>',
            str_replace(',', '.', number_format($jml_jasa_p2_intiptv ? : '0')),

            '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=ORDER_CUTOFF&witel=ALL&mitra=ALL&startDate='.$startDate.'&endDate='.$endDate.'&layanan=p3">'.str_replace(',', '.', number_format($jml_p3 ? : '0')).'</a>',
            str_replace(',', '.', number_format($jml_jasa_p3 ? : '0')),

            '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=ORDER_CUTOFF&witel=ALL&mitra=ALL&startDate='.$startDate.'&endDate='.$endDate.'&layanan=p1_survey">'.str_replace(',', '.', number_format($jml_p1_survey ? : '0')).'</a>',
            str_replace(',', '.', number_format($jml_jasa_p1_survey ? : '0')),

            '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=ORDER_CUTOFF&witel=ALL&mitra=ALL&startDate='.$startDate.'&endDate='.$endDate.'&layanan=p2_tlpint_survey">'.str_replace(',', '.', number_format($jml_p2_tlpint_survey ? : '0')).'</a>',
            str_replace(',', '.', number_format($jml_jasa_p2_tlpint_survey ? : '0')),

            '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=ORDER_CUTOFF&witel=ALL&mitra=ALL&startDate='.$startDate.'&endDate='.$endDate.'&layanan=p2_intiptv_survey">'.str_replace(',', '.', number_format($jml_p2_intiptv_survey ? : '0')).'</a>',
            str_replace(',', '.', number_format($jml_jasa_p2_intiptv_survey ? : '0')),

            '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=ORDER_CUTOFF&witel=ALL&mitra=ALL&startDate='.$startDate.'&endDate='.$endDate.'&layanan=p3_survey">'.str_replace(',', '.', number_format($jml_p3_survey ? : '0')).'</a>',
            str_replace(',', '.', number_format($jml_jasa_p3_survey ? : '0')),

            '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=ORDER_CUTOFF&witel=ALL&mitra=ALL&startDate='.$startDate.'&endDate='.$endDate.'&layanan=wms">'.str_replace(',', '.', number_format($jml_wms ? : '0')).'</a>',
            str_replace(',', '.', number_format($jml_jasa_wms ? : '0')),

            '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=ORDER_CUTOFF&witel=ALL&mitra=ALL&startDate='.$startDate.'&endDate='.$endDate.'&layanan=wms_lite">'.str_replace(',', '.', number_format($jml_wms_lite ? : '0')).'</a>',
            str_replace(',', '.', number_format($jml_jasa_wms_lite ? : '0')),

            '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=ORDER_CUTOFF&witel=ALL&mitra=ALL&startDate='.$startDate.'&endDate='.$endDate.'&layanan=addon_stb">'.str_replace(',', '.', number_format($jml_addon_stb ? : '0')).'</a>',
            str_replace(',', '.', number_format($jml_jasa_addon_stb ? : '0')),

            '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=ORDER_CUTOFF&witel=ALL&mitra=ALL&startDate='.$startDate.'&endDate='.$endDate.'&layanan=second_stb">'.str_replace(',', '.', number_format($jml_second_stb ? : '0')).'</a>',
            str_replace(',', '.', number_format($jml_jasa_second_stb ? : '0')),

            '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=ORDER_CUTOFF&witel=ALL&mitra=ALL&startDate='.$startDate.'&endDate='.$endDate.'&layanan=change_stb">'.str_replace(',', '.', number_format($jml_change_stb ? : '0')).'</a>',
            str_replace(',', '.', number_format($jml_jasa_change_stb ? : '0')),

            '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=ORDER_CUTOFF&witel=ALL&mitra=ALL&startDate='.$startDate.'&endDate='.$endDate.'&layanan=ont_premium">'.str_replace(',', '.', number_format($jml_ont_premium ? : '0')).'</a>',
            str_replace(',', '.', number_format($jml_jasa_ont_premium ? : '0')),

            '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=ORDER_CUTOFF&witel=ALL&mitra=ALL&startDate='.$startDate.'&endDate='.$endDate.'&layanan=wifiext">'.str_replace(',', '.', number_format($jml_wifiext ? : '0')).'</a>',
            str_replace(',', '.', number_format($jml_jasa_wifiext ? : '0')),

            '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=ORDER_CUTOFF&witel=ALL&mitra=ALL&startDate='.$startDate.'&endDate='.$endDate.'&layanan=meshwifi">'.str_replace(',', '.', number_format($jml_meshwifi ? : '0')).'</a>',
            str_replace(',', '.', number_format($jml_jasa_meshwifi ? : '0')),

            '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=ORDER_CUTOFF&witel=ALL&mitra=ALL&startDate='.$startDate.'&endDate='.$endDate.'&layanan=plc">'.str_replace(',', '.', number_format($jml_plc ? : '0')).'</a>',
            str_replace(',', '.', number_format($jml_jasa_plc ? : '0')),

            '<a style="color: #6c757d" target="_blank" href="/Rekon/provisioning/dashboard/order-detail?view=ORDER_CUTOFF&witel=ALL&mitra=ALL&startDate='.$startDate.'&endDate='.$endDate.'&layanan=indihome_smart">'.str_replace(',', '.', number_format($jml_indihome_smart ? : '0')).'</a>',
            str_replace(',', '.', number_format($jml_jasa_indihome_smart ? : '0')),

            str_replace(',', '.', number_format($total_ssl ? : '0')),
            str_replace(',', '.', number_format($total_jasa ? : '0'))
          ];

      break;
    }

    return \Response::json($result);
  }

  public function provDashboardOrderDetail()
  {
    $view      = Input::get('view');
    $startDate = Input::get('startDate');
    $endDate   = Input::get('endDate');
    $witel     = Input::get('witel');
    $mitra     = Input::get('mitra');
    $month     = Input::get('month');
    $day       = Input::get('day');
    $layanan   = Input::get('layanan');

    return view('Rekon.provisioning.dashboard_order_detail', compact('view', 'startDate', 'endDate', 'witel', 'mitra', 'month', 'day', 'layanan'));
  }

  public function ajx_provDashboardOrderDetail()
  {
    $view      = Input::get('view');
    $startDate = Input::get('startDate');
    $endDate   = Input::get('endDate');
    $witel     = Input::get('witel');
    $mitra     = Input::get('mitra');
    $month     = Input::get('month');
    $day       = Input::get('day');
    $layanan   = Input::get('layanan');

    $result['data'] = [];
    $numb = 1;

    $data = RekonModel::provDashboardOrderDetail($view, $startDate, $endDate, $witel, $mitra, $month, $day, $layanan);

    foreach ($data as $key => $value)
    {
      $result['data'][] = [
        $numb++,
        '<div class="custom-control custom-switch"><input type="checkbox" class="custom-control-input" id="'.$value->ORDER_ID.'" checked><label class="custom-control-label" for="'.$value->ORDER_ID.'"></label></div>',
        $value->is_layanan,
        $value->ORDER_ID,
        $value->REGIONAL,
        $value->WITEL,
        $value->mitra,
        $value->DATEL,
        $value->STO,
        $value->EXTERN_ORDER_ID,
        $value->JENIS_PSB,
        $value->TYPE_TRANSAKSI,
        $value->FLAG_DEPOSIT,
        $value->STATUS_RESUME,
        $value->STATUS_MESSAGE,
        $value->KCONTACT,
        $value->ORDER_DATE,
        $value->NCLI,
        $value->NDEM,
        $value->SPEEDY,
        $value->POTS,

        $value->utt_statusName,
        $value->utt_qcStatusName,
        $value->utt_create_dtm,

        $value->CUSTOMER_NAME,
        str_replace(array("+62-", "+62-0", "+62-00", "+62-000", "+622-", "+622-0", "+622-00", "+622-000"), "0", $value->NO_HP),
        $value->EMAIL,
        $value->INSTALL_ADDRESS,
        $value->CUSTOMER_ADDRESS,
        $value->CITY_NAME,
        $value->GPS_LATITUDE,
        $value->GPS_LONGITUDE,
        $value->PACKAGE_NAME,
        $value->LOC_ID,
        $value->DEVICE_ID,
        $value->AGENT_ID,
        $value->WFM_ID,
        $value->SCHEDSTART,
        $value->SCHEDFINISH,
        $value->ACTSTART,
        $value->ACTFINISH,
        $value->SCHEDULE_LABOR,
        $value->FINISH_LABOR,
        $value->LAST_UPDATED_DATE,
        $value->TYPE_LAYANAN,
        $value->ISI_COMMENT,
        $value->TINDAK_LANJUT,
        $value->USER_ID_TL,
        $value->TL_DATE,
        $value->TANGGAL_PROSES,
        $value->TANGGAL_MANJA,
        $value->HIDE,
        $value->CATEGORY,
        $value->PROVIDER,
        $value->NPER,
        $value->AMCREW,
        $value->STATUS_WO,
        $value->STATUS_TASK,
        $value->CHANNEL,
        $value->GROUP_CHANNEL,
        $value->PRODUCT,
        $value->dc_roll,
        $value->precon50,
        $value->precon70,
        $value->precon80,
        (($value->precon50 * 50) + ($value->precon70 * 70) + ($value->precon80 * 80)),
        $value->adaptor_sc,
        $value->otp_ftth,
        $value->prekso15,
        $value->prekso20,
        $value->patchcord10,
        $value->patchcord15,
        $value->utpc6,
        $value->tiang7,
        $value->tiang9,
        $value->traycable,
        $value->breket,
        $value->sclamp,
        $value->rj45t5,
        $value->rj45t6,
        $value->soc_ils,
        $value->soc_sum,
      ];
    }

    return \Response::json($result);
  }
}