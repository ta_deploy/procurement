<?php

namespace App\Http\Controllers;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Events\BeforeSheet;
use Maatwebsite\Excel\Events\BeforeWriting;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use Maatwebsite\Excel\Excel;
use DB;
class Excel_RincianJustifikasiPSB implements  WithEvents
{
    use Exportable;

    public function __construct($data, $output_file)
    {
        $this->data = $data;
        $this->output_file = $output_file;
    }

    public function only_month($date)
    {
        $bulan = array (1 =>   'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );

        $num = date('m', strtotime($date));

        if ($num < 10)
        {
            $keys = str_replace('0', '', $num);
        } else {
            $keys = $num;
        }

        return $bulan[$keys];
    }

    public function convert_month($date, $cetak_hari = false)
    {
        $bulan = array (1 =>   'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );

        $hari = array ( 1 =>    'Senin',
            'Selasa',
            'Rabu',
            'Kamis',
            'Jumat',
            'Sabtu',
            'Minggu'
        );

        $result = date('d m Y', strtotime($date));
        $split = explode(' ', $result);
        $hasilnya = $split[0] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[2];

        if ($cetak_hari)
        {
            $num = date('N', strtotime($date));
            return $hari[$num].' '.$hasilnya;
        }

        return $hasilnya;
    }

    public function ttd_user($rule)
    {
        return DB::table('procurement_dok_ttd')
        ->leftJoin('procurement_user', 'procurement_dok_ttd.id_user', '=', 'procurement_user.id')
        ->select('procurement_user.*')
        ->where('procurement_dok_ttd.witel', $this->data->witel)
        ->where('procurement_dok_ttd.pekerjaan', 'PSB')
        ->where('procurement_dok_ttd.rule_dok', $rule)
        ->first();
    }

    public function registerEvents(): array
    {
        return [
            BeforeExport::class  => function(BeforeExport $event) {

                $reader = new Xlsx();
                \PhpOffice\PhpSpreadsheet\Shared\File::setUseUploadTempDirectory(true);
                $spreadSheet = $reader->load(public_path(). '/template_doc/psb/khs_2021/Justifikasi_dan_Rincian_SSL_(PSB).xlsx');

                $spreadSheet->getSheet(1)->getCell('J2')->SetValue('Tgl. Terbit : '.date('d/m/Y', strtotime($this->data->tanggal_boq)) );
                $spreadSheet->getSheet(1)->getCell('F6')->SetValue('PT. TELKOM AKSES WITEL ' . $this->data->witel . '');
                $spreadSheet->getSheet(1)->getCell('F7')->SetValue('PEKERJAAN PASANG SAMBUNGAN BARU (PSB) PERIODE '. strtoupper($this->data->bulan).' WITEL ' . $this->data->witel . '');
                $spreadSheet->getSheet(1)->getCell('F10')->SetValue($this->data->id_project);

                $jml_pekerjaan_psb = 
                    ($this->data->p1_survey_ssl * $this->data->p1_survey_hss) +
                    ($this->data->p2_tlpint_survey_ssl * $this->data->p2_tlpint_survey_hss) +
                    ($this->data->p2_intiptv_survey_ssl * $this->data->p2_intiptv_survey_hss) + 
                    ($this->data->p3_survey_ssl * $this->data->p3_survey_hss) +

                    ($this->data->p1_ssl * $this->data->p1_hss) +
                    ($this->data->p2_ssl * $this->data->p2_hss) + // versi khs 2021
                    ($this->data->p2_tlpint_ssl * $this->data->p2_tlpint_hss) +
                    ($this->data->p2_intiptv_ssl * $this->data->p2_intiptv_hss) + 
                    ($this->data->p3_ssl * $this->data->p3_hss);

                $jml_pekerjaan_migrasi =
                    ($this->data->migrasi_service_1p2p_ssl * $this->data->migrasi_service_1p2p_hss) +
                    ($this->data->migrasi_service_1p3p_ssl * $this->data->migrasi_service_1p3p_hss) +
                    ($this->data->migrasi_service_2p3p_ssl * $this->data->migrasi_service_2p3p_hss);

                $jml_pekerjaan_tambahan =
                    ($this->data->ikr_addon_stb_ssl * $this->data->ikr_addon_stb_hss) +
                    ($this->data->change_stb_ssl * $this->data->change_stb_hss) +
                    ($this->data->indihome_smart_ssl * $this->data->indihome_smart_hss) +
                    ($this->data->wifiextender_ssl * $this->data->wifiextender_hss) +
                    ($this->data->plc_ssl * $this->data->plc_hss) +
                    ($this->data->lme_wifi_pt1_ssl * $this->data->lme_wifi_pt1_hss) +
                    ($this->data->lme_ap_indoor_ssl * $this->data->lme_ap_indoor_hss) +
                    ($this->data->lme_ap_outdoor_ssl * $this->data->lme_ap_outdoor_hss) +
                    ($this->data->ont_premium_ssl * $this->data->ont_premium_hss) +
                    ($this->data->pu_s7_140_ssl * $this->data->pu_s7_140_hss) +
                    ($this->data->pu_s9_140_ssl * $this->data->pu_s9_140_hss);

                $total_nilai = $jml_pekerjaan_psb + $jml_pekerjaan_migrasi + $jml_pekerjaan_tambahan;


                $spreadSheet->getSheet(1)->getCell('F11')->SetValue('Rp. ' . number_format($total_nilai, '0', ',', '.') .' - (BELUM TERMASUK PPN ' . $this->data->ppn_numb . '%)');

                $spreadSheet->getSheet(1)->getCell('F12')->SetValue(strtoupper($this->only_month($this->data->rekon_periode1)).' - '.strtoupper($this->only_month($this->data->rekon_periode2)));

                $judul = 'Pekerjaan Pasang Sambungan Baru (PSB) Periode '.$this->data->month.' Witel ' . ucfirst(strtolower($this->data->witel)) . '';

                $spreadSheet->getSheet(1)->getCell('C15')->SetValue('Untuk mempercepat '.$judul.' agar selesai sesuai dengan waktu yang telah ditentukan.');

                $spreadSheet->getSheet(1)->getCell('C16')->SetValue(''.$judul.' ini untuk memastikan program kerja lain dapat berjalan secara paralel.');

                $spreadSheet->getSheet(1)->getCell('C19')->SetValue('Mudah dalam pengawasan dan pelaksanaan '.$judul.'');

                $spreadSheet->getSheet(1)->getCell('C22')->SetValue('Meningkatkan kecepatan '.$judul.' dan meningkatkan revenue perusahaan.');

                $spreadSheet->getSheet(1)->getCell('D26')->SetValue($judul);

                $spreadSheet->getSheet(1)->getCell('C33')->SetValue('Waktu    : '.$this->convert_month($this->data->rekon_periode1).' - '.$this->convert_month($this->data->rekon_periode2));

                $spreadSheet->getSheet(1)->getCell('C34')->SetValue('Tempat    : ' . $this->data->witel . '');

                $spreadSheet->getSheet(1)->getCell('C36')->SetValue('MITRA : '.preg_replace('/^PT/', 'PT.', $this->data->nama_company).'');

                $spreadSheet->getSheet(1)->getCell('E44')->SetValue($this->ttd_user('sm_krj')->user . ' / ' . $this->ttd_user('sm_krj')->nik);
                $spreadSheet->getSheet(1)->getCell('G44')->SetValue($this->ttd_user('sm_krj')->jabatan);
                $spreadSheet->getSheet(1)->getCell('E45')->SetValue($this->ttd_user('mngr_krj')->user . ' / ' . $this->ttd_user('mngr_krj')->nik);
                $spreadSheet->getSheet(1)->getCell('G45')->SetValue($this->ttd_user('mngr_krj')->jabatan);

                $spreadSheet->getSheet(1)->getCell('I44')->SetValue( date('d/m/Y', strtotime($this->data->tanggal_boq)) );
                $spreadSheet->getSheet(1)->getCell('I45')->SetValue( date('d/m/Y', strtotime($this->data->tanggal_boq)) );
                $spreadSheet->getSheet(1)->getCell('I46')->SetValue( date('d/m/Y', strtotime($this->data->tanggal_boq)) );
                $spreadSheet->getSheet(1)->getCell('I47')->SetValue( date('d/m/Y', strtotime($this->data->tanggal_boq)) );

                if ($total_nilai > 0 && $total_nilai <= 200000000)
                {
                    $rule = 'krglbh_dua_jt';
                } elseif ($total_nilai > 200000000 && $total_nilai <= 500000000) {
                    $rule = 'antr_dualima_jt';
                } else {
                    $rule = 'lbh_lima_jt';
                }

                $spreadSheet->getSheet(1)->getCell('E47')->SetValue($this->ttd_user($rule)->user . ' / ' . $this->ttd_user($rule)->nik);
                $spreadSheet->getSheet(1)->getCell('G47')->SetValue($this->ttd_user($rule)->jabatan);

                $spreadSheet->getSheet(0)->getCell('A2')->SetValue('RINCIAN PERKIRAAN PEKERJAAN PSB PERIODE '.strtoupper($this->convert_month($this->data->rekon_periode1)).' - '.strtoupper($this->convert_month($this->data->rekon_periode2)));

                $spreadSheet->getSheet(0)->getCell('A3')->SetValue(preg_replace('/^PT/', 'PT.',$this->data->nama_company) );

                $spreadSheet->getSheet(0)->getCell('C6')->SetValue($this->data->p1_survey_hss);
                $spreadSheet->getSheet(0)->getCell('D6')->SetValue($this->data->p1_survey_ssl);
                $spreadSheet->getSheet(0)->getCell('C7')->SetValue($this->data->p2_tlpint_survey_hss);
                $spreadSheet->getSheet(0)->getCell('D7')->SetValue($this->data->p2_tlpint_survey_ssl);
                $spreadSheet->getSheet(0)->getCell('C8')->SetValue($this->data->p2_intiptv_survey_hss);
                $spreadSheet->getSheet(0)->getCell('D8')->SetValue($this->data->p2_intiptv_survey_ssl);
                $spreadSheet->getSheet(0)->getCell('C9')->SetValue($this->data->p3_survey_hss);
                $spreadSheet->getSheet(0)->getCell('D9')->SetValue($this->data->p3_survey_ssl);
                $spreadSheet->getSheet(0)->getCell('C10')->SetValue($this->data->p1_hss);
                $spreadSheet->getSheet(0)->getCell('D10')->SetValue($this->data->p1_ssl);
                $spreadSheet->getSheet(0)->getCell('C11')->SetValue($this->data->p2_hss);
                $spreadSheet->getSheet(0)->getCell('D11')->SetValue($this->data->p2_ssl);
                $spreadSheet->getSheet(0)->getCell('C12')->SetValue($this->data->p2_tlpint_hss);
                $spreadSheet->getSheet(0)->getCell('D12')->SetValue($this->data->p2_tlpint_ssl);
                $spreadSheet->getSheet(0)->getCell('C13')->SetValue($this->data->p2_intiptv_hss);
                $spreadSheet->getSheet(0)->getCell('D13')->SetValue($this->data->p2_intiptv_ssl);
                $spreadSheet->getSheet(0)->getCell('C14')->SetValue($this->data->p3_hss);
                $spreadSheet->getSheet(0)->getCell('D14')->SetValue($this->data->p3_ssl);
                $spreadSheet->getSheet(0)->getCell('C15')->SetValue($this->data->migrasi_service_1p2p_hss);
                $spreadSheet->getSheet(0)->getCell('D15')->SetValue($this->data->migrasi_service_1p2p_ssl);
                $spreadSheet->getSheet(0)->getCell('C16')->SetValue($this->data->migrasi_service_1p3p_hss);
                $spreadSheet->getSheet(0)->getCell('D16')->SetValue($this->data->migrasi_service_1p3p_ssl);
                $spreadSheet->getSheet(0)->getCell('C17')->SetValue($this->data->migrasi_service_2p3p_hss);
                $spreadSheet->getSheet(0)->getCell('D17')->SetValue($this->data->migrasi_service_2p3p_ssl);
                $spreadSheet->getSheet(0)->getCell('C18')->SetValue($this->data->lme_wifi_pt1_hss);
                $spreadSheet->getSheet(0)->getCell('D18')->SetValue($this->data->lme_wifi_pt1_ssl);
                $spreadSheet->getSheet(0)->getCell('C19')->SetValue($this->data->lme_ap_indoor_hss);
                $spreadSheet->getSheet(0)->getCell('D19')->SetValue($this->data->lme_ap_indoor_ssl);
                $spreadSheet->getSheet(0)->getCell('C20')->SetValue($this->data->lme_ap_outdoor_hss);
                $spreadSheet->getSheet(0)->getCell('D20')->SetValue($this->data->lme_ap_outdoor_ssl);
                $spreadSheet->getSheet(0)->getCell('C21')->SetValue($this->data->indihome_smart_hss);
                $spreadSheet->getSheet(0)->getCell('D21')->SetValue($this->data->indihome_smart_ssl);
                $spreadSheet->getSheet(0)->getCell('C22')->SetValue($this->data->plc_hss);
                $spreadSheet->getSheet(0)->getCell('D22')->SetValue($this->data->plc_ssl);
                $spreadSheet->getSheet(0)->getCell('C23')->SetValue($this->data->ikr_addon_stb_hss);
                $spreadSheet->getSheet(0)->getCell('D23')->SetValue($this->data->ikr_addon_stb_ssl);
                $spreadSheet->getSheet(0)->getCell('C24')->SetValue($this->data->change_stb_hss);
                $spreadSheet->getSheet(0)->getCell('D24')->SetValue($this->data->change_stb_ssl);
                $spreadSheet->getSheet(0)->getCell('C25')->SetValue($this->data->ont_premium_hss);
                $spreadSheet->getSheet(0)->getCell('D25')->SetValue($this->data->ont_premium_ssl);
                $spreadSheet->getSheet(0)->getCell('C26')->SetValue($this->data->wifiextender_hss);
                $spreadSheet->getSheet(0)->getCell('D26')->SetValue($this->data->wifiextender_ssl);
                $spreadSheet->getSheet(0)->getCell('C27')->SetValue($this->data->pu_s7_140_hss);
                $spreadSheet->getSheet(0)->getCell('D27')->SetValue($this->data->pu_s7_140_ssl);
                $spreadSheet->getSheet(0)->getCell('C28')->SetValue($this->data->pu_s9_140_hss);
                $spreadSheet->getSheet(0)->getCell('D28')->SetValue($this->data->pu_s9_140_ssl);
                $spreadSheet->getSheet(0)->getCell('D36')->SetValue($this->ttd_user('sm_krj')->user);



                $writer = IOFactory::createWriter($spreadSheet, 'Xlsx');

                switch ($this->output_file)
                {
                    case 'save':

                            $path = public_path() . "/upload2/" . $this->data->id . '/';

                            if (!file_exists($path))
                            {
                                if (!mkdir($path, 0777, true))
                                {
                                    return 'gagal menyiapkan folder rekon';
                                }
                            }

                            $writer->save($path."Justifikasi_dan_Rincian_SSL_(PSB)_".str_replace(" ", "_", $this->data->nama_company).".xlsx");
                        break;
                    
                    default:
                            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                            header('Content-Disposition: attachment;filename="Justifikasi_dan_Rincian_SSL_(PSB)_'. str_replace(" ", "_", $this->data->nama_company).'.xlsx"');

                            $writer->save('php://output');
                            die;
                        break;
                }
            }
        ];
    }
}