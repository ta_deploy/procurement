<?php

namespace App\Http\Controllers;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Events\BeforeSheet;
use Maatwebsite\Excel\Events\BeforeWriting;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use Maatwebsite\Excel\Excel;
use DB;
class Excel_SuratPesananPSB implements  WithEvents
{
    use Exportable;

    public function __construct($data, $output_file)
    {
        $this->data = $data;
        $this->output_file = $output_file;
    }

    public function convert_month($date, $cetak_hari = false)
    {
        $bulan = array (1 =>   'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );

        $hari = array ( 1 => 'Senin',
            'Selasa',
            'Rabu',
            'Kamis',
            'Jumat',
            'Sabtu',
            'Minggu'
        );

        $result = date('d m Y', strtotime($date) );
        $split = explode(' ', $result);
        $hasilnya = $split[0] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[2];

        if($cetak_hari)
        {
            $num = date('N', strtotime($date) );
            return $hari[$num].' '.$hasilnya;
        }

        return $hasilnya;
    }

    public function only_day($date)
    {
        $hari = array ( 1 => 'Senin',
            'Selasa',
            'Rabu',
            'Kamis',
            'Jumat',
            'Sabtu',
            'Minggu'
        );

        $num = date('N', strtotime($date) );
        return $hari[$num];
    }

    public function terbilang($nilai)
    {
        if ($nilai < 0)
        {
            $hasil = "minus " . trim($this->penyebut($nilai) );
        }
        else
        {
            $hasil = trim($this->penyebut($nilai) );
        }

        return $hasil;
    }

    public function penyebut($nilai)
    {
        $nilai = abs($nilai);
        $huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
        $temp = "";

        if($nilai < 12)
        {
            $temp = " " . $huruf[$nilai];
        }
        else if($nilai < 20)
        {
            $temp = $this->penyebut($nilai - 10) . " belas";
        }
        else if($nilai < 100)
        {
            $temp = $this->penyebut($nilai / 10) . " puluh" . $this->penyebut($nilai % 10);
        }
        else if($nilai < 200)
        {
            $temp = " seratus" . $this->penyebut($nilai - 100);
        }
        else if($nilai < 1000)
        {
            $temp = $this->penyebut($nilai / 100) . " ratus" . $this->penyebut($nilai % 100);
        }
        else if($nilai < 2000)
        {
            $temp = " seribu" . $this->penyebut($nilai - 1000);
        }
        else if($nilai < 1000000)
        {
            $temp = $this->penyebut($nilai / 1000) . " ribu" . $this->penyebut($nilai % 1000);
        }
        else if($nilai < 1000000000)
        {
            $temp = $this->penyebut($nilai / 1000000) . " juta" . $this->penyebut($nilai % 1000000);
        }
        else if($nilai < 1000000000000)
        {
            $temp = $this->penyebut($nilai / 1000000000) . " milyar" . $this->penyebut(fmod($nilai, 1000000000) );
        }
        else if($nilai < 1000000000000000)
        {
            $temp = $this->penyebut($nilai / 1000000000000) . " trilyun" . $this->penyebut(fmod($nilai, 1000000000000) );
        }

        return$temp;
    }

    public function ttd_user($rule)
    {
        return DB::table('procurement_dok_ttd')
        ->leftJoin('procurement_user', 'procurement_dok_ttd.id_user', '=', 'procurement_user.id')
        ->select('procurement_user.*')
        ->where('procurement_dok_ttd.witel', $this->data->witel)
        ->where('procurement_dok_ttd.pekerjaan', 'PSB')
        ->where('procurement_dok_ttd.rule_dok', $rule)
        ->first();
    }

    public function registerEvents(): array
    {
        return [
            BeforeExport::class  => function(BeforeExport $event) {

                $reader = new Xlsx();
                \PhpOffice\PhpSpreadsheet\Shared\File::setUseUploadTempDirectory(true);
                $spreadSheet = $reader->load(public_path(). '/template_doc/psb/khs_2021/Surat_Pesanan_PSB.xlsx');

                $spreadSheet->getSheet(0)->getCell('B1')->SetValue($this->data->judul);
                $spreadSheet->getSheet(0)->getCell('B2')->SetValue('Pekerjaan Pasang Sambungan Baru (PSB) Periode '.$this->data->month.' '.date('Y', strtotime($this->data->tanggal_boq)).' Witel '.ucfirst(strtolower($this->data->witel)));
                $spreadSheet->getSheet(0)->getCell('B3')->SetValue(strtoupper($this->data->regional));
                $spreadSheet->getSheet(0)->getCell('B4')->SetValue(strtoupper($this->data->witel));
                $spreadSheet->getSheet(0)->getCell('B5')->SetValue($this->data->id_project);
                $spreadSheet->getSheet(0)->getCell('B6')->SetValue($this->data->no_sp);
                $spreadSheet->getSheet(0)->getCell('B7')->SetValue(strtoupper($this->convert_month($this->data->tgl_sp)));
                $spreadSheet->getSheet(0)->getCell('B8')->SetValue(preg_replace('/^PT/', 'PT.', $this->data->nama_company));
                $spreadSheet->getSheet(0)->getCell('B9')->SetValue(strtoupper($this->data->wakil_mitra));
                $spreadSheet->getSheet(0)->getCell('B10')->SetValue(strtoupper($this->data->jabatan_mitra));

                $jml_pekerjaan_psb = 
                    ($this->data->p1_survey_ssl * $this->data->p1_survey_hss) +
                    ($this->data->p2_tlpint_survey_ssl * $this->data->p2_tlpint_survey_hss) +
                    ($this->data->p2_intiptv_survey_ssl * $this->data->p2_intiptv_survey_hss) + 
                    ($this->data->p3_survey_ssl * $this->data->p3_survey_hss) +

                    ($this->data->p1_ssl * $this->data->p1_hss) +
                    ($this->data->p2_ssl * $this->data->p2_hss) + // versi khs 2021
                    ($this->data->p2_tlpint_ssl * $this->data->p2_tlpint_hss) +
                    ($this->data->p2_intiptv_ssl * $this->data->p2_intiptv_hss) + 
                    ($this->data->p3_ssl * $this->data->p3_hss);

                $jml_pekerjaan_migrasi =
                    ($this->data->migrasi_service_1p2p_ssl * $this->data->migrasi_service_1p2p_hss) +
                    ($this->data->migrasi_service_1p3p_ssl * $this->data->migrasi_service_1p3p_hss) +
                    ($this->data->migrasi_service_2p3p_ssl * $this->data->migrasi_service_2p3p_hss);

                $jml_pekerjaan_tambahan =
                    ($this->data->ikr_addon_stb_ssl * $this->data->ikr_addon_stb_hss) +
                    ($this->data->change_stb_ssl * $this->data->change_stb_hss) +
                    ($this->data->indihome_smart_ssl * $this->data->indihome_smart_hss) +
                    ($this->data->wifiextender_ssl * $this->data->wifiextender_hss) +
                    ($this->data->plc_ssl * $this->data->plc_hss) +
                    ($this->data->lme_wifi_pt1_ssl * $this->data->lme_wifi_pt1_hss) +
                    ($this->data->lme_ap_indoor_ssl * $this->data->lme_ap_indoor_hss) +
                    ($this->data->lme_ap_outdoor_ssl * $this->data->lme_ap_outdoor_hss) +
                    ($this->data->ont_premium_ssl * $this->data->ont_premium_hss) +
                    ($this->data->pu_s7_140_ssl * $this->data->pu_s7_140_hss) +
                    ($this->data->pu_s9_140_ssl * $this->data->pu_s9_140_hss);

                $nominal_sp = substr_replace(($jml_pekerjaan_psb + $jml_pekerjaan_migrasi + $jml_pekerjaan_tambahan), '000', -3, 3);

                $nominal_sp_ppn = ($nominal_sp * ($this->data->ppn_numb / 100));

                $g_total = $nominal_sp + $nominal_sp_ppn;

                if ($nominal_sp > 0 && $nominal_sp <= 200000000)
                {
                    $rule = 'krglbh_dua_jt';
                }
                elseif ($nominal_sp > 200000000 && $nominal_sp <= 500000000)
                {
                    $rule = 'antr_dualima_jt';
                }
                else
                {
                    $rule = 'lbh_lima_jt';
                }

                $spreadSheet->getSheet(0)->getCell('B11')->SetValue(strtoupper($this->ttd_user($rule)->user));
                $spreadSheet->getSheet(0)->getCell('B12')->SetValue(strtoupper($this->ttd_user($rule)->jabatan));
                $spreadSheet->getSheet(0)->getCell('B13')->SetValue($this->data->no_kontrak_ta);
                $spreadSheet->getSheet(0)->getCell('B14')->SetValue($this->convert_month($this->data->date_kontrak_ta));
                $spreadSheet->getSheet(0)->getCell('B15')->SetValue($this->data->no_penetapan_khs);
                $spreadSheet->getSheet(0)->getCell('B16')->SetValue($this->convert_month($this->data->date_khs_penetapan));
                $spreadSheet->getSheet(0)->getCell('B17')->SetValue($this->data->no_kesanggupan_khs);
                $spreadSheet->getSheet(0)->getCell('B18')->SetValue($this->convert_month($this->data->date_khs_kesanggupan));
                $spreadSheet->getSheet(0)->getCell('B19')->SetValue($this->data->month);
                $spreadSheet->getSheet(0)->getCell('B20')->SetValue(date('Y', strtotime($this->data->tanggal_boq)));
                $spreadSheet->getSheet(0)->getCell('B21')->SetValue($this->convert_month($this->data->rekon_periode1));
                $spreadSheet->getSheet(0)->getCell('B22')->SetValue($this->convert_month($this->data->rekon_periode2));
                $spreadSheet->getSheet(0)->getCell('B23')->SetValue($this->data->tgl_jatuh_tmp);
                $spreadSheet->getSheet(0)->getCell('B24')->SetValue(ucwords($this->terbilang($this->data->tgl_jatuh_tmp)));

                $spreadSheet->getSheet(0)->getCell('B25')->SetValue(number_format($nominal_sp, 0, ',', '.'));
                $spreadSheet->getSheet(0)->getCell('B26')->SetValue(ucwords($this->terbilang($nominal_sp)));
                $spreadSheet->getSheet(0)->getCell('B27')->SetValue($this->data->ppn_numb);
                $spreadSheet->getSheet(0)->getCell('B28')->SetValue(number_format($g_total, 0, ',', '.'));
                $spreadSheet->getSheet(0)->getCell('B29')->SetValue(ucwords($this->terbilang($g_total)));

                $spreadSheet->getSheet(0)->getCell('B30')->SetValue($this->data->p1_survey_hss);
                $spreadSheet->getSheet(0)->getCell('B31')->SetValue($this->data->p1_survey_ssl);
                if ($this->data->p1_survey_ssl == '0')
                {
                    $spreadSheet->getSheet(1)->getCell('E30')->SetValue('');
                    $spreadSheet->getSheet(1)->getCell('F30')->SetValue('');
                    $spreadSheet->getSheet(1)->getCell('G30')->SetValue('');
                }
                $spreadSheet->getSheet(0)->getCell('B34')->SetValue($this->data->p2_tlpint_survey_hss);
                $spreadSheet->getSheet(0)->getCell('B35')->SetValue($this->data->p2_tlpint_survey_ssl);
                if ($this->data->p2_tlpint_survey_ssl == '0')
                {
                    $spreadSheet->getSheet(1)->getCell('E31')->SetValue('');
                    $spreadSheet->getSheet(1)->getCell('F31')->SetValue('');
                    $spreadSheet->getSheet(1)->getCell('G31')->SetValue('');
                }
                $spreadSheet->getSheet(0)->getCell('B36')->SetValue($this->data->p2_intiptv_survey_hss);
                $spreadSheet->getSheet(0)->getCell('B37')->SetValue($this->data->p2_intiptv_survey_ssl);
                if ($this->data->p2_intiptv_survey_ssl == '0')
                {
                    $spreadSheet->getSheet(1)->getCell('E32')->SetValue('');
                    $spreadSheet->getSheet(1)->getCell('F32')->SetValue('');
                    $spreadSheet->getSheet(1)->getCell('G32')->SetValue('');
                }
                $spreadSheet->getSheet(0)->getCell('B38')->SetValue($this->data->p3_survey_hss);
                $spreadSheet->getSheet(0)->getCell('B39')->SetValue($this->data->p3_survey_ssl);
                if ($this->data->p3_survey_ssl == '0')
                {
                    $spreadSheet->getSheet(1)->getCell('E33')->SetValue('');
                    $spreadSheet->getSheet(1)->getCell('F33')->SetValue('');
                    $spreadSheet->getSheet(1)->getCell('G33')->SetValue('');
                }
                $spreadSheet->getSheet(0)->getCell('B40')->SetValue($this->data->p1_hss);
                $spreadSheet->getSheet(0)->getCell('B41')->SetValue($this->data->p1_ssl);
                if ($this->data->p1_ssl == '0')
                {
                    $spreadSheet->getSheet(1)->getCell('E34')->SetValue('');
                    $spreadSheet->getSheet(1)->getCell('F34')->SetValue('');
                    $spreadSheet->getSheet(1)->getCell('G34')->SetValue('');
                }
                $spreadSheet->getSheet(0)->getCell('B32')->SetValue($this->data->p2_hss);
                $spreadSheet->getSheet(0)->getCell('B33')->SetValue($this->data->p2_ssl);
                if ($this->data->p2_ssl == '0')
                {
                    $spreadSheet->getSheet(1)->getCell('E35')->SetValue('');
                    $spreadSheet->getSheet(1)->getCell('F35')->SetValue('');
                    $spreadSheet->getSheet(1)->getCell('G35')->SetValue('');
                }
                $spreadSheet->getSheet(0)->getCell('B42')->SetValue($this->data->p2_tlpint_hss);
                $spreadSheet->getSheet(0)->getCell('B43')->SetValue($this->data->p2_tlpint_ssl);
                if ($this->data->p2_tlpint_ssl == '0')
                {
                    $spreadSheet->getSheet(1)->getCell('E36')->SetValue('');
                    $spreadSheet->getSheet(1)->getCell('F36')->SetValue('');
                    $spreadSheet->getSheet(1)->getCell('G36')->SetValue('');
                }
                $spreadSheet->getSheet(0)->getCell('B44')->SetValue($this->data->p2_intiptv_hss);
                $spreadSheet->getSheet(0)->getCell('B45')->SetValue($this->data->p2_intiptv_ssl);
                if ($this->data->p2_intiptv_ssl == '0')
                {
                    $spreadSheet->getSheet(1)->getCell('E37')->SetValue('');
                    $spreadSheet->getSheet(1)->getCell('F37')->SetValue('');
                    $spreadSheet->getSheet(1)->getCell('G37')->SetValue('');
                }
                $spreadSheet->getSheet(0)->getCell('B46')->SetValue($this->data->p3_hss);
                $spreadSheet->getSheet(0)->getCell('B47')->SetValue($this->data->p3_ssl);
                if ($this->data->p3_ssl == '0')
                {
                    $spreadSheet->getSheet(1)->getCell('E38')->SetValue('');
                    $spreadSheet->getSheet(1)->getCell('F38')->SetValue('');
                    $spreadSheet->getSheet(1)->getCell('G38')->SetValue('');
                }
                $spreadSheet->getSheet(0)->getCell('B63')->SetValue($this->data->migrasi_service_1p2p_hss);
                $spreadSheet->getSheet(0)->getCell('B64')->SetValue($this->data->migrasi_service_1p2p_ssl);
                if ($this->data->migrasi_service_1p2p_ssl == '0')
                {
                    $spreadSheet->getSheet(1)->getCell('E57')->SetValue('');
                    $spreadSheet->getSheet(1)->getCell('F57')->SetValue('');
                    $spreadSheet->getSheet(1)->getCell('G57')->SetValue('');
                }
                $spreadSheet->getSheet(0)->getCell('B65')->SetValue($this->data->migrasi_service_1p3p_hss);
                $spreadSheet->getSheet(0)->getCell('B66')->SetValue($this->data->migrasi_service_1p3p_ssl);
                if ($this->data->migrasi_service_1p3p_ssl == '0')
                {
                    $spreadSheet->getSheet(1)->getCell('E58')->SetValue('');
                    $spreadSheet->getSheet(1)->getCell('F58')->SetValue('');
                    $spreadSheet->getSheet(1)->getCell('G58')->SetValue('');
                }
                $spreadSheet->getSheet(0)->getCell('B67')->SetValue($this->data->migrasi_service_2p3p_hss);
                $spreadSheet->getSheet(0)->getCell('B68')->SetValue($this->data->migrasi_service_2p3p_ssl);
                if ($this->data->migrasi_service_2p3p_ssl == '0')
                {
                    $spreadSheet->getSheet(1)->getCell('E59')->SetValue('');
                    $spreadSheet->getSheet(1)->getCell('F59')->SetValue('');
                    $spreadSheet->getSheet(1)->getCell('G59')->SetValue('');
                }
                $spreadSheet->getSheet(0)->getCell('B57')->SetValue($this->data->lme_wifi_pt1_hss);
                $spreadSheet->getSheet(0)->getCell('B58')->SetValue($this->data->lme_wifi_pt1_ssl);
                if ($this->data->lme_wifi_pt1_ssl == '0')
                {
                    $spreadSheet->getSheet(1)->getCell('E63')->SetValue('');
                    $spreadSheet->getSheet(1)->getCell('F63')->SetValue('');
                    $spreadSheet->getSheet(1)->getCell('G63')->SetValue('');
                }
                $spreadSheet->getSheet(0)->getCell('B59')->SetValue($this->data->lme_ap_indoor_hss);
                $spreadSheet->getSheet(0)->getCell('B60')->SetValue($this->data->lme_ap_indoor_ssl);
                if ($this->data->lme_ap_indoor_ssl == '0')
                {
                    $spreadSheet->getSheet(1)->getCell('E64')->SetValue('');
                    $spreadSheet->getSheet(1)->getCell('F64')->SetValue('');
                    $spreadSheet->getSheet(1)->getCell('G64')->SetValue('');
                }
                $spreadSheet->getSheet(0)->getCell('B61')->SetValue($this->data->lme_ap_outdoor_hss);
                $spreadSheet->getSheet(0)->getCell('B62')->SetValue($this->data->lme_ap_outdoor_ssl);
                if ($this->data->lme_ap_outdoor_ssl == '0')
                {
                    $spreadSheet->getSheet(1)->getCell('E65')->SetValue('');
                    $spreadSheet->getSheet(1)->getCell('F65')->SetValue('');
                    $spreadSheet->getSheet(1)->getCell('G65')->SetValue('');
                }
                $spreadSheet->getSheet(0)->getCell('B69')->SetValue($this->data->indihome_smart_hss);
                $spreadSheet->getSheet(0)->getCell('B70')->SetValue($this->data->indihome_smart_ssl);
                if ($this->data->indihome_smart_ssl == '0')
                {
                    $spreadSheet->getSheet(1)->getCell('E67')->SetValue('');
                    $spreadSheet->getSheet(1)->getCell('F67')->SetValue('');
                    $spreadSheet->getSheet(1)->getCell('G67')->SetValue('');
                }
                $spreadSheet->getSheet(0)->getCell('B71')->SetValue($this->data->plc_hss);
                $spreadSheet->getSheet(0)->getCell('B72')->SetValue($this->data->plc_ssl);
                if ($this->data->plc_ssl == '0')
                {
                    $spreadSheet->getSheet(1)->getCell('E70')->SetValue('');
                    $spreadSheet->getSheet(1)->getCell('F70')->SetValue('');
                    $spreadSheet->getSheet(1)->getCell('G70')->SetValue('');
                }
                $spreadSheet->getSheet(0)->getCell('B48')->SetValue($this->data->ont_premium_hss);
                $spreadSheet->getSheet(0)->getCell('B49')->SetValue($this->data->ont_premium_ssl);
                if ($this->data->ont_premium_ssl == '0')
                {
                    $spreadSheet->getSheet(1)->getCell('E71')->SetValue('');
                    $spreadSheet->getSheet(1)->getCell('F71')->SetValue('');
                    $spreadSheet->getSheet(1)->getCell('G71')->SetValue('');
                }
                $spreadSheet->getSheet(0)->getCell('B50')->SetValue($this->data->ikr_addon_stb_hss);
                $spreadSheet->getSheet(0)->getCell('B51')->SetValue($this->data->ikr_addon_stb_ssl);
                if ($this->data->ikr_addon_stb_ssl == '0')
                {
                    $spreadSheet->getSheet(1)->getCell('E72')->SetValue('');
                    $spreadSheet->getSheet(1)->getCell('F72')->SetValue('');
                    $spreadSheet->getSheet(1)->getCell('G72')->SetValue('');
                }
                $spreadSheet->getSheet(0)->getCell('B75')->SetValue($this->data->change_stb_hss);
                $spreadSheet->getSheet(0)->getCell('B76')->SetValue($this->data->change_stb_ssl);
                if ($this->data->change_stb_ssl == '0')
                {
                    $spreadSheet->getSheet(1)->getCell('E73')->SetValue('');
                    $spreadSheet->getSheet(1)->getCell('F73')->SetValue('');
                    $spreadSheet->getSheet(1)->getCell('G73')->SetValue('');
                }
                $spreadSheet->getSheet(0)->getCell('B52')->SetValue($this->data->wifiextender_hss);
                $spreadSheet->getSheet(0)->getCell('B53')->SetValue($this->data->wifiextender_ssl);
                if ($this->data->wifiextender_ssl == '0')
                {
                    $spreadSheet->getSheet(1)->getCell('E74')->SetValue('');
                    $spreadSheet->getSheet(1)->getCell('F74')->SetValue('');
                    $spreadSheet->getSheet(1)->getCell('G74')->SetValue('');
                }
                $spreadSheet->getSheet(0)->getCell('B54')->SetValue($this->data->pu_s7_140_hss);
                $spreadSheet->getSheet(0)->getCell('B55')->SetValue($this->data->pu_s7_140_ssl);
                if ($this->data->pu_s7_140_ssl == '0')
                {
                    $spreadSheet->getSheet(1)->getCell('E78')->SetValue('');
                    $spreadSheet->getSheet(1)->getCell('F78')->SetValue('');
                    $spreadSheet->getSheet(1)->getCell('G78')->SetValue('');
                }

                $spreadSheet->getSheet(0)->getCell('B73')->SetValue($this->data->pu_s9_140_hss);
                $spreadSheet->getSheet(0)->getCell('B74')->SetValue($this->data->pu_s9_140_ssl);
                if ($this->data->pu_s9_140_ssl == '0')
                {
                    $spreadSheet->getSheet(1)->getCell('E79')->SetValue('');
                    $spreadSheet->getSheet(1)->getCell('F79')->SetValue('');
                    $spreadSheet->getSheet(1)->getCell('G79')->SetValue('');
                }

                $spreadSheet->getSheet(0)->getCell('B56')->SetValue($this->data->persentase_7_kpi);



                $writer = IOFactory::createWriter($spreadSheet, 'Xlsx');

                switch ($this->output_file)
                {
                    case 'save':

                            $path = public_path() . "/upload2/" . $this->data->id . '/';

                            if (!file_exists($path))
                            {
                                if (!mkdir($path, 0777, true))
                                {
                                    return 'gagal menyiapkan folder rekon';
                                }
                            }

                            $writer->save($path."Surat_Pesanan_PSB_".str_replace(" ", "_", $this->data->nama_company).".xlsx");
                        break;
                    
                    default:
                            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                            header('Content-Disposition: attachment;filename="Surat_Pesanan_PSB_'.str_replace(" ", "_", $this->data->nama_company).'.xlsx"');

                            $writer->save('php://output');
                            die;
                        break;
                }
            }
        ];
    }
}