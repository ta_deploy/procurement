<?php

namespace App\Http\Controllers;

use App\DA\AdminModel;
use App\DA\ToolsModel;
use App\DA\ReportModel;
use App\DA\MitraModel;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use DB;

date_default_timezone_set("Asia/Makassar");
class ReportController extends Controller
{
  //TODO: fight me
  protected $spreadsheetId = '1X20LKIrFP8JSVJKisRZr_uNKksr6_77McBJBsJUGB8k';

  // public function list_pks()
  // {
  //   $data = ReportModel::get_list_pks();
  //   return view('Report.Area.list_pks', compact('data'));
  // }

  // public function input_pks()
  // {
  //   return view('Area.Rekon.edit_pks');
  // }

  // public function save_pks(Request $req)
  // {
  //   ReportModel::update_pks($req);
  //   return redirect('/Report/pks/list')->with('alerts', [
  //     ['type' => 'success', 'text' => 'Data PKS berhasil diperbaharui!']
  //   ]);
  // }

  // public function update_pks(Request $req, $id)
  // {
  //  ReportModel::update_pks($req, $id);
  //   return redirect('/Report/pks/list')->with('alerts', [
  //     ['type' => 'success', 'text' => 'Data PKS berhasil diperbaharui!']
  //   ]);
  // }

  // public function edit_pks($id)
  // {
  //   $data = ReportModel::get_pkd($id);
  //   return view('Area.Rekon.edit_pks', compact('data'));
  // }

  public function rfc_get($m1, $m2)
  {
    $data = ReportModel::rfc_rekap($m1, $m2);
    return view('Report.material_rfc', compact('data', 'm1', 'm2') );
  }

  public function show_material()
  {
    $data = AdminModel::get_design(2);
    return view('Report.material', ['data' => $data]);
  }

  public function input_material()
  {
    $kerjaan = ToolsModel::list_pekerjaan();
    $mitra   = AdminModel::get_mitra(16);
    return view('Area.material_input', compact('kerjaan', 'mitra') );
  }

  public function save_material(Request $req)
  {
    ReportModel::save_material($req);
    return redirect('/Report/data/material')->with('alerts', [
      ['type' => 'success', 'text' => 'Data KHS '.$req->pekerjaan.' berhasil diperbaharui!']
    ]);
  }

  public function delete_material($id)
  {
    $material = AdminModel::get_design(1, $id);
    ReportModel::delete_material($id);
    return redirect('/Report/data/material')->with('alerts', [
      ['type' => 'success', 'text' => 'Material '.$material->designator.' Berhasil Dihapus!']
    ]);
  }

  public function tes_bulk()
  {
    // $qr = DB::select("UPDATE procurement_boq_upload pbu
    // LEFT JOIN procurement_req_pid prp ON prp.id_upload = pbu.id
    // LEFT JOIN procurement_pid pp ON pp.id = prp.id_pid_req
    // SET pp.budget_exceeded = 1, pp.budget = 0
    // WHERE judul IN ( 'PENGADAAN PEKERJAAN QE RELOKASI TBN PLE 07 2021 LOKASI BANJARMASIN WITEL KALSEL',
    // 'PENGADAAN PEKERJAAN QE RECOVERY TBN PLE & BTB 08 2021 LOKASI BANJARMASIN WITEL KALSEL',
    // 'PENGADAAN PEKERJAAN QE RELOKASI MAP KDG 03 2021 LOKASI BANJARMASIN WITEL KALSEL',
    // 'PENGADAAN PEKERJAAN QE RECOVERY MAP KDG 03 2021 LOKASI BANJARMASIN WITEL KALSEL',
    // 'PENGADAAN PEKERJAAN QE RECOVERY MAP KDG 04 2021 LOKASI BANJARMASIN WITEL KALSEL',
    // 'PENGADAAN PEKERJAAN QE RELOKASI MAP KDG 04 2021 LOKASI BANJARMASIN WITEL KALSEL',
    // 'PENGADAAN PEKERJAAN QE RECOVERY MAP KDG 05 2021 LOKASI BANJARMASIN WITEL KALSEL',
    // 'PENGADAAN PEKERJAAN QE RELOKASI MAP KDG 05 2021 LOKASI BANJARMASIN WITEL KALSEL',
    // 'PENGADAAN PEKERJAAN QE RECOVERY MAP KDG 06 2021 LOKASI BANJARMASIN WITEL KALSEL',
    // 'PENGADAAN PEKERJAAN QE RECOVERY MAP RTA, KDG, BBR & LUL 07 2021 LOKASI BANJARMASIN WITEL KALSEL',
    // 'PENGADAAN PEKERJAAN QE RELOKASI MAP RTA, KDG, BBR & LUL 07 2021 LOKASI BANJARMASIN WITEL KALSEL',
    // 'PENGADAAN PEKERJAAN QE RECOVERY MAP BBR, KDG & BRI 08 2021 LOKASI BANJARMASIN WITEL KALSEL',
    // 'PENGADAAN PEKERJAAN QE RELOKASI AGB BJM-PLE 03 2021 LOKASI BANJARMASIN WITEL KALSEL',
    // 'PENGADAAN PEKERJAAN QE RECOVERY AGB BBR-PLE 03 2021 LOKASI BANJARMASIN WITEL KALSEL',
    // 'PENGADAAN PEKERJAAN QE RELOKASI AGB BJM-PLE 04 2021 LOKASI BANJARMASIN WITEL KALSEL',
    // 'PENGADAAN PEKERJAAN QE RELOKASI AGB BJM-PLE 05 2021 LOKASI BANJARMASIN WITEL KALSEL',
    // 'PENGADAAN PEKERJAAN QE RELOKASI AGB BJM, BBR & PLE 06 2021 LOKASI BANJARMASIN WITEL KALSEL',
    // 'PENGADAAN PEKERJAAN QE RECOVERY AGB BJM 07 2021 LOKASI BANJARMASIN WITEL KALSEL',
    // 'PENGADAAN PEKERJAAN QE RELOKASI AGB BJM 07 2021 LOKASI BANJARMASIN WITEL KALSEL',
    // 'PENGADAAN PEKERJAAN QE RECOVERY AGB BJM & BBR 08 2021 LOKASI BANJARMASIN WITEL KALSEL')");
  }

  // public function show_gd()
  // {
  //   $client = new \Google_Client();
  //   $client->setApplicationName('My PHP App');
  //   $client->setScopes([\Google_Service_Sheets::SPREADSHEETS]);
  //   $client->setAccessType('offline');
  //   $client->setAuthConfig('/srv/htdocs/procurement_dev3/public/google.json');

  //   $sheets = new \Google_Service_Sheets($client);
  //   $sheet = $sheets->spreadsheets->get($this->spreadsheetId)->getSheets();
  //   foreach ($sheet as $sheetl) {
  //     $list[] = $sheetl->properties->title;
  //   }
  //   $list_sheet = array_values(preg_grep('/^TAGIHAN (?!2018)/i', $list));
  //   // dd('tes');
  //   return view('Report.ExcelGs.gd_list', ['list_sheet' => $list_sheet]);
  // }

  public function result_gd(Request $req)
  {
    return json_encode($this->get_table_gd('table', $req->id));
  }

  public function get_table_gd($opt, $range, $filter = [])
  {
    $client = new \Google_Client();
    $client->setApplicationName('My PHP App');
    $client->setScopes([\Google_Service_Sheets::SPREADSHEETS]);
    $client->setAccessType('offline');
    $client->setAuthConfig('/srv/htdocs/procurement_dev3/public/google.json');

    $sheets = new \Google_Service_Sheets($client);

    $headers = $sheets->spreadsheets_values->get($this->spreadsheetId, $range . '!A1:AG1', ['majorDimension' => 'ROWS']);
    $header = array_map(function ($x) {
      $x = strtolower($x);
      $x = ltrim($x);
      $x = ucwords($x);
      return $x;
    }, $headers->values[0]);
    $rows = $sheets->spreadsheets_values->get($this->spreadsheetId, $range . '!A2:AG', ['majorDimension' => 'ROWS']);
    $new_r = [];
    $main_d = [];
    $header[0] = 'No';
    // dd($header);
    if (isset($rows['values'])) {
      foreach ($rows['values'] as $key => $row) {
        if (isset($row[1])) {
          $data = strtolower($row[1]);
          $row[1] = ucfirst($data);
        }

        $count = count($row);
        if ($count < 32) {
          for ($i = 0; $i < (32 - $count); $i++) {
            array_push($row, '');
          }
        }
        // dd($filter);
        if ($opt == 'ftable') {
          if (count($filter) != 0) {
            foreach ($filter as $f) {
              if ($row[0] == $f) {
                $new_r[] = $row;
              }
            }
          }
        }
        // dd($row);
        if ($opt == 'table') {
          // dd($header, $row);
          foreach ($row as $m_k => $data) {
            foreach ($header as $key => $h) {
              if ($m_k == $key & ($h == "Nama Mitra" || $h == "Posisi Tagihan" || $h == "No" || $h == "Witel" || $h == "Status Pembayaran")) {
                $main_d[$h] = $data;
              }
            }
          }
          if ($main_d['Posisi Tagihan'] != '') {
            $final_data[] = $main_d;
          }
        }
      }

      if ($opt == 'ftable') {
        // dd($new_r);
        foreach ($new_r as $metadata) {
          foreach ($metadata as $m_k => $data) {
            foreach ($header as $key => $h) {
              if ($m_k == $key) {
                $main_d[$h] = $data;
              }
            }
          }
          $final_data[] = $main_d;
        }
      }
    }
    // dd($final_data);
    return $final_data;
  }

  public function ajax_list_full_gd(Request $req)
  {
    // dd($req->all());
    $data = $this->get_table_gd('ftable', $req->id, $req->no);
    return json_encode($data);
  }

  public function get_list_tagihan2020()
  {
    $client = new \Google_Client();
    $client->setApplicationName('My PHP App');
    $client->setScopes([\Google_Service_Sheets::SPREADSHEETS]);
    $client->setAccessType('offline');
    $client->setAuthConfig('/srv/htdocs/procurement_dev3/public/google.json');

    $spreadsheetId_tagihan = '1X20LKIrFP8JSVJKisRZr_uNKksr6_77McBJBsJUGB8k';
    $sheets = new \Google_Service_Sheets($client);
    $sheet = $sheets->spreadsheets->get($spreadsheetId_tagihan)->getSheets();

    $rows = $sheets->spreadsheets_values->get($spreadsheetId_tagihan, 'TAGIHAN 2020', ['majorDimension' => 'ROWS']);

    $rows_dt_raw = $sheets->spreadsheets_values->get($spreadsheetId_tagihan, 'TAGIHAN 2020', ['majorDimension' => 'ROWS', 'valueRenderOption' => 'UNFORMATTED_VALUE']);

    unset($rows->values[0]);
    unset($rows_dt_raw->values[0]);

    foreach($rows->values as $key => $val){
      if(count($val) >= 20 && isset($val[1]) && isset($val[1]) && preg_match('/(?i)Banjarmasin/', $val[1])){
        $data[$key] = $val;
      }
    }

    foreach($rows_dt_raw->values as $key => $val){
      if(count($val) >= 20 && isset($val[1]) && isset($val[1]) && preg_match('/(?i)Banjarmasin/', $val[1])){
        $data_raw[$key] = $val;
      }
    }

    //search regex like if(isset($val[1]) && preg_match('/(?i)Banjarmasin/', $val[1])){

    foreach($data_raw as $key => $val){
      //umur TL
      // ambil hasil toc + sp
      $msg_toc = 'Tidak Ada TOC';

        if (array_key_exists(7, $val) == TRUE && $val[7] != '' ){
          $toc = $val[7];
          $sp = array_key_exists(6, $val) == TRUE && $val[6] != '' ? gmdate("Y-m-d", (25569 + (($val[6] - 25569) * 86400 / 86400) - 25569) * 86400) : date('Y-m-d');
          $tagihan_mit = array_key_exists(22, $val) == TRUE && $val[22] != '' ? gmdate("Y-m-d", (25569 + (($val[22] - 25569) * 86400 / 86400) - 25569) * 86400) : date('Y-m-d');
          $toc1 = $toc + 1;
          $expected_output = date('Y-m-d', strtotime($sp." ".$toc1 ." days"));
          $datediff = strtotime($tagihan_mit) - strtotime($tagihan_mit > $expected_output ? $expected_output : $sp);
          $distance =  round($datediff / (60 * 60 * 24));
          $msg_toc = $tagihan_mit.' ('.$distance.') Hari';
        }

      $data[$key]['umur_tl'] = $msg_toc;

      //umur tagihan procurement area
      $date1 = array_key_exists(22, $val) == TRUE && $val[22] != '' ? strtotime(gmdate("Y-m-d", (25569 + (($val[22] - 25569) * 86400 / 86400) - 25569) * 86400)) : null;
      $msg_procar = 'Tanggal Procurement Area Belum Diinput!';

      if($date1) {
        $date2 = array_key_exists(27, $val) == TRUE && $val[27] != '' ? strtotime(gmdate("Y-m-d", (25569 + (($val[27] - 25569) * 86400 / 86400) - 25569) * 86400)) : time();
        $datediff =  $date2 - $date1;
        $tag_proc_mit = round($datediff / (60 * 60 * 24));

        if(array_key_exists(27, $val) == TRUE && $val[27] != ''){
          $msg_procar = gmdate("Y-m-d", (25569 + (($val[27] - 25569) * 86400 / 86400) - 25569) * 86400).' ('.$tag_proc_mit.') Hari';
        } else {
          $msg_procar = $tag_proc_mit.' Hari';
        }
      }

      $data[$key]['umur_proc_area'] = $msg_procar;

      //umur tagihan procurement req
      $date1 = array_key_exists(27, $val) == TRUE && $val[27] != '' ? strtotime(gmdate("Y-m-d", (25569 + (($val[27] - 25569) * 86400 / 86400) - 25569) * 86400)) : null;
      $msg_procreq = 'Tanggal Procurement Req Belum Diinput!';

      if($date1) {
        $date2 = array_key_exists(31, $val) == TRUE && $val[31] != '' ? strtotime(gmdate("Y-m-d", (25569 + (($val[31] - 25569) * 86400 / 86400) - 25569) * 86400)) : time();
        $datediff =  $date2 - $date1;
        $tag_proc_req = round($datediff / (60 * 60 * 24));

        if(array_key_exists(31, $val) == TRUE && $val[31] != ''){
          $msg_procreq = gmdate("Y-m-d", (25569 + (($val[31] - 25569) * 86400 / 86400) - 25569) * 86400).' ('.$tag_proc_req.') Hari';
        } else {
          $msg_procreq = $tag_proc_req.' Hari';
        }
      }

      $data[$key]['umur_proc_req'] = $msg_procreq;

      //umur tagihan finance
      $date1 = array_key_exists(31, $val) == TRUE && $val[31] != '' ? strtotime(gmdate("Y-m-d", (25569 + (($val[31] - 25569) * 86400 / 86400) - 25569) * 86400)) : null;
      $msg_fin = 'Tanggal Tagihan Finance Belum Diinput!';

      if($date1) {
        $date2 = array_key_exists(38, $val) == TRUE && $val[38] != '' ? strtotime(gmdate("Y-m-d", (25569 + (($val[38] - 25569) * 86400 / 86400) - 25569) * 86400)) : time();
        $datediff =  $date2 - $date1;
        $tag_fin = round($datediff / (60 * 60 * 24));

        if(array_key_exists(38, $val) == TRUE && $val[38] != ''){
          $msg_fin = gmdate("Y-m-d", (25569 + (($val[38] - 25569) * 86400 / 86400) - 25569) * 86400).' ('.$tag_fin.') Hari';
        } else {
          $msg_fin = $tag_fin.' Hari';
        }
      }

      $data[$key]['umur_fin'] = $msg_fin;

    }
    return view('Report.ExcelGs.list_tagihan2020', compact('data'));
  }

  public function add_catatan($id)
  {
    $client = new \Google_Client();
    $client->setApplicationName('My PHP App');
    $client->setScopes([\Google_Service_Sheets::SPREADSHEETS]);
    $client->setAccessType('offline');
    $client->setAuthConfig('/srv/htdocs/procurement_dev3/public/google.json');

    $spreadsheetId = '1X20LKIrFP8JSVJKisRZr_uNKksr6_77McBJBsJUGB8k';

    $sheets = new \Google_Service_Sheets($client);
    $sheet = $sheets->spreadsheets->get($spreadsheetId)->getSheets();

    $rows_dt = $sheets->spreadsheets_values->get($spreadsheetId, 'TAGIHAN 2020', ['majorDimension' => 'ROWS']);
    $data = $rows_dt->values[$id];

    if (isset($data[1]) && preg_match('/(?i)Banjarmasin/', $data[1])) {
        return view('Area.ExcelGs.input_catatan_tghn', compact('data'));
    }
    else
    {
      dd('Bukan area Kita!');
    }
  }

  public function copas_datagihan(Request $req, $id)
  {
    $this->paste_tagihan_data($req, $id);
    return back()->with('alerts', [
        ['type' => 'success', 'text' => 'Berhasil Ditambahkan ke '.$req->chk_sheet.'!']
    ]);
  }

  public function update_tagihan(Request $req, $id)
  {
    $client = new \Google_Client();
    $client->setApplicationName('My PHP App');
    $client->setScopes([\Google_Service_Sheets::SPREADSHEETS]);
    $client->setAccessType('offline');
    $client->setAuthConfig('/srv/htdocs/procurement_dev3/public/google.json');

    $spreadsheetId = '1X20LKIrFP8JSVJKisRZr_uNKksr6_77McBJBsJUGB8k';
    $sheets = new \Google_Service_Sheets($client);
    $sheet = $sheets->spreadsheets->get($spreadsheetId)->getSheets();

    $rows = $sheets->spreadsheets_values->get($spreadsheetId, 'TAGIHAN 2020', ['majorDimension' => 'ROWS']);

    $las_data = end($rows->values);
    $no = $las_data[0] + 1;

    $get_all_row = $sheets->spreadsheets->get($spreadsheetId, ['ranges' => 'TAGIHAN 2020'])->sheets[0]->properties->gridProperties->rowCount;

    if($get_all_row < 1 + count($rows->values)) {
      $body_inst = new \Google_Service_Sheets_BatchUpdateSpreadsheetRequest([
        "requests" => [
          "insertDimension" => [
            "range" => [
              "sheetId" => 931424936,
              "dimension" => "ROWS",
              "startIndex"=> $get_all_row,
              "endIndex"=> 1 + count($rows->values)
            ],
            "inheritFromBefore" => true
          ]
        ]
      ]);
      $sheets->spreadsheets->batchUpdate($spreadsheetId, $body_inst);
    }

    $body_border = new \Google_Service_Sheets_BatchUpdateSpreadsheetRequest([
        "requests" => [
          "updateBorders" => [
            "range" => [
              "sheetId" => 931424936,
              "startRowIndex"=> count($rows->values),
              "endRowIndex"=> count($rows->values) + 1,
              "startColumnIndex"=> 0,
              "endColumnIndex"=> 40
            ],
            "top" => [
              "style" => "SOLID",
              "width" => 1,
            ],
            "bottom" => [
              "style" => "SOLID",
              "width" => 1,
            ],
            "left" => [
              "style" => "SOLID",
              "width" => 1,
            ],
            "right" => [
              "style" => "SOLID",
              "width" => 1,
            ],
            "innerHorizontal" => [
              "style" => "SOLID",
              "width" => 1,
            ],
            "innerVertical" => [
              "style" => "SOLID",
              "width" => 1,
            ]
          ]
        ]
      ]);
    //XXX: insert latest data
      $no = $id + 1;
      $data_booking[] = new \Google_Service_Sheets_ValueRange([
        'range' => 'TAGIHAN 2020!A'.$no,
        'values' => [
          [
            $no,
            'Banjarmasin',
            $req->p_kerja ?? '',
            $req->jus_keb ?? '',
            $req->mitra_select ?? '',
            $req->no_surat_p ?? '',
            $req->date_sp ?? '',
            $req->toc ?? '',
            $req->uraian_p ?? '',
            $req->nilai_sp ?? '',
            $req->nilai_rek ?? '',
            $req->pic_pr ?? '',
            $req->no_pic_pr ?? '',
            $req->date_input_pr ?? '',
            $req->pic_po ?? '',
            $req->no_pic_po ?? '',
            $req->date_input_po ?? '',
            $req->no_gr_sap ?? '',
            $req->date_input_gr ?? '',
            $req->date_gr ?? '',
            $req->job_stts ?? '',
            $req->posisi_tgh ?? '',
            $req->date_terima_tag_mit ?? '',
            $req->no_berkas ?? '',
            $req->proc_area ?? '',
            $req->area_ket ?? '',
            $req->area_posisi ?? '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            $req->pembayaran ?? '',
            $req->no_apm ?? '',
            $req->date_account ?? '',
            $req->date_byr ?? '',
            $req->myta ?? '',
          ],
        ]
      ]);
      $date_arra = [6, 13, 16, 18, 19, 22, 37, 38];

      foreach($data_booking[0]->values[0] as $k => $dta){
        // if(in_array($k, $date_arra)) {
        //   $body_form = new \Google_Service_Sheets_BatchUpdateSpreadsheetRequest([
        //     "requests" => [
        //       "repeatCell" => [
        //         "range" => [
        //           "sheetId" => 107036219,
        //           "startRowIndex"=> count($rows->values),
        //           "endRowIndex"=> count($rows->values) + 1,
        //           "startColumnIndex"=> $k - 1,
        //           "endColumnIndex"=> $k
        //         ],
        //         "cell" => [
        //           "userEnteredFormat" => [
        //             "numberFormat" => [
        //               "type" => "DATE",
        //               "pattern" => "ddd MMMMM yyyy"
        //             ],
        //             "horizontalAlignment"  => "CENTER",
        //           ]
        //         ],
        //         "fields" => "userEnteredFormat.numberFormat"
        //       ]
        //     ]
        //   ]);
        //   $sheets->spreadsheets->batchUpdate($spreadsheetId, $body_form);
        // }

        if($dta == ''){
          $body_style = new \Google_Service_Sheets_BatchUpdateSpreadsheetRequest([
            "requests" => [
              "repeatCell" => [
                "range" => [
                  "sheetId" => 931424936,
                  "startRowIndex"=> count($rows->values),
                  "endRowIndex"=> count($rows->values) + 1,
                  "startColumnIndex"=> $k - 1,
                  "endColumnIndex"=> $k
                ],
                "cell" => [
                  "userEnteredFormat" => [
                    "backgroundColor" => [
                      "red" => 255 / 255,
                      "green" => 165 / 255,
                      "blue" => 1 / 255
                    ],
                    "horizontalAlignment"  => "CENTER",
                  ]
                ],
                "fields" => "userEnteredFormat(backgroundColor, horizontalAlignment)"
              ]
            ]
          ]);
          $sheets->spreadsheets->batchUpdate($spreadsheetId, $body_style);
        }
      }

      $body = new \Google_Service_Sheets_BatchUpdateValuesRequest([
        'valueInputOption' => 'USER_ENTERED',
        'data' => $data_booking
      ]);

      $sheets->spreadsheets_values->batchUpdate($spreadsheetId, $body);
      $this->paste_tagihan_data($req, $id);

      return redirect('/Report/list/tag2020')->with('alerts', [
          ['type' => 'success', 'text' => 'Berhasil Diperbaharui!']
      ]);
  }

  public function edit_tagihan($id)
  {
    $client = new \Google_Client();
    $client->setApplicationName('My PHP App');
    $client->setScopes([\Google_Service_Sheets::SPREADSHEETS]);
    $client->setAccessType('offline');
    $client->setAuthConfig('/srv/htdocs/procurement_dev3/public/google.json');

    $spreadsheetId = '1X20LKIrFP8JSVJKisRZr_uNKksr6_77McBJBsJUGB8k';

    $sheets = new \Google_Service_Sheets($client);
    $sheet = $sheets->spreadsheets->get($spreadsheetId)->getSheets();

    $rows_dt = $sheets->spreadsheets_values->get($spreadsheetId, 'TAGIHAN 2020', ['majorDimension' => 'ROWS']);

    $rows_dt_raw = $sheets->spreadsheets_values->get($spreadsheetId, 'TAGIHAN 2020', ['majorDimension' => 'ROWS', 'valueRenderOption' => 'UNFORMATTED_VALUE']);

    $data = $rows_dt->values[$id];

    $data_raw = $rows_dt_raw->values[$id];
    // dd($data_raw, $data);
    return view('Area.ExcelGs.edit_tagihan', compact('data', 'data_raw'));
  }

  public function paste_tagihan_data($req, $id)
  {
    $arr_odp = 1;
    $client = new \Google_Client();
    $client->setApplicationName('My PHP App');
    $client->setScopes([\Google_Service_Sheets::SPREADSHEETS]);
    $client->setAccessType('offline');
    $client->setAuthConfig('/srv/htdocs/procurement_dev3/public/google.json');

    $spreadsheetId = '1X20LKIrFP8JSVJKisRZr_uNKksr6_77McBJBsJUGB8k';
    $spreadsheetId_paste = '1ThnOpc8APxxbpARazTKe0UAAMkWWkBvqFGoVoykiljA';

    $sheets = new \Google_Service_Sheets($client);
    $sheet = $sheets->spreadsheets->get($spreadsheetId)->getSheets();

    $list = [
      'IOAN' => 0,
      'WILSUS' => 1460490921,
      'PSB' => 1826842087,
      'Maintenance' => 1558414809,
      'Konstruksi' => 1640700378,
      'SPBU' => 371615515
    ];

    $rows_dt = $sheets->spreadsheets_values->get($spreadsheetId, 'TAGIHAN 2020', ['majorDimension' => 'ROWS']);

    $rows_dt_raw = $sheets->spreadsheets_values->get($spreadsheetId, 'TAGIHAN 2020', ['majorDimension' => 'ROWS', 'valueRenderOption' => 'UNFORMATTED_VALUE']);

    $data_rw = $rows_dt_raw->values[$id];

    $data = $rows_dt->values[$id];

    if ($req->chk_sheet) {
      $rows = $sheets->spreadsheets_values->get($spreadsheetId_paste, $req->chk_sheet, ['majorDimension' => 'ROWS']);

      //cek row
      $get_all_row = $sheets->spreadsheets->get($spreadsheetId_paste, ['ranges' => $req->chk_sheet])->sheets[0]->properties->gridProperties->rowCount;

      if ($get_all_row < $arr_odp + count($rows->values)) {
        $body_inst = new \Google_Service_Sheets_BatchUpdateSpreadsheetRequest([
          "requests" => [
            "insertDimension" => [
              "range" => [
                "sheetId" => $list[$req->chk_sheet],
                "dimension" => "ROWS",
                "startIndex"=> $get_all_row,
                "endIndex"=> $arr_odp + count($rows->values)
              ],
              "inheritFromBefore" => true
            ]
          ]
        ]);
        $sheets->spreadsheets->batchUpdate($spreadsheetId, $body_inst);
      }

      $no = 1;

      $body_border = new \Google_Service_Sheets_BatchUpdateSpreadsheetRequest([
        "requests" => [
          "updateBorders" => [
            "range" => [
              "sheetId" => $list[$req->chk_sheet],
              "startRowIndex"=> count($rows->values),
              "endRowIndex"=> count($rows->values) + 1,
              "startColumnIndex"=> 0,
              "endColumnIndex"=> 19
            ],
            "top" => [
              "style" => "SOLID",
              "width" => 1,
            ],
            "bottom" => [
              "style" => "SOLID",
              "width" => 1,
            ],
            "left" => [
              "style" => "SOLID",
              "width" => 1,
            ],
            "right" => [
              "style" => "SOLID",
              "width" => 1,
            ],
            "innerHorizontal" => [
              "style" => "SOLID",
              "width" => 1,
            ],
            "innerVertical" => [
              "style" => "SOLID",
              "width" => 1,
            ]
          ]
        ]
      ]);

    $sheets->spreadsheets->batchUpdate($spreadsheetId_paste, $body_border);

    // //XXX: warna border
    // $body_style = new \Google_Service_Sheets_BatchUpdateSpreadsheetRequest([
    //   "requests" => [
    //     "repeatCell" => [
    //       "range" => [
    //         "sheetId" => $list[$req->chk_sheet],
    //         "startRowIndex"=> count($rows->values),
    //         "endRowIndex"=> count($rows->values) + 1,
    //         "startColumnIndex"=> 0,
    //         "endColumnIndex"=> 10
    //       ],
    //       "cell" => [
    //         "userEnteredFormat" => [
    //           "backgroundColor" => [
    //             "red" => 246 / 255,
    //             "green" => 178 / 255,
    //             "blue" => 107 / 255
    //           ],
    //           "horizontalAlignment"  => "CENTER",
    //         ]
    //       ],
    //       "fields" => "userEnteredFormat(backgroundColor, horizontalAlignment)"
    //     ]
    //   ]
    // ]);

    // $sheets->spreadsheets->batchUpdate($spreadsheetId_paste, $body_style);

    //XXX: insert latest data
    $row_skrg = count($rows->values)+$no;
    $data_booking[] = new \Google_Service_Sheets_ValueRange([
      'range' => $req->chk_sheet.'!A'.$row_skrg,
      'values' => [
        [
          $data[0],
          $data[1],
          $data[4],
          $data[8],
          $data[2],
          isset($data_rw[6]) && $data_rw[6] != '' ? gmdate("F", (25569 + (($data_rw[6] - 25569) * 86400 / 86400) - 25569) * 86400) : '',
          $data[5],
          $data[9],
          $data[10],
          isset($data_rw[6]) && $data_rw[6] != '' ? gmdate("d/m/Y", (25569 + (($data_rw[6] - 25569) * 86400 / 86400) - 25569) * 86400) : '',
          \Google_Model::NULL_VALUE,
          isset($data_rw[6]) && $data_rw[6] != '' ? gmdate("d/m/Y", (25569 + (($data_rw[6] - 25569) * 86400 / 86400) - 25569) * 86400) : '' ,
          isset($data_rw[22]) && $data_rw[22] != '' ? gmdate("d/m/Y", (25569 + (($data_rw[22] - 25569) * 86400 / 86400) - 25569) * 86400) : '',
          isset($data_rw[26]) && $data_rw[26] != '' ? gmdate("d/m/Y", (25569 + (($data_rw[26] - 25569) * 86400 / 86400) - 25569) * 86400) : '',
          isset($data_rw[30]) && $data_rw[30] != '' ? gmdate("d/m/Y", (25569 + (($data_rw[30] - 25569) * 86400 / 86400) - 25569) * 86400) : '',
          isset($data_rw[37]) && $data_rw[37] != '' ? gmdate("d/m/Y", (25569 + (($data_rw[37] - 25569) * 86400 / 86400) - 25569) * 86400) : '',
          \Google_Model::NULL_VALUE,
          \Google_Model::NULL_VALUE,
          $req->ket_pk ?? \Google_Model::NULL_VALUE,
        ],
      ]
    ]);
    //kalau kosong = {}gapi-php-null
    $body = new \Google_Service_Sheets_BatchUpdateValuesRequest([
      'valueInputOption' => 'USER_ENTERED',
      'data' => $data_booking
    ]);

    $sheets->spreadsheets_values->batchUpdate($spreadsheetId_paste, $body);

    }
  }

  public function input_tag()
  {
    return view('Area.ExcelGs.input_tagihan');
  }

  public function post_tag(Request $req)
  {
    $client = new \Google_Client();
    $client->setApplicationName('My PHP App');
    $client->setScopes([\Google_Service_Sheets::SPREADSHEETS]);
    $client->setAccessType('offline');
    $client->setAuthConfig('/srv/htdocs/procurement_dev3/public/google.json');

    $spreadsheetId = '1X20LKIrFP8JSVJKisRZr_uNKksr6_77McBJBsJUGB8k';
    $sheets = new \Google_Service_Sheets($client);
    $sheet = $sheets->spreadsheets->get($spreadsheetId)->getSheets();

    $rows = $sheets->spreadsheets_values->get($spreadsheetId, 'TAGIHAN 2020', ['majorDimension' => 'ROWS']);

    $las_data = end($rows->values);
    $no = $las_data[0] + 1;

    $get_all_row = $sheets->spreadsheets->get($spreadsheetId, ['ranges' => 'TAGIHAN 2020'])->sheets[0]->properties->gridProperties->rowCount;

    if($get_all_row < 1 + count($rows->values)) {
      $body_inst = new \Google_Service_Sheets_BatchUpdateSpreadsheetRequest([
        "requests" => [
          "insertDimension" => [
            "range" => [
              "sheetId" => 931424936,
              "dimension" => "ROWS",
              "startIndex"=> $get_all_row,
              "endIndex"=> 1 + count($rows->values)
            ],
            "inheritFromBefore" => true
          ]
        ]
      ]);
      $sheets->spreadsheets->batchUpdate($spreadsheetId, $body_inst);
    }

    $body_border = new \Google_Service_Sheets_BatchUpdateSpreadsheetRequest([
      "requests" => [
        "updateBorders" => [
          "range" => [
            "sheetId" => 931424936,
            "startRowIndex"=> count($rows->values),
            "endRowIndex"=> count($rows->values) + 1,
            "startColumnIndex"=> 0,
            "endColumnIndex"=> 40
          ],
          "top" => [
            "style" => "SOLID",
            "width" => 1,
          ],
          "bottom" => [
            "style" => "SOLID",
            "width" => 1,
          ],
          "left" => [
            "style" => "SOLID",
            "width" => 1,
          ],
          "right" => [
            "style" => "SOLID",
            "width" => 1,
          ],
          "innerHorizontal" => [
            "style" => "SOLID",
            "width" => 1,
          ],
          "innerVertical" => [
            "style" => "SOLID",
            "width" => 1,
          ]
        ]
      ]
    ]);
    //XXX: insert latest data

    $row_skrg = count($rows->values)+1;

    $data_booking[] = new \Google_Service_Sheets_ValueRange([
      'range' => 'TAGIHAN 2020!A'.$row_skrg,
      'values' => [
        [
          $no,
          'Banjarmasin',
          $req->p_kerja ?? '',
          $req->jus_keb ?? '',
          $req->mitra_select ?? '',
          $req->no_surat_p ?? '',
          $req->date_sp ?? '',
          $req->toc ?? '',
          $req->uraian_p ?? '',
          $req->nilai_sp ?? '',
          $req->nilai_rek ?? '',
          $req->pic_pr ?? '',
          $req->no_pic_pr ?? '',
          $req->date_input_pr ?? '',
          $req->pic_po ?? '',
          $req->no_pic_po ?? '',
          $req->date_input_po ?? '',
          $req->no_gr_sap ?? '',
          $req->date_input_gr ?? '',
          $req->date_gr ?? '',
          $req->job_stts ?? '',
          $req->posisi_tgh ?? '',
          $req->date_terima_tag_mit ?? '',
          $req->no_berkas ?? '',
          $req->proc_area ?? '',
          $req->area_ket ?? '',
          $req->area_posisi ?? '',
          '',
          '',
          '',
          '',
          '',
          '',
          '',
          '',
          $req->pembayaran ?? '',
          $req->no_apm ?? '',
          $req->date_account ?? '',
          $req->date_byr ?? '',
          $req->myta ?? '',
        ],
      ]
    ]);
    $date_arra = [6, 13, 16, 18, 19, 22, 37, 38];

    foreach($data_booking[0]->values[0] as $k => $dta){
      // if(in_array($k, $date_arra)) {
      //   $body_form = new \Google_Service_Sheets_BatchUpdateSpreadsheetRequest([
      //     "requests" => [
      //       "repeatCell" => [
      //         "range" => [
      //           "sheetId" => 107036219,
      //           "startRowIndex"=> count($rows->values),
      //           "endRowIndex"=> count($rows->values) + 1,
      //           "startColumnIndex"=> $k - 1,
      //           "endColumnIndex"=> $k
      //         ],
      //         "cell" => [
      //           "userEnteredFormat" => [
      //             "numberFormat" => [
      //               "type" => "DATE",
      //               "pattern" => "ddd MMMMM yyyy"
      //             ],
      //             "horizontalAlignment"  => "CENTER",
      //           ]
      //         ],
      //         "fields" => "userEnteredFormat.numberFormat"
      //       ]
      //     ]
      //   ]);
      //   $sheets->spreadsheets->batchUpdate($spreadsheetId, $body_form);
      // }

      if($dta == ''){
        $body_style = new \Google_Service_Sheets_BatchUpdateSpreadsheetRequest([
          "requests" => [
            "repeatCell" => [
              "range" => [
                "sheetId" => 931424936,
                "startRowIndex"=> count($rows->values),
                "endRowIndex"=> count($rows->values) + 1,
                "startColumnIndex"=> $k - 1,
                "endColumnIndex"=> $k
              ],
              "cell" => [
                "userEnteredFormat" => [
                  "backgroundColor" => [
                    "red" => 255 / 255,
                    "green" => 165 / 255,
                    "blue" => 1 / 255
                  ],
                  "horizontalAlignment"  => "CENTER",
                ]
              ],
              "fields" => "userEnteredFormat(backgroundColor, horizontalAlignment)"
            ]
          ]
        ]);
        $sheets->spreadsheets->batchUpdate($spreadsheetId, $body_style);
      }
    }

    $body = new \Google_Service_Sheets_BatchUpdateValuesRequest([
      'valueInputOption' => 'USER_ENTERED',
      'data' => $data_booking
    ]);

    $sheets->spreadsheets_values->batchUpdate($spreadsheetId, $body);
    $this->paste_tagihan_data($req, count($rows->values));

    return redirect('/Report/list/tag2020')->with('alerts', [
        ['type' => 'success', 'text' => 'Berhasil Ditambahkan!']
    ]);
  }

  public function get_list_tagihan2021()
  {
    $client = new \Google_Client();
    $client->setApplicationName('My PHP App');
    $client->setScopes([\Google_Service_Sheets::SPREADSHEETS]);
    $client->setAccessType('offline');
    $client->setAuthConfig('/srv/htdocs/procurement_dev3/public/google.json');

    $spreadsheetId_tagihan = '1tlG2b7Xir6OR53oqrpg3PfCGAdFfzH0ktJuZS2yl2Kc';
    $sheets = new \Google_Service_Sheets($client);
    $sheet = $sheets->spreadsheets->get($spreadsheetId_tagihan)->getSheets();

    $rows = $sheets->spreadsheets_values->get($spreadsheetId_tagihan, 'TAGIHAN 2021', ['majorDimension' => 'ROWS']);

    $rows_dt_raw = $sheets->spreadsheets_values->get($spreadsheetId_tagihan, 'TAGIHAN 2021', ['majorDimension' => 'ROWS', 'valueRenderOption' => 'UNFORMATTED_VALUE']);

    unset($rows->values[0]);
    unset($rows_dt_raw->values[0]);

    foreach($rows->values as $key => $val){
      if(count($val) >= 20 && isset($val[1]) && isset($val[1]) && preg_match('/(?i)Banjarmasin/', $val[1])){
        $data[$key] = $val;
      }
    }

    foreach($rows_dt_raw->values as $key => $val){
      if(count($val) >= 20 && isset($val[1]) && isset($val[1]) && preg_match('/(?i)Banjarmasin/', $val[1])){
        $data_raw[$key] = $val;
      }
    }

    //search regex like if(isset($val[1]) && preg_match('/(?i)Banjarmasin/', $val[1])){

    foreach($data_raw as $key => $val){
      //umur TL
      // ambil hasil toc + sp
      $msg_toc = 'Tidak Ada TOC';

        if (array_key_exists(7, $val) == TRUE && $val[7] != '' ){
          $toc = $val[7];
          $sp = array_key_exists(6, $val) == TRUE && $val[6] != '' ? gmdate("Y-m-d", (25569 + (($val[6] - 25569) * 86400 / 86400) - 25569) * 86400) : date('Y-m-d');
          $tagihan_mit = array_key_exists(22, $val) == TRUE && $val[22] != '' ? gmdate("Y-m-d", (25569 + (($val[22] - 25569) * 86400 / 86400) - 25569) * 86400) : date('Y-m-d');
          $toc1 = $toc + 1;
          $expected_output = date('Y-m-d', strtotime($sp." ".$toc1 ." days"));
          $datediff = strtotime($tagihan_mit) - strtotime($tagihan_mit > $expected_output ? $expected_output : $sp);
          $distance =  round($datediff / (60 * 60 * 24));
          $msg_toc = $tagihan_mit.' ('.$distance.') Hari';
        }

      $data[$key]['umur_tl'] = $msg_toc;

      //umur tagihan procurement area
      $date1 = array_key_exists(22, $val) == TRUE && $val[22] != '' ? strtotime(gmdate("Y-m-d", (25569 + (($val[22] - 25569) * 86400 / 86400) - 25569) * 86400)) : null;
      $msg_procar = 'Tanggal Procurement Area Belum Diinput!';

      if($date1) {
        $date2 = array_key_exists(27, $val) == TRUE && $val[27] != '' ? strtotime(gmdate("Y-m-d", (25569 + (($val[27] - 25569) * 86400 / 86400) - 25569) * 86400)) : time();
        $datediff =  $date2 - $date1;
        $tag_proc_mit = round($datediff / (60 * 60 * 24));

        if(array_key_exists(27, $val) == TRUE && $val[27] != ''){
          $msg_procar = gmdate("Y-m-d", (25569 + (($val[27] - 25569) * 86400 / 86400) - 25569) * 86400).' ('.$tag_proc_mit.') Hari';
        } else {
          $msg_procar = $tag_proc_mit.' Hari';
        }
      }

      $data[$key]['umur_proc_area'] = $msg_procar;

      //umur tagihan procurement req
      $date1 = array_key_exists(27, $val) == TRUE && $val[27] != '' ? strtotime(gmdate("Y-m-d", (25569 + (($val[27] - 25569) * 86400 / 86400) - 25569) * 86400)) : null;
      $msg_procreq = 'Tanggal Procurement Req Belum Diinput!';

      if($date1) {
        $date2 = array_key_exists(31, $val) == TRUE && $val[31] != '' ? strtotime(gmdate("Y-m-d", (25569 + (($val[31] - 25569) * 86400 / 86400) - 25569) * 86400)) : time();
        $datediff =  $date2 - $date1;
        $tag_proc_req = round($datediff / (60 * 60 * 24));

        if(array_key_exists(31, $val) == TRUE && $val[31] != ''){
          $msg_procreq = gmdate("Y-m-d", (25569 + (($val[31] - 25569) * 86400 / 86400) - 25569) * 86400).' ('.$tag_proc_req.') Hari';
        } else {
          $msg_procreq = $tag_proc_req.' Hari';
        }
      }

      $data[$key]['umur_proc_req'] = $msg_procreq;

      //umur tagihan finance
      $date1 = array_key_exists(31, $val) == TRUE && $val[31] != '' ? strtotime(gmdate("Y-m-d", (25569 + (($val[31] - 25569) * 86400 / 86400) - 25569) * 86400)) : null;
      $msg_fin = 'Tanggal Tagihan Finance Belum Diinput!';

      if($date1) {
        $date2 = array_key_exists(38, $val) == TRUE && $val[38] != '' ? strtotime(gmdate("Y-m-d", (25569 + (($val[38] - 25569) * 86400 / 86400) - 25569) * 86400)) : time();
        $datediff =  $date2 - $date1;
        $tag_fin = round($datediff / (60 * 60 * 24));

        if(array_key_exists(38, $val) == TRUE && $val[38] != ''){
          $msg_fin = gmdate("Y-m-d", (25569 + (($val[38] - 25569) * 86400 / 86400) - 25569) * 86400).' ('.$tag_fin.') Hari';
        } else {
          $msg_fin = $tag_fin.' Hari';
        }
      }

      $data[$key]['umur_fin'] = $msg_fin;
    }
    // dd($data);
    return view('Report.ExcelGs.list_tagihan2021', compact('data'));
  }

  public function add_catatan_21($id)
  {
    $client = new \Google_Client();
    $client->setApplicationName('My PHP App');
    $client->setScopes([\Google_Service_Sheets::SPREADSHEETS]);
    $client->setAccessType('offline');
    $client->setAuthConfig('/srv/htdocs/procurement_dev3/public/google.json');

    $spreadsheetId = '1tlG2b7Xir6OR53oqrpg3PfCGAdFfzH0ktJuZS2yl2Kc';

    $sheets = new \Google_Service_Sheets($client);
    $sheet = $sheets->spreadsheets->get($spreadsheetId)->getSheets();

    $rows_dt = $sheets->spreadsheets_values->get($spreadsheetId, 'TAGIHAN 2021', ['majorDimension' => 'ROWS']);
    $data = $rows_dt->values[$id];

    if (isset($data[1]) && preg_match('/(?i)Banjarmasin/', $data[1])) {
        return view('Area.ExcelGs.input_catatan_tghn_21', compact('data'));
    }
    else
    {
      dd('Bukan area Kita!');
    }
  }

  public function copas_datagihan_21(Request $req, $id)
  {
    $this->paste_tagihan_data_21($req, $id);

    return redirect('/Report/list/tag2021')->with('alerts', [
        ['type' => 'success', 'text' => 'Berhasil Ditambahkan!']
    ]);
  }

  public function paste_tagihan_data_21($req, $id)
  {
    $arr_odp = 1;
    $client = new \Google_Client();
    $client->setApplicationName('My PHP App');
    $client->setScopes([\Google_Service_Sheets::SPREADSHEETS]);
    $client->setAccessType('offline');
    $client->setAuthConfig('/srv/htdocs/procurement_dev3/public/google.json');

    $spreadsheetId = '1tlG2b7Xir6OR53oqrpg3PfCGAdFfzH0ktJuZS2yl2Kc';
    $spreadsheetId_paste = '1ThnOpc8APxxbpARazTKe0UAAMkWWkBvqFGoVoykiljA';

    $sheets = new \Google_Service_Sheets($client);
    $sheet = $sheets->spreadsheets->get($spreadsheetId)->getSheets();

    $list = [
      'IOAN' => 0,
      'WILSUS' => 1460490921,
      'PSB' => 1826842087,
      'Maintenance' => 1558414809,
      'Konstruksi' => 1640700378,
      'SPBU' => 371615515
    ];

    $rows_dt = $sheets->spreadsheets_values->get($spreadsheetId, 'TAGIHAN 2021', ['majorDimension' => 'ROWS']);

    $rows_dt_raw = $sheets->spreadsheets_values->get($spreadsheetId, 'TAGIHAN 2021', ['majorDimension' => 'ROWS', 'valueRenderOption' => 'UNFORMATTED_VALUE']);

    // foreach($rows_dt->values as $val1)
    // {
    //   if (isset($val1[1]) && preg_match('/(?i)Banjarmasin/', $val1[1])) {
    //     $rw_data1[] = $val1;
    //   }
    // }

    // foreach($rows_dt_raw->values as $val2)
    // {
    //   if (isset($val2[1]) && preg_match('/(?i)Banjarmasin/', $val2[1])) {
    //     $data_rw[] = $val2;
    //   }
    // }
    // $rw_data1 = array_chunk($rw_data1, 40);

    // foreach ($rw_data1 as $key1 => $rw_data){
    //   foreach ($rw_data as $key => $data) {
    //     if(trim($data[2], ' ') == 'MANAGE SERVICE')
    //     {
    //       $sheetss = 'IOAN';
    //     }
    //     elseif(trim($data[2], ' ') == 'OPTIMA')
    //     {
    //       $sheetss = 'Konstruksi';
    //     }
    //     elseif(trim($data[2], ' ') == 'PROVISIONING')
    //     {
    //       $sheetss = 'PSB';
    //     }
    //     elseif(trim($data[2], ' ') == "IOAN")
    //     {
    //       $sheetss = 'IOAN';
    //     }
    //     elseif(trim($data[2], ' ') == "WILSUS")
    //     {
    //       $sheetss = 'WILSUS';
    //     }
    //     elseif(trim($data[2], ' ') == "PSB")
    //     {
    //       $sheetss = 'PSB';
    //     }
    //     elseif(trim($data[2], ' ') == "MAINTENANCE")
    //     {
    //       $sheetss = 'Maintenance';
    //     }
    //     elseif(trim($data[2], ' ') == "KONSTRUKSI")
    //     {
    //       $sheetss = 'Konstruksi';
    //     }
    //     elseif(trim($data[2], ' ') == "SPBU")
    //     {
    //       $sheetss = 'SPBU';
    //     }

    //     if ($sheetss) {
    //       $rows = $sheets->spreadsheets_values->get($spreadsheetId_paste, $sheetss, ['majorDimension' => 'ROWS']);

    //       //cek row
    //       $get_all_row = $sheets->spreadsheets->get($spreadsheetId_paste, ['ranges' => $sheetss])->sheets[0]->properties->gridProperties->rowCount;

    //       if ($get_all_row < count($arr_odp) + count($rows->values)) {
    //         $body_inst = new \Google_Service_Sheets_BatchUpdateSpreadsheetRequest([
    //           "requests" => [
    //             "insertDimension" => [
    //               "range" => [
    //                 "sheetId" => $list[$sheetss],
    //                 "dimension" => "ROWS",
    //                 "startIndex"=> $get_all_row,
    //                 "endIndex"=> count($arr_odp) + count($rows->values)
    //               ],
    //               "inheritFromBefore" => true
    //             ]
    //           ]
    //         ]);
    //         $sheets->spreadsheets->batchUpdate($spreadsheetId, $body_inst);
    //       }

    //       $no = 1;

    //       $body_border = new \Google_Service_Sheets_BatchUpdateSpreadsheetRequest([
    //       "requests" => [
    //         "updateBorders" => [
    //           "range" => [
    //             "sheetId" => $list[$sheetss],
    //             "startRowIndex"=> count($rows->values),
    //             "endRowIndex"=> count($rows->values) + 1,
    //             "startColumnIndex"=> 0,
    //             "endColumnIndex"=> 19
    //           ],
    //           "top" => [
    //             "style" => "SOLID",
    //             "width" => 1,
    //           ],
    //           "bottom" => [
    //             "style" => "SOLID",
    //             "width" => 1,
    //           ],
    //           "left" => [
    //             "style" => "SOLID",
    //             "width" => 1,
    //           ],
    //           "right" => [
    //             "style" => "SOLID",
    //             "width" => 1,
    //           ],
    //           "innerHorizontal" => [
    //             "style" => "SOLID",
    //             "width" => 1,
    //           ],
    //           "innerVertical" => [
    //             "style" => "SOLID",
    //             "width" => 1,
    //           ]
    //         ]
    //       ]
    //     ]);

    //           $sheets->spreadsheets->batchUpdate($spreadsheetId_paste, $body_border);

    //           //XXX: insert latest data
    //           $row_skrg = count($rows->values)+$no;
    //           $data_booking[] = new \Google_Service_Sheets_ValueRange([
    //       'range' => $sheetss.'!A'.$row_skrg,
    //       'values' => [
    //         [
    //           $data[0],
    //           $data[1],
    //           $data[4],
    //           $data[12],
    //           $data[3],
    //           isset($data_rw[$key][8]) && $data_rw[$key][8] != '' ? gmdate("F", (25569 + (($data_rw[$key][8] - 25569) * 86400 / 86400) - 25569) * 86400) : '',
    //           $data[7],
    //           $data[13],
    //           $data[14],
    //           isset($data_rw[$key][8]) && $data_rw[$key][8] != '' ? gmdate("d/m/Y", (25569 + (($data_rw[$key][8] - 25569) * 86400 / 86400) - 25569) * 86400) : '',
    //           \Google_Model::NULL_VALUE,
    //           isset($data_rw[$key][8]) && $data_rw[$key][8] != '' ? gmdate("d/m/Y", (25569 + (($data_rw[$key][8] - 25569) * 86400 / 86400) - 25569) * 86400) : '',
    //           isset($data_rw[$key][25]) && $data_rw[$key][25] != '' ? gmdate("d/m/Y", (25569 + (($data_rw[$key][25] - 25569) * 86400 / 86400) - 25569) * 86400) : '',
    //           isset($data_rw[$key][30]) && $data_rw[$key][30] != '' ? gmdate("d/m/Y", (25569 + (($data_rw[$key][30] - 25569) * 86400 / 86400) - 25569) * 86400) : '',
    //           isset($data_rw[$key][34]) && $data_rw[$key][34] != '' ? gmdate("d/m/Y", (25569 + (($data_rw[$key][34] - 25569) * 86400 / 86400) - 25569) * 86400) : '',
    //           isset($data_rw[$key][42]) && $data_rw[$key][42] != '' ? gmdate("d/m/Y", (25569 + (($data_rw[$key][42] - 25569) * 86400 / 86400) - 25569) * 86400) : '',
    //           \Google_Model::NULL_VALUE,
    //           \Google_Model::NULL_VALUE,
    //           \Google_Model::NULL_VALUE,
    //         ],
    //       ]
    //     ]);
    //           //kalau kosong = {}gapi-php-null
    //           $body = new \Google_Service_Sheets_BatchUpdateValuesRequest([
    //       'valueInputOption' => 'USER_ENTERED',
    //       'data' => $data_booking
    //     ]);
    //           $sheets->spreadsheets_values->batchUpdate($spreadsheetId_paste, $body);
    //       }
    //       print_r($key.' '.$sheetss."\n");
    //     }
    //     echo $key1;
    //     sleep(100);
    // }

    // dd('try me bitch');

    $data_rw = $rows_dt_raw->values[$id];

    $data = $rows_dt->values[$id];

    if ($req->chk_sheet) {
      $rows = $sheets->spreadsheets_values->get($spreadsheetId_paste, $req->chk_sheet, ['majorDimension' => 'ROWS']);

      //cek row
      $get_all_row = $sheets->spreadsheets->get($spreadsheetId_paste, ['ranges' => $req->chk_sheet])->sheets[0]->properties->gridProperties->rowCount;

      if ($get_all_row < $arr_odp + count($rows->values)) {
        $body_inst = new \Google_Service_Sheets_BatchUpdateSpreadsheetRequest([
          "requests" => [
            "insertDimension" => [
              "range" => [
                "sheetId" => $list[$req->chk_sheet],
                "dimension" => "ROWS",
                "startIndex"=> $get_all_row,
                "endIndex"=> $arr_odp + count($rows->values)
              ],
              "inheritFromBefore" => true
            ]
          ]
        ]);
        $sheets->spreadsheets->batchUpdate($spreadsheetId, $body_inst);
      }

      $no = 1;

      $body_border = new \Google_Service_Sheets_BatchUpdateSpreadsheetRequest([
        "requests" => [
          "updateBorders" => [
            "range" => [
              "sheetId" => $list[$req->chk_sheet],
              "startRowIndex"=> count($rows->values),
              "endRowIndex"=> count($rows->values) + 1,
              "startColumnIndex"=> 0,
              "endColumnIndex"=> 19
            ],
            "top" => [
              "style" => "SOLID",
              "width" => 1,
            ],
            "bottom" => [
              "style" => "SOLID",
              "width" => 1,
            ],
            "left" => [
              "style" => "SOLID",
              "width" => 1,
            ],
            "right" => [
              "style" => "SOLID",
              "width" => 1,
            ],
            "innerHorizontal" => [
              "style" => "SOLID",
              "width" => 1,
            ],
            "innerVertical" => [
              "style" => "SOLID",
              "width" => 1,
            ]
          ]
        ]
      ]);

    $sheets->spreadsheets->batchUpdate($spreadsheetId_paste, $body_border);

    // //XXX: warna border
    // $body_style = new \Google_Service_Sheets_BatchUpdateSpreadsheetRequest([
    //   "requests" => [
    //     "repeatCell" => [
    //       "range" => [
    //         "sheetId" => $list[$req->chk_sheet],
    //         "startRowIndex"=> count($rows->values),
    //         "endRowIndex"=> count($rows->values) + 1,
    //         "startColumnIndex"=> 9,
    //         "endColumnIndex"=> 10
    //       ],
    //       "cell" => [
    //         "userEnteredFormat" => [
    //           "backgroundColor" => [
    //             "red" => 255 / 255,
    //             "green" => 165 / 255,
    //             "blue" => 1 / 255
    //           ],
    //           "horizontalAlignment"  => "CENTER",
    //         ]
    //       ],
    //       "fields" => "userEnteredFormat(backgroundColor, horizontalAlignment)"
    //     ]
    //   ]
    // ]);

    // $sheets->spreadsheets->batchUpdate($spreadsheetId_paste, $body_style);

    //XXX: insert latest data
    $row_skrg = count($rows->values)+$no;
    $data_booking[] = new \Google_Service_Sheets_ValueRange([
      'range' => $req->chk_sheet.'!A'.$row_skrg,
      'values' => [
        [
          $data[0],
          $data[1],
          $data[4],
          $data[12],
          $data[3],
          isset($data_rw[8]) && $data_rw[8] != '' ? gmdate("F", (25569 + (($data_rw[8] - 25569) * 86400 / 86400) - 25569) * 86400) : '',
          $data[7],
          $data[13],
          $data[14],
          isset($data_rw[8]) && $data_rw[8] != '' ? gmdate("d/m/Y", (25569 + (($data_rw[8] - 25569) * 86400 / 86400) - 25569) * 86400) : '',
          \Google_Model::NULL_VALUE,
          isset($data_rw[8]) && $data_rw[8] != '' ? gmdate("d/m/Y", (25569 + (($data_rw[8] - 25569) * 86400 / 86400) - 25569) * 86400) : '',
          isset($data_rw[25]) && $data_rw[25] != '' ? gmdate("d/m/Y", (25569 + (($data_rw[25] - 25569) * 86400 / 86400) - 25569) * 86400) : '',
          isset($data_rw[30]) && $data_rw[30] != '' ? gmdate("d/m/Y", (25569 + (($data_rw[30] - 25569) * 86400 / 86400) - 25569) * 86400) : '',
          isset($data_rw[34]) && $data_rw[34] != '' ? gmdate("d/m/Y", (25569 + (($data_rw[34] - 25569) * 86400 / 86400) - 25569) * 86400) : '',
          isset($data_rw[42]) && $data_rw[42] != '' ? gmdate("d/m/Y", (25569 + (($data_rw[42] - 25569) * 86400 / 86400) - 25569) * 86400) : '',
          \Google_Model::NULL_VALUE,
          \Google_Model::NULL_VALUE,
          $req->ket_pk ?? \Google_Model::NULL_VALUE,
        ],
      ]
    ]);
    //kalau kosong = {}gapi-php-null
    $body = new \Google_Service_Sheets_BatchUpdateValuesRequest([
      'valueInputOption' => 'USER_ENTERED',
      'data' => $data_booking
    ]);

    $sheets->spreadsheets_values->batchUpdate($spreadsheetId_paste, $body);

    }
  }

  // public function show_pra_kon()
  // {
  //   $data = ReportModel::show_pra_kon();
  //   return view('Report.Kontrak.amandemen', ['data' => $data]);
  // }

	public function home(Request $req)
	{
    $table = $table_second = $chart = $log = $tbl = $mitra_modify = $witel = [];

    $mitra = ReportModel::active_mitra();
    foreach($mitra as $k => $v)
    {
      $witel_mod[$v->witel] = $v->witel;
      $mitra_modify[$v->witel][$k]['id']   = $v->id;
      $mitra_modify[$v->witel][$k]['text'] = $v->text;
    }

    $PA = $mitra = $operation = $pr = $commerce = $finance = $total = $finish = 0;

    if($req->all() )
    {
      $table = ReportModel::dashboard($req->witel, $req->tgl_hidden, $req->tgl, $req->mitra, $req->khs);
      $log   = ReportModel::log_rekon($req->witel, $req->tgl_hidden, $req->tgl, $req->mitra, $req->khs);

      if($table)
      {
        foreach($table as $key => $val)
        {
          if(!isset($tbl[$val->witel_new][$val->mitra_nm ?? $val->nama_company]['mitra_nm']) )
          {
            $tbl[$val->witel_new][$val->mitra_nm ?? $val->nama_company]['PA']        = 0;
            $tbl[$val->witel_new][$val->mitra_nm ?? $val->nama_company]['mitra']     = 0;
            $tbl[$val->witel_new][$val->mitra_nm ?? $val->nama_company]['operation'] = 0;
            $tbl[$val->witel_new][$val->mitra_nm ?? $val->nama_company]['pr']        = 0;
            $tbl[$val->witel_new][$val->mitra_nm ?? $val->nama_company]['commerce']  = 0;
            $tbl[$val->witel_new][$val->mitra_nm ?? $val->nama_company]['finance']   = 0;
            $tbl[$val->witel_new][$val->mitra_nm ?? $val->nama_company]['finish']    = 0;
            $tbl[$val->witel_new][$val->mitra_nm ?? $val->nama_company]['total']     = 0;
          }

          $tbl[$val->witel_new][$val->mitra_nm ?? $val->nama_company]['mitra_nm']   = $val->mitra_nm ?? $val->nama_company;
          $tbl[$val->witel_new][$val->mitra_nm ?? $val->nama_company]['PA']        += $val->proca;
          $tbl[$val->witel_new][$val->mitra_nm ?? $val->nama_company]['mitra']     += $val->mitra;
          $tbl[$val->witel_new][$val->mitra_nm ?? $val->nama_company]['operation'] += $val->operation;
          $tbl[$val->witel_new][$val->mitra_nm ?? $val->nama_company]['pr']        += $val->prog;
          $tbl[$val->witel_new][$val->mitra_nm ?? $val->nama_company]['commerce']  += $val->commerce;
          $tbl[$val->witel_new][$val->mitra_nm ?? $val->nama_company]['finance']   += $val->finance;
          $tbl[$val->witel_new][$val->mitra_nm ?? $val->nama_company]['finish']    += $val->finish;
          $tbl[$val->witel_new][$val->mitra_nm ?? $val->nama_company]['total']     += $val->proca + $val->mitra + $val->operation + $val->prog + $val->commerce + $val->finance + $val->finish;
        }

        foreach($table as $key => $val)
        {
          $chart['PA']        = $PA        += $val->proca;
          $chart['mitra']     = $mitra     += $val->mitra;
          $chart['operation'] = $operation += $val->operation;
          $chart['pr']        = $pr        += $val->prog;
          $chart['commerce']  = $commerce  += $val->commerce;
          $chart['finance']   = $finance   += $val->finance;
          $chart['finish']    = $finish    += $val->finish;
          $chart['total']     = $total     += $PA + $mitra + $operation + $pr + $commerce + $finance + $finish;
        }
      }
    }
    $data_pekerjaan = AdminModel::find_work(0, 'active');
    // dd($tbl, $log);
    return view('home', compact('tbl', 'req', 'chart', 'log', 'mitra_modify', 'witel_mod', 'data_pekerjaan') );
	}

  public function history_rekon()
  {
    $data = ReportModel::show_yearly_ps();
    // dd($data);
    $final_res = [];
    foreach($data as $key => $val)
    {
      $final_res[$val->nama_company][] = $val;
    }
    // dd($data, $final_res);
    return view('Report.Rekon.matrix_laporan', compact('final_res'));
  }

  // public function list_sp($id, Request $req)
  // {
  //   $final_data = ReportModel::get_listed_mitra($id, $req->witel, $req->tgl_hidden, $req->tgl, $req->mitra, $req->pekerjaan);
  //   return view('Report.Rekon.listSpAllMitra', compact('final_data'));
  // }

	public function mitra_list_sp($id_m, $id, Request $req)
	{
    $data = ReportModel::get_mitra_sp($id_m, $id, $req->witel, $req->tgl_hidden, $req->tgl, $req->mitra, $req->khs);
    // dd($data);
    return view('Report.Rekon.SpPerMitra', compact('data'));
	}

  public function get_timeline_data($id)
  {
    $data = ReportModel::timeline($id);

    // $data_final = $data_raw = [];
    // $no = 0;

    // foreach($data as $key => $val)
    // {
    //  $data_raw[$key] = $val;

    //  if($val['step'] ==1)
    //  {
    //   $data_raw[$key]['urut'] = ++$no;
    //  }
    //  elseif($val['step'] == 2)
    //  {
    //   $data_raw[$key]['urut'] = $no;
    //  }
    //  else
    //  {
    //   $data_raw[$key]['urut'] = --$no;
    //  }
    // }

    // $data_boq = [];
    // dd($data);
    foreach($data as $key => $val)
    {
      $data_boq[$key]['created_at']  = $val['tgl_start'];
      $data_boq[$key]['created_end'] = $val['tgl_end'];

      if(!empty($val['tgl_start']))
      {
        // $selisih = date_diff(date_create($val['tgl_start']),date_create($val['tgl_end']) );
        $data_boq[$key]['selisih_waktu'] = $val['selisih_waktu'];
      }
      else
      {
        $data_boq[$key]['selisih_waktu'] = null;
      }

      $data_boq[$key]['action']       = $val['action'];
      $data_boq[$key]['urutan']       = $val['urutan'];
      $data_boq[$key]['aktor_id']     = $val['aktor'];
      $data_boq[$key]['aktor_nama']   = $val['aktor_nama'];
      $data_boq[$key]['nama_kerjaan'] = $val['nama'];
      $data_boq[$key]['hari_diff']    = $val['hari_diff'];

      if($val['aktor'] == 2)
      {
        $data_boq[$key]['mitra'] = $val['nama'];
      }else{
        $data_boq[$key]['mitra'] = null;
      }

      if($val['aktor'] == 1)
      {
        $data_boq[$key]['proc_area'] = $val['nama'];
      }
      else
      {
        $data_boq[$key]['proc_area'] = null;
      }

      if($val['aktor'] == 7)
      {
        $data_boq[$key]['commerce'] = $val['nama'];
      }
      else {
        $data_boq[$key]['commerce'] = null;
      }

      if($val['aktor'] == 5)
      {
        $data_boq[$key]['tl'] = $val['nama'];
      }
      else
      {
        $data_boq[$key]['tl'] = null;
      }

      if($val['aktor'] == 6)
      {
        $data_boq[$key]['proc_req'] = $val['nama'];
      }
      else {
        $data_boq[$key]['proc_req'] = null;
      }

      if($val['aktor'] == 3)
      {
        $data_boq[$key]['operation'] = $val['nama'];
      }
      else {
        $data_boq[$key]['operation'] = null;
      }
    }

    return $data_boq;
  }

  public function timeline_sp($id)
  {
    $log      = ReportModel::log_rekon_mitra($id);
    $data_boq = $this->get_timeline_data($id);
    // dd($data_boq, $log);
    return view('Tools.timeline_work', compact('data_boq', 'log') );
  }

	public function dalapac(Request $req)
	{
    $mitra = ReportModel::dalapa_mitra();
    $mitra_modify = $witel = $result = [];

    foreach($mitra as $k => $v)
    {
      $witel[$v->witel] = $v->witel;
      $mitra_modify[$v->witel][$k]['id']   = $v->id;
      $mitra_modify[$v->witel][$k]['text'] = $v->text;
    }

    if($req->all() )
    {
      $result = ReportModel::get_dalapa_data($req);
    }

		return view('Report.Rekon.dalapa_form', compact('mitra_modify', 'witel' ,'req', 'result') );
	}

  public function read_excel_dalapa(Request $req)
  {
    $tes = ReportModel::read_dalapa($req);
  }

  public function dashboard()
  {
    $mitra_modify = $witel = [];
    $data     = ReportModel::getDataDashboard(session('auth')->Witel_New, null, null, null, null, null, null, null, null);
    $tampilan = null;
    $mitra    = ReportModel::active_mitra();

    foreach($mitra as $k => $v)
    {
      $witel_mod[$v->witel] = $v->witel;
      $mitra_modify[$v->witel][$k]['id']   = $v->id;
      $mitra_modify[$v->witel][$k]['text'] = $v->text;
    }

    $data_pekerjaan = AdminModel::find_work(0, 'active');
    return view('Report.Rekon.dashboard', compact('data', 'mitra_modify', 'witel_mod', 'tampilan', 'data_pekerjaan') );
  }

  public function getDashboardData(Request $req)
  {
    if($req->jenis_dashboard == 'Simple')
    {
      $data = ReportModel::getDataDashboard($req->witel, $req->tgl_hidden, $req->tgl, $req->mitra, $req->khs, $req->tampilan, $req->tgl_progress_kerja, $req->jenis_period);
    }
    else
    {
      $data = ReportModel::list_pekerjaan_aktif($req->witel, $req->tgl_hidden, $req->tgl, $req->mitra, $req->khs, $req->tgl_progress_kerja, $req->jenis_period);
    }
    return view('Report.Rekon.dashboardAjax', compact('data'), ['jenis' => $req->jenis_dashboard, 'tampilan' => $req->tampilan, 'req' => $req->all()]);
  }

  public function dashboard_lpa($jenis_period, $witel, $tgl_hidden, $mitra, $khs, $jenis_dashboard, $tgl_progress_kerja)
  {
    // jenis_period=all/witel=All/tgl_hidden=all/mitra=all/khs=All/jenis_dashboard=Detail/tgl_progress_kerja=2022-03-01 - 2022-03-31
    $data = ReportModel::list_pekerjaan_aktif($witel, $tgl_hidden, null, $mitra, $khs, $tgl_progress_kerja, $jenis_period);
    return view('Report.Rekon.dashboard_lpa', compact('data') );
  }

  public function dashboard_simple($jenis_period, $witel, $tgl_hidden, $mitra, $khs, $jenis_dashboard, $tampilan, $tgl_progress_kerja)
  {
    //  jenis_period=all/witel=All/tgl_hidden=all/mitra=all/khs=All/jenis_dashboard=Simple/tampilan=val_sp/tgl_progress_kerja=2022-03-01 - 2022-03-31
    // val_sp
    // price_sp
    // price_rekon
    // val_price_sp
    // val_sp_price_rekon
    // dd($tgl_progress_kerja);
    $data = ReportModel::getDataDashboard($witel, $tgl_hidden, null, $mitra, $khs, $tampilan, $tgl_progress_kerja, $jenis_period);
    return view('Report.Rekon.dashboard_simple', compact('data', 'tampilan') );
  }

  public function telegram_bot($jenis)
	{
    $botToken = '1902669622:AAHz0cl18e8-IF5waY1uQIj33Nxqq72EzFc';
    $website="https://api.telegram.org/bot".$botToken;
    $chat_id = [
      'rendy' => '519446576',
      'trial' => '-1001727682139',
      'piloting' => '-516141238',
    ];
    switch ($jenis) {
      case 'reminder_work':
        $data = ReportModel::data_telegram_bot('reminder_work');

        $df = [];

        foreach($data as $d)
        {
          $df[preg_replace('/^PT/', 'PT.', $d->mitra_nm)][$d->id]['judul']       = $d->judul;
          $df[preg_replace('/^PT/', 'PT.', $d->mitra_nm)][$d->id]['mitra_nm']    = $d->mitra_nm;
          $df[preg_replace('/^PT/', 'PT.', $d->mitra_nm)][$d->id]['step_skrg']   = $d->step_skrg;
          $df[preg_replace('/^PT/', 'PT.', $d->mitra_nm)][$d->id]['step_next']   = $d->step_next;
          $df[preg_replace('/^PT/', 'PT.', $d->mitra_nm)][$d->id]['last_update'] = $d->tgl_last_update;
        }

        if($df)
        {
          foreach($df as $k => $v)
          {
            $message = '';
            $message .= "Daftar pekerjaan yang belum selesai\n";

            $message .= $k . "\n";
            foreach($v as $vv)
            {
              $message .= "<b><u>".$vv['judul']."</u></b>\n";
              $message .= "- Loker Sekarang: ".$vv['step_skrg']."\n";
              $message .= "- Loker Selanjutnya: ".$vv['step_next']."\n";
              $message .= "Terakhir Dikerjakan: ".$vv['last_update']."\n";
              $mitra = str_replace(' ', '', $vv['mitra_nm']);
            }

            $message .= "#UNFINISHED_".$mitra."_".date('Y_m_d')."\n";

            $params=[
              'chat_id' => $chat_id['trial'],
              'parse_mode' => 'html',
              'text' => "$message",
            ];

            $ch = curl_init();
            $optArray = array(
              CURLOPT_URL => $website.'/sendMessage',
              CURLOPT_RETURNTRANSFER => 1,
              CURLOPT_POST => 1,
              CURLOPT_POSTFIELDS => $params,
            );
            curl_setopt_array($ch, $optArray);
            $result = curl_exec($ch);
            curl_close($ch);
          }
        }
        else
        {
          $params=[
            'chat_id' => $chat_id['trial'],
            'parse_mode' => 'html',
            'text' => "Semua Pekerjaan selesai!",
          ];

          $ch = curl_init();
          $optArray = array(
            CURLOPT_URL => $website.'/sendMessage',
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => $params,
          );
          curl_setopt_array($ch, $optArray);
          $result = curl_exec($ch);
          curl_close($ch);
        }
      break;
      case 'current_work':
        $data = ReportModel::data_telegram_bot('current_work');

        $df = [];

        foreach($data as $d)
        {
          $df[preg_replace('/^PT/', 'PT.', $d->mitra_nm)][$d->id]['judul']       = $d->judul;
          $df[preg_replace('/^PT/', 'PT.', $d->mitra_nm)][$d->id]['mitra_nm']    = $d->mitra_nm;
          $df[preg_replace('/^PT/', 'PT.', $d->mitra_nm)][$d->id]['step_skrg']   = $d->step_skrg;
          $df[preg_replace('/^PT/', 'PT.', $d->mitra_nm)][$d->id]['step_next']   = $d->step_next;
          $df[preg_replace('/^PT/', 'PT.', $d->mitra_nm)][$d->id]['last_update'] = $d->tgl_last_update;
        }

        if($df)
        {
          foreach($df as $k => $v)
          {
            $message = '';
            $message .= "Daftar pekerjaan Dikerjakan Hari Ini\n";

            $message .= $k . "\n";
            foreach($v as $vv)
            {
              $message .= "<b><u>".$vv['judul']."</u></b>\n";
              $message .= "- Loker Sekarang: ".$vv['step_skrg']."\n";
              $message .= "- Loker Selanjutnya: ".$vv['step_next']."\n";
              $message .= "Terakhir Dikerjakan: ".$vv['last_update']."\n";
              $mitra = str_replace(' ', '', $vv['mitra_nm']);
            }

            $message .= "#CURRENTWORK_".$mitra."_".date('Y_m_d')."\n";

            $params=[
              'chat_id' => $chat_id['trial'],
              'parse_mode' => 'html',
              'text' => "$message",
            ];

            $ch = curl_init();
            $optArray = array(
              CURLOPT_URL            => $website.'/sendMessage',
              CURLOPT_RETURNTRANSFER => 1,
              CURLOPT_POST           => 1,
              CURLOPT_POSTFIELDS     => $params,
            );
            curl_setopt_array($ch, $optArray);
            $result = curl_exec($ch);
            curl_close($ch);
          }
        }
        else
        {
          $params=[
            'chat_id' => $chat_id['trial'],
            'parse_mode' => 'html',
            'text' => "Tidak ada pekerjaan yang dikerjakan hari ini",
          ];

          $ch = curl_init();
          $optArray = array(
            CURLOPT_URL            => $website.'/sendMessage',
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_POST           => 1,
            CURLOPT_POSTFIELDS     => $params,
          );
          curl_setopt_array($ch, $optArray);
          $result = curl_exec($ch);
          curl_close($ch);
        }
      break;
      case 'photo_dashboard':
        exec('cd /srv/htdocs/puppet_rend;nodejs dashboard_pla.js ');
        exec('cd /srv/htdocs/puppet_rend;nodejs dashboard_simple.js ');
        exec('cd /srv/htdocs/puppet_rend;nodejs dashboard_log_procurement.js ');
        $file_pla = '/srv/htdocs/d_liauw/procurement_dev/public/dashboard_pla.png';
        $file_simple = '/srv/htdocs/d_liauw/procurement_dev/public/dashboard_simple.png';
        $file_log = '/srv/htdocs/d_liauw/procurement_dev/public/dashboard_log.png';

        if (file_exists($file_pla))
        {
          $params_photo=[
            'chat_id' => $chat_id['piloting'],
            'caption' => "Laporan Dahboard pekerjaan Detail \n#DetailDashboard_".date('Y_m_d'),
            'photo' => new \CURLFile($file_pla),
          ];

          $ch = curl_init();
          $optArray_photo = array(
            CURLOPT_URL            => $website.'/sendPhoto',
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_POST           => 1,
            CURLOPT_POSTFIELDS     => $params_photo,
            CURLOPT_HTTPHEADER     => ['Content-Type: multipart/form-data'],
          );
          curl_setopt_array($ch, $optArray_photo);
          $result = curl_exec($ch);
          curl_close($ch);
        }

        if (file_exists($file_simple))
        {
          $params_photo=[
            'chat_id' => $chat_id['piloting'],
            'caption' => "Laporan Dahboard Pekerjaan Simple \n#SimpleDashboard_".date('Y_m_d'),
            'photo' => new \CURLFile($file_simple),
          ];

          $ch = curl_init();
          $optArray_photo = array(
            CURLOPT_URL            => $website.'/sendPhoto',
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_POST           => 1,
            CURLOPT_POSTFIELDS     => $params_photo,
            CURLOPT_HTTPHEADER     => ['Content-Type: multipart/form-data'],
          );
          curl_setopt_array($ch, $optArray_photo);
          $result = curl_exec($ch);
          curl_close($ch);
        }

        if (file_exists($file_log))
        {
          $params_photo=[
            'chat_id' => $chat_id['piloting'],
            'caption' => "Laporan Dahboard Pekerjaan Log \n#LogDashboard_".date('Y_m_d'),
            'photo' => new \CURLFile($file_log),
          ];

          $ch = curl_init();
          $optArray_photo = array(
            CURLOPT_URL            => $website.'/sendPhoto',
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_POST           => 1,
            CURLOPT_POSTFIELDS     => $params_photo,
            CURLOPT_HTTPHEADER     => ['Content-Type: multipart/form-data'],
          );
          curl_setopt_array($ch, $optArray_photo);
          $result = curl_exec($ch);
          curl_close($ch);
        }
      break;
    }
	}

  public function search_data(Request $req)
	{
    $mitra = ReportModel::active_mitra();
    $mitra_modify = $witel = $result = [];

    foreach($mitra as $k => $v)
    {
      $witel[$v->witel] = $v->witel;
      $mitra_modify[$v->witel][$k]['id'] = $v->id;
      $mitra_modify[$v->witel][$k]['text'] = $v->text;
    }

    if($req->all() )
    {
      $result = ReportModel::get_result_search($req);

      if(in_array($req->search, ['simple', 'detail', 'sp_num']) && count($result[1]) == 1 && $result[1][0]->active == 1)
      {
        return redirect('/get_detail_laporan/'.$result[1][0]->id);
      }
    }

    $kerjaan = ToolsModel::list_pekerjaan();
    $all_sto = AdminModel::area_alamat();

		return view('Tools.searching', compact('mitra_modify', 'req', 'witel', 'result', 'kerjaan', 'all_sto') );
	}

	public function get_detail_laporan($id)
	{
		$data             = AdminModel::get_data_final($id);
    $step_information = AdminModel::rest_step($data);
    $log_timeline     = $this->get_timeline_data($id);

    usort($log_timeline, function($a, $b) {
      return $b['hari_diff'] <=> $a['hari_diff'];
    });

    foreach($log_timeline as $k => $val)
    {
      if(is_null($val['selisih_waktu']) )
      {
        unset($log_timeline[$k]);
      }
    }

    $log_timeline = array_values($log_timeline);

    $log = ReportModel::log_rekon_mitra($id);
    $data_pid = MitraModel::get_all_pid($id);

    $data_ex = $pid_ex = $tanpa_rfc = $history_rfc = $rfc = [];

    foreach ($data_pid as $key => $v) {
      if(array_filter([$v->be_mt, $v->be_j, $v->be_mm] ) )
      {
        $data_ex[$v->id_pid] = array_filter([$v->be_mt, $v->be_j, $v->be_mm] );
      }
    }

    foreach($data_ex as $v)
    {
      $pid_ex[] = implode(', ', $v);
    }

    $data_rfc = MitraModel::get_rfc_history($id, 'all');
    $find_material_price = $count_download = 0;

    $all_design_rfc = AdminModel::find_all_rfc();
    $all_design_rfc = json_decode(json_encode($all_design_rfc), TRUE);

    $get_all_lok = DB::table('procurement_Boq_lokasi As pbl')
    ->leftjoin('procurement_req_pid As prp', 'pbl.id', '=', 'prp.id_lokasi')
    ->leftjoin('procurement_pid As pp', 'pp.id', '=', 'prp.id_pid_req')
    ->select('pbl.*', 'pp.pid')
    ->where('pbl.id_upload', $id)
    ->WhereNotNull('pp.id')
    ->get();

    $tanpa_rfc = $rfc_out = $rfc_return = $rfc_lop_out = $rfc_lop_return =  $rfc = $rfc_lop = [];

    foreach($data_rfc as $kc1 => $valc1)
    {
      $find_k_pbl = array_search($valc1->wbs_element, array_column(json_decode(json_encode($get_all_lok, TRUE) ), 'pid') );

      $data_pbl = [];

      if($find_k_pbl !== FALSE)
      {
        $data_pbl = $get_all_lok[$find_k_pbl];
      }

      $history_rfc[$valc1->rfc] = $valc1;
      $check_path = '/mnt/hdd4/upload/storage/rfc_ttd/';
      $rfc_file = @preg_grep('~^'.$valc1->rfc.'.*$~', scandir($check_path) );

      $key_adr_pid = array_search($valc1->material, array_column($all_design_rfc, 'design_rfc') );

      if(count($rfc_file) != 0)
      {
        $history_rfc[$valc1->rfc]->download = 'ada';
        $count_download += 1;
      }
      else
      {
        $check_path = public_path() . '/upload2/' . $id . '/dokumen_rfc_up/';
        $rfc_file = @preg_grep('~^'.$valc1->rfc.'.*$~', scandir($check_path) );
        if(count($rfc_file) != 0)
        {
          $history_rfc[$valc1->rfc]->download = 'ada';
          $count_download += 1;
        }
        else
        {
          $history_rfc[$valc1->rfc]->download = 'kdd';
        }
      }

      $rfc_out[$valc1->material]['material'] = $valc1->material;

      if($data_pbl)
      {
        $rfc_lop_out[$data_pbl->id][$valc1->material]['material'] = $valc1->material;
      }

      $multiple = 1;

      if($key_adr_pid !== FALSE)
      {
        $multiple = $all_design_rfc[$key_adr_pid]['qty_per_item'];
      }

      if($valc1->type == 'Out')
      {
        $rfc_out[$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['jenis']        = 'Out';
        $rfc_out[$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['no_reservasi'] = $valc1->no_reservasi;
        $rfc_out[$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['nomor_rfc_gi'] = $valc1->no_rfc;
        $rfc_out[$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['vol_give']     = $valc1->quantity;
        $rfc_out[$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['use']          = $valc1->terpakai;
        $rfc_out[$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['id_pro']       = $valc1->id_pro;

        if(!isset($rfc_out[$valc1->material]['isi_m']['rumus_terpakai']) )
        {
          $rfc_out[$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['rumus_terpakai'] = $valc1->terpakai;
        }

        $rfc_out[$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['rumus_terpakai'] = ($valc1->terpakai * $multiple);

        $rfc_out[$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['status_rfc_gi'] = $valc1->status_rfc;
        $rfc_out[$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['id_pro'] = $valc1->id_pro;

        $rfc_out[$valc1->material]['keterangan'] = $valc1->keterangan;

        if($data_pbl)
        {
          $rfc_lop_out[$data_pbl->id][$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['jenis']        = 'Out';
          $rfc_lop_out[$data_pbl->id][$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['nomor_rfc_gi'] = $valc1->no_rfc;
          $rfc_lop_out[$data_pbl->id][$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['vol_give']     = $valc1->quantity;
          $rfc_lop_out[$data_pbl->id][$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['use']          = $valc1->terpakai;
          $rfc_lop_out[$data_pbl->id][$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['no_reservasi'] = $valc1->no_reservasi;

          if(!isset($rfc_lop_out[$data_pbl->id][$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['rumus_terpakai']) )
          {
            $rfc_lop_out[$data_pbl->id][$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['rumus_terpakai'] = $valc1->terpakai;
          }

          $rfc_lop_out[$data_pbl->id][$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['rumus_terpakai'] = ($valc1->terpakai * $multiple);

          $rfc_lop_out[$data_pbl->id][$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['status_rfc_gi'] = $valc1->status_rfc;
          $rfc_lop_out[$data_pbl->id][$valc1->material]['isi_m'][$valc1->no_reservasi]['out']['id_pro'] = $valc1->id_pro;

          $rfc_lop_out[$data_pbl->id][$valc1->material]['keterangan'] = $valc1->keterangan;
        }
      }

      if($valc1->type == 'Return')
      {
        $rfc_return[$valc1->material][$valc1->id_pro]['jenis']             = 'Return';
        $rfc_return[$valc1->material][$valc1->id_pro]['nomor_rfc_return']  = $valc1->no_rfc;
        $rfc_return[$valc1->material][$valc1->id_pro]['return']            = $valc1->quantity;
        $rfc_return[$valc1->material][$valc1->id_pro]['status_rfc_return'] = $valc1->status_rfc;
        $rfc_return[$valc1->material][$valc1->id_pro]['id_pro']            = $valc1->id_pro;
        $rfc_return[$valc1->material][$valc1->id_pro]['no_reservasi']      = $valc1->no_reservasi;

        if($data_pbl)
        {
          $rfc_lop_return[$data_pbl->id][$valc1->material][$valc1->id_pro]['jenis']             = 'return';
          $rfc_lop_return[$data_pbl->id][$valc1->material][$valc1->id_pro]['nomor_rfc_return']  = $valc1->no_rfc;
          $rfc_lop_return[$data_pbl->id][$valc1->material][$valc1->id_pro]['return']            = $valc1->quantity;
          $rfc_lop_return[$data_pbl->id][$valc1->material][$valc1->id_pro]['no_reservasi']      = $valc1->no_reservasi;
          $rfc_lop_return[$data_pbl->id][$valc1->material][$valc1->id_pro]['id_pro']            = $valc1->id_pro;
          $rfc_lop_return[$data_pbl->id][$valc1->material][$valc1->id_pro]['status_rfc_return'] = $valc1->status_rfc;
        }
      }

      if(!isset($rfc_out[$valc1->material]['total_m']) )
      {
        $rfc_out[$valc1->material]['total_m'] = 0;
      }

      $rfc_out[$valc1->material]['total_m'] += ($valc1->type == 'Out' ? $valc1->quantity : 0);

      if(!isset($rfc_out[$valc1->material]['total_use']) )
      {
        $rfc_out[$valc1->material]['total_use'] = 0;
      }

      $rfc_out[$valc1->material]['total_use'] += ($valc1->type == 'Out' ? $valc1->terpakai * $multiple : 0);

      if($rfc_lop_out)
      {
        if(!isset($rfc_lop_out[$data_pbl->id][$valc1->material]['total_m']) )
        {
          $rfc_lop_out[$data_pbl->id][$valc1->material]['total_m'] = 0;
        }

        $rfc_lop_out[$data_pbl->id][$valc1->material]['total_m'] += ($valc1->type == 'Out' ? $valc1->quantity : 0);

        if(!isset($rfc_lop_out[$data_pbl->id][$valc1->material]['total_use']) )
        {
          $rfc_lop_out[$data_pbl->id][$valc1->material]['total_use'] = 0;
        }

        $rfc_lop_out[$data_pbl->id][$valc1->material]['total_use'] += ($valc1->type == 'Out' ? $valc1->terpakai * $multiple : 0);
      }
    }

    $all_material = array_unique(array_merge(array_keys($rfc_out), array_keys($rfc_return) ) );
    $rfc = array_map(function($x){
      return $x = [];
    }, array_flip($all_material) );

    foreach($rfc_out as $k => $v)
    {
      $rfc[$k] = $v;
    }

    foreach($rfc_return as $k => $v)
    {
      $key_rfc = array_column($rfc, 'material');
      $find_k = array_keys($key_rfc, $k);

      foreach($find_k as $kk => $vv)
      {
        $data_rfc_rev = $rfc[$key_rfc[$vv] ]['isi_m'];

        foreach($data_rfc_rev as $k3 => $v3)
        {
          $matchingKeys = array_keys(array_filter($v, function($item) use ($k3) {
            return $item['no_reservasi'] == $k3;
          }) );

          foreach($matchingKeys as $k4 => $v4)
          {
            $rfc[$key_rfc[$vv] ]['isi_m'][$v[$v4]['no_reservasi'] ]['return'][$v4] = $v[$v4];
          }
        }
      }
    }

    foreach($rfc_lop_out as $k0 => $v0)
    {
      if(@$rfc_lop_return[$k0])
      {
        $rfc_return = $rfc_lop_return[$k0];
        $all_material = array_unique(array_merge(array_keys($v0), array_keys($rfc_return ?? []) ) );
        $rfc_lop_raw = array_map(function($x){
          return $x = [];
        }, array_flip($all_material) );
        $rfc_lop = [];

        foreach($v0 as $k => $v)
        {
          $rfc_lop_raw[$k] = $v;
        }

        foreach($rfc_return as $k => $v)
        {
          $key_rfc = array_column($rfc, 'material');
          $find_k = array_keys($key_rfc, $k);

          foreach($find_k as $kk => $vv)
          {
            $data_rfc_lop = $rfc_lop_raw[$key_rfc[$vv] ]['isi_m'];

            foreach($data_rfc_lop as $k3 => $v3)
            {
              $matchingKeys = array_keys(array_filter($v, function($item) use ($k3) {
                return $item['no_reservasi'] == $k3;
              }) );

              foreach($matchingKeys as $k4 => $v4)
              {
                $rfc_lop_raw[$key_rfc[$vv] ]['isi_m'][$v[$v4]['no_reservasi'] ]['return'][$v4] = $v[$v4];
              }
            }
          }
        }

        $rfc_lop[$k0] = $rfc_lop_raw;
      }
    }

    $find_rfr = [];

    foreach($data_rfc as $k => $v)
    {
      $find_rfr[$v->id_reserv][$k]['type'] = $v->type;
      $find_rfr[$v->id_reserv][$k]['no_rfc'] = $v->no_rfc;
      $find_rfr[$v->id_reserv][$k]['material'] = $v->material;
    }

    foreach($find_rfr as $k => $v)
    {
      $v =  array_values($v);

      if(!in_array('Out', array_column($v, 'type') ) )
      {
        $find_return = array_keys(array_column($v, 'type'), 'Return');

        foreach($find_return as $vv)
        {
          $tanpa_rfc[] = $v[$vv]['no_rfc'] .' ('. $v[$vv]['material'] .')';
        }
      }
    }
    $that = new ProgressController();

    $key = $that->key_data();
		return view('Tools.detail_data_work', compact('data', 'rfc', 'pid_ex', 'log', 'step_information', 'log_timeline', 'tanpa_rfc', 'key') );
	}

	// public function list_budget_exceeded()
	// {
	// 	$data = ReportModel::list_budget_exceeded();
  //   return view('Report.list_budget_exceeded', compact('data') );
	// }

	public function update_BE($id, Request $req)
	{
    $data = ReportModel::update_be($id, $req);
    return back()->with('alerts', [
      ['type' => 'success', 'text' => $data->judul .' Budget Exceed']
    ]);
	}

	public function detail_dashboard($jenis_table, $witel, $tgl_hidden, $tgl, $mitra, $pekerjaan, $tampilan, $tgl_progress_kerja, $jenis_period, $aktor)
	{
    $data = ReportModel::detail_dashboard($jenis_table, $witel, $tgl_hidden, $tgl, $mitra, $pekerjaan, $tampilan, $tgl_progress_kerja, $jenis_period, $aktor);
    return view('Report.Rekon.SpPerMitra', compact('data') );
	}

	public function input_material_manual($id)
	{
    $data = $data_ref =[];
    $kerjaan = ToolsModel::list_pekerjaan();
    $mitra   = AdminModel::get_mitra(16);

    if(preg_match("/^[0-9]+$/", $id))
    {
      $data     = AdminModel::get_design(1, $id);
      $data_ref = AdminModel::get_design(1, $data->pd_refer);
    }
    // dd($mitra);
		return view('Operation.edit_material', compact('id', 'data_ref', 'data', 'mitra', 'kerjaan') );
	}

	public function save_material_manual($id, Request $req)
	{
    $msg = ReportModel::save_material_manual($req, $id);
    return back()->with('alerts', [
      ['type' => 'success', 'text' => $msg]
    ]);
	}

	public function detail_pekerjaan($witel, $tgl_hidden, $tgl, $mitra, $pekerjaan, $tgl_progress_kerja, $jenis_period, $aktor)
	{
    // dd($witel, $tgl_hidden, $tgl, $mitra, $pekerjaan, $tgl_progress_kerja, $jenis_period, $aktor);
    $data = ReportModel::get_detail_dashboard($witel, $tgl_hidden, $tgl, $mitra, $pekerjaan, $tgl_progress_kerja, $jenis_period, $aktor);
    return view('Report.Rekon.SpPerMitra', compact('data') );
	}

	public function edit_bank_dokumen_ttd($jenis)
	{
    $kerjaan = ToolsModel::list_pekerjaan();
    //opt, psb, cons
    $data_bank_ttd = [
      [
        'id'     => 'gm',
        'detail' => 'General Manager',
        'jenis'  => 'opt, psb, cons',
        'group'  => '1'
      ],
      [
        'id'     => 'osm',
        'detail' => 'OSM',
        'jenis'  => 'opt, psb, cons',
        'group'  => '1'
      ],
      [
        'id'     => 'm_fin',
        'detail' => 'Manager Finance',
        'jenis'  => 'opt, psb, cons',
        'group'  => '1'
      ],
      [
        'id'     => 'krglbh_dua_jt',
        'detail' => '≤ 200 Juta',
        'jenis'  => 'opt, psb, cons',
        'group'  => '2'
      ],
      [
        'id'     => 'antr_dualima_jt',
        'detail' => 'Antara 200 Juta ≥ 500 Juta',
        'jenis'  => 'opt, psb, cons',
        'group'  => '2'
      ],
      [
        'id'     => 'lbh_lima_jt',
        'detail' => '> 500 Juta',
        'jenis'  => 'opt, psb, cons',
        'group'  => '2'
      ],
      [
        'id'     => 'apprvJust_krglbh_limaratus_jt',
        'detail' => 'Persetujuan Justifikasi ≤ 500 Juta',
        'jenis'  => 'opt',
        'group'  => '2'
      ],
      [
        'id'     => 'apprvJust_lbh_limaratus_jt',
        'detail' => 'Persetujuan Justifikasi > 500 Juta',
        'jenis'  => 'opt',
        'group'  => '2'
      ],
      [
        'id'     => 'apprvJust_lbh_duaratus_jt',
        'detail' => 'Persetujuan Justifikasi ≤ 200 Juta',
        'jenis'  => 'cons',
        'group'  => '2'
      ],
      [
        'id'     => 'apprvJust_ant_duaratus_satuM',
        'detail' => 'Persetujuan Justifikasi Antara 200 Juta ≥ 1 Milyar',
        'jenis'  => 'cons',
        'group'  => '2'
      ],
      [
        'id'     => 'apprvJust_lbh_satuM',
        'detail' => 'Persetujuan Justifikasi > 1 Milyar',
        'jenis'  => 'cons',
        'group'  => '2'
      ],
      [
        'id'     => 'mngr_krj',
        'detail' => 'Manager Pekerjaan',
        'jenis'  => 'opt, psb, cons',
        'group'  => '3'
      ],
      [
        'id'     => 'invt_cntrl',
        'detail' => 'Inventory Control',
        'jenis'  => 'opt, psb, cons',
        'group'  => '3'
      ],
      [
        'id'     => 'mngr_ss',
        'detail' => 'Manager Shared Service',
        'jenis'  => 'opt, psb, cons',
        'group'  => '3'
      ],
      [
        'id'     => 'sm_krj',
        'detail' => 'Site Manager',
        'jenis'  => 'opt, psb, cons',
        'group'  => '3'
      ],
      [
        'id'     => 'wh',
        'detail' => 'Gudang',
        'jenis'  => 'opt, cons',
        'group'  => '3'
      ],
    ];

    $finish_cut = array_map(function ($v) use($jenis) {
      if( in_array($jenis, explode(', ', $v['jenis'] ) ) ) return $v;
    }, $data_bank_ttd);

    $finish_cut = array_filter($finish_cut);

    $jenisP['psb'] = 'PSB';
    $jenisP['opt'] = 'QE';
    $jenisP['cons'] = 'Construction';
    $all_witel = ReportModel::get_all_witel();

    $data_dok_class =  ReportModel::get_class_dok($jenisP[$jenis], session('auth')->Witel_New);

    foreach($finish_cut as $k => $v)
    {
			$find_k = array_keys(array_column(json_decode(json_encode(@$data_dok_class), TRUE ), 'rule_dok'), $v['id']);

      $data[$v['group'] ][$k]['id'] = $v['id'];
      $data[$v['group'] ][$k]['text'] = $v['detail'];

      if(!isset($data[$v['group'] ][$k]['avail']) )
      {
        $data[$v['group'] ][$k]['avail'] = 0;
        $data[$v['group'] ][$k]['expired'] = 0;
      }

      foreach($find_k as $vx)
      {
        $data_k = $data_dok_class[$vx];

        if(date('Y-m-d', strtotime($data_k->tgl_start) ) <= date('Y-m-d') && date('Y-m-d', strtotime($data_k->tgl_end) ) >= date('Y-m-d') )
        {
          $data[$v['group'] ][$k]['avail'] += 1;
        }
        else
        {
          $data[$v['group'] ][$k]['expired'] += 1;
        }
      }
    }

		return view('Tools.edit_ttd_bank', compact('kerjaan', 'data', 'all_witel'), ['title' => $jenisP[$jenis], 'witel' => session('auth')->Witel_New ] );
	}

	// public function update_bank_dokumen_ttd(Request $req, $jenis)
	// {
	// 	ReportModel::update_or_insert_ttd_dok($req, $jenis);
  //   return redirect('/home')->with('alerts', [
  //     ['type' => 'success', 'text' => 'Nik Untuk Kelengkapan Dokumen Berhasil Disimpan!']
  //   ]);
	// }

	public function find_user(Request $req)
	{
		$search = strtoupper($req->searchTerm);
    $check_data = ReportModel::find_user($search);

    $data = [];

    foreach ($check_data as $row) {
      // dd($row->ORDER_ID);
      $data[] = array("id" => $row->id, "text" => $row->user .' (' . $row->nik .'/'. $row->jabatan .')');
    }
    return \Response::json($data);
	}

	public function list_pid()
	{
		$data    = ReportModel::get_pid_budget(0);
		$data_ex = ReportModel::get_pid_budget(1);
    $apm_all = ReportModel::get_apm_budget_all();

		$pid_aman = $pid_be = [];

		foreach($data as $d)
		{
			$pid = $d->pid;

			if(!isset($pid_aman[$d->id]) )
			{
				$pid_aman[$d->id]['pid']             = $pid;
				$pid_aman[$d->id]['keterangan']      = $d->keterangan;
				$pid_aman[$d->id]['total_sp']        = $d->total_material_sp + $d->total_jasa_sp;
				$pid_aman[$d->id]['total_rekon']     = $d->total_material_rekon + $d->total_jasa_rekon;
				$pid_aman[$d->id]['id']              = $d->id;
				$pid_aman[$d->id]['budget']          = $d->budget;
				$pid_aman[$d->id]['created_at']      = $d->created_at;
				$pid_aman[$d->id]['created_by']      = $d->created_by;
				$pid_aman[$d->id]['budget_exceeded'] = $d->budget_exceeded;
				$pid_aman[$d->id]['budget_all']      = 0;
			}

			$pid_aman[$d->id]['budget_all']       += $d->budget_up;
			$pid_aman[$d->id]['judul'][$d->id_pbu] = $d->judul;

      $find_k   = array_search($d->pid, array_column(json_decode(json_encode($apm_all, TRUE) ), 'wbs') );

      if($find_k !== FALSE)
      {
        $data_apm = $apm_all[$find_k];
        $pid_aman[$d->id]['keperluan'] = $data_apm['keperluan'];
      }
		}

		usort($pid_aman, function($a, $b) {
			return $b['budget_all'] <=> $a['budget_all'];
		});
    // dd($data_ex);
		foreach($data_ex as $d)
		{
			$pid = $d->pid;

			if(!isset($pid_be[$d->id]) )
			{
				$pid_be[$d->id]['pid']             = $pid;
				$pid_be[$d->id]['keterangan']      = $d->keterangan;
				$pid_be[$d->id]['total_sp']        = $d->total_material_sp + $d->total_jasa_sp;
				$pid_be[$d->id]['total_rekon']     = $d->total_material_rekon + $d->total_jasa_rekon;
				$pid_be[$d->id]['id']              = $d->id;
				$pid_be[$d->id]['budget']          = $d->budget;
				$pid_be[$d->id]['created_at']      = $d->created_at;
				$pid_be[$d->id]['created_by']      = $d->created_by;
				$pid_be[$d->id]['budget_exceeded'] = $d->budget_exceeded;
				$pid_be[$d->id]['budget_all']      = 0;
			}

			$pid_be[$d->id]['budget_all']       += $d->budget_up;
			$pid_be[$d->id]['judul'][$d->id_pbu] = $d->judul;

      $find_k = array_search($d->pid, array_column(json_decode(json_encode($apm_all, TRUE) ), 'wbs') );
      if($find_k !== FALSE)
      {
        $data_apm = $apm_all[$find_k];
        $pid_be[$d->id]['keperluan'] = $data_apm['keperluan'];
      }
		}

		usort($pid_be, function($a, $b) {
			return $b['budget_all'] <=> $a['budget_all'];
		});
    // dd($pid_be);
		return view('Report.Commerce.monitor_pid', compact('pid_aman' , 'pid_be') );
	}

	public function see_pid()
	{
		$data = ReportModel::get_pid();
		return view('Report.Commerce.get_list_pid', compact('data') );
	}

	public function edit_pid($jenis, $id)
	{
		$data_log = ReportModel::get_pid_single($id);
		$data_pid_mitra     = ['nama_company' => $data_log->nama_company, 'id_company' => $data_log->id_company];
		$data['pid']        = $data_log->pid;
		$data['keterangan'] = $data_log->keterangan;
		$data['budget']     = $data_log->budget;
		$data['be']         = $data_log->budget_exceeded;

		$history_be = ReportModel::list_pekerjaan_pid($id, $jenis);

		if($history_be)
    {
			$jumlah = 0;

			foreach($history_be as $valc1)
			{
				$total_sp      = $valc1->total_material_sp + $valc1->total_jasa_sp;
				$total_rekon   = $valc1->total_material_rekon + $valc1->total_jasa_rekon;
				$total_kerjaan = ($total_rekon == 0 ? $total_sp: $total_rekon);
				$jumlah       +=  $total_kerjaan;

			}

			$data['jumlah'] = $jumlah;
			// $data['jumlah'] = (abs(($data_log->budget < 0 ? 0 : $data_log->budget) + $jumlah) );
		}
		return view('Commerce.create_pid', compact('data', 'data_log', 'data_log', 'history_be', 'data_pid_mitra') );
	}

	public function update_main_pid(Request $req, $jenis, $id)
	{
		$msg = ReportModel::update_main_pid($req, $id);
    return redirect($msg['direct'])->with('alerts', $msg['isi']);
	}

	public function tabel_sla(Request $req)
	{
    $data = [];

    if($req->all() )
    {
      $data = AdminModel::timeline($req->witel, $req->tgl_hidden, $req->tgl, $req->mitra, $req->khs, $req->view_table);
    }

    $mitra = ReportModel::active_mitra();

    foreach($mitra as $k => $v)
    {
      $witel_mod[$v->witel]                = $v->witel;
      $mitra_modify[$v->witel][$k]['id']   = $v->id;
      $mitra_modify[$v->witel][$k]['text'] = $v->text;
    }

    $data_pekerjaan = AdminModel::find_work(0, 'active');

		return view('Report.list_sla', compact('data', 'witel_mod', 'data_pekerjaan', 'mitra_modify', 'req') );
	}

	public function detail_sla(Request $req, $view_table, $aktor, $status)
	{
    $data = ReportModel::get_detail_sla($view_table, $aktor, $status, $req);
    $result = $data['result'];
    $title = $data['title'];
    return view('Report.detail_sla', compact('result', 'title', 'view_table') );
	}

	public function download_sla(Request $req, $view_table, $aktor, $status)
	{
    $get_data = ReportModel::get_detail_sla($view_table, $aktor, $status, $req);

    $head = [
      'Uraian',
      'Pekerjaan',
      'Total SP',
      'Total Rekon',
      'Jenis Pekerjaan',
      'Surat Kesanggupan',
      'Tanggal Surat Kesanggupan',
      'Surat Penetapan',
      'Tanggal Surat Penetapan',
      'Nama Mitra',
      'Witel Mitra',
      'PKS/Kontrak',
      'Amandemen PKS/Kontrak',
      'Tanggal Amandemen PKS/Kontrak',
      'Nomor Surat Pesanan',
      'Tanggal Surat Pesanan',
      'Amandemen Surat Pesanan',
      'Tanggal Amandemen Surat Pesanan',
      'Nomor SPP',
      'Nomor Receipt',
      'Nomor BAUT',
      'Nomor BAST',
      'Nomor BA ABD',
      'Nomor BA Rekon',
      'Tanggal Buat',
      'Tanggal Modifikasi',
      'Umur',
      'Loker Sebelum',
      'Loker Sekarang',
      'Loker Selanjutnya'
    ];

    foreach ($get_data['result'] as $key => $val) {
      $data[$key]['judul']             = $val->judul;
      $data[$key]['pekerjaan']         = $val->pekerjaan;
      $data[$key]['total_sp']          = $val->total_material_sp + $val->total_jasa_sp;
      $data[$key]['total_rekon']       = $val->total_material_rekon + $val->total_jasa_rekon;
      $data[$key]['jenis_work']        = $val->jenis_work;
      $data[$key]['surat_kesanggupan'] = $val->surat_kesanggupan;
      $data[$key]['tgl_surat_sanggup'] = $val->tgl_surat_sanggup;
      $data[$key]['surat_penetapan']   = $val->surat_penetapan;
      $data[$key]['tgl_s_pen']         = $val->tgl_s_pen;
      $data[$key]['nama_company']      = $val->mitra_nm;
      $data[$key]['witel_mitra']       = $val->witel_mitra;
      $data[$key]['pks']               = $val->pks;
      $data[$key]['amd_pks']           = $val->amd_pks;
      $data[$key]['tgl_amd_pks']       = $val->tgl_amd_pks;
      $data[$key]['no_sp']             = $val->no_sp;
      $data[$key]['tgl_sp']            = $val->tgl_sp;
      $data[$key]['amd_sp']            = $val->amd_sp;
      $data[$key]['tgl_amd_sp']        = $val->tgl_amd_sp;
      $data[$key]['spp_num']           = $val->spp_num;
      $data[$key]['receipt_num']       = $val->receipt_num;
      $data[$key]['BAUT']              = $val->BAUT;
      $data[$key]['BAST']              = $val->BAST;
      $data[$key]['ba_abd']            = $val->ba_abd;
      $data[$key]['ba_rekon']          = $val->ba_rekon;
      $data[$key]['created_at']        = $val->created_at;
      $data[$key]['tgl_last_update']   = $val->tgl_last_update;
      $data[$key]['umur']              = $val->jml_hri;
      $data[$key]['step_before']       = $val->step_before;
      $data[$key]['step_now']          = $val->step_now;
      $data[$key]['step_after']        = $val->step_after;
    }
    return Excel::download(new ExcelCommon($data, 'single', $head), 'Pekerjaan Detail SLA Loker ' . $get_data['title'] . '.xlsx');
	}

	public function monitor_budget()
	{
		$data = ReportModel::getBudgetList();
    return view('Report.budget_monitor', compact('data'));
	}

	public function rfc_problem()
	{
		$data_rfc       = AdminModel::get_rfc_boq('all');
		$design_rfc     = AdminModel::get_design_boq(0, [1, 2], 'dash_compare_rfc');
    $all_design_rfc = AdminModel::find_all_rfc();

    $design_rfc = json_decode(json_encode($design_rfc), TRUE);
    $all_design_rfc = json_decode(json_encode($all_design_rfc), TRUE);

    $volum_design = $rekap_mat = [];

    foreach($data_rfc as $valc1)
    {
      $key_dr_id_pbu = array_keys(array_column($design_rfc, 'id_pbu'), $valc1->id_pbu );

      foreach($key_dr_id_pbu as $v)
      {
        $key_adr_design = array_search($design_rfc[$v]['designator'], array_column($all_design_rfc, 'designator') );

        if($key_adr_design !== FALSE)
        {
          $key_adr_pid = array_keys(array_column($all_design_rfc, 'design_rfc'), $valc1->material);

          foreach($key_adr_pid as $vpid)
          {
            if($all_design_rfc[$vpid]['designator'] == $design_rfc[$v]['designator'] )
            {

              $volum_design[$valc1->mitra_nm]['rfc'][$valc1->rfc]['material']    = $design_rfc[$v]['designator'];

              if(!isset($volum_design[$valc1->mitra_nm][$valc1->judul][$design_rfc[$v]['designator'] ]['nama_material_rfc'][$valc1->material]['qty']) )
              {
                $volum_design[$valc1->mitra_nm][$valc1->judul][$design_rfc[$v]['designator'] ]['nama_material_rfc'][$valc1->material]['qty'] = 0;
              }

              $volum_design[$valc1->mitra_nm][$valc1->judul][$design_rfc[$v]['designator'] ]['nama_material_rfc'][$valc1->material]['qty'] += ($valc1->type == 'Out' ? $valc1->quantity: 0);
              $volum_design[$valc1->mitra_nm]['rfc'][$valc1->rfc]['dipakai']     = ($valc1->terpakai * $all_design_rfc[$vpid]['designator']);
              $volum_design[$valc1->mitra_nm]['rfc'][$valc1->rfc]['nilai_rekon'][$design_rfc[$v]['id'] ] = $design_rfc[$v]['rekon'];
              // $volum_design[$valc1->mitra_nm][$design_rfc[$v]['designator'] ]['material'] = $design_rfc[$v]['designator'];
              // $volum_design[$valc1->mitra_nm][$design_rfc[$v]['designator'] ]['nama_material_rfc'][$valc1->material]['nama'] = $valc1->material;

              // $volum_design[$valc1->mitra_nm][$design_rfc[$v]['designator'] ]['total_volum_design'][$design_rfc[$v]['id'] ] = $design_rfc[$v]['rekon'];

              // if(!isset($volum_design[$valc1->mitra_nm][$design_rfc[$v]['designator'] ]['total_volum_rfc'] ) )
              // {
              //   $volum_design[$valc1->mitra_nm][$design_rfc[$v]['designator'] ]['total_volum_rfc'] = 0;
              // }

              // $volum_design[$valc1->mitra_nm][$design_rfc[$v]['designator'] ]['total_volum_rfc'] += ($valc1->terpakai * $all_design_rfc[$vpid]['designator']);

              // if(!isset($rekap_mat[$design_rfc[$v]['designator'] ]['jml_rfc'] ) )
              // {
              //   $rekap_mat[$design_rfc[$v]['designator'] ]['jml_rfc'] = 0;
              // }

              // $rekap_mat[$design_rfc[$v]['designator'] ]['jml_rekon'][$valc1->judul][$design_rfc[$v]['id'] ] = (int)$design_rfc[$v]['rekon'];
              // $rekap_mat[$design_rfc[$v]['designator'] ]['jml_rfc'][$valc1->judul]  += ($valc1->terpakai * $all_design_rfc[$vpid]['designator']);
            }
          }
        }
      }
    }

    foreach($rekap_mat as $k => $v)
    {
      foreach($v['jml_rekon'] as $kk => $vv)
      {
        $rekap_mat[$k]['jml_rekon'][$kk] = array_sum($vv);
      }
    }

    foreach($rekap_mat as $k => $v)
    {
      foreach($v as $kk => $vv)
      {
        $rekap_mat[$k][$kk] = array_sum($vv);
      }
      $rekap_mat[$k]['sisa'] =  $rekap_mat[$k]['jml_rekon'] - $rekap_mat[$k]['jml_rfc'];
    }
	}

	public function download_spm(Request $req, $jenis, $isi)
	{
    switch ($jenis) {
      case 'dd':
        $url = explode('/', $isi);
        $isi = ReportModel::detail_dashboard($url[0], $url[1], $url[2], $url[3], $url[4], $url[5], $url[6], $url[7], $url[8], $url[9]);
      break;
      case 'rls':
        $url = explode('/', $isi);
        $isi = ReportModel::get_mitra_sp($url[0], $url[1], $req->witel, $req->tgl_hidden, $req->tgl, $req->mitra, $req->khs);
      break;
      default:
        $url = explode('/', $isi);
        $isi = ReportModel::get_detail_dashboard($url[0], $url[1], $url[2], $url[3], $url[4], $url[5], $url[6], $url[7]);
      break;
    }

    $head = [
      'Uraian',
      'Pekerjaan',
      'Loker',
      'Total SP',
      'Total Rekon',
      'Jenis Pekerjaan',
      'Surat Kesanggupan',
      'Tanggal Surat Kesanggupan',
      'Surat Penetapan',
      'Tanggal Surat Penetapan',
      'Nama Mitra',
      'Witel Mitra',
      'PKS/Kontrak',
      'Amandemen PKS/Kontrak',
      'Tanggal Amandemen PKS/Kontrak',
      'Nomor Surat Pesanan',
      'Tanggal Surat Pesanan',
      'Amandemen Surat Pesanan',
      'Tanggal Amandemen Surat Pesanan',
      'Nomor SPP',
      'Nomor Receipt',
      'Nomor BAUT',
      'Nomor BAST',
      'Nomor BA ABD',
      'Nomor BA Rekon',
      'Tanggal Buat',
      'Tanggal Modifikasi',
      'Umur',
      'Loker Sebelum',
      'Loker Sekarang',
      'Loker Selanjutnya'
    ];

    foreach ($isi as $key => $val) {
      $data[$key]['judul']             = $val->judul;
      $data[$key]['pekerjaan']         = $val->pekerjaan;
      $data[$key]['aktor_nama']         = $val->aktor_nama;
      $data[$key]['total_sp']          = $val->total_material_sp + $val->total_jasa_sp;
      $data[$key]['total_rekon']       = $val->total_material_rekon + $val->total_jasa_rekon;
      $data[$key]['jenis_work']        = $val->jenis_work;
      $data[$key]['surat_kesanggupan'] = $val->surat_kesanggupan;
      $data[$key]['tgl_surat_sanggup'] = $val->tgl_surat_sanggup;
      $data[$key]['surat_penetapan']   = $val->surat_penetapan;
      $data[$key]['tgl_s_pen']         = $val->tgl_s_pen;
      $data[$key]['nama_company']      = $val->mitra_nm;
      $data[$key]['witel_mitra']       = $val->witel_mitra;
      $data[$key]['pks']               = $val->pks;
      $data[$key]['amd_pks']           = $val->amd_pks;
      $data[$key]['tgl_amd_pks']       = $val->tgl_amd_pks;
      $data[$key]['no_sp']             = $val->no_sp;
      $data[$key]['tgl_sp']            = $val->tgl_sp;
      $data[$key]['amd_sp']            = $val->amd_sp;
      $data[$key]['tgl_amd_sp']        = $val->tgl_amd_sp;
      $data[$key]['spp_num']           = $val->spp_num;
      $data[$key]['receipt_num']       = $val->receipt_num;
      $data[$key]['BAUT']              = $val->BAUT;
      $data[$key]['BAST']              = $val->BAST;
      $data[$key]['ba_abd']            = $val->ba_abd;
      $data[$key]['ba_rekon']          = $val->ba_rekon;
      $data[$key]['created_at']        = $val->created_at;
      $data[$key]['tgl_last_update']   = $val->tgl_last_update;
      $data[$key]['umur']              = $val->jml_hri;
      $data[$key]['step_before']       = $val->step_before;
      $data[$key]['step_now']          = $val->step_now;
      $data[$key]['step_after']        = $val->step_after;
    }

    return Excel::download(new ExcelCommon($data, 'single', $head), 'Excel List Pekerjaan.xlsx');
	}

	public function history_user(Request $req)
	{
		$data = ReportModel::get_dok_ttd($req->id, $req->work, $req->witel);
    return \Response::json($data);
	}

	public function insert_or_update_ttd_dok(Request $req)
	{
		$msg = ReportModel::ins_or_up_ttd($req);
    return back()->with('alerts', $msg['isi']);
	}

	public function delete_user_ttd_Dok($id)
	{
		$msg = ReportModel::delete_user_dok($id);
    return back()->with('alerts', $msg['isi']);
	}

	// public function edit_NI()
	// {
	// 	return view('Mitra.material_NI');
	// }

	// public function save_NI(Request $req)
	// {
  //   ReportModel::save_NI($req);
  //   return back()->with('alerts', [
  //     ['type' => 'success', 'text' => 'Berhasil! Designator New Item Berhasil Ditambahkan!']
  //   ]);
	// }

}