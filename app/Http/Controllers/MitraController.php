<?php

namespace App\Http\Controllers;

use App\DA\AdminModel;
use App\DA\UploadModel;
use App\DA\ReportModel;
use App\DA\MitraModel;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use DB;
use stdClass;
use Session;

date_default_timezone_set("Asia/Makassar");
class MitraController extends Controller
{

  // public function input_rfc($id)
	// {
  //   $data_sp = AdminModel::get_data_final($id, 'ps', 'REKON');
  //   $material = MitraModel::get_m(0, true);
  //   // dd($material);
  //   return view('Mitra.add_rfc_osp', compact('data_sp'), ['material' => $material]);
	// }

  // public function edit_rfc_lok($id_sp, $id_lok)
	// {
  //   $data_sp = AdminModel::get_data_final($id_sp);
	// 	$data = MitraModel::get_sp_lokasi($id_sp, $id_lok);
  //   $material = MitraModel::get_m($id_sp, false, $id_lok);
  //   $data_single = MitraModel::get_rfc_osp($id_sp, $id_lok);
  //   return view('Mitra.Rekon.Finish.add_rfc_osp', compact('data', 'data_single', 'data_sp'), ['material' => $material]);
	// }

  // public function update_rfc_lok(Request $req, $id_sp, $id_lok)
	// {
  //   $msg = MitraModel::update_rfc($req, $id_sp, $id_lok);
  //   return redirect('/Mitra/osp_fo/'.$id_sp)->with('alerts', $msg);
	// }

  // public function download_ba_kesepakatan($id_sp)
  // {
  //   $check_path = public_path() . '/upload2/' . $id_sp . '/dokumen_sanggup/kesepakatan/';
  //   $files = preg_grep('~^File Kesepakatan Harga.*$~', scandir($check_path));

  //   if(count($files) != 0)
  //   {
  //     $files = array_values($files);
  //     $file = $check_path.'/'.$files[0];
  //     header('Content-Description: File Transfer');
  //     header('Content-Type: application/octet-stream');
  //     header('Content-Disposition: attachment; filename='.basename($file));
  //     header('Expires: 0');
  //     header('Cache-Control: must-revalidate');
  //     header('Pragma: public');
  //     header('Content-Length: ' . filesize($file));
  //     ob_clean();
  //     flush();
  //     readfile($file);
  //     exit;
  //   }
  // }

	public function search_rfc(Request $req)
	{
    $check_data = MitraModel::search_rfc($req);
    $data = [];

    if($check_data)
    {
      foreach ($check_data as $row) {
        $data[] = array("id" => $row->no_rfc, "text" => $row->no_rfc. ' ('. $row->wbs_element .') | '. $row->type . ' | '. $row->mitra);
      }
    }
    return \Response::json($data);
	}

	public function search_rfc_plan(Request $req)
	{
    $check_data = MitraModel::search_rfc_plan($req);
    $data = [];

    if($check_data)
    {
      foreach ($check_data as $row) {
        $data[] = array("id" => $row->no_rfc, "text" => $row->no_rfc. ' ('. $row->wbs_element .') | '. $row->type . ' | '. $row->mitra);
      }
    }
    return \Response::json($data);
	}

  public static function save_pelurusan_material(Request $req, $id)
  {
    $msg = MitraModel::save_pelurusan($req, $id);
    return redirect($msg['direct'])->with('alerts', $msg['isi'])->withInput();
  }

	public function change_stts_rfc($jenis, $id)
	{
    if($jenis == 'delete')
    {
      $id = MitraModel::delete_rfc($id);
      return back()->with('alerts', [
        ['type' => 'success', 'text' => 'Nomor RFC '. $id .' Berhasil Dihapus!']
      ]);
    }
    else
    {
      $msg = MitraModel::lurus_rfc($id);
      return back()->with('alerts', [
        ['type' => 'success', 'text' => $msg]
      ]);
    }
	}

	public function get_inventory(Request $req)
	{
		$inventory = MitraModel::inventory($req->isi);

    $data['inventory'] = $inventory;

    $pekerjaan_lokasi = MitraModel::pekerjaan_per_lok($req->id_pbu);

    foreach($pekerjaan_lokasi as $val)
    {
      $data['lokasi'][$val->pid ][] = $val;
    }

    $pid = MitraModel::lokasi_pid($req->id_pbu);

    foreach($pid as $val)
    {
      $data['pid'][] = $val;
    }

    $data['download'] = FALSE;
    $check_path = '/mnt/hdd4/upload/storage/rfc_ttd/';
    $files = @preg_grep('~^'.$inventory[0]->no_rfc.'.*$~', scandir($check_path));

    if(count($files) != 0)
    {
      $data['download'] = TRUE;
    }
    else
    {
      $check_path = public_path() . '/upload2/' . $req->id_pbu . '/dokumen_rfc_up/';
      $files = @preg_grep('~^'.$inventory[0]->no_rfc.'.*$~', scandir($check_path) );

      if(count($files) != 0)
      {
        $data['download'] = TRUE;
      }
    }

    return \Response::json($data);
	}

	public function submit_used_rfc(Request $req)
	{
    MitraModel::save_terpakai_mat($req);
	}

	public function submit_file_rfc(Request $req)
	{
    $path = public_path() . '/upload2/' . $req->id_upload . '/dokumen_rfc_up/';

		if (!file_exists($path)) {
			if (!mkdir($path, 0770, true)) {
				return 'gagal menyiapkan folder sanggup';
			}
		}

		if ($req->hasFile('file_rfc')) {
			$file = $req->file('file_rfc');
			try {
				$filename = $req->no_rfc.'.'.strtolower($file->getClientOriginalExtension() );
				$file->move("$path", "$filename");
			} catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
				return 'gagal menyimpan RFC';
			}
		}

    return back()->with('alerts', [
      ['type' => 'success', 'text' => 'Berhasil Tambah File RFC!']
    ]);
	}

	public function submit_ket_rfc(Request $req)
	{
    MitraModel::save_keterangan_rfc($req);
	}

	public function save_pelurusan_rfc($id)
	{
    $msg = MitraModel::save_pelurusan_rfc($id);
    return redirect('progressList/8')->with('alerts', [
      ['type' => 'success', 'text' => $msg]
    ]);
	}

	public function delete_rfc_permanent($id)
	{
		MitraModel::delete_permanen_rfc($id);

    return back()->with('alerts', [
      ['type' => 'success', 'text' => 'Berhasil Menghapus RFC!']
    ]);
	}

	public function find_used_rfc(Request $req)
	{
    $data = MitraModel::find_rfc('old_rfc',$req->isi);
    return \Response::json($data);
	}

}
