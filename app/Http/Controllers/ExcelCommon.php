<?php

namespace App\Http\Controllers;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Concerns\WithEvents;

class ExcelCommon implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents
{
	use Exportable;

	public function __construct($data, $type, $head = []){
		$this->data = $data;
		$this->type = $type;
		$this->head = $head;
	}

	public function collection(){
		if($this->type == 'single')
		{
			return collect($this->data);
		}
		else
		{
			return collect([]);
		}
	}

	public function headings(): array{
		if($this->type == 'single')
		{
			return $this->head;
		}
		else
		{
			return [];
		}
	}

	public function number_to_alphabet($number) {
    $number = intval($number);

    if($number <= 0)
		{
			return '';
    }

    $alphabet = '';

    while($number != 0)
		{
			$p = ($number - 1) % 26;
			$number = intval(($number - $p) / 26);
			$alphabet = chr(65 + $p) . $alphabet;
    }

    return $alphabet;
	}

	public function registerEvents(): array
	{
		if( $this->type == 'multi_dashboard_excl')
		{
			return [
				BeforeExport::class  => function(BeforeExport $event)
				{
					$border_Style = [
						'borders' => [
							'allBorders' => [
								'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
							],
						],
						'alignment' => [
							'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
							'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
						]
					];

					$event->writer->getProperties()->setCreator('PT. TELKOM AKSES');

					foreach($this->data as $key => $val)
					{
						$event->writer->createSheet();

						foreach($val['head'] as $key_c1 => $val_c1)
						{
							$nkey = $key_c1 +1;
							$column = $this->number_to_alphabet($nkey).''. 1;
							$event->writer->setActiveSheetIndex($key)->getCell($column)->setValue($val_c1);
						}

						$nkey2 = 0;
						$column = '';

						foreach($val['data'] as $key_c2 => $val_c2)
						{
							$nkey2 = $key_c2 + 2;
							$no = 0;
							foreach($val_c2 as $val_cc2)
							{
								$column = $this->number_to_alphabet(++$no);
								$event->writer->setActiveSheetIndex($key)->getCell($column .''. $nkey2)->setValue($val_cc2);
							}
						}

						$event->writer->setActiveSheetIndex($key)->getStyle('A1:'.$column .''. $nkey2)->applyFromArray($border_Style);

						foreach (range('A', $event->writer->getActiveSheet()->getHighestDataColumn()) as $col) {
							$event->writer->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
						}
					}

				}
			];
		}

		if( $this->type == 'excl_planning')
		{
			return [
				BeforeExport::class => function(BeforeExport $event)
				{
					$border_Style = [
						'borders' => [
							'allBorders' => [
								'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
							],
						],
						'alignment' => [
							'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
							'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
						]
					];
					$event->writer->createSheet();

					$event->writer->getProperties()->setCreator('PT. TELKOM AKSES');

					$data = $this->data[0]['data'];
					$designator = $this->data[0]['designator'];

					$event->writer->setActiveSheetIndex(0)->getCell("A1")->setValue('Judul :');
					$event->writer->setActiveSheetIndex(0)->getCell("B1")->setValue($data['judul']);
					$event->writer->setActiveSheetIndex(0)->getCell("A5")->setValue('Nomor');
					$event->writer->setActiveSheetIndex(0)->getCell("B5")->setValue('Designator');
					$event->writer->setActiveSheetIndex(0)->getCell("C5")->setValue('Uraian');
					$event->writer->setActiveSheetIndex(0)->getCell("D5")->setValue('Satuan');
					$event->writer->setActiveSheetIndex(0)->getCell("E5")->setValue('Paket 7');
					$event->writer->setActiveSheetIndex(0)->getCell("E8")->setValue('Material');
					$event->writer->setActiveSheetIndex(0)->getCell("F8")->setValue('Jasa');
					$event->writer->setActiveSheetIndex(0)->getCell("G5")->setValue('Total Nilai');
					$event->writer->setActiveSheetIndex(0)->mergeCells('A5:A9');
					$event->writer->setActiveSheetIndex(0)->mergeCells('B5:B9');
					$event->writer->setActiveSheetIndex(0)->mergeCells('C5:C9');
					$event->writer->setActiveSheetIndex(0)->mergeCells('D5:D9');
					$event->writer->setActiveSheetIndex(0)->mergeCells('E5:F9');
					$event->writer->setActiveSheetIndex(0)->mergeCells('G5:G9');

					$start_number = 9;
					$no_kolom = $no_data_material = $no_data_tiket = 0;

					foreach($designator as $v)
					{
						++$no_kolom;
						$no = ++$start_number;

						$event->writer->setActiveSheetIndex(0)->getCell("A$no")->setValue($no_kolom);
						$event->writer->setActiveSheetIndex(0)->getCell("B$no")->setValue($v['designator']);
						$event->writer->setActiveSheetIndex(0)->getCell("C$no")->setValue($v['uraian']);
						$event->writer->setActiveSheetIndex(0)->getCell("D$no")->setValue($v['satuan']);
						$event->writer->setActiveSheetIndex(0)->getCell("E$no")->setValue($v['material']);
						$event->writer->setActiveSheetIndex(0)->getCell("F$no")->setValue($v['jasa']);
					}

					for ($i = 'H'; $i !== 'ZZ'; $i++){
						$charl_load_raw[] = $i;
					}

					$charl_load = array_chunk($charl_load_raw, 3);

					// dd($data['material_tomman'], $charl_load);

					$total_all = [];

					foreach($data['material_tomman'] as $k => $v)
					{
						$get_char = $charl_load[$no_data_material];

						$event->writer->setActiveSheetIndex(0)->getCell($get_char[0].'9')->setValue('qty');
						$event->writer->setActiveSheetIndex(0)->getStyle($get_char[0].'9')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('fff200');

						$start_number_2 = 9;

						foreach($v as $kk => $vv)
						{
							if(!isset($total_all[$kk]) )
							{
								$total_all[$kk] = 0;
							}

							$total_all[$kk] += $vv;

							$no_2 = ++$start_number_2;
							$event->writer->setActiveSheetIndex(0)->getCell($get_char[0].''.$no_2)->setValue($vv);

							$event->writer->setActiveSheetIndex(0)->getStyle($get_char[0].''.$no_2)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('fff200');

						}
						++$no_data_material;
					}

					$total_all = array_values($total_all);

					foreach($total_all as $k => $v)
					{
						$no_tot = ($k + 10);

						$event->writer->setActiveSheetIndex(0)->getCell('G'.$no_tot)->setValue($v);
					}

					foreach($data['tiket_tomman'] as $k => $v)
					{
						$get_char = $charl_load[$no_data_tiket];
						$event->writer->setActiveSheetIndex(0)->getCell($get_char[0].'6')->setValue(@implode(', ',array_merge([], ...array_column($v, 'rfc') ) ) );
						$event->writer->setActiveSheetIndex(0)->getCell($get_char[0].'7')->setValue(@implode(', ', array_merge([], ...array_column($v, 'no_tiket') ) ) );

						$event->writer->setActiveSheetIndex(0)->getCell($get_char[0].'8')->setValue($k);

						$event->writer->setActiveSheetIndex(0)->mergeCells($get_char[0].'5:'.$get_char[2].'5');
						$event->writer->setActiveSheetIndex(0)->mergeCells($get_char[0].'6:'.$get_char[2].'6');
						$event->writer->setActiveSheetIndex(0)->mergeCells($get_char[0].'7:'.$get_char[2].'7');
						$event->writer->setActiveSheetIndex(0)->mergeCells($get_char[0].'8:'.$get_char[2].'8');

						$event->writer->setActiveSheetIndex(0)->getCell($get_char[1].'9')->setValue('Nomor Tiket');
						$event->writer->setActiveSheetIndex(0)->getCell($get_char[2].'9')->setValue('RFC/RFR');

						$start_number_2 = 9;

						foreach($v as $kk => $vv)
						{
							$no_2 = ++$start_number_2;
							$event->writer->setActiveSheetIndex(0)->getCell($get_char[1].''.$no_2)->setValue(@implode(', ' , $vv['no_tiket']));
							$event->writer->setActiveSheetIndex(0)->getCell($get_char[2].''.$no_2)->setValue(@implode(', ' , $vv['rfc']));

						$event->writer->setActiveSheetIndex(0)->getStyle($get_char[1].''.($no_2 - 1) .':'. $get_char[2].''.$no_2)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('FF0000');

						}
						++$no_data_tiket;
					}

					// $last_char = $charl_load_raw[( (count($data['tiket_tomman']) * 3) )];
					// $event->writer->setActiveSheetIndex(0)->getStyle('A6:'. $last_char .''. $no_2)->applyFromArray($border_Style);
					$event->writer->setActiveSheetIndex(0)->getStyle('A5:'. $get_char[2] .''. $no_2)->applyFromArray($border_Style);

					$event->writer->setActiveSheetIndex(0)->getStyle('H5:'.$get_char[2].'8')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('FF0000');
				}
			];
		}
		else
		{
			return [];
		}
	}
}