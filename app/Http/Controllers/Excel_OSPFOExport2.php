<?php

namespace App\Http\Controllers;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Events\BeforeSheet;
use Maatwebsite\Excel\Events\BeforeWriting;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use Maatwebsite\Excel\Excel;
use DB;
use App\DA\ReportModel;
use App\DA\AdminModel;

class Excel_OSPFOExport2 implements  WithEvents
{
    use Exportable;

    public function __construct($data)
    {
        $this->data  = $data[0];
        $this->data1 = $data[1];
        $this->data2 = $data[2];
        $this->data3 = $data[3];
        $this->data4 = $data[4];
        $this->data5 = $data[5];
        $this->opsi  = isset($data[6]) ? $data[6] : '';
        $this->datasl = $data[7];
    }

    public function convert_month($date, $cetak_hari = false)
    {
        $bulan = array (1 =>   'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );

        $hari = array ( 1 => 'Senin',
            'Selasa',
            'Rabu',
            'Kamis',
            'Jumat',
            'Sabtu',
            'Minggu'
        );

        $result = date('d m Y', strtotime($date) );
        $split = explode(' ', $result);
        $hasilnya = $split[0] . ' ' . $bulan[(int)$split[1] ] . ' ' . $split[2];

        if($cetak_hari)
        {
            $num = date('N', strtotime($date) );
            return $hari[$num].' '.$hasilnya;
        }

        return $hasilnya;
    }

    public function only_day($date)
    {
        $hari = array ( 1 => 'Senin',
            'Selasa',
            'Rabu',
            'Kamis',
            'Jumat',
            'Sabtu',
            'Minggu'
        );

        $num = date('N', strtotime($date) );
        return $hari[$num];
    }

    public function terbilang($nilai)
    {
        if ($nilai < 0)
        {
            $hasil = "minus " . trim($this->penyebut($nilai) );
        }
        else
        {
            $hasil = trim($this->penyebut($nilai) );
        }
        $hasil = preg_replace('/\s+/', ' ', $hasil);
        return $hasil;
    }

    public function penyebut($nilai)
    {
        $nilai = abs($nilai);
        $huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
        $temp = "";

        if($nilai < 12)
        {
            $temp = " " . $huruf[$nilai];
        }
        else if($nilai < 20)
        {
            $temp = $this->penyebut($nilai - 10) . " belas";
        }
        else if($nilai < 100)
        {
            $temp = $this->penyebut($nilai / 10) . " puluh" . $this->penyebut($nilai % 10);
        }
        else if($nilai < 200)
        {
            $temp = " seratus" . $this->penyebut($nilai - 100);
        }
        else if($nilai < 1000)
        {
            $temp = $this->penyebut($nilai / 100) . " ratus" . $this->penyebut($nilai % 100);
        }
        else if($nilai < 2000)
        {
            $temp = " seribu" . $this->penyebut($nilai - 1000);
        }
        else if($nilai < 1000000)
        {
            $temp = $this->penyebut($nilai / 1000) . " ribu" . $this->penyebut($nilai % 1000);
        }
        else if($nilai < 1000000000)
        {
            $temp = $this->penyebut($nilai / 1000000) . " juta" . $this->penyebut($nilai % 1000000);
        }
        else if($nilai < 1000000000000)
        {
            $temp = $this->penyebut($nilai / 1000000000) . " milyar" . $this->penyebut(fmod($nilai, 1000000000) );
        }
        else if($nilai < 1000000000000000)
        {
            $temp = $this->penyebut($nilai / 1000000000000) . " trilyun" . $this->penyebut(fmod($nilai, 1000000000000) );
        }

        return$temp;
    }

    public function registerEvents(): array
    {
        return [
            BeforeExport::class  => function(BeforeExport $event) {
                $fill_border_lampir = [
                    'fill' => [
                        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                        'startColor' => [
                            'argb' => '4CAEE3',
                        ],
                    ]
                ];

                $fill_border_nego_sepakat = [
                    'fill' => [
                        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                        'startColor' => [
                            'argb' => '595959',
                        ],
                    ]
                ];

                $border_Style = [
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
                        ],
                    ],
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    ]
                ];

                $reader = new Xlsx();
                \PhpOffice\PhpSpreadsheet\Shared\File::setUseUploadTempDirectory(true);
                $spreadSheet = $reader->load(public_path().'/template_doc/maintenance_down_2021/Template Invoice & BA OSP FO v2.xlsx');

                $bulan = array (
                'Januari' => '01',
                'Februari' => '02',
                'Maret' => '03',
                'April' => '04',
                'Mei' => '05',
                'Juni' => '06',
                'Juli' => '07',
                'Agustus' => '08',
                'September' => '09',
                'Oktober' => '10',
                'November' => '11',
                'Desember' => '12'
                );

                $bulan_angk = array (
                    '01' =>'Januari',
                    '02' =>'Februari',
                    '03' =>'Maret',
                    '04' =>'April',
                    '05' =>'Mei',
                    '06' =>'Juni',
                    '07' =>'Juli',
                    '08' =>'Agustus',
                    '09' =>'September',
                    '10' =>'Oktober',
                    '11' =>'November',
                    '12' =>'Desember'
                );
                // $data->no_khs_maintenance;
                // dd($data);

                if($this->data->tgl_pks == 0000-00-00)
                {
                    $this->data->tgl_pks = null;
                }
                // dd($this->data);
                $AdminModel = new AdminModel();
                $pph        = $AdminModel->find_pph($this->data->tgl_bast);
                $pph_sp     = $AdminModel->find_pph($this->data->tgl_sp);
		        $pph_text         = $AdminModel->find_pph_text($this->data->tgl_bast);
		        $pph_text_sp      = $AdminModel->find_pph_text($this->data->tgl_sp);
		        $pph_text_amd     = $AdminModel->find_pph_text($this->data->tgl_amd_sp);

                $foundInCells = array();
                $searchTerm = '${lok_kerja}';
                $no = 0;

                foreach ($spreadSheet->getWorksheetIterator() as $CurrentWorksheet)
                {
                    $ws = $CurrentWorksheet->getTitle();

                    foreach ($CurrentWorksheet->getRowIterator() as $row)
                    {
                        $cellIterator = $row->getCellIterator();
                        $cellIterator->setIterateOnlyExistingCells(FALSE);

                        foreach ($cellIterator as $cell)
                        {
                            if (!empty($cell->getValue() ) && strpos(strtolower($cell->getValue() ), strtolower($searchTerm)) !== FALSE)
                            {
                                $no++;
                                $foundInCells[$no]['sheet'] = $spreadSheet->getIndex($spreadSheet->getSheetByName($ws) );
                                $foundInCells[$no]['cell'] = $cell->getCoordinate();
                            }
                        }
                    }
                }

                foreach($foundInCells as $v)
                {
                    $richText = new \PhpOffice\PhpSpreadsheet\RichText\RichText();
                    $richText->createText('This invoice is ');
                    $payable = $richText->createTextRun('payable within thirty days after the end of the month');
                    $payable->getFont()->setBold(true);
                    $richText->createText(', unless specified otherwise on the invoice.');
                    // dd($richText);
                    $get_val = $spreadSheet->getSheet($v['sheet'])->getCell($v['cell'])->getValue();
                    if($get_val != '${lok_kerja}')
                    {
                        // dd(explode('${lok_kerja}', $get_val), $get_val );
                    }
                    $spreadSheet->getSheet($v['sheet'])->getCell($v['cell'])->SetValue($richText);
                }

                $spreadSheet->getSheet(0)->getCell('A1')->SetValue(substr_replace($this->data->mitra_nm, 'PT.', 0, 2) );
                $spreadSheet->getSheet(0)->getCell('B1')->SetValue($pph_text);
                $spreadSheet->getSheet(0)->getCell('C1')->SetValue($pph_text_sp);
                $spreadSheet->getSheet(0)->getCell('D1')->SetValue($pph_text_amd);
                $spreadSheet->getSheet(0)->getCell('A2')->SetValue($this->data2->bank);
                $spreadSheet->getSheet(0)->getCell('A3')->SetValue($this->data2->cabang_bank);
                $spreadSheet->getSheet(0)->getCell('B3')->SetValue($this->data2->NPWP);
                $spreadSheet->getSheet(0)->getCell('A4')->SetValue($this->data2->rek);
                $spreadSheet->getSheet(0)->getCell('A5')->SetValue($this->data2->alamat_company);
                $spreadSheet->getSheet(0)->getCell('A6')->SetValue($this->data2->wakil_mitra);
                $spreadSheet->getSheet(0)->getCell('A7')->SetValue($this->data2->jabatan_mitra);
                $spreadSheet->getSheet(0)->getCell('A8')->SetValue($this->data->spp_num);

                $get_lok = DB::table('procurement_area')->where('witel', $this->data->witel)->first();

                $spreadSheet->getSheet(0)->getCell('A9')->SetValue(ucwords($get_lok->area) );
                $spreadSheet->getSheet(0)->getCell('B9')->SetValue($this->data->tgl_faktur);
                $spreadSheet->getSheet(0)->getCell('C9')->SetValue($this->convert_month($this->data->tgl_faktur) );
                $spreadSheet->getSheet(0)->getCell('A10')->SetValue($this->data->judul);
                $spreadSheet->getSheet(0)->getCell('B10')->SetValue($this->data->lokasi_pekerjaan);
                $spreadSheet->getSheet(0)->getCell('A11')->SetValue($this->data->pks);
                $spreadSheet->getSheet(0)->getCell('B11')->SetValue($this->data->no_sp);
                $spreadSheet->getSheet(0)->getCell('A12')->SetValue($this->data->tgl_pks);
                $spreadSheet->getSheet(0)->getCell('B12')->SetValue($this->convert_month( ($this->data->tgl_pks) ) );
                $spreadSheet->getSheet(0)->getCell('A13')->SetValue($this->data->tgl_sp);
                $spreadSheet->getSheet(0)->getCell('B13')->SetValue($this->convert_month($this->data->tgl_sp) );
                $spreadSheet->getSheet(0)->getCell('A14')->SetValue($this->data->total_material_sp);
                $spreadSheet->getSheet(0)->getCell('B14')->SetValue($this->data->total_jasa_sp);
                $spreadSheet->getSheet(0)->getCell('A15')->SetValue($this->data->gd_sp);
                $spreadSheet->getSheet(0)->getCell('B15')->SetValue( ($this->data->gd_sp * $pph_sp) );
                $spreadSheet->getSheet(0)->getCell('E15')->SetValue( ($this->data->gd_rekon * $pph) );
                $spreadSheet->getSheet(0)->getCell('C15')->SetValue(ucwords($this->terbilang($this->data->gd_sp) .' rupiah') );
                $spreadSheet->getSheet(0)->getCell('D15')->SetValue(ucwords($this->terbilang(number_format($this->data->gd_sp + ($this->data->gd_sp * $pph_sp), '0', '', '') ) .' rupiah') );
                $spreadSheet->getSheet(0)->getCell('A16')->SetValue($this->data->receipt_num);
                $spreadSheet->getSheet(0)->getCell('A17')->SetValue($this->data->surat_penetapan);
                $spreadSheet->getSheet(0)->getCell('A18')->SetValue($this->data->tgl_s_pen);
                $spreadSheet->getSheet(0)->getCell('B18')->SetValue($this->convert_month($this->data->tgl_s_pen) );
                $spreadSheet->getSheet(0)->getCell('A19')->SetValue((date('d-m-Y', strtotime('+'. ($this->data->tgl_jatuh_tmp - 1). ' day', strtotime($this->data->tgl_sp) ) ) ) );
                $spreadSheet->getSheet(0)->getCell('B19')->SetValue($this->convert_month( (date('d-m-Y', strtotime('+'. ($this->data->tgl_jatuh_tmp - 1). ' day', strtotime($this->data->tgl_sp ?? $this->data->tgl_s_pen) ) ) ) ) );

                $nilai_ttd = ($this->data->gd_rekon == 0 ? $this->data->gd_sp : $this->data->gd_rekon);

                if ($nilai_ttd >= 0 && $nilai_ttd <= 200000000)
				{
					$ttd = 'krglbh_dua_jt';
				} elseif ($nilai_ttd > 200000000 && $nilai_ttd <= 500000000) {
					$ttd = 'antr_dualima_jt';
				} else {
					$ttd = 'lbh_lima_jt';
				}

				$data_ttd = ReportModel::get_dokumen_id($ttd, $this->data->pekerjaan, $this->data->witel);

                $spreadSheet->getSheet(0)->getCell('A20')->SetValue($data_ttd->user);
                $spreadSheet->getSheet(0)->getCell('A21')->SetValue($data_ttd->jabatan);
                $spreadSheet->getSheet(0)->getCell('B21')->SetValue($data_ttd->nik);
                $spreadSheet->getSheet(0)->getCell('A22')->SetValue($this->data->surat_kesanggupan);
                $spreadSheet->getSheet(0)->getCell('B22')->SetValue($this->data->tgl_surat_sanggup);
                $spreadSheet->getSheet(0)->getCell('C22')->SetValue($this->convert_month($this->data->tgl_surat_sanggup) );
                $spreadSheet->getSheet(0)->getCell('A23')->SetValue($this->data->ba_rekon);
                $spreadSheet->getSheet(0)->getCell('B23')->SetValue($this->data->tgl_ba_rekon);
                $spreadSheet->getSheet(0)->getCell('C23')->SetValue($this->convert_month($this->data->tgl_ba_rekon) );
                $spreadSheet->getSheet(0)->getCell('A24')->SetValue($this->data->total_material_rekon);
                $spreadSheet->getSheet(0)->getCell('B24')->SetValue($this->data->total_jasa_rekon);
                $spreadSheet->getSheet(0)->getCell('A25')->SetValue($this->data->gd_rekon);
                $spreadSheet->getSheet(0)->getCell('B25')->SetValue($this->data->gd_rekon * $pph);
                $spreadSheet->getSheet(0)->getCell('C25')->SetValue(ucwords($this->terbilang($this->data->gd_rekon) .' rupiah') );
                $spreadSheet->getSheet(0)->getCell('D25')->SetValue(ucwords($this->terbilang(number_format($this->data->gd_rekon + $this->data->gd_rekon * $pph, '0', '', '') ) .' rupiah') );
                $spreadSheet->getSheet(0)->getCell('A26')->SetValue($this->data->BAST);
                $spreadSheet->getSheet(0)->getCell('B26')->SetValue($this->data->tgl_bast);
                $spreadSheet->getSheet(0)->getCell('C26')->SetValue($this->convert_month($this->data->tgl_bast) );
                $spreadSheet->getSheet(0)->getCell('A27')->SetValue(ucwords($this->only_day(date('Y-m-d', strtotime($this->data->tgl_bast) ) ) ) );
                $spreadSheet->getSheet(0)->getCell('B27')->SetValue(ucwords($this->terbilang(date('d', strtotime($this->data->tgl_bast) ) ) ) );
                $spreadSheet->getSheet(0)->getCell('C27')->SetValue(ucwords($bulan_angk[date('m', strtotime($this->data->tgl_bast) )] ));
                $spreadSheet->getSheet(0)->getCell('D27')->SetValue(ucwords($this->terbilang(date('Y', strtotime($this->data->tgl_bast) ) ) ) );
                $spreadSheet->getSheet(0)->getCell('E27')->SetValue(date('d', strtotime($this->data->tgl_bast) ) );
                $spreadSheet->getSheet(0)->getCell('F27')->SetValue(date('m', strtotime($this->data->tgl_bast) ) );
                $spreadSheet->getSheet(0)->getCell('G27')->SetValue(date('Y', strtotime($this->data->tgl_bast) ) );
                $spreadSheet->getSheet(0)->getCell('A28')->SetValue($this->data->BAUT);
                $spreadSheet->getSheet(0)->getCell('B28')->SetValue($this->data->tgl_baut);
                $spreadSheet->getSheet(0)->getCell('C28')->SetValue($this->convert_month($this->data->tgl_baut) );

                $data_pu = ReportModel::get_dokumen_id('sm_krj', $this->data->pekerjaan, $this->data->witel);
                $spreadSheet->getSheet(0)->getCell('A29')->SetValue($data_pu->user);
                $spreadSheet->getSheet(0)->getCell('B29')->SetValue($data_pu->jabatan);

                $data_pu = ReportModel::get_dokumen_id('mngr_krj', $this->data->pekerjaan, $this->data->witel);
				$project_nama = $data_pu->user;
				$project_jab = $data_pu->jabatan;

                $spreadSheet->getSheet(0)->getCell('A30')->SetValue($project_nama);
                $spreadSheet->getSheet(0)->getCell('B30')->SetValue($project_jab);

                $data_pu = ReportModel::get_dokumen_id('gm', $this->data->pekerjaan, $this->data->witel);

                $spreadSheet->getSheet(0)->getCell('A31')->SetValue($data_pu->user);
                $spreadSheet->getSheet(0)->getCell('B31')->SetValue($data_pu->jabatan);
                $spreadSheet->getSheet(0)->getCell('C31')->SetValue($data_pu->nik);
                $spreadSheet->getSheet(0)->getCell('A32')->SetValue($this->data->ba_abd);
                $spreadSheet->getSheet(0)->getCell('B32')->SetValue($this->data->tgl_ba_abd);
                $spreadSheet->getSheet(0)->getCell('C32')->SetValue($this->convert_month($this->data->tgl_ba_abd) );
                $spreadSheet->getSheet(0)->getCell('A33')->SetValue($this->data->ba_rekon);
                $spreadSheet->getSheet(0)->getCell('B33')->SetValue($this->data->tgl_ba_rekon);
                $spreadSheet->getSheet(0)->getCell('C33')->SetValue($this->convert_month($this->data->tgl_ba_rekon) );
                $spreadSheet->getSheet(0)->getCell('A34')->SetValue(ucwords($this->only_day( (date('d-m-Y', strtotime('+'. ($this->data->tgl_jatuh_tmp - 1). ' day', strtotime($this->data->tgl_sp) ) ) ) ) ) );
                $spreadSheet->getSheet(0)->getCell('B34')->SetValue(ucwords($this->terbilang(date('d', strtotime('+'. ($this->data->tgl_jatuh_tmp - 1). ' day', strtotime($this->data->tgl_sp) ) ) ) ) );
                $spreadSheet->getSheet(0)->getCell('C34')->SetValue(ucwords($bulan_angk[date('m', strtotime('+'. ($this->data->tgl_jatuh_tmp - 1). ' day', strtotime($this->data->tgl_sp) ) )]) );
                $spreadSheet->getSheet(0)->getCell('D34')->SetValue(ucwords($this->terbilang(date('Y', strtotime('+'. ($this->data->tgl_jatuh_tmp - 1). ' day', strtotime($this->data->tgl_sp) ) ) ) ) );
                $spreadSheet->getSheet(0)->getCell('E34')->SetValue(date('d', strtotime('+'. ($this->data->tgl_jatuh_tmp - 1). ' day', strtotime($this->data->tgl_sp) ) ) );
                $spreadSheet->getSheet(0)->getCell('G34')->SetValue(date('Y', strtotime('+'. ($this->data->tgl_jatuh_tmp - 1). ' day', strtotime($this->data->tgl_sp) ) ) );
                $spreadSheet->getSheet(0)->getCell('A35')->SetValue($this->data->amd_sp );
                $spreadSheet->getSheet(0)->getCell('A36')->SetValue(number_format($this->data->total_material_rekon, 0, '.', '.') );
                $spreadSheet->getSheet(0)->getCell('A37')->SetValue(number_format($this->data->total_jasa_rekon, 0, '.', '.') );
                $spreadSheet->getSheet(0)->getCell('A38')->SetValue(number_format($this->data->gd_rekon, 0, '.', '.') );
                $spreadSheet->getSheet(0)->getCell('A39')->SetValue(number_format($this->data->gd_rekon * $pph, 0, '.', '.') );
                $spreadSheet->getSheet(0)->getCell('A40')->SetValue(number_format($this->data->total_material_sp, 0, '.', '.') );
                $spreadSheet->getSheet(0)->getCell('A41')->SetValue(number_format($this->data->total_jasa_sp, 0, '.', '.') );
                $spreadSheet->getSheet(0)->getCell('A42')->SetValue(number_format($this->data->gd_sp, 0, '.', '.') );
                $spreadSheet->getSheet(0)->getCell('A43')->SetValue(number_format( ($this->data->gd_sp * $pph_sp), 0, '.', '.') );
                $spreadSheet->getSheet(0)->getCell('A44')->SetValue($this->data->invoice );
                $spreadSheet->getSheet(0)->getCell('A46')->SetValue(number_format($this->data->gd_rekon + ($this->data->gd_rekon * $pph), 0, '.', '.'));
                $spreadSheet->getSheet(0)->getCell('A47')->SetValue(ucwords($this->only_day( (date('d-m-Y',  strtotime($this->data->tgl_ba_abd) ) ) ) ) );
                $spreadSheet->getSheet(0)->getCell('B47')->SetValue(ucwords($this->terbilang(date('d',  strtotime($this->data->tgl_ba_abd) ) ) ) );
                $spreadSheet->getSheet(0)->getCell('C47')->SetValue(ucwords($bulan_angk[date('m',  strtotime($this->data->tgl_ba_abd) ) ]) );
                $spreadSheet->getSheet(0)->getCell('D47')->SetValue(ucwords($this->terbilang(date('Y',  strtotime($this->data->tgl_ba_abd) ) ) ) );
                $spreadSheet->getSheet(0)->getCell('E47')->SetValue(date('d', strtotime($this->data->tgl_ba_abd) ) );
                $spreadSheet->getSheet(0)->getCell('F47')->SetValue(date('m', strtotime($this->data->tgl_ba_abd) ) );
                $spreadSheet->getSheet(0)->getCell('G47')->SetValue(date('Y', strtotime($this->data->tgl_ba_abd) ) );
                $spreadSheet->getSheet(0)->getCell('A48')->SetValue($this->data2->telp );
                $spreadSheet->getSheet(0)->getCell('A49')->SetValue("Pada hari ini ". ucwords($this->only_day( (date('d-m-Y', strtotime('- 1 day', strtotime($this->data->tgl_s_pen) ) ) ) ) ) ." tanggal ". ucwords($this->terbilang(date('d', strtotime('- 1 day', strtotime($this->data->tgl_s_pen) ) ) ) ) ." bulan ". ucwords($bulan_angk[date('m', strtotime('- 1 day', strtotime($this->data->tgl_s_pen) ) )] ) ." Tahun ". ucwords($this->terbilang(date('Y', strtotime('- 1 day', strtotime($this->data->tgl_s_pen) ) ) ) ) .", (". date('d/m/Y', strtotime('- 1 day', strtotime($this->data->tgl_s_pen) ) ) .")" );
                $spreadSheet->getSheet(0)->getCell('B49')->SetValue("Pada hari ini ". ucwords($this->only_day( (date('d-m-Y', strtotime($this->data->tgl_s_pen) ) ) ) ) ." tanggal ". ucwords($this->terbilang(date('d', strtotime($this->data->tgl_s_pen) ) ) ) ." bulan ". ucwords($bulan_angk[date('m', strtotime($this->data->tgl_s_pen) )] ) ." Tahun ". ucwords($this->terbilang(date('Y', strtotime($this->data->tgl_s_pen) ) ) ) .", (". date('d/m/Y', strtotime($this->data->tgl_s_pen) ) .")" );
                $spreadSheet->getSheet(0)->getCell('A50')->SetValue(ucwords($get_lok->witel) );
                $spreadSheet->getSheet(0)->getCell('A51')->SetValue(ucwords($this->data2->lokasi_comp) );
                $spreadSheet->getSheet(0)->getCell('A52')->SetValue(ucwords($this->data->jenis_kontrak) );
                $spreadSheet->getSheet(0)->getCell('B52')->SetValue(ucwords(($this->data->jenis_kontrak == 'Kontrak Putus' ? 'kontrak' : 'surat pesanan')) );
                $spreadSheet->getSheet(0)->getCell('A53')->SetValue($this->data->id_project);

                $nik_pab = ReportModel::get_dokumen_id('osm', $this->data->pekerjaan, $this->data->witel);
                $spreadSheet->getSheet(0)->getCell('A54')->SetValue($nik_pab->user);
                $spreadSheet->getSheet(0)->getCell('B54')->SetValue($nik_pab->jabatan);
                $spreadSheet->getSheet(0)->getCell('C54')->SetValue($nik_pab->nik);

                $nik_pam = ReportModel::get_dokumen_id('m_fin', $this->data->pekerjaan, $this->data->witel);
                $spreadSheet->getSheet(0)->getCell('A55')->SetValue($nik_pam->user);
                $spreadSheet->getSheet(0)->getCell('B55')->SetValue($nik_pam->jabatan);
                $spreadSheet->getSheet(0)->getCell('C55')->SetValue($nik_pam->nik);
                $spreadSheet->getSheet(0)->getCell('A56')->SetValue($this->data2->pph);

                $nilai_ttd = $this->data->gd_rekon;

                if ($nilai_ttd >= 0 && $nilai_ttd <= 200000000)
				{
					$ttd = 'krglbh_dua_jt';
				} elseif ($nilai_ttd > 200000000 && $nilai_ttd <= 500000000) {
					$ttd = 'antr_dualima_jt';
				} else {
					$ttd = 'lbh_lima_jt';
				}

				$data_ttd = ReportModel::get_dokumen_id($ttd, $this->data->pekerjaan, $this->data->witel);

                $spreadSheet->getSheet(0)->getCell('A59')->SetValue($data_ttd->user);
                $spreadSheet->getSheet(0)->getCell('A60')->SetValue($data_ttd->jabatan);
                $spreadSheet->getSheet(0)->getCell('B60')->SetValue($data_ttd->nik);

                $data_waspang = ReportModel::get_proc_user($this->data->waspang_id);

				if($data_waspang)
				{
					$waspang_nama = $data_waspang->user;
					$waspang_nik  = $data_waspang->jabatan;
				}
				else
				{
					$waspang_nama = '';
					$waspang_nik  = '';
				}

                $spreadSheet->getSheet(0)->getCell('A61')->SetValue($waspang_nama);
                $spreadSheet->getSheet(0)->getCell('A62')->SetValue($waspang_nik);

                $spreadSheet->getSheet(0)->getCell('A63')->SetValue(ucwords($this->only_day( (date('d-m-Y',  strtotime($this->data->tgl_baut) ) ) ) ) );
                $spreadSheet->getSheet(0)->getCell('B63')->SetValue(ucwords($this->terbilang(date('d',  strtotime($this->data->tgl_baut) ) ) ) );
                $spreadSheet->getSheet(0)->getCell('C63')->SetValue(ucwords($bulan_angk[date('m',  strtotime($this->data->tgl_baut) ) ]) );
                $spreadSheet->getSheet(0)->getCell('D63')->SetValue(ucwords($this->terbilang(date('Y',  strtotime($this->data->tgl_baut) ) ) ) );
                $spreadSheet->getSheet(0)->getCell('E63')->SetValue(date('d', strtotime($this->data->tgl_baut) ) );
                $spreadSheet->getSheet(0)->getCell('F63')->SetValue(date('m', strtotime($this->data->tgl_baut) ) );
                $spreadSheet->getSheet(0)->getCell('G63')->SetValue(date('Y', strtotime($this->data->tgl_baut) ) );

                $pid = DB::Table('procurement_req_pid As prp')
                ->leftjoin('procurement_pid As pp', 'pp.id', '=', 'prp.id_pid_req')
                ->select('pp.pid')
                ->where('id_upload', $this->data->id)
                ->get();

                foreach($pid as $v)
                {
                    $pid_all[$v->pid] = $v->pid;
                }
                // dd(implode(', ', $pid_all) );
                $spreadSheet->getSheet(0)->getCell('A57')->SetValue(implode(', ', $pid_all) );

                $khs = AdminModel::find_work($this->data->pekerjaan);
                $spreadSheet->getSheet(0)->getCell('A58')->SetValue(html_entity_decode($khs->uraian, ENT_QUOTES, 'UTF-8') );

                $data_boq_sp = DB::SELECT("SELECT pbd.*, pd.uraian, pd.satuan, pbd.material as material_default, pbd.jasa as jasa_default, pbl.sto, pbl.lokasi, pbl.pid_sto
                FROM procurement_boq_upload pbu
                LEFT JOIN procurement_Boq_lokasi pbl ON pbu.id = pbl.id_upload
                LEFT JOIN procurement_Boq_design pbd ON pbl.id = pbd.id_boq_lokasi
                LEFT JOIN procurement_designator pd ON pd.id = pbd.id_design
                LEFT JOIN procurement_mitra pm ON pm.id = pbu.mitra_id
                WHERE pbu.id = ".$this->data->id." AND pbd.status_design = 0 AND pd.active = 1 AND pbu.active = 1 GROUP BY pbd.id");
                $data_bq = $data_tot = [];

                foreach($data_boq_sp as $key => $val)
                {
                    if(!isset($data_tot['sp']['total']) )
                    {
                        $data_tot['sp']['total']['material_sp']    = 0;
                        $data_tot['sp']['total']['jasa_sp']        = 0;
                        $data_tot['sp']['total']['material_rekon'] = 0;
                        $data_tot['sp']['total']['jasa_rekon']     = 0;
                    }

                    $data_tot['sp']['total']['material_sp']    += intval($val->material) * intval($val->sp);
                    $data_tot['sp']['total']['jasa_sp']        += intval($val->jasa) * intval($val->sp);
                    $data_tot['sp']['total']['material_rekon'] += intval($val->material) * intval($val->rekon);
                    $data_tot['sp']['total']['jasa_rekon']     += intval($val->jasa) * intval($val->rekon);

                    $data_bq['sp'][$val->id_boq_lokasi]['lokasi']      = $val->lokasi;
                    $data_bq['sp'][$val->id_boq_lokasi]['sto']         = $val->sto;
                    $data_bq['sp'][$val->id_boq_lokasi]['pid_sto_wbs'] = $val->pid_sto;

                    $pid_lok = DB::Table('procurement_req_pid As prp')
                    ->leftjoin('procurement_pid As pp', 'pp.id', '=', 'prp.id_pid_req')
                    ->select('pp.pid')
                    ->where('id_lokasi', $val->id_boq_lokasi)
                    ->get();

                    foreach($pid_lok as $v)
                    {
                        $pid_per_lok[$v->pid] = $v->pid;
                    }

                    $data_bq['sp'][$val->id_boq_lokasi]['pid_sto']                  = implode(', ', $pid_per_lok);
                    $data_bq['sp'][$val->id_boq_lokasi]['list'][$key]['id_design']  = $val->id_design;
                    $data_bq['sp'][$val->id_boq_lokasi]['list'][$key]['designator'] = $val->designator;
                    $data_bq['sp'][$val->id_boq_lokasi]['list'][$key]['uraian']     = $val->uraian;
                    $data_bq['sp'][$val->id_boq_lokasi]['list'][$key]['satuan']     = $val->satuan;
                    $data_bq['sp'][$val->id_boq_lokasi]['list'][$key]['material']   = intval($val->material);
                    $data_bq['sp'][$val->id_boq_lokasi]['list'][$key]['jasa']       = intval($val->jasa);
                    $data_bq['sp'][$val->id_boq_lokasi]['list'][$key]['sp']         = intval($val->sp);
                    $data_bq['sp'][$val->id_boq_lokasi]['list'][$key]['rekon']      = 0;
                    $data_bq['sp'][$val->id_boq_lokasi]['list'][$key]['tambah']     = 0;
                    $data_bq['sp'][$val->id_boq_lokasi]['list'][$key]['kurang']     = 0;
                }

                $data_boq_rekon = DB::SELECT("SELECT pbd.*, pd.uraian, pd.satuan, pbd.material as material_default, pbd.jasa as jasa_default, pbl.sto, pbl.lokasi, pbl.pid_sto
                FROM procurement_boq_upload pbu
                LEFT JOIN procurement_Boq_lokasi pbl ON pbu.id = pbl.id_upload
                LEFT JOIN procurement_Boq_design pbd ON pbl.id = pbd.id_boq_lokasi
                LEFT JOIN procurement_designator pd ON pd.id = pbd.id_design
                LEFT JOIN procurement_mitra pm ON pm.id = pbu.mitra_id
                WHERE pbu.id = ".$this->data->id." AND pbd.status_design = 1 AND pd.active = 1 AND pbu.active = 1 GROUP BY pbd.id");

                foreach($data_boq_rekon as $key => $val)
                {
                    if(!isset($data_tot['rekon']['total']) )
                    {
                        $data_tot['rekon']['total']['material_sp']     = 0;
                        $data_tot['rekon']['total']['jasa_sp']         = 0;
                        $data_tot['rekon']['total']['material_rekon']  = 0;
                        $data_tot['rekon']['total']['jasa_rekon']      = 0;
                        $data_tot['rekon']['total']['material_tambah'] = 0;
                        $data_tot['rekon']['total']['jasa_tambah']     = 0;
                        $data_tot['rekon']['total']['material_kurang'] = 0;
                        $data_tot['rekon']['total']['jasa_kurang']     = 0;
                    }

                    $data_tot['rekon']['total']['material_sp']     += intval($val->material) * intval($val->sp);
                    $data_tot['rekon']['total']['jasa_sp']         += intval($val->jasa) * intval($val->sp);
                    $data_tot['rekon']['total']['material_rekon']  += intval($val->material) * intval($val->rekon);
                    $data_tot['rekon']['total']['jasa_rekon']      += intval($val->jasa) * intval($val->rekon);
                    $data_tot['rekon']['total']['material_tambah'] += intval($val->material) * intval($val->tambah);
                    $data_tot['rekon']['total']['jasa_tambah']     += intval($val->jasa) * intval($val->tambah);
                    $data_tot['rekon']['total']['material_kurang'] += intval($val->material) * intval($val->kurang);
                    $data_tot['rekon']['total']['jasa_kurang'] +=  intval($val->jasa) * intval($val->kurang);

                    $data_bq['rekon'][$val->id_boq_lokasi]['lokasi']      = $val->lokasi;
                    $data_bq['rekon'][$val->id_boq_lokasi]['sto']         = $val->sto;
                    $data_bq['rekon'][$val->id_boq_lokasi]['pid_sto_wbs'] = $val->pid_sto;

                    $pid_lok = DB::Table('procurement_req_pid As prp')
                    ->leftjoin('procurement_pid As pp', 'pp.id', '=', 'prp.id_pid_req')
                    ->select('pp.pid')
                    ->where('id_lokasi', $val->id_boq_lokasi)
                    ->get();

                    foreach($pid_lok as $v)
                    {
                        $pid_per_lok[$v->pid] = $v->pid;
                    }

                    $data_bq['rekon'][$val->id_boq_lokasi]['pid_sto']                  = implode(', ', $pid_per_lok);
                    $data_bq['rekon'][$val->id_boq_lokasi]['list'][$key]['id_design']  = $val->id_design;
                    $data_bq['rekon'][$val->id_boq_lokasi]['list'][$key]['designator'] = $val->designator;
                    $data_bq['rekon'][$val->id_boq_lokasi]['list'][$key]['uraian']     = html_entity_decode($val->uraian, ENT_QUOTES, 'UTF-8');
                    $data_bq['rekon'][$val->id_boq_lokasi]['list'][$key]['satuan']     = $val->satuan;
                    $data_bq['rekon'][$val->id_boq_lokasi]['list'][$key]['material']   = intval($val->material);
                    $data_bq['rekon'][$val->id_boq_lokasi]['list'][$key]['jasa']       = intval($val->jasa);
                    $data_bq['rekon'][$val->id_boq_lokasi]['list'][$key]['sp']         = intval($val->sp);
                    $data_bq['rekon'][$val->id_boq_lokasi]['list'][$key]['rekon']      = intval($val->rekon);
                    $data_bq['rekon'][$val->id_boq_lokasi]['list'][$key]['tambah']     = intval($val->tambah);
                    $data_bq['rekon'][$val->id_boq_lokasi]['list'][$key]['kurang']     = intval($val->kurang);
                }

                if(!empty($data_tot['rekon']) )
                {
                    $spreadSheet->getSheet(0)->getCell('C24')->SetValue($data_tot['rekon']['total']['material_tambah']);
                    $spreadSheet->getSheet(0)->getCell('D24')->SetValue($data_tot['rekon']['total']['jasa_tambah']);
                    $spreadSheet->getSheet(0)->getCell('E24')->SetValue($data_tot['rekon']['total']['material_kurang']);
                    $spreadSheet->getSheet(0)->getCell('F24')->SetValue($data_tot['rekon']['total']['jasa_kurang']);
                }

                $sheet =[
                    5  => 'SP',
                    6  => 'LAMPIRAN SP',
                    9  => 'BAUT',
                    10 => 'BA ABD',
                    11 => 'BA REKONSILIASI',
                    12 => 'Rekap BA Rekon',
                    13 => 'BOQ',
                    14 => 'Lamp BA Rekon',
                    15 => 'BAKH SP',
                    16 => 'BAKH REKON',
                ];

                for ($i = 'G'; $i !== 'ZZ'; $i++){
                    $index[] = $i;
                }

                $spreadSheet->getSheet(1)->getStyle('A6')->getAlignment()->setWrapText(true);
                $spreadSheet->getSheet(1)->getRowDimension((6) )->setRowHeight(30);

                $data_bq['sp'] = array_values($data_bq['sp']);
                $data_bq['rekon'] = @array_values($data_bq['rekon']);
                // dd($val);
                foreach($sheet as $title => $val)
                {
                    $k = $spreadSheet->getIndex(
                        $spreadSheet->getSheetByName($val)
                    );

                    if($val == 'SP')
                    {
                        $num5 = 0;
                        $no_lok = 0;

                        foreach($this->datasl as $kk => $v)
                        {
                            $no_lok = $no_lok++;
                            $num5 += 1;
                            // dd($no_lok);
                            $spreadSheet->getSheet($k)->insertNewRowBefore((23 + $no_lok), 1);
                            $spreadSheet->getSheet($k)->getCell('A'. (22 + $num5) )->setValue($v['lokasi']);
                            $spreadSheet->getSheet($k)->getCell('H'. (22 + $num5) )->setValue($v['gd_sp']);

                            $spreadSheet->getSheet($k)->mergeCells('A'. (22 + $num5) . ':'. 'G'. (22 + $num5) );
                            $spreadSheet->getSheet($k)->mergeCells('H'. (22 + $num5) . ':'. 'I'. (22 + $num5) );
                            $spreadSheet->getSheet($k)->mergeCells('J'. (22 + $num5) . ':'. 'L'. (25 + $num5) );

                        }
                    }

                    if($val == 'BAUT')
                    {
                        $num6 = 0;
                        $no_lok = 0;
                        foreach($this->datasl as $kk => $v)
                        {
                            $no_lok = $no_lok++;
                            $num6 += 1;
                            // dd($no_lok);
                            $spreadSheet->getSheet($k)->insertNewRowBefore((16 + $no_lok), 1);
                            $spreadSheet->getSheet($k)->getCell('D'. (15 + $num6) )->setValue($num6);
                            $spreadSheet->getSheet($k)->getCell('F'. (15 + $num6) )->setValue($v['lokasi']);
                            $spreadSheet->getSheet($k)->getCell('K'. (15 + $num6) )->setValue($v['sto']);

                            $spreadSheet->getSheet($k)->mergeCells('D'. (15 + $num6) . ':'. 'E'. (15 + $num6) );
                            $spreadSheet->getSheet($k)->mergeCells('F'. (15 + $num6) . ':'. 'J'. (15 + $num6) );
                            $spreadSheet->getSheet($k)->mergeCells('K'. (15 + $num6) . ':'. 'L'. (15 + $num6) );
                        }
                        // $spreadSheet->getSheet($k)->getStyle('D'. (15 + $num6))->getAlignment()->setWrapText(true);
                        // $spreadSheet->getSheet($k)->getRowDimension( (15 + $num6) )->setRowHeight(35);
                    }

                    if($val == 'BA ABD')
                    {
                        //sheet7
                        $num7 = 0;
                        $no_lok = 0;
                        foreach($this->datasl as $kk => $v)
                        {
                            $num7 += 1;
                            $no_lok = $no_lok++;

                            $spreadSheet->getSheet($k)->insertNewRowBefore((16 + $no_lok), 1);
                            $spreadSheet->getSheet($k)->getCell('B'. (15 + $num7) )->setValue($num7);
                            $spreadSheet->getSheet($k)->getCell('C'. (15 + $num7) )->setValue($v['lokasi']);
                            $spreadSheet->getSheet($k)->getCell('E'. (15 + $num7) )->setValue($v['sto']);
                            $spreadSheet->getSheet($k)->mergeCells('C'. (15 + $num7) . ':'. 'D'. (15 + $num7) );
                            $spreadSheet->getSheet($k)->mergeCells('E'. (15 + $num7) . ':'. 'F'. (15 + $num7) );
                            // $spreadSheet->getSheet($k)->getStyle('F'. (15 + $num7) )->getAlignment()->setWrapText(true);
                            // $spreadSheet->getSheet($k)->getRowDimension( (15 + $num7) )->setRowHeight(35);
                        }
                    }

                    if($val == 'BA REKONSILIASI')
                    {
                        if($this->data3)
                        {
                            $num8 =0;
                            $no_lok = 0;

                            foreach($this->data3 as $kk => $v)
                            {
                                $num8 += 1;
                                $no_lok = $no_lok++;
                                $spreadSheet->getSheet($k)->insertNewRowBefore((18 + $no_lok), 1);
                                $spreadSheet->getSheet($k)->getCell('C'. (17 + $num8) )->setValue(++$kk);
                                $spreadSheet->getSheet($k)->getCell('D'. (17 + $num8) )->setValue($v->lokasi);
                                $spreadSheet->getSheet($k)->getCell('F'. (17 + $num8) )->setValue($v->pid_sto);
                                $spreadSheet->getSheet($k)->getCell('G'. (17 + $num8) )->setValue($v->sto);
                            }

                            $spreadSheet->getSheet($k)->getStyle('D'. (17 + $num8) )->getAlignment()->setWrapText(true);
                            $spreadSheet->getSheet($k)->getStyle('F'. (17 + $num8) )->getAlignment()->setWrapText(true);
                            $spreadSheet->getSheet($k)->getRowDimension( (17 + $num8) )->setRowHeight(61);

                            $spreadSheet->getSheet($k)->mergeCells('D'. (17 + $num8) . ':'. 'E'. (17 + $num8) );
                            $spreadSheet->getSheet($k)->getStyle('C'. (16 + $num8) .':'. 'G'. (17 + $num8) )->applyFromArray($border_Style);
                            $spreadSheet->getSheet($k)->getStyle('C'. (16 + $num8) .':'. 'G'. (17 + $num8) )->getFont()->setBold(true);

                            $no_lok = 0;

                            foreach($this->data3 as $kk => $v)
                            {
                                $bask = $num8 + 1;
                                $no_lok = $no_lok++;

                                $spreadSheet->getSheet($k)->insertNewRowBefore((49 + $num8 + $no_lok), 1);
                                $spreadSheet->getSheet($k)->getCell('C'. (48 + $bask) )->setValue(1);
                                $spreadSheet->getSheet($k)->getCell('D'. (48 + $bask) )->setValue($v->lokasi);
                                $spreadSheet->getSheet($k)->getCell('E'. (48 + $bask) )->setValue( ($v->total_material_rekon != 0 ? 'Rp. '. number_format($v->total_material_rekon, 0, '.', '.') : '') );
                                $spreadSheet->getSheet($k)->getCell('F'. (48 + $bask) )->setValue( ($v->total_jasa_rekon != 0 ? 'Rp. '. number_format($v->total_jasa_rekon, 0, '.', '.') : '') );
                                $spreadSheet->getSheet($k)->getCell('G'. (48 + $bask) )->setValue('Rp. '. number_format($v->gd_rekon, 0, '.', '.') );

                                $num8 = $bask;
                            }

                            $no_lok = 0;

                            foreach($this->data3 as $kk => $v)
                            {
                                $bask = $num8 + 1;
                                $no_lok = $no_lok++;
                                $spreadSheet->getSheet($k)->insertNewRowBefore((57 + $num8 + $no_lok), 1);

                                $spreadSheet->getSheet($k)->getCell('C'. (56 + $bask) )->setValue(1);
                                $spreadSheet->getSheet($k)->getCell('D'. (56 + $bask) )->setValue($v->lokasi);

                                $toc_tgl = date('Y-m-d');

                                if($this->data->tgl_jatuh_tmp && $this->data->tgl_sp)
                                {
                                    $toc_tgl = (date('Y-m-d', strtotime($this->data->tgl_sp .' +'. ($this->data->tgl_jatuh_tmp - 1) .' day' ) ) );
                                }

                                $spreadSheet->getSheet($k)->getCell('E'. (56 + $bask) )->setValue($this->convert_month( (date('d-m-Y', strtotime($toc_tgl) ) ) ) );
                                $spreadSheet->getSheet($k)->getCell('F'. (56 + $bask) )->setValue($this->convert_month( (date('d-m-Y', strtotime($this->data->tgl_baut) ) ) ) );
                                $spreadSheet->getSheet($k)->getCell('G'. (56 + $bask) )->setValue('-');

                                $num8 = $bask;
                            }
                        }
                    }

                    if($val == 'Rekap BA Rekon')
                    {
                        $material_sp2 = $jasa_sp2 = $material_tambah2 = $jasa_tambah2 = $material_kurang2 = $jasa_kurang2 = $material_rekon2 = $jasa_rekon2 = 0;
                        $material_sp3 = $jasa_sp3 = $material_tambah3 = $jasa_tambah3 = $material_kurang3 = $jasa_kurang3 = $material_rekon3 = $jasa_rekon3 = 0;

                        foreach($this->data5 as $v)
                        {
                            if($v->rekon)
                            {
                                if($this->data->bulan_pengerjaan >= '2021-11')
                                {
                                    $spreadSheet->getSheet($k)->getCell('B8')->setValue('Nama Pekerjaan');

                                    $g_design_9['lokasi'][0]['sto'] = $v->sto;
                                    $g_design_9['lokasi'][0]['title'] = $this->data->judul;
                                    $g_design_9['lokasi'][0]['material_sp'] = $material_sp2 += $v->sp * $v->material;
                                    $g_design_9['lokasi'][0]['jasa_sp'] = $jasa_sp2 += $v->sp * $v->jasa;

                                    $g_design_9['lokasi'][0]['material_rekon'] = $material_rekon2 += $v->rekon * $v->material;
                                    $g_design_9['lokasi'][0]['jasa_rekon'] = $jasa_rekon2 += $v->rekon * $v->jasa;

                                    $g_design_9['lokasi'][0]['material_tambah'] = $material_tambah2 += $v->tambah * $v->material;
                                    $g_design_9['lokasi'][0]['jasa_tambah'] = $jasa_tambah2 += $v->tambah * $v->jasa;

                                    $g_design_9['lokasi'][0]['material_kurang'] = $material_kurang2 += $v->kurang * $v->material;
                                    $g_design_9['lokasi'][0]['jasa_kurang'] = $jasa_kurang2 += $v->kurang * $v->jasa;
                                }
                                else
                                {
                                    $spreadSheet->getSheet($k)->getCell('B8')->setValue('Lokasi');

                                    $g_design_9['lokasi'][$v->id_boq_lokasi]['sto'] = $v->sto;
                                    $g_design_9['lokasi'][$v->id_boq_lokasi]['title'] = $v->lokasi;
                                    $g_design_9['lokasi'][$v->id_boq_lokasi]['material_sp'] = $material_sp2 += $v->sp * $v->material;
                                    $g_design_9['lokasi'][$v->id_boq_lokasi]['jasa_sp'] = $jasa_sp2 += $v->sp * $v->jasa;

                                    $g_design_9['lokasi'][$v->id_boq_lokasi]['material_rekon'] = $material_rekon2 += $v->rekon * $v->material;
                                    $g_design_9['lokasi'][$v->id_boq_lokasi]['jasa_rekon'] = $jasa_rekon2 += $v->rekon * $v->jasa;

                                    $g_design_9['lokasi'][$v->id_boq_lokasi]['material_tambah'] = $material_tambah2 += $v->tambah * $v->material;
                                    $g_design_9['lokasi'][$v->id_boq_lokasi]['jasa_tambah'] = $jasa_tambah2 += $v->tambah * $v->jasa;

                                    $g_design_9['lokasi'][$v->id_boq_lokasi]['material_kurang'] = $material_kurang2 += $v->kurang * $v->material;
                                    $g_design_9['lokasi'][$v->id_boq_lokasi]['jasa_kurang'] = $jasa_kurang2 += $v->kurang * $v->jasa;
                                }

                                $g_design_9['total']['material_sp'] = $material_sp3 += $v->sp * $v->material;
                                $g_design_9['total']['jasa_sp'] = $jasa_sp3 += $v->sp * $v->jasa;

                                $g_design_9['total']['material_rekon'] = $material_rekon3 += $v->rekon * $v->material;
                                $g_design_9['total']['jasa_rekon'] = $jasa_rekon3 += $v->rekon * $v->jasa;

                                $g_design_9['total']['material_tambah'] = $material_tambah3 += $v->tambah * $v->material;
                                $g_design_9['total']['jasa_tambah'] = $jasa_tambah3 += $v->tambah * $v->jasa;

                                $g_design_9['total']['material_kurang'] = $material_kurang3 += $v->kurang * $v->material;
                                $g_design_9['total']['jasa_kurang'] = $jasa_kurang3 += $v->kurang * $v->jasa;
                            }
                        }

                        if(!empty($g_design_9) )
                        {
                            $raw_num9 = 0;
                            $no_lok = 0;
                            // dd($g_design_9);
                            foreach($g_design_9['lokasi'] as $v)
                            {
                                $no_lok = $no_lok++;
                                $num9 = ++$raw_num9;
                                $spreadSheet->getSheet($k)->insertNewRowBefore((11 + $no_lok), 1);

                                $spreadSheet->getSheet($k)->getCell('A'. (10 + $num9) )->setValue($num9);
                                $spreadSheet->getSheet($k)->getCell('B'. (10 + $num9) )->setValue($v['title']);
                                $spreadSheet->getSheet($k)->getStyle('B'. (10 + $num9) )->getAlignment()->setWrapText(true);
                                $spreadSheet->getSheet($k)->getRowDimension( (10 + $num9) )->setRowHeight(-1);

                                $spreadSheet->getSheet($k)->getCell('C'. (10 + $num9) )->setValue($v['material_sp']);
                                $spreadSheet->getSheet($k)->getCell('D'. (10 + $num9) )->setValue($v['jasa_sp']);
                                $spreadSheet->getSheet($k)->getCell('E'. (10 + $num9) )->setValue($v['material_sp'] + $v['jasa_sp']);

                                $spreadSheet->getSheet($k)->getCell('F'. (10 + $num9) )->setValue($v['material_rekon']);
                                $spreadSheet->getSheet($k)->getCell('G'. (10 + $num9) )->setValue($v['jasa_rekon']);
                                $spreadSheet->getSheet($k)->getCell('H'. (10 + $num9) )->setValue($v['material_rekon'] + $v['jasa_rekon']);

                                $spreadSheet->getSheet($k)->getCell('I'. (10 + $num9) )->setValue($v['material_tambah']);
                                $spreadSheet->getSheet($k)->getCell('J'. (10 + $num9) )->setValue($v['jasa_tambah']);
                                $spreadSheet->getSheet($k)->getCell('K'. (10 + $num9) )->setValue($v['material_tambah'] + $v['jasa_tambah']);

                                $spreadSheet->getSheet($k)->getCell('L'. (10 + $num9) )->setValue($v['material_kurang']);
                                $spreadSheet->getSheet($k)->getCell('M'. (10 + $num9) )->setValue($v['jasa_kurang']);
                                $spreadSheet->getSheet($k)->getCell('N'. (10 + $num9) )->setValue($v['material_kurang'] + $v['jasa_kurang']);
                                // dd($g_design_9);
                                $spreadSheet->getSheet($k)->getCell('C'. (12 + $num9) )->setValue($v['material_sp']);
                                $spreadSheet->getSheet($k)->getCell('D'. (12 + $num9) )->setValue($v['jasa_sp']);
                                $spreadSheet->getSheet($k)->getCell('E'. (12 + $num9) )->setValue($v['material_sp'] + $v['jasa_sp']);

                                $spreadSheet->getSheet($k)->getCell('F'. (12 + $num9) )->setValue($v['material_rekon']);
                                $spreadSheet->getSheet($k)->getCell('G'. (12 + $num9) )->setValue($v['jasa_rekon']);
                                $spreadSheet->getSheet($k)->getCell('H'. (12 + $num9) )->setValue($v['material_rekon'] + $v['jasa_rekon']);

                                $spreadSheet->getSheet($k)->getCell('I'. (12 + $num9) )->setValue($v['material_tambah']);
                                $spreadSheet->getSheet($k)->getCell('J'. (12 + $num9) )->setValue($v['jasa_tambah']);
                                $spreadSheet->getSheet($k)->getCell('K'. (12 + $num9) )->setValue($v['material_tambah'] + $v['jasa_tambah']);

                                $spreadSheet->getSheet($k)->getCell('L'. (12 + $num9) )->setValue($v['material_kurang']);
                                $spreadSheet->getSheet($k)->getCell('M'. (12 + $num9) )->setValue($v['jasa_kurang']);
                                $spreadSheet->getSheet($k)->getCell('N'. (12 + $num9) )->setValue($v['material_kurang'] + $v['jasa_kurang']);
                            }
                        }
                    }

                    if($val == 'LAMPIRAN SP')
                    {
                        $num_lamp = 0;
                        $tot_sp = [];
                        $tot_all = [];

                        if($data_bq)
                        {
                            $spreadSheet->getSheet($k)->insertNewColumnBefore('G', count($data_bq['sp']) );
                            $spreadSheet->getSheet($k)->insertNewRowBefore(11, count($data_bq['sp'][0]['list']) );

                            foreach($data_bq['sp'][0]['list'] as $kk => $vv)
                            {
                                $num_lamp = ++$kk;
                                $spreadSheet->getSheet($k)->getCell('A'. (10 + $num_lamp) )->setValue($num_lamp);
                                $spreadSheet->getSheet($k)->getCell('B'. (10 + $num_lamp) )->setValue($vv['designator']);
                                $spreadSheet->getSheet($k)->getCell('C'. (10 + $num_lamp) )->setValue($vv['uraian']);
                                $spreadSheet->getSheet($k)->getCell('D'. (10 + $num_lamp) )->setValue($vv['satuan']);
                                $spreadSheet->getSheet($k)->getCell('E'. (10 + $num_lamp) )->setValue($vv['material'] != 0 ? $vv['material'] : '-');
                                $spreadSheet->getSheet($k)->getCell('F'. (10 + $num_lamp) )->setValue($vv['jasa'] != 0 ? $vv['jasa'] : '-');
                            }

                            $num_lamp = 0;
                            $num_lamp_head =0;

                            foreach($data_bq['sp'] as $k1 => $v2)
                            {
                                $num_lamp_head = $num_lamp_head++;
                                $spreadSheet->getSheet($k)->getCell($index[$k1 + $num_lamp_head].''. 8 )->setValue($v2['sto']);
                                $spreadSheet->getSheet($k)->getCell($index[$k1 + $num_lamp_head].''. 9 )->setValue($v2['lokasi']);
                                $spreadSheet->getSheet($k)->getCell($index[$k1 + $num_lamp_head].''. 10 )->setValue($v2['pid_sto']);
                                $spreadSheet->getSheet($k)->getStyle($index[$k1 + $num_lamp_head].''. 8  .':'. $index[$k1 + $num_lamp_head].''. 10 )->applyFromArray($border_Style);
                                $spreadSheet->getSheet($k)->getStyle($index[$k1 + $num_lamp_head].''. 8  .':'. $index[$k1 + $num_lamp_head].''. 10 )->applyFromArray([
                                    'fill' => [
                                        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                                        'startColor' => [
                                            'argb' => 'ffff00',
                                        ],
                                    ]
                                ]);
                                // $spreadSheet->getSheet($k)->mergeCells($index[$k1 + $num_lamp_head].''. 8 . ':'. $index[$k1 + $num_lamp_head].''. 9 );
                                $v2['list'] = array_values($v2['list']);
                                foreach($v2['list'] as $k2 => $v3)
                                {
                                    $num_lamp = ++$k2;
                                    $spreadSheet->getSheet($k)->getCell($index[$k1].''. (10 + $num_lamp) )->setValue($v3['sp']);

                                    if(!isset($tot_sp[$k1]) )
                                    {
                                        $tot_sp[$k1]['material_sp'] = 0;
                                        $tot_sp[$k1]['jasa_sp'] = 0;
                                    }

                                    $tot_sp[$k1]['material_sp'] += $v3['sp'] * $v3['material'];
                                    $tot_sp[$k1]['jasa_sp'] += $v3['sp'] * $v3['jasa'];

                                    if(!isset($tot_all[($num_lamp + 10)]) )
                                    {
                                        $tot_all[($num_lamp + 10)] = 0;
                                    }

                                    $tot_all[($num_lamp + 10)] += $v3['sp'];
                                }

                                $spreadSheet->getSheet($k)->getCell($index[$k1].''. (11 + $num_lamp) )->setValue($tot_sp[$k1]['material_sp'] != 0 ? $tot_sp[$k1]['material_sp'] : '-');
                                $spreadSheet->getSheet($k)->getCell($index[$k1].''. (12 + $num_lamp) )->setValue($tot_sp[$k1]['jasa_sp'] != 0 ? $tot_sp[$k1]['jasa_sp'] : '-');

                                $total_sp = $tot_sp[$k1]['jasa_sp'] + $tot_sp[$k1]['material_sp'];

                                $spreadSheet->getSheet($k)->getCell($index[$k1].''. (13 + $num_lamp) )->setValue($total_sp != 0 ? $total_sp: '-');
                                $spreadSheet->getSheet($k)->getCell($index[$k1].''. (14 + $num_lamp) )->setValue($total_sp != 0 ? $total_sp * $pph_sp : '-');
                                $spreadSheet->getSheet($k)->getCell($index[$k1].''. (15 + $num_lamp) )->setValue($total_sp != 0 ? $total_sp + ($total_sp * $pph_sp ) : '-');
                            }

                            foreach($tot_all as $key => $val)
                            {
                                $spreadSheet->getSheet($k)->getCell($index[($k1 + 1)].''. $key )->setValue($val);
                            }
                            // dd($data_tot);
                            $spreadSheet->getSheet($k)->getCell($index[($k1 + 1)].''. (11 + $num_lamp) )->setValue($data_tot['sp']['total']['material_sp'] != 0 ? $data_tot['sp']['total']['material_sp'] : '-');
                            $spreadSheet->getSheet($k)->getCell($index[($k1 + 1)].''. (12 + $num_lamp) )->setValue($data_tot['sp']['total']['jasa_sp'] != 0 ? $data_tot['sp']['total']['jasa_sp'] : '-');

                            $total_all_sp = $data_tot['sp']['total']['jasa_sp'] + $data_tot['sp']['total']['material_sp'];

                            $spreadSheet->getSheet($k)->getCell($index[($k1 + 1)].''. (13 + $num_lamp) )->setValue($total_all_sp != 0 ? $total_all_sp : '-');
                            $spreadSheet->getSheet($k)->getCell($index[($k1 + 1)].''. (14 + $num_lamp) )->setValue($total_all_sp != 0 ? $total_all_sp * $pph_sp : '-');
                            $spreadSheet->getSheet($k)->getCell($index[($k1 + 1)].''. (15 + $num_lamp) )->setValue($total_all_sp != 0 ? $total_all_sp + ($total_all_sp * $pph_sp) : '-');
                        }
                    }

                    if($val == 'BAKH')
                    {
                        $num_lamp = 0;
                        $tot_sp = $data_design_bq = [];

                        foreach($this->data5 as $kk => $val)
                        {
                            if($val->jenis_khs != 16)
                            {
                                $data_design_bq[$val->designator] = $val;
                            }
                        }

                        $data_design_bq = array_values($data_design_bq);
                        // dd($data_design_bq);
                        if($data_design_bq)
                        {
                            $spreadSheet->getSheet($k)->insertNewRowBefore(16, count($data_design_bq) );
                            $no_num = 0;

                            foreach($data_design_bq as $v2)
                            {
                                $num_lamp = ++$no_num;
                                $spreadSheet->getSheet($k)->getCell('C'. (15 + $num_lamp) )->setValue($num_lamp);
                                $spreadSheet->getSheet($k)->getCell('D'. (15 + $num_lamp) )->setValue($v2->designator);
                                $spreadSheet->getSheet($k)->getCell('E'. (15 + $num_lamp) )->setValue(html_entity_decode($v2->uraian, ENT_QUOTES, 'UTF-8') );
                                $spreadSheet->getSheet($k)->getRowDimension( (15 + $num_lamp) )->setRowHeight(-1);
                                $spreadSheet->getSheet($k)->getCell('F'. (15 + $num_lamp) )->setValue($v2->satuan);
                                $spreadSheet->getSheet($k)->getCell('G'. (15 + $num_lamp) )->setValue($v2->material);
                                $spreadSheet->getSheet($k)->mergeCells('G'. (15 + $num_lamp) . ':'. 'H'. (15 + $num_lamp) );
                                $spreadSheet->getSheet($k)->getCell('I'. (15 + $num_lamp) )->setValue($v2->jasa);
                                $spreadSheet->getSheet($k)->mergeCells('I'. (15 + $num_lamp) . ':'. 'J'. (15 + $num_lamp) );
                            }
                        }
                    }

                    if($val == 'BOQ')
                    {
                        $num_boq_tb = 0;
                        $tot_rekon = $tot_all = $unused = $get_unused = [];

                        foreach($data_bq['rekon'] As $v)
                        {
                            foreach($v['list'] As $vv)
                            {
                                $unused[$vv['id_design'] ]['id_mat'] = $vv['id_design'];

                                if(!isset($unused[$vv['id_design'] ]['mat'] ) )
                                {
                                    $unused[$vv['id_design'] ]['mat'] = 0;
                                }

                                $unused[$vv['id_design'] ]['mat'] += $vv['rekon'];
                            }
                        }

                        foreach($unused as $v)
                        {
                            if($v['mat'] == 0)
                            {
                                $get_unused = $v;
                            }
                        }

                        if($get_unused)
                        {
                            foreach($data_bq['rekon'] As $kv => $v)
                            {
                                foreach($v['list'] As $kk => &$vv)
                                {
                                    $search_unused = array_search($vv['id_design'], array_column($get_unused, 'id_mat') );
                                    if($search_unused !== FALSE)
                                    {
                                        unset($data_bq['rekon'][$kv]['list'][$kk]);
                                    }
                                }
                            }
                        }

                        if($data_bq['rekon'])
                        {
                            $spreadSheet->getSheet($k)->insertNewColumnBefore('G', count($data_bq['rekon']) );
                            $spreadSheet->getSheet($k)->insertNewRowBefore(11, count($data_bq['rekon'][0]['list']) );

                            foreach($data_bq['rekon'][0]['list'] as $kk => $vv)
                            {
                                $num_boq_tb = ++$kk;
                                $spreadSheet->getSheet($k)->getCell('A'. (10 + $num_boq_tb) )->setValue($num_boq_tb);
                                $spreadSheet->getSheet($k)->getCell('B'. (10 + $num_boq_tb) )->setValue($vv['designator']);
                                $spreadSheet->getSheet($k)->getCell('C'. (10 + $num_boq_tb) )->setValue($vv['uraian']);
                                $spreadSheet->getSheet($k)->getCell('D'. (10 + $num_boq_tb) )->setValue($vv['satuan']);
                                $spreadSheet->getSheet($k)->getCell('E'. (10 + $num_boq_tb) )->setValue($vv['material'] != 0 ? $vv['material'] : '-');
                                $spreadSheet->getSheet($k)->getCell('F'. (10 + $num_boq_tb) )->setValue($vv['jasa'] != 0 ? $vv['jasa'] : '-');
                            }

                            $num_boq_tb = 0;
                            $num_boq_tb_head = 0;
                            // dd($data_bq['rekon']);
                            foreach($data_bq['rekon'] as $k1 => $v2)
                            {
                                $num_boq_tb_head = $num_boq_tb_head++;
                                $spreadSheet->getSheet($k)->getCell($index[$k1 + $num_boq_tb_head].''. 8 )->setValue($v2['sto']);
                                $spreadSheet->getSheet($k)->getCell($index[$k1 + $num_boq_tb_head].''. 9 )->setValue($v2['lokasi']);
                                $spreadSheet->getSheet($k)->getCell($index[$k1 + $num_boq_tb_head].''. 10 )->setValue($v2['pid_sto']);
                                $spreadSheet->getSheet($k)->getStyle($index[$k1 + $num_boq_tb_head].''. 8  .':'. $index[$k1 + $num_boq_tb_head].''. 10 )->applyFromArray($border_Style);
                                $spreadSheet->getSheet($k)->getStyle($index[$k1 + $num_boq_tb_head].''. 8  .':'. $index[$k1 + $num_boq_tb_head].''. 10 )->applyFromArray([
                                    'fill' => [
                                        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                                        'startColor' => [
                                            'argb' => 'ffff00',
                                        ],
                                    ]
                                ]);

                                $v2['list'] = array_values($v2['list']);

                                foreach($v2['list'] as $k2 => $v3)
                                {
                                    $num_boq_tb = ++$k2;
                                    $spreadSheet->getSheet($k)->getCell($index[$k1].''. (10 + $num_boq_tb) )->setValue($v3['rekon']);

                                    if(!isset($tot_rekon[$k1]) )
                                    {
                                        $tot_rekon[$k1]['material_rekon'] = 0;
                                        $tot_rekon[$k1]['jasa_rekon'] = 0;
                                    }

                                    $tot_rekon[$k1]['material_rekon'] += $v3['rekon'] * $v3['material'];
                                    $tot_rekon[$k1]['jasa_rekon'] += $v3['rekon'] * $v3['jasa'];

                                    if(!isset($tot_all[($num_boq_tb + 10)]) )
                                    {
                                        $tot_all[($num_boq_tb + 10)] = 0;
                                    }

                                    $tot_all[($num_boq_tb + 10)] += $v3['rekon'];
                                }

                                $spreadSheet->getSheet($k)->getCell($index[$k1].''. (11 + $num_boq_tb) )->setValue($tot_rekon[$k1]['material_rekon'] != 0 ? $tot_rekon[$k1]['material_rekon'] : '-');
                                $spreadSheet->getSheet($k)->getCell($index[$k1].''. (12 + $num_boq_tb) )->setValue($tot_rekon[$k1]['jasa_rekon'] != 0 ? $tot_rekon[$k1]['jasa_rekon'] : '-');

                                $total_rekon = $tot_rekon[$k1]['jasa_rekon'] + $tot_rekon[$k1]['material_rekon'];

                                $spreadSheet->getSheet($k)->getCell($index[$k1].''. (13 + $num_boq_tb) )->setValue($total_rekon != 0 ? $total_rekon : '-');
                                $spreadSheet->getSheet($k)->getCell($index[$k1].''. (14 + $num_boq_tb) )->setValue($total_rekon != 0 ? $total_rekon * $pph : '-');
                                $spreadSheet->getSheet($k)->getCell($index[$k1].''. (15 + $num_boq_tb) )->setValue($total_rekon != 0 ? $total_rekon + ($total_rekon * $pph) : '-');
                            }
                            // dd($tot_all, $k1);
                            foreach($tot_all as $key => $val_x)
                            {
                                $spreadSheet->getSheet($k)->getCell($index[($k1 + 1)].''. $key )->setValue($val_x);
                            }

                            $spreadSheet->getSheet($k)->getCell($index[($k1 + 1)].''. (11 + $num_boq_tb) )->setValue($data_tot['rekon']['total']['material_rekon'] != 0 ? $data_tot['rekon']['total']['material_rekon'] : '-');
                            $spreadSheet->getSheet($k)->getCell($index[($k1 + 1)].''. (12 + $num_boq_tb) )->setValue($data_tot['rekon']['total']['jasa_rekon'] != 0 ? $data_tot['rekon']['total']['jasa_rekon'] : '-');

                            $total_all_rekon = $data_tot['rekon']['total']['jasa_rekon'] + $data_tot['rekon']['total']['material_rekon'];

                            $spreadSheet->getSheet($k)->getCell($index[($k1 + 1)].''. (13 + $num_boq_tb) )->setValue($total_all_rekon != 0 ? $total_all_rekon : '-');
                            $spreadSheet->getSheet($k)->getCell($index[($k1 + 1)].''. (14 + $num_boq_tb) )->setValue($total_all_rekon != 0 ? $total_all_rekon * $pph : '-');
                            $spreadSheet->getSheet($k)->getCell($index[($k1 + 1)].''. (15 + $num_boq_tb) )->setValue($total_all_rekon != 0 ? $total_all_rekon + ($total_all_rekon * $pph) : '-');
                        }
                    }

                    if($val == 'Lamp BA Rekon')
                    {
                        $num_lam_ba_rek = 0;

                        if($data_bq['rekon'])
                        {
                            $spreadSheet->getSheet($k)->insertNewColumnBefore('G', count($data_bq['rekon']) *4 );
                            $spreadSheet->getSheet($k)->insertNewRowBefore(12, count($data_bq['rekon'][0]['list']) );

                            foreach($data_bq['rekon'][0]['list'] as $kk => $vv)
                            {
                                $num_lam_ba_rek = ++$kk;
                                $spreadSheet->getSheet($k)->getCell('A'. (11 + $num_lam_ba_rek) )->setValue($num_lam_ba_rek);
                                $spreadSheet->getSheet($k)->getCell('B'. (11 + $num_lam_ba_rek) )->setValue($vv['designator']);
                                $spreadSheet->getSheet($k)->getColumnDimension('B')->setAutoSize(true);
                                $spreadSheet->getSheet($k)->getStyle('C'. (11 + $num_lam_ba_rek) )->getAlignment()->setWrapText(true);
                                $spreadSheet->getSheet($k)->getRowDimension( (11 + $num_lam_ba_rek) )->setRowHeight(-1);
                                $spreadSheet->getSheet($k)->getCell('C'. (11 + $num_lam_ba_rek) )->setValue($vv['uraian']);
                                $spreadSheet->getSheet($k)->getCell('D'. (11 + $num_lam_ba_rek) )->setValue($vv['satuan']);
                                $spreadSheet->getSheet($k)->getCell('E'. (11 + $num_lam_ba_rek) )->setValue($vv['material'] != 0 ? $vv['material'] : '-');
                                $spreadSheet->getSheet($k)->getCell('F'. (11 + $num_lam_ba_rek) )->setValue($vv['jasa'] != 0 ? $vv['jasa'] : '-');
                            }

                            $num_lam_ba_rek = 0;
                            $tot_all = [];

                            foreach($data_bq['rekon'] as $k1 => $v2)
                            {
                                // $spreadSheet->getSheet($k)->mergeCells($index[($k1 * 4) + 0].''. 8 . ':'. $index[($k1 * 4) + 3].''. 8);
                                $spreadSheet->getSheet($k)->getCell($index[$k1 * 4].''. 8)->setValue($v2['sto']);
                                $spreadSheet->getSheet($k)->getCell($index[$k1 * 4].''. 9)->setValue($v2['lokasi']);
                                $spreadSheet->getSheet($k)->getCell($index[$k1 * 4].''. 10)->setValue($v2['pid_sto']);
                                $spreadSheet->getSheet($k)->getCell($index[($k1 * 4) + 0].''. 11)->setValue('SP');
                                $spreadSheet->getSheet($k)->getCell($index[($k1 * 4) + 1].''. 11)->setValue('Rekon');
                                $spreadSheet->getSheet($k)->getCell($index[($k1 * 4) + 2].''. 11)->setValue('Tambah');
                                $spreadSheet->getSheet($k)->getCell($index[($k1 * 4) + 3].''. 11)->setValue('Kurang');

                                $spreadSheet->getSheet($k)->mergeCells($index[$k1 * 4].''. 8 . ':'. $index[($k1 * 4) + 3].''. 8 );
                                $spreadSheet->getSheet($k)->mergeCells($index[$k1 * 4].''. 9 . ':'. $index[($k1 * 4) + 3].''. 9 );
                                $spreadSheet->getSheet($k)->mergeCells($index[$k1 * 4].''. 10 . ':'. $index[($k1 * 4) + 3].''. 10 );

                                $spreadSheet->getSheet($k)->getStyle($index[$k1 * 4].''. 8 .':'. $index[($k1 * 4) + 3].''. 10)->applyFromArray($border_Style);
                                $spreadSheet->getSheet($k)->getStyle($index[$k1 * 4].''. 8 .':'. $index[($k1 * 4) + 3].''. 10)->applyFromArray([
                                    'fill' => [
                                        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                                        'startColor' => [
                                            'argb' => 'ffff00',
                                        ],
                                    ]
                                ]);

                                $spreadSheet->getSheet($k)->getStyle($index[$k1 * 4].''. 10)->getAlignment()->setWrapText(true);

                                // dd($v2['list']);
                                $v2['list'] = array_values($v2['list']);

                                foreach($v2['list'] as $k2 => $v3)
                                {
                                    $num_lam_ba_rek = ++$k2;

                                    $spreadSheet->getSheet($k)->getCell($index[($k1 * 4) + 0].''. (11 + $num_lam_ba_rek) )->setValue($v3['sp'] != 0 ? $v3['sp'] : '-');
                                    $spreadSheet->getSheet($k)->getCell($index[($k1 * 4) + 1].''. (11 + $num_lam_ba_rek) )->setValue($v3['rekon'] != 0 ? $v3['rekon'] : '-');
                                    $spreadSheet->getSheet($k)->getCell($index[($k1 * 4) + 2].''. (11 + $num_lam_ba_rek) )->setValue($v3['tambah'] != 0 ? $v3['tambah'] : '-');
                                    $spreadSheet->getSheet($k)->getCell($index[($k1 * 4) + 3].''. (11 + $num_lam_ba_rek) )->setValue($v3['kurang'] != 0 ? $v3['kurang'] : '-');

                                    if(!isset($tot_gd[$k1]) )
                                    {
                                        $tot_gd[$k1]['material_rekon'] = 0;
                                        $tot_gd[$k1]['jasa_rekon'] = 0;

                                        $tot_gd[$k1]['material_sp'] = 0;
                                        $tot_gd[$k1]['jasa_sp'] = 0;

                                        $tot_gd[$k1]['material_tambah'] = 0;
                                        $tot_gd[$k1]['jasa_tambah'] = 0;

                                        $tot_gd[$k1]['material_kurang'] = 0;
                                        $tot_gd[$k1]['jasa_kurang'] = 0;
                                    }

                                    $tot_gd[$k1]['material_rekon'] += $v3['rekon'] * $v3['material'];
                                    $tot_gd[$k1]['jasa_rekon'] += $v3['rekon'] * $v3['jasa'];

                                    $tot_gd[$k1]['material_sp'] += $v3['sp'] * $v3['material'];
                                    $tot_gd[$k1]['jasa_sp'] += $v3['sp'] * $v3['jasa'];

                                    $tot_gd[$k1]['material_tambah'] += $v3['tambah'] * $v3['material'];
                                    $tot_gd[$k1]['jasa_tambah'] += $v3['tambah'] * $v3['jasa'];

                                    $tot_gd[$k1]['material_kurang'] += $v3['kurang'] * $v3['material'];
                                    $tot_gd[$k1]['jasa_kurang'] += $v3['kurang'] * $v3['jasa'];

                                    if(!isset($tot_all[11 + $num_lam_ba_rek]) )
                                    {
                                        $tot_all[11 + $num_lam_ba_rek]['material_sp'] = 0;
                                        $tot_all[11 + $num_lam_ba_rek]['material_rekon'] = 0;

                                        $tot_all[11 + $num_lam_ba_rek]['tambah'] = 0;
                                        $tot_all[11 + $num_lam_ba_rek]['kurang'] = 0;
                                    }

                                    $tot_all[11 + $num_lam_ba_rek]['material_sp'] += $v3['sp'];
                                    $tot_all[11 + $num_lam_ba_rek]['material_rekon'] += $v3['rekon'];

                                    $tot_all[11 + $num_lam_ba_rek]['tambah'] += $v3['tambah'];
                                    $tot_all[11 + $num_lam_ba_rek]['kurang'] += $v3['kurang'];

                                }
                                // dd($tot_all);
                                $spreadSheet->getSheet($k)->getCell($index[($k1 * 4) + 0]. (12 + $num_lam_ba_rek) )->setValue($tot_gd[$k1]['material_sp'] != 0 ? $tot_gd[$k1]['material_sp'] : '-');
                                $spreadSheet->getSheet($k)->getCell($index[($k1 * 4) + 0]. (13 + $num_lam_ba_rek) )->setValue($tot_gd[$k1]['jasa_sp'] != 0 ? $tot_gd[$k1]['jasa_sp'] : '-');

                                $total_gd_sp = $tot_gd[$k1]['jasa_sp'] + $tot_gd[$k1]['material_sp'];

                                $spreadSheet->getSheet($k)->getCell($index[($k1 * 4) + 0]. (14 + $num_lam_ba_rek) )->setValue($total_gd_sp != 0 ? $total_gd_sp : '-');
                                $spreadSheet->getSheet($k)->getCell($index[($k1 * 4) + 0]. (15 + $num_lam_ba_rek) )->setValue($total_gd_sp != 0 ? $total_gd_sp * $pph : '-');
                                $spreadSheet->getSheet($k)->getCell($index[($k1 * 4) + 0]. (16 + $num_lam_ba_rek) )->setValue($total_gd_sp != 0 ? $total_gd_sp + ($total_gd_sp * $pph) : '-');

                                $spreadSheet->getSheet($k)->getCell($index[1 + ($k1 * 4) + 0] . (12 + $num_lam_ba_rek) )->setValue($tot_gd[$k1]['material_rekon'] != 0 ? $tot_gd[$k1]['material_rekon'] : '-');
                                $spreadSheet->getSheet($k)->getCell($index[1 + ($k1 * 4) + 0] . (13 + $num_lam_ba_rek) )->setValue($tot_gd[$k1]['jasa_rekon'] != 0 ? $tot_gd[$k1]['jasa_rekon'] : '-');

                                $total_gd_rekon = $tot_gd[$k1]['jasa_rekon'] + $tot_gd[$k1]['jasa_rekon'];

                                $spreadSheet->getSheet($k)->getCell($index[1 + ($k1 * 4) + 0] . (14 + $num_lam_ba_rek) )->setValue($total_gd_rekon != 0 ? $total_gd_rekon : '-');
                                $spreadSheet->getSheet($k)->getCell($index[1 + ($k1 * 4) + 0] . (15 + $num_lam_ba_rek) )->setValue($total_gd_rekon != 0 ? $total_gd_rekon * $pph : '-');
                                $spreadSheet->getSheet($k)->getCell($index[1 + ($k1 * 4) + 0] . (16 + $num_lam_ba_rek) )->setValue($total_gd_rekon != 0 ? $total_gd_rekon + ($total_gd_rekon * $pph)  : '-');

                                $spreadSheet->getSheet($k)->getCell($index[2 + ($k1 * 4) + 0] . (12 + $num_lam_ba_rek) )->setValue($tot_gd[$k1]['material_tambah'] != 0 ? $tot_gd[$k1]['material_tambah'] : '-');
                                $spreadSheet->getSheet($k)->getCell($index[2 + ($k1 * 4) + 0] . (13 + $num_lam_ba_rek) )->setValue($tot_gd[$k1]['jasa_tambah'] != 0 ? $tot_gd[$k1]['jasa_tambah'] : '-');

                                $total_gd_tambah = $tot_gd[$k1]['jasa_tambah'] + $tot_gd[$k1]['material_tambah'];

                                $spreadSheet->getSheet($k)->getCell($index[2 + ($k1 * 4) + 0] . (14 + $num_lam_ba_rek) )->setValue($total_gd_tambah != 0 ? $total_gd_tambah : '-');
                                $spreadSheet->getSheet($k)->getCell($index[2 + ($k1 * 4) + 0] . (15 + $num_lam_ba_rek) )->setValue($total_gd_tambah != 0 ? $total_gd_tambah * $pph : '-');
                                $spreadSheet->getSheet($k)->getCell($index[2 + ($k1 * 4) + 0] . (16 + $num_lam_ba_rek) )->setValue($total_gd_tambah != 0 ? $total_gd_tambah + ($total_gd_tambah * $pph) : '-');

                                $spreadSheet->getSheet($k)->getCell($index[3 + ($k1 * 4) + 0] . (12 + $num_lam_ba_rek) )->setValue($tot_gd[$k1]['material_kurang'] != 0 ? $tot_gd[$k1]['material_kurang'] : '-');
                                $spreadSheet->getSheet($k)->getCell($index[3 + ($k1 * 4) + 0] . (13 + $num_lam_ba_rek) )->setValue($tot_gd[$k1]['jasa_kurang'] != 0 ? $tot_gd[$k1]['jasa_kurang'] : '-');

                                $total_gd_kurang = $tot_gd[$k1]['jasa_kurang'] + $tot_gd[$k1]['material_kurang'];

                                $spreadSheet->getSheet($k)->getCell($index[3 + ($k1 * 4) + 0] . (14 + $num_lam_ba_rek) )->setValue($total_gd_kurang != 0 ? $total_gd_kurang : '-');
                                $spreadSheet->getSheet($k)->getCell($index[3 + ($k1 * 4) + 0] . (15 + $num_lam_ba_rek) )->setValue($total_gd_kurang != 0 ? $total_gd_kurang * $pph : '-');
                                $spreadSheet->getSheet($k)->getCell($index[3 + ($k1 * 4) + 0] . (16 + $num_lam_ba_rek) )->setValue($total_gd_kurang != 0 ? $total_gd_kurang + ($total_gd_kurang * $pph) : '-');

                                $spreadSheet->getSheet($k)->getStyle($index[($k1 * 4) + 0]. (11 + $num_lam_ba_rek) .':'. $index[3 + ($k1 * 4) + 0] . (16 + $num_lam_ba_rek) )->applyFromArray($border_Style);

                            }

                            foreach($tot_all as $key => $val)
                            {
                                $spreadSheet->getSheet($k)->getCell($index[( (count($data_bq['rekon']) * 4) + 0)].''. $key )->setValue($val['material_sp'] != 0 ? $val['material_sp'] : '-');
                                $spreadSheet->getSheet($k)->getCell($index[( (count($data_bq['rekon']) * 4) + 1)].''. $key )->setValue($val['material_rekon'] != 0 ? $val['material_rekon'] : '-');
                                $spreadSheet->getSheet($k)->getCell($index[( (count($data_bq['rekon']) * 4) + 2)].''. $key )->setValue($val['tambah'] != 0 ? $val['tambah'] : '-');
                                $spreadSheet->getSheet($k)->getCell($index[( (count($data_bq['rekon']) * 4) + 3)].''. $key )->setValue($val['kurang'] != 0 ? $val['kurang'] : '-');
                            }

                            $spreadSheet->getSheet($k)->getCell($index[( (count($data_bq['rekon']) * 4) + 0)].''. (12 + $num_lam_ba_rek) )->setValue($data_tot['rekon']['total']['material_sp'] != 0 ? $data_tot['rekon']['total']['material_sp'] : '-');
                            $spreadSheet->getSheet($k)->getCell($index[( (count($data_bq['rekon']) * 4) + 0)].''. (13 + $num_lam_ba_rek) )->setValue($data_tot['rekon']['total']['jasa_sp'] != 0 ? $data_tot['rekon']['total']['jasa_sp'] : '-');

                            $gd_tot_sp = $data_tot['rekon']['total']['jasa_sp'] + $data_tot['rekon']['total']['material_sp'];

                            $spreadSheet->getSheet($k)->getCell($index[( (count($data_bq['rekon']) * 4) + 0)].''. (14 + $num_lam_ba_rek) )->setValue($gd_tot_sp != 0 ? $gd_tot_sp : '-');
                            $spreadSheet->getSheet($k)->getCell($index[( (count($data_bq['rekon']) * 4) + 0)].''. (15 + $num_lam_ba_rek) )->setValue($gd_tot_sp != 0 ? $gd_tot_sp * $pph : '-');
                            $spreadSheet->getSheet($k)->getCell($index[( (count($data_bq['rekon']) * 4) + 0)].''. (16 + $num_lam_ba_rek) )->setValue($gd_tot_sp != 0 ? $gd_tot_sp + ($gd_tot_sp * $pph) : '-');

                            $spreadSheet->getSheet($k)->getCell($index[( (count($data_bq['rekon']) * 4) + 1)].''. (12 + $num_lam_ba_rek) )->setValue($data_tot['rekon']['total']['material_rekon'] != 0 ? $data_tot['rekon']['total']['material_rekon'] : '-');
                            $spreadSheet->getSheet($k)->getCell($index[( (count($data_bq['rekon']) * 4) + 1)].''. (13 + $num_lam_ba_rek) )->setValue($data_tot['rekon']['total']['jasa_rekon'] != 0 ? $data_tot['rekon']['total']['jasa_rekon'] : '-');

                            $gd_tot_rekon = $data_tot['rekon']['total']['jasa_rekon'] + $data_tot['rekon']['total']['material_rekon'];

                            $spreadSheet->getSheet($k)->getCell($index[( (count($data_bq['rekon']) * 4) + 1)].''. (14 + $num_lam_ba_rek) )->setValue($gd_tot_rekon != 0 ? $gd_tot_rekon : '-');
                            $spreadSheet->getSheet($k)->getCell($index[( (count($data_bq['rekon']) * 4) + 1)].''. (15 + $num_lam_ba_rek) )->setValue($gd_tot_rekon != 0 ? $gd_tot_rekon * $pph : '-');
                            $spreadSheet->getSheet($k)->getCell($index[( (count($data_bq['rekon']) * 4) + 1)].''. (16 + $num_lam_ba_rek) )->setValue($gd_tot_rekon != 0 ? $gd_tot_rekon + ($gd_tot_rekon * $pph) : '-');

                            $spreadSheet->getSheet($k)->getCell($index[( (count($data_bq['rekon']) * 4) + 2)].''. (12 + $num_lam_ba_rek) )->setValue($data_tot['rekon']['total']['material_tambah'] != 0 ? $data_tot['rekon']['total']['material_tambah'] : '-');
                            $spreadSheet->getSheet($k)->getCell($index[( (count($data_bq['rekon']) * 4) + 2)].''. (13 + $num_lam_ba_rek) )->setValue($data_tot['rekon']['total']['jasa_tambah'] != 0 ? $data_tot['rekon']['total']['jasa_tambah'] : '-');

                            $gd_tot_tambah = $data_tot['rekon']['total']['jasa_tambah'] + $data_tot['rekon']['total']['material_tambah'];

                            $spreadSheet->getSheet($k)->getCell($index[( (count($data_bq['rekon']) * 4) + 2)].''. (14 + $num_lam_ba_rek) )->setValue($gd_tot_tambah != 0 ? $gd_tot_tambah : '-');
                            $spreadSheet->getSheet($k)->getCell($index[( (count($data_bq['rekon']) * 4) + 2)].''. (15 + $num_lam_ba_rek) )->setValue($gd_tot_tambah != 0 ? $gd_tot_tambah * $pph : '-');
                            $spreadSheet->getSheet($k)->getCell($index[( (count($data_bq['rekon']) * 4) + 2)].''. (16 + $num_lam_ba_rek) )->setValue($gd_tot_tambah != 0 ? $gd_tot_tambah + ($gd_tot_tambah * $pph) : '-');

                            $spreadSheet->getSheet($k)->getCell($index[( (count($data_bq['rekon']) * 4) + 3)].''. (12 + $num_lam_ba_rek) )->setValue($data_tot['rekon']['total']['material_kurang'] != 0 ? $data_tot['rekon']['total']['material_kurang'] : '-');
                            $spreadSheet->getSheet($k)->getCell($index[( (count($data_bq['rekon']) * 4) + 3)].''. (13 + $num_lam_ba_rek) )->setValue($data_tot['rekon']['total']['jasa_kurang'] != 0 ? $data_tot['rekon']['total']['jasa_kurang'] : '-');

                            $gd_tot_krg = $data_tot['rekon']['total']['jasa_tambah'] + $data_tot['rekon']['total']['material_tambah'];

                            $spreadSheet->getSheet($k)->getCell($index[( (count($data_bq['rekon']) * 4) + 3)].''. (14 + $num_lam_ba_rek) )->setValue($gd_tot_krg != 0 ? $gd_tot_krg : '-');
                            $spreadSheet->getSheet($k)->getCell($index[( (count($data_bq['rekon']) * 4) + 3)].''. (15 + $num_lam_ba_rek) )->setValue($gd_tot_krg != 0 ? $gd_tot_krg * $pph : '-');
                            $spreadSheet->getSheet($k)->getCell($index[( (count($data_bq['rekon']) * 4) + 3)].''. (16 + $num_lam_ba_rek) )->setValue($gd_tot_krg != 0 ? $gd_tot_krg + ($gd_tot_krg * $pph) : '-');
                        }
                    }
                }

                if(!$this->data->spp_num)
                {
                    $spreadSheet->removeSheetByIndex(
                        $spreadSheet->getIndex(
                            $spreadSheet->getSheetByName('SPP')
                        )
                    );
                }

                if(!$this->data->receipt_num)
                {
                    $spreadSheet->removeSheetByIndex(
                        $spreadSheet->getIndex(
                            $spreadSheet->getSheetByName('Kwitansi')
                        )
                    );
                }

                if(!$this->data->invoice)
                {
                    $spreadSheet->removeSheetByIndex(
                        $spreadSheet->getIndex(
                            $spreadSheet->getSheetByName('Invoice')
                        )
                    );
                }


                if($this->data->bulan_pengerjaan >= '2021-11')
                {
                    $remove_item = [
                        'AMD SP',
                        'S.Penetapan',
                        'SP',
                        'BAUT',
                        'BA ABD',
                        'BA REKONSILIASI',
                        'BAST-I'
                    ];

                    if(!$this->data->amd_sp)
                    {
                        $spreadSheet->removeSheetByIndex(
                            $spreadSheet->getIndex(
                                $spreadSheet->getSheetByName('AMD 2021')
                            )
                        );

                        $remove_item = array_diff($remove_item, ['AMD 2021'] );

                    }
                    // $all = $spreadSheet->getSheetNames();
                    // dd($remove_item, $all, $this->data->amd_sp);
                    foreach($remove_item as $v)
                    {
                        $spreadSheet->removeSheetByIndex(
                            $spreadSheet->getIndex(
                                $spreadSheet->getSheetByName($v)
                            )
                        );
                    }
                }
                else
                {
                    $remove_item = [
                        'S.Penetapan',
                        'SP',
                        'LAMPIRAN SP',
                        'AMD 2021',
                        'BAST-I',
                        'BA REKONSILIASI',
                        'Rekap BA Rekon',
                        'BOQ',
                        'Lamp BA Rekon',
                        'BAKH',
                    ];

                    switch ($this->data->step_id) {
                        case 4:
                            $remove_item = array_slice($remove_item, 1);
                        break;
                        case 6:
                            $remove_item = array_slice($remove_item, 4);
                        break;
                    }

                    if($this->data->step_id < 8)
                    {
                        $remove_item = array_diff($remove_item, ['BA REKONSILIASI', 'Rekap BA Rekon', 'BOQ', 'Lamp BA Rekon', 'BAKH'] );
                    }

                    if(!$this->data->amd_sp)
                    {
                        $spreadSheet->removeSheetByIndex(
                            $spreadSheet->getIndex(
                                $spreadSheet->getSheetByName('AMD SP')
                            )
                        );

                        $remove_item = array_diff($remove_item, ['AMD SP'] );
                    }

                    if(!$this->data2->pph)
                    {
                        $spreadSheet->removeSheetByIndex(
                            $spreadSheet->getIndex(
                                $spreadSheet->getSheetByName('FORM_TAGIHAN_MITRA')
                            )
                        );

                        $remove_item = array_diff($remove_item, ['FORM_TAGIHAN_MITRA'] );
                    }

                    if(!$this->data->BAUT)
                    {
                        $spreadSheet->removeSheetByIndex(
                            $spreadSheet->getIndex(
                                $spreadSheet->getSheetByName('BAUT')
                            )
                        );

                        $remove_item = array_diff($remove_item, ['BAUT'] );
                    }

                    if(!$this->data->ba_abd)
                    {
                        $spreadSheet->removeSheetByIndex(
                            $spreadSheet->getIndex(
                                $spreadSheet->getSheetByName('BA ABD')
                            )
                        );

                        $remove_item = array_diff($remove_item, ['BA ABD'] );
                    }

                    if($remove_item)
                    {
                        foreach($remove_item as $item_remove_v)
                        {
                            $spreadSheet->removeSheetByIndex(
                                $spreadSheet->getIndex(
                                    $spreadSheet->getSheetByName($item_remove_v)
                                )
                            );
                        }
                    }
                }

                $writer = IOFactory::createWriter($spreadSheet, 'Xlsx');

                if($this->opsi == 'save')
                {
                    $path = public_path(). "/upload2/". $this->data->id .'/excel_osp_FO/';

                    if (!file_exists($path) ) {
                        if (!mkdir($path, 0770, true) ) {
                            return 'gagal menyiapkan folder sanggup';
                        }
                    }

                    $writer->save($path."Template Invoice & BA OSP FO ".$this->data->judul. ".xlsx");
                }
                else
                {
                    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                    header('Content-Disposition: attachment;filename="Template Invoice & BA OSP FOs.xlsx"');

                    $writer->save('php://output');
                    die;
                }
            },
            BeforeWriting::class => function(BeforeWriting $event) {
                $event->writer->setActiveSheetIndex(0);
            }
        ];
    }
}