<?php

namespace App\Http\Controllers;

use App\DA\AdminModel;
use App\DA\MitraModel;
use App\DA\ReportModel;
use App\DA\ToolsModel;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use ZipArchive;
use DB;

date_default_timezone_set("Asia/Makassar");
class AdminController extends Controller
{

  // public function input_pra_con($id)
  // {
  //   $data = AdminModel::show_Single_id_data($id);
  //   return view('Area.Kontrak.input', ['data' => $data]);
  // }

  public function input_amand($id)
  {
    $data = AdminModel::show_Single_id_data($id);
    return view('Area.Kontrak.input_amand', ['data' => $data]);
  }

  // public function input_excel($id)
  // {
  //   $material = AdminModel::get_m(0, true);
  //   $material_me = AdminModel::get_m($id);
  //   $data = AdminModel::show_Single_id_data($id);
  //   return view('Area.Kontrak.input_excel', ['data' => $data, 'material' => $material, 'm_mine' => $material_me]);
  // }

  // public function save_data(Request $req)
  // {
  //   AdminModel::save_kontrak($req, 'kontrak', 0);
  //   return redirect('/');
  // }

  public function save_amand(Request $req, $id)
  {
    // AdminModel::save_kontrak($req, 'amand', $id);
    return redirect('/');
  }

  public function mitra_search(Request $req)
  {
    $search = strtoupper($req->searchTerm);
    $check_data = AdminModel::mitra_search($search);
    $data = [];
    foreach ($check_data as $row) {
      // dd($row->ORDER_ID);
      $data[] = array("id" => $row->id, "text" => $row->nama_company);
    }
    return \Response::json($data);
  }

  public function mitra_search_per_witel(Request $req)
  {
    $search = strtoupper($req->searchTerm);
    $check_data = AdminModel::mitra_search($search, 1);
    $data = [];
    foreach ($check_data as $row) {
      // dd($row->ORDER_ID);
      $data[] = array("id" => $row->id, "text" => $row->nama_company);
    }
    return \Response::json($data);
  }

  public function mitra_searchtag(Request $req)
  {
    $search = strtoupper($req->searchTerm);
    $check_data = AdminModel::mitra_search($search);
    foreach ($check_data as $row) {
      // dd($row->ORDER_ID);
      $data[] = array("id" => $row->nama_company, "text" => $row->nama_company);
    }
    return \Response::json($data);
  }

  // public function download($t, $id)
  // {
  //   $main_data = AdminModel::show_Single_id_data($id);
  //   $material = AdminModel::search_material($id);
  //   // dd($data);
  //   if ($t == 'kontrak') {
  //     $this->generateDocx_kontrak($id);
  //   } elseif ($t == 'pra_kontrak') {
  //     $this->generateDocx_pra_kontrak($id);
  //   } elseif ($t == 'aman') {
  //     $this->generateDocx_amandemen($id);
  //   } elseif ($t == 'excel_1') {
  //     $data = AdminModel::show_Single_id_data($id);
  //     $ba = $this->convert_month($data->tgl_excel, true);
  //     $split = explode(' ', $ba);
  //     $hari = $split[0];
  //     $tgl = $this->terbilang($split[1]);
  //     $bulan = $split[2];
  //     $tahun = $this->terbilang($split[3]);
  //     $time_tr = $data->tgl_kontrak;
  //     if (date('w', strtotime(date($time_tr, strtotime("+4 days")))) >= 5) {
  //       $result = 4 + 8 - date('w', strtotime(date($time_tr, strtotime("+4 days"))));
  //       $date_future__4 = $this->convert_month("+$result days");
  //     } else {
  //       $date_future__4 = $this->convert_month(date('Y-m-d', strtotime("+4 days", strtotime($time_tr))));
  //     }
  //     return Excel::download(new ExcelExport([$main_data, $material], $hari, $tgl, $bulan, $tahun, $date_future__4), 'Ini Excel nya Mas.xlsx');
  //   }
  // }

  // public function save_excel(Request $req, $id)
  // {
  //   AdminModel::save_kontrak($req, 'excel', $id);
  //   return redirect('/');
  // }

  public function generateDocx_kontrak($id)
  {
    //kontrak
    $data = AdminModel::show_Single_id_data($id);
    $time_tr = $data->tgl_kontrak;
    $conver_harga_borong = str_replace(",", "", $data->harga_borong);
    $kontrak = public_path() . '/template_doc/kontrak.docx';
    $template_kon = new \PhpOffice\PhpWord\TemplateProcessor($kontrak);
    $template_kon->setValue('nama_company', htmlspecialchars($data->nama_company));
    $template_kon->setValue('hk_kerja_sama', htmlspecialchars($data->hk_kerja_sama));
    if (date('w', strtotime(date($time_tr, strtotime("+10 days")))) > 5) {
      $result = 10 + 7 - date('w', strtotime(date('Y-m-d', strtotime("+10 days"))));
      $after_Date_kon = $this->convert_month("+$result days", true);
      $raw_rmD = date('d-m-Y', strtotime("+$result days"));
    } else {
      $after_Date_kon = $this->convert_month("+10 days", true);
      $raw_rmD = date('d-m-Y', strtotime("+10 days"));
    }
    $date_future__5 = $data->tgl_penetapan;
    if (date('N', strtotime(date($date_future__5))) == 1) {
      // $result = 4 + 7 - date('N', strtotime(date('Y-m-d')));
      $date_future__4 = $this->convert_month(date('Y-m-d', strtotime("-2 days", strtotime($date_future__5))));
    } else {
      $date_future__4 = $this->convert_month(date('Y-m-d', strtotime("-1 days", strtotime($date_future__5))));
    }
    $rmD = explode(" ", $after_Date_kon);
    $template_kon->setValue('date_future_10_day', htmlspecialchars($rmD[0]));
    $template_kon->setValue('date_future_10_day_1', htmlspecialchars(ucwords($this->terbilang($rmD[1]))));
    $template_kon->setValue('date_future_10_month', htmlspecialchars($rmD[2]));
    $template_kon->setValue('nama_bos', htmlspecialchars($data->nama_spk));
    $template_kon->setValue('jabatan_bos', htmlspecialchars($data->jabatan_spk));
    $template_kon->setValue('date_future_10_year', htmlspecialchars(ucwords($this->terbilang($rmD[3]))));
    $template_kon->setValue('date_future_10_result', htmlspecialchars($raw_rmD));
    $template_kon->setValue('alamat_com', htmlspecialchars($data->alamat_company));
    $template_kon->setValue('mitra_nama', htmlspecialchars($data->wakil_mitra));
    $template_kon->setValue('mitra_po', htmlspecialchars($data->jabatan_mitra));
    $template_kon->setValue('pengadaan', htmlspecialchars($data->pengadaan));
    $template_kon->setValue('nego', htmlspecialchars($date_future__4));
    $total_month = $data->total_kerja;
    $template_kon->setValue('janji', htmlspecialchars($total_month . ' (' . ucwords($this->terbilang($total_month) . ')')));
    $template_kon->setValue('harga_borong', htmlspecialchars(number_format($conver_harga_borong, 3, ".", ".")));
    $template_kon->setValue('harga_borong_terbilang', htmlspecialchars(ucwords($this->terbilang($conver_harga_borong) .' rupiah')));
    $template_kon->setValue('bank', htmlspecialchars($data->bank));
    $template_kon->setValue('rek', htmlspecialchars($data->rek));
    $file_name_kontrak = str_replace(" ", "", "hasil kontrak") . '.docx';
    $file_loc = public_path() . '/' . $data->mitra_nm . '_' . $file_name_kontrak;
    $template_kon->saveAS($file_loc);
    $this->downloadable_content($file_loc);
  }

  public function generateDocx_pra_kontrak($id)
  {
    $data = AdminModel::show_Single_id_data($id);
    $time_tr = $data->tgl_kontrak;
    $conver_harga_borong = str_replace(",", "", $data->harga_borong);
    //pra kontrak
    $pra_kontrak = public_path() . '/template_doc/pra_kontrak.docx';
    $template_pora = new \PhpOffice\PhpWord\TemplateProcessor($pra_kontrak);
    $template_pora->setValue('hk_tawar', htmlspecialchars($data->hk_tawar));
    $template_pora->setValue('Date', htmlspecialchars($this->convert_month($data->tgl_kontrak)));
    if (date('N', strtotime(date($time_tr, strtotime("+1 days")))) == 6) {
      // $result = 7 + 1 - date('N', strtotime(date('Y-m-d')));
      $date_future__1 = $this->convert_month(date('Y-m-d', strtotime("+2 days", strtotime($time_tr))), true);
    } else {
      $date_future__1 = $this->convert_month(date('Y-m-d', strtotime("+1 days", strtotime($time_tr))), true);
    }
    $template_pora->setValue('Date_pass1', htmlspecialchars($date_future__1));
    $template_pora->setValue('nama_company', htmlspecialchars($data->nama_company));
    $template_pora->setValue('pengadaan', htmlspecialchars($data->pengadaan));
    $date_future__5 = $data->tgl_penetapan;
    if (date('N', strtotime(date($date_future__5))) == 1) {
      // $result = 4 + 7 - date('N', strtotime(date('Y-m-d')));
      $date_future__4 = $this->convert_month(date('Y-m-d', strtotime("-2 days", strtotime($date_future__5))));
    } else {
      $date_future__4 = $this->convert_month(date('Y-m-d', strtotime("-1 days", strtotime($date_future__5))));
    }
    $template_pora->setValue('date_future_5', htmlspecialchars($date_future__5));
    $template_pora->setValue('date_future_4', htmlspecialchars($date_future__4));
    $template_pora->setValue('hk_tetap', htmlspecialchars($data->hk_tetap));
    $template_pora->setValue('nama_spk', htmlspecialchars(strtoupper($data->nama_spk)));
    $template_pora->setValue('jabatan_spk', htmlspecialchars(strtoupper($data->jabatan_spk)));
    $total_month = $data->total_kerja;
    $template_pora->setValue('janji', htmlspecialchars($total_month . ' (' . ucwords($this->terbilang($total_month) . ')')));
    $template_pora->setValue('harga_borong', htmlspecialchars(number_format($conver_harga_borong, 3, ".", ".")));
    $template_pora->setValue('harga_borong_terbilang', htmlspecialchars(ucwords($this->terbilang($conver_harga_borong).' rupiah')));
    $file_name = str_replace(" ", "_", "hasil pra kontrak") . '.docx';
    $file = public_path() . '/' . $data->nama_company . '_' . $file_name;
    $template_pora->saveAS($file);
    $this->downloadable_content($file);
  }

  public function generateDocx_amandemen($id)
  {
    $data = AdminModel::show_Single_id_data($id);
    $conver_harga_amang = str_replace(",", "", $data->harga_amand);
    $amandemen = public_path() . '/template_doc/amandemen.docx';
    $template_aman = new \PhpOffice\PhpWord\TemplateProcessor($amandemen);
    //pra kontrak
    $template_aman->setValue('nama_company', htmlspecialchars($data->mitra_));
    $template_aman->setValue('pengadaan', htmlspecialchars($data->pengadaan));
    $template_aman->setValue('alamat_com', htmlspecialchars($data->alamat_company));
    $template_aman->setValue('nama_spk', htmlspecialchars(strtoupper($data->nama_spk)));
    $template_aman->setValue('jabatan_spk', htmlspecialchars(strtoupper($data->jabatan_spk)));
    $template_aman->setValue('mitra_nama', htmlspecialchars($data->wakil_mitra));
    $template_aman->setValue('mitra_po', htmlspecialchars($data->jabatan_mitra));
    $raw_raw_rmD = date('d-m-Y', strtotime("+0 days"));
    $raw_rmD = $this->convert_month("+0 days", true);
    $rmD = explode(" ", $raw_rmD);
    $template_aman->setValue('date_day', htmlspecialchars($rmD[0]));
    $template_aman->setValue('date_day_1', htmlspecialchars(ucwords($this->terbilang($rmD[1]))));
    $template_aman->setValue('date_month', htmlspecialchars($rmD[2]));
    $template_aman->setValue('date_year', htmlspecialchars(ucwords($this->terbilang($rmD[3]))));
    $template_aman->setValue('date_result', htmlspecialchars($raw_raw_rmD));
    $template_aman->setValue('hk_aman', htmlspecialchars($data->hk_aman));
    $template_aman->setValue('nama_bos', htmlspecialchars($data->nama_spk));
    $template_aman->setValue('jabatan_bos', htmlspecialchars($data->jabatan_spk));
    $template_aman->setValue('hk_kerja_sama', htmlspecialchars($data->hk_kerja_sama));
    $template_aman->setValue('tgl_hk_kerja_sama', htmlspecialchars(strtoupper($this->convert_month($data->created_at))));
    $template_aman->setValue('harga_borong', htmlspecialchars(number_format($data->harga_borong, 3, ".", ".")));
    $template_aman->setValue('harga_borong_terbilang', htmlspecialchars(ucwords($this->terbilang($data->harga_borong).' rupiah')));
    $template_aman->setValue('harga_amand', htmlspecialchars(number_format($conver_harga_amang, 3, ".", ".")));
    $template_aman->setValue('harga_amand_terbilang', htmlspecialchars(ucwords($this->terbilang($conver_harga_amang).' rupiah')));
    $file_name = str_replace(" ", "", "hasil amandemen") . '.docx';
    $file = public_path() . '/' . $data->nama_company . '_' . $file_name;
    $template_aman->saveAS($file);
    $this->downloadable_content($file);
  }

  public function terbilang($nilai)
  {
    if ($nilai < 0) {
      $hasil = "minus " . trim($this->penyebut($nilai));
    } else {
      $hasil = trim($this->penyebut($nilai));
    }
    $hasil = preg_replace('/\s+/', ' ', $hasil);
    return $hasil;
  }

  public function penyebut($nilai)
  {
    $nilai = abs($nilai);
    $huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
    $temp = "";
    if ($nilai < 12) {
      $temp = " " . $huruf[$nilai];
    } else if ($nilai < 20) {
      $temp = $this->penyebut($nilai - 10) . " belas";
    } else if ($nilai < 100) {
      $temp = $this->penyebut($nilai / 10) . " puluh" . $this->penyebut($nilai % 10);
    } else if ($nilai < 200) {
      $temp = " seratus" . $this->penyebut($nilai - 100);
    } else if ($nilai < 1000) {
      $temp = $this->penyebut($nilai / 100) . " ratus" . $this->penyebut($nilai % 100);
    } else if ($nilai < 2000) {
      $temp = " seribu" . $this->penyebut($nilai - 1000);
    } else if ($nilai < 1000000) {
      $temp = $this->penyebut($nilai / 1000) . " ribu" . $this->penyebut($nilai % 1000);
    } else if ($nilai < 1000000000) {
      $temp = $this->penyebut($nilai / 1000000) . " juta" . $this->penyebut($nilai % 1000000);
    } else if ($nilai < 1000000000000) {
      $temp = $this->penyebut($nilai / 1000000000) . " milyar" . $this->penyebut(fmod($nilai, 1000000000));
    } else if ($nilai < 1000000000000000) {
      $temp = $this->penyebut($nilai / 1000000000000) . " trilyun" . $this->penyebut(fmod($nilai, 1000000000000));
    }
    return $temp;
  }

  public function only_day($date)
  {
    $hari = array ( 1 => 'Senin',
        'Selasa',
        'Rabu',
        'Kamis',
        'Jumat',
        'Sabtu',
        'Minggu'
    );

    $num = date('N', strtotime($date));
    return $hari[$num];
  }

  public function download_excel_ospfo($id_boq_upload, $jenis = null)
  {
    //Maintenance INVOICE & BA Maintenance
    $data         = AdminModel::get_data_final($id_boq_upload);
    $self_comp    = ToolsModel::find_mitra('PT TELKOM AKSES');
    $mitra        = AdminModel::get_mitra($data->mitra_id);
    $lokasi       = MitraModel::get_lokasi($id_boq_upload);
    $design_sp    = MitraModel::get_design($id_boq_upload);
    $design_rekon = MitraModel::get_design($id_boq_upload, 1);

    foreach ($lokasi as $v) {
      $data_sl[$v->sto .' '. $v->lokasi]['lokasi'] = $v->lokasi;
      $data_sl[$v->sto .' '. $v->lokasi]['sto']    = $v->sto;
      $data_sl[$v->sto .' '. $v->lokasi]['gd_sp']  = $v->gd_sp;
    }
    // dd($lokasi, $design);
    Excel::store(new Excel_OSPFOExport([$data, $self_comp, $mitra, $lokasi, $design_sp, $design_rekon, $jenis, $data_sl]), 'Ini Excel Maintenance.xlsx');
  }

  public function downloadable_content($name)
  {
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header("Content-Type: application/force-download");
    header('Content-Disposition: attachment; filename=' . urlencode(basename($name)));
    header('Expires: 0');
    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    header('Pragma: public');
    header('Content-Length: ' . filesize($name));
    ob_clean();
    flush();
    readfile($name);
    unlink($name);
    exit;
  }

  public function convert_month($date, $cetak_hari = false)
  {
    $bulan = array(1 => 'Januari',
      'Februari',
      'Maret',
      'April',
      'Mei',
      'Juni',
      'Juli',
      'Agustus',
      'September',
      'Oktober',
      'November',
      'Desember',
    );

    $hari = array(1 => 'Senin',
      'Selasa',
      'Rabu',
      'Kamis',
      'Jumat',
      'Sabtu',
      'Minggu',
    );

    $result = date('d m Y', strtotime($date));
    $split = explode(' ', $result);
    $hasilnya = $split[0] . ' ' . $bulan[(int) $split[1]] . ' ' . $split[2];

    if ($cetak_hari) {
      $num = date('N', strtotime($date));
      return $hari[$num] . ', ' . $hasilnya;
    }

    return $hasilnya;
  }

  public function get_rfc_detail(Request $req)
  {
    $data = AdminModel::search_rfc($req);
    return \Response::json($data);
  }

  public function detail_sp(Request $req)
  {
    $data = AdminModel::get_data_final($req->id);
    // dd($req->all(), $data);
    return \Response::json($data);
  }

  public function get_detail_data_ajx(Request $req, $id)
  {
    if($id == 'no_sp')
    {
      $data = AdminModel::get_data_final($req->id);
    }
    return \Response::json($data);
  }

  public function download_full_rar($id_boq_up)
  {
    return AdminModel::download_file_all_rar($id_boq_up);
  }

  public static function download_zip_psb($version, $id)
  {
    return AdminModel::download_zip_psb($version, $id);
  }

  public function reject_sp(Request $req, $id)
  {
    AdminModel::reject_data($req, $id);
    $data = AdminModel::get_data_final($id);
    return redirect('/progressList/'.$data->step_id)->with('alerts', [
      ['type' => 'warning', 'text' => "$data->judul Berhasil Direject"]
    ]);
  }

	// public function verify_boq()
	// {
	// 	$ready_rekon = AdminModel::get_data_rekon(11);
	// 	$reject_rekon = AdminModel::get_data_rekon(18);
  //   $select2 = AdminModel::get_steps_by_urutan(12);
  //   // dd($select2);
  //   // dd($ready_rekon);
  //   return view('Report.Area.Rekon.Finish.list_verify_boqRekon', compact('ready_rekon', 'reject_rekon', 'select2') );
	// }

  // public function submit_rekon_final(Request $req)
	// {
  //   // dd($req->all() );
	// 	$msg = AdminModel::save_rekon_final($req);
  //   return redirect('/Admin/verify/boq')->with('alerts', $msg);
	// }

	// public function requestBA()
	// {
	// 	$data = AdminModel::request_ba();
  //   return view('Report.Area.list_ba_nomor', compact('data') );
	// }

	public function download_excel_dashboard($witel, $mitra, $khs, $period, $jenis_tanggal, $tgl)
	{
    $data = AdminModel::excel_dashboard($witel, $mitra, $khs, $period, $jenis_tanggal, $tgl, 'data');
    $log = AdminModel::excel_dashboard($witel, $mitra, $khs, $period, $jenis_tanggal, $tgl, 'log');
    return Excel::download(new ExcelCommon([$data, $log], 'multi_dashboard_excl'), 'Pekerjaan Table SLA.xlsx');
	}

  // public function bulk()
  // {
  //   $arr = [['judul' => 'PENGADAAN PEKERJAAN QE RECOVERY TBN PLE & BTB 07 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC14R639-0054']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN QE RELOKASI TBN PLE 07 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC17R639-0145']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN QE RECOVERY TBN PLE & BTB 08 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC14R639-0055']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN QE RELOKASI MAP KDG 03 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC17R639-0123']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN QE RECOVERY MAP KDG 03 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC14R639-0032']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN QE RECOVERY MAP KDG 04 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC14R639-0037']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN QE RELOKASI MAP KDG 04 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC17R639-0126']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN QE RECOVERY MAP KDG 05 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC14R639-0039']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN QE RELOKASI MAP KDG 05 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC17R639-0135']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN QE RECOVERY MAP KDG 06 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC14R639-0043']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN QE RELOKASI MAP KDG & RTA 06 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC17R639-0144']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN MAINTENANCE JARINGAN AKSES MAP ALL QE AKSES KDG, BRI, LUL 07 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC15R639-0001']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN QE RECOVERY MAP RTA, KDG, BBR & LUL 07 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC14R639-0048']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN QE RELOKASI MAP RTA, KDG, BBR & LUL 07 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC17R639-0146']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN MAINTENANCE JARINGAN AKSES MAP ALL QE AKSES RTA, KDG & BRI 08 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC15R639-0001']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN QE RECOVERY MAP BBR, KDG & BRI 08 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC14R639-0059']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN MAINTENANCE JARINGAN AKSES IBS ALL QE AKSES BBR 03 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC15R639-0001']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN MAINTENANCE JARINGAN AKSES IBS ALL QE AKSES BBR 04 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC15R639-0001']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN QE RELOKASI IBS BBR 04 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC17R639-0130']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN MAINTENANCE JARINGAN AKSES IBS ALL QE AKSES BBR 05 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC15R639-0001']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN QE RELOKASI IBS BBR 05 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC17R639-0134']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN QE RECOVERY IBS BBR 05 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC14R639-0038']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN MAINTENANCE JARINGAN AKSES IBS ALL QE AKSES BBR 06 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC15R639-0001']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN QE RECOVERY IBS BBR 06 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC14R639-0044']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN QE RELOKASI IBS BBR 06 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC17R639-0140']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN ALL QE AKSES WKA BBR 03 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC15R639-0001']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN NORMALISASI QE AKSES WKA LUL 04 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC15R639-0001']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN QE RELOKASI UPATEK ULI 01 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC17R639-0084']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN QE RELOKASI UPATEK ULI 02 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC17R639-0119']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN QE RECOVERY UPATEK ULI 02 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC14R639-0029']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN QE RELOKASI UPATEK ULI 03 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC17R639-0122']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN QE RECOVERY UPATEK ULI 03 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC14R639-0031']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN MAINTENANCE JARINGAN AKSES UPATEK ALL QE AKSES ULI 04 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC15R639-0001']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN QE RECOVERY UPATEK ULI 04 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC14R639-0034']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN QE RELOKASI UPATEK ULI 04 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC17R639-0129']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN QE RECOVERY UPATEK ULI 05 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC14R639-0040']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN QE RELOKASI UPATEK ULI 05 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC17R639-0131']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN QE RECOVERY UPATEK ULI 06 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC14R639-0046']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN QE RELOKASI UPATEK ULI 06 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC17R639-0142']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN QE RECOVERY UPATEK ULI 07 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC14R639-0052']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN QE RELOKASI UPATEK ULI 07 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC17R639-0147']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN QE RECOVERY UPATEK ULI 08 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC14R639-0058']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN BENJAR ODP (CLEANSING DC) AGB BJM, ULI, BRI, KDG & PLE 01 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC15R639-0001']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN MAINTENANCE JARINGAN AKSES AGB ALL QE AKSES BJM 01 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC15R639-0001']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN QE RELOKASI AGB BJM 01 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC17R639-0100']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN QE RECOVERY AGB BJM - RTA 01 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC14R639-0020']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN MAINTENANCE JARINGAN AKSES AGB ALL QE AKSES BJM 02 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC15R639-0001']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN QE RELOKASI AGB BJM - PLE 02 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC17R639-0118']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN QE RECOVERY AGB BJM - RTA 02 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC14R639-0012']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN MAINTENANCE JARINGAN AKSES AGB ALL QE AKSES BJM-PLE 03 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC15R639-0001']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN QE RELOKASI AGB BJM-PLE 03 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['1MC17R639-0124']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN QE RECOVERY AGB BBR-PLE 03 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC14R639-0030']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN MAINTENANCE JARINGAN AKSES AGB ALL QE AKSES BJM-PLE 04 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC15R639-0001']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN QE RECOVERY AGB BBR-PLE 04 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC14R639-0035']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN QE RELOKASI AGB BJM-PLE 04 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC17R639-0128']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN MAINTENANCE JARINGAN AKSES AGB ALL QE AKSES BJM, LUL & PLE 05 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC15R639-0001']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN QE RECOVERY AGB BBR-PLE 05 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC14R639-0041']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN QE RELOKASI AGB BJM-PLE 05 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC17R639-0133']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN MAINTENANCE JARINGAN AKSES AGB ALL QE AKSES BJM, LUL & PLE 06 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC15R639-0001']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN QE RELOKASI AGB BJM, BBR & PLE 06 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC17R639-0143']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN QE RECOVERY AGB BBR-PLE 06 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC14R639-0045']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN QE RECOVERY AGB BJM 07 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC14R639-0049']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN QE RELOKASI AGB BJM 07 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC17R639-0149']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN MAINTENANCE JARINGAN AKSES AGB ALL QE AKSES BJM 07 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC15R639-0001']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN MAINTENANCE JARINGAN AKSES AGB ALL QE AKSES BJM & LUL 08 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC15R639-0001']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN QE RECOVERY AGB BJM & BBR 08 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC14R639-0056']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN MAINTENANCE JARINGAN AKSES STM ALL QE AKSES BJM-KYG 06 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC15R639-0001']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN MAINTENANCE JARINGAN AKSES STM ALL QE AKSES BJM-KYG 07 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC15R639-0001']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN QE RECOVERY STM ULI 07 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC14R639-0050']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN MAINTENANCE JARINGAN AKSES STM ALL QE AKSES BJM-KYG 08 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC15R639-0001']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN QE RELOKASI CUI LUL, TJL & BLC 12 2020 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC17R639-0018'], ['21MC17R639-0016'], ['21MC17R639-0017'], ['21MC17R639-0023'], ['21MC17R639-0027'], ['21MC17R639-0009']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN QE RELOKASI CUI LUL, TJL & BLC 11 2020 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC17R639-0028'],['21MC17R639-0029']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN QE RELOKASI CUI LUL, TJL & BLC 10 2020 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC17R639-0006'],	['21MC17R639-0007'], [	'21MC17R639-0002'], ['21MC17R639-0003'],	['21MC17R639-0005']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN QE RELOKASI CUI LUL, TJL & BLC 01 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC17R639-0043']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN BENJAR ODP ( CLEANSING DC ) CUI LUL, BBR, TJL & BLC 01 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC15R639-0001']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN QE RECOVERY CUI LUL, TJL & BLC 01 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC14R639-0014']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN QE RELOKASI CUI LUL, TJL & BLC 02 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC17R639-0121']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN QE RECOVERY CUI LUL, TJL & BLC 02 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC14R639-0028']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN QE RELOKASI CUI MTP, BLC & KPL 03 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC17R639-0125']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN QE RECOVERY CUI MTP, TJL & KPL 03 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC14R639-0033']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN QE RECOVERY CUI MTP, TJL & KPL 04 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC14R639-0036']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN QE RELOKASI CUI MTP, BLC & KPL 04 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC17R639-0127']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN QE RECOVERY CUI MTP, TJL & KPL 05 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC14R639-0042']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN QE RELOKASI CUI MTP, BLC & KPL 05 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC17R639-0132']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN MAINTENANCE JARINGAN AKSES CUI ALL QE AKSES BBR-TJL 06 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC15R639-0001']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN QE RECOVERY CUI BBR, AMT & KPL 06 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC14R639-0047']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN QE RELOKASI CUI MTP, BLC & KPL 06 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC17R639-0141']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN MAINTENANCE JARINGAN AKSES CUI ALL QE AKSES BBR-TJL 07 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC15R639-0001']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN QE RECOVERY CUI BBR, AMT & KPL 07 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC14R639-0051']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN QE RELOKASI CUI MTP, BLC & KPL 07 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC17R639-0148']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN MAINTENANCE JARINGAN AKSES CUI ALL QE AKSES BBR-TJL 08 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC15R639-0001']]],
  //   ['judul' => 'PENGADAAN PEKERJAAN QE RECOVERY CUI BBR, AMT & KPL 08 2021 LOKASI BANJARMASIN WITEL KALSEL', 'pid' => [['21MC14R639-0057']]]];
  //   // dd($arr);
  //   DB::transaction(function () use ($arr)
	// 	{
  //     $no = -1;
  //     foreach($arr as $val)
  //     {
  //       $find_pbu = DB::table('procurement_boq_upload As pbu')
  //       ->leftjoin('procurement_Boq_lokasi As pbl', 'pbu.id', '=', 'pbl.id_upload')
  //       ->where('judul', 'like', '%'. $val['judul']. '%')
  //       ->select('pbu.id', 'pbu.judul', 'pbl.id as id_pbl')
  //       ->get();
  //       $no++;
  //       foreach($find_pbu as $keyc1 => $val_pbu)
  //       {
  //         if($val_pbu->judul == $val['judul'])
  //         {
  //           $final_data[$no][$val_pbu->id]['judul'] = $val['judul'];

  //           if(!isset($final_data[$no][$val_pbu->id]['pbl'][$keyc1]) )
  //           {
  //             // $final_data[$no][$val_pbu->id]['pbl'][$keyc1]['id_pbu'] = $val_pbu->id;
  //             // $final_data[$no][$val_pbu->id]['pbl'][$keyc1]['id_pbl'] = $val_pbu->id_pbl;
  //           }

  //           foreach($val['pid'][$keyc1] as $keyc3 => $valc3){
  //             $final_data[$no][$val_pbu->id]['pbl'][$keyc1]['isi_pid'][$keyc3]['id_pbu'] = $val_pbu->id;
  //             $final_data[$no][$val_pbu->id]['pbl'][$keyc1]['isi_pid'][$keyc3]['id_pbl'] = $val_pbu->id_pbl;
  //             $final_data[$no][$val_pbu->id]['pbl'][$keyc1]['isi_pid'][$keyc3]['pid'] = $valc3;
  //             $find_pid = DB::table('procurement_pid')->where('pid', $valc3)->first();
  //             $final_data[$no][$val_pbu->id]['pbl'][$keyc1]['isi_pid'][$keyc3]['id_pid_req'] = $find_pid->id;
  //             $final_data[$no][$val_pbu->id]['pbl'][$keyc1]['isi_pid'][$keyc3]['budget'] = 300000;

  //             DB::table('procurement_req_pid')->insert([
  //               'id_upload' => $val_pbu->id,
  //               'id_lokasi' => $val_pbu->id_pbl,
  //               'id_pid_req' => $find_pid->id,
  //               'budget' => 300000,
  //               'created_at' => 18940469
  //             ]);
  //           }
  //         }
  //       }
  //     }
  //     // dd($final_data);
	// 	});
  // }

  public static function save_filepsb(Request $req)
  {
    $id_file = $req->input('id_file');
    switch ($id_file) {
      case 'justifikasi':
            AdminModel::saveCreateJustifikasiPSB($req);

            return back()->with('alerts', [
              ['type' => 'success', 'text' => 'Justifikasi & Rincian SSL (PSB) Berhasil Dibuat']
            ]);
        break;

      case 'dokumenpelengkap':
            AdminModel::PengisianNomorSuratPSB($req);

            return back()->with('alerts', [
              ['type' => 'success', 'text' => 'Nomor Surat dan Lainnya Berhasil di Perbaharui']
            ]);
        break;

      case 'cutoff_psb':
        $get_sc = str_replace(array("\r\n", "\n", " "), "", explode(';', $req->input('sc_cutoff')));
        $insert[] = [];

        $null_kpro = $duplicate_cutoff = null;
        foreach ($get_sc as $value)
        {
          if ($value != "")
          {
            $check_kpro = DB::table('kpro_tr6')->where('ORDER_ID', $value)->first();
            $check_layanan = DB::table('procurement_layanan_psb')->where('alias_layanan', $req->layanan)->first();
            if ($check_kpro == null)
            {
              $layanan = strtoupper($check_layanan->layanan);
              $null_kpro .= "$value ($layanan); ";
            }
            else
            {
              $check = DB::table('procurement_cutoff_psb')->where('sc_id', $value)->first();

              if ($check == null)
              {
                $insert[] = [
                  'tahun_kegiatan'   => date('Y'),
                  'periode_kegiatan' => $req->input('periode_kegiatan'),
                  'periode1'         => $req->input('periode1'),
                  'periode2'         => $req->input('periode2'),
                  'mitra_id'         => $req->input('mitra'),
                  'sc_id'            => $value,
                  'layanan'          => $req->input('layanan'),
                  'created_by'       => session('auth')->id_user
                ];
              }
              else
              {
                $layanan = strtoupper($check_layanan->layanan);
                $duplicate_cutoff .= "$value ($layanan); ";
              }
            }
          }
        }

        if ($null_kpro == null && $duplicate_cutoff == null)
        {
          if (array_filter($insert))
          {
            $chunk = array_chunk(array_filter($insert), 500);
            foreach($chunk as $data)
            {
              DB::table('procurement_cutoff_psb')->insert($data);
            }
          }
        }

        if ($null_kpro != null && $duplicate_cutoff != null)
        {
          return back()->with('alerts', [
            ['type' => 'success', 'text' => 'Data Cutoff PSB Periode '.$req->input('periode1').' s/d '.$req->input('periode2').' Berhasil di Simpan'],
            ['type' => 'danger', 'text' => '<strong>Data Rejected</strong> : '.$null_kpro.' Tidak Ada di <strong>K-PRO</strong>!'],
            ['type' => 'danger', 'text' => '<strong>Data Rejected</strong> : '.$duplicate_cutoff.' Sudah Masuk <strong>Cutoff</strong> Sebelumnya!']
          ]);
        }
        elseif ($null_kpro != null)
        {
          return back()->with('alerts', [
            ['type' => 'success', 'text' => 'Data Cutoff PSB Periode '.$req->input('periode1').' s/d '.$req->input('periode2').' Berhasil di Simpan'],
            ['type' => 'danger', 'text' => '<strong>Data Rejected</strong> : '.$null_kpro.' Tidak Ada di <strong>K-PRO</strong>!']
          ]);
        }
        elseif ($duplicate_cutoff != null)
        {
          return back()->with('alerts', [
            ['type' => 'success', 'text' => 'Data Cutoff PSB Periode '.$req->input('periode1').' s/d '.$req->input('periode2').' Berhasil di Simpan'],
            ['type' => 'danger', 'text' => '<strong>Data Rejected</strong> : '.$duplicate_cutoff.' Sudah Masuk <strong>Cutoff</strong> Sebelumnya!']
          ]);
        }
        else
        {
          return back()->with('alerts', [
            ['type' => 'success', 'text' => 'Data Cutoff PSB Periode '.$req->input('periode1').' s/d '.$req->input('periode2').' Berhasil di Simpan']
          ]);
        }
        break;

      case 'saveDataRekonPsb':
        AdminModel::saveDataRekonPsb($req);

        return back()->with('alerts', [
          ['type' => 'success', 'text' => 'Data Rekon PSB Mitra Berhasil di Simpan']
        ]);
        break;

      case 'doc_generate_new':
        AdminModel::save_doc_generate_new($req);

        return back()->with('alerts', [
          ['type' => 'success', 'text' => 'Success! Generate Documents']
        ]);
        break;
    }
  }

  public function download_filepsb($type, $id, $output)
  {
    $data = AdminModel::download_filepsb($type, $id);

    switch ($type) {
      case 'justifikasi':
            Excel::store(new Excel_RincianJustifikasiPSB($data, $output),  'Justifikasi_dan_Rincian_SSL_(PSB)_Mitra.xlsx');
        break;

      case 'surat_pesanan':
            Excel::store(new Excel_SuratPesananPSB($data, $output),  'Surat_Pesanan_PSB_Mitra.xlsx');
        break;

      case 'dokumen_pelengkap':
            Excel::store(new Excel_SuratDokumenPelengkapPSB($data, $output),  'Dokumen_Pelengkap_Lainnya_Mitra.xlsx');
        break;

      case 'dokpelengkap_skema_orderbased':
            Excel::store(new Excel_DokPelengkapSkemaOrderBased($data, $output),  'DokPelengkap_Skema_OrderBased.xlsx');
        break;

      case 'dokpelengkap_skema_orderbasedxorder':
            Excel::store(new Excel_DokPelengkapSkemaOrderBasedxOrder($data, $output),  'DokPelengkap_Skema_OrderBasedxOrder.xlsx');
        break;

      case 'dokpelengkap_skema_orderbasedxkalimantan':
            Excel::store(new Excel_DokPelengkapSkemaOrderBasedxKalimantan($data, $output),  'DokPelengkap_Skema_OrderBasedxKalimantan.xlsx');
        break;
    }
  }

  function search_material(Request $req)
  {
    $data = [];

    if($req->pekerjaan)
    {
      $check_data = AdminModel::searching_material_complex($req);

      foreach ($check_data as $row) {
        $data[] = array("id" => $row->id, "text" => $row->designator .' ('.$row->jenis.' | '. $row->namcomp .')', "jenis" => $row->jenis, "id_mitra" => $row->design_mitra_id, "design_mitra_id" => $row->design_mitra_id, "id_design" => $row->id, "material" => $row->material, "jasa" => $row->jasa, "uraian" => html_entity_decode($row->uraian, ENT_QUOTES, 'UTF-8'), "designator" => $row->designator, "namcomp" => $row->namcomp);
      }
    }
    return \Response::json($data);
  }

	public function download_rfc($id, $no_rfc)
	{
    $check_path = '/mnt/hdd4/upload/storage/rfc_ttd/';
    $files = @preg_grep('~^'.$no_rfc.'.*$~', scandir($check_path) );
    if(count($files) != 0)
    {
      $files = array_values($files);
      $file = $check_path.'/'.$files[0];
      header('Content-Description: File Transfer');
      header('Content-Type: application/octet-stream');
      header('Content-Disposition: attachment; filename='.basename($file));
      header('Expires: 0');
      header('Cache-Control: must-revalidate');
      header('Pragma: public');
      header('Content-Length: ' . filesize($file));
      ob_clean();
      flush();
      readfile($file);
      exit;
    }
    else
    {
      $check_path = public_path() . '/upload2/' . $id . '/dokumen_rfc_up/';
      $rfc_file = @preg_grep('~^'.$no_rfc.'.*$~', scandir($check_path) );
      if(count($rfc_file) != 0)
      {
        $rfc_file = array_values($rfc_file);
        $file = $check_path.'/'.$rfc_file[0];
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename='.basename($file));
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($file));
        ob_clean();
        flush();
        readfile($file);
        exit;
      }
    }
	}

  public function download_rfc_bulk($id)
  {
    $data_rfc = MitraModel::get_rfc_history($id);
    $data = AdminModel::get_data_final($id);
    $response = '';
    $rfc = [];

    foreach($data_rfc as $kc1 => $valc1)
		{
			$rfc[$kc1]['rfc'] = $valc1->rfc;
		}

    foreach($rfc as $key => $val)
    {
      $check_path = '/mnt/hdd4/upload/storage/rfc_ttd/';
      $rfc_file = @preg_grep('~^'.$val['rfc'].'.*$~', scandir($check_path) );
      $path= '';
      // dd($rfc_file);
      if(count($rfc_file) != 0)
      {
        $files = array_values($rfc_file);
        $path = "/mnt/hdd4/upload/storage/rfc_ttd/".$files[0];
      }
      else
      {
        $check_path = public_path() . '/upload2/' . $id . '/dokumen_rfc_up/';
        $rfc_file = @preg_grep('~^'.$val['rfc'].'.*$~', scandir($check_path) );

        if(count($rfc_file) != 0)
        {
          $files = array_values($rfc_file);
          $path = public_path() . '/upload2/' . $id . '/dokumen_rfc_up/'.$files[0];
        }
      }

      if($path)
      {
        if (!file_exists(public_path() . '/upload2/' . $id . '/procurement/rfc/')) {
          if (!mkdir(public_path() . '/upload2/' . $id . '/procurement/rfc/', 0770, true)) {
            return 'gagal menyiapkan folder data_mitra';
          }
        }
        \File::copy($path , (public_path() . '/upload2/' . $id . '/procurement/rfc/'.$files[0]) );
      }
    }

    if(count($data_rfc) )
    {
      $rootPath = public_path() . '/upload2/' . $id . '/procurement/rfc/';
      $new_name_file = $data->judul.'_RFC.zip';

      $zip = new ZipArchive();
      $zip->open($new_name_file, ZipArchive::CREATE | ZipArchive::OVERWRITE);

      $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($rootPath) , RecursiveIteratorIterator::LEAVES_ONLY);

      foreach ($files as $name => $file)
      {
        if (!$file->isDir())
        {
          $filePath = $file->getRealPath();
          $relativePath = substr($filePath, strlen($rootPath) + 1);
          $zip->addFile($filePath, $file->GetFileName());
        }
      }
      $zip->close();
      \File::deleteDirectory($rootPath);

      $headers =['Content-type: application/zip'];

      $response = response()->download(public_path() .'/'. $new_name_file, $new_name_file, $headers);
      register_shutdown_function('unlink', public_path() .'/'. $new_name_file);
      register_shutdown_function('unlink', public_path() .'/upload/' . $id . '/procurement/rfc/');
      // Here file is not deleted yet
    }

		return $response;
  }

	public function sp_budgetP(Request $req, $jenis, $id)
	{
    $msg = AdminModel::update_budget_sp($req, $jenis, $id);

    return redirect('/progressList/3')->with('alerts', [
      ['type' => 'warning', 'text' => "Pekerjaan Sudah Dikembalikan Karena $msg"]
    ]);
	}

	public function download_boq_op($id)
	{
    $check_data = ReportModel::get_boq_data($id);

		$all_data = DB::table('procurement_dok_ttd')
    ->where([
			['pekerjaan', $check_data->pekerjaan],
			['witel', $check_data->witel],
		])
		->GroupBy('rule_dok')
		->get();

		$class = [];

		foreach($all_data as $v)
		{
			$class[$v->rule_dok] = '';
		}

		$find_user = DB::table('procurement_dok_ttd As pdt')
		->leftjoin('procurement_user As ps', 'pdt.id_user', '=', 'ps.id')
		->where([
			['pdt.pekerjaan', $check_data->pekerjaan],
			['pdt.witel', $check_data->witel],
		])
		->get();

		foreach($class as $k => $v)
		{
			$find_k = array_keys(array_column(json_decode(json_encode(@$find_user), TRUE ), 'rule_dok'), $k);

      $data[$k]['id'] = $k;

      if(!isset($data[$k]['avail']) )
      {
        $data[$k]['avail'] = 0;
        $data[$k]['expired'] = 0;
      }

      foreach($find_k as $vx)
      {
        $data_k = $find_user[$vx];

        if(date('Y-m-d', strtotime($data_k->tgl_start) ) <= date('Y-m-d') && date('Y-m-d', strtotime($data_k->tgl_end) ) >= date('Y-m-d') )
        {
          $data[$k]['avail'] += 1;
        }
        else
        {
          $data[$k]['expired'] += 1;
        }
      }
		}

    $find_avail = array_column($data, 'avail');

    // if(array_search(0, $find_avail) !== FALSE)
    // {
    //   return back()->with('alerts', [
    //     ['type' => 'warning', 'text' => "Dokumen tidak bisa diunduh karena terdeteksi user sudah melewati masa tenggang" ]
    //   ]);
    // }

		$that_op = new \App\Http\Controllers\OperationController();
    return $that_op::download_boq_operation($id, 'dwn', 'status');
	}

	public function dashboard_compare_rfc()
	{
    $data_rfc = AdminModel::get_rfc_boq('all');

		$design_rfc = AdminModel::get_design_boq(0, [1], 'dash_compare_rfc');
    $design_rfc = json_decode(json_encode($design_rfc), TRUE);

    $all_design_rfc = AdminModel::find_all_rfc();
    $all_design_rfc = json_decode(json_encode($all_design_rfc), TRUE);

    $rfc = $volum_design = $rekap_mat = [];
    $count = $material_rfc = $jasa_rfc = 0;
    $cari = false;

    foreach($data_rfc as $kc1 => $valc1)
    {
      $key_adr_pid   = array_keys(array_column($all_design_rfc, 'design_rfc'), $valc1->material);
      $key_dr_id_pbu = array_keys(array_column($design_rfc, 'id_pbu'), $valc1->id_pbu );

      foreach($key_dr_id_pbu as $v)
      {
        $key_adr_design = array_search($design_rfc[$v]['designator'], array_column($all_design_rfc, 'designator') );

        if($key_adr_design !== FALSE)
        {
          $sum_rekon = [];
          foreach($key_adr_pid as $vpid)
          {
            if($all_design_rfc[$vpid]['designator'] == $design_rfc[$v]['designator'])
            {
              $material_rekon[$design_rfc[$v]['id'] ] = (int)$design_rfc[$v]['rekon'] * (int)$design_rfc[$v]['material'];
              $jasa_rekon[$design_rfc[$v]['id'] ]     = (int)$design_rfc[$v]['rekon'] * (int)$design_rfc[$v]['jasa'];

              $material_rfc += ($valc1->terpakai * $all_design_rfc[$vpid]['qty_per_item']) * $design_rfc[$v]['material'];
              $jasa_rfc     += ($valc1->terpakai * $all_design_rfc[$vpid]['qty_per_item']) * $design_rfc[$v]['jasa'];

              $dashboard_mitra[$valc1->mitra_nm][$design_rfc[$v]['designator'] ]['id'] = $design_rfc[$v]['id_design'];

              if(!isset($dashboard_mitra[$valc1->mitra_nm][$design_rfc[$v]['designator'] ]['terpakai'])   )
              {
                $dashboard_mitra[$valc1->mitra_nm][$design_rfc[$v]['designator'] ]['terpakai'] = [];
                $dashboard_mitra[$valc1->mitra_nm][$design_rfc[$v]['designator'] ]['rfc'] = 0;
              }

              $dashboard_mitra[$valc1->mitra_nm][$design_rfc[$v]['designator'] ]['terpakai'][$design_rfc[$v]['id'] ] = $design_rfc[$v]['rekon'];

              if($valc1->type == 'Out')
              {
                $dashboard_mitra[$valc1->mitra_nm][$design_rfc[$v]['designator'] ]['rfc'] += ($valc1->terpakai * $all_design_rfc[$vpid]['qty_per_item']);
              }
              else
              {
                $dashboard_mitra[$valc1->mitra_nm][$design_rfc[$v]['designator'] ]['rfc'] -= ($valc1->terpakai * $all_design_rfc[$vpid]['qty_per_item']);
              }

              if(!isset($rekap_mat[$design_rfc[$v]['designator'] ]['jml_rfc'][$valc1->judul] ) )
              {
                $rekap_mat[$design_rfc[$v]['designator'] ]['jml_rfc'][$valc1->judul] = 0;
              }

              $rekap_mat[$design_rfc[$v]['designator'] ]['jml_rekon'][$valc1->judul][$design_rfc[$v]['id'] ] = (int)$design_rfc[$v]['rekon'];
              $rekap_mat[$design_rfc[$v]['designator'] ]['jml_rfc'][$valc1->judul] += ($valc1->terpakai * $all_design_rfc[$vpid]['qty_per_item']);
            }
          }
        }
      }
    }

    foreach($rekap_mat as $k => $v)
    {
      foreach($v as $kk => $vv)
      {
        $rekap_mat[$k][$kk] = array_sum($vv);
      }
    }

    foreach($dashboard_mitra as $k => $v)
    {
      $result_diff = array_diff(array_keys($rekap_mat), array_keys($v) );
      $dm[$k] = $v;

      foreach($v as $kkx => $vvx)
      {
        $dm[$k][$kkx]['terpakai'] = array_sum($vvx['terpakai']);
        $dm[$k][$kkx]['selisih'] = abs((int)array_sum($vvx['terpakai']) - (int)$vvx['rfc']);
      }

      foreach($result_diff as $vv)
      {
        $dm[$k][$vv]['terpakai'] = 0;
        $dm[$k][$vv]['rfc']      = 0;
        $dm[$k][$vv]['selisih']  = 0;
      }
    }

    ksort($dm);

    foreach($dm as &$value) {
      ksort($value);
    }

    $headers = array_keys(current(array_values($dm) ) );
    asort($headers);
    // dd($dashboard_mitra, $dm, $headers);
    return view('Tools.dashboard_comparing_rfc', compact('material_rfc', 'jasa_rfc', 'dm'), ['header_mat' => $headers, 'material_rekon' => array_sum($material_rekon), 'jasa_rekon' => array_sum($jasa_rekon)] );
	}

	public function comp_rfc($mitra)
	{
    $mitra = urldecode($mitra);

    $data_rfc = AdminModel::get_rfc_boq($mitra, 'mitra');

		$design_rfc = AdminModel::get_design_boq(0, [1, 2], 'dash_compare_rfc');
    $design_rfc = json_decode(json_encode($design_rfc), TRUE);

    $all_design_rfc = AdminModel::find_all_rfc();
    $all_design_rfc = json_decode(json_encode($all_design_rfc), TRUE);

    $rfc = $volum_design = $rekap_mat = [];
    $count = $material_rfc = $jasa_rfc = 0;
    $cari = false;

    foreach($data_rfc as $kc1 => $valc1)
    {
      $key_adr_pid   = array_keys(array_column($all_design_rfc, 'design_rfc'), $valc1->material);
      $key_dr_id_pbu = array_keys(array_column($design_rfc, 'id_pbu'), $valc1->id_pbu );

      foreach($key_dr_id_pbu as $v)
      {
        $key_adr_design = array_search($design_rfc[$v]['designator'], array_column($all_design_rfc, 'designator') );

        if($key_adr_design !== FALSE)
        {
          foreach($key_adr_pid as $vpid)
          {
            if($all_design_rfc[$vpid]['designator'] == $design_rfc[$v]['designator'])
            {
              $volum_design[$valc1->mitra_nm][$valc1->judul][$design_rfc[$v]['designator'] ]['material'] = $design_rfc[$v]['designator'];
              $volum_design[$valc1->mitra_nm][$valc1->judul][$design_rfc[$v]['designator'] ]['nama_material_rfc'][$valc1->material]['nama'] = $valc1->material;

              if(!isset($volum_design[$valc1->mitra_nm][$valc1->judul][$design_rfc[$v]['designator'] ]['nama_material_rfc'][$valc1->material]['qty']) )
              {
                $volum_design[$valc1->mitra_nm][$valc1->judul][$design_rfc[$v]['designator'] ]['nama_material_rfc'][$valc1->material]['qty'] = 0;
              }

              $volum_design[$valc1->mitra_nm][$valc1->judul][$design_rfc[$v]['designator'] ]['nama_material_rfc'][$valc1->material]['rfc'][$valc1->rfc]['isi'] = $valc1->quantity;
              $volum_design[$valc1->mitra_nm][$valc1->judul][$design_rfc[$v]['designator'] ]['nama_material_rfc'][$valc1->material]['rfc'][$valc1->rfc]['id_boq'] = $valc1->id_pbu;

              if($valc1->type == 'Out')
              {
                $volum_design[$valc1->mitra_nm][$valc1->judul][$design_rfc[$v]['designator'] ]['nama_material_rfc'][$valc1->material]['qty'] += $valc1->quantity;
              }

              $material_rekon[$design_rfc[$v]['id'] ] = (int)$design_rfc[$v]['rekon'] * (int)$design_rfc[$v]['material'];
              $jasa_rekon[$design_rfc[$v]['id'] ]     = (int)$design_rfc[$v]['rekon'] * (int)$design_rfc[$v]['jasa'];

              $material_rfc += ($valc1->terpakai * $all_design_rfc[$vpid]['qty_per_item']) * $design_rfc[$v]['material'];
              $jasa_rfc     += ($valc1->terpakai * $all_design_rfc[$vpid]['qty_per_item']) * $design_rfc[$v]['jasa'];

              if($valc1->keterangan)
              {
                $volum_design[$valc1->mitra_nm][$valc1->judul][$design_rfc[$v]['designator'] ]['nama_material_rfc'][$valc1->material]['keterangan'][$valc1->keterangan] = $valc1->keterangan;
              }

              $volum_design[$valc1->mitra_nm][$valc1->judul][$design_rfc[$v]['designator'] ]['total_volum_design'][$design_rfc[$v]['id'] ] = $design_rfc[$v]['rekon'];

              if(!isset($volum_design[$valc1->mitra_nm][$valc1->judul][$design_rfc[$v]['designator'] ]['total_volum_rfc'] ) )
              {
                $volum_design[$valc1->mitra_nm][$valc1->judul][$design_rfc[$v]['designator'] ]['total_volum_rfc'] = 0;
              }

              if($valc1->type == 'Out')
              {
                $volum_design[$valc1->mitra_nm][$valc1->judul][$design_rfc[$v]['designator'] ]['total_volum_rfc'] += ($valc1->terpakai * $all_design_rfc[$vpid]['qty_per_item']);
              }
              else
              {
                $volum_design[$valc1->mitra_nm][$valc1->judul][$design_rfc[$v]['designator'] ]['total_volum_rfc'] -= ($valc1->terpakai * $all_design_rfc[$vpid]['qty_per_item']);
              }

              if(!isset($rekap_mat[$design_rfc[$v]['designator'] ]['jml_rfc'][$valc1->judul] ) )
              {
                $rekap_mat[$design_rfc[$v]['designator'] ]['jml_rfc'][$valc1->judul] = 0;
              }

              $rekap_mat[$design_rfc[$v]['designator'] ]['jml_rekon'][$valc1->judul][$design_rfc[$v]['id'] ] = (int)$design_rfc[$v]['rekon'];
              $rekap_mat[$design_rfc[$v]['designator'] ]['jml_rfc'][$valc1->judul]  += ($valc1->terpakai * $all_design_rfc[$vpid]['qty_per_item']);
            }
          }
        }
      }
    }

    foreach($volum_design as $k => $v)
    {
      foreach($v as $kk => $vv)
      {
        foreach($vv as $kkk => $vvv)
        {
          $volum_design[$k][$kk][$kkk]['total_volum_design'] = array_sum($vvv['total_volum_design']);
        }
      }
    }

    foreach($rekap_mat as $k => $v)
    {
      foreach($v['jml_rekon'] as $kk => $vv)
      {
        $rekap_mat[$k]['jml_rekon'][$kk] = array_sum($vv);
      }
    }

    foreach($rekap_mat as $k => $v)
    {
      foreach($v as $kk => $vv)
      {
        $rekap_mat[$k][$kk] = array_sum($vv);
      }

      $rekap_mat[$k]['sisa'] = $rekap_mat[$k]['jml_rekon'] - $rekap_mat[$k]['jml_rfc'];
    }
    // dd($volum_design);
    return view('Tools.list_comparing_rfc', compact('volum_design', 'material_rfc', 'jasa_rfc'), ['material_rekon' => array_sum($material_rekon), 'jasa_rekon' => array_sum($jasa_rekon)] );
	}

	public function upload_file_mass()
	{
		return view('Tools.upload_massal');
	}

	public function submit_file_mass(Request $req)
	{
		$path = public_path()."/upload/temuan/";
		$this->handlefile_massal($path, $req);

    return back()->with('alerts', [
			['type' => 'info', 'text' => 'Berhasil Upload Massal']
		]);
	}

	private function handlefile_massal($path, $req)
	{
		if (!file_exists($path) )
		{
			if (! mkdir($path, 0770, true)){
				return [
					'type' => 'danger',
					'text' => 'Gagal Menyiapkan Folder'
				];
			}
		}

		foreach($req->file('file') as $fto)
		{
			$name = $fto->getClientOriginalName();
			try
			{
				$moved = $fto->move("$path", $name);
			}
			catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e)
			{
				return [
					'type' => 'danger',
					'text' => 'Gagal Upload'
				];
			}
		}
	}

	public function load_user_ttd(Request $req)
	{
    $data_user  = ReportModel::get_list_user_dok($req->job, $req->isi, 'search');
    return \Response::json($data_user);
	}

	public function rfc_wh_sync()
	{
    $data = ReportModel::get_list_rfc_wh_sync();

    foreach($data as $v)
    {
      $fd[$v->designator][] = $v;
    }

    $parent_rfc = $parent_wh = [];

    foreach($fd as $k => $v)
    {
      if(count($v) > 1)
      {
        $parent_wh[$k] = $v;
      }
      else
      {
        foreach($v as $vv)
        {
          $parent_rfc[$vv->design_rfc][] = $k;
        }
      }
    }

    return view('Tools.list_rfc_wh', compact('parent_wh', 'parent_rfc') );
	}

  public function ttd_user($pekerjaan, $witel, $rule)
  {
      return DB::table('procurement_dok_ttd')
      ->leftJoin('procurement_user', 'procurement_dok_ttd.id_user', '=', 'procurement_user.id')
      ->select('procurement_user.*')
      ->where('procurement_dok_ttd.witel', $witel)
      ->where('procurement_dok_ttd.pekerjaan', $pekerjaan)
      ->where('procurement_dok_ttd.rule_dok', $rule)
      ->first();
  }
}