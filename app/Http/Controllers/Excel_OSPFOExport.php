<?php

namespace App\Http\Controllers;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Events\BeforeSheet;
use Maatwebsite\Excel\Events\BeforeWriting;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use Maatwebsite\Excel\Excel;
use App\Http\Controllers\ProgressController;
use DB;
use App\DA\ReportModel;
use App\DA\AdminModel;

class Excel_OSPFOExport implements WithEvents
{
    use Exportable;

    public function __construct($data)
    {
        $this->data  = $data[0];
        $this->data1 = $data[1];
        $this->data2 = $data[2];
        $this->data3 = $data[3];
        $this->data4 = $data[4];
        $this->data5 = $data[5];
        $this->opsi  = isset($data[6]) ? $data[6] : '';
        $this->datasl = $data[7];
    }

    public function convert_month($date, $cetak_hari = false)
    {
        $bulan = array (1 =>   'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );

        $hari = array ( 1 => 'Senin',
            'Selasa',
            'Rabu',
            'Kamis',
            'Jumat',
            'Sabtu',
            'Minggu'
        );

        $result = date('d m Y', strtotime($date) );
        $split = explode(' ', $result);
        $hasilnya = $split[0] . ' ' . $bulan[(int)$split[1] ] . ' ' . $split[2];

        if($cetak_hari)
        {
            $num = date('N', strtotime($date) );
            return $hari[$num].' '.$hasilnya;
        }

        return $hasilnya;
    }

    public function only_day($date)
    {
        $hari = array ( 1 => 'Senin',
            'Selasa',
            'Rabu',
            'Kamis',
            'Jumat',
            'Sabtu',
            'Minggu'
        );

        $num = date('N', strtotime($date) );
        return $hari[$num];
    }

    public function terbilang($nilai)
    {
        if ($nilai < 0)
        {
            $hasil = "minus " . trim($this->penyebut($nilai) );
        }
        else
        {
            $hasil = trim($this->penyebut($nilai) );
        }
        $hasil = preg_replace('/\s+/', ' ', $hasil);
        return $hasil;
    }

    public function penyebut($nilai)
    {
        $nilai = abs($nilai);
        $huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
        $temp = "";

        if($nilai < 12)
        {
            $temp = " " . $huruf[$nilai];
        }
        else if($nilai < 20)
        {
            $temp = $this->penyebut($nilai - 10) . " belas";
        }
        else if($nilai < 100)
        {
            $temp = $this->penyebut($nilai / 10) . " puluh" . $this->penyebut($nilai % 10);
        }
        else if($nilai < 200)
        {
            $temp = " seratus" . $this->penyebut($nilai - 100);
        }
        else if($nilai < 1000)
        {
            $temp = $this->penyebut($nilai / 100) . " ratus" . $this->penyebut($nilai % 100);
        }
        else if($nilai < 2000)
        {
            $temp = " seribu" . $this->penyebut($nilai - 1000);
        }
        else if($nilai < 1000000)
        {
            $temp = $this->penyebut($nilai / 1000) . " ribu" . $this->penyebut($nilai % 1000);
        }
        else if($nilai < 1000000000)
        {
            $temp = $this->penyebut($nilai / 1000000) . " juta" . $this->penyebut($nilai % 1000000);
        }
        else if($nilai < 1000000000000)
        {
            $temp = $this->penyebut($nilai / 1000000000) . " milyar" . $this->penyebut(fmod($nilai, 1000000000) );
        }
        else if($nilai < 1000000000000000)
        {
            $temp = $this->penyebut($nilai / 1000000000000) . " trilyun" . $this->penyebut(fmod($nilai, 1000000000000) );
        }

        return$temp;
    }

    public function registerEvents(): array
    {
        return [
            BeforeExport::class  => function(BeforeExport $event) {
                $that = new ProgressController();

                $fill_border_lampir = [
                    'fill' => [
                        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                        'startColor' => [
                            'argb' => '4CAEE3',
                        ],
                    ]
                ];

                $fill_border_nego_sepakat = [
                    'fill' => [
                        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                        'startColor' => [
                            'argb' => '595959',
                        ],
                    ]
                ];

                $border_Style = [
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
                        ],
                    ],
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    ]
                ];

                $reader = new Xlsx();
                \PhpOffice\PhpSpreadsheet\Shared\File::setUseUploadTempDirectory(true);
                $spreadSheet = $reader->load(public_path().'/template_doc/maintenance_down_2021/Template Invoice & BA OSP FO.xlsx');

                $security = $spreadSheet->getSecurity();

                $spreadSheet->getActiveSheet()->getProtection()->setSheet(true);
                $spreadSheet->getActiveSheet()->getProtection()->setInsertRows(true);

                $security->setLockWindows(true);
                $security->setLockStructure(true);

                $security->setWorkbookPassword($that->key_data() );

                $bulan = array (
                    'Januari'   => '01',
                    'Februari'  => '02',
                    'Maret'     => '03',
                    'April'     => '04',
                    'Mei'       => '05',
                    'Juni'      => '06',
                    'Juli'      => '07',
                    'Agustus'   => '08',
                    'September' => '09',
                    'Oktober'   => '10',
                    'November'  => '11',
                    'Desember'  => '12'
                );

                $bulan_angk = array (
                    '01' => 'Januari',
                    '02' => 'Februari',
                    '03' => 'Maret',
                    '04' => 'April',
                    '05' => 'Mei',
                    '06' => 'Juni',
                    '07' => 'Juli',
                    '08' => 'Agustus',
                    '09' => 'September',
                    '10' => 'Oktober',
                    '11' => 'November',
                    '12' => 'Desember'
                );
                // $data->no_khs_maintenance;
                // dd($data);

                if($this->data->tgl_pks == 0000-00-00)
                {
                    $this->data->tgl_pks = null;
                }
                // dd($this->data);
                $AdminModel = new AdminModel();
                $pph       = $AdminModel->find_pph($this->data->tgl_ba_rekon);
		        $pph_text  = $AdminModel->find_pph_text($this->data->tgl_ba_rekon);

                $pph_sp       = $AdminModel->find_pph($this->data->tgl_sp);
		        $pph_text_sp  = $AdminModel->find_pph_text($this->data->tgl_sp);

                $pph_amd       = $AdminModel->find_pph($this->data->tgl_amd_sp);
		        $pph_text_amd  = $AdminModel->find_pph_text($this->data->tgl_amd_sp);

                $pph_faktur       = $AdminModel->find_pph($this->data->tgl_faktur);
		        $pph_text_faktur  = $AdminModel->find_pph_text($this->data->tgl_faktur);

                $pph_bast       = $AdminModel->find_pph($this->data->tgl_bast);
		        $pph_text_bast  = $AdminModel->find_pph_text($this->data->tgl_bast);
                // dd($pph_faktur, $this->data->tgl_faktur);
                // $foundInCells = array();
                // $searchTerm = 'PT. TELKOM AKSES';

                // foreach ($spreadSheet->getWorksheetIterator() as $CurrentWorksheet)
                // {
                //     $ws = $CurrentWorksheet->getTitle();

                //     foreach ($CurrentWorksheet->getRowIterator() as $row) {

                //         $cellIterator = $row->getCellIterator();
                //         $cellIterator->setIterateOnlyExistingCells(FALSE);

                //         foreach ($cellIterator as $cell) {

                //             if (!empty($cell->getValue() ) && strpos(strtolower($cell->getValue() ), strtolower($searchTerm)) !== FALSE)
                //             {
                //                 $foundInCells[] = $ws . '!' . $cell->getCoordinate();
                //             }
                //         }
                //     }
                // }

                // dd($foundInCells);

                $spreadSheet->getSheet(0)->getCell('A1')->SetValue(substr_replace($this->data->mitra_nm, 'PT.', 0, 2) );
                $spreadSheet->getSheet(0)->getCell('B1')->SetValue($pph_text);
                $spreadSheet->getSheet(0)->getCell('C1')->SetValue($pph_text_sp);
                $spreadSheet->getSheet(0)->getCell('D1')->SetValue($pph_text_amd);
                $spreadSheet->getSheet(0)->getCell('E1')->SetValue($pph_text_faktur);
                $spreadSheet->getSheet(0)->getCell('B2')->SetValue($pph);
                $spreadSheet->getSheet(0)->getCell('C2')->SetValue($pph_sp);
                $spreadSheet->getSheet(0)->getCell('D2')->SetValue($pph_amd);
                $spreadSheet->getSheet(0)->getCell('E2')->SetValue($pph_faktur);
                $spreadSheet->getSheet(0)->getCell('F1')->SetValue($pph_bast);

                $pph_gd_rekon = $AdminModel->convert_floor_ceil($this->data->tgl_ba_rekon, $pph * $this->data->gd_rekon);
                $pph_sp_rekon = $AdminModel->convert_floor_ceil($this->data->tgl_sp, $pph_sp * $this->data->gd_rekon);
                $pph_amd_rekon = $AdminModel->convert_floor_ceil($this->data->tgl_amd_sp, $pph_amd * $this->data->gd_rekon);
                $pph_faktur_rekon = $AdminModel->convert_floor_ceil($this->data->tgl_faktur, $pph_faktur * $this->data->gd_rekon);

                $spreadSheet->getSheet(0)->getCell('H1')->SetValue($pph_gd_rekon);
                $spreadSheet->getSheet(0)->getCell('H2')->SetValue($pph_sp_rekon);
                $spreadSheet->getSheet(0)->getCell('H3')->SetValue($pph_amd_rekon);
                $spreadSheet->getSheet(0)->getCell('H4')->SetValue($pph_faktur_rekon);

                $pph_gd_sp_rekon = $AdminModel->convert_floor_ceil($this->data->tgl_ba_rekon, $pph * $this->data->gd_sp);
                $grand_total_sp = $AdminModel->convert_floor_ceil($this->data->tgl_sp, $pph_sp * $this->data->gd_sp);
                $gd_sp_amd = $AdminModel->convert_floor_ceil($this->data->tgl_amd_sp, $pph_amd * $this->data->gd_sp);
                $gd_sp_faktur = $AdminModel->convert_floor_ceil($this->data->tgl_faktur, $pph_faktur * $this->data->gd_sp);

                $spreadSheet->getSheet(0)->getCell('H7')->SetValue(ucwords($this->terbilang(number_format($this->data->gd_sp + $pph_gd_sp_rekon, 0, '', '') ) . ' Rupiah' ) );
                $spreadSheet->getSheet(0)->getCell('H8')->SetValue(ucwords($this->terbilang(number_format($this->data->gd_sp + $grand_total_sp, 0, '', '') ) . ' Rupiah' ) );
                $spreadSheet->getSheet(0)->getCell('H9')->SetValue(ucwords($this->terbilang(number_format($this->data->gd_sp + $gd_sp_amd, 0, '', '') ) . ' Rupiah' ) );
                $spreadSheet->getSheet(0)->getCell('H10')->SetValue(ucwords($this->terbilang(number_format($this->data->gd_sp + $gd_sp_faktur, 0, '', '') ) . ' Rupiah' ) );

                $spreadSheet->getSheet(0)->getCell('H13')->SetValue(number_format($this->data->gd_sp + $pph_gd_sp_rekon) );
                $spreadSheet->getSheet(0)->getCell('H14')->SetValue(number_format($this->data->gd_sp + $grand_total_sp) );
                $spreadSheet->getSheet(0)->getCell('H15')->SetValue(number_format($this->data->gd_sp + $gd_sp_amd) );
                $spreadSheet->getSheet(0)->getCell('H16')->SetValue(number_format($this->data->gd_sp + $gd_sp_faktur) );

                $spreadSheet->getSheet(0)->getCell('G1')->SetValue($pph_gd_sp_rekon);
                $spreadSheet->getSheet(0)->getCell('G2')->SetValue($grand_total_sp);
                $spreadSheet->getSheet(0)->getCell('G3')->SetValue($gd_sp_amd);
                $spreadSheet->getSheet(0)->getCell('G4')->SetValue($gd_sp_faktur);

                $spreadSheet->getSheet(0)->getCell('G7')->SetValue(ucwords($this->terbilang(number_format($this->data->gd_rekon + $pph_gd_rekon, 0, '', '') ) . ' Rupiah' ) );
                $spreadSheet->getSheet(0)->getCell('G8')->SetValue(ucwords($this->terbilang(number_format($this->data->gd_rekon + $pph_sp_rekon, 0, '', '') ) . ' Rupiah' ) );
                $spreadSheet->getSheet(0)->getCell('G9')->SetValue(ucwords($this->terbilang(number_format($this->data->gd_rekon + $pph_amd_rekon, 0, '', '') ) . ' Rupiah' ) );
                $spreadSheet->getSheet(0)->getCell('G10')->SetValue(ucwords($this->terbilang(number_format($this->data->gd_rekon + $pph_faktur_rekon, 0, '', '') ) . ' Rupiah' ) );

                $spreadSheet->getSheet(0)->getCell('G13')->SetValue(number_format($this->data->gd_rekon + $pph_gd_rekon) );
                $spreadSheet->getSheet(0)->getCell('G14')->SetValue(number_format($this->data->gd_rekon + $pph_sp_rekon) );
                $spreadSheet->getSheet(0)->getCell('G15')->SetValue(number_format($this->data->gd_rekon + $pph_amd_rekon) );
                $spreadSheet->getSheet(0)->getCell('G16')->SetValue(number_format($this->data->gd_rekon + $pph_faktur_rekon) );

                $spreadSheet->getSheet(0)->getCell('A2')->SetValue($this->data2->bank);
                $spreadSheet->getSheet(0)->getCell('A3')->SetValue($this->data2->cabang_bank);
                $spreadSheet->getSheet(0)->getCell('B3')->SetValue($this->data2->NPWP);
                $spreadSheet->getSheet(0)->getCell('A4')->SetValue($this->data2->rek);
                $spreadSheet->getSheet(0)->getCell('A5')->SetValue($this->data2->alamat_company);
                $spreadSheet->getSheet(0)->getCell('A6')->SetValue($this->data2->wakil_mitra);
                $spreadSheet->getSheet(0)->getCell('A7')->SetValue($this->data2->jabatan_mitra);
                $spreadSheet->getSheet(0)->getCell('A8')->SetValue($this->data->spp_num);

                $get_lok = DB::table('procurement_area')->where('witel', $this->data->witel)->first();

                $spreadSheet->getSheet(0)->getCell('A9')->SetValue(ucwords($get_lok->area) );
                $spreadSheet->getSheet(0)->getCell('B9')->SetValue($this->data->tgl_faktur);
                $spreadSheet->getSheet(0)->getCell('C9')->SetValue($this->convert_month($this->data->tgl_faktur) );
                $spreadSheet->getSheet(0)->getCell('A10')->SetValue($this->data->judul);
                $spreadSheet->getSheet(0)->getCell('B10')->SetValue($this->data->lokasi_pekerjaan);
                $spreadSheet->getSheet(0)->getCell('A11')->SetValue($this->data->pks);
                $spreadSheet->getSheet(0)->getCell('B11')->SetValue($this->data->no_sp);
                $spreadSheet->getSheet(0)->getCell('A12')->SetValue($this->data->tgl_pks);
                $spreadSheet->getSheet(0)->getCell('B12')->SetValue($this->convert_month( ($this->data->tgl_pks) ) );
                $spreadSheet->getSheet(0)->getCell('A13')->SetValue($this->data->tgl_sp);
                $spreadSheet->getSheet(0)->getCell('B13')->SetValue($this->convert_month($this->data->tgl_sp) );
                $spreadSheet->getSheet(0)->getCell('A14')->SetValue($this->data->total_material_sp);
                $spreadSheet->getSheet(0)->getCell('B14')->SetValue($this->data->total_jasa_sp);
                $spreadSheet->getSheet(0)->getCell('A15')->SetValue($this->data->gd_sp);
                $spreadSheet->getSheet(0)->getCell('B15')->SetValue($grand_total_sp);
                $spreadSheet->getSheet(0)->getCell('E15')->SetValue($pph_gd_rekon);
                $spreadSheet->getSheet(0)->getCell('C15')->SetValue(ucwords($this->terbilang(number_format($this->data->gd_sp, 0, '', '') ) .' rupiah') );
                $spreadSheet->getSheet(0)->getCell('D15')->SetValue(ucwords($this->terbilang(number_format($this->data->gd_sp + $grand_total_sp, '0', '', '') ) .' rupiah') );
                $spreadSheet->getSheet(0)->getCell('A16')->SetValue($this->data->receipt_num);
                $spreadSheet->getSheet(0)->getCell('A17')->SetValue($this->data->surat_penetapan);
                $spreadSheet->getSheet(0)->getCell('A18')->SetValue($this->data->tgl_s_pen);
                $spreadSheet->getSheet(0)->getCell('B18')->SetValue($this->convert_month($this->data->tgl_s_pen) );
                $spreadSheet->getSheet(0)->getCell('A19')->SetValue((date('d-m-Y', strtotime('+'. ($this->data->tgl_jatuh_tmp - 1). ' day', strtotime($this->data->tgl_sp) ) ) ) );
                $spreadSheet->getSheet(0)->getCell('B19')->SetValue($this->convert_month( (date('d-m-Y', strtotime('+'. ($this->data->tgl_jatuh_tmp - 1). ' day', strtotime($this->data->tgl_sp ?? $this->data->tgl_s_pen) ) ) ) ) );

                $nilai_ttd = $this->data->gd_sp;

                if ($nilai_ttd >= 0 && $nilai_ttd <= 200000000)
				{
					$ttd = 'krglbh_dua_jt';
				} elseif ($nilai_ttd > 200000000 && $nilai_ttd <= 500000000) {
					$ttd = 'antr_dualima_jt';
				} else {
					$ttd = 'lbh_lima_jt';
				}

				$data_ttd = ReportModel::get_dokumen_id($ttd, $this->data->pekerjaan, $this->data->witel, $this->data->tgl_sp);

                $spreadSheet->getSheet(0)->getCell('A20')->SetValue($data_ttd->user);
                $spreadSheet->getSheet(0)->getCell('A21')->SetValue($data_ttd->jabatan);
                $spreadSheet->getSheet(0)->getCell('B21')->SetValue($data_ttd->nik);
                $spreadSheet->getSheet(0)->getCell('A22')->SetValue($this->data->surat_kesanggupan);
                $spreadSheet->getSheet(0)->getCell('B22')->SetValue($this->data->tgl_surat_sanggup);
                $spreadSheet->getSheet(0)->getCell('C22')->SetValue($this->convert_month($this->data->tgl_surat_sanggup) );
                $spreadSheet->getSheet(0)->getCell('A23')->SetValue($this->data->ba_rekon);
                $spreadSheet->getSheet(0)->getCell('B23')->SetValue($this->data->tgl_ba_rekon);
                $spreadSheet->getSheet(0)->getCell('C23')->SetValue($this->convert_month($this->data->tgl_ba_rekon) );
                $spreadSheet->getSheet(0)->getCell('A24')->SetValue($this->data->total_material_rekon);
                $spreadSheet->getSheet(0)->getCell('B24')->SetValue($this->data->total_jasa_rekon);
                $spreadSheet->getSheet(0)->getCell('A25')->SetValue($this->data->gd_rekon);
                $spreadSheet->getSheet(0)->getCell('B25')->SetValue($pph_gd_rekon);
                $spreadSheet->getSheet(0)->getCell('C25')->SetValue(ucwords($this->terbilang(number_format($this->data->gd_rekon, 0, '', '') ) .' rupiah') );
                $spreadSheet->getSheet(0)->getCell('D25')->SetValue(ucwords($this->terbilang(number_format($this->data->gd_rekon + $pph_gd_rekon, '0', '', '') ) .' rupiah') );
                $spreadSheet->getSheet(0)->getCell('A26')->SetValue($this->data->BAST);
                $spreadSheet->getSheet(0)->getCell('B26')->SetValue($this->data->tgl_bast);
                $spreadSheet->getSheet(0)->getCell('C26')->SetValue($this->convert_month($this->data->tgl_bast) );
                $spreadSheet->getSheet(0)->getCell('A27')->SetValue("   Hari ini ". ucwords($this->only_day(date('Y-m-d', strtotime($this->data->tgl_bast) ) ) ) ." tanggal ". ucwords($this->terbilang(date('d', strtotime($this->data->tgl_bast) ) ) ) ." bulan ". ucwords($bulan_angk[date('m', strtotime($this->data->tgl_bast) )] ) ." Tahun ". ucwords($this->terbilang(date('Y', strtotime($this->data->tgl_bast) ) ) ) .", (". date('d/m/Y', strtotime($this->data->tgl_bast) ) .")");
                $spreadSheet->getSheet(0)->getCell('A28')->SetValue($this->data->BAUT);
                $spreadSheet->getSheet(0)->getCell('B28')->SetValue($this->data->tgl_baut);
                $spreadSheet->getSheet(0)->getCell('C28')->SetValue($this->convert_month($this->data->tgl_baut) );

                $data_pu = ReportModel::get_dokumen_id('sm_krj', $this->data->pekerjaan, $this->data->witel, $this->data->tgl_baut);
                $spreadSheet->getSheet(0)->getCell('A29')->SetValue($data_pu->user);
                $spreadSheet->getSheet(0)->getCell('B29')->SetValue($data_pu->jabatan);

                $data_pu = ReportModel::get_dokumen_id('sm_krj', $this->data->pekerjaan, $this->data->witel, $this->data->tgl_ba_abd);
                $spreadSheet->getSheet(0)->getCell('C29')->SetValue($data_pu->user);
                $spreadSheet->getSheet(0)->getCell('D29')->SetValue($data_pu->jabatan);

                $data_pu = ReportModel::get_dokumen_id('sm_krj', $this->data->pekerjaan, $this->data->witel, $this->data->tgl_ba_rekon);
                $spreadSheet->getSheet(0)->getCell('E29')->SetValue($data_pu->user);
                $spreadSheet->getSheet(0)->getCell('F29')->SetValue($data_pu->jabatan);

                $data_pu = ReportModel::get_dokumen_id('mngr_krj', $this->data->pekerjaan, $this->data->witel, $this->data->tgl_baut);
                $spreadSheet->getSheet(0)->getCell('A30')->SetValue($data_pu->user);
                $spreadSheet->getSheet(0)->getCell('B30')->SetValue($data_pu->jabatan);

                $data_pu = ReportModel::get_dokumen_id('mngr_krj', $this->data->pekerjaan, $this->data->witel, $this->data->tgl_ba_abd);
                $spreadSheet->getSheet(0)->getCell('C30')->SetValue($data_pu->user);
                $spreadSheet->getSheet(0)->getCell('D30')->SetValue($data_pu->jabatan);

                $data_pu = ReportModel::get_dokumen_id('mngr_krj', $this->data->pekerjaan, $this->data->witel, $this->data->tgl_ba_rekon);
                $spreadSheet->getSheet(0)->getCell('E30')->SetValue($data_pu->user);
                $spreadSheet->getSheet(0)->getCell('F30')->SetValue($data_pu->jabatan);

                $data_pu = ReportModel::get_dokumen_id('mngr_krj', $this->data->pekerjaan, $this->data->witel, $this->data->tgl_s_pen);
                $spreadSheet->getSheet(0)->getCell('G30')->SetValue($data_pu->user);
                $spreadSheet->getSheet(0)->getCell('H30')->SetValue($data_pu->jabatan);

                $data_pu = ReportModel::get_dokumen_id('gm', $this->data->pekerjaan, $this->data->witel, $this->data->tgl_ba_rekon);
                $spreadSheet->getSheet(0)->getCell('A31')->SetValue($data_pu->user);
                $spreadSheet->getSheet(0)->getCell('B31')->SetValue($data_pu->jabatan);
                $spreadSheet->getSheet(0)->getCell('C31')->SetValue($data_pu->nik);

                $data_pu = ReportModel::get_dokumen_id('gm', $this->data->pekerjaan, $this->data->witel, 'latest');
                $spreadSheet->getSheet(0)->getCell('D31')->SetValue($data_pu->user);
                $spreadSheet->getSheet(0)->getCell('E31')->SetValue($data_pu->jabatan);
                $spreadSheet->getSheet(0)->getCell('F31')->SetValue($data_pu->nik);

                $spreadSheet->getSheet(0)->getCell('A32')->SetValue($this->data->ba_abd);
                $spreadSheet->getSheet(0)->getCell('B32')->SetValue($this->data->tgl_ba_abd);
                $spreadSheet->getSheet(0)->getCell('C32')->SetValue($this->convert_month($this->data->tgl_ba_abd) );
                $spreadSheet->getSheet(0)->getCell('A33')->SetValue($this->data->ba_rekon);
                $spreadSheet->getSheet(0)->getCell('B33')->SetValue($this->data->tgl_ba_rekon);
                $spreadSheet->getSheet(0)->getCell('C33')->SetValue($this->convert_month($this->data->tgl_ba_rekon) );

                $spreadSheet->getSheet(0)->getCell('A34')->SetValue("Pada hari ini ". ucwords($this->only_day( (date('d-m-Y', strtotime('+'. ($this->data->tgl_jatuh_tmp - 1). ' day', strtotime($this->data->tgl_sp) ) ) ) ) ) ." tanggal ". ucwords($this->terbilang(date('d', strtotime('+'. ($this->data->tgl_jatuh_tmp - 1). ' day', strtotime($this->data->tgl_sp) ) ) ) ) ." bulan ". ucwords($bulan_angk[date('m', strtotime('+'. ($this->data->tgl_jatuh_tmp - 1). ' day', strtotime($this->data->tgl_sp) ) )])  ." Tahun ". ucwords($this->terbilang(date('Y', strtotime('+'. ($this->data->tgl_jatuh_tmp - 1). ' day', strtotime($this->data->tgl_sp) ) ) ) ) .", (". $this->convert_month(date('d-m-Y', strtotime('+'. ($this->data->tgl_jatuh_tmp - 1). ' day', strtotime($this->data->tgl_sp) ) ) ) .")");

                $spreadSheet->getSheet(0)->getCell('A35')->SetValue($this->data->amd_sp );
                $spreadSheet->getSheet(0)->getCell('A36')->SetValue(number_format($this->data->total_material_rekon, 0, '.', '.') );
                $spreadSheet->getSheet(0)->getCell('A37')->SetValue(number_format($this->data->total_jasa_rekon, 0, '.', '.') );
                $spreadSheet->getSheet(0)->getCell('A38')->SetValue(number_format($this->data->gd_rekon, 0, '.', '.') );
                $spreadSheet->getSheet(0)->getCell('A39')->SetValue(number_format($pph_gd_rekon, 0, '.', '.') );
                $spreadSheet->getSheet(0)->getCell('A40')->SetValue(number_format($this->data->total_material_sp, 0, '.', '.') );
                $spreadSheet->getSheet(0)->getCell('A41')->SetValue(number_format($this->data->total_jasa_sp, 0, '.', '.') );
                $spreadSheet->getSheet(0)->getCell('A42')->SetValue(number_format($this->data->gd_sp, 0, '.', '.') );
                $spreadSheet->getSheet(0)->getCell('A43')->SetValue(number_format($grand_total_sp, 0, '.', '.') );
                $spreadSheet->getSheet(0)->getCell('A44')->SetValue($this->data->invoice );
                $spreadSheet->getSheet(0)->getCell('A46')->SetValue(number_format($this->data->gd_rekon + $pph_gd_rekon, 0, '.', '.') );

                $spreadSheet->getSheet(0)->getCell('A47')->SetValue("Pada hari ini ". ucwords($this->only_day( (date('d-m-Y',  strtotime($this->data->tgl_baut) ) ) ) ) ." tanggal ". ucwords($this->terbilang(date('d',  strtotime($this->data->tgl_baut) ) ) ) ." bulan ". ucwords($bulan_angk[date('m',  strtotime($this->data->tgl_baut) ) ]) ." Tahun ". ucwords($this->terbilang(date('Y',  strtotime($this->data->tgl_baut) ) ) ) .", (". $this->convert_month(date('d-m-Y',  strtotime($this->data->tgl_baut) ) )  .")");

                $spreadSheet->getSheet(0)->getCell('A35')->SetValue($this->data->amd_sp );
                $spreadSheet->getSheet(0)->getCell('A48')->SetValue($this->data2->telp );

                $spreadSheet->getSheet(0)->getCell('A49')->SetValue("Pada hari ini ". ucwords($this->only_day( (date('d-m-Y', strtotime('- 1 day', strtotime($this->data->tgl_s_pen) ) ) ) ) ) ." tanggal ". ucwords($this->terbilang(date('d', strtotime('- 1 day', strtotime($this->data->tgl_s_pen) ) ) ) ) ." bulan ". ucwords($bulan_angk[date('m', strtotime('- 1 day', strtotime($this->data->tgl_s_pen) ) )] ) ." Tahun ". ucwords($this->terbilang(date('Y', strtotime('- 1 day', strtotime($this->data->tgl_s_pen) ) ) ) ) .", (". date('d/m/Y', strtotime('- 1 day', strtotime($this->data->tgl_s_pen) ) ) .")" );
                $spreadSheet->getSheet(0)->getCell('B49')->SetValue("Pada hari ini ". ucwords($this->only_day( (date('d-m-Y', strtotime($this->data->tgl_s_pen) ) ) ) ) ." tanggal ". ucwords($this->terbilang(date('d', strtotime($this->data->tgl_s_pen) ) ) ) ." bulan ". ucwords($bulan_angk[date('m', strtotime($this->data->tgl_s_pen) )] ) ." Tahun ". ucwords($this->terbilang(date('Y', strtotime($this->data->tgl_s_pen) ) ) ) .", (". date('d/m/Y', strtotime($this->data->tgl_s_pen) ) .")" );
                $spreadSheet->getSheet(0)->getCell('A50')->SetValue(ucwords($get_lok->witel) );
                $spreadSheet->getSheet(0)->getCell('A51')->SetValue(ucwords($this->data2->lokasi_comp) );
                $spreadSheet->getSheet(0)->getCell('A52')->SetValue(ucwords($this->data->jenis_kontrak) );
                $spreadSheet->getSheet(0)->getCell('B52')->SetValue(ucwords(($this->data->jenis_kontrak == 'Kontrak Putus' ? 'kontrak' : 'surat pesanan')) );
                $spreadSheet->getSheet(0)->getCell('A53')->SetValue($this->data->id_project);

                $nik_pab = ReportModel::get_dokumen_id('osm', $this->data->pekerjaan, $this->data->witel, 'latest');
                $spreadSheet->getSheet(0)->getCell('A54')->SetValue($nik_pab->user);
                $spreadSheet->getSheet(0)->getCell('B54')->SetValue($nik_pab->jabatan);
                $spreadSheet->getSheet(0)->getCell('C54')->SetValue($nik_pab->nik);

                $nik_pam = ReportModel::get_dokumen_id('m_fin', $this->data->pekerjaan, $this->data->witel, 'latest');
                $spreadSheet->getSheet(0)->getCell('A55')->SetValue($nik_pam->user);
                $spreadSheet->getSheet(0)->getCell('B55')->SetValue($nik_pam->jabatan);
                $spreadSheet->getSheet(0)->getCell('C55')->SetValue($nik_pam->nik);

                $spreadSheet->getSheet(0)->getCell('A56')->SetValue($this->data2->pph);

                $pid = DB::Table('procurement_req_pid As prp')
                ->leftjoin('procurement_pid As pp', 'pp.id', '=', 'prp.id_pid_req')
                ->select('pp.pid')
                ->where('id_upload', $this->data->id)
                ->get();

                foreach($pid as $v)
                {
                    $pid_all[$v->pid] = $v->pid;
                }
                // dd(implode(', ', $pid_all) );
                $spreadSheet->getSheet(0)->getCell('A57')->SetValue(implode(', ', $pid_all) );

                $khs = AdminModel::find_work($this->data->pekerjaan);
                $spreadSheet->getSheet(0)->getCell('A58')->SetValue($khs->uraian);

                $nilai_ttd = $this->data->gd_rekon;

                if ($nilai_ttd >= 0 && $nilai_ttd <= 200000000)
				{
					$ttd = 'krglbh_dua_jt';
				} elseif ($nilai_ttd > 200000000 && $nilai_ttd <= 500000000) {
					$ttd = 'antr_dualima_jt';
				} else {
					$ttd = 'lbh_lima_jt';
				}

				$data_ttd = ReportModel::get_dokumen_id($ttd, $this->data->pekerjaan, $this->data->witel, $this->data->tgl_ba_rekon);

                $spreadSheet->getSheet(0)->getCell('A59')->SetValue($data_ttd->user);
                $spreadSheet->getSheet(0)->getCell('A60')->SetValue($data_ttd->jabatan);
                $spreadSheet->getSheet(0)->getCell('B60')->SetValue($data_ttd->nik);

				$data_ttd = ReportModel::get_dokumen_id($ttd, $this->data->pekerjaan, $this->data->witel, 'latest');

                $spreadSheet->getSheet(0)->getCell('C59')->SetValue($data_ttd->user);
                $spreadSheet->getSheet(0)->getCell('C60')->SetValue($data_ttd->jabatan);
                $spreadSheet->getSheet(0)->getCell('D60')->SetValue($data_ttd->nik);

				$data_ttd = ReportModel::get_dokumen_id($ttd, $this->data->pekerjaan, $this->data->witel, $this->data->tgl_amd_sp);

                $spreadSheet->getSheet(0)->getCell('A61')->SetValue(@$data_ttd->user);
                $spreadSheet->getSheet(0)->getCell('A62')->SetValue(@$data_ttd->jabatan);
                $spreadSheet->getSheet(0)->getCell('B62')->SetValue(@$data_ttd->nik);
                $spreadSheet->getSheet(0)->getCell('A63')->SetValue("Pada hari ini ". ucwords($this->only_day( (date('d-m-Y', strtotime($this->data->tgl_amd_sp) ) ) ) ) ." tanggal ". ucwords($this->terbilang(date('d', strtotime($this->data->tgl_amd_sp) ) ) ) ." bulan ". ucwords($bulan_angk[date('m', strtotime($this->data->tgl_amd_sp) )] ) ." Tahun ". ucwords($this->terbilang(date('Y', strtotime($this->data->tgl_amd_sp) ) ) ) .", (". date('d/m/Y', strtotime($this->data->tgl_amd_sp) ) .")" );

                $data_boq_sp_raw = DB::SELECT("SELECT pbu.jenis_work, pbl.sub_jenis_p, pbd.*, pd.uraian, pd.satuan, pbd.material as material_default, pd.design_mitra_id, pbd.jasa as jasa_default, pbl.sto, pbl.lokasi, pbl.pid_sto
                FROM procurement_boq_upload pbu
                LEFT JOIN procurement_Boq_lokasi pbl ON pbu.id = pbl.id_upload
                LEFT JOIN procurement_Boq_design pbd ON pbl.id = pbd.id_boq_lokasi
                LEFT JOIN procurement_designator pd ON pd.id = pbd.id_design
                LEFT JOIN procurement_mitra pm ON pm.id = pbu.mitra_id
                WHERE pbu.id = ".$this->data->id." AND pd.active = 1 AND pbu.active = 1 AND pbd.status_design = 0 GROUP BY pbd.id");

                $order_sto = explode(', ', $this->data->sto_pekerjaan);
                $data_boq_sp = [];

                // if($data_boq_sp_raw)
                // {
                //     $get_jenis_kerja = DB::table('procurement_pekerjaan')->where('jenis', $data_boq_sp_raw[0]->jenis_work)->get();

                //     foreach($get_jenis_kerja as $gjk)
                //     {
                //         foreach($order_sto as $v)
                //         {
                //             $find_k = array_keys(array_column(json_decode(json_encode($data_boq_sp_raw), 'TRUE'), 'sto'), $v);

                //             foreach($find_k as $vv)
                //             {
                //                 $dbr = $data_boq_sp_raw[$vv];

                //                 if($gjk->sub_jenis == $dbr->sub_jenis_p)
                //                 {
                //                     $data_boq_sp[] = $dbr;
                //                 }
                //             }
                //         }
                //     }
                // }
                $data_boq_sp = $data_boq_sp_raw;
                $data_bq = $data_tot = [];

                $get_all_material = [];

                foreach($data_boq_sp as $key => $val)
                {
                    $get_all_material[$val->id_design .'-'. intval($val->material) .'-'. intval($val->jasa)]['id_design']  = $val->id_design;
                    $get_all_material[$val->id_design .'-'. intval($val->material) .'-'. intval($val->jasa)]['jenis_khs']  = $val->design_mitra_id;
                    $get_all_material[$val->id_design .'-'. intval($val->material) .'-'. intval($val->jasa)]['designator'] = $val->designator;
                    $get_all_material[$val->id_design .'-'. intval($val->material) .'-'. intval($val->jasa)]['uraian']     = $val->uraian;
                    $get_all_material[$val->id_design .'-'. intval($val->material) .'-'. intval($val->jasa)]['satuan']     = $val->satuan;
                    $get_all_material[$val->id_design .'-'. intval($val->material) .'-'. intval($val->jasa)]['material']   = intval($val->material);
                    $get_all_material[$val->id_design .'-'. intval($val->material) .'-'. intval($val->jasa)]['jasa']       = intval($val->jasa);
                    $get_all_material[$val->id_design .'-'. intval($val->material) .'-'. intval($val->jasa)]['sp']         = 0;
                    $get_all_material[$val->id_design .'-'. intval($val->material) .'-'. intval($val->jasa)]['rekon']      = 0;
                    $get_all_material[$val->id_design .'-'. intval($val->material) .'-'. intval($val->jasa)]['tambah']     = 0;
                    $get_all_material[$val->id_design .'-'. intval($val->material) .'-'. intval($val->jasa)]['kurang']     = 0;
                }

                foreach($data_boq_sp as $key => $val)
                {
                    if(!isset($data_tot['sp']['total']) )
                    {
                        $data_tot['sp']['total']['material_sp']    = 0;
                        $data_tot['sp']['total']['jasa_sp']        = 0;
                        $data_tot['sp']['total']['material_rekon'] = 0;
                        $data_tot['sp']['total']['jasa_rekon']     = 0;
                    }

                    $data_tot['sp']['total']['material_sp']    += intval($val->material) * intval($val->sp);
                    $data_tot['sp']['total']['jasa_sp']        += intval($val->jasa) * intval($val->sp);
                    $data_tot['sp']['total']['material_rekon'] += intval($val->material) * intval($val->rekon);
                    $data_tot['sp']['total']['jasa_rekon']     += intval($val->jasa) * intval($val->rekon);

                    $data_bq['sp'][$val->id_boq_lokasi]['lokasi']      = $val->lokasi;
                    $data_bq['sp'][$val->id_boq_lokasi]['sto']         = $val->sto;
                    $data_bq['sp'][$val->id_boq_lokasi]['pid_sto_wbs'] = $val->pid_sto;

                    $split_pid_all = explode(', ', $val->pid_sto);
                    $split_pid = explode('-', $split_pid_all[0]);
                    // dd($split_pid);
                    $data_bq['sp'][$val->id_boq_lokasi]['pid_sto_lok'] = @$split_pid[0] .'-'. @$split_pid[1];

                    $pid_lok = DB::Table('procurement_req_pid As prp')
                    ->leftjoin('procurement_pid As pp', 'pp.id', '=', 'prp.id_pid_req')
                    ->select('pp.pid')
                    ->where('id_lokasi', $val->id_boq_lokasi)
                    ->get();

                    foreach($pid_lok as $v)
                    {
                        $pid_per_lok[$v->pid] = $v->pid;
                    }

                    $data_bq['sp'][$val->id_boq_lokasi]['pid_sto'] = implode(', ', $pid_per_lok);

                    if(!isset($data_bq['sp'][$val->id_boq_lokasi]['list']) )
                    {
                        $data_bq['sp'][$val->id_boq_lokasi]['list']  = $get_all_material;
                    }

                    if($data_bq['sp'][$val->id_boq_lokasi]['list'][$val->id_design .'-'. intval($val->material) .'-'. intval($val->jasa)])
                    {
                        $data_bq['sp'][$val->id_boq_lokasi]['list'][$val->id_design .'-'. intval($val->material) .'-'. intval($val->jasa)]['sp']     = intval($val->sp);
                        $data_bq['sp'][$val->id_boq_lokasi]['list'][$val->id_design .'-'. intval($val->material) .'-'. intval($val->jasa)]['rekon']  = intval($val->rekon);
                        $data_bq['sp'][$val->id_boq_lokasi]['list'][$val->id_design .'-'. intval($val->material) .'-'. intval($val->jasa)]['tambah'] = intval($val->tambah);
                        $data_bq['sp'][$val->id_boq_lokasi]['list'][$val->id_design .'-'. intval($val->material) .'-'. intval($val->jasa)]['kurang'] = intval($val->kurang);
                    }
                }

                foreach($data_bq['sp'] as $k => $v)
                {
                    foreach($v['list'] as $kk => $vv)
                    {
                        $data_bq['sp'][$k]['list'] = array_values($data_bq['sp'][$k]['list']);
                    }
                }

                $data_boq_rekon_raw = DB::SELECT("SELECT pbu.jenis_work, pbl.sub_jenis_p, pbd.*, pd.uraian, pd.satuan, pbd.material as material_default, pd.design_mitra_id, pbd.jasa as jasa_default, pbl.sto, pbl.lokasi, pbl.pid_sto
                FROM procurement_boq_upload pbu
                LEFT JOIN procurement_Boq_lokasi pbl ON pbu.id = pbl.id_upload
                LEFT JOIN procurement_Boq_design pbd ON pbl.id = pbd.id_boq_lokasi
                LEFT JOIN procurement_designator pd ON pd.id = pbd.id_design
                LEFT JOIN procurement_mitra pm ON pm.id = pbu.mitra_id
                WHERE pbu.id = ".$this->data->id." AND pd.active = 1 AND pbu.active = 1 AND pbd.status_design = 1 GROUP BY pbd.id");

                $data_boq_rekon_rw = array_merge($data_boq_sp, $data_boq_rekon_raw);

                $order_sto = explode(', ', $this->data->sto_pekerjaan);
                $data_boq_rekon = [];

                if($data_boq_rekon_rw)
                {
                    $get_jenis_kerja = DB::table('procurement_pekerjaan')->where('jenis', $data_boq_rekon_rw[0]->jenis_work)->get();

                    foreach($get_jenis_kerja as $gjk)
                    {
                        foreach($order_sto as $v)
                        {
                            $find_k = array_keys(array_column(json_decode(json_encode($data_boq_rekon_rw), 'TRUE'), 'sto'), $v);

                            foreach($find_k as $vv)
                            {
                                $dbr = $data_boq_rekon_rw[$vv];

                                if($gjk->sub_jenis == $dbr->sub_jenis_p)
                                {
                                    $data_boq_rekon[] = $dbr;
                                }
                            }
                        }
                    }
                }
                $data_boq_rekon = $data_boq_rekon_rw;
                $get_all_material = [];

                foreach($data_boq_rekon as $key => $val)
                {
                    $get_all_material[$val->id_design .'-'. intval($val->material) .'-'. intval($val->jasa)]['id_design']  = $val->id_design;
                    $get_all_material[$val->id_design .'-'. intval($val->material) .'-'. intval($val->jasa)]['jenis_khs']  = $val->design_mitra_id;
                    $get_all_material[$val->id_design .'-'. intval($val->material) .'-'. intval($val->jasa)]['designator'] = $val->designator;
                    $get_all_material[$val->id_design .'-'. intval($val->material) .'-'. intval($val->jasa)]['uraian']     = $val->uraian;
                    $get_all_material[$val->id_design .'-'. intval($val->material) .'-'. intval($val->jasa)]['satuan']     = $val->satuan;
                    $get_all_material[$val->id_design .'-'. intval($val->material) .'-'. intval($val->jasa)]['material']   = intval($val->material);
                    $get_all_material[$val->id_design .'-'. intval($val->material) .'-'. intval($val->jasa)]['jasa']       = intval($val->jasa);
                    $get_all_material[$val->id_design .'-'. intval($val->material) .'-'. intval($val->jasa)]['sp']         = 0;
                    $get_all_material[$val->id_design .'-'. intval($val->material) .'-'. intval($val->jasa)]['rekon']      = 0;
                    $get_all_material[$val->id_design .'-'. intval($val->material) .'-'. intval($val->jasa)]['tambah']     = 0;
                    $get_all_material[$val->id_design .'-'. intval($val->material) .'-'. intval($val->jasa)]['kurang']     = 0;
                }

                foreach($data_boq_rekon as $key => $val)
                {
                    $data_bq['rekon'][$val->id_boq_lokasi]['lokasi'] = $val->lokasi;
                    $data_bq['rekon'][$val->id_boq_lokasi]['sto']    = $val->sto;

                    $split_pid_all = explode(', ', $val->pid_sto);

                    $result_pid_lok = [];
                    foreach($split_pid_all as $spa)
                    {
                        $split_pid = explode('-', $spa);
                        $result_pid_lok[@$split_pid[0] .'-'. @$split_pid[1]] = @$split_pid[0] .'-'. @$split_pid[1];
                    }

                    $data_bq['rekon'][$val->id_boq_lokasi]['pid_sto_lok'] = implode(', ', $result_pid_lok);

                    $data_bq['rekon'][$val->id_boq_lokasi]['pid_sto_wbs'] = $val->pid_sto;

                    $pid_lok = DB::Table('procurement_req_pid As prp')
                    ->leftjoin('procurement_pid As pp', 'pp.id', '=', 'prp.id_pid_req')
                    ->select('pp.pid')
                    ->where('id_lokasi', $val->id_boq_lokasi)
                    ->get();

                    foreach($pid_lok as $v)
                    {
                        $pid_per_lok[$v->pid] = $v->pid;
                    }

                    $data_bq['rekon'][$val->id_boq_lokasi]['pid_sto'] = implode(', ', $pid_per_lok);

                    if(!isset($data_bq['rekon'][$val->id_boq_lokasi]['list']) )
                    {
                        $data_bq['rekon'][$val->id_boq_lokasi]['list']  = $get_all_material;
                    }

                    if($data_bq['rekon'][$val->id_boq_lokasi]['list'][$val->id_design .'-'. intval($val->material) .'-'. intval($val->jasa)])
                    {
                        $data_bq['rekon'][$val->id_boq_lokasi]['list'][$val->id_design .'-'. intval($val->material) .'-'. intval($val->jasa)]['sp']     = intval($val->sp);
                        $data_bq['rekon'][$val->id_boq_lokasi]['list'][$val->id_design .'-'. intval($val->material) .'-'. intval($val->jasa)]['rekon']  = intval($val->rekon);
                        $data_bq['rekon'][$val->id_boq_lokasi]['list'][$val->id_design .'-'. intval($val->material) .'-'. intval($val->jasa)]['tambah'] = intval($val->rekon) > intval($val->sp) ? intval($val->rekon) - intval($val->sp) : 0;
                        $data_bq['rekon'][$val->id_boq_lokasi]['list'][$val->id_design .'-'. intval($val->material) .'-'. intval($val->jasa)]['kurang'] = intval($val->rekon) < intval($val->sp) ? intval($val->sp) - intval($val->rekon) : 0;
                    }
                }

                if(@$data_bq['rekon'])
                {
                    foreach($data_bq['rekon'] as $k => $v)
                    {
                        foreach($v['list'] as $kk => $vv)
                        {
                            $data_bq['rekon'][$k]['list'] = array_values($data_bq['rekon'][$k]['list']);
                        }
                    }

                    foreach($data_bq['rekon'] as $k => $v)
                    {
                        foreach($v['list'] as $kk => $vv)
                        {
                            if(!isset($data_tot['rekon']['total']['material_sp']) )
                            {
                                $data_tot['rekon']['total']['material_sp']     = 0;
                                $data_tot['rekon']['total']['jasa_sp']         = 0;
                                $data_tot['rekon']['total']['material_rekon']  = 0;
                                $data_tot['rekon']['total']['jasa_rekon']      = 0;
                                $data_tot['rekon']['total']['material_tambah'] = 0;
                                $data_tot['rekon']['total']['jasa_tambah']     = 0;
                                $data_tot['rekon']['total']['material_kurang'] = 0;
                                $data_tot['rekon']['total']['jasa_kurang']     = 0;
                            }

                            $data_tot['rekon']['total']['material_sp']     += intval($vv['material']) * intval($vv['sp']);
                            $data_tot['rekon']['total']['jasa_sp']         += intval($vv['jasa']) * intval($vv['sp']);
                            $data_tot['rekon']['total']['material_rekon']  += intval($vv['material']) * intval($vv['rekon']);
                            $data_tot['rekon']['total']['jasa_rekon']      += intval($vv['jasa']) * intval($vv['rekon']);
                            $data_tot['rekon']['total']['material_tambah'] += intval($vv['material']) * intval($vv['tambah']);
                            $data_tot['rekon']['total']['jasa_tambah']     += intval($vv['jasa']) * intval($vv['tambah']);
                            $data_tot['rekon']['total']['material_kurang'] += intval($vv['material']) * intval($vv['kurang']);
                            $data_tot['rekon']['total']['jasa_kurang']     += intval($vv['jasa']) * intval($vv['kurang']);
                        }
                    }
                }

                if(!empty($data_tot['rekon']) )
                {
                    $spreadSheet->getSheet(0)->getCell('C24')->SetValue($data_tot['rekon']['total']['material_tambah']);
                    $spreadSheet->getSheet(0)->getCell('D24')->SetValue($data_tot['rekon']['total']['jasa_tambah']);
                    $spreadSheet->getSheet(0)->getCell('E24')->SetValue($data_tot['rekon']['total']['material_kurang']);
                    $spreadSheet->getSheet(0)->getCell('F24')->SetValue($data_tot['rekon']['total']['jasa_kurang']);
                }

                $sheet =[
                    5  => 'SP',
                    6  => 'LAMPIRAN SP',
                    9  => 'BAUT',
                    10 => 'BA ABD',
                    11 => 'BA REKONSILIASI',
                    12 => 'Rekap BA Rekon',
                    13 => 'BOQ',
                    14 => 'Lamp BA Rekon',
                    16 => 'BAKH SP',
                    15 => 'BAKH REKON',
                ];

                for ($i = 'G'; $i !== 'ZZ'; $i++){
                    $index[] = $i;
                }

                $spreadSheet->getSheet(1)->getStyle('A6')->getAlignment()->setWrapText(true);
                $spreadSheet->getSheet(1)->getRowDimension((6) )->setRowHeight(30);

                $data_bq['sp'] = array_values($data_bq['sp']);
                $data_bq['rekon'] = @array_values($data_bq['rekon']);

                foreach($sheet as $title => $val)
                {
                    $k_parent = $spreadSheet->getIndex(
                        $spreadSheet->getSheetByName($val)
                    );

                    if($val == 'SP')
                    {
                        $num5 = 0;
                        $no_lok = 0;

                        foreach($this->datasl as $kk => $v)
                        {
                            $no_lok = $no_lok++;
                            $num5 += 1;

                            $spreadSheet->getSheet($k_parent)->insertNewRowBefore( (22 + $num5 + $no_lok), 1);
                            $spreadSheet->getSheet($k_parent)->getCell('A'. (22 + $num5) )->setValue($v['lokasi']);
                            $spreadSheet->getSheet($k_parent)->getCell('H'. (22 + $num5) )->setValue($v['gd_sp']);

                            $spreadSheet->getSheet($k_parent)->mergeCells('A'. (22 + $num5) . ':'. 'G'. (22 + $num5) );
                            $spreadSheet->getSheet($k_parent)->mergeCells('H'. (22 + $num5) . ':'. 'I'. (22 + $num5) );
                            $spreadSheet->getSheet($k_parent)->mergeCells('J'. (22 + $num5) . ':'. 'L'. (25 + $num5) );
                        }
                    }

                    if($val == 'BAUT')
                    {
                        $num6 = 0;
                        $no_lok = 0;
                        foreach($this->datasl as $kk => $v)
                        {
                            $no_lok = $no_lok++;
                            $num6 += 1;
                            // dd($no_lok);
                            $spreadSheet->getSheet($k_parent)->insertNewRowBefore( (15 + $num6 + $no_lok), 1);
                            $spreadSheet->getSheet($k_parent)->getCell('D'. (15 + $num6) )->setValue($num6);
                            $spreadSheet->getSheet($k_parent)->getCell('F'. (15 + $num6) )->setValue($v['lokasi']);
                            $spreadSheet->getSheet($k_parent)->getCell('K'. (15 + $num6) )->setValue($v['sto']);

                            $spreadSheet->getSheet($k_parent)->mergeCells('D'. (15 + $num6) . ':'. 'E'. (15 + $num6) );
                            $spreadSheet->getSheet($k_parent)->mergeCells('F'. (15 + $num6) . ':'. 'J'. (15 + $num6) );
                            $spreadSheet->getSheet($k_parent)->mergeCells('K'. (15 + $num6) . ':'. 'L'. (15 + $num6) );
                        }
                        // $spreadSheet->getSheet($k_parent)->getStyle('D'. (15 + $num6))->getAlignment()->setWrapText(true);
                        // $spreadSheet->getSheet($k_parent)->getRowDimension( (15 + $num6) )->setRowHeight(35);
                    }

                    if($val == 'BA ABD')
                    {
                        //sheet7
                        $num7 = 0;
                        $no_lok = 0;
                        foreach($this->datasl as $kk => $v)
                        {
                            $num7 += 1;
                            $no_lok = $no_lok++;

                            $spreadSheet->getSheet($k_parent)->insertNewRowBefore((15 + $num7 + $no_lok), 1);
                            $spreadSheet->getSheet($k_parent)->getCell('B'. (15 + $num7) )->setValue($num7);
                            $spreadSheet->getSheet($k_parent)->getCell('C'. (15 + $num7) )->setValue($v['lokasi']);
                            $spreadSheet->getSheet($k_parent)->getCell('E'. (15 + $num7) )->setValue($v['sto']);
                            $spreadSheet->getSheet($k_parent)->mergeCells('C'. (15 + $num7) . ':'. 'D'. (15 + $num7) );
                            $spreadSheet->getSheet($k_parent)->mergeCells('E'. (15 + $num7) . ':'. 'F'. (15 + $num7) );
                            // $spreadSheet->getSheet($k_parent)->getStyle('F'. (15 + $num7) )->getAlignment()->setWrapText(true);
                            // $spreadSheet->getSheet($k_parent)->getRowDimension( (15 + $num7) )->setRowHeight(35);
                        }
                    }

                    if($val == 'BA REKONSILIASI')
                    {
                        if($this->data3)
                        {
                            $num8 =0;
                            $no_lok = 0;

                            foreach($this->data3 as $kk => $v)
                            {
                                $num8 += 1;
                                $no_lok = $no_lok++;
                                $spreadSheet->getSheet($k_parent)->insertNewRowBefore((17 + $num8 + $no_lok), 1);
                                $spreadSheet->getSheet($k_parent)->getCell('C'. (17 + $num8) )->setValue(++$kk);
                                $spreadSheet->getSheet($k_parent)->getCell('D'. (17 + $num8) )->setValue($v->lokasi);
                                $spreadSheet->getSheet($k_parent)->getCell('F'. (17 + $num8) )->setValue($v->pid_sto);
                                $spreadSheet->getSheet($k_parent)->getCell('G'. (17 + $num8) )->setValue($v->sto);

                                $spreadSheet->getSheet($k_parent)->getStyle('D'. (17 + $num8) )->getAlignment()->setWrapText(true);
                                $spreadSheet->getSheet($k_parent)->getStyle('F'. (17 + $num8) )->getAlignment()->setWrapText(true);
                                $spreadSheet->getSheet($k_parent)->getRowDimension( (17 + $num8) )->setRowHeight(61);

                                $spreadSheet->getSheet($k_parent)->mergeCells('D'. (17 + $num8) . ':'. 'E'. (17 + $num8) );
                            }


                            $spreadSheet->getSheet($k_parent)->getStyle('C17:'. 'G'. (17 + $num8) )->applyFromArray($border_Style);
                            $spreadSheet->getSheet($k_parent)->getStyle('C17:'. 'G'. (17 + $num8) )->getFont()->setBold(true);

                            $no_lok = 0;

                            foreach($this->data3 as $kk => $v)
                            {
                                $bask = ++$num8;
                                $no_lok = $no_lok++;

                                $spreadSheet->getSheet($k_parent)->insertNewRowBefore((48 + $num8 + $no_lok), 1);
                                $spreadSheet->getSheet($k_parent)->getCell('C'. (48 + $bask) )->setValue(++$kk);
                                $spreadSheet->getSheet($k_parent)->getCell('D'. (48 + $bask) )->setValue($v->lokasi);
                                $spreadSheet->getSheet($k_parent)->getCell('E'. (48 + $bask) )->setValue( ($v->total_material_rekon != 0 ? 'Rp. '. number_format($v->total_material_rekon, 0, '.', '.') : '') );
                                $spreadSheet->getSheet($k_parent)->getCell('F'. (48 + $bask) )->setValue( ($v->total_jasa_rekon != 0 ? 'Rp. '. number_format($v->total_jasa_rekon, 0, '.', '.') : '') );
                                $spreadSheet->getSheet($k_parent)->getCell('G'. (48 + $bask) )->setValue('Rp. '. number_format($v->gd_rekon, 0, '.', '.') );
                            }

                            $num8 = $bask;
                            $no_lok = 0;

                            foreach($this->data3 as $kk => $v)
                            {
                                $bask = ++$num8;
                                $no_lok = $no_lok++;
                                $spreadSheet->getSheet($k_parent)->insertNewRowBefore((56 + $num8 + $no_lok), 1);

                                $spreadSheet->getSheet($k_parent)->getCell('C'. (56 + $bask) )->setValue(++$kk);
                                $spreadSheet->getSheet($k_parent)->getCell('D'. (56 + $bask) )->setValue($v->lokasi);

                                $toc_tgl = date('Y-m-d');

                                if($this->data->tgl_jatuh_tmp && $this->data->tgl_sp)
                                {
                                    $toc_tgl = (date('Y-m-d', strtotime($this->data->tgl_sp .' +'. ($this->data->tgl_jatuh_tmp - 1) .' day' ) ) );
                                }

                                $spreadSheet->getSheet($k_parent)->getCell('E'. (56 + $bask) )->setValue($this->convert_month( (date('d-m-Y', strtotime($toc_tgl) ) ) ) );
                                $spreadSheet->getSheet($k_parent)->getCell('F'. (56 + $bask) )->setValue($this->convert_month( (date('d-m-Y', strtotime($this->data->tgl_baut) ) ) ) );
                                $spreadSheet->getSheet($k_parent)->getCell('G'. (56 + $bask) )->setValue('-');
                            }

                            $num8 = $bask;

                            $spreadSheet->getSheet($k_parent)->getStyle('B'. (86 + $bask) . ':' . 'I'. (95 + $bask) )->getFont()->setBold(true);
                            $spreadSheet->getSheet($k_parent)->mergeCells('B'. (86 + $bask) . ':' . 'I'. (86 + $bask) );

                            $spreadSheet->getSheet($k_parent)->getStyle('B'. (86 + $bask) . ':' . 'I'. (95 + $bask) )->applyFromArray([
                                'alignment' => [
                                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                                    'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                                ]
                            ]);

                            if($this->data->gd_rekon <= 500000000)
                            {
                                $spreadSheet->getSheet($k_parent)->getCell('B'. (86 + $bask) )->setValue('MENGETAHUI,');
                                $spreadSheet->getSheet($k_parent)->mergeCells('B'. (94 + $bask) . ':' . 'I'. (94 + $bask) );
                                $spreadSheet->getSheet($k_parent)->getCell('B'. (94 + $bask) )->setValue('=MASTER!E30');
                                $spreadSheet->getSheet($k_parent)->mergeCells('B'. (95 + $bask) . ':' . 'I'. (95 + $bask) );
                                $spreadSheet->getSheet($k_parent)->getCell('B'. (95 + $bask) )->setValue('=MASTER!F30');
                            }
                            else
                            {
                                $spreadSheet->getSheet($k_parent)->getCell('B'. (86 + $bask) )->setValue('Mengetahui & Menyetujui');

                                $spreadSheet->getSheet($k_parent)->mergeCells('B'. (88 + $bask) . ':' . 'D'. (88 + $bask) );
                                $spreadSheet->getSheet($k_parent)->getCell('B'. (88 + $bask) )->setValue('PT. TELKOM AKSES');
                                $spreadSheet->getSheet($k_parent)->mergeCells('B'. (94 + $bask) . ':' . 'D'. (94 + $bask) );
                                $spreadSheet->getSheet($k_parent)->getCell('B'. (94 + $bask) )->setValue('=MASTER!E30');
                                $spreadSheet->getSheet($k_parent)->mergeCells('B'. (95 + $bask) . ':' . 'D'. (95 + $bask) );
                                $spreadSheet->getSheet($k_parent)->getCell('B'. (95 + $bask) )->setValue('=MASTER!F30');

                                $spreadSheet->getSheet($k_parent)->mergeCells('G'. (88 + $bask) . ':' . 'I'. (88 + $bask) );
                                $spreadSheet->getSheet($k_parent)->getCell('G'. (88 + $bask) )->setValue('PT. TELKOM AKSES');
                                $spreadSheet->getSheet($k_parent)->mergeCells('G'. (94 + $bask) . ':' . 'I'. (94 + $bask) );
                                $spreadSheet->getSheet($k_parent)->getCell('G'. (94 + $bask) )->setValue('=MASTER!A54');
                                $spreadSheet->getSheet($k_parent)->mergeCells('G'. (95 + $bask) . ':' . 'I'. (95 + $bask) );
                                $spreadSheet->getSheet($k_parent)->getCell('G'. (95 + $bask) )->setValue('=MASTER!B54');
                            }
                        }
                    }

                    if($val == 'Rekap BA Rekon')
                    {
                        // foreach($this->data5 as $v)
                        // {
                        //     if($this->data->bulan_pengerjaan >= '2021-11')
                        //     {
                        //         $spreadSheet->getSheet($k_parent)->getCell('B8')->setValue('Nama Pekerjaan');

                        //         $g_design_9['lokasi'][0]['sto'] = $v->sto;
                        //         $g_design_9['lokasi'][0]['title'] = $this->data->judul;
                        //         if(!isset($g_design_9['lokasi'][0]['material_sp']) )
                        //         {
                        //             $g_design_9['lokasi'][0]['material_sp'] = 0;
                        //             $g_design_9['lokasi'][0]['jasa_sp'] = 0;

                        //             $g_design_9['lokasi'][0]['material_rekon'] = 0;
                        //             $g_design_9['lokasi'][0]['jasa_rekon'] = 0;

                        //             $g_design_9['lokasi'][0]['material_tambah'] = 0;
                        //             $g_design_9['lokasi'][0]['jasa_tambah'] = 0;

                        //             $g_design_9['lokasi'][0]['material_kurang'] = 0;
                        //             $g_design_9['lokasi'][0]['jasa_kurang'] = 0;
                        //         }
                        //         $g_design_9['lokasi'][0]['material_sp'] += $v->sp * $v->material;
                        //         $g_design_9['lokasi'][0]['jasa_sp'] += $v->sp * $v->jasa;

                        //         $g_design_9['lokasi'][0]['material_rekon'] += $v->rekon * $v->material;
                        //         $g_design_9['lokasi'][0]['jasa_rekon'] += $v->rekon * $v->jasa;

                        //         $g_design_9['lokasi'][0]['material_tambah'] += $v->tambah * $v->material;
                        //         $g_design_9['lokasi'][0]['jasa_tambah'] += $v->tambah * $v->jasa;

                        //         $g_design_9['lokasi'][0]['material_kurang'] += $v->kurang * $v->material;
                        //         $g_design_9['lokasi'][0]['jasa_kurang'] += $v->kurang * $v->jasa;
                        //     }
                        //     else
                        //     {
                        //         $spreadSheet->getSheet($k_parent)->getCell('B8')->setValue('Lokasi');

                        //         $g_design_9['lokasi'][$v->id_boq_lokasi]['sto'] = $v->sto;
                        //         $g_design_9['lokasi'][$v->id_boq_lokasi]['title'] = $v->lokasi;

                        //         if(!isset($g_design_9['lokasi'][$v->id_boq_lokasi]['material_sp']) )
                        //         {
                        //             $g_design_9['lokasi'][$v->id_boq_lokasi]['material_sp'] = 0;
                        //             $g_design_9['lokasi'][$v->id_boq_lokasi]['jasa_sp'] = 0;

                        //             $g_design_9['lokasi'][$v->id_boq_lokasi]['material_rekon'] = 0;
                        //             $g_design_9['lokasi'][$v->id_boq_lokasi]['jasa_rekon'] = 0;

                        //             $g_design_9['lokasi'][$v->id_boq_lokasi]['material_tambah'] = 0;
                        //             $g_design_9['lokasi'][$v->id_boq_lokasi]['jasa_tambah'] = 0;

                        //             $g_design_9['lokasi'][$v->id_boq_lokasi]['material_kurang'] = 0;
                        //             $g_design_9['lokasi'][$v->id_boq_lokasi]['jasa_kurang'] = 0;
                        //         }
                        //         $g_design_9['lokasi'][$v->id_boq_lokasi]['material_sp'] += $v->sp * $v->material;
                        //         $g_design_9['lokasi'][$v->id_boq_lokasi]['jasa_sp'] += $v->sp * $v->jasa;

                        //         $g_design_9['lokasi'][$v->id_boq_lokasi]['material_rekon'] += $v->rekon * $v->material;
                        //         $g_design_9['lokasi'][$v->id_boq_lokasi]['jasa_rekon'] += $v->rekon * $v->jasa;

                        //         $g_design_9['lokasi'][$v->id_boq_lokasi]['material_tambah'] += $v->tambah * $v->material;
                        //         $g_design_9['lokasi'][$v->id_boq_lokasi]['jasa_tambah'] += $v->tambah * $v->jasa;

                        //         $g_design_9['lokasi'][$v->id_boq_lokasi]['material_kurang'] += $v->kurang * $v->material;
                        //         $g_design_9['lokasi'][$v->id_boq_lokasi]['jasa_kurang'] += $v->kurang * $v->jasa;
                        //     }

                        //     if(!isset($g_design_9['total']['material_sp']) )
                        //     {
                        //         $g_design_9['total']['material_sp'] = 0;
                        //         $g_design_9['total']['jasa_sp'] = 0;

                        //         $g_design_9['total']['material_rekon'] = 0;
                        //         $g_design_9['total']['jasa_rekon'] = 0;

                        //         $g_design_9['total']['material_tambah'] = 0;
                        //         $g_design_9['total']['jasa_tambah'] = 0;

                        //         $g_design_9['total']['material_kurang'] = 0;
                        //         $g_design_9['total']['jasa_kurang'] = 0;
                        //     }
                        //     $g_design_9['total']['material_sp'] += $v->sp * $v->material;
                        //     $g_design_9['total']['jasa_sp'] += $v->sp * $v->jasa;

                        //     $g_design_9['total']['material_rekon'] += $v->rekon * $v->material;
                        //     $g_design_9['total']['jasa_rekon'] += $v->rekon * $v->jasa;

                        //     $g_design_9['total']['material_tambah'] += $v->tambah * $v->material;
                        //     $g_design_9['total']['jasa_tambah'] += $v->tambah * $v->jasa;

                        //     $g_design_9['total']['material_kurang'] += $v->kurang * $v->material;
                        //     $g_design_9['total']['jasa_kurang'] += $v->kurang * $v->jasa;
                        // }
                        // // dd($g_design_9);

                        foreach($data_bq['rekon'] as $k => $v)
                        {
                            foreach($v['list'] as $vv)
                            {
                                if($this->data->bulan_pengerjaan >= '2021-11')
                                {
                                    $spreadSheet->getSheet($k_parent)->getCell('B8')->setValue('Nama Pekerjaan');

                                    $g_design_9['lokasi'][0]['sto'] = $v['sto'];
                                    $g_design_9['lokasi'][0]['title'] = $this->data->judul;
                                    if(!isset($g_design_9['lokasi'][0]['material_sp']) )
                                    {
                                        $g_design_9['lokasi'][0]['material_sp'] = 0;
                                        $g_design_9['lokasi'][0]['jasa_sp'] = 0;

                                        $g_design_9['lokasi'][0]['material_rekon'] = 0;
                                        $g_design_9['lokasi'][0]['jasa_rekon'] = 0;

                                        $g_design_9['lokasi'][0]['material_tambah'] = 0;
                                        $g_design_9['lokasi'][0]['jasa_tambah'] = 0;

                                        $g_design_9['lokasi'][0]['material_kurang'] = 0;
                                        $g_design_9['lokasi'][0]['jasa_kurang'] = 0;
                                    }
                                    $g_design_9['lokasi'][0]['material_sp'] += $vv['sp'] * $vv['material'];
                                    $g_design_9['lokasi'][0]['jasa_sp']     += $vv['sp'] * $vv['jasa'];

                                    $g_design_9['lokasi'][0]['material_rekon'] += $vv['rekon'] * $vv['material'];
                                    $g_design_9['lokasi'][0]['jasa_rekon']     += $vv['rekon'] * $vv['jasa'];

                                    $g_design_9['lokasi'][0]['material_tambah'] += $vv['tambah'] * $vv['material'];
                                    $g_design_9['lokasi'][0]['jasa_tambah']     += $vv['tambah'] * $vv['jasa'];

                                    $g_design_9['lokasi'][0]['material_kurang'] += $vv['kurang'] * $vv['material'];
                                    $g_design_9['lokasi'][0]['jasa_kurang']     += $vv['kurang'] * $vv['jasa'];
                                }
                                else
                                {
                                    $spreadSheet->getSheet($k_parent)->getCell('B8')->setValue('Lokasi');

                                    $g_design_9['lokasi'][$k]['sto'] = $v['sto'];
                                    $g_design_9['lokasi'][$k]['title'] = $v['lokasi'];

                                    if(!isset($g_design_9['lokasi'][$k]['material_sp']) )
                                    {
                                        $g_design_9['lokasi'][$k]['material_sp'] = 0;
                                        $g_design_9['lokasi'][$k]['jasa_sp'] = 0;

                                        $g_design_9['lokasi'][$k]['material_rekon'] = 0;
                                        $g_design_9['lokasi'][$k]['jasa_rekon'] = 0;

                                        $g_design_9['lokasi'][$k]['material_tambah'] = 0;
                                        $g_design_9['lokasi'][$k]['jasa_tambah'] = 0;

                                        $g_design_9['lokasi'][$k]['material_kurang'] = 0;
                                        $g_design_9['lokasi'][$k]['jasa_kurang'] = 0;
                                    }

                                    $g_design_9['lokasi'][$k]['material_sp'] += $vv['sp'] * $vv['material'];
                                    $g_design_9['lokasi'][$k]['jasa_sp']     += $vv['sp'] * $vv['jasa'];

                                    $g_design_9['lokasi'][$k]['material_rekon'] += $vv['rekon'] * $vv['material'];
                                    $g_design_9['lokasi'][$k]['jasa_rekon']     += $vv['rekon'] * $vv['jasa'];

                                    $g_design_9['lokasi'][$k]['material_tambah'] += $vv['tambah'] * $vv['material'];
                                    $g_design_9['lokasi'][$k]['jasa_tambah']     += $vv['tambah'] * $vv['jasa'];

                                    $g_design_9['lokasi'][$k]['material_kurang'] += $vv['kurang'] * $vv['material'];
                                    $g_design_9['lokasi'][$k]['jasa_kurang']     += $vv['kurang'] * $vv['jasa'];
                                }

                                if(!isset($g_design_9['total']['material_sp']) )
                                {
                                    $g_design_9['total']['material_sp'] = 0;
                                    $g_design_9['total']['jasa_sp']     = 0;

                                    $g_design_9['total']['material_rekon'] = 0;
                                    $g_design_9['total']['jasa_rekon']     = 0;

                                    $g_design_9['total']['material_tambah'] = 0;
                                    $g_design_9['total']['jasa_tambah']     = 0;

                                    $g_design_9['total']['material_kurang'] = 0;
                                    $g_design_9['total']['jasa_kurang']     = 0;
                                }

                                $g_design_9['total']['material_sp'] += $vv['sp'] * $vv['material'];
                                $g_design_9['total']['jasa_sp']     += $vv['sp'] * $vv['jasa'];

                                $g_design_9['total']['material_rekon'] += $vv['rekon'] * $vv['material'];
                                $g_design_9['total']['jasa_rekon']     += $vv['rekon'] * $vv['jasa'];

                                $g_design_9['total']['material_tambah'] += $vv['tambah'] * $vv['material'];
                                $g_design_9['total']['jasa_tambah']     += $vv['tambah'] * $vv['jasa'];

                                $g_design_9['total']['material_kurang'] += $vv['kurang'] * $vv['material'];
                                $g_design_9['total']['jasa_kurang']     += $vv['kurang'] * $vv['jasa'];
                            }
                        }

                        if(!empty($g_design_9) )
                        {
                            $raw_num9 = 0;
                            $no_lok = 0;
                            foreach($g_design_9['lokasi'] as $v)
                            {
                                $no_lok = $no_lok++;
                                $num9 = ++$raw_num9;
                                $spreadSheet->getSheet($k_parent)->insertNewRowBefore((11 + $no_lok), 1);

                                $spreadSheet->getSheet($k_parent)->getCell('A'. (11 + $num9) )->setValue($num9);
                                $spreadSheet->getSheet($k_parent)->getCell('B'. (11 + $num9) )->setValue($v['title']);
                                $spreadSheet->getSheet($k_parent)->getStyle('B'. (11 + $num9) )->getAlignment()->setWrapText(true);
                                $spreadSheet->getSheet($k_parent)->getRowDimension( (11 + $num9) )->setRowHeight(-1);

                                $spreadSheet->getSheet($k_parent)->getCell('C'. (11 + $num9) )->setValue($v['material_sp']);
                                $spreadSheet->getSheet($k_parent)->getCell('D'. (11 + $num9) )->setValue($v['jasa_sp']);
                                $spreadSheet->getSheet($k_parent)->getCell('E'. (11 + $num9) )->setValue($v['material_sp'] + $v['jasa_sp']);

                                $spreadSheet->getSheet($k_parent)->getCell('F'. (11 + $num9) )->setValue($v['material_rekon']);
                                $spreadSheet->getSheet($k_parent)->getCell('G'. (11 + $num9) )->setValue($v['jasa_rekon']);
                                $spreadSheet->getSheet($k_parent)->getCell('H'. (11 + $num9) )->setValue($v['material_rekon'] + $v['jasa_rekon']);

                                $spreadSheet->getSheet($k_parent)->getCell('I'. (11 + $num9) )->setValue($v['material_tambah']);
                                $spreadSheet->getSheet($k_parent)->getCell('J'. (11 + $num9) )->setValue($v['jasa_tambah']);
                                $spreadSheet->getSheet($k_parent)->getCell('K'. (11 + $num9) )->setValue($v['material_tambah'] + $v['jasa_tambah']);

                                $spreadSheet->getSheet($k_parent)->getCell('L'. (11 + $num9) )->setValue($v['material_kurang']);
                                $spreadSheet->getSheet($k_parent)->getCell('M'. (11 + $num9) )->setValue($v['jasa_kurang']);
                                $spreadSheet->getSheet($k_parent)->getCell('N'. (11 + $num9) )->setValue($v['material_kurang'] + $v['jasa_kurang']);
                                // dd($g_design_9);
                                $spreadSheet->getSheet($k_parent)->getCell('C'. (12 + $num9) )->setValue($v['material_sp']);
                                $spreadSheet->getSheet($k_parent)->getCell('D'. (12 + $num9) )->setValue($v['jasa_sp']);
                                $spreadSheet->getSheet($k_parent)->getCell('E'. (12 + $num9) )->setValue($v['material_sp'] + $v['jasa_sp']);

                                $spreadSheet->getSheet($k_parent)->getCell('F'. (12 + $num9) )->setValue($v['material_rekon']);
                                $spreadSheet->getSheet($k_parent)->getCell('G'. (12 + $num9) )->setValue($v['jasa_rekon']);
                                $spreadSheet->getSheet($k_parent)->getCell('H'. (12 + $num9) )->setValue($v['material_rekon'] + $v['jasa_rekon']);

                                $spreadSheet->getSheet($k_parent)->getCell('I'. (12 + $num9) )->setValue($v['material_tambah']);
                                $spreadSheet->getSheet($k_parent)->getCell('J'. (12 + $num9) )->setValue($v['jasa_tambah']);
                                $spreadSheet->getSheet($k_parent)->getCell('K'. (12 + $num9) )->setValue($v['material_tambah'] + $v['jasa_tambah']);

                                $spreadSheet->getSheet($k_parent)->getCell('L'. (12 + $num9) )->setValue($v['material_kurang']);
                                $spreadSheet->getSheet($k_parent)->getCell('M'. (12 + $num9) )->setValue($v['jasa_kurang']);
                                $spreadSheet->getSheet($k_parent)->getCell('N'. (12 + $num9) )->setValue($v['material_kurang'] + $v['jasa_kurang']);
                            }
                        }

                        $spreadSheet->getSheet($k_parent)->getStyle('B'. (24 + $num9) . ':' . 'M'. (31 + $num9) )->getFont()->setBold(true);
                        $spreadSheet->getSheet($k_parent)->mergeCells('F'. (24 + $num9) . ':' . 'I'. (24 + $num9) );

                        $spreadSheet->getSheet($k_parent)->getStyle('B'. (24 + $num9) . ':' . 'M'. (31 + $num9) )->applyFromArray([
                            'alignment' => [
                                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                            ]
                        ]);

                        if($this->data->gd_rekon <= 500000000)
                        {
                            $spreadSheet->getSheet($k_parent)->getCell('F'. (24 + $num9) )->setValue('MENGETAHUI,');

                            $spreadSheet->getSheet($k_parent)->mergeCells('F'. (30 + $num9) . ':' . 'I'. (30 + $num9) );
                            $spreadSheet->getSheet($k_parent)->getCell('F'. (30 + $num9) )->setValue('=MASTER!D31');

                            $spreadSheet->getSheet($k_parent)->mergeCells('F'. (31 + $num9) . ':' . 'I'. (31 + $num9) );
                            $spreadSheet->getSheet($k_parent)->getCell('F'. (31 + $num9) )->setValue('=MASTER!E31');
                        }
                        else
                        {
                            $spreadSheet->getSheet($k_parent)->getCell('F'. (24 + $num9) )->setValue('Mengetahui & Menyetujui');

                            $spreadSheet->getSheet($k_parent)->mergeCells('B'. (25 + $num9) . ':' . 'E'. (25 + $num9) );
                            $spreadSheet->getSheet($k_parent)->getCell('B'. (25 + $num9) )->setValue('PT. TELKOM AKSES');

                            $spreadSheet->getSheet($k_parent)->mergeCells('B'. (30 + $num9) . ':' . 'E'. (30 + $num9) );
                            $spreadSheet->getSheet($k_parent)->getCell('B'. (30 + $num9) )->setValue('=MASTER!D31');

                            $spreadSheet->getSheet($k_parent)->mergeCells('B'. (31 + $num9) . ':' . 'E'. (31 + $num9) );
                            $spreadSheet->getSheet($k_parent)->getCell('B'. (31 + $num9) )->setValue('=MASTER!E31');

                            $spreadSheet->getSheet($k_parent)->mergeCells('J'. (25 + $num9) . ':' . 'M'. (25 + $num9) );
                            $spreadSheet->getSheet($k_parent)->getCell('J'. (25 + $num9) )->setValue('PT. TELKOM AKSES');

                            $spreadSheet->getSheet($k_parent)->mergeCells('J'. (30 + $num9) . ':' . 'M'. (30 + $num9) );
                            $spreadSheet->getSheet($k_parent)->getCell('J'. (30 + $num9) )->setValue('=MASTER!A54');

                            $spreadSheet->getSheet($k_parent)->mergeCells('J'. (31 + $num9) . ':' . 'M'. (31 + $num9) );
                            $spreadSheet->getSheet($k_parent)->getCell('J'. (31 + $num9) )->setValue('=MASTER!B54');
                        }
                    }

                    if($val == 'LAMPIRAN SP')
                    {
                        $num_lamp = 0;
                        $tot_sp = [];
                        $tot_all = [];

                        if($data_bq)
                        {
                            $spreadSheet->getSheet($k_parent)->insertNewColumnBefore('G', count($data_bq['sp']) );
                            $spreadSheet->getSheet($k_parent)->insertNewRowBefore(11, count($data_bq['sp'][0]['list']) );

                            foreach($data_bq['sp'][0]['list'] as $kk => $vv)
                            {
                                $num_lamp = ++$kk;
                                $spreadSheet->getSheet($k_parent)->getCell('A'. (10 + $num_lamp) )->setValue($num_lamp);
                                $spreadSheet->getSheet($k_parent)->getCell('B'. (10 + $num_lamp) )->setValue($vv['designator']);
                                $spreadSheet->getSheet($k_parent)->getCell('C'. (10 + $num_lamp) )->setValue($vv['uraian']);
                                $spreadSheet->getSheet($k_parent)->getCell('D'. (10 + $num_lamp) )->setValue($vv['satuan']);
                                $spreadSheet->getSheet($k_parent)->getCell('E'. (10 + $num_lamp) )->setValue($vv['material'] != 0 ? $vv['material'] : '-');
                                $spreadSheet->getSheet($k_parent)->getCell('F'. (10 + $num_lamp) )->setValue($vv['jasa'] != 0 ? $vv['jasa'] : '-');
                            }

                            $num_lamp = 0;
                            $num_lamp_head =0;

                            foreach($data_bq['sp'] as $k1 => $v2)
                            {
                                $num_lamp_head = $num_lamp_head++;
                                $spreadSheet->getSheet($k_parent)->getCell($index[$k1 + $num_lamp_head].''. 8 )->setValue($v2['sto']);
                                $spreadSheet->getSheet($k_parent)->getCell($index[$k1 + $num_lamp_head].''. 9 )->setValue($v2['lokasi']);

                                $spreadSheet->getSheet($k_parent)->getCell($index[$k1 + $num_lamp_head].''. 10 )->setValue($v2['pid_sto_lok']);
                                $spreadSheet->getSheet($k_parent)->getStyle($index[$k1 + $num_lamp_head].''. 8  .':'. $index[$k1 + $num_lamp_head].''. 10 )->applyFromArray($border_Style);
                                $spreadSheet->getSheet($k_parent)->getStyle($index[$k1 + $num_lamp_head].''. 8  .':'. $index[$k1 + $num_lamp_head].''. 10 )->applyFromArray([
                                    'fill' => [
                                        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                                        'startColor' => [
                                            'argb' => 'ffff00',
                                        ],
                                    ]
                                ]);
                                // $spreadSheet->getSheet($k_parent)->mergeCells($index[$k1 + $num_lamp_head].''. 8 . ':'. $index[$k1 + $num_lamp_head].''. 9 );
                                $v2['list'] = array_values($v2['list']);
                                foreach($v2['list'] as $k2 => $v3)
                                {
                                    $num_lamp = ++$k2;
                                    $spreadSheet->getSheet($k_parent)->getCell($index[$k1].''. (10 + $num_lamp) )->setValue($v3['sp'] != 0 ?$v3['sp'] : '-');

                                    if(!isset($tot_sp[$k1]) )
                                    {
                                        $tot_sp[$k1]['material_sp'] = 0;
                                        $tot_sp[$k1]['jasa_sp'] = 0;
                                    }

                                    $tot_sp[$k1]['material_sp'] += $v3['sp'] * $v3['material'];
                                    $tot_sp[$k1]['jasa_sp'] += $v3['sp'] * $v3['jasa'];

                                    if(!isset($tot_all[($num_lamp + 10)]) )
                                    {
                                        $tot_all[($num_lamp + 10)] = 0;
                                    }

                                    $tot_all[($num_lamp + 10)] += $v3['sp'];
                                }

                                $spreadSheet->getSheet($k_parent)->getCell($index[$k1].''. (11 + $num_lamp) )->setValue($tot_sp[$k1]['material_sp'] != 0 ? $tot_sp[$k1]['material_sp'] : '-');
                                $spreadSheet->getSheet($k_parent)->getCell($index[$k1].''. (12 + $num_lamp) )->setValue($tot_sp[$k1]['jasa_sp'] != 0 ? $tot_sp[$k1]['jasa_sp'] : '-');

                                $total_sp = $tot_sp[$k1]['jasa_sp'] + $tot_sp[$k1]['material_sp'];

                                $sp_pph = $AdminModel->convert_floor_ceil($this->data->tgl_sp, $total_sp * $pph_sp);

                                $spreadSheet->getSheet($k_parent)->getCell($index[$k1].''. (13 + $num_lamp) )->setValue($total_sp != 0 ? $total_sp: '-');
                                $spreadSheet->getSheet($k_parent)->getCell($index[$k1].''. (14 + $num_lamp) )->setValue($total_sp != 0 ? $sp_pph : '-');
                                $spreadSheet->getSheet($k_parent)->getCell($index[$k1].''. (15 + $num_lamp) )->setValue($total_sp != 0 ? $total_sp + $sp_pph : '-');
                            }

                            foreach($tot_all as $key => $val)
                            {
                                $spreadSheet->getSheet($k_parent)->getCell($index[($k1 + 1)].''. $key )->setValue($val);
                            }
                            // dd($data_tot);
                            $spreadSheet->getSheet($k_parent)->getCell($index[($k1 + 1)].''. (11 + $num_lamp) )->setValue($data_tot['sp']['total']['material_sp'] != 0 ? $data_tot['sp']['total']['material_sp'] : '-');
                            $spreadSheet->getSheet($k_parent)->getCell($index[($k1 + 1)].''. (12 + $num_lamp) )->setValue($data_tot['sp']['total']['jasa_sp'] != 0 ? $data_tot['sp']['total']['jasa_sp'] : '-');

                            $total_all_sp = $data_tot['sp']['total']['jasa_sp'] + $data_tot['sp']['total']['material_sp'];

                            $all_sp_pph = $AdminModel->convert_floor_ceil($this->data->tgl_sp, $total_all_sp * $pph_sp);

                            $spreadSheet->getSheet($k_parent)->getCell($index[($k1 + 1)].''. (13 + $num_lamp) )->setValue($total_all_sp != 0 ? $total_all_sp : '-');
                            $spreadSheet->getSheet($k_parent)->getCell($index[($k1 + 1)].''. (14 + $num_lamp) )->setValue($total_all_sp != 0 ? $all_sp_pph : '-');
                            $spreadSheet->getSheet($k_parent)->getCell($index[($k1 + 1)].''. (15 + $num_lamp) )->setValue($total_all_sp != 0 ? $total_all_sp + $all_sp_pph : '-');
                        }
                    }

                    if($val == 'BAKH REKON')
                    {
                        $num_lamp = 0;
                        $tot_sp = $data_design_bq = [];

                        $data_rekon_sp = DB::SELECT("SELECT pbd.*, pbl.lokasi, pbl.sto, pd.jenis, pbu.pekerjaan, pd.design_mitra_id, pd.satuan, pd.uraian, pbd.material as material_default, pbd.jasa as jasa_default, pbl.sto
                        FROM procurement_boq_upload pbu
                        LEFT JOIN procurement_Boq_lokasi pbl ON pbu.id = pbl.id_upload
                        LEFT JOIN procurement_Boq_design pbd ON pbl.id = pbd.id_boq_lokasi
                        LEFT JOIN procurement_designator pd ON pd.id = pbd.id_design
                        LEFT JOIN procurement_rfc_osp pro ON pro.id_upload = pbu.id
                        LEFT JOIN procurement_mitra pm ON pm.id = pbu.mitra_id
                        WHERE pbu.id = ". $this->data->id. " AND pbu.active = 1 AND pd.active = 1 AND pbd.jenis_khs = 0
                        -- WHERE pro.active = 1 AND pro.status_rfc = 'LURUS' AND pbu.id = ". $this->data->id. " AND pbu.active = 1 AND pbd.jenis_khs = 0
                        GROUP BY pbl.id, pbd.id");
                        $data_design_bq_sp = [];

                        foreach($data_rekon_sp as $kk => $val)
                        {
                            if($val->design_mitra_id != 16)
                            {
                                $data_design_bq_sp[$val->id_design] = $val;
                            }
                        }

                        foreach($this->data5 as $kk => $val)
                        {
                            if($val->design_mitra_id != 16)
                            {
                                $data_design_bq[$val->id_design] = $val;
                            }
                        }

                        foreach($data_design_bq_sp as $ks => $v)
                        {
                            unset($data_design_bq[$ks]);
                        }

                        $data_design_bq = array_values($data_design_bq);

                        if($data_design_bq)
                        {
                            $spreadSheet->getSheet($k_parent)->insertNewRowBefore(16, count($data_design_bq) );
                            $no_num = 0;

                            foreach($data_design_bq as $v2)
                            {
                                $num_lamp = ++$no_num;
                                $spreadSheet->getSheet($k_parent)->getCell('C'. (15 + $num_lamp) )->setValue($num_lamp);
                                $spreadSheet->getSheet($k_parent)->getCell('D'. (15 + $num_lamp) )->setValue($v2->designator);
                                // $spreadSheet->getSheet($k_parent)->getCell('E'. (15 + $num_lamp) )->setValue(html_entity_decode($v2->uraian, ENT_QUOTES, 'UTF-8') );
                                $spreadSheet->getSheet($k_parent)->getRowDimension( (15 + $num_lamp) )->setRowHeight(-1);
                                $spreadSheet->getSheet($k_parent)->getCell('E'. (15 + $num_lamp) )->setValue($v2->satuan);
                                $spreadSheet->getSheet($k_parent)->getCell('F'. (15 + $num_lamp) )->setValue($v2->material);
                                $spreadSheet->getSheet($k_parent)->mergeCells('F'. (15 + $num_lamp) . ':'. 'G'. (15 + $num_lamp) );
                                $spreadSheet->getSheet($k_parent)->getCell('H'. (15 + $num_lamp) )->setValue($v2->jasa);
                                $spreadSheet->getSheet($k_parent)->mergeCells('H'. (15 + $num_lamp) . ':'. 'I'. (15 + $num_lamp) );
                            }
                        }
                    }

                    if($val == 'BAKH SP')
                    {
                        $num_lamp = 0;
                        $tot_sp = $data_design_bq = [];

                        $data_rekon_sp = DB::SELECT("SELECT pbd.*, pbl.lokasi, pbl.sto, pd.jenis, pbu.pekerjaan, pd.design_mitra_id, pd.satuan, pd.uraian, pbd.material as material_default, pbd.jasa as jasa_default, pbl.sto
                        FROM procurement_boq_upload pbu
                        LEFT JOIN procurement_Boq_lokasi pbl ON pbu.id = pbl.id_upload
                        LEFT JOIN procurement_Boq_design pbd ON pbl.id = pbd.id_boq_lokasi
                        LEFT JOIN procurement_designator pd ON pd.id = pbd.id_design
                        LEFT JOIN procurement_rfc_osp pro ON pro.id_upload = pbu.id
                        LEFT JOIN procurement_mitra pm ON pm.id = pbu.mitra_id
                        WHERE pbu.id = ". $this->data->id. " AND pbu.active = 1 AND pd.active = 1 AND pbd.jenis_khs = 0
                        -- WHERE pro.active = 1 AND pro.status_rfc = 'LURUS' AND pbu.id = ". $this->data->id. " AND pbu.active = 1 AND pbd.jenis_khs = 0
                        GROUP BY pbl.id, pbd.id");

                        foreach($data_rekon_sp as $kk => $val)
                        {
                            if($val->design_mitra_id != 16)
                            {
                                $data_design_bq[$val->id_design] = $val;
                            }
                        }

                        $data_design_bq = array_values($data_design_bq);
                        // dd($data_design_bq, $data_rekon_sp);
                        if($data_design_bq)
                        {
                            $spreadSheet->getSheet($k_parent)->insertNewRowBefore(16, count($data_design_bq) );
                            $no_num = 0;

                            foreach($data_design_bq as $v2)
                            {
                                $num_lamp = ++$no_num;
                                $spreadSheet->getSheet($k_parent)->getCell('C'. (15 + $num_lamp) )->setValue($num_lamp);
                                $spreadSheet->getSheet($k_parent)->getCell('D'. (15 + $num_lamp) )->setValue($v2->designator);
                                // $spreadSheet->getSheet($k_parent)->getCell('E'. (15 + $num_lamp) )->setValue(html_entity_decode($v2->uraian, ENT_QUOTES, 'UTF-8') );
                                $spreadSheet->getSheet($k_parent)->getRowDimension( (15 + $num_lamp) )->setRowHeight(-1);
                                $spreadSheet->getSheet($k_parent)->getCell('E'. (15 + $num_lamp) )->setValue($v2->satuan);
                                $spreadSheet->getSheet($k_parent)->getCell('F'. (15 + $num_lamp) )->setValue($v2->material);
                                $spreadSheet->getSheet($k_parent)->mergeCells('F'. (15 + $num_lamp) . ':'. 'G'. (15 + $num_lamp) );
                                $spreadSheet->getSheet($k_parent)->getCell('H'. (15 + $num_lamp) )->setValue($v2->jasa);
                                $spreadSheet->getSheet($k_parent)->mergeCells('H'. (15 + $num_lamp) . ':'. 'I'. (15 + $num_lamp) );
                            }
                        }
                    }

                    if($val == 'BOQ')
                    {
                        $boq_filter = $data_bq['rekon'];
                        if($boq_filter)
                        {
                            $num_boq_tb = 0;
                            $tot_rekon = $tot_all = $unused = $get_unused = [];

                            foreach($boq_filter As $v)
                            {
                                foreach($v['list'] As $vv)
                                {
                                    $unused[$vv['id_design'] ]['id_mat'] = $vv['id_design'];

                                    if(!isset($unused[$vv['id_design'] ]['mat'] ) )
                                    {
                                        $unused[$vv['id_design'] ]['mat'] = 0;
                                    }

                                    $unused[$vv['id_design'] ]['mat'] += $vv['rekon'];
                                }
                            }

                            foreach($unused as $v)
                            {
                                if($v['mat'] == 0)
                                {
                                    $get_unused[] = $v;
                                }
                            }
                            // dd($get_unused, $unused);
                            if($get_unused)
                            {
                                foreach($boq_filter As $kv => $v)
                                {
                                    foreach($v['list'] As $kk => &$vv)
                                    {
                                        $search_unused = array_search($vv['id_design'], array_column($get_unused, 'id_mat') );

                                        if($search_unused !== FALSE)
                                        {
                                            unset($boq_filter[$kv]['list'][$kk]);
                                        }
                                    }
                                }
                            }

                            foreach($boq_filter As $kv => $v)
                            {
                                $boq_filter[$kv]['list'] = array_values($v['list']);

                            }

                            $spreadSheet->getSheet($k_parent)->insertNewColumnBefore('G', count($boq_filter) );
                            $spreadSheet->getSheet($k_parent)->insertNewRowBefore(11, count($boq_filter[0]['list']) );

                            foreach($boq_filter[0]['list'] as $kk => $vv)
                            {
                                $num_boq_tb = ++$kk;
                                $spreadSheet->getSheet($k_parent)->getCell('A'. (10 + $num_boq_tb) )->setValue($num_boq_tb);
                                $spreadSheet->getSheet($k_parent)->getCell('B'. (10 + $num_boq_tb) )->setValue($vv['designator']);
                                $spreadSheet->getSheet($k_parent)->getCell('C'. (10 + $num_boq_tb) )->setValue($vv['uraian']);
                                $spreadSheet->getSheet($k_parent)->getCell('D'. (10 + $num_boq_tb) )->setValue($vv['satuan']);
                                $spreadSheet->getSheet($k_parent)->getCell('E'. (10 + $num_boq_tb) )->setValue($vv['material'] != 0 ? $vv['material'] : '-');
                                $spreadSheet->getSheet($k_parent)->getCell('F'. (10 + $num_boq_tb) )->setValue($vv['jasa'] != 0 ? $vv['jasa'] : '-');
                            }

                            $num_boq_tb = 0;
                            $num_boq_tb_head = 0;

                            foreach($boq_filter as $k1 => $v2)
                            {
                                $num_boq_tb_head = $num_boq_tb_head++;
                                $spreadSheet->getSheet($k_parent)->getCell($index[$k1 + $num_boq_tb_head].''. 8 )->setValue($v2['sto']);
                                $spreadSheet->getSheet($k_parent)->getCell($index[$k1 + $num_boq_tb_head].''. 9 )->setValue($v2['lokasi']);
                                $spreadSheet->getSheet($k_parent)->getCell($index[$k1 + $num_boq_tb_head].''. 10 )->setValue($v2['pid_sto_lok']);
                                $spreadSheet->getSheet($k_parent)->getStyle($index[$k1 + $num_boq_tb_head].''. 8  .':'. $index[$k1 + $num_boq_tb_head].''. 10 )->applyFromArray($border_Style);
                                $spreadSheet->getSheet($k_parent)->getStyle($index[$k1 + $num_boq_tb_head].''. 8  .':'. $index[$k1 + $num_boq_tb_head].''. 10 )->applyFromArray([
                                    'fill' => [
                                        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                                        'startColor' => [
                                            'argb' => 'ffff00',
                                        ],
                                    ]
                                ]);

                                $v2['list'] = array_values($v2['list']);

                                foreach($v2['list'] as $k2 => $v3)
                                {
                                    $num_boq_tb = ++$k2;
                                    $spreadSheet->getSheet($k_parent)->getCell($index[$k1].''. (10 + $num_boq_tb) )->setValue($v3['rekon'] != 0 ? $v3['rekon'] : '-');

                                    if(!isset($tot_rekon[$k1]) )
                                    {
                                        $tot_rekon[$k1]['material_rekon'] = 0;
                                        $tot_rekon[$k1]['jasa_rekon'] = 0;
                                    }

                                    $tot_rekon[$k1]['material_rekon'] += $v3['rekon'] * $v3['material'];
                                    $tot_rekon[$k1]['jasa_rekon'] += $v3['rekon'] * $v3['jasa'];

                                    if(!isset($tot_all[($num_boq_tb + 10)]) )
                                    {
                                        $tot_all[($num_boq_tb + 10)] = 0;
                                    }

                                    $tot_all[($num_boq_tb + 10)] += $v3['rekon'];
                                }

                                $spreadSheet->getSheet($k_parent)->getCell($index[$k1].''. (11 + $num_boq_tb) )->setValue(@$tot_rekon[$k1]['material_rekon'] != 0 ? @$tot_rekon[$k1]['material_rekon'] : '-');
                                $spreadSheet->getSheet($k_parent)->getCell($index[$k1].''. (12 + $num_boq_tb) )->setValue(@$tot_rekon[$k1]['jasa_rekon'] != 0 ? @$tot_rekon[$k1]['jasa_rekon'] : '-');

                                $total_rekon = @$tot_rekon[$k1]['jasa_rekon'] + @$tot_rekon[$k1]['material_rekon'];

                                $spreadSheet->getSheet($k_parent)->getCell($index[$k1].''. (13 + $num_boq_tb) )->setValue($total_rekon != 0 ? $total_rekon : '-');

                                $pph_rekon = $AdminModel->convert_floor_ceil($this->data->tgl_ba_rekon, $total_rekon * $pph);
                                $spreadSheet->getSheet($k_parent)->getCell($index[$k1].''. (14 + $num_boq_tb) )->setValue($total_rekon != 0 ? $pph_rekon : '-');
                                $spreadSheet->getSheet($k_parent)->getCell($index[$k1].''. (15 + $num_boq_tb) )->setValue($total_rekon != 0 ? $total_rekon + $pph_rekon : '-');
                            }
                            // dd($tot_all, $k1);
                            foreach($tot_all as $key => $val_x)
                            {
                                $spreadSheet->getSheet($k_parent)->getCell($index[($k1 + 1)].''. $key )->setValue($val_x);
                            }

                            $spreadSheet->getSheet($k_parent)->getCell($index[($k1 + 1)].''. (11 + $num_boq_tb) )->setValue($data_tot['rekon']['total']['material_rekon'] != 0 ? $data_tot['rekon']['total']['material_rekon'] : '-');
                            $spreadSheet->getSheet($k_parent)->getCell($index[($k1 + 1)].''. (12 + $num_boq_tb) )->setValue($data_tot['rekon']['total']['jasa_rekon'] != 0 ? $data_tot['rekon']['total']['jasa_rekon'] : '-');

                            $total_all_rekon = $data_tot['rekon']['total']['jasa_rekon'] + $data_tot['rekon']['total']['material_rekon'];

                            $spreadSheet->getSheet($k_parent)->getCell($index[($k1 + 1)].''. (13 + $num_boq_tb) )->setValue($total_all_rekon != 0 ? $total_all_rekon : '-');

                            $pph_all_rekon = $AdminModel->convert_floor_ceil($this->data->tgl_ba_rekon, $total_all_rekon * $pph);
                            $spreadSheet->getSheet($k_parent)->getCell($index[($k1 + 1)].''. (14 + $num_boq_tb) )->setValue($total_all_rekon != 0 ? $pph_all_rekon : '-');
                            $spreadSheet->getSheet($k_parent)->getCell($index[($k1 + 1)].''. (15 + $num_boq_tb) )->setValue($total_all_rekon != 0 ? $total_all_rekon + $pph_all_rekon : '-');

                            $spreadSheet->getSheet($k_parent)->getStyle('B'. (28 + $num_boq_tb) . ':' . $index[($k1 + 1)] .''. (36 + $num_boq_tb) )->getFont()->setBold(true);
                            $spreadSheet->getSheet($k_parent)->mergeCells('B'. (28 + $num_boq_tb) . ':' . $index[($k1 + 1)] .''. (28 + $num_boq_tb) );

                            $spreadSheet->getSheet($k_parent)->getStyle('B'. (28 + $num_boq_tb) . ':' . $index[($k1 + 1)] .''. (36 + $num_boq_tb) )->applyFromArray([
                                'alignment' => [
                                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                                    'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                                ]
                            ]);

                            if($this->data->gd_rekon <= 500000000)
                            {
                                $spreadSheet->getSheet($k_parent)->getCell('B'. (28 + $num_boq_tb) )->setValue('MENGETAHUI,');

                                $spreadSheet->getSheet($k_parent)->mergeCells('B'. (34 + $num_boq_tb) . ':' . $index[($k1 + 1)] .''. (34 + $num_boq_tb) );
                                $spreadSheet->getSheet($k_parent)->getCell('B'. (34 + $num_boq_tb) )->setValue('=MASTER!D31');

                                $spreadSheet->getSheet($k_parent)->mergeCells('B'. (35 + $num_boq_tb) . ':' . $index[($k1 + 1)] .''. (35 + $num_boq_tb) );
                                $spreadSheet->getSheet($k_parent)->getCell('B'. (35 + $num_boq_tb) )->setValue('=MASTER!E31');
                            }
                            else
                            {
                                $spreadSheet->getSheet($k_parent)->getCell('B'. (28 + $num_boq_tb) )->setValue('Mengetahui & Menyetujui');

                                $spreadSheet->getSheet($k_parent)->mergeCells('B'. (29 + $num_boq_tb) . ':' . 'C'. (29 + $num_boq_tb) );
                                $spreadSheet->getSheet($k_parent)->getCell('B'. (29 + $num_boq_tb) )->setValue('PT. TELKOM AKSES');

                                $spreadSheet->getSheet($k_parent)->mergeCells('B'. (34 + $num_boq_tb) . ':' . 'C'. (34 + $num_boq_tb) );
                                $spreadSheet->getSheet($k_parent)->getCell('B'. (34 + $num_boq_tb) )->setValue('=MASTER!D31');

                                $spreadSheet->getSheet($k_parent)->mergeCells('B'. (35 + $num_boq_tb) . ':' . 'C'. (35 + $num_boq_tb) );
                                $spreadSheet->getSheet($k_parent)->getCell('B'. (35 + $num_boq_tb) )->setValue('=MASTER!E31');

                                // $spreadSheet->getSheet($k_parent)->mergeCells($index[($k1 + 1)]. ''. (19 + $num_boq_tb) . ':' . 'M'. (19 + $num_boq_tb) );
                                $spreadSheet->getSheet($k_parent)->getCell($index[($k1 + 1)]. ''. (29 + $num_boq_tb) )->setValue('PT. TELKOM AKSES');
                                // $spreadSheet->getSheet($k_parent)->mergeCells($index[($k1 + 1)]. ''. (34 + $num_boq_tb) . ':' . 'M'. (34 + $num_boq_tb) );
                                $spreadSheet->getSheet($k_parent)->getCell($index[($k1 + 1)]. ''. (34 + $num_boq_tb) )->setValue('=MASTER!A54');
                                // $spreadSheet->getSheet($k_parent)->mergeCells($index[($k1 + 1)]. ''. (35 + $num_boq_tb) . ':' . 'M'. (35 + $num_boq_tb) );
                                $spreadSheet->getSheet($k_parent)->getCell($index[($k1 + 1)]. ''. (35 + $num_boq_tb) )->setValue('=MASTER!B54');
                            }
                        }
                    }

                    if($val == 'Lamp BA Rekon')
                    {
                        $num_lam_ba_rek = 0;

                        if($data_bq['rekon'])
                        {
                            $spreadSheet->getSheet($k_parent)->insertNewColumnBefore('G', count($data_bq['rekon']) * 4);
                            $spreadSheet->getSheet($k_parent)->insertNewRowBefore(12, count($data_bq['rekon'][0]['list']) );

                            foreach($data_bq['rekon'][0]['list'] as $kk => $vv)
                            {
                                $num_lam_ba_rek = ++$kk;
                                $spreadSheet->getSheet($k_parent)->getCell('A'. (11 + $num_lam_ba_rek) )->setValue($num_lam_ba_rek);
                                $spreadSheet->getSheet($k_parent)->getCell('B'. (11 + $num_lam_ba_rek) )->setValue($vv['designator']);
                                $spreadSheet->getSheet($k_parent)->getColumnDimension('B')->setAutoSize(true);
                                $spreadSheet->getSheet($k_parent)->getStyle('C'. (11 + $num_lam_ba_rek) )->getAlignment()->setWrapText(true);
                                $spreadSheet->getSheet($k_parent)->getRowDimension( (11 + $num_lam_ba_rek) )->setRowHeight(-1);
                                $spreadSheet->getSheet($k_parent)->getCell('C'. (11 + $num_lam_ba_rek) )->setValue($vv['uraian']);
                                $spreadSheet->getSheet($k_parent)->getCell('D'. (11 + $num_lam_ba_rek) )->setValue($vv['satuan']);
                                $spreadSheet->getSheet($k_parent)->getCell('E'. (11 + $num_lam_ba_rek) )->setValue($vv['material'] != 0 ? $vv['material'] : '-');
                                $spreadSheet->getSheet($k_parent)->getCell('F'. (11 + $num_lam_ba_rek) )->setValue($vv['jasa'] != 0 ? $vv['jasa'] : '-');
                            }

                            $num_lam_ba_rek = 0;
                            $tot_all = [];

                            foreach($data_bq['rekon'] as $k1 => $v2)
                            {
                                // $spreadSheet->getSheet($k_parent)->mergeCells($index[($k1 * 4) + 0].''. 8 . ':'. $index[($k1 * 4) + 3].''. 8);
                                $spreadSheet->getSheet($k_parent)->getCell($index[$k1 * 4].''. 8)->setValue($v2['sto']);
                                $spreadSheet->getSheet($k_parent)->getCell($index[$k1 * 4].''. 9)->setValue($v2['lokasi']);
                                $spreadSheet->getSheet($k_parent)->getCell($index[$k1 * 4].''. 10)->setValue($v2['pid_sto_lok']);
                                $spreadSheet->getSheet($k_parent)->getCell($index[($k1 * 4) + 0].''. 11)->setValue('SP');
                                $spreadSheet->getSheet($k_parent)->getCell($index[($k1 * 4) + 1].''. 11)->setValue('Rekon');
                                $spreadSheet->getSheet($k_parent)->getCell($index[($k1 * 4) + 2].''. 11)->setValue('Tambah');
                                $spreadSheet->getSheet($k_parent)->getCell($index[($k1 * 4) + 3].''. 11)->setValue('Kurang');

                                $spreadSheet->getSheet($k_parent)->mergeCells($index[$k1 * 4].''. 8 . ':'. $index[($k1 * 4) + 3].''. 8 );
                                $spreadSheet->getSheet($k_parent)->mergeCells($index[$k1 * 4].''. 9 . ':'. $index[($k1 * 4) + 3].''. 9 );
                                $spreadSheet->getSheet($k_parent)->mergeCells($index[$k1 * 4].''. 10 . ':'. $index[($k1 * 4) + 3].''. 10 );

                                $spreadSheet->getSheet($k_parent)->getStyle($index[$k1 * 4].''. 8 .':'. $index[($k1 * 4) + 3].''. 10)->applyFromArray($border_Style);
                                $spreadSheet->getSheet($k_parent)->getStyle($index[$k1 * 4].''. 8 .':'. $index[($k1 * 4) + 3].''. 10)->applyFromArray([
                                    'fill' => [
                                        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                                        'startColor' => [
                                            'argb' => 'ffff00',
                                        ],
                                    ]
                                ]);

                                $spreadSheet->getSheet($k_parent)->getStyle($index[$k1 * 4].''. 10)->getAlignment()->setWrapText(true);

                                // dd($v2['list']);
                                $v2['list'] = array_values($v2['list']);

                                foreach($v2['list'] as $k2 => $v3)
                                {
                                    $num_lam_ba_rek = ++$k2;

                                    $spreadSheet->getSheet($k_parent)->getCell($index[($k1 * 4) + 0].''. (11 + $num_lam_ba_rek) )->setValue($v3['sp'] != 0 ? $v3['sp'] : '-');
                                    $spreadSheet->getSheet($k_parent)->getCell($index[($k1 * 4) + 1].''. (11 + $num_lam_ba_rek) )->setValue($v3['rekon'] != 0 ? $v3['rekon'] : '-');
                                    $spreadSheet->getSheet($k_parent)->getCell($index[($k1 * 4) + 2].''. (11 + $num_lam_ba_rek) )->setValue($v3['tambah'] != 0 ? $v3['tambah'] : '-');
                                    $spreadSheet->getSheet($k_parent)->getCell($index[($k1 * 4) + 3].''. (11 + $num_lam_ba_rek) )->setValue($v3['kurang'] != 0 ? $v3['kurang'] : '-');

                                    if(!isset($tot_gd[$k1]) )
                                    {
                                        $tot_gd[$k1]['material_rekon'] = 0;
                                        $tot_gd[$k1]['jasa_rekon'] = 0;

                                        $tot_gd[$k1]['material_sp'] = 0;
                                        $tot_gd[$k1]['jasa_sp'] = 0;

                                        $tot_gd[$k1]['material_tambah'] = 0;
                                        $tot_gd[$k1]['jasa_tambah'] = 0;

                                        $tot_gd[$k1]['material_kurang'] = 0;
                                        $tot_gd[$k1]['jasa_kurang'] = 0;
                                    }

                                    $tot_gd[$k1]['material_rekon'] += $v3['rekon'] * $v3['material'];
                                    $tot_gd[$k1]['jasa_rekon'] += $v3['rekon'] * $v3['jasa'];

                                    $tot_gd[$k1]['material_sp'] += $v3['sp'] * $v3['material'];
                                    $tot_gd[$k1]['jasa_sp'] += $v3['sp'] * $v3['jasa'];

                                    $tot_gd[$k1]['material_tambah'] += $v3['tambah'] * $v3['material'];
                                    $tot_gd[$k1]['jasa_tambah'] += $v3['tambah'] * $v3['jasa'];

                                    $tot_gd[$k1]['material_kurang'] += $v3['kurang'] * $v3['material'];
                                    $tot_gd[$k1]['jasa_kurang'] += $v3['kurang'] * $v3['jasa'];

                                    if(!isset($tot_all[11 + $num_lam_ba_rek]) )
                                    {
                                        $tot_all[11 + $num_lam_ba_rek]['material_sp'] = 0;
                                        $tot_all[11 + $num_lam_ba_rek]['material_rekon'] = 0;

                                        $tot_all[11 + $num_lam_ba_rek]['tambah'] = 0;
                                        $tot_all[11 + $num_lam_ba_rek]['kurang'] = 0;
                                    }

                                    $tot_all[11 + $num_lam_ba_rek]['material_sp'] += $v3['sp'];
                                    $tot_all[11 + $num_lam_ba_rek]['material_rekon'] += $v3['rekon'];

                                    $tot_all[11 + $num_lam_ba_rek]['tambah'] += $v3['tambah'];
                                    $tot_all[11 + $num_lam_ba_rek]['kurang'] += $v3['kurang'];

                                }

                                if(isset($tot_gd) )
                                {
                                    $spreadSheet->getSheet($k_parent)->getCell($index[($k1 * 4) + 0]. (12 + $num_lam_ba_rek) )->setValue($tot_gd[$k1]['material_sp'] != 0 ? $tot_gd[$k1]['material_sp'] : '-');
                                    $spreadSheet->getSheet($k_parent)->getCell($index[($k1 * 4) + 0]. (13 + $num_lam_ba_rek) )->setValue($tot_gd[$k1]['jasa_sp'] != 0 ? $tot_gd[$k1]['jasa_sp'] : '-');

                                    $total_gd_sp = $tot_gd[$k1]['jasa_sp'] + $tot_gd[$k1]['material_sp'];
                                    $pph_gd_sp = $AdminModel->convert_floor_ceil($this->data->tgl_ba_rekon, $total_gd_sp * $pph_sp);

                                    $spreadSheet->getSheet($k_parent)->getCell($index[($k1 * 4) + 0]. (14 + $num_lam_ba_rek) )->setValue($total_gd_sp != 0 ? $total_gd_sp : '-');

                                    $spreadSheet->getSheet($k_parent)->getCell($index[($k1 * 4) + 0]. (15 + $num_lam_ba_rek) )->setValue($total_gd_sp != 0 ? $pph_gd_sp : '-');
                                    $spreadSheet->getSheet($k_parent)->getCell($index[($k1 * 4) + 0]. (16 + $num_lam_ba_rek) )->setValue($total_gd_sp != 0 ? $total_gd_sp + $pph_gd_sp : '-');

                                    $spreadSheet->getSheet($k_parent)->getCell($index[1 + ($k1 * 4) + 0] . (12 + $num_lam_ba_rek) )->setValue($tot_gd[$k1]['material_rekon'] != 0 ? $tot_gd[$k1]['material_rekon'] : '-');
                                    $spreadSheet->getSheet($k_parent)->getCell($index[1 + ($k1 * 4) + 0] . (13 + $num_lam_ba_rek) )->setValue($tot_gd[$k1]['jasa_rekon'] != 0 ? $tot_gd[$k1]['jasa_rekon'] : '-');

                                    $total_gd_rekon = $tot_gd[$k1]['jasa_rekon'] + $tot_gd[$k1]['material_rekon'];
                                    $pph_gd_rekon = $AdminModel->convert_floor_ceil($this->data->tgl_ba_rekon, $total_gd_rekon * $pph);

                                    $spreadSheet->getSheet($k_parent)->getCell($index[1 + ($k1 * 4) + 0] . (14 + $num_lam_ba_rek) )->setValue($total_gd_rekon != 0 ? $total_gd_rekon : '-');

                                    $spreadSheet->getSheet($k_parent)->getCell($index[1 + ($k1 * 4) + 0] . (15 + $num_lam_ba_rek) )->setValue($total_gd_rekon != 0 ? $pph_gd_rekon : '-');
                                    $spreadSheet->getSheet($k_parent)->getCell($index[1 + ($k1 * 4) + 0] . (16 + $num_lam_ba_rek) )->setValue($total_gd_rekon != 0 ? $total_gd_rekon + $pph_gd_rekon : '-');

                                    $spreadSheet->getSheet($k_parent)->getCell($index[2 + ($k1 * 4) + 0] . (12 + $num_lam_ba_rek) )->setValue($tot_gd[$k1]['material_tambah'] != 0 ? $tot_gd[$k1]['material_tambah'] : '-');
                                    $spreadSheet->getSheet($k_parent)->getCell($index[2 + ($k1 * 4) + 0] . (13 + $num_lam_ba_rek) )->setValue($tot_gd[$k1]['jasa_tambah'] != 0 ? $tot_gd[$k1]['jasa_tambah'] : '-');

                                    $total_gd_tambah = $tot_gd[$k1]['jasa_tambah'] + $tot_gd[$k1]['material_tambah'];
                                    $pph_gd_tambah = $AdminModel->convert_floor_ceil($this->data->tgl_ba_rekon, $total_gd_tambah * $pph);

                                    $spreadSheet->getSheet($k_parent)->getCell($index[2 + ($k1 * 4) + 0] . (14 + $num_lam_ba_rek) )->setValue($total_gd_tambah != 0 ? $total_gd_tambah : '-');

                                    $spreadSheet->getSheet($k_parent)->getCell($index[2 + ($k1 * 4) + 0] . (15 + $num_lam_ba_rek) )->setValue($total_gd_tambah != 0 ? $pph_gd_tambah : '-');
                                    $spreadSheet->getSheet($k_parent)->getCell($index[2 + ($k1 * 4) + 0] . (16 + $num_lam_ba_rek) )->setValue($total_gd_tambah != 0 ? $total_gd_tambah + $pph_gd_tambah : '-');

                                    $spreadSheet->getSheet($k_parent)->getCell($index[3 + ($k1 * 4) + 0] . (12 + $num_lam_ba_rek) )->setValue($tot_gd[$k1]['material_kurang'] != 0 ? $tot_gd[$k1]['material_kurang'] : '-');
                                    $spreadSheet->getSheet($k_parent)->getCell($index[3 + ($k1 * 4) + 0] . (13 + $num_lam_ba_rek) )->setValue($tot_gd[$k1]['jasa_kurang'] != 0 ? $tot_gd[$k1]['jasa_kurang'] : '-');

                                    $total_gd_kurang = $tot_gd[$k1]['jasa_kurang'] + $tot_gd[$k1]['material_kurang'];
                                    $pph_gd_kurang = $AdminModel->convert_floor_ceil($this->data->tgl_ba_rekon, $total_gd_kurang * $pph);

                                    $spreadSheet->getSheet($k_parent)->getCell($index[3 + ($k1 * 4) + 0] . (14 + $num_lam_ba_rek) )->setValue($total_gd_kurang != 0 ? $total_gd_kurang : '-');

                                    $spreadSheet->getSheet($k_parent)->getCell($index[3 + ($k1 * 4) + 0] . (15 + $num_lam_ba_rek) )->setValue($total_gd_kurang != 0 ? $pph_gd_kurang : '-');
                                    $spreadSheet->getSheet($k_parent)->getCell($index[3 + ($k1 * 4) + 0] . (16 + $num_lam_ba_rek) )->setValue($total_gd_kurang != 0 ? $total_gd_kurang + $pph_gd_kurang : '-');

                                    $spreadSheet->getSheet($k_parent)->getStyle($index[($k1 * 4) + 0]. (11 + $num_lam_ba_rek) .':'. $index[3 + ($k1 * 4) + 0] . (16 + $num_lam_ba_rek) )->applyFromArray($border_Style);
                                }

                            }

                            foreach($tot_all as $key => $val)
                            {
                                $spreadSheet->getSheet($k_parent)->getCell($index[( (count($data_bq['rekon']) * 4) + 0)].''. $key )->setValue($val['material_sp'] != 0 ? $val['material_sp'] : '-');
                                $spreadSheet->getSheet($k_parent)->getCell($index[( (count($data_bq['rekon']) * 4) + 1)].''. $key )->setValue($val['material_rekon'] != 0 ? $val['material_rekon'] : '-');
                                $spreadSheet->getSheet($k_parent)->getCell($index[( (count($data_bq['rekon']) * 4) + 2)].''. $key )->setValue($val['tambah'] != 0 ? $val['tambah'] : '-');
                                $spreadSheet->getSheet($k_parent)->getCell($index[( (count($data_bq['rekon']) * 4) + 3)].''. $key )->setValue($val['kurang'] != 0 ? $val['kurang'] : '-');
                            }

                            $spreadSheet->getSheet($k_parent)->getCell($index[( (count($data_bq['rekon']) * 4) + 0)].''. (12 + $num_lam_ba_rek) )->setValue($data_tot['rekon']['total']['material_sp'] != 0 ? $data_tot['rekon']['total']['material_sp'] : '-');
                            $spreadSheet->getSheet($k_parent)->getCell($index[( (count($data_bq['rekon']) * 4) + 0)].''. (13 + $num_lam_ba_rek) )->setValue($data_tot['rekon']['total']['jasa_sp'] != 0 ? $data_tot['rekon']['total']['jasa_sp'] : '-');

                            $gd_tot_sp = $data_tot['rekon']['total']['jasa_sp'] + $data_tot['rekon']['total']['material_sp'];
                            $spreadSheet->getSheet($k_parent)->getCell($index[( (count($data_bq['rekon']) * 4) + 0)].''. (14 + $num_lam_ba_rek) )->setValue($gd_tot_sp != 0 ? $gd_tot_sp : '-');

                            $pph_gd_tot_sp = $AdminModel->convert_floor_ceil($this->data->tgl_ba_rekon, $gd_tot_sp * $pph);
                            $spreadSheet->getSheet($k_parent)->getCell($index[( (count($data_bq['rekon']) * 4) + 0)].''. (15 + $num_lam_ba_rek) )->setValue($gd_tot_sp != 0 ? $pph_gd_tot_sp : '-');
                            $spreadSheet->getSheet($k_parent)->getCell($index[( (count($data_bq['rekon']) * 4) + 0)].''. (16 + $num_lam_ba_rek) )->setValue($gd_tot_sp != 0 ? $gd_tot_sp + $pph_gd_tot_sp : '-');

                            $spreadSheet->getSheet($k_parent)->getCell($index[( (count($data_bq['rekon']) * 4) + 1)].''. (12 + $num_lam_ba_rek) )->setValue($data_tot['rekon']['total']['material_rekon'] != 0 ? $data_tot['rekon']['total']['material_rekon'] : '-');
                            $spreadSheet->getSheet($k_parent)->getCell($index[( (count($data_bq['rekon']) * 4) + 1)].''. (13 + $num_lam_ba_rek) )->setValue($data_tot['rekon']['total']['jasa_rekon'] != 0 ? $data_tot['rekon']['total']['jasa_rekon'] : '-');

                            $gd_tot_rekon = $data_tot['rekon']['total']['jasa_rekon'] + $data_tot['rekon']['total']['material_rekon'];
                            $spreadSheet->getSheet($k_parent)->getCell($index[( (count($data_bq['rekon']) * 4) + 1)].''. (14 + $num_lam_ba_rek) )->setValue($gd_tot_rekon != 0 ? $gd_tot_rekon : '-');

                            $pph_gd_tot_rekon = $AdminModel->convert_floor_ceil($this->data->tgl_ba_rekon, $gd_tot_rekon * $pph);
                            $spreadSheet->getSheet($k_parent)->getCell($index[( (count($data_bq['rekon']) * 4) + 1)].''. (15 + $num_lam_ba_rek) )->setValue($gd_tot_rekon != 0 ? $pph_gd_tot_rekon : '-');
                            $spreadSheet->getSheet($k_parent)->getCell($index[( (count($data_bq['rekon']) * 4) + 1)].''. (16 + $num_lam_ba_rek) )->setValue($gd_tot_rekon != 0 ? $gd_tot_rekon + $pph_gd_tot_rekon : '-');

                            $spreadSheet->getSheet($k_parent)->getCell($index[( (count($data_bq['rekon']) * 4) + 2)].''. (12 + $num_lam_ba_rek) )->setValue($data_tot['rekon']['total']['material_tambah'] != 0 ? $data_tot['rekon']['total']['material_tambah'] : '-');
                            $spreadSheet->getSheet($k_parent)->getCell($index[( (count($data_bq['rekon']) * 4) + 2)].''. (13 + $num_lam_ba_rek) )->setValue($data_tot['rekon']['total']['jasa_tambah'] != 0 ? $data_tot['rekon']['total']['jasa_tambah'] : '-');

                            $gd_tot_tambah = $data_tot['rekon']['total']['jasa_tambah'] + $data_tot['rekon']['total']['material_tambah'];
                            $spreadSheet->getSheet($k_parent)->getCell($index[( (count($data_bq['rekon']) * 4) + 2)].''. (14 + $num_lam_ba_rek) )->setValue($gd_tot_tambah != 0 ? $gd_tot_tambah : '-');

                            $pph_gd_tot_tambah = $AdminModel->convert_floor_ceil($this->data->tgl_ba_rekon, $gd_tot_tambah * $pph);
                            $spreadSheet->getSheet($k_parent)->getCell($index[( (count($data_bq['rekon']) * 4) + 2)].''. (15 + $num_lam_ba_rek) )->setValue($gd_tot_tambah != 0 ? $pph_gd_tot_tambah : '-');
                            $spreadSheet->getSheet($k_parent)->getCell($index[( (count($data_bq['rekon']) * 4) + 2)].''. (16 + $num_lam_ba_rek) )->setValue($gd_tot_tambah != 0 ? $gd_tot_tambah + $pph_gd_tot_tambah : '-');

                            $spreadSheet->getSheet($k_parent)->getCell($index[( (count($data_bq['rekon']) * 4) + 3)].''. (12 + $num_lam_ba_rek) )->setValue($data_tot['rekon']['total']['material_kurang'] != 0 ? $data_tot['rekon']['total']['material_kurang'] : '-');
                            $spreadSheet->getSheet($k_parent)->getCell($index[( (count($data_bq['rekon']) * 4) + 3)].''. (13 + $num_lam_ba_rek) )->setValue($data_tot['rekon']['total']['jasa_kurang'] != 0 ? $data_tot['rekon']['total']['jasa_kurang'] : '-');

                            $gd_tot_krg = $data_tot['rekon']['total']['jasa_kurang'] + $data_tot['rekon']['total']['material_kurang'];
                            $pph_gd_tot_tambah = $AdminModel->convert_floor_ceil($this->data->tgl_ba_rekon, $gd_tot_krg * $pph);

                            $spreadSheet->getSheet($k_parent)->getCell($index[( (count($data_bq['rekon']) * 4) + 3)].''. (14 + $num_lam_ba_rek) )->setValue($gd_tot_krg != 0 ? $gd_tot_krg : '-');
                            $spreadSheet->getSheet($k_parent)->getCell($index[( (count($data_bq['rekon']) * 4) + 3)].''. (15 + $num_lam_ba_rek) )->setValue($gd_tot_krg != 0 ? $pph_gd_tot_tambah : '-');
                            $spreadSheet->getSheet($k_parent)->getCell($index[( (count($data_bq['rekon']) * 4) + 3)].''. (16 + $num_lam_ba_rek) )->setValue($gd_tot_krg != 0 ? $gd_tot_krg + $pph_gd_tot_tambah : '-');
                        }

                        $spreadSheet->getSheet($k_parent)->getStyle('A'. (29 + $num_lam_ba_rek) . ':' . $index[( (count($data_bq['rekon']) * 4) + 3)]. ''. (36 + $num_lam_ba_rek) )->getFont()->setBold(true);
                        $spreadSheet->getSheet($k_parent)->mergeCells('A'. (29 + $num_lam_ba_rek) . ':' . $index[( (count($data_bq['rekon']) * 4) + 3)]. ''. (29 + $num_lam_ba_rek) );

                        $spreadSheet->getSheet($k_parent)->getStyle('A'. (29 + $num_lam_ba_rek) . ':' . $index[( (count($data_bq['rekon']) * 4) + 3)]. ''. (36 + $num_lam_ba_rek) )->applyFromArray([
                            'alignment' => [
                                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                            ]
                        ]);

                        if($this->data->gd_rekon <= 500000000)
                        {
                            $spreadSheet->getSheet($k_parent)->getCell('A'. (29 + $num_lam_ba_rek) )->setValue('MENGETAHUI,');

                            $spreadSheet->getSheet($k_parent)->mergeCells('A'. (35 + $num_lam_ba_rek) . ':' . $index[( (count($data_bq['rekon']) * 4) + 3)]. ''. (35 + $num_lam_ba_rek) );
                            $spreadSheet->getSheet($k_parent)->getCell('A'. (35 + $num_lam_ba_rek) )->setValue('=MASTER!D31');

                            $spreadSheet->getSheet($k_parent)->mergeCells('A'. (36 + $num_lam_ba_rek) . ':' . $index[( (count($data_bq['rekon']) * 4) + 3)]. ''. (36 + $num_lam_ba_rek) );
                            $spreadSheet->getSheet($k_parent)->getCell('A'. (36 + $num_lam_ba_rek) )->setValue('=MASTER!E31');
                        }
                        else
                        {
                            $spreadSheet->getSheet($k_parent)->getCell('A'. (29 + $num_lam_ba_rek) )->setValue('Mengetahui & Menyetujui');

                            $spreadSheet->getSheet($k_parent)->mergeCells('A'. (30 + $num_lam_ba_rek) . ':' . 'C'. (30 + $num_lam_ba_rek) );
                            $spreadSheet->getSheet($k_parent)->getCell('A'. (30 + $num_lam_ba_rek) )->setValue('PT. TELKOM AKSES');

                            $spreadSheet->getSheet($k_parent)->mergeCells('A'. (35 + $num_lam_ba_rek) . ':' . 'C'. (35 + $num_lam_ba_rek) );
                            $spreadSheet->getSheet($k_parent)->getCell('A'. (35 + $num_lam_ba_rek) )->setValue('=MASTER!D31');

                            $spreadSheet->getSheet($k_parent)->mergeCells('A'. (36 + $num_lam_ba_rek) . ':' . 'C'. (36 + $num_lam_ba_rek) );
                            $spreadSheet->getSheet($k_parent)->getCell('A'. (36 + $num_lam_ba_rek) )->setValue('=MASTER!E31');

                            $spreadSheet->getSheet($k_parent)->mergeCells($index[( (count($data_bq['rekon']) * 4) + 1)]. ''. (30 + $num_lam_ba_rek) . ':' . $index[( (count($data_bq['rekon']) * 4) + 3)] . '' . (30 + $num_lam_ba_rek) );
                            $spreadSheet->getSheet($k_parent)->getCell($index[( (count($data_bq['rekon']) * 4) + 1)]. ''. (30 + $num_lam_ba_rek) )->setValue('PT. TELKOM AKSES');

                            $spreadSheet->getSheet($k_parent)->mergeCells($index[( (count($data_bq['rekon']) * 4) + 1)]. ''. (35 + $num_lam_ba_rek) . ':' . $index[( (count($data_bq['rekon']) * 4) + 3)] . '' . (35 + $num_lam_ba_rek) );
                            $spreadSheet->getSheet($k_parent)->getCell($index[( (count($data_bq['rekon']) * 4) + 1)]. ''. (35 + $num_lam_ba_rek) )->setValue('=MASTER!A54');

                            $spreadSheet->getSheet($k_parent)->mergeCells($index[( (count($data_bq['rekon']) * 4) + 1)]. ''. (36 + $num_lam_ba_rek) . ':' . $index[( (count($data_bq['rekon']) * 4) + 3)] . '' . (36 + $num_lam_ba_rek) );
                            $spreadSheet->getSheet($k_parent)->getCell($index[( (count($data_bq['rekon']) * 4) + 1)]. ''. (36 + $num_lam_ba_rek) )->setValue('=MASTER!B54');
                        }
                    }
                }

                if(!$this->data->spp_num)
                {
                    $spreadSheet->removeSheetByIndex(
                        $spreadSheet->getIndex(
                            $spreadSheet->getSheetByName('SPP')
                        )
                    );
                }

                if(!$this->data->receipt_num)
                {
                    $spreadSheet->removeSheetByIndex(
                        $spreadSheet->getIndex(
                            $spreadSheet->getSheetByName('Kwitansi')
                        )
                    );
                }

                // if(!$this->data->invoice)
                // {
                //     $spreadSheet->removeSheetByIndex(
                //         $spreadSheet->getIndex(
                //             $spreadSheet->getSheetByName('Invoice')
                //         )
                //     );
                // }


                if($this->data->bulan_pengerjaan >= '2021-11')
                {
                    $remove_item = [
                        'AMD SP',
                        'S.Penetapan',
                        'SP',
                        'BAUT',
                        'BA ABD',
                        'AMD 2021',
                        'BAKH REKON',
                        'BA REKONSILIASI',
                        'BAST-I'
                    ];

                    if($this->data->amd_sp)
                    {
                        $remove_item = array_diff($remove_item, ['BAKH REKON', 'AMD 2021'] );
                    }

                    foreach($remove_item as $v)
                    {
                        $spreadSheet->removeSheetByIndex(
                            $spreadSheet->getIndex(
                                $spreadSheet->getSheetByName($v)
                            )
                        );
                    }
                }
                else
                {
                    $remove_item = [
                        'S.Penetapan',
                        'SP',
                        'LAMPIRAN SP',
                        'AMD 2021',
                        'BAST-I',
                        'BA REKONSILIASI',
                        'Rekap BA Rekon',
                        'BOQ',
                        'Lamp BA Rekon',
                        'BAKH SP',
                        'BAKH REKON',
                    ];

                    switch ($this->data->urutan) {
                        case 4:
                            $remove_item = array_slice($remove_item, 1);
                        break;
                        case 6:
                            $remove_item = array_slice($remove_item, 4);
                        break;
                    }

                    if($this->data->urutan >= 6)
                    {
                        $remove_item = array_diff($remove_item, ['S.Penetapan', 'LAMPIRAN SP', 'SP', 'BAKH SP'] );
                    }

                    if($this->data->urutan > 7)
                    {
                        $remove_item = array_diff($remove_item, ['BA REKONSILIASI', 'Rekap BA Rekon', 'BOQ', 'Lamp BA Rekon', 'BAKH REKON', 'BAST-I'] );
                    }

                    if($this->data->amd_sp)
                    {
                        $remove_item = array_diff($remove_item, ['AMD SP'] );
                    }

                    if($this->data2->pph)
                    {
                        $remove_item = array_diff($remove_item, ['FORM_TAGIHAN_MITRA'] );
                    }

                    if($this->data->BAUT)
                    {
                        $remove_item = array_diff($remove_item, ['BAUT'] );
                    }

                    if($this->data->ba_abd)
                    {
                        $remove_item = array_diff($remove_item, ['BA ABD'] );
                    }

                    if($remove_item)
                    {
                        foreach($remove_item as $item_remove_v)
                        {
                            $spreadSheet->removeSheetByIndex(
                                $spreadSheet->getIndex(
                                    $spreadSheet->getSheetByName($item_remove_v)
                                )
                            );
                        }
                    }
                }

                $writer = IOFactory::createWriter($spreadSheet, 'Xlsx');

                if($this->opsi == 'save')
                {
                    $path = public_path(). "/upload2/". $this->data->id .'/excel_osp_FO/';

                    if (!file_exists($path) ) {
                        if (!mkdir($path, 0770, true) ) {
                            return 'gagal menyiapkan folder sanggup';
                        }
                    }

                    $writer->save($path."Template Invoice & BA OSP FO ".$this->data->judul. ".xlsx");
                }
                else
                {
                    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                    header('Content-Disposition: attachment;filename="Template Invoice & BA OSP FOs.xlsx"');

                    $writer->save('php://output');
                    die;
                }
            },
            BeforeWriting::class => function(BeforeWriting $event) {
                $event->writer->setActiveSheetIndex(0);
            }
        ];
    }
}