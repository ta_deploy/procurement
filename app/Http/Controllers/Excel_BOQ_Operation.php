<?php

namespace App\Http\Controllers;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Events\BeforeSheet;
use Maatwebsite\Excel\Events\BeforeWriting;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Color;
use App\DA\ReportModel;
use App\DA\AdminModel;
use App\Http\Controllers\ProgressController;
use Maatwebsite\Excel\Excel;
use DB;
class Excel_BOQ_Operation implements  WithEvents
{
    use Exportable;

    public function __construct($data)
    {
        $this->id  = $data[0];
        $this->opsi = $data[1];
        $this->status = $data[2];
    }

    public function registerEvents(): array
    {
        return [
            BeforeExport::class  => function(BeforeExport $event) {
                $data = DB::Table('procurement_boq_upload As pbu')
                ->leftjoin('1_2_employee As emp', 'pbu.created_by', '=', 'emp.nik')
                ->select('pbu.*', 'emp.Witel_New')
                ->where('pbu.id', $this->id)
                ->first();

                $data_rfc = AdminModel::get_rfc_boq($this->id);
                $data_rfc = array_map(function($item) {
                    return (array)$item;
                }, $data_rfc->toArray() );

                // dd($this->data);

                $check_db = ReportModel::get_boq_data($this->id);

                $sd['plan'] = 0;

                if($check_db->urutan == 7 )
                {
                    $sd['rekon_belum_validasi'] = 2;
                }
                elseif($check_db->urutan > 7)
                {
                    $sd['rekon'] = 1;
                }

                if($this->status != 'All')
                {
                    // $sd = array_filter($sd, function($k){
                    //     return $k == $this->status;
                    // }, ARRAY_FILTER_USE_KEY);
                    $sd = [];

                    if($check_db->urutan == 7 )
                    {
                        $sd['rekon_belum_validasi'] = 2;
                    }
                    elseif($check_db->urutan > 7)
                    {
                        $sd['rekon'] = 1;
                    }
                    else
                    {
                        $sd['plan'] = 0;
                    }
                }

                $get_material_rfc = DB::table('procurement_designator As pd')
                ->leftjoin('procurement_design_rfc As pdr', 'pd.id', '=', 'pdr.id_design')
                ->select('pdr.*', 'pd.designator')
                ->Where([
                    ['pdr.pekerjaan', $check_db->pekerjaan],
                    ['pd.active', 1]
                ])
                ->get();

                $get_material_rfc = array_map(function($item) {
                    return (array)$item;
                }, $get_material_rfc->toArray() );

                $that = new ProgressController();

                foreach($sd as $title_key => $v)
                {
                    $reader = new Xlsx();
                    \PhpOffice\PhpSpreadsheet\Shared\File::setUseUploadTempDirectory(true);

                    $spreadSheet = $reader->load(public_path().'/template_doc/maintenance_down_2021/TEMPLATE BOQ_OPER.xlsx');

                    $security = $spreadSheet->getSecurity();

                    $spreadSheet->getActiveSheet()->getProtection()->setSheet(true);
                    $spreadSheet->getActiveSheet()->getProtection()->setInsertRows(true);

                    $security->setLockWindows(true);
                    $security->setLockStructure(true);

                    $security->setWorkbookPassword($that->key_data() );

                    $spreadSheet->getSheet(0)->getCell('B2')->SetValue(': '.$data->judul);
                    $spreadSheet->getSheet(0)->getCell('B3')->SetValue(": Witel ". ucwords($data->Witel_New) );

                    $sql = "pbd.status_design = $v";

                    if($title_key == 'rekon')
                    {
                        $sql = "pbd.status_design IN (0, 1)";
                    }

                    $data_boq_raw = DB::SELECT("SELECT pbu.jenis_work, pbd.*, pd.uraian, pd.satuan, pbl.sub_jenis_p, pbd.material as material_default, pbd.jasa as jasa_default, pbl.lokasi, pbl.sto, pbl.pid_sto
                    FROM procurement_boq_upload pbu
                    LEFT JOIN procurement_Boq_lokasi pbl ON pbu.id = pbl.id_upload
                    LEFT JOIN procurement_Boq_design pbd ON pbl.id = pbd.id_boq_lokasi
                    LEFT JOIN procurement_designator pd ON pd.id = pbd.id_design
                    WHERE pbu.id = ".$this->id." AND pbu.active = 1 AND pd.active = 1 AND $sql GROUP BY pbd.id ORDER BY pbl.sub_jenis_p ASC, pbl.sto ASC, pbd.status_design ASC");

                    $order_sto = explode(', ', $data->sto_pekerjaan);
                    $data_boq = [];
                    $get_jenis_kerja = DB::table('procurement_pekerjaan')->where('jenis', $data_boq_raw[0]->jenis_work)->get();

                    // foreach($get_jenis_kerja as $gjk)
                    // {
                    //     foreach($order_sto as $v)
                    //     {
                    //         $find_k = array_keys(array_column(json_decode(json_encode($data_boq_raw), 'TRUE'), 'sto'), $v);

                    //         foreach($find_k as $vv)
                    //         {
                    //             $dbr = $data_boq_raw[$vv];
                    //             if($gjk->sub_jenis == $dbr->sub_jenis_p)
                    //             {
                    //                 $data_boq[] = $dbr;
                    //             }
                    //         }
                    //     }
                    // }
                    $data_boq = $data_boq_raw;
                    // dd($data_boq_raw, $get_jenis_kerja);
                    $data_bq = $data_tot = $data_rekap_rekon = $data_pid = $pid_all = [];
                    $tot_mat_sp = $tot_jasa_sp = $tot_mat_rek = $tot_jasa_rek = 0;

                    $get_all_material = $gam = [];

                    foreach($data_boq as $key => $val)
                    {
                        $gam[$val->id_design .'-'. $val->jenis_khs .'-'. $val->material .'-'. $val->jasa]['id_design']  = $val->id_design;
                        $gam[$val->id_design .'-'. $val->jenis_khs .'-'. $val->material .'-'. $val->jasa]['jenis_khs']  = $val->jenis_khs;
                        $gam[$val->id_design .'-'. $val->jenis_khs .'-'. $val->material .'-'. $val->jasa]['designator'] = $val->designator;
                        $gam[$val->id_design .'-'. $val->jenis_khs .'-'. $val->material .'-'. $val->jasa]['uraian']     = html_entity_decode($val->uraian, ENT_QUOTES, 'UTF-8');
                        $gam[$val->id_design .'-'. $val->jenis_khs .'-'. $val->material .'-'. $val->jasa]['satuan']     = $val->satuan;
                        $gam[$val->id_design .'-'. $val->jenis_khs .'-'. $val->material .'-'. $val->jasa]['material']   = intval($val->material);
                        $gam[$val->id_design .'-'. $val->jenis_khs .'-'. $val->material .'-'. $val->jasa]['jasa']       = intval($val->jasa);
                        $gam[$val->id_design .'-'. $val->jenis_khs .'-'. $val->material .'-'. $val->jasa]['sp']         = 0;
                        $gam[$val->id_design .'-'. $val->jenis_khs .'-'. $val->material .'-'. $val->jasa]['rekon']      = 0;
                        $gam[$val->id_design .'-'. $val->jenis_khs .'-'. $val->material .'-'. $val->jasa]['tambah']     = 0;
                        $gam[$val->id_design .'-'. $val->jenis_khs .'-'. $val->material .'-'. $val->jasa]['kurang']     = 0;
                    }

                    foreach($data_boq as $key => $val)
                    {
                        if(!isset($data_tot['total']) )
                        {
                            $data_tot['total']['material_sp']    = 0;
                            $data_tot['total']['jasa_sp']        = 0;
                            $data_tot['total']['material_rekon'] = 0;
                            $data_tot['total']['jasa_rekon']     = 0;
                            $data_tot['total']['material_tmbh']  = 0;
                            $data_tot['total']['material_krg']   = 0;
                            $data_tot['total']['jasa_tmbh']      = 0;
                            $data_tot['total']['jasa_krg']       = 0;
                        }

                        if(!$data_rekap_rekon)
                        {
                            $data_rekap_rekon = $gam;
                        }

                        if($data_rekap_rekon[$val->id_design .'-'. $val->jenis_khs .'-'. $val->material .'-'. $val->jasa])
                        {
                            $data_rekap_rekon[$val->id_design .'-'. $val->jenis_khs .'-'. $val->material .'-'. $val->jasa]['sp']     += intval($val->sp);
                            $data_rekap_rekon[$val->id_design .'-'. $val->jenis_khs .'-'. $val->material .'-'. $val->jasa]['rekon']  += intval($val->rekon);
                            $data_rekap_rekon[$val->id_design .'-'. $val->jenis_khs .'-'. $val->material .'-'. $val->jasa]['tambah'] += intval($val->rekon) > intval($val->sp) ? intval($val->rekon) - intval($val->sp) : 0;
                            $data_rekap_rekon[$val->id_design .'-'. $val->jenis_khs .'-'. $val->material .'-'. $val->jasa]['kurang'] += $title_key != 'plan' && intval($val->rekon) < intval($val->sp) ? intval($val->sp) - intval($val->rekon) : 0;
                        }

                        $data_pid[$val->id_boq_lokasi][$val->pid_sto] = $val->pid_sto;

                        $data_bq[$val->id_boq_lokasi]['total']['material_sp']    = $tot_mat_sp   += intval($val->material) * intval($val->sp);
                        $data_bq[$val->id_boq_lokasi]['total']['jasa_sp']        = $tot_jasa_sp  += intval($val->jasa) * intval($val->sp);
                        $data_bq[$val->id_boq_lokasi]['total']['material_rekon'] = $tot_mat_rek  += intval($val->material) * intval($val->rekon);
                        $data_bq[$val->id_boq_lokasi]['total']['jasa_rekon']     = $tot_jasa_rek += intval($val->jasa) * intval($val->rekon);

                        $split_pid_all = explode(', ', $val->pid_sto);
                        $result_pid_lok = [];

                        foreach($split_pid_all as $spa)
                        {
                            $split_pid = explode('-', $spa);
                            $result_pid_lok[@$split_pid[0] .'-'. @$split_pid[1]] = @$split_pid[0] .'-'. @$split_pid[1];
                        }

                        $data_bq[$val->id_boq_lokasi]['pid_sto'] = implode(', ', $result_pid_lok);

                        $data_bq[$val->id_boq_lokasi]['pid'] = $val->pid_sto;
                        $data_bq[$val->id_boq_lokasi]['sto'] = $val->sto;
                        $data_bq[$val->id_boq_lokasi]['lokasi'] = $val->lokasi;
                        $data_bq[$val->id_boq_lokasi]['sub_jenis_p'] = $val->sub_jenis_p;

                        if(!isset($data_bq[$val->id_boq_lokasi]['list']) )
                        {
                            $data_bq[$val->id_boq_lokasi]['list']  = $gam;
                        }

                        if($data_bq[$val->id_boq_lokasi]['list'][$val->id_design .'-'. $val->jenis_khs .'-'. $val->material .'-'. $val->jasa])
                        {
                            $data_bq[$val->id_boq_lokasi]['list'][$val->id_design .'-'. $val->jenis_khs .'-'. $val->material .'-'. $val->jasa]['sp']     = intval($val->sp);
                            $data_bq[$val->id_boq_lokasi]['list'][$val->id_design .'-'. $val->jenis_khs .'-'. $val->material .'-'. $val->jasa]['rekon']  = intval($val->rekon);
                            $data_bq[$val->id_boq_lokasi]['list'][$val->id_design .'-'. $val->jenis_khs .'-'. $val->material .'-'. $val->jasa]['tambah'] = intval($val->rekon) > intval($val->sp) ? intval($val->rekon) - intval($val->sp) : 0;
                            $data_bq[$val->id_boq_lokasi]['list'][$val->id_design .'-'. $val->jenis_khs .'-'. $val->material .'-'. $val->jasa]['kurang'] = $title_key != 'plan' && intval($val->rekon) < intval($val->sp) ? intval($val->sp) - intval($val->rekon) : 0;
                        }
                    }

                    foreach($data_bq as $k => $v)
                    {
                        $data_bq[$k]['list'] = array_values($data_bq[$k]['list']);
                    }

                    foreach($data_bq as $k => $v)
                    {
                        foreach($data_bq[$k]['list'] as $kk => $vv)
                        {
                            $data_tot['total']['material_sp']    += intval($vv['material']) * intval($vv['sp']);
                            $data_tot['total']['jasa_sp']        += intval($vv['jasa']) * intval($vv['sp']);
                            $data_tot['total']['material_rekon'] += intval($vv['material']) * intval($vv['rekon']);
                            $data_tot['total']['jasa_rekon']     += intval($vv['jasa']) * intval($vv['rekon']);
                            $data_tot['total']['material_tmbh']  += intval($vv['material']) * intval($vv['tambah']);
                            $data_tot['total']['material_krg']   += intval($vv['material']) * intval($vv['kurang']);
                            $data_tot['total']['jasa_tmbh']      += intval($vv['jasa']) * intval($vv['tambah']);
                            $data_tot['total']['jasa_krg']       += intval($vv['jasa']) * intval($vv['kurang']);
                        }
                    }

                    $data_rekap_rekon = array_values($data_rekap_rekon);
                    // dd($data_rekap_rekon, $data_bq);
                    $data_bq = array_values($data_bq);
                    $pid_all = array_reduce($data_pid, 'array_merge', array() );
                    $pid_all = implode(', ', $pid_all);

                    if($title_key == 'rekon')
                    {
                        $jml_klm = 7 * count($data_bq);
                    }
                    else
                    {
                        $jml_klm = 4 * count($data_bq);
                    }

                    $charl_load = $klm_excel = [];

                    for ($i = 'J'; $i !== 'ZZ'; $i++){
                        $charl_load[] = $i;
                    }

                    for ($a = 1; $a <= $jml_klm; $a++){
                        $klm_excel[$a] = $charl_load[$a];
                    }

                    if($data_bq)
                    {
                        $spreadSheet->getSheet(0)->insertNewRowBefore(10, count($data_bq[0]['list']) );
                        $spreadSheet->getSheet(0)->getCell('G5')->setValue($pid_all);
                        $spreadSheet->getSheet(0)->getStyle('G5')->getAlignment()->setWrapText(true);

                        if($title_key == 'rekon')
                        {
                            $chunk_klm = array_chunk($klm_excel, 7);
                        }
                        else
                        {
                            $chunk_klm = array_chunk($klm_excel, 4);
                        }

                        $style_center = array(
                            'alignment' => array(
                                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                            )
                        );

                        $total_per_lok = $total_nilai = [];

                        foreach($data_bq as $k1 => $v2)
                        {
                            // dd($v2);
                            $v2['list'] = array_values($v2['list']);

                            foreach($v2['list'] as $k2 => $v3)
                            {
                                $num_lamp = ++$k2;
                                $spreadSheet->getSheet(0)->getCell('A'. (9 + $num_lamp) )->setValue($num_lamp);
                                $spreadSheet->getSheet(0)->getCell('B'. (9 + $num_lamp) )->setValue($v3['designator']);
                                $spreadSheet->getSheet(0)->getCell('C'. (9 + $num_lamp) )->setValue($v3['uraian']);
                                $spreadSheet->getSheet(0)->getCell('D'. (9 + $num_lamp) )->setValue($v3['satuan']);
                                $spreadSheet->getSheet(0)->getCell('E'. (9 + $num_lamp) )->setValue($v3['material'] != 0 ? $v3['material'] : '-');
                                $spreadSheet->getSheet(0)->getCell('F'. (9 + $num_lamp) )->setValue($v3['jasa'] != 0 ? $v3['jasa'] : '-');
                                $no = 0;

                                foreach($chunk_klm as $kk2 => $vv2)
                                {
                                    if($k1 == $kk2)
                                    {
                                        $spreadSheet->getSheet(0)->getCell($vv2[0] .'5')->setValue($v2['pid_sto']);
                                        $spreadSheet->getSheet(0)->mergeCells($vv2[0] .'5:' . $vv2[3] .'5');

                                        $spreadSheet->getSheet(0)->getCell($vv2[0] .'6')->setValue($v2['sto']);
                                        $spreadSheet->getSheet(0)->mergeCells($vv2[0] .'6:' . $vv2[3] .'6');

                                        $spreadSheet->getSheet(0)->getCell($vv2[0] .'7')->setValue($v2['lokasi'] .' ('. $v2['sub_jenis_p'] . ')');
                                        $spreadSheet->getSheet(0)->mergeCells($vv2[0] .'7:' . $vv2[3] .'8');

                                        $spreadSheet->getSheet(0)->getStyle($vv2[0] . '5:' . $vv2[3] .'8' )->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('FFFF00');
                                        $spreadSheet->getSheet(0)->getStyle($vv2[0] .'5:' . $vv2[3] .'8')->applyFromArray($style_center);

                                        $spreadSheet->getSheet(0)->getCell($vv2[0] .'9')->setValue('SP');
                                        $spreadSheet->getSheet(0)->getCell($vv2[1] .'9')->setValue('REKON');
                                        $spreadSheet->getSheet(0)->getCell($vv2[2] .'9')->setValue('TAMBAH');
                                        $spreadSheet->getSheet(0)->getCell($vv2[3] .'9')->setValue('KURANG');
                                        $spreadSheet->getSheet(0)->getStyle($vv2[0] .'9:' . $vv2[3] .'9')->applyFromArray($style_center);
                                        $spreadSheet->getSheet(0)->getStyle($vv2[0] . '9:' . $vv2[3] .'9' )->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('FF0000');


                                        $spreadSheet->getSheet(0)->getCell($vv2[0] .''. (9 + $num_lamp) )->setValue($v3['sp'] != 0 ? $v3['sp'] : '-');
                                        $spreadSheet->getSheet(0)->getCell($vv2[1] .''. (9 + $num_lamp) )->setValue($v3['rekon'] != 0 ? $v3['rekon'] : '-');
                                        $spreadSheet->getSheet(0)->getCell($vv2[2] .''. (9 + $num_lamp) )->setValue($v3['tambah'] != 0 ? $v3['tambah'] : '-');
                                        $spreadSheet->getSheet(0)->getCell($vv2[3] .''. (9 + $num_lamp) )->setValue($v3['kurang'] != 0 ? $v3['kurang'] : '-');

                                        for ($a = 1; $a <= 3; $a++)
                                        {
                                            if(!isset($total_per_lok[$a][$kk2][$vv2[0] ]) )
                                            {
                                                $total_per_lok[$a][$kk2][$vv2[0] ] = 0;
                                                $total_per_lok[$a][$kk2][$vv2[1] ] = 0;
                                                $total_per_lok[$a][$kk2][$vv2[2] ] = 0;
                                                $total_per_lok[$a][$kk2][$vv2[3] ] = 0;
                                            }

                                            if($a == 1)
                                            {
                                                $total_per_lok[$a][$kk2][$vv2[0] ] += $v3['sp'] * $v3['material'];
                                                $total_per_lok[$a][$kk2][$vv2[1] ] += $v3['rekon'] * $v3['material'];
                                                $total_per_lok[$a][$kk2][$vv2[2] ] += $v3['tambah'] * $v3['material'];
                                                $total_per_lok[$a][$kk2][$vv2[3] ] += $v3['kurang'] * $v3['material'];
                                            }

                                            if($a == 2)
                                            {
                                                $total_per_lok[$a][$kk2][$vv2[0] ] += $v3['sp'] * $v3['jasa'];
                                                $total_per_lok[$a][$kk2][$vv2[1] ] += $v3['rekon'] * $v3['jasa'];
                                                $total_per_lok[$a][$kk2][$vv2[2] ] += $v3['tambah'] * $v3['jasa'];
                                                $total_per_lok[$a][$kk2][$vv2[3] ] += $v3['kurang'] * $v3['jasa'];
                                            }

                                            if($a == 3)
                                            {
                                                $total_per_lok[$a][$kk2][$vv2[0] ] += ($v3['sp'] * $v3['material']) + ($v3['sp'] * $v3['jasa']);
                                                $total_per_lok[$a][$kk2][$vv2[1] ] += ($v3['rekon'] * $v3['material']) + ($v3['rekon'] * $v3['jasa']);
                                                $total_per_lok[$a][$kk2][$vv2[2] ] += ($v3['tambah'] * $v3['material']) + ($v3['tambah'] * $v3['jasa']);
                                                $total_per_lok[$a][$kk2][$vv2[3] ] += ($v3['kurang'] * $v3['material']) + ($v3['kurang'] * $v3['jasa']);
                                            }
                                        }

                                        if($title_key == 'rekon')
                                        {
                                            $spreadSheet->getSheet(0)->getCell($vv2[4] .'5')->setValue('Material');
                                            $spreadSheet->getSheet(0)->mergeCells($vv2[4] .'5:' . $vv2[6] .'8');

                                            $spreadSheet->getSheet(0)->getCell($vv2[4] .'9')->setValue('BON MATERIAL');
                                            $spreadSheet->getSheet(0)->getCell($vv2[5] .'9')->setValue('NO RFC');
                                            $spreadSheet->getSheet(0)->getCell($vv2[6] .'9')->setValue('NO RFR');

                                            $spreadSheet->getSheet(0)->getStyle($vv2[4] . '5:' . $vv2[6] .'9' )->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('FF0000');
                                            $spreadSheet->getSheet(0)->getStyle($vv2[4] .'5:' . $vv2[6] .'9')->applyFromArray($style_center);

                                            $find_k_pbl = array_keys(array_column($data_rfc, 'wbs_element'), $v2['pid_sto']);
                                            $data_pbl_rfc_out = $data_pbl_rfc_return = [];

                                            foreach($find_k_pbl as $key_pbl)
                                            {
                                                $collect_rfc = $data_rfc[$key_pbl];
                                                $search_k = array_search($collect_rfc['material'], array_column($get_material_rfc, 'design_rfc') );

                                                if($collect_rfc['jenis_rfc'] == 'Out')
                                                {
                                                    if($search_k !== FALSE)
                                                    {
                                                        $match_design = preg_replace('/\s+/', '', $get_material_rfc[$search_k]['designator']);
                                                    }
                                                    else
                                                    {
                                                        $match_design = preg_replace('/\s+/', '', $collect_rfc['material']);
                                                    }

                                                    $data_pbl_rfc_out[$collect_rfc['material'] ]['designator'] = $match_design;
                                                    $data_pbl_rfc_out[$collect_rfc['material'] ]['rfc'][$collect_rfc['rfc'] ] = $collect_rfc['rfc'];

                                                    if(!isset($data_pbl_rfc_out[$collect_rfc['material'] ]['qty']) )
                                                    {
                                                        $data_pbl_rfc_out[$collect_rfc['material'] ]['qty'] = 0;
                                                    }

                                                    $data_pbl_rfc_out[$collect_rfc['material'] ]['qty'] += $collect_rfc['terpakai'];
                                                }
                                                else
                                                {
                                                    if($search_k !== FALSE)
                                                    {
                                                        $match_design = preg_replace('/\s+/', '', $get_material_rfc[$search_k]['designator']);
                                                    }
                                                    else
                                                    {
                                                        $match_design = preg_replace('/\s+/', '', $collect_rfc['material']);
                                                    }

                                                    $data_pbl_rfc_return[$collect_rfc['material'] ]['designator'] = $match_design;
                                                    $data_pbl_rfc_return[$collect_rfc['material'] ][$collect_rfc['rfc'] ] = $collect_rfc['rfc'];

                                                    if(!isset($data_pbl_rfc_return[$collect_rfc['material'] ]['qty']) )
                                                    {
                                                        $data_pbl_rfc_return[$collect_rfc['material'] ]['qty'] = 0;
                                                    }

                                                    $data_pbl_rfc_return[$collect_rfc['material'] ]['qty'] += $collect_rfc['terpakai'];
                                                }
                                            }

                                            $key_rfc_out = array_map(function($x){
                                                return preg_replace('/\s+/', '', $x);
                                            }, array_keys($data_pbl_rfc_out) );

                                            $key_rfc_return = array_map(function($x){
                                                return preg_replace('/\s+/', '', $x);
                                            }, array_keys($data_pbl_rfc_return) );

                                            $rfc_out_array = $rfc_return_array = [];


                                            if($v3['jenis_khs'] == 16)
                                            {
                                                $bon_material = 0;

                                                if($key_rfc_out)
                                                {
                                                    if(!in_array(preg_replace('/\s+/', '', $v3['designator']), $key_rfc_out ) )
                                                    {
                                                        $search_k = array_keys(array_column($data_pbl_rfc_out, 'designator'), $v3['designator']);

                                                        if(count($search_k) > 0)
                                                        {
                                                            $get_result_rfc_keys = array_intersect_key($key_rfc_out, array_flip($search_k) );
                                                            $get_result_rfc_data = array_intersect_key($data_pbl_rfc_out, array_flip($get_result_rfc_keys) );

                                                            foreach($get_result_rfc_data as $vrrd)
                                                            {
                                                                $bon_material += $vrrd['qty'];

                                                                foreach($vrrd['rfc'] as $vvrrd2)
                                                                {
                                                                    $rfc_out_array[$vvrrd2] = $vvrrd2;
                                                                }
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        $bon_material += $data_pbl_rfc_out[$v3['designator'] ]['qty'];

                                                        foreach($data_pbl_rfc_out[$v3['designator'] ]['rfc'] as $vrrd)
                                                        {
                                                            $rfc_out_array[$vrrd] = $vrrd;
                                                        }

                                                    }

                                                    $rfc_out_array = implode(',', $rfc_out_array);

                                                    $spreadSheet->getSheet(0)->getCell($vv2[5] .''. (9 + $num_lamp) )->setValue($rfc_out_array);

                                                    if($key_rfc_return)
                                                    {
                                                        if(!in_array(preg_replace('/\s+/', '', $v3['designator']), $key_rfc_return ) )
                                                        {
                                                            $search_k = array_keys(array_column($data_pbl_rfc_out, 'designator'), $v3['designator']);

                                                            if(count($search_k) > 0)
                                                            {
                                                                $get_result_rfc_keys = array_intersect_key($key_rfc_return, array_flip($search_k) );
                                                                $get_result_rfc_data = array_intersect_key($data_pbl_rfc_out, array_flip($get_result_rfc_keys) );

                                                                foreach($get_result_rfc_data as $vrrd)
                                                                {
                                                                    $bon_material += $vrrd['qty'];

                                                                    foreach($vrrd['rfc'] as $vvrrd2)
                                                                    {
                                                                        $rfc_return_array[$vvrrd2] = $vvrrd2;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            foreach($data_pbl_rfc_out[$v3['designator'] ]['rfc'] as $vrrd)
                                                            {
                                                                $bon_material += $data_pbl_rfc_out[$v3['designator'] ]['qty'];

                                                                $rfc_return_array[$vrrd] = $vrrd;
                                                            }
                                                        }

                                                        $rfc_return_array = implode(',', $rfc_return_array);
                                                        $spreadSheet->getSheet(0)->getCell($vv2[6] .''. (9 + $num_lamp) )->setValue($rfc_return_array);
                                                    }

                                                    $spreadSheet->getSheet(0)->getCell($vv2[4] .''. (9 + $num_lamp) )->setValue($bon_material == 0 ? '-' : $bon_material);
                                                }
                                            }
                                        }
                                    }
                                }

                                if(!isset($total_nilai[($num_lamp + 9)]) )
                                {
                                    $total_nilai[($num_lamp + 9)]['sp'] = 0;
                                    $total_nilai[($num_lamp + 9)]['rekon'] = 0;
                                    $total_nilai[($num_lamp + 9)]['tambah'] = 0;
                                    $total_nilai[($num_lamp + 9)]['kurang'] = 0;
                                }

                                $total_nilai[($num_lamp + 9)]['sp'] += $v3['sp'];
                                $total_nilai[($num_lamp + 9)]['rekon'] += $v3['rekon'];
                                $total_nilai[($num_lamp + 9)]['tambah'] += $v3['tambah'];
                                $total_nilai[($num_lamp + 9)]['kurang'] += $v3['kurang'];
                            }
                        }

                        $spreadSheet->getSheet(0)->getStyle('A10:N'. (9 + $num_lamp) )->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('ffffff');
                        $spreadSheet->getSheet(0)->getStyle('A9:F'. (9 + $num_lamp) )->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN)->setColor(new Color('000000') );

                        foreach($total_nilai as $key => $val)
                        {
                            $spreadSheet->getSheet(0)->getCell('G'. $key)->setValue($val['sp'] != 0 ? $val['sp'] : '-');
                            $spreadSheet->getSheet(0)->getCell('H'. $key)->setValue($val['rekon'] != 0 ? $val['rekon'] : '-');
                            $spreadSheet->getSheet(0)->getCell('I'. $key)->setValue($val['tambah'] != 0 ? $val['tambah'] : '-');
                            $spreadSheet->getSheet(0)->getCell('J'. $key)->setValue($val['kurang'] != 0 ? $val['kurang'] : '-');
                        }

                        $spreadSheet->getSheet(0)->getCell('G'. ($key + 1) )->setValue($data_tot['total']['material_sp'] != 0 ? $data_tot['total']['material_sp'] : '-');
                        $spreadSheet->getSheet(0)->getCell('G'. ($key + 2) )->setValue($data_tot['total']['jasa_sp'] != 0 ? $data_tot['total']['jasa_sp'] : '-');

                        $tot_sp = $data_tot['total']['jasa_sp'] + $data_tot['total']['material_sp'];

                        $spreadSheet->getSheet(0)->getCell('G'. ($key + 3) )->setValue($tot_sp != 0 ? $tot_sp : '-');
                        $spreadSheet->getSheet(0)->getStyle('G'. ($key + 3) )->getAlignment()->setWrapText(true);

                        $spreadSheet->getSheet(0)->getCell('H'. ($key + 1) )->setValue($data_tot['total']['material_rekon'] != 0 ? $data_tot['total']['material_rekon'] : '-');
                        $spreadSheet->getSheet(0)->getCell('H'. ($key + 2) )->setValue($data_tot['total']['jasa_rekon'] != 0 ? $data_tot['total']['jasa_rekon'] : '-');

                        $tot_rek = $data_tot['total']['jasa_rekon'] + $data_tot['total']['material_rekon'];

                        $spreadSheet->getSheet(0)->getCell('H'. ($key + 3) )->setValue($tot_rek != 0 ? $tot_rek : '-');
                        $spreadSheet->getSheet(0)->getStyle('H'. ($key + 3) )->getAlignment()->setWrapText(true);

                        $spreadSheet->getSheet(0)->getCell('I'. ($key + 1) )->setValue($data_tot['total']['material_tmbh'] != 0 ? $data_tot['total']['material_tmbh'] : '-');
                        $spreadSheet->getSheet(0)->getCell('I'. ($key + 2) )->setValue($data_tot['total']['jasa_tmbh'] != 0 ? $data_tot['total']['jasa_tmbh'] : '-');

                        $tot_tmbh = $data_tot['total']['jasa_tmbh'] + $data_tot['total']['material_tmbh'];

                        $spreadSheet->getSheet(0)->getCell('I'. ($key + 3) )->setValue($tot_tmbh != 0 ? $tot_tmbh : '-');

                        $spreadSheet->getSheet(0)->getCell('J'. ($key + 1) )->setValue($data_tot['total']['material_krg'] != 0 ? $data_tot['total']['material_krg'] : '-');
                        $spreadSheet->getSheet(0)->getCell('J'. ($key + 2) )->setValue($data_tot['total']['jasa_krg'] != 0 ? $data_tot['total']['jasa_krg'] : '-');

                        $tot_krg = $data_tot['total']['jasa_krg'] + $data_tot['total']['material_krg'];

                        $spreadSheet->getSheet(0)->getCell('J'. ($key + 3) )->setValue($tot_krg != 0 ? $tot_krg : '-');

                        foreach($total_per_lok as $k => $v)
                        {
                            foreach($v as $kk => $vv)
                            {
                                $no = 0;
                                foreach($vv as $kkk => $vvv)
                                {
                                    ++$no;
                                    $spreadSheet->getSheet(0)->getCell($kkk .''.($key + $k) )->setValue($vvv != 0 ? $vvv : '-');

                                    if($no % 2 == 0)
                                    {
                                        $spreadSheet->getSheet(0)->getStyle($kkk .''.($key + $k) )->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('FFFF00');
                                    }
                                    else
                                    {
                                        $spreadSheet->getSheet(0)->getStyle($kkk .''.($key + $k) )->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('A6A6A6');
                                    }
                                }
                            }
                        }

                        $klm_excel = array_values($klm_excel);

                        $awal_final = current($klm_excel);
                        $end_final = end($klm_excel);

                        $spreadSheet->getSheet(0)->getStyle($awal_final . '5:' . $end_final .''. ($key + $k) )->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN)->setColor(new Color('000000') );
                        $spreadSheet->getSheet(0)->getStyle($awal_final . '5:' . $end_final .''. ($key + $k) )->getNumberFormat()->setFormatCode('#,##0_-');
                    }

                    $writer = IOFactory::createWriter($spreadSheet, 'Xlsx');

                    if($this->opsi == 'save')
                    {
                        $path = public_path(). "/upload2/". $this->id .'/excel_osp_FO/';

                        if (!file_exists($path) ) {
                            if (!mkdir($path, 0770, true) ) {
                                return 'gagal menyiapkan folder sanggup';
                            }
                        }

                        $writer->save($path."Template BOQ ".strtoupper(str_replace('_', ' ', $title_key) )." ".$data->judul. ".xlsx");
                    }
                    else
                    {
                        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                        header('Content-Disposition: attachment;filename="BOQ '.strtoupper(str_replace('_', ' ', $title_key) ).' '.$data->judul.'.xlsx"');

                        $writer->save('php://output');
                        die;
                    }
                }
            },
            BeforeWriting::class => function(BeforeWriting $event) {
                $event->writer->setActiveSheetIndex(0);
            }
        ];
    }
}