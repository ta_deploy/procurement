<?php

namespace App\Http\Controllers;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Events\BeforeSheet;
use Maatwebsite\Excel\Events\BeforeWriting;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use Maatwebsite\Excel\Excel;
use DB;
class ExcelExport implements  WithEvents
{
    use Exportable;

    public function __construct($data, $h, $t, $b , $T, $ba){
        $this->child    = $data[0];
        $this->material = $data[1];
        $this->h        = $h;
        $this->b        = $b;
        $this->t        = $t;
        $this->T        = $T;
        $this->ba       = $ba;
    }

    public function convert_month($date, $cetak_hari = false){
        $bulan = array (1 =>   'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );

        $hari = array ( 1 =>    'Senin',
            'Selasa',
            'Rabu',
            'Kamis',
            'Jumat',
            'Sabtu',
            'Minggu'
        );

        $result = date('d m Y', strtotime($date));
        $split = explode(' ', $result);
        $hasilnya = $split[0] . ' ' . $bulan[(int)$split[1] ] . ' ' . $split[2];

        if ($cetak_hari) {
            $num = date('N', strtotime($date));
            return $hari[$num].' '.$hasilnya;
        }

        return $hasilnya;
    }

    public function registerEvents(): array
    {
        return [
            BeforeExport::class  => function(BeforeExport $event) {
                $fill_border_lampir = [
                    'fill' => [
                        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                        'startColor' => [
                            'argb' => '4CAEE3',
                        ],
                    ]
                ];

                $fill_border_nego_sepakat = [
                    'fill' => [
                        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                        'startColor' => [
                            'argb' => '595959',
                        ],
                    ]
                ];

                $border_Style = [
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
                        ],
                    ],
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    ]
                ];

                $event->writer->getProperties()->setCreator('Rendy');
                $worksheet1 = $event->writer->createSheet();
                $worksheet2 = $event->writer->createSheet();
                $worksheet3 = $event->writer->createSheet();
                $worksheet4 = $event->writer->createSheet();
                $worksheet1->setTitle('SPK');
                $worksheet2->setTitle('LAMPIRAN SPK');
                $worksheet3->setTitle('LAMPIRAN KONTRAK');
                $worksheet4->setTitle('BA NEGOSIASI DAN KESEPAKATAN');
                $dimension= ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M'];
                for ($i = 0; $i <= 3; $i++) {
                    // $event->writer->setActiveSheetIndex($i)->getStyle('A1:N70')->getAlignment()->setWrapText(true);
                    foreach ($dimension as $data) {
                    // $event->writer->setActiveSheetIndex($i)->getColumnDimension($data)->setAutoSize(true);
                    }
                }

                //material
                $nomor = 1;
                $push_material_jasa = 0;
                $push_material = 0;
                $push_jasa = 0;

                foreach($this->material as $key_material_spk => $val){
                    $push_material      += intval($val->material) * intval($val->jumlah);
                    $push_jasa          += intval($val->jasa) * intval($val->jumlah);
                    $push_material_jasa += $push_material + $push_jasa;
                    $event->writer->setActiveSheetIndex(1)->getCell("A".(9+$key_material_spk))->setValue($nomor++);
                    $event->writer->setActiveSheetIndex(1)->getCell("B".(9+$key_material_spk))->setValue('');
                    $event->writer->setActiveSheetIndex(1)->getCell("C".(9+$key_material_spk))->setValue(html_entity_decode($val->uraian, ENT_QUOTES, 'UTF-8') );
                    $event->writer->setActiveSheetIndex(1)->getCell("H".(9+$key_material_spk))->setValue("$val->sat");
                    $event->writer->setActiveSheetIndex(1)->getCell("I".(9+$key_material_spk))->setValue(intval($val->material));
                    $event->writer->setActiveSheetIndex(1)->getCell("J".(9+$key_material_spk))->setValue(intval($val->jasa));
                    $event->writer->setActiveSheetIndex(1)->getCell("K".(9+$key_material_spk))->setValue(intval($val->material) + intval($val->jasa));
                    $event->writer->setActiveSheetIndex(1)->getCell("L".(9+$key_material_spk))->setValue(intval($val->qty));
                    $event->writer->setActiveSheetIndex(1)->getStyle('I'.(9+$key_material_spk))->getNumberFormat()->setFormatCode('#,##0');
                    $event->writer->setActiveSheetIndex(1)->getStyle('J'.(9+$key_material_spk))->getNumberFormat()->setFormatCode('#,##0');
                    $event->writer->setActiveSheetIndex(1)->getStyle('K'.(9+$key_material_spk))->getNumberFormat()->setFormatCode('#,##0');
                    $event->writer->setActiveSheetIndex(1)->getStyle('L'.(9+$key_material_spk))->getNumberFormat()->setFormatCode('#,##0');
                    $event->writer->setActiveSheetIndex(1)->getStyle('K'.(9+$key_material_spk))->getFont()->setBold(true);

                    $event->writer->setActiveSheetIndex(2)->getCell("A".(10+$key_material_spk))->setValue($nomor++);
                    $event->writer->setActiveSheetIndex(2)->getCell("B".(10+$key_material_spk))->setValue('');
                    $event->writer->setActiveSheetIndex(2)->getCell("C".(10+$key_material_spk))->setValue(html_entity_decode($val->uraian, ENT_QUOTES, 'UTF-8') );
                    $event->writer->setActiveSheetIndex(2)->getCell("H".(10+$key_material_spk))->setValue("$val->sat");
                    $event->writer->setActiveSheetIndex(2)->getCell("I".(10+$key_material_spk))->setValue(intval($val->material));
                    $event->writer->setActiveSheetIndex(2)->getCell("J".(10+$key_material_spk))->setValue(intval($val->jasa));
                    $event->writer->setActiveSheetIndex(2)->getCell("K".(10+$key_material_spk))->setValue(intval($val->material) + intval($val->jasa));
                    $event->writer->setActiveSheetIndex(2)->getCell("L".(10+$key_material_spk))->setValue(intval($val->qty));
                    $event->writer->setActiveSheetIndex(2)->getStyle('I'.(10+$key_material_spk))->getNumberFormat()->setFormatCode('#,##0');
                    $event->writer->setActiveSheetIndex(2)->getStyle('J'.(10+$key_material_spk))->getNumberFormat()->setFormatCode('#,##0');
                    $event->writer->setActiveSheetIndex(2)->getStyle('K'.(10+$key_material_spk))->getNumberFormat()->setFormatCode('#,##0');
                    $event->writer->setActiveSheetIndex(2)->getStyle('L'.(10+$key_material_spk))->getNumberFormat()->setFormatCode('#,##0');
                    $event->writer->setActiveSheetIndex(2)->getStyle('K'.(10+$key_material_spk))->getFont()->setBold(true);

                    $event->writer->setActiveSheetIndex(3)->getCell("A".(13+$key_material_spk))->setValue($nomor++);
                    $event->writer->setActiveSheetIndex(3)->getCell("B".(13+$key_material_spk))->setValue($this->child->design);
                    $event->writer->setActiveSheetIndex(3)->getCell("C".(13+$key_material_spk))->setValue(html_entity_decode($val->uraian, ENT_QUOTES, 'UTF-8') );
                    $event->writer->setActiveSheetIndex(3)->getCell("D".(13+$key_material_spk))->setValue("$val->sat");
                    $event->writer->setActiveSheetIndex(3)->getCell("E".(13+$key_material_spk))->setValue(intval($val->material));
                    $event->writer->setActiveSheetIndex(3)->getCell("F".(13+$key_material_spk))->setValue(intval($val->jasa));
                    $event->writer->setActiveSheetIndex(3)->getStyle('E'.(13+$key_material_spk))->getNumberFormat()->setFormatCode('#,##0');
                    $event->writer->setActiveSheetIndex(3)->getStyle('F'.(13+$key_material_spk))->getNumberFormat()->setFormatCode('#,##0');
                    $event->writer->setActiveSheetIndex(3)->getStyle('E'.(13+$key_material_spk))->getFont()->setBold(true);
                    $event->writer->setActiveSheetIndex(3)->getStyle('F'.(13+$key_material_spk))->getFont()->setBold(true);
                }

                $event->writer->setActiveSheetIndex(0)->mergeCells('A1:M1');
                $event->writer->setActiveSheetIndex(0)->mergeCells('A29:A30');
                $event->writer->setActiveSheetIndex(0)->mergeCells('B31:J31');
                $event->writer->setActiveSheetIndex(0)->mergeCells('B29:J30');
                $event->writer->setActiveSheetIndex(0)->mergeCells('K29:L29');
                $event->writer->setActiveSheetIndex(0)->mergeCells('M29:M30');

                $data_spk = [
                    'A1'  => "SURAT PERINTAH KERJA",
                    'A3'  => "Nomor",
                    'C3'  => ': '.$this->child->hk_spk,
                    'A4'  => "Pekerjaan",
                    'C4'  => ': '.$this->child->pengadaan,
                    'A5'  => "Lokasi",
                    'C5'  => "Area Kalimantan Selatan",
                    'A7'  => 'Pada Hari ini '.$this->h.', Tanggal '.$this->t.' Bulan '.$this->b.' Tahun '.$this->T.', Yang bertanda tangan di bawah ini :',
                    'A9'  => "Perusahaan",
                    'C9'  => "PT.TELKOM AKSES",
                    'A10' => "Nama",
                    'C10' => ': '.$this->child->nama_spk,
                    'A11' => "Jabatan",
                    'C11' => ': '.$this->child->jabatan_spk,
                    'A12' => "Alamat",
                    'C12' => ': '.$this->child->alamat_spk,
                    'A14' => "Dalam Hal Ini Disebut Pemberi Tugas",
                    'A16' => "Perusahaan",
                    'C16' => ': '.preg_replace('/^PT/', 'PT.', $this->child->nama_company),
                    'A17' => "Nama",
                    'C17' => ': '.$this->child->wakil_mitra,
                    'A18' => "Jabatan",
                    'C18' => ': '.$this->child->jabatan_mitra,
                    'A19' => "Alamat",
                    'C19' => ': '.$this->child->alamat_company,
                    'A20' => "Dalam Hal Ini Disebut Pemberi Tugas",
                    'A22' => "Dengan ini Pemberi Tugas menunjuk kepada Penerima Tugas untuk melaksanakan pekerjaan sebagaimana ketentuan sebagai berikut :",
                    'A24' => "1",
                    'B24' => "LINGKUP PEKERJAAN",
                    'A25' => "a.",
                    'B25' => $this->child->pengadaan,
                    'B31' => $this->child->pengadaan,
                    'A26' => "b.",
                    'B26' => "Rincian Lingkup Pekerjaan sebagaimana tercantum pada lampiran SPK :",
                    'A29' => "NO",
                    'A31' => "1",
                    'B29' => "DESKRIPSI",
                    'K29' => "NILAI",
                    'K30' => "MATERIAL",
                    'K31' =>  $push_material,
                    'L30' => "JASA",
                    'L31' => $push_jasa,
                    'M29' => "JUMLAH",
                    'M31' => $push_material_jasa,
                    'A33' => "2",
                    'A35' => "a.",
                    'A36' => "b.",
                    'A37' => "c.",
                    'A38' => "d.",
                    'B38' => 'Jangka waktu pelaksanaan Pekerjaan adalah '.$this->child->total_kerja.' hari terhitung dari terbitnya SPK ini.',
                    'B33' => "SYARAT-SYARAT",
                    'B35' => "Menunjuk 1 (satu) orang PIC khusus untuk mengawal order tersebut.",
                    'B36' => "Lingkup pekerjaan tersebut di atas harus dilaksanakan sesuai dengan gambar-gambar rencana dan spesifikasi tekniknya serta sesuai dengan petunjuk baik lisan maupun ",
                    'B37' => "Menghormati / Mentaati dan Menjalankan segala peraturan yang berlaku dilingkungan / lokasi kerja.",
                    'A42' => "Di keluarkan di Banjarmasin",
                    'B43' => 'Pada Tanggal '.$this->convert_month($this->child->tgl_excel),
                    'A45' => "Penerima Tugas,",
                    'A46' =>preg_replace('/^PT/', 'PT.',$this->child->nama_company),
                    'A51' => $this->child->wakil_mitra,
                    'A52' => $this->child->jabatan_mitra,
                    'M45' => "Penerima Tugas,",
                    'M46' => "PT. TELKOM AKSES",
                    'M51' => $this->child->nama_spk,
                    'M52' => $this->child->jabatan_spk
                ];

                foreach ($data_spk as $key_spk => $val_spk) {
                    $event->writer->setActiveSheetIndex(0)->getCell("$key_spk")->setValue("$val_spk");
                }

                $event->writer->setActiveSheetIndex(0)->getStyle('A24')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                $event->writer->setActiveSheetIndex(0)->getStyle('A25:A26')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
                $event->writer->setActiveSheetIndex(0)->getStyle('A1')->getFont()->setUnderline(true);
                $event->writer->setActiveSheetIndex(0)->getStyle('A1')->getFont()->setBold(true);
                $event->writer->setActiveSheetIndex(0)->getStyle('A1')->getFont()->setSize(16);
                $event->writer->setActiveSheetIndex(0)->getStyle('A51:M51')->getFont()->setBold(true);
                $event->writer->setActiveSheetIndex(0)->getStyle('A1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $event->writer->setActiveSheetIndex(0)->getStyle('K31')->getNumberFormat()->setFormatCode('#,##0');
                $event->writer->setActiveSheetIndex(0)->getStyle('L31')->getNumberFormat()->setFormatCode('#,##0');
                $event->writer->setActiveSheetIndex(0)->getStyle('M31')->getNumberFormat()->setFormatCode('#,##0');
                $event->writer->setActiveSheetIndex(0)->getStyle('A29:M31')->applyFromArray($border_Style);
                $event->writer->setActiveSheetIndex(0)->getStyle('A29:M30')->getFont()->setSize(12);
                $event->writer->setActiveSheetIndex(0)->getStyle('M51')->getFont()->setUnderline(true);
                $event->writer->setActiveSheetIndex(0)->getStyle('A51')->getFont()->setUnderline(true);
                $event->writer->setActiveSheetIndex(0)->getStyle('A29:M30')->getFont()->setBold(true);
                $event->writer->setActiveSheetIndex(0)->getStyle('A29:M30')->getFont()->SetName('Arial Narrow');
                $event->writer->setActiveSheetIndex(0)->getStyle('A33')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                $event->writer->setActiveSheetIndex(0)->getStyle('A33:A38')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
                $event->writer->setActiveSheetIndex(0)->getStyle('M46')->getFont()->setBold(true);

                $event->writer->setActiveSheetIndex(1)->mergeCells('A1:L1');
                $event->writer->setActiveSheetIndex(1)->mergeCells('A2:L2');
                $event->writer->setActiveSheetIndex(1)->mergeCells('A3:L3');

                $event->writer->setActiveSheetIndex(1)->mergeCells('I6:J6');

                $data_lampiran_spk = [
                    'A1'  => 'LAMPIRAN SPK :'.$this->child->hk_spk.' Tanggal '.$this->convert_month($this->child->tgl_excel),
                    'A2'  => $this->child->pengadaan,
                    'A3'  =>preg_replace('/^PT/', 'PT.',$this->child->nama_company),
                    'A6'  => "NO",
                    'B6'  => "DESIGNATOR",
                    'C6'  => "URAIAN PEKERJAAN",
                    'H6'  => "SAT",
                    'I6'  => "HARGA SATUAN",
                    'I7'  => "Material",
                    'J7'  => "Jasa",
                    'K6'  => "JUMLAH",
                    'L6'  => "VOLUME"
                ];

                foreach ($data_lampiran_spk as $key_lspk => $val_lspk) {
                    $event->writer->setActiveSheetIndex(1)->getCell("$key_lspk")->setValue("$val_lspk");
                }

                for ($i = 6; $i <= 13+$key_material_spk; $i++) {
                    $event->writer->setActiveSheetIndex(1)->mergeCells('C'.$i.':G'.$i);
                }

                $lanjutan_lampiran_spk = [
                    'C'.(11+$key_material_spk) => "MATERIAL",
                    'C'.(12+$key_material_spk) => "JASA",
                    'C'.(13+$key_material_spk) => "GRAND TOTAL",
                    'L'.(11+$key_material_spk) => $push_material,
                    'L'.(12+$key_material_spk) => $push_jasa,
                    'L'.(13+$key_material_spk) => $push_material_jasa,
                    'B'.(15+$key_material_spk) => 'PT. TELKOM AKSES',
                    'B'.(24+$key_material_spk) => $this->child->nama_spk,
                    'B'.(25+$key_material_spk) => $this->child->jabatan_spk,
                    'H'.(15+$key_material_spk) =>preg_replace('/^PT/', 'PT.',$this->child->nama_company),
                    'H'.(24+$key_material_spk) => $this->child->wakil_mitra,
                    'H'.(25+$key_material_spk) => $this->child->jabatan_mitra
                ];

                foreach ($lanjutan_lampiran_spk as $key_lspk => $val_lspk) {
                    $event->writer->setActiveSheetIndex(1)->getCell("$key_lspk")->setValue("$val_lspk");
                }

                $event->writer->setActiveSheetIndex(1)->getStyle('A1:J4')->getFont()->SetName('Times New Roman');
                $event->writer->setActiveSheetIndex(1)->getStyle('A1:J4')->getFont()->setSize(15);
                $event->writer->setActiveSheetIndex(1)->getStyle('A6:L7')->getFont()->setSize(11);
                $event->writer->setActiveSheetIndex(1)->getStyle('A6:L7')->getFont()->setBold(true);
                $event->writer->setActiveSheetIndex(1)->getStyle('C'.(13+$key_material_spk))->getFont()->setBold(true);
                $event->writer->setActiveSheetIndex(1)->getStyle('L'.(13+$key_material_spk))->getFont()->setBold(true);
                $event->writer->setActiveSheetIndex(1)->getStyle('B'.(24+$key_material_spk))->getFont()->setBold(true);
                $event->writer->setActiveSheetIndex(1)->getStyle('B'.(24+$key_material_spk).':k'.(24+$key_material_spk))->getFont()->setBold(true);
                $event->writer->setActiveSheetIndex(1)->getStyle('H'.(24+$key_material_spk))->getFont()->setBold(true);
                $event->writer->setActiveSheetIndex(1)->getStyle('A6:L7')->getFont()->SetName('Calibri');
                $event->writer->setActiveSheetIndex(1)->mergeCells('H'.(15+$key_material_spk).':K'.(16+$key_material_spk));
                $event->writer->setActiveSheetIndex(1)->mergeCells('B'.(15+$key_material_spk).':D'.(16+$key_material_spk));
                $event->writer->setActiveSheetIndex(1)->mergeCells('B'.(24+$key_material_spk).':D'.(24+$key_material_spk));
                $event->writer->setActiveSheetIndex(1)->mergeCells('B'.(25+$key_material_spk).':D'.(25+$key_material_spk));
                $event->writer->setActiveSheetIndex(1)->mergeCells('H'.(24+$key_material_spk).':K'.(24+$key_material_spk));
                $event->writer->setActiveSheetIndex(1)->mergeCells('H'.(25+$key_material_spk).':K'.(25+$key_material_spk));
                $event->writer->setActiveSheetIndex(1)->getStyle('A6:L'.(13+$key_material_spk))->applyFromArray($border_Style);
                $event->writer->setActiveSheetIndex(1)->getStyle('B'.(15+$key_material_spk).':H'.(25+$key_material_spk))->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $event->writer->setActiveSheetIndex(1)->getStyle('L'.(11+$key_material_spk))->getNumberFormat()->setFormatCode('#,##0');
                $event->writer->setActiveSheetIndex(1)->getStyle('L'.(12+$key_material_spk))->getNumberFormat()->setFormatCode('#,##0');
                $event->writer->setActiveSheetIndex(1)->getStyle('L'.(13+$key_material_spk))->getNumberFormat()->setFormatCode('#,##0');
                $event->writer->setActiveSheetIndex(1)->getStyle('H'.(24+$key_material_spk))->getFont()->setUnderline(true);
                $event->writer->setActiveSheetIndex(1)->getStyle('B'.(24+$key_material_spk))->getFont()->setUnderline(true);
                $event->writer->setActiveSheetIndex(1)->getStyle('B'.(15+$key_material_spk).':H'.(25+$key_material_spk))->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->writer->setActiveSheetIndex(1)->getStyle('C9:C'.(13+$key_material_spk))->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                $event->writer->setActiveSheetIndex(1)->getStyle('I9:L'.(13+$key_material_spk))->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);

                //ini sheet 2
                $event->writer->setActiveSheetIndex(2)->mergeCells('A2:L2');
                $event->writer->setActiveSheetIndex(2)->mergeCells('A3:L3');
                $event->writer->setActiveSheetIndex(2)->mergeCells('A5:G5');
                $event->writer->setActiveSheetIndex(2)->mergeCells('I7:J7');

                $data_lampiran_kontrak = [
                    'A2'  => "LAMPIRAN BOQ PERJANJIAN KERJA SAMA",
                    'A3'  => $this->child->pengadaan,
                    'A5'  => 'NOMOR KONTRAK :'.$this->child->hk_spk. ' Tanggal '.$this->convert_month($this->child->tgl_excel) ,
                    'A7'  => "NO",
                    'B7'  => "DESIGNATOR",
                    'C7'  => "URAIAN PEKERJAAN",
                    'H7'  => "SAT",
                    'I7'  => "Harga Satuan",
                    'I8'  => "Material",
                    'J8'  => "Jasa",
                    'K7'  => "JUMLAH",
                    'L7'  => "Volume"
                ];

                foreach ($data_lampiran_kontrak as $key_lspk => $val_lspk) {
                    $event->writer->setActiveSheetIndex(2)->getCell("$key_lspk")->setValue("$val_lspk");
                }

                for ($i = 7; $i <= 14+$key_material_spk; $i++) {
                    $event->writer->setActiveSheetIndex(2)->mergeCells('C'.$i.':G'.$i);
                }

                $lanjutan_lampiran_kontrak = [
                    'C'.(12+$key_material_spk) => "MATERIAL",
                    'C'.(13+$key_material_spk) => "JASA",
                    'C'.(14+$key_material_spk) => "GRAND TOTAL",
                    'L'.(12+$key_material_spk) => $push_material,
                    'L'.(13+$key_material_spk) => $push_jasa,
                    'L'.(14+$key_material_spk) => $push_material_jasa,
                    'B'.(16+$key_material_spk) => 'PT. TELKOM AKSES',
                    'B'.(25+$key_material_spk) => $this->child->nama_spk,
                    'B'.(26+$key_material_spk) => $this->child->jabatan_spk,
                    'H'.(25+$key_material_spk) => $this->child->wakil_mitra,
                    'H'.(26+$key_material_spk) => $this->child->jabatan_mitra,
                    'H'.(16+$key_material_spk) =>preg_replace('/^PT/', 'PT.',$this->child->nama_company)
                ];

                foreach ($lanjutan_lampiran_kontrak as $key_kontrak => $val_kontrak) {
                    $event->writer->setActiveSheetIndex(2)->getCell("$key_kontrak")->setValue("$val_kontrak");
                }

                $event->writer->setActiveSheetIndex(2)->mergeCells('B'.(16+$key_material_spk).':D'.(17+$key_material_spk));
                $event->writer->setActiveSheetIndex(2)->mergeCells('H'.(16+$key_material_spk).':K'.(17+$key_material_spk));
                $event->writer->setActiveSheetIndex(2)->mergeCells('B'.(25+$key_material_spk).':D'.(25+$key_material_spk));
                $event->writer->setActiveSheetIndex(2)->mergeCells('H'.(25+$key_material_spk).':K'.(25+$key_material_spk));
                $event->writer->setActiveSheetIndex(2)->mergeCells('B'.(26+$key_material_spk).':D'.(26+$key_material_spk));
                $event->writer->setActiveSheetIndex(2)->mergeCells('H'.(26+$key_material_spk).':K'.(26+$key_material_spk));

                $event->writer->setActiveSheetIndex(2)->getStyle('A2:L5')->getFont()->SetName('Times New Roman');
                $event->writer->setActiveSheetIndex(2)->getStyle('A2:L5')->getFont()->setSize(15);
                $event->writer->setActiveSheetIndex(2)->getStyle('A7:L8')->getFont()->setSize(11);
                $event->writer->setActiveSheetIndex(2)->getStyle('A7:L8')->getFont()->setBold(true);
                $event->writer->setActiveSheetIndex(2)->getStyle('K10')->getFont()->setBold(true);
                $event->writer->setActiveSheetIndex(2)->getStyle('C'.(14+$key_material_spk))->getFont()->setBold(true);
                $event->writer->setActiveSheetIndex(2)->getStyle('L'.(14+$key_material_spk))->getFont()->setBold(true);
                $event->writer->setActiveSheetIndex(2)->getStyle('C'.(25+$key_material_spk))->getFont()->setBold(true);
                $event->writer->setActiveSheetIndex(2)->getStyle('H'.(25+$key_material_spk))->getFont()->setBold(true);
                $event->writer->setActiveSheetIndex(2)->getStyle('B'.(25+$key_material_spk).':K'.(25+$key_material_spk))->getFont()->setBold(true);
                $event->writer->setActiveSheetIndex(2)->getStyle('A7:L8')->getFont()->SetName('Calibri');
                $event->writer->setActiveSheetIndex(2)->getStyle('A7:L'.(14+$key_material_spk))->applyFromArray($border_Style);
                $event->writer->setActiveSheetIndex(2)->getStyle('A2:L3')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $event->writer->setActiveSheetIndex(2)->getStyle('B'.(16+$key_material_spk).':k'.(26+$key_material_spk))->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $event->writer->setActiveSheetIndex(2)->getStyle('H'.(25+$key_material_spk))->getFont()->setUnderline(true);
                $event->writer->setActiveSheetIndex(2)->getStyle('B'.(25+$key_material_spk))->getFont()->setUnderline(true);
                $event->writer->setActiveSheetIndex(2)->getStyle('L'.(12+$key_material_spk))->getNumberFormat()->setFormatCode('#,##0');
                $event->writer->setActiveSheetIndex(2)->getStyle('L'.(13+$key_material_spk))->getNumberFormat()->setFormatCode('#,##0');
                $event->writer->setActiveSheetIndex(2)->getStyle('L'.(14+$key_material_spk))->getNumberFormat()->setFormatCode('#,##0');
                $event->writer->setActiveSheetIndex(2)->getStyle('B'.(16+$key_material_spk).':k'.(26+$key_material_spk))->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->writer->setActiveSheetIndex(2)->getStyle('C10:C'.(14+$key_material_spk))->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                $event->writer->setActiveSheetIndex(2)->getStyle('L10:L'.(14+$key_material_spk))->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
                $event->writer->setActiveSheetIndex(2)->getStyle('A7:L8')->applyFromArray($fill_border_lampir);

                $event->writer->setActiveSheetIndex(3)->mergeCells('A1:F2');
                $event->writer->setActiveSheetIndex(3)->mergeCells('A3:F3');
                $event->writer->setActiveSheetIndex(3)->mergeCells('A4:F4');
                $event->writer->setActiveSheetIndex(3)->mergeCells('A5:B5');
                $event->writer->setActiveSheetIndex(3)->mergeCells('A6:B6');
                $event->writer->setActiveSheetIndex(3)->mergeCells('A7:B7');
                $event->writer->setActiveSheetIndex(3)->mergeCells('A9:A11');
                $event->writer->setActiveSheetIndex(3)->mergeCells('B9:B11');
                $event->writer->setActiveSheetIndex(3)->mergeCells('C9:C11');
                $event->writer->setActiveSheetIndex(3)->mergeCells('D9:D11');
                $event->writer->setActiveSheetIndex(3)->mergeCells('E9:F10');
                $event->writer->setActiveSheetIndex(3)->mergeCells('A12:C12');

                $data_NEGO = [
                    'A1'  => "BA NEGOSIASI & KESEPAKATAN HARGA",
                    'A5'  => "Tempat",
                    'C5'  => ": R. RAPAT TELKOM AKSES REG 6",
                    'A6'  => "Tanggal",
                    'C6'  => $this->ba,
                    'A7'  => "Peserta",
                    'C7'  =>preg_replace('/^PT/', 'PT.',$this->child->nama_company),
                    'A9'  => "NO",
                    'B9'  => "ID",
                    'C9'  => "URAIAN PEKERJAAN",
                    'D9'  => "SATUAN",
                    'E9'  => "HARGA SATUAN (RP)",
                    'E11' => "MATERIAL",
                    'F11' => "JASA"
                ];

                foreach ($data_NEGO as $key_nego => $val_nego) {
                    $event->writer->setActiveSheetIndex(3)->getCell("$key_nego")->setValue("$val_nego");
                }

                $lanjutan_lampiran_BA = [
                    'A'.(16+$key_material_spk) => "PT. TELKOM AKSES",
                    'A'.(20+$key_material_spk) => $this->child->nama_ba,
                    'A'.(21+$key_material_spk) => $this->child->jabatan_ba,
                    'C'.(22+$key_material_spk) => "Mengetahui/Menyetujui",
                    'C'.(27+$key_material_spk) => $this->child->nama_spk,
                    'C'.(28+$key_material_spk) => $this->child->jabatan_spk,
                    'D'.(16+$key_material_spk) =>preg_replace('/^PT/', 'PT.',$this->child->nama_company),
                    'E'.(20+$key_material_spk) => $this->child->wakil_mitra,
                    'E'.(21+$key_material_spk) => $this->child->jabatan_mitra
                ];

                foreach ($lanjutan_lampiran_BA as $key_BA => $val_BA) {
                    $event->writer->setActiveSheetIndex(3)->getCell("$key_BA")->setValue("$val_BA");
                }

                $event->writer->setActiveSheetIndex(3)->mergeCells('A'.(16+$key_material_spk).':B'.(16+$key_material_spk));
                $event->writer->setActiveSheetIndex(3)->mergeCells('D'.(16+$key_material_spk).':G'.(16+$key_material_spk));
                $event->writer->setActiveSheetIndex(3)->mergeCells('A'.(20+$key_material_spk).':B'.(20+$key_material_spk));
                $event->writer->setActiveSheetIndex(3)->mergeCells('A'.(21+$key_material_spk).':B'.(21+$key_material_spk));
                $event->writer->setActiveSheetIndex(3)->mergeCells('E'.(20+$key_material_spk).':F'.(20+$key_material_spk));
                $event->writer->setActiveSheetIndex(3)->mergeCells('E'.(21+$key_material_spk).':F'.(21+$key_material_spk));
                $event->writer->setActiveSheetIndex(3)->getStyle('A1:F11')->getFont()->setBold(true);
                $event->writer->setActiveSheetIndex(3)->getStyle('E13:F'.(13+$key_material_spk))->getFont()->setBold(true);
                $event->writer->setActiveSheetIndex(3)->getStyle('A'.(16+$key_material_spk).':G'.(20+$key_material_spk))->getFont()->setBold(true);
                $event->writer->setActiveSheetIndex(3)->getStyle('C'.(22+$key_material_spk).':C'.(27+$key_material_spk))->getFont()->setBold(true);
                $event->writer->setActiveSheetIndex(3)->getStyle('C'.(27+$key_material_spk))->getFont()->setUnderline(true);
                $event->writer->setActiveSheetIndex(3)->getStyle('C'.(20+$key_material_spk))->getFont()->setUnderline(true);
                $event->writer->setActiveSheetIndex(3)->getStyle('A'.(20+$key_material_spk))->getFont()->setUnderline(true);
                $event->writer->setActiveSheetIndex(3)->getStyle('E'.(20+$key_material_spk))->getFont()->setUnderline(true);
                $event->writer->setActiveSheetIndex(3)->getStyle('A1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $event->writer->setActiveSheetIndex(3)->getStyle('A1')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->writer->setActiveSheetIndex(3)->getStyle('A'.(20+$key_material_spk).':E'.(21+$key_material_spk))->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $event->writer->setActiveSheetIndex(3)->getStyle('C'.(22+$key_material_spk).':C'.(28+$key_material_spk))->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $event->writer->setActiveSheetIndex(3)->getStyle('A9:F11')->applyFromArray($fill_border_nego_sepakat);
                $event->writer->setActiveSheetIndex(3)->getStyle('A9:F'.(13+$key_material_spk))->applyFromArray($border_Style);
                $event->writer->setActiveSheetIndex(3)->getStyle('A9:F11')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
            },
            BeforeWriting::class => function(BeforeWriting $event) {
                $event->writer->setActiveSheetIndex(0);
            }
        ];
    }
}