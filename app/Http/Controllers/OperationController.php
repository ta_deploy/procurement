<?php

namespace App\Http\Controllers;

use App\DA\ToolsModel;
use App\DA\OperationModel;
use App\DA\MitraModel;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;
use App\DA\AdminModel;
use App\DA\ReportModel;
use DB;
use Illuminate\Support\Facades\Input;

date_default_timezone_set("Asia/Makassar");

class OperationController extends Controller
{

	public static function download_justify($id, $jenis = 'download')
	{
		$data = AdminModel::get_data_final($id);
		Excel::store(new Excel_justifyExport([$data, $jenis]),  'Justify.xlsx');
	}

	public static function download_boq_operation($id, $jenis = 'download', $status = 'All')
	{
		Excel::store(new Excel_BOQ_Operation([$id, $jenis, $status]),  'boq.xlsx');
	}

	public function delete_upload($id)
	{
		$msg = OperationModel::delete_pid($id);
    return redirect($msg['direct'])->with('alerts', $msg['isi']);
	}

	public function input_rfc_design(Request $req, $id)
	{
		OperationModel::input_rfc_designator($req, $id);
		return back()->with('alerts', [
			['type' => 'success', 'text' => 'RFC berhasil diperbaharui!']
		]);
	}

	public function setting_wbs($id)
	{
		$data_pbu = AdminModel::get_data_final($id);

		if($data_pbu->step_id >= 6)
		{
			return redirect('/home')->with('alerts', [
				['type' => 'warning', 'text' => 'Pekerjaan sudah masuk Pasca Kerja!!']
			]);
		}

		$find_pid   = OperationModel::find_pid_byUp($id);
		$get_budget = OperationModel::get_budget($id);

		$idp = [];

		foreach($find_pid as $v)
		{
			$check_pid = OperationModel::check_pid($v);
			$idp[$check_pid->idp] = $check_pid->idp;
		}

		// dd($find_pid, $idp);

		$ll = OperationModel::get_lokasi($id);
		$save_pid = $save_pid_per_lok = [];

		foreach($ll as $k => $v)
		{
			$exp = explode(', ', $v->pid_sto);

			foreach($exp as $vvv)
			{
				$save_pid_per_lok[$v->id][] = $vvv;
			}
		}

		foreach($idp as $val)
		{
			$exp = explode(',', $val);

			foreach($exp as $v)
			{
				$save_pid[] = $v;
			}
		}

		$kerjaan       = ToolsModel::list_pekerjaan();
		$all_sto       = AdminModel::area_alamat($data_pbu->witel);
		$design        = AdminModel::get_design(3, $id);
		$sub_jenis_all = AdminModel::get_pekerjaan($data_pbu->jenis_work, 'sub_jenis_all_sj');

		$last_data = OperationModel::get_last_data($id, 0);
		// dd($last_data);
		$data_item = $data_item_rw = [];

		// foreach($last_data as $val)
		// {
			// $data_item[$val->id_boq_lokasi][] = $val;

			// $last_design[$val->id_design]['id']       = $val->id_design;
			// $last_design[$val->id_design]['material'] = $val->material;
			// $last_design[$val->id_design]['jasa']     = $val->jasa;
			// $last_design[$val->id_design]['jenis']    = $val->jenis;
			// $last_design[$val->id_design]['namcomp']  = $val->namcomp;
		// }

		// $data_item = array_values($data_item);
		$data = AdminModel::get_data_final($id);
		$get_pid = MitraModel::get_pid_by_id($id);

		// $last_data_mat = AdminModel::get_last_material_boq($id, 'all');
		$last_data_mat = AdminModel::get_last_material_boq($id, 0);
		$order_mat_1 = $order_mat_0 = $order_mat_2 = [];

		foreach($last_data_mat as $v)
		{
			if($v->status_design == 1)
			{
				$order_mat_1[] = $v;
			}

			if($v->status_design == 2)
			{
				$order_mat_2[] = $v;
			}

			if($v->status_design == 0)
			{
				$order_mat_0[] = $v;
			}
		}

		if($order_mat_1)
		{
			$last_data_mat = $order_mat_1;
		}
		elseif($order_mat_2)
		{
			$last_data_mat = $order_mat_2;
		}
		elseif($order_mat_0)
		{
			$last_data_mat = $order_mat_0;
		}

		foreach($last_data_mat as $k => $val)
		{
			$data_material[$val->id_design .'_'. $val->design_mitra_id]['id_design']       = $val->id_design;
			$data_material[$val->id_design .'_'. $val->design_mitra_id]['design_mitra_id'] = $val->design_mitra_id;
			$data_material[$val->id_design .'_'. $val->design_mitra_id]['jenis']           = $val->jenis;
			$data_material[$val->id_design .'_'. $val->design_mitra_id]['designator']      = $val->designator;
			$data_material[$val->id_design .'_'. $val->design_mitra_id]['uraian']          = html_entity_decode($val->uraian, ENT_QUOTES, 'UTF-8');
			$data_material[$val->id_design .'_'. $val->design_mitra_id]['jenis_khs']       = $val->jenis_khs;
			$data_material[$val->id_design .'_'. $val->design_mitra_id]['material']        = $val->material;
			$data_material[$val->id_design .'_'. $val->design_mitra_id]['jasa']            = $val->jasa;
			$data_material[$val->id_design .'_'. $val->design_mitra_id]['namcomp']         = $val->namcomp;
			$data_material[$val->id_design .'_'. $val->design_mitra_id]['sp']              = $val->sp;
			$data_material[$val->id_design .'_'. $val->design_mitra_id]['rekon']           = $val->rekon;
		}

		$data_material = array_values($data_material);

		foreach($last_data as $val)
		{
			if(!isset($data_item_rw[$val->id_boq_lokasi]) )
			{
				foreach($data_material as $k => $v)
				{
					$data_item_rw[$val->id_boq_lokasi][$v['id_design'] .'-'. $v['jenis_khs'] ] = (object)[
						'id'              => 0,
						'sto'             => $val->sto,
						'id_boq_lokasi'   => $val->id_boq_lokasi,
						'sub_jenis_p'   	=> $val->sub_jenis_p,
						'lokasi' 				  => $val->lokasi,
						'id_design'       => $v['id_design'],
						'jenis_khs'       => $v['jenis_khs'],
						'designator'      => $v['designator'],
						'material'        => $v['material'],
						'jasa'            => $v['jasa'],
						'sp'              => 0,
						'rekon'           => 0,
						'tambah'          => 0,
						'kurang'          => 0,
						'status_design'   => 0,
						'jenis'           => $v['jenis'],
						'namcomp'         => $v['namcomp'],
						'design_mitra_id' => $v['design_mitra_id'],
						'uraian'          => $v['uraian'],
					];
				}
			}

			if($data_item_rw[$val->id_boq_lokasi][$val->id_design .'-'. $val->jenis_khs])
			{
				$data_item_rw[$val->id_boq_lokasi][$val->id_design .'-'. $val->jenis_khs ]->id              = $val->id;
				$data_item_rw[$val->id_boq_lokasi][$val->id_design .'-'. $val->jenis_khs ]->sp              = $val->sp;
				$data_item_rw[$val->id_boq_lokasi][$val->id_design .'-'. $val->jenis_khs ]->rekon           = $val->rekon;
				$data_item_rw[$val->id_boq_lokasi][$val->id_design .'-'. $val->jenis_khs ]->material        = $val->material;
				$data_item_rw[$val->id_boq_lokasi][$val->id_design .'-'. $val->jenis_khs ]->jasa            = $val->jasa;
				$data_item_rw[$val->id_boq_lokasi][$val->id_design .'-'. $val->jenis_khs ]->jenis           = $val->jenis;
				$data_item_rw[$val->id_boq_lokasi][$val->id_design .'-'. $val->jenis_khs ]->namcomp         = $val->namcomp;
				$data_item_rw[$val->id_boq_lokasi][$val->id_design .'-'. $val->jenis_khs ]->design_mitra_id = $val->design_mitra_id;
			}
		}

		foreach($data_item_rw as $k => $v)
		{
			$data_item[] = array_values($v);
		}
		// dd($save_pid_per_lok, $save_pid);
		return view('Operation.setting_wbs', compact('kerjaan', 'data_pbu', 'all_sto', 'data_material', 'design', 'data', 'data_item', 'save_pid_per_lok', 'get_pid', 'get_budget', 'sub_jenis_all'), ['pid' => $save_pid] );
	}

	public function update_wbs(Request $req, $id)
	{
		// dd(json_decode($req->material_input),json_decode($req->lokasi_input) );
		return OperationModel::update_wbs($req, $id);
	}

	public function edit_po($id)
	{
		$yes = false;

		if($yes)
		{
			$data = DB::Table('procurement_boq_upload As pbu')
			->leftjoin('procurement_Boq_lokasi As pbl', 'pbu.id', '=', 'pbl.id_upload')
			->leftjoin('procurement_Boq_design As pbd', 'pbd.id_boq_lokasi', '=', 'pbl.id')
			->select('pbu.id As id_pbu', 'pbd.*')
			->where([
				['pbu.active', 1],
				['pbu.id', $id],
				['pbu.pekerjaan', '!=', 'PSB']
			])
			->get();

			foreach($data as $k => $d)
			{
				$final_data[$d->id_pbu]['id_pbu'] = $d->id_pbu;

				$data_pbl[$d->id_boq_lokasi]['id'] = $d->id_boq_lokasi;

				if(!isset($final_data[$d->id_pbu]['total_material_sp']) )
				{
					$final_data[$d->id_pbu]['total_material_sp'] = 0;
					$final_data[$d->id_pbu]['total_jasa_sp']     = 0;
					$final_data[$d->id_pbu]['total_sp']     		 = 0;

					$final_data[$d->id_pbu]['total_material_rek_sp_plan'] = 0;
					$final_data[$d->id_pbu]['total_jasa_rek_sp_plan']     = 0;

					$final_data[$d->id_pbu]['total_material_rekon_plan'] = 0;
					$final_data[$d->id_pbu]['total_jasa_rekon_plan']     = 0;
					$final_data[$d->id_pbu]['total_materialT_rek_plan']  = 0;
					$final_data[$d->id_pbu]['total_materialK_rek_plan']  = 0;
					$final_data[$d->id_pbu]['total_jasaT_rek_plan']      = 0;
					$final_data[$d->id_pbu]['total_jasaK_rek_plan']      = 0;

					$final_data[$d->id_pbu]['total_material_rek_sp'] = 0;
					$final_data[$d->id_pbu]['total_jasa_rek_sp']     = 0;

					$final_data[$d->id_pbu]['total_material_rekon'] = 0;
					$final_data[$d->id_pbu]['total_jasa_rekon']     = 0;
					$final_data[$d->id_pbu]['total_materialT_rek']  = 0;
					$final_data[$d->id_pbu]['total_materialK_rek']  = 0;
					$final_data[$d->id_pbu]['total_jasaT_rek']      = 0;
					$final_data[$d->id_pbu]['total_jasaK_rek']      = 0;
				}

				if(!isset($data_pbl[$d->id_boq_lokasi]['material_sp']) )
				{
					$data_pbl[$d->id_boq_lokasi]['material_sp'] = 0;
					$data_pbl[$d->id_boq_lokasi]['jasa_sp']     = 0;

					$data_pbl[$d->id_boq_lokasi]['material_rek_sp_plan'] = 0;
					$data_pbl[$d->id_boq_lokasi]['jasa_rek_sp_plan']     = 0;

					$data_pbl[$d->id_boq_lokasi]['material_rek_plan']  = 0;
					$data_pbl[$d->id_boq_lokasi]['jasa_rek_plan']      = 0;
					$data_pbl[$d->id_boq_lokasi]['materialT_rek_plan'] = 0;
					$data_pbl[$d->id_boq_lokasi]['materialK_rek_plan'] = 0;
					$data_pbl[$d->id_boq_lokasi]['jasaT_rek_plan']     = 0;
					$data_pbl[$d->id_boq_lokasi]['jasaK_rek_plan']     = 0;

					$data_pbl[$d->id_boq_lokasi]['material_sp_rek'] = 0;
					$data_pbl[$d->id_boq_lokasi]['jasa_sp_rek']     = 0;

					$data_pbl[$d->id_boq_lokasi]['material_rek']  = 0;
					$data_pbl[$d->id_boq_lokasi]['jasa_rek']      = 0;
					$data_pbl[$d->id_boq_lokasi]['materialT_rek'] = 0;
					$data_pbl[$d->id_boq_lokasi]['materialK_rek'] = 0;
					$data_pbl[$d->id_boq_lokasi]['jasaT_rek']     = 0;
					$data_pbl[$d->id_boq_lokasi]['jasaK_rek']     = 0;
				}

				if($d->status_design == 0)
				{
					$final_data[$d->id_pbu]['total_material_sp'] += $d->material * $d->sp;
					$final_data[$d->id_pbu]['total_jasa_sp']     += $d->jasa * $d->sp;
					$final_data[$d->id_pbu]['total_sp']     		 += ($d->material * $d->sp) + ($d->jasa * $d->sp);

					$data_pbl[$d->id_boq_lokasi]['material_sp'] += $d->material * $d->sp;
					$data_pbl[$d->id_boq_lokasi]['jasa_sp']     += $d->jasa * $d->sp;
				}

				if($d->status_design == 2)
				{
					$final_data[$d->id_pbu]['total_material_rek_sp_plan'] += $d->material * $d->sp;
					$final_data[$d->id_pbu]['total_jasa_rek_sp_plan']     += $d->jasa * $d->sp;

					$final_data[$d->id_pbu]['total_material_rekon_plan'] += $d->material * $d->rekon;
					$final_data[$d->id_pbu]['total_jasa_rekon_plan']     += $d->jasa * $d->rekon;
					$final_data[$d->id_pbu]['total_materialT_rek_plan']  += $d->tambah * $d->material;
					$final_data[$d->id_pbu]['total_materialK_rek_plan']  += $d->kurang * $d->material;
					$final_data[$d->id_pbu]['total_jasaT_rek_plan']      += $d->tambah * $d->jasa;
					$final_data[$d->id_pbu]['total_jasaK_rek_plan']      += $d->kurang * $d->jasa;

					$data_pbl[$d->id_boq_lokasi]['material_rek_sp_plan'] += $d->material * $d->sp;
					$data_pbl[$d->id_boq_lokasi]['jasa_rek_sp_plan']     += $d->jasa * $d->sp;

					$data_pbl[$d->id_boq_lokasi]['material_rek_plan'] += $d->material * $d->rekon;
					$data_pbl[$d->id_boq_lokasi]['jasa_rek_plan']     += $d->jasa * $d->rekon;
					$data_pbl[$d->id_boq_lokasi]['materialT_rek_plan']  += $d->tambah * $d->material;
					$data_pbl[$d->id_boq_lokasi]['materialK_rek_plan']  += $d->kurang * $d->material;
					$data_pbl[$d->id_boq_lokasi]['jasaT_rek_plan']      += $d->tambah * $d->jasa;
					$data_pbl[$d->id_boq_lokasi]['jasaK_rek_plan']      += $d->kurang * $d->jasa;
				}

				if($d->status_design == 1)
				{
					$final_data[$d->id_pbu]['total_material_rek_sp'] += $d->material * $d->sp;
					$final_data[$d->id_pbu]['total_jasa_rek_sp']     += $d->jasa * $d->sp;

					$final_data[$d->id_pbu]['total_material_rekon'] += $d->material * $d->rekon;
					$final_data[$d->id_pbu]['total_jasa_rekon']     += $d->jasa * $d->rekon;
					$final_data[$d->id_pbu]['total_materialT_rek']  += $d->tambah * $d->material;
					$final_data[$d->id_pbu]['total_materialK_rek']  += $d->kurang * $d->material;
					$final_data[$d->id_pbu]['total_jasaT_rek']      += $d->tambah * $d->jasa;
					$final_data[$d->id_pbu]['total_jasaK_rek']      += $d->kurang * $d->jasa;


					$data_pbl[$d->id_boq_lokasi]['material_sp_rek'] += $d->material * $d->sp;
					$data_pbl[$d->id_boq_lokasi]['jasa_sp_rek']     += $d->jasa * $d->sp;

					$data_pbl[$d->id_boq_lokasi]['material_rek']  += $d->material * $d->rekon;
					$data_pbl[$d->id_boq_lokasi]['jasa_rek']      += $d->jasa * $d->rekon;
					$data_pbl[$d->id_boq_lokasi]['materialT_rek'] += $d->tambah * $d->material;
					$data_pbl[$d->id_boq_lokasi]['materialK_rek'] += $d->kurang * $d->material;
					$data_pbl[$d->id_boq_lokasi]['jasaT_rek']     += $d->tambah * $d->jasa;
					$data_pbl[$d->id_boq_lokasi]['jasaK_rek']     += $d->kurang * $d->jasa;
				}
			}
			// dd($final_data, $data_pbl);
			DB::transaction(function () use ($final_data, $data_pbl){
				foreach ($final_data as $v) {
					DB::Table('procurement_boq_upload')->where('id', $v['id_pbu'])->update([
						"total_material_sp"          => $v['total_material_sp'],
						"total_jasa_sp"              => $v['total_jasa_sp'],
						"total_sp"		               => $v['total_sp'],
						"total_material_rek_sp_plan" => $v['total_material_rek_sp_plan'],
						"total_jasa_rek_sp_plan"     => $v['total_jasa_rek_sp_plan'],
						"total_material_rekon_plan"  => $v['total_material_rekon_plan'],
						"total_jasa_rekon_plan"      => $v['total_jasa_rekon_plan'],
						"total_materialT_rek_plan"   => $v['total_materialT_rek_plan'],
						"total_materialK_rek_plan"   => $v['total_materialK_rek_plan'],
						"total_jasaT_rek_plan"       => $v['total_jasaT_rek_plan'],
						"total_jasaK_rek_plan"       => $v['total_jasaK_rek_plan'],
						"total_material_rek_sp"      => $v['total_material_rek_sp'],
						"total_jasa_rek_sp"          => $v['total_jasa_rek_sp'],
						"total_material_rekon"       => $v['total_material_rekon'],
						"total_jasa_rekon"           => $v['total_jasa_rekon'],
						"total_materialT_rek"        => $v['total_materialT_rek'],
						"total_materialK_rek"        => $v['total_materialK_rek'],
						"total_jasaT_rek"            => $v['total_jasaT_rek'],
						"total_jasaK_rek"            => $v['total_jasaK_rek'],
					]);
				}

				foreach ($data_pbl as $vv) {
					DB::Table('procurement_Boq_lokasi')->where('id', $vv['id'])->update([
						"material_sp"          => $vv['material_sp'],
						"jasa_sp"              => $vv['jasa_sp'],
						"material_sp_rek_plan" => $vv['material_rek_sp_plan'],
						"jasa_sp_rek_plan"     => $vv['jasa_rek_sp_plan'],
						"material_rek_plan"    => $vv['material_rek_plan'],
						"jasa_rek_plan"        => $vv['jasa_rek_plan'],
						"materialT_rek_plan"   => $vv['materialT_rek_plan'],
						"materialK_rek_plan"   => $vv['materialK_rek_plan'],
						"jasaT_rek_plan"       => $vv['jasaT_rek_plan'],
						"jasaK_rek_plan"       => $vv['jasaK_rek_plan'],
						"material_sp_rek"      => $vv['material_sp_rek'],
						"jasa_sp_rek"          => $vv['jasa_sp_rek'],
						"material_rek"         => $vv['material_rek'],
						"jasa_rek"             => $vv['jasa_rek'],
						"materialT_rek"        => $vv['materialT_rek'],
						"materialK_rek"        => $vv['materialK_rek'],
						"jasaT_rek"            => $vv['jasaT_rek'],
						"jasaK_rek"            => $vv['jasaK_rek'],
					]);
				}
			});
		}

		$idp = [];

		$data_pbu = AdminModel::get_data_final($id);

		if(!in_array(session('auth')->proc_level, [4, 99, 44]) && in_array(session('auth')->proc_level, [3]) && !in_array($data_pbu->step_id, [9, 13, 20]) )
		{
			if(session('auth')->proc_level == 44)
			{
				$find_witel = DB::Table('promise_witel As w1')
				->leftjoin('promise_witel As w2', 'w2.Regional', '=', 'w1.Regional')
				->where('w1.Witel', session('auth')->Witel_New)
				->get();

				foreach($find_witel as $vw)
				{
					$all_witel[] = $vw->Witel;
				}

				if(!in_array($data_pbu->witel, $all_witel) )
				{
					return back()->with('alerts', [
						['type' => 'danger', 'text' => 'Gagal Karena Berbeda Witel!']
					]);
				}
			}
			elseif(session('auth')->Witel_New != $data_pbu->witel && session('auth')->proc_level == 4)
			{
				return back()->with('alerts', [
					['type' => 'danger', 'text' => 'Gagal Karena Berbeda Witel!']
				]);
			}

			return back()->with('alerts', [
				['type' => 'danger', 'text' => 'Gagal Karena Pekerjaan Sudah Jauh!']
			]);
		}

		$find_pid      = OperationModel::find_pid_byUp($id);
		$get_budget    = OperationModel::get_budget($id);
		$sub_jenis_all = AdminModel::get_pekerjaan($data_pbu->jenis_work, 'sub_jenis_all_sj');
		// dd($sub_jenis_all);
		foreach($find_pid as $v)
		{
			$check_pid = OperationModel::check_pid($v);

			if($check_pid->idp)
			{
				$idp[$check_pid->idp] = $check_pid->idp;
			}
			else
			{
				$wbs = explode(',', $v->idp);

				foreach($wbs as $vv)
				{
					$idp[$v->pid .'-'. $vv] = $v->pid .'-'. $vv;
				}
			}
		}

		$sd = 0;
		$ch_d = ReportModel::get_boq_data($id);

		if($ch_d->urutan == 7)
		{
			$sd = 2;
		}
		elseif($ch_d->urutan > 7)
		{
			$sd = 1;
		}

		$ll = OperationModel::get_lokasi($id);

		$save_pid = $save_pid_per_lok = [];

		foreach($ll as $k => $v)
		{
			if($v->pid_sto)
			{
				$exp = explode(',', $v->pid_sto);

				foreach($exp as $vvv)
				{
					$save_pid_per_lok[$v->id][] = trim($vvv);
				}
			}
			else
			{
				$get_list_pid = DB::table('procurement_Boq_lokasi As pbl')
				->LeftJoin('procurement_req_pid As prp', 'pbl.id', '=', 'prp.id_lokasi')
				->select('pbl.*', 'prp.id_pid_req As id_pid')
				->where('pbl.id', $v->id)->get();

				foreach($get_list_pid as $glp)
				{
					if($glp->id_pid)
					{
						$data = DB::SELECT("SELECT (SELECT CONCAT_WS(',',
						( CASE WHEN (SELECT SUM(pbd.material)
						FROM procurement_Boq_design pbd
						WHERE pbd.id_boq_lokasi = ".$glp->id." AND pbd.jenis_khs= 16) != 0 THEN CONCAT_WS('-', ppc.pid, '01-01-01' ) ELSE NULL END),
						( CASE WHEN (SELECT SUM(pbd.material)
						FROM procurement_Boq_design pbd
						WHERE pbd.id_boq_lokasi = ".$glp->id." AND (pbd.jenis_khs != 16 OR pbd.jenis_khs IS NULL)) != 0 THEN CONCAT_WS('-', ppc.pid, '01-01-02' ) ELSE NULL END),
						( CASE WHEN (SELECT SUM(pbd.jasa)
						FROM procurement_Boq_design pbd
						WHERE pbd.id_boq_lokasi = ".$glp->id.") != 0 THEN CONCAT_WS('-', ppc.pid, '01-02-01' ) ELSE NULL END) ) ) As idp
						FROM procurement_pid ppc
						WHERE ppc.id = ".$glp->id_pid)[0];

						if($data)
						{
							$exp = explode(',', $data->idp);

							foreach($exp as $vvv)
							{
								$save_pid_per_lok[$glp->id][] = trim($vvv);
							}
						}
					}
				}
			}
		}

		foreach($idp as $val)
		{
			$exp = explode(',', $val);

			foreach($exp as $v)
			{
				$save_pid[] = $v;
			}
		}

		$kerjaan  = ToolsModel::list_pekerjaan();
		$design   = AdminModel::get_design(3, $id);
		$data_pbu = AdminModel::get_data_final($id);
		$all_sto  = AdminModel::area_alamat($data_pbu->witel);

		$last_data = OperationModel::get_last_data($id, $sd);
		$data_item = $data_item_rw = [];

		// foreach($last_data as $val)
		// {
			// $data_item[$val->id_boq_lokasi][] = $val;

			// $last_design[$val->id_design]['id']       = $val->id_design;
			// $last_design[$val->id_design]['material'] = $val->material;
			// $last_design[$val->id_design]['jasa']     = $val->jasa;
		// }

		// $data_item = array_values($data_item);

		$data = AdminModel::get_data_final($id);
		$find_id_p = MitraModel::get_pid_by_id($id);

		if(!$save_pid)
		{
			$find_id_p = ReportModel::get_boq_data($id);
			if($find_id_p->id_project)
			{
				$save_pid = explode(', ', $find_id_p->id_project);
			}
		}

		$last_data_mat = AdminModel::get_last_material_boq($id, 'all');
		$order_mat_1 = $order_mat_0 = $order_mat_2 = [];

		foreach($last_data_mat as $v)
		{
			if($v->status_design == 1)
			{
				$order_mat_1[] = $v;
			}

			if($v->status_design == 2)
			{
				$order_mat_2[] = $v;
			}

			if($v->status_design == 0)
			{
				$order_mat_0[] = $v;
			}
		}

		$last_data_mat = $order_mat_0;

		if($ch_d->urutan == 7)
		{
			$last_data_mat = $order_mat_2;
		}
		elseif($ch_d->urutan > 7)
		{
			$last_data_mat = $order_mat_1;
		}

		// $last_data_mat = array_merge($order_mat_0, $order_mat_2, $order_mat_1);

		$data_material = [];

		foreach($last_data_mat as $k => $val)
		{
			$data_material[$val->id_design .'_'. $val->design_mitra_id]['id_design']       = $val->id_design;
			$data_material[$val->id_design .'_'. $val->design_mitra_id]['design_mitra_id'] = $val->design_mitra_id;
			$data_material[$val->id_design .'_'. $val->design_mitra_id]['jenis']           = $val->jenis;
			$data_material[$val->id_design .'_'. $val->design_mitra_id]['designator']      = $val->designator;
			$data_material[$val->id_design .'_'. $val->design_mitra_id]['uraian']          = html_entity_decode($val->uraian, ENT_QUOTES, 'UTF-8');
			$data_material[$val->id_design .'_'. $val->design_mitra_id]['jenis_khs']       = $val->jenis_khs;
			$data_material[$val->id_design .'_'. $val->design_mitra_id]['material']        = $val->material;
			$data_material[$val->id_design .'_'. $val->design_mitra_id]['jasa']            = $val->jasa;
			$data_material[$val->id_design .'_'. $val->design_mitra_id]['namcomp']         = $val->namcomp;
			$data_material[$val->id_design .'_'. $val->design_mitra_id]['sp']              = $val->sp;
			$data_material[$val->id_design .'_'. $val->design_mitra_id]['rekon']           = $val->rekon;
		}

		$data_material = array_values($data_material);

		foreach($last_data_mat as $val)
		{
			if(!isset($data_item_rw[$val->id_boq_lokasi]) )
			{
				foreach($data_material as $k => $v)
				{
					$data_item_rw[$val->id_boq_lokasi][$v['id_design'] .'-'. $v['jenis_khs'] ] = (object)[
						'id'              => 0,
						'id_boq_lokasi'   => $val->id_boq_lokasi,
						'lokasi' 				  => $val->lokasi,
						'sto'		 				  => $val->sto,
						'sub_jenis_p' 		=> $val->sub_jenis_p,
						'id_design'       => $v['id_design'],
						'jenis_khs'       => $v['jenis_khs'],
						'designator'      => $v['designator'],
						'material'        => $v['material'],
						'jasa'            => $v['jasa'],
						'sp'              => 0,
						'rekon'           => 0,
						'tambah'          => 0,
						'kurang'          => 0,
						'status_design'   => 0,
						'jenis'           => $v['jenis'],
						'namcomp'         => $v['namcomp'],
						'design_mitra_id' => $v['design_mitra_id'],
						'uraian'          => $v['uraian'],
						'new_boq'         => $val->new_boq,
					];
				}
			}

			if($data_item_rw[$val->id_boq_lokasi][$val->id_design .'-'. $val->jenis_khs])
			{
				$data_item_rw[$val->id_boq_lokasi][$val->id_design .'-'. $val->jenis_khs ]->id              = $val->id;
				$data_item_rw[$val->id_boq_lokasi][$val->id_design .'-'. $val->jenis_khs ]->sp              = $val->sp;
				$data_item_rw[$val->id_boq_lokasi][$val->id_design .'-'. $val->jenis_khs ]->rekon           = $val->rekon;
				$data_item_rw[$val->id_boq_lokasi][$val->id_design .'-'. $val->jenis_khs ]->tambah          = $val->tambah;
				$data_item_rw[$val->id_boq_lokasi][$val->id_design .'-'. $val->jenis_khs ]->kurang          = $val->kurang;
				$data_item_rw[$val->id_boq_lokasi][$val->id_design .'-'. $val->jenis_khs ]->material        = $val->material;
				$data_item_rw[$val->id_boq_lokasi][$val->id_design .'-'. $val->jenis_khs ]->jasa            = $val->jasa;
				$data_item_rw[$val->id_boq_lokasi][$val->id_design .'-'. $val->jenis_khs ]->jenis           = $val->jenis;
				$data_item_rw[$val->id_boq_lokasi][$val->id_design .'-'. $val->jenis_khs ]->namcomp         = $val->namcomp;
				$data_item_rw[$val->id_boq_lokasi][$val->id_design .'-'. $val->jenis_khs ]->design_mitra_id = $val->design_mitra_id;
			}
		}

		foreach($data_item_rw as $k => $v)
		{
			$data_item[] = array_values($v);
		}

		$step = $data_pbu->step_id;

		if($data_pbu->action_ps != 'forward')
		{
			$step = $data_pbu->step_after;
		}

		$save_pid = array_unique(array_map(function($x){
			return trim($x);
		}, $save_pid) );

		$check_pasca = DB::SELECT("SELECT * FROM procurement_boq_upload a
		LEFT JOIN procurement_Boq_lokasi b ON a.id = b.id_upload
		LEFT JOIN procurement_Boq_design c ON c.id_boq_lokasi = b.id
		WHERE a.id = $id AND c.status_design != 0");

		if(!in_array(session('auth')->proc_level, [3, 4, 44, 99] ) )
		{
			$check_pasca = [];
		}

		return view('Operation.edit_pekerjaan', compact('kerjaan', 'step', 'data_material', 'data_pbu', 'sub_jenis_all', 'all_sto', 'design', 'data', 'data_item', 'save_pid_per_lok', 'get_budget'), ['pid' => $save_pid, 'sync_block' => count($check_pasca)]);
	}

	public function update_po(Request $req, $id)
	{
		return OperationModel::update_po($req, $id);
	}

	public function check_judul(Request $req)
	{
		$msg = ReportModel::check_judul($req);
		return \Response::json($msg);
	}

	public function check_sub_pekerjaan(Request $req)
	{
		$chck_jenis = AdminModel::find_work($req->pekerjaan, 'jenis');

		$data = [];
		if($chck_jenis)
		{
			foreach($chck_jenis as $v)
			{
				$data [] = ['id' => $v->sub_jenis, 'text' => $v->sub_jenis];
			}
		}

		return \Response::json($data);
	}

	public function return_po($id)
	{
		$data = OperationModel::return_po($id);

		return redirect('/progress/input')->with('alerts', [
			['type' => 'info', 'text' => 'Pekerjaan '. $data->judul .' Berhasil Dikembalikan!']
		]);
	}

	public function edit_kolom($id)
	{
		$idp = [];

		$data_pbu = AdminModel::get_data_final($id);

		if(!in_array(session('auth')->proc_level, [4, 99, 44]) && in_array(session('auth')->proc_level, [3]) && !in_array($data_pbu->step_id, [9, 13, 20]) )
		{
			if(session('auth')->proc_level == 44)
			{
				$find_witel = DB::Table('promise_witel As w1')
				->leftjoin('promise_witel As w2', 'w2.Regional', '=', 'w1.Regional')
				->where('w1.Witel', session('auth')->Witel_New)
				->get();

				foreach($find_witel as $vw)
				{
					$all_witel[] = $vw->Witel;
				}

				if(!in_array($data_pbu->witel, $all_witel) )
				{
					return back()->with('alerts', [
						['type' => 'danger', 'text' => 'Gagal Karena Berbeda Witel!']
					]);
				}
			}
		}
		elseif(!in_array(session('auth')->proc_level, [4, 99, 44]) && in_array(session('auth')->proc_level, [3]) && !in_array($data_pbu->step_id, [13, 20]) )
		{
			if(session('auth')->Witel_New != $data_pbu->witel && session('auth')->proc_level == 4)
			{
				return back()->with('alerts', [
					['type' => 'danger', 'text' => 'Gagal Karena Berbeda Witel!']
				]);
			}

			return back()->with('alerts', [
				['type' => 'danger', 'text' => 'Gagal Karena Pekerjaan Sudah Jauh!']
			]);
		}

		$find_pid      = OperationModel::find_pid_byUp($id);
		$get_budget    = OperationModel::get_budget($id);
		$sub_jenis_all = AdminModel::get_pekerjaan($data_pbu->jenis_work, 'sub_jenis_all_sj');

		foreach($find_pid as $v)
		{
			$check_pid = OperationModel::check_pid($v);

			$idp[$check_pid->idp] = $check_pid->idp;
		}

		$sd = 0;
		$ch_d = ReportModel::get_boq_data($id);

		$kerjaan  = ToolsModel::list_pekerjaan();
		$design   = AdminModel::get_design(3, $id);
		$data_pbu = AdminModel::get_data_final($id);
		$all_sto  = AdminModel::area_alamat($data_pbu->witel);

		$last_data = OperationModel::get_last_data($id, $sd);
		$data_item = $data_item_rw = [];

		$data = AdminModel::get_data_final($id);
		$find_id_p = MitraModel::get_pid_by_id($id);

		$last_data_mat = AdminModel::get_last_material_boq($id);

		$data_material = [];

		foreach($last_data_mat as $k => $val)
		{
			$data_material[$val->id_design .'_'. $val->design_mitra_id]['id_design']       = $val->id_design;
			$data_material[$val->id_design .'_'. $val->design_mitra_id]['design_mitra_id'] = $val->design_mitra_id;
			$data_material[$val->id_design .'_'. $val->design_mitra_id]['jenis']           = $val->jenis;
			$data_material[$val->id_design .'_'. $val->design_mitra_id]['designator']      = $val->designator;
			$data_material[$val->id_design .'_'. $val->design_mitra_id]['uraian']          = html_entity_decode($val->uraian, ENT_QUOTES, 'UTF-8');
			$data_material[$val->id_design .'_'. $val->design_mitra_id]['jenis_khs']       = $val->jenis_khs;
			$data_material[$val->id_design .'_'. $val->design_mitra_id]['material']        = $val->material;
			$data_material[$val->id_design .'_'. $val->design_mitra_id]['jasa']            = $val->jasa;
			$data_material[$val->id_design .'_'. $val->design_mitra_id]['namcomp']         = $val->namcomp;
			$data_material[$val->id_design .'_'. $val->design_mitra_id]['sp']              = $val->sp;
			$data_material[$val->id_design .'_'. $val->design_mitra_id]['rekon']           = $val->rekon;
		}

		$data_material = array_values($data_material);

		foreach($last_data_mat as $val)
		{
			if(!$data_item_rw)
			{
				foreach($data_material as $k => $v)
				{
					$data_item_rw[$v['id_design'] .'-'. $v['jenis_khs'] ] = (object)[
						'id_design'       => $v['id_design'],
						'jenis_khs'       => $v['jenis_khs'],
						'designator'      => $v['designator'],
						'material'        => $v['material'],
						'jasa'            => $v['jasa'],
						'sp'              => 0,
						'rekon'           => 0,
						'tambah'          => 0,
						'kurang'          => 0,
						'status_design'   => 0,
						'jenis'           => $v['jenis'],
						'namcomp'         => $v['namcomp'],
						'design_mitra_id' => $v['design_mitra_id'],
						'uraian'          => $v['uraian'],
						'new_boq'         => $val->new_boq,
					];
				}
			}

			if(@$data_item_rw[$val->id_design .'-'. $val->jenis_khs])
			{
				$data_item_rw[$val->id_design .'-'. $val->jenis_khs ]->sp             += $val->sp;
				$data_item_rw[$val->id_design .'-'. $val->jenis_khs ]->rekon          += $val->rekon;
				$data_item_rw[$val->id_design .'-'. $val->jenis_khs ]->tambah         += $val->tambah;
				$data_item_rw[$val->id_design .'-'. $val->jenis_khs ]->kurang         += $val->kurang;
				$data_item_rw[$val->id_design .'-'. $val->jenis_khs ]->material        = $val->material;
				$data_item_rw[$val->id_design .'-'. $val->jenis_khs ]->jasa            = $val->jasa;
				$data_item_rw[$val->id_design .'-'. $val->jenis_khs ]->jenis           = $val->jenis;
				$data_item_rw[$val->id_design .'-'. $val->jenis_khs ]->namcomp         = $val->namcomp;
				$data_item_rw[$val->id_design .'-'. $val->jenis_khs ]->design_mitra_id = $val->design_mitra_id;
			}
		}
		// dd($data_item_rw);
		$data_item[] = array_values($data_item_rw);

		$step = $data_pbu->step_id;

		if($data_pbu->action_ps != 'forward')
		{
			$step = $data_pbu->step_after;
		}

		return view('Operation.edit_kolom', compact('kerjaan', 'step', 'data_material', 'data_pbu', 'sub_jenis_all', 'all_sto', 'design', 'data', 'data_item') );
	}

	public function update_kolom($id, Request $req)
	{
		$msg = OperationModel::update_kolom($id, $req);
    return redirect($msg['direct'])->with('alerts', $msg['isi']);
	}
}