<?php

namespace App\Http\Controllers;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Events\BeforeSheet;
use Maatwebsite\Excel\Events\BeforeWriting;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use Maatwebsite\Excel\Excel;
use DB;
class Excel_DokPelengkapSkemaOrderBasedxOrder implements  WithEvents
{
    use Exportable;

    public function __construct($data, $output_file)
    {
        $this->data = $data;
        $this->output_file = $output_file;
    }

    public function convert_month($date, $cetak_hari = false)
    {
        $bulan = array (1 =>   'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );

        $hari = array ( 1 => 'Senin',
            'Selasa',
            'Rabu',
            'Kamis',
            'Jumat',
            'Sabtu',
            'Minggu'
        );

        $result = date('d m Y', strtotime($date) );
        $split = explode(' ', $result);
        $hasilnya = $split[0] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[2];

        if($cetak_hari)
        {
            $num = date('N', strtotime($date) );
            return $hari[$num].' '.$hasilnya;
        }

        return $hasilnya;
    }

    public function only_day($date)
    {
        $hari = array ( 1 => 'Senin',
            'Selasa',
            'Rabu',
            'Kamis',
            'Jumat',
            'Sabtu',
            'Minggu'
        );

        $num = date('N', strtotime($date) );
        return $hari[$num];
    }

    public function terbilang($nilai)
    {
        if ($nilai < 0)
        {
            $hasil = "minus " . trim($this->penyebut($nilai) );
        }
        else
        {
            $hasil = trim($this->penyebut($nilai) );
        }

        return $hasil;
    }

    public function penyebut($nilai)
    {
        $nilai = abs($nilai);
        $huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
        $temp = "";

        if($nilai < 12)
        {
            $temp = " " . $huruf[$nilai];
        }
        else if($nilai < 20)
        {
            $temp = $this->penyebut($nilai - 10) . " belas";
        }
        else if($nilai < 100)
        {
            $temp = $this->penyebut($nilai / 10) . " puluh" . $this->penyebut($nilai % 10);
        }
        else if($nilai < 200)
        {
            $temp = " seratus" . $this->penyebut($nilai - 100);
        }
        else if($nilai < 1000)
        {
            $temp = $this->penyebut($nilai / 100) . " ratus" . $this->penyebut($nilai % 100);
        }
        else if($nilai < 2000)
        {
            $temp = " seribu" . $this->penyebut($nilai - 1000);
        }
        else if($nilai < 1000000)
        {
            $temp = $this->penyebut($nilai / 1000) . " ribu" . $this->penyebut($nilai % 1000);
        }
        else if($nilai < 1000000000)
        {
            $temp = $this->penyebut($nilai / 1000000) . " juta" . $this->penyebut($nilai % 1000000);
        }
        else if($nilai < 1000000000000)
        {
            $temp = $this->penyebut($nilai / 1000000000) . " milyar" . $this->penyebut(fmod($nilai, 1000000000) );
        }
        else if($nilai < 1000000000000000)
        {
            $temp = $this->penyebut($nilai / 1000000000000) . " trilyun" . $this->penyebut(fmod($nilai, 1000000000000) );
        }

        return$temp;
    }

    public function ttd_user($rule)
    {
        return DB::table('procurement_dok_ttd')
        ->leftJoin('procurement_user', 'procurement_dok_ttd.id_user', '=', 'procurement_user.id')
        ->select('procurement_user.*')
        ->where('procurement_dok_ttd.witel', $this->data->witel)
        ->where('procurement_dok_ttd.pekerjaan', 'PSB')
        ->where('procurement_dok_ttd.rule_dok', $rule)
        ->first();
    }

    public function verificator_proc($nik)
    {
        return DB::table('1_2_employee')->where('nik', $nik)->first();
    }

    public function registerEvents(): array
    {
        return [
            BeforeExport::class  => function(BeforeExport $event) {

                $border_Style = [
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
                        ],
                    ],
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    ]
                ];

                $reader = new Xlsx();
                \PhpOffice\PhpSpreadsheet\Shared\File::setUseUploadTempDirectory(true);
                $spreadSheet = $reader->load(public_path(). '/template_doc/psb/beta_ho/v2_manual/DokPelengkap_Skema_OrderBasedxOrder.xlsx');

                if (in_array(date('m'), [1, 2, 3]))
                {
                    // Q1
                    $kpi_target_pspi = 96;
                    $kpi_target_tticomply = 91;
                } elseif (in_array(date('m'), [4, 5, 6])) {
                    // Q2
                    $kpi_target_pspi = 97;
                    $kpi_target_tticomply = 95;
                } elseif (in_array(date('m'), [7, 8, 9, 10, 11, 12])) {
                    // Q3 Q4
                    $kpi_target_pspi = 98;
                    $kpi_target_tticomply = 99;
                };

                $kpi_pspi = @round( (str_replace(',', '.', $this->data->kpi_real_pspi) / $kpi_target_pspi) * 100, 2);
                $kpi_tticomply = @round( (str_replace(',', '.', $this->data->kpi_real_tticomply) / $kpi_target_tticomply) * 100, 2);
                $kpi_ffg = @round( (str_replace(',', '.', $this->data->kpi_real_ffg) / 99) * 100, 2);
                $kpi_validasicore = @round( (str_replace(',', '.', $this->data->kpi_real_validasicore) / 100) * 100, 2);
                $kpi_ttrffg = @round( (str_replace(',', '.', $this->data->kpi_real_ttrffg) / 99) * 100, 2);
                $kpi_ujipetik = @round( (str_replace(',', '.', $this->data->kpi_real_ujipetik) / 99) * 100, 2);
                $kpi_qc2 = @round( (str_replace(',', '.', $this->data->kpi_real_qc2) / 99) * 100, 2);

                $ach_kpi_pspi = @round( ($kpi_pspi * 25) / 100, 2);
                $ach_kpi_tticomply = @round( ($kpi_tticomply * 25) / 100, 2);
                $ach_kpi_ffg = @round( ($kpi_ffg * 15) / 100, 2);
                $ach_kpi_validasicore = @round( ($kpi_validasicore * 5) / 100, 2);
                $ach_kpi_ttrffg = @round( ($kpi_ttrffg * 10) / 100, 2);
                $ach_kpi_ujipetik = @round( ($kpi_ujipetik * 10) / 100, 2);
                $ach_kpi_qc2 = @round( ($kpi_qc2 * 10) / 100, 2);

                $ach_kpi = ($ach_kpi_pspi + $ach_kpi_tticomply + $ach_kpi_ffg + $ach_kpi_validasicore + $ach_kpi_ttrffg + $ach_kpi_ujipetik + $ach_kpi_qc2);
                
                if ($ach_kpi > 100)
                {
                    $kpi = 100;
                } elseif ($ach_kpi < 90) {
                    $kpi = 90;
                } else {
                    $kpi = $ach_kpi;
                };

                $jml_pekerjaan_psb = 
                    ($this->data->p1_survey_ssl * $this->data->p1_survey_hss) +
                    ($this->data->p2_tlpint_survey_ssl * $this->data->p2_tlpint_survey_hss) +
                    ($this->data->p2_intiptv_survey_ssl * $this->data->p2_intiptv_survey_hss) + 
                    ($this->data->p3_survey_ssl * $this->data->p3_survey_hss) +

                    ($this->data->p1_ssl * $this->data->p1_hss) +
                    ($this->data->p2_ssl * $this->data->p2_hss) + // versi khs 2021
                    ($this->data->p2_tlpint_ssl * $this->data->p2_tlpint_hss) +
                    ($this->data->p2_intiptv_ssl * $this->data->p2_intiptv_hss) + 
                    ($this->data->p3_ssl * $this->data->p3_hss);

                $jml_pekerjaan_migrasi =
                    ($this->data->migrasi_service_1p2p_ssl * $this->data->migrasi_service_1p2p_hss) +
                    ($this->data->migrasi_service_1p3p_ssl * $this->data->migrasi_service_1p3p_hss) +
                    ($this->data->migrasi_service_2p3p_ssl * $this->data->migrasi_service_2p3p_hss);

                $jml_pekerjaan_tambahan =
                    ($this->data->ikr_addon_stb_ssl * $this->data->ikr_addon_stb_hss) +
                    ($this->data->indihome_smart_ssl * $this->data->indihome_smart_hss) +
                    ($this->data->wifiextender_ssl * $this->data->wifiextender_hss) +
                    ($this->data->plc_ssl * $this->data->plc_hss) +
                    ($this->data->lme_wifi_pt1_ssl * $this->data->lme_wifi_pt1_hss) +
                    ($this->data->lme_ap_indoor_ssl * $this->data->lme_ap_indoor_hss) +
                    ($this->data->lme_ap_outdoor_ssl * $this->data->lme_ap_outdoor_hss) +
                    ($this->data->ont_premium_ssl * $this->data->ont_premium_hss) +
                    ($this->data->migrasi_stb_ssl * $this->data->migrasi_stb_hss) +
                    ($this->data->instalasi_ipcam_ssl * $this->data->instalasi_ipcam_hss) +
                    ($this->data->pu_s7_140_ssl * $this->data->pu_s7_140_hss) +
                    ($this->data->pu_s9_140_ssl * $this->data->pu_s9_140_hss);

                $imbal_jasa = (($kpi / 100) * $jml_pekerjaan_psb) + ($jml_pekerjaan_migrasi + $jml_pekerjaan_tambahan);

                $nilai_dpp = substr_replace(floor($imbal_jasa), '000', -3, 3);
                $nilai_ppn = $nilai_dpp * (str_replace(',', '.', $this->data->ppn_numb) / 100);
		        $nilai_pph = $nilai_dpp * (str_replace(',', '.', $this->data->pph_mitra) / 100);
                $nilai_kwitansi = ($nilai_dpp + $nilai_ppn);

                $spreadSheet->getSheet(0)->getCell('B1')->SetValue('Pekerjaan '.$this->data->jenis_work.' Periode '.$this->data->month.' '.date('Y', strtotime($this->data->rekon_periode2)).' Witel '. ucfirst(strtolower($this->data->witel)));
                $spreadSheet->getSheet(0)->getCell('B2')->SetValue($this->data->nama_company);
                $spreadSheet->getSheet(0)->getCell('B3')->SetValue($this->data->alamat_company);
                $spreadSheet->getSheet(0)->getCell('B4')->SetValue($this->data->telp_mitra);
                $spreadSheet->getSheet(0)->getCell('B5')->SetValue($this->data->wakil_mitra);
                $spreadSheet->getSheet(0)->getCell('B6')->SetValue($this->data->jabatan_mitra);
                $spreadSheet->getSheet(0)->getCell('B7')->SetValue($this->data->rek);
                $spreadSheet->getSheet(0)->getCell('B8')->SetValue($this->data->bank);
                $spreadSheet->getSheet(0)->getCell('B9')->SetValue($this->data->atas_nama);
                $spreadSheet->getSheet(0)->getCell('B10')->SetValue($this->data->cabang_bank);
                $spreadSheet->getSheet(0)->getCell('B11')->SetValue($this->data->NPWP);
                $spreadSheet->getSheet(0)->getCell('B12')->SetValue($this->data->lokasi_npwp);
                $spreadSheet->getSheet(0)->getCell('B13')->SetValue($this->data->no_amandemen1);
                $spreadSheet->getSheet(0)->getCell('B14')->SetValue($this->convert_month($this->data->date_amandemen1));
                $spreadSheet->getSheet(0)->getCell('B15')->SetValue($this->data->no_amandemen2);
                $spreadSheet->getSheet(0)->getCell('B16')->SetValue($this->convert_month($this->data->date_amandemen2));
                $spreadSheet->getSheet(0)->getCell('B17')->SetValue($this->data->no_amandemen3);
                $spreadSheet->getSheet(0)->getCell('B18')->SetValue($this->convert_month($this->data->date_amandemen3));
                $spreadSheet->getSheet(0)->getCell('B19')->SetValue($this->data->no_amandemen4);
                $spreadSheet->getSheet(0)->getCell('B20')->SetValue($this->convert_month($this->data->date_amandemen4));
                $spreadSheet->getSheet(0)->getCell('B21')->SetValue($this->data->no_amandemen5);
                $spreadSheet->getSheet(0)->getCell('B22')->SetValue($this->convert_month($this->data->date_amandemen5));
                $spreadSheet->getSheet(0)->getCell('B23')->SetValue($this->data->no_kontrak_ta);
                $spreadSheet->getSheet(0)->getCell('B24')->SetValue($this->convert_month($this->data->date_kontrak_ta));
                $spreadSheet->getSheet(0)->getCell('B25')->SetValue($this->data->no_penetapan_khs);
                $spreadSheet->getSheet(0)->getCell('B26')->SetValue($this->convert_month($this->data->date_khs_penetapan));
                $spreadSheet->getSheet(0)->getCell('B27')->SetValue($this->data->no_kesanggupan_khs);
                $spreadSheet->getSheet(0)->getCell('B28')->SetValue($this->convert_month($this->data->date_khs_kesanggupan));
                $spreadSheet->getSheet(0)->getCell('B29')->SetValue($this->convert_month($this->data->rekon_periode1));
                $spreadSheet->getSheet(0)->getCell('B30')->SetValue($this->convert_month($this->data->rekon_periode2));
                $spreadSheet->getSheet(0)->getCell('B31')->SetValue($this->data->bulan);
                $spreadSheet->getSheet(0)->getCell('B32')->SetValue($this->data->area);
                $spreadSheet->getSheet(0)->getCell('B33')->SetValue($this->data->area_sto);
                $spreadSheet->getSheet(0)->getCell('B34')->SetValue($this->data->id_regional);
                $spreadSheet->getSheet(0)->getCell('B35')->SetValue($this->data->regional);
                $spreadSheet->getSheet(0)->getCell('B36')->SetValue(ucwords(strtolower($this->data->witel)));
                $spreadSheet->getSheet(0)->getCell('B37')->SetValue($this->data->id_project);
                $spreadSheet->getSheet(0)->getCell('B38')->SetValue($this->data->pph_mitra);
                $spreadSheet->getSheet(0)->getCell('B39')->SetValue($kpi);
                $spreadSheet->getSheet(0)->getCell('B40')->SetValue($this->data->no_sp);
                $spreadSheet->getSheet(0)->getCell('B41')->SetValue($this->convert_month($this->data->tgl_sp));
                $spreadSheet->getSheet(0)->getCell('B42')->SetValue($this->data->invoice);
                $spreadSheet->getSheet(0)->getCell('B43')->SetValue($this->data->spp_num);
                $spreadSheet->getSheet(0)->getCell('B44')->SetValue($this->data->receipt_num);
                $spreadSheet->getSheet(0)->getCell('B45')->SetValue($this->data->faktur);
                $spreadSheet->getSheet(0)->getCell('B46')->SetValue($this->convert_month($this->data->tgl_faktur));
                $spreadSheet->getSheet(0)->getCell('B47')->SetValue($this->data->no_ba_pemeriksaan);
                $spreadSheet->getSheet(0)->getCell('B48')->SetValue($this->data->BAUT);
                $spreadSheet->getSheet(0)->getCell('B49')->SetValue($this->data->amd_sp);
                $spreadSheet->getSheet(0)->getCell('B50')->SetValue($this->data->no_baij);
                $spreadSheet->getSheet(0)->getCell('B51')->SetValue($this->data->no_bap);
                $spreadSheet->getSheet(0)->getCell('B52')->SetValue($this->data->BAST);
                $spreadSheet->getSheet(0)->getCell('B53')->SetValue($this->data->no_ba_pemeliharaan);
                $spreadSheet->getSheet(0)->getCell('B54')->SetValue($this->only_day($this->data->rekon_periode2));
                $spreadSheet->getSheet(0)->getCell('B55')->SetValue(ucwords($this->terbilang(date('d', strtotime($this->data->rekon_periode2)))));
                $spreadSheet->getSheet(0)->getCell('B56')->SetValue($this->data->month);
                $spreadSheet->getSheet(0)->getCell('B57')->SetValue(ucwords($this->terbilang(date('Y', strtotime($this->data->rekon_periode2)))));
                $spreadSheet->getSheet(0)->getCell('B58')->SetValue(date('d/m/Y', strtotime($this->data->rekon_periode2)));
                $spreadSheet->getSheet(0)->getCell('B59')->SetValue($this->ttd_user('mngr_krj')->nik);
                $spreadSheet->getSheet(0)->getCell('B60')->SetValue($this->ttd_user('mngr_krj')->user);
                $spreadSheet->getSheet(0)->getCell('B61')->SetValue($this->ttd_user('mngr_krj')->jabatan);
                $spreadSheet->getSheet(0)->getCell('B62')->SetValue($this->ttd_user('m_fin')->nik);
                $spreadSheet->getSheet(0)->getCell('B63')->SetValue($this->ttd_user('m_fin')->user);
                $spreadSheet->getSheet(0)->getCell('B64')->SetValue($this->ttd_user('gm')->user);
                $spreadSheet->getSheet(0)->getCell('B65')->SetValue($this->ttd_user('gm')->nik);
                $spreadSheet->getSheet(0)->getCell('B66')->SetValue($this->ttd_user('gm')->jabatan);
                $spreadSheet->getSheet(0)->getCell('B67')->SetValue($this->ttd_user('osm')->user);
                $spreadSheet->getSheet(0)->getCell('B68')->SetValue($this->ttd_user('osm')->nik);
                $spreadSheet->getSheet(0)->getCell('B69')->SetValue($this->ttd_user('osm')->jabatan);
                $spreadSheet->getSheet(0)->getCell('B70')->SetValue($this->verificator_proc($this->data->verificator_by)->nik);
                $spreadSheet->getSheet(0)->getCell('B71')->SetValue($this->verificator_proc($this->data->verificator_by)->nama);

                if ($nilai_dpp > 0 && $nilai_dpp <= 200000000)
                {    
                    $rule = 'krglbh_dua_jt';
                } elseif ($nilai_dpp > 200000000 && $nilai_dpp <= 500000000) {
                    $rule = 'antr_dualima_jt';
                } else {
                    $rule = 'lbh_lima_jt';

                    $spreadSheet->getSheet(1)->getCell('E29')->SetValue(strtoupper($this->ttd_user('mngr_krj')->jabatan));
                    $spreadSheet->getSheet(1)->getCell('E35')->SetValue(strtoupper($this->ttd_user('mngr_krj')->user));
                    $spreadSheet->getSheet(1)->getCell('E36')->SetValue('NIK. ' . $this->ttd_user('mngr_krj')->nik);
                }
                $spreadSheet->getSheet(0)->getCell('B72')->SetValue($this->ttd_user($rule)->nik);
                $spreadSheet->getSheet(0)->getCell('B73')->SetValue(strtoupper($this->ttd_user($rule)->user));
                $spreadSheet->getSheet(0)->getCell('B74')->SetValue($this->ttd_user($rule)->jabatan);

                $spreadSheet->getSheet(0)->getCell('B75')->SetValue($kpi_target_pspi.'%');
                $spreadSheet->getSheet(0)->getCell('B76')->SetValue($this->data->kpi_real_pspi.'%');
                $spreadSheet->getSheet(0)->getCell('B77')->SetValue($kpi_target_tticomply.'%');
                $spreadSheet->getSheet(0)->getCell('B78')->SetValue($this->data->kpi_real_tticomply.'%');
                $spreadSheet->getSheet(0)->getCell('B79')->SetValue('99%');
                $spreadSheet->getSheet(0)->getCell('B80')->SetValue($this->data->kpi_real_ffg.'%');
                $spreadSheet->getSheet(0)->getCell('B81')->SetValue('100%');
                $spreadSheet->getSheet(0)->getCell('B82')->SetValue($this->data->kpi_real_validasicore.'%');
                $spreadSheet->getSheet(0)->getCell('B83')->SetValue('99%');
                $spreadSheet->getSheet(0)->getCell('B84')->SetValue($this->data->kpi_real_ttrffg.'%');
                $spreadSheet->getSheet(0)->getCell('B85')->SetValue('99%');
                $spreadSheet->getSheet(0)->getCell('B86')->SetValue($this->data->kpi_real_ujipetik.'%');
                $spreadSheet->getSheet(0)->getCell('B87')->SetValue('99%');
                $spreadSheet->getSheet(0)->getCell('B88')->SetValue($this->data->kpi_real_qc2.'%');
                $spreadSheet->getSheet(0)->getCell('B89')->SetValue($nilai_dpp);
                $spreadSheet->getSheet(0)->getCell('B90')->SetValue($this->data->ppn_numb);
                $spreadSheet->getSheet(0)->getCell('B91')->SetValue(ucwords($this->terbilang($nilai_kwitansi)));
                $spreadSheet->getSheet(0)->getCell('B92')->SetValue(ucwords($this->terbilang($nilai_dpp)));
                $spreadSheet->getSheet(0)->getCell('B93')->SetValue('');
                $spreadSheet->getSheet(0)->getCell('B94')->SetValue('');
                $spreadSheet->getSheet(0)->getCell('B95')->SetValue($this->ttd_user('sm_krj')->nik);
                $spreadSheet->getSheet(0)->getCell('B96')->SetValue($this->ttd_user('sm_krj')->user);
                $spreadSheet->getSheet(0)->getCell('B97')->SetValue($this->ttd_user('sm_krj')->jabatan);
                $spreadSheet->getSheet(0)->getCell('B98')->SetValue($this->ttd_user('invt_cntrl')->nik);
                $spreadSheet->getSheet(0)->getCell('B99')->SetValue($this->ttd_user('invt_cntrl')->user);
                $spreadSheet->getSheet(0)->getCell('B100')->SetValue($this->ttd_user('invt_cntrl')->jabatan);
                $spreadSheet->getSheet(0)->getCell('B101')->SetValue($this->ttd_user('mngr_ss')->nik);
                $spreadSheet->getSheet(0)->getCell('B102')->SetValue($this->ttd_user('mngr_ss')->user);
                $spreadSheet->getSheet(0)->getCell('B103')->SetValue($this->ttd_user('mngr_ss')->jabatan);
                $spreadSheet->getSheet(0)->getCell('B104')->SetValue($this->data->p1_survey_hss);
                $spreadSheet->getSheet(0)->getCell('B105')->SetValue($this->data->p1_survey_ssl);
                $spreadSheet->getSheet(0)->getCell('B106')->SetValue($this->data->p2_tlpint_survey_hss);
                $spreadSheet->getSheet(0)->getCell('B107')->SetValue($this->data->p2_tlpint_survey_ssl);
                $spreadSheet->getSheet(0)->getCell('B108')->SetValue($this->data->p2_intiptv_survey_hss);
                $spreadSheet->getSheet(0)->getCell('B109')->SetValue($this->data->p2_intiptv_survey_ssl);
                $spreadSheet->getSheet(0)->getCell('B110')->SetValue($this->data->p3_survey_hss);
                $spreadSheet->getSheet(0)->getCell('B111')->SetValue($this->data->p3_survey_ssl);
                $spreadSheet->getSheet(0)->getCell('B112')->SetValue($this->data->p1_hss);
                $spreadSheet->getSheet(0)->getCell('B113')->SetValue($this->data->p1_ssl);
                $spreadSheet->getSheet(0)->getCell('B114')->SetValue($this->data->p2_tlpint_hss);
                $spreadSheet->getSheet(0)->getCell('B115')->SetValue($this->data->p2_tlpint_ssl);
                $spreadSheet->getSheet(0)->getCell('B116')->SetValue($this->data->p2_intiptv_hss);
                $spreadSheet->getSheet(0)->getCell('B117')->SetValue($this->data->p2_intiptv_ssl);
                $spreadSheet->getSheet(0)->getCell('B118')->SetValue($this->data->p3_hss);
                $spreadSheet->getSheet(0)->getCell('B119')->SetValue($this->data->p3_ssl);
                $spreadSheet->getSheet(0)->getCell('B120')->SetValue($this->data->migrasi_service_1p2p_hss);
                $spreadSheet->getSheet(0)->getCell('B121')->SetValue($this->data->migrasi_service_1p2p_ssl);
                $spreadSheet->getSheet(0)->getCell('B122')->SetValue($this->data->migrasi_service_1p3p_hss);
                $spreadSheet->getSheet(0)->getCell('B123')->SetValue($this->data->migrasi_service_1p3p_ssl);
                $spreadSheet->getSheet(0)->getCell('B124')->SetValue($this->data->migrasi_service_2p3p_hss);
                $spreadSheet->getSheet(0)->getCell('B125')->SetValue($this->data->migrasi_service_2p3p_ssl);
                $spreadSheet->getSheet(0)->getCell('B126')->SetValue($this->data->ikr_addon_stb_hss);
                $spreadSheet->getSheet(0)->getCell('B127')->SetValue($this->data->ikr_addon_stb_ssl);
                $spreadSheet->getSheet(0)->getCell('B128')->SetValue($this->data->indihome_smart_hss);
                $spreadSheet->getSheet(0)->getCell('B129')->SetValue($this->data->indihome_smart_ssl);
                $spreadSheet->getSheet(0)->getCell('B130')->SetValue($this->data->wifiextender_hss);
                $spreadSheet->getSheet(0)->getCell('B131')->SetValue($this->data->wifiextender_ssl);
                $spreadSheet->getSheet(0)->getCell('B132')->SetValue($this->data->plc_hss);
                $spreadSheet->getSheet(0)->getCell('B133')->SetValue($this->data->plc_ssl);
                $spreadSheet->getSheet(0)->getCell('B134')->SetValue($this->data->lme_wifi_pt1_hss);
                $spreadSheet->getSheet(0)->getCell('B135')->SetValue($this->data->lme_wifi_pt1_ssl);
                $spreadSheet->getSheet(0)->getCell('B136')->SetValue($this->data->ont_premium_hss);
                $spreadSheet->getSheet(0)->getCell('B137')->SetValue($this->data->ont_premium_ssl);
                $spreadSheet->getSheet(0)->getCell('B138')->SetValue($this->data->migrasi_stb_hss);
                $spreadSheet->getSheet(0)->getCell('B139')->SetValue($this->data->migrasi_stb_ssl);
                $spreadSheet->getSheet(0)->getCell('B140')->SetValue($this->data->lme_ap_indoor_hss);
                $spreadSheet->getSheet(0)->getCell('B141')->SetValue($this->data->lme_ap_indoor_ssl);
                $spreadSheet->getSheet(0)->getCell('B142')->SetValue($this->data->instalasi_ipcam_hss);
                $spreadSheet->getSheet(0)->getCell('B143')->SetValue($this->data->instalasi_ipcam_ssl);
                $spreadSheet->getSheet(0)->getCell('B144')->SetValue($this->data->lme_ap_outdoor_hss);
                $spreadSheet->getSheet(0)->getCell('B145')->SetValue($this->data->lme_ap_outdoor_ssl);
                $spreadSheet->getSheet(0)->getCell('B146')->SetValue($this->data->pu_s7_140_hss);
                $spreadSheet->getSheet(0)->getCell('B147')->SetValue($this->data->pu_s7_140_ssl);
                $spreadSheet->getSheet(0)->getCell('B148')->SetValue($this->data->pu_s9_140_hss);
                $spreadSheet->getSheet(0)->getCell('B149')->SetValue($this->data->pu_s9_140_ssl);

                $writer = IOFactory::createWriter($spreadSheet, 'Xlsx');

                switch ($this->output_file)
                {
                    case 'save':

                            $path = public_path() . "/upload2/" . $this->data->id . '/';

                            if (!file_exists($path))
                            {
                                if (!mkdir($path, 0777, true))
                                {
                                    return 'gagal menyiapkan folder rekon';
                                }
                            }

                            $writer->save($path . "DokPelengkap_Skema_OrderBasedxOrder" . str_replace(" ", "_", $this->data->nama_company) . ".xlsx");
                        break;
                    
                    default:
                            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                            header('Content-Disposition: attachment;filename="DokPelengkap_Skema_OrderBasedxOrder'.str_replace(" ", "_", $this->data->nama_company).'.xlsx"');
            
                            $writer->save('php://output');
                            die;
                        break;
                }

            },
            BeforeWriting::class => function (BeforeWriting $event) {
                $event->writer->setActiveSheetIndex(0);
            }
        ];
    }
}