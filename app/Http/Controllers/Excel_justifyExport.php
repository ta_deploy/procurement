<?php

namespace App\Http\Controllers;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Events\BeforeSheet;
use Maatwebsite\Excel\Events\BeforeWriting;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use Maatwebsite\Excel\Excel;
use DB;
use App\DA\ReportModel;
use App\DA\AdminModel;
use App\Http\Controllers\ProgressController;

class Excel_justifyExport implements  WithEvents
{
    use Exportable;

    public function __construct($data){
        $this->data = $data[0];
        $this->opsi = $data[1];
    }

    public function convert_month($date, $cetak_hari = false){
        $bulan = array (1 =>   'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );

        $hari = array ( 1 =>    'Senin',
            'Selasa',
            'Rabu',
            'Kamis',
            'Jumat',
            'Sabtu',
            'Minggu'
        );

        $result = date('d m Y', strtotime($date));
        $split = explode(' ', $result);
        $hasilnya = $split[0] . ' ' . $bulan[(int)$split[1] ] . ' ' . $split[2];

        if ($cetak_hari) {
            $num = date('N', strtotime($date));
            return $hari[$num].' '.$hasilnya;
        }

        return $hasilnya;
    }

    public function registerEvents(): array
    {
        return [
            BeforeExport::class  => function(BeforeExport $event) {
                $border_Style = [
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
                        ],
                    ],
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    ]
                ];

                $bulan = array (1 =>   'Januari',
                    'Februari',
                    'Maret',
                    'April',
                    'Mei',
                    'Juni',
                    'Juli',
                    'Agustus',
                    'September',
                    'Oktober',
                    'November',
                    'Desember'
                );

                $reader = new Xlsx();
                \PhpOffice\PhpSpreadsheet\Shared\File::setUseUploadTempDirectory(true);
                $that = new ProgressController();

                $spreadSheet = $reader->load(public_path().'/template_doc/maintenance_down_2021/justifikasi_plan.xlsx');

                $security = $spreadSheet->getSecurity();

                $spreadSheet->getActiveSheet()->getProtection()->setSheet(true);
                $spreadSheet->getActiveSheet()->getProtection()->setInsertRows(true);

                $security->setLockWindows(true);
                $security->setLockStructure(true);

                $security->setWorkbookPassword($that->key_data() );
                //isi cell
                $get_area = DB::table('procurement_area')->where('witel', $this->data->witel)->first();

                $spreadSheet->getSheet(0)->getCell('F6')->SetValue('PT. TELKOM AKSES WITEL '.strtoupper($get_area->witel).' (' . strtoupper($get_area->area) . ')');
                $spreadSheet->getSheet(0)->getCell('F7')->SetValue($this->data->judul ?? '');
                $spreadSheet->getSheet(0)->getCell('F10')->SetValue($this->data->id_project ?? '');

                $AdminModel = new AdminModel();

                $get_c_prp = DB::table('procurement_req_pid')
                ->where('id_upload', $this->data->id)
                ->OrderBy('id', 'ASC')
                ->limit(1)
                ->first();

        		$pph_text = $AdminModel->find_pph_text(date('Y-m-d', strtotime("first day of this month", strtotime($get_c_prp->created_at) ) ) );

                $spreadSheet->getSheet(0)->getCell('F19')->SetValue('Rp. '.number_format($this->data->gd_sp, '0', ',', '.').' - (BELUM TERMASUK PPN '.$pph_text.')');
                $spreadSheet->getSheet(0)->getCell('F20')->SetValue($bulan[intval(date('m', strtotime($get_c_prp->created_at) ) )] .' '. date('Y', strtotime($get_c_prp->created_at) ) ?? '' );

                if($this->data->pekerjaan == 'Construction')
                {
                    $spreadSheet->getSheet(0)->getCell('C9')->SetValue('Jenis PO');
                    $spreadSheet->getSheet(0)->getCell('F9')->SetValue($this->data->jenis_po);
                    $spreadSheet->getSheet(0)->unmergeCells('I36:K36');
                    $spreadSheet->getSheet(0)->unmergeCells('I37:K41');
                    $spreadSheet->getSheet(0)->getCell('I36')->SetValue('Nilai');
                    $spreadSheet->getSheet(0)->mergeCells('I36:J36');
                    $spreadSheet->getSheet(0)->getCell('K36')->SetValue('Keterangan');
                    $spreadSheet->getSheet(0)->mergeCells('K36:L36');
                    $spreadSheet->getSheet(0)->getCell('I37')->SetValue('Rp. '.number_format($this->data->gd_sp, '0', ',', '.') );
                    $spreadSheet->getSheet(0)->mergeCells('I37:J41');
                    $spreadSheet->getSheet(0)->getCell('K37')->SetValue('BOQ Terlampir');
                    $spreadSheet->getSheet(0)->mergeCells('K37:L41');
                    $spreadSheet->getSheet(0)->getStyle('I36:L41')->applyFromArray($border_Style);

                    $spreadSheet->getSheet(0)->getCell('C48')->SetValue('1)  Pengadaan dimaksud adalah pengadaan untuk kelancaran pekerjaan Konstruksi.');
                    $spreadSheet->getSheet(0)->setTitle("JUSTIFIKASI CONS");
                }

                $spreadSheet->getSheet(0)->getCell('C49')->SetValue('2)  Pengadaan ini dilaksanakan dengan Pekerjaan ' . $this->data->judul . ' Lokasi ' . $get_area->area . ' Witel ' . $this->data->witel);

                $spreadSheet->getSheet(0)->getCell('D52')->SetValue('   : '. $this->data->toc .' Hari' ?? '');
                $spreadSheet->getSheet(0)->getCell('C53')->SetValue("Tempat  : LOKASI " .$get_area->area. " WITEL ". $get_area->witel);

                $spreadSheet->getSheet(0)->getCell('C55')->SetValue("MITRA : ". $this->data->nama_company);

                $data_user = ReportModel::get_dokumen_id('sm_krj', $this->data->pekerjaan, $this->data->witel, $this->data->bulan_pengerjaan);

                $spreadSheet->getSheet(0)->getCell('E64')->SetValue($data_user->user.'/'.$data_user->nik);
                $spreadSheet->getSheet(0)->getCell('G64')->SetValue($data_user->jabatan);

                $data_user = ReportModel::get_dokumen_id('mngr_krj', $this->data->pekerjaan, $this->data->witel, $this->data->bulan_pengerjaan);

                $spreadSheet->getSheet(0)->getCell('E65')->SetValue($data_user->user.'/'.$data_user->nik);
                $spreadSheet->getSheet(0)->getCell('G65')->SetValue($data_user->jabatan);

                $data_user = ReportModel::get_dokumen_id('mngr_ss', $this->data->pekerjaan, $this->data->witel, $this->data->bulan_pengerjaan);

                $spreadSheet->getSheet(0)->getCell('E66')->SetValue($data_user->user.'/'.$data_user->nik);
                $spreadSheet->getSheet(0)->getCell('G66')->SetValue($data_user->jabatan);

                $data_user = ReportModel::get_dokumen_id('m_fin', $this->data->pekerjaan, $this->data->witel, $this->data->bulan_pengerjaan);

                $spreadSheet->getSheet(0)->getCell('E67')->SetValue($data_user->user.'/'.$data_user->nik);
                $spreadSheet->getSheet(0)->getCell('G67')->SetValue($data_user->jabatan);

                if(in_array($this->data->pekerjaan, ['QE']) )
                {
                    if ($this->data->gd_sp >= 0 && $this->data->gd_sp <= 500000000)
                    {
                        $id_ttd = 'apprvJust_krglbh_limaratus_jt';
                    } else
                    {
                        $id_ttd = 'apprvJust_lbh_limaratus_jt';
                    }
                }
                elseif($this->data->pekerjaan == 'Construction')
                {
                    if ($this->data->gd_sp >= 0 && $this->data->gd_sp <= 200000000)
                    {
                        $id_ttd = 'apprvJust_lbh_duaratus_jt';
                    } elseif ($this->data->gd_sp > 200000000 && $this->data->gd_sp <= 1000000000)
                    {
                        $id_ttd = 'apprvJust_ant_duaratus_satuM';
                    } else
                    {
                        $id_ttd = 'apprvJust_lbh_satuM';
                    }
                }

                $data_ttd = ReportModel::get_dokumen_id($id_ttd, $this->data->pekerjaan, $this->data->witel, $this->data->bulan_pengerjaan);

                $spreadSheet->getSheet(0)->getCell('E68')->SetValue($data_ttd->user.'/'.$data_ttd->nik);
                $spreadSheet->getSheet(0)->getCell('G68')->SetValue($data_ttd->jabatan);

                $writer = IOFactory::createWriter($spreadSheet, 'Xlsx');
                if($this->opsi == 'save')
                {
                    $path = public_path(). "/upload2/". $this->data->id .'/Justif/';

                    if (!file_exists($path)) {
                        if (!mkdir($path, 0770, true)) {
                            return 'gagal menyiapkan folder sanggup';
                        }
                    }

                    $writer->save($path."Template Surat Justif ".$this->data->judul. ".xlsx");
                }
                else
                {
                    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                    header('Content-Disposition: attachment;filename="SP '.$this->data->judul.'.xlsx"');
                    $writer->save('php://output');
                    die;
                }
            }
        ];
    }
}