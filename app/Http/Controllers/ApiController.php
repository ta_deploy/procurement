<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use DB;

date_default_timezone_set("Asia/Makassar");

class ApiController extends Controller
{
    public static function mitra_witel_tacticalpro()
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://tacticalpro.co.id/api/get/mitra/witel',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'apisecret: U1R3ci9uTjZEOVRVY2NicTFMUlA4dz09'
            ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        $result = json_decode($response);

        if ($result->code == 200)
        {
            foreach ($result->data as $value)
            {
                $insert[] = [
                    'id_mitra' => $value->id_mitra,
                    'mitra' => $value->mitra,
                    'reg' => $value->reg,
                    'witel_ms2n' => $value->witel_ms2n
                ];
            }

            DB::table('procurement_mitra_nasional')->insert($insert);

            print_r("all done");
        } else {
            print_r("all failed");
        }
    }

    public static function rekon_tacticalpro($witel, $resource, $start_date, $end_date)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://tacticalpro.co.id/api/rekon/promise',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => array('witel' => $witel, 'resource' => $resource, 'start_date' => $start_date, 'end_date' => $end_date),
            CURLOPT_HTTPHEADER => array(
                'apisecret: SWViRmZjZE5qMHlMSkd5ZmZiVW1HRVdIVHZxVU5Bejk='
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        $result = json_decode($response);

        if ($result->code == 200)
        {
            foreach ($result->data->result as $key => $value)
            {
                dd($key, count($value), $value);
                $insert[] = [];
            }
        }
    }
}

?>