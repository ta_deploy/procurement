<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\DA\LoginModel;
use App\DA\OperationModel;
use App\DA\ToolsModel;
use DB;
use Validator;

date_default_timezone_set("Asia/Makassar");
setlocale(LC_ALL, 'IND', 'id_ID');
class LoginController extends Controller
{
	public function login_View()
	{
		return view('login');
	}

	public function reloadCaptcha()
	{
		$result['captcha'] = urldecode(captcha_img('math'));
		return $result;
	}

	public function check_login(Request $req, LoginModel $Loginmodel)
	{
		$rules = [ 'captcha' => 'required|captcha' ];

		$validator = Validator::make($req->all(), $rules);
		if ($validator->fails())
		{
		return redirect('/login')->with('alerts', [
			['type' => 'danger', 'text' => 'Invalid Captcha!']
		]);
		}
		$user       = $req->input('username');
		$pass       = $req->input('password');
		$type_login = $req->input('type_login');

		if (empty($type_login))
		{
			return redirect('/login')->with('alerts', [
				['type' => 'danger', 'text' => 'Maaf, Pilih Tipe Login Terlebih Dahulu!']
			]);
		}

		$result    = $Loginmodel->login($user, $pass);
		$sso_login = $Loginmodel->sso_login($user, $pass);

		switch ($type_login) {
			case 'lokal':
				if (count($result) > 0)
				{
					$session = $req->session();
					$session->put('auth', $result[0]);
					$this->ensureLocalUserHasRememberToken($result[0], $Loginmodel);
					return $this->successResponse($result[0]->psb_remember_token);
				} else {
					return redirect('/login')->with('alerts', [
						['type' => 'danger', 'text' => 'NIK atau Password yang Dimasukan Salah :(']
					]);
				}
			break;

			case 'sso':
				if ($sso_login['auth'] == 'Yes')
				{
					$check = DB::table('1_2_employee as emp')
					->leftJoin('user as u', 'emp.nik', '=', 'u.id_karyawan')
					->where('u.id_karyawan', $user)
					->first();

					if(!$check)
					{
						$all_regional = DB::Table('procurement_mitra_nasional')->get();
						$get_user = DB::Table('procurement_step')
						->Where('active', 1)
						->GroupBy('aktor_nama')
						->get();

						$sso_login['nik'] = $req->username;
						$sso_login['pwd'] = $req->password;

						return view('Tools.register_sso', compact('all_regional', 'get_user'), ['user_detail' => json_encode($sso_login)] );
					}
					else
					{
						$result  = $Loginmodel->login_without_pass($user);
						$session = $req->session();
						$session->put('auth', $result[0]);
						$this->ensureLocalUserHasRememberToken($result[0], $Loginmodel);
						return $this->successResponse($result[0]->psb_remember_token);
					}
				} else {
					dd('NIK atau Password Salah :(');
				}
				break;
		}
	}

	public function logout()
	{
		Session::forget('auth');
		Session::forget('badges');
		return redirect('/login')->withCookie(cookie()->forever('presistent-token', ''));
	}

	private function ensureLocalUserHasRememberToken($localUser, LoginModel $Loginmodel)
	{
		$token = $localUser->psb_remember_token;
		if (!$localUser->psb_remember_token) {
			$token = $this->generateRememberToken($localUser->id_user);
			$Loginmodel->remembertoke($localUser, $token);
			$localUser->psb_remember_token = $token;
		}

		return $token;
	}

	private function generateRememberToken($nik)
	{
		return md5($nik . microtime());
	}

	private function successResponse($rememberToken)
	{
		if (Session::has('auth-originalUrl')) {
			$url = Session::pull('auth-originalUrl');
		} else {
			$url = '/';
		}

		$response = redirect($url);
		if ($rememberToken) {
			$response->withCookie(cookie()->forever('presistent-token', $rememberToken));
		}
		return $response;
	}

	public function setting_pekerjaan()
	{
    $data = ToolsModel::list_pekerjaan();
		$load_data = OperationModel::load_pekerjaan();
		return view('Tools.setting_khs_user', ['data' => $data[0] ], compact('load_data'));
	}

	public function save_pekerjaan(Request $req)
	{
		LoginModel::insert_update_khs($req);
		return redirect('/setting_pekerjaan')->with('alerts', [
			['type' => 'info', 'text' => 'Pekerjaan Yang Sudah Dilihat Berhasi Diubah!']
		]);
	}

	public function sso_saved_updated(Request $req)
	{
		return LoginModel::insert_updated_sso($req);
	}
}
