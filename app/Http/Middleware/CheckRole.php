<?php

namespace App\Http\Middleware;

use Closure;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\DA\ToolsModel;

class CheckRole
{
    public function handle($request, Closure $next, ...$roles)
    {
        //procurement_area
        $valid_user = [
            1 => 'PA',
            'mitra',
            'operation',
            'superadmin',
            'TL',
            'PR',
            'commerce',
            'finance'
        ];

        $valid_user[99] = 'super_superadmin';
        $valid_user[44] = 'regional_superadmin';

        $level = Session('auth')->proc_level;
        // dd($roles, empty($roles), $level);
        if($level == '2')
        {
            $check_mitra = ToolsModel::find_mitra(session('auth')->mitra_amija_pt);

            if(empty($check_mitra->lokasi_npwp) || empty($check_mitra->lokasi_comp) )
            {
              return redirect('/tools/edit/mitra');
            }
        }

        if(empty($roles) || $level == 0){
            return redirect('login');
        }

        if (in_array($valid_user[$level], $roles) || $roles[0] == 'all') {
            return $next($request);
        }

        Session::put('auth-originalUrl', $request->fullUrl());
        if ($request->ajax()) {
            return response('UNAUTHORIZED', 401);
        } else {
            Session::put('auth-originalUrl', '');
            return redirect('login');
        }
    }

}
