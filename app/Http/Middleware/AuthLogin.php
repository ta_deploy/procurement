<?php

namespace App\Http\Middleware;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\ReportController;
use App\DA\LoginModel;
use DB;
use Closure;

class AuthLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      $rememberToken = $request->cookie('presistent-token');

      if ($rememberToken) {
        $user = LoginModel::login_by_token($rememberToken);
        if (count($user)>0 && $user[0]->proc_level != 0) {
          Session::put('auth', $user[0]);
          return $next($request);
        }
      }

      Session::put('auth-originalUrl', $request->fullUrl());
      if ($request->ajax()) {
        return response('UNAUTHORIZED', 401);
      } else {
        return redirect('login');
      }
    }
  }
