@extends('layout')
@section('title', 'Input Material' )
@section('headerS')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="/css/daterangepicker.css">
@section('content')
@if (Session::has('alerts'))
	@foreach(Session::get('alerts') as $alert)
		<div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
	@endforeach
@endif
<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-md-12">
			<h2 class="page-title">Upload KHS</h2>
			<div class="card-deck" style="display: block">
				<form id="submit_lampiran_boq" class="row" enctype="multipart/form-data" method="post">
					{{ csrf_field() }}
					<div class="col-md-12">
						<div class="card shadow mb-4">
							<div class="card-body">
								<div class="form-group row">
									<label class="col-form-label col-md-2 pull-right" for="pekerjaan">Pekerjaan</label>
									<div class="col-md-10">
										<select name="pekerjaan" class="form-control input-transparent" id="pekerjaan">
											@foreach ($kerjaan[2] as $val)
												<option value="{{ $val->id }}">{{ $val->text }}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-md-2 pull-right" for="mitra">Mitra</label>
									<div class="col-md-10">
										<select name="mitra" class="form-control input-transparent" id="mitra">
											<option value="{{ $mitra->id }}">{{ $mitra->nama_company }}</option>
											<option value="0">MITRA</option>
										</select>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-md-2 pull-right">Tanggal Awal</label>
									<div class="col-md-5">
										<select id="tahun_start" class="form-control tahun" name="tahun_start" required>
											@for ($i = date('Y'); $i >= date('Y', strtotime('-1 year') ); $i--)
												<option value="{{ $i }}">{{ $i }}</option>
											@endfor
										</select>
									</div>
									<div class="col-md-5">
										<select id="bulan_start" class="form-control bulan" name="bulan_start" required>
											<option value="01">Januari</option>
											<option value="02">Februari</option>
											<option value="03">Maret</option>
											<option value="04">April</option>
											<option value="05">Mei</option>
											<option value="06">Juni</option>
											<option value="07">Juli</option>
											<option value="08">Agustus</option>
											<option value="09">September</option>
											<option value="10">Oktober</option>
											<option value="11">November</option>
											<option value="12">Desember</option>
										</select>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-md-2 pull-right">Tanggal Akhir</label>
									<div class="col-md-5">
										<select id="tahun_end" class="form-control tahun" name="tahun_end" required>
											@for ($i = date('Y', strtotime('+10 year') ); $i >= date('Y'); $i--)
												<option value="{{ $i }}">{{ $i }}</option>
											@endfor
										</select>
									</div>
									<div class="col-md-5">
										<select id="bulan_end" class="form-control bulan" name="bulan_end" required>
											<option value="01">Januari</option>
											<option value="02">Februari</option>
											<option value="03">Maret</option>
											<option value="04">April</option>
											<option value="05">Mei</option>
											<option value="06">Juni</option>
											<option value="07">Juli</option>
											<option value="08">Agustus</option>
											<option value="09">September</option>
											<option value="10">Oktober</option>
											<option value="11">November</option>
											<option value="12">Desember</option>
										</select>
									</div>
								</div>
								<div class="form-group row">
                  <label class="col-form-label col-md-2 pull-right" for="upload_khs">Lampiran File KHS</label>
                  <div class="col-md-10">
										<?php
											$file = "/template_doc/Contoh_Khs_Format.xlsx";
											if(@file_exists(public_path().$file))
											{
												$path = "$file";
											}
										?>
										<input type="file" id="upload_khs" name="upload_khs" class="form-control-file" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel">
										@if (file_exists(public_path().$file))
											<a href="{{ $path }}">
												<h5><span class="badge badge-pill badge-info"><i class="fe fe-download"></i>&nbsp;Format KHS</span></h5>
											</a>
										@endif
                  </div>
                </div>
								<div class="form-group mb-3">
									<div class="custom-file">
										<button type="submit" class="btn btn-block btn_submit btn-primary"><i class="fe fe-disk fe-16"></i>&nbsp;Submit KHS</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection
@section('footerS')
<script src="/js/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="/js/daterangepicker.js"></script>
<script type="text/javascript">
	$(function(){
		$('#pekerjaan').select2({
			width: '100%',
			placeholder: 'Pilih jenis pekerjaan',
		});

		$('#mitra').select2({
			width: '100%',
			placeholder: 'Pilih Nama Perusahaan',
		});

		// var start = moment().startOf('month'),
		// end = moment().endOf('month');

		// $('.tgl').daterangepicker({
		// 	locale: {
		// 		format: 'YYYY-MM-DD'
		// 	},
		// 	showDropdowns: true,
		// 	startDate: start,
		// 	endDate: end
		// })
	});
</script>
@endsection