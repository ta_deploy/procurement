@extends('layout')
@section('title', 'Input Amandemen')
@section('style')
@endsection
@section('content')
<div class="body">
	<div class="row">
		<form id="validation-form" class="form-horizontal form-label-left" method="post">
		<div class="col-md-6">
			<section class="widget">
				<header>
					<h4>
						<i class="fa fa-check-square-o"></i>
						Pengisian Kontrak dan Pra Kontrak
						<small>Untuk Menghasilkan Dokumen Word</small>
					</h4>
				</header>
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				{{-- pra kontrak --}}
				<fieldset>
					<input type="hidden" value="" name="id_input_kontrak">
					<legend class="section">Pra Kontrak</legend>
					<div class="form-group">
						<label class="control-label col-md-5" for="hk_aman">Nomor HK Amandemen</label>
						<div class="col-md-7">
							<input type="text" id="hk_aman" name="hk_aman" class="form-control input-transparent" required="required" value="{{ $data->hk_aman }}">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-5" for="harga_amand">Harga Terbaru</label>
						<div class="col-md-7">
							<input type="text" id="harga_amand" name="harga_amand" class="form-control input-transparent" required="required" value="{{ $data->harga_amand }}">
						</div>
					</div>
				</fieldset>
				{{-- kontrak --}}
				<div class="form-actions">
					<div class="row">
						<div class="col-md-8 col-md-offset-4">
							<button type="submit" class="btn btn-danger">Validate &amp; Submit</button>
							<button type="button" class="btn btn-default">Cancel</button>
						</div>
					</div>
				</div>
			</section>
		</div>
		{{-- isi Data  Perusahaan --}}
		<div class="col-md-6 ">
			<section class="widget">
				<header>
					<h4>
						<i class="fa fa-black-tie"></i>
						Pengisian Data Perusahaan Mitra
					</h4>
				</header>
				<div class="body">
					<fieldset>
						<legend class="section">Data Perusahaan Mitra</legend>
						<div class="form-group">
							<label class="control-label col-md-5" for="nama_company">Nama Perusahaan</label>
							<div class="col-md-7">
								<input type="text" id="nama_company" name="nama_company" class="form-control input-transparent"
								readonly value="{{ $data->nama_company }}">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-5" for="alamat_company">Alamat Perusahaan</label>
							<div class="col-md-7">
								<input type="text" id="alamat_company" name="alamat_company" class="form-control input-transparent"
								readonly value="{{ $data->alamat_company }}">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-5" for="bank">Nama Bank</label>
							<div class="col-md-7">
								<input type="text" id="bank" name="bank" class="form-control input-transparent"
								readonly value="{{ $data->bank }}">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-5" for="rek">Nomor Rekening</label>
							<div class="col-md-7">
								<input type="text" id="rek" name="rek" class="form-control input-transparent"
								readonly value="{{ $data->rek }}">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-5" for="wakil_mitra">Nama PIC</label>
							<div class="col-md-7">
								<input type="text" id="wakil_mitra" name="wakil_mitra" class="form-control input-transparent"
								readonly value="{{ $data->wakil_mitra }}">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-5" for="jabatan_mitra">Jabatan PIC</label>
							<div class="col-md-7">
								<input type="text" id="jabatan_mitra" name="jabatan_mitra" class="form-control input-transparent"
								readonly value="{{ $data->jabatan_mitra }}">
							</div>
						</div>
					</fieldset>
				</div>
			</section>
		</div>
	</form>
</div>
@endsection
@section('footerS')
<script src="/js/forms-validation.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/jquery.inputmask.bundle.js"></script>
<script type="text/javascript">
	$('#rek').val(function(index, value) {
		return value
		.replace(/\D/g, "")
		.replace(/\B(?=(\d{3})+(?!\d))/g, "-")
		;
	});

	$('#harga_amand').keyup(function(event) {

  // skip for arrow keys
		if(event.which >= 37 && event.which <= 40) return;

  // format number
		$(this).val(function(index, value) {
			return value
			.replace(/\D/g, "")
			.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
			;
		});
	});
</script>
@endsection