@extends('layout')
@section('title', $id == 'input' ? 'Input Data' : 'Edit Data Mitra')
@section('style')
@endsection
@section('headerS')
<link rel="stylesheet" href="/css/daterangepicker.css" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
@endsection
@section('content')
@if (Session::has('alerts'))
@foreach(Session::get('alerts') as $alert)
<div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
@endforeach
@endif
<div class="container-fluid">
   <div class="row justify-content-center">
      <div class="col-md-12">
         <h2 class="page-title">{{ session('auth')->mitra_amija_pt ? 'Profile Mitra' : 'Pilih Mitra' }}</h2>
         <p style="color: red;">{{ session('auth')->mitra_amija_pt ? 'Data Harus Diisi Sesuai NPWP dan Kontrak!!' : 'Harap Pilih Mitra!!' }}</p>
         <div class="card-deck" style="display: block;">
            <form id="input_pra" class="row" method="post" enctype="multipart/form-data">
               {{ csrf_field() }}
			   @if (session('auth')->mitra_amija_pt)
               <div class="col-md-6">
                  <div class="card shadow mb-4">
                     <div class="card-header">
                        <strong class="card-title">Data Perusahaan Mitra</strong>
                     </div>
                     <div class="card-body">
                        <div class="form-group row">
                           <label class="col-form-label pull-right col-md-5" for="nama_company">Nama Perusahaan</label>
                           <div class="col-md-7">
                              <input type="text" id="nama_company" name="nama_company" class="form-control input-transparent" value="{{ Request::old('nama_company') ?? @$mitra_me->nama_company }}" />
                           </div>
                        </div>
                        <div class="form-group row">
                           <label class="col-form-label pull-right col-md-5" for="alias_mitra">Alias</label>
                           <div class="col-md-7">
                              <input type="text" id="alias_mitra" name="alias_mitra" class="form-control input-transparent" required="required" value="{{ Request::old('alias_mitra') ?? @$mitra_me->mitra_amija }}" />
                           </div>
                        </div>
                        <div class="form-group row">
                           <label class="col-form-label pull-right col-md-5" for="alamat_company">Alamat Perusahaan</label>
                           <div class="col-md-7">
                              <textarea rows="2" style="resize: none;" cols="50" id="alamat_company" name="alamat_company" class="form-control input-transparent" required="required">{{ Request::old('alamat_company') ?? @$mitra_me->alamat_company }}</textarea>
                           </div>
                        </div>
                        <div class="form-group row">
                           <label class="col-form-label pull-right col-md-5" for="lokasi_comp">Lokasi TTD</label>
                           <div class="col-md-7">
                              <input type="text" id="lokasi_comp" name="lokasi_comp" class="form-control input-transparent" required="required" value="{{ Request::old('lokasi_comp') ?? @$mitra_me->lokasi_comp }}" />
                              <code>Untuk Lokasi TTD kwitansi, Invoice, SPB. Contoh (Banjarmasin, 08 Oktober 2021)</code>
                           </div>
                        </div>
                        <div class="form-group row">
                           <label class="col-form-label pull-right col-md-5" for="wakil_mitra">Nama PIC</label>
                           <div class="col-md-7">
                              <input type="text" id="wakil_mitra" name="wakil_mitra" class="form-control input-transparent" required="required" value="{{ Request::old('wakil_mitra') ?? @$mitra_me->wakil_mitra }}" />
                           </div>
                        </div>
                        <div class="form-group row">
                           <label class="col-form-label pull-right col-md-5" for="jabatan_mitra">Jabatan PIC</label>
                           <div class="col-md-7">
                              <input type="text" id="jabatan_mitra" name="jabatan_mitra" class="form-control input-transparent" required="required" value="{{ Request::old('jabatan_mitra') ?? @$mitra_me->jabatan_mitra }}" />
                           </div>
                        </div>
                        <div class="form-group row">
                           <label class="col-form-label pull-right col-md-5" for="telp">No Telpon</label>
                           <div class="col-md-7">
                              <input type="text" id="telp" name="telp" class="form-control input-transparent" required="required" value="{{ Request::old('telp') ?? @$mitra_me->telp }}" />
                           </div>
                        </div>
                        <div class="form-group row">
                           <label class="col-form-label pull-right col-md-5" for="witel">Witel</label>
                           <div class="col-md-7">
                              <select name="witel" id="witel" class="form-control input-transparent witel" required>
                                 @foreach ($get_witel as $witel)
                                 <option value="{{ $witel->id }}">{{ $witel->text }}</option>
                                 @endforeach
                              </select>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="card shadow mb-4">
                     <div class="card-header">
                        <strong class="card-title">Data Rekening Mitra</strong>
                     </div>
                     <div class="card-body">
                        <div class="form-group row">
                           <label class="col-form-label pull-right col-md-5" for="rek">Nomor Rekening</label>
                           <div class="col-md-7">
                              <input type="text" id="rek" name="rek" class="form-control input-transparent" required="required" value="{{ Request::old('rek') ?? @$mitra_me->rek }}" />
                           </div>
                        </div>
                        <div class="form-group row">
                           <label class="col-form-label pull-right col-md-5" for="pph">Nilai Pph</label>
                           <div class="col-md-7">
                              <select name="pph" id="pph" class="form-control input-transparent pph" required>
                                 @foreach ($get_pph as $pph)
                                 <option value="{{ $pph->id }}">{{ $pph->text }}</option>
                                 @endforeach
                              </select>
                           </div>
                        </div>
                        <div class="form-group row">
                           <label class="col-form-label pull-right col-md-5" for="bank">Nama Bank</label>
                           <div class="col-md-7">
                              <input type="text" id="bank" name="bank" class="form-control input-transparent" required="required" value="{{ Request::old('bank') ?? @$mitra_me->bank }}" />
                              <code>MANDIRI, BCA, etc</code>
                           </div>
                        </div>
                        <div class="form-group row">
                           <label class="col-form-label pull-right col-md-5" for="cabang_bank">Cabang Bank</label>
                           <div class="col-md-7">
                              <input type="text" id="cabang_bank" name="cabang_bank" class="form-control input-transparent" required="required" value="{{ Request::old('cabang_bank') ?? @$mitra_me->cabang_bank }}" />
                              <code>Banjarmasin</code>
                           </div>
                        </div>
                        <div class="form-group row">
                           <label class="col-form-label pull-right col-md-5" for="atas_nama">Atas Nama</label>
                           <div class="col-md-7">
                              <input type="text" id="atas_nama" name="atas_nama" class="form-control input-transparent" required="required" value="{{ Request::old('atas_nama') ?? @$mitra_me->atas_nama }}" />
                           </div>
                        </div>
                        <div class="form-group row">
                           <label class="col-form-label pull-right col-md-5" for="npwp">NPWP</label>
                           <div class="col-md-7">
                              <input
                                 type="text"
                                 id="npwp"
                                 name="npwp"
                                 data-inputmask="'mask': '9{2}.9{3}.9{3}.9{1}-9{3}.9{3}'"
                                 class="form-control input-transparent"
                                 required="required"
                                 value="{{ Request::old('npwp') ?? @$mitra_me->NPWP }}"
                              />
                           </div>
                        </div>
                        <div class="form-group row">
                           <label class="col-form-label pull-right col-md-5" for="lokasi_npwp">Lokasi NPWP</label>
                           <div class="col-md-7">
                              <input type="text" id="lokasi_npwp" name="lokasi_npwp" class="form-control input-transparent" required="required" value="{{ Request::old('lokasi_npwp') ?? @$mitra_me->lokasi_npwp }}" />
                              <code>Untuk Lokasi TTD Faktur Pajak. Contoh (Banjarmasin, 08 Oktober 2021)</code>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="card shadow mb-4">
                     <div class="card-header">
                        <strong class="card-title">Data Nomor Dokumen ( QE, Construction, PSB )</strong>
                     </div>
                     <div class="card-body">
                        <div class="form-group row">
                           <label class="col-form-label pull-right col-md-5" for="no_kontrak_ta">Nomor Kontrak TA</label>
                           <div class="col-md-7">
                              <input type="text" id="no_kontrak_ta" name="no_kontrak_ta" class="form-control input-transparent" required="required" value="{{ !empty($mitra_me) ? $mitra_me->no_kontrak_ta : '' }}" />
                           </div>
                        </div>
                        <div class="form-group row">
                           <label class="col-form-label pull-right col-md-5" for="date_kontrak_ta">Tanggal Kontrak TA</label>
                           <div class="col-md-7">
                              <input type="text" id="date_kontrak_ta" name="date_kontrak_ta" class="form-control date-picker" required="required" value="{{ @$mitra_me->date_kontrak_ta ?? date('Y-m-d') }}" />
                           </div>
                        </div>
                        <div class="form-group row">
                           <label class="col-form-label pull-right col-md-5" for="no_penetapan_khs">Nomor Penetapan KHS</label>
                           <div class="col-md-7">
                              <input type="text" id="no_penetapan_khs" name="no_penetapan_khs" class="form-control input-transparent" required="required" value="{{ !empty($mitra_me) ? $mitra_me->no_penetapan_khs : '' }}" />
                           </div>
                        </div>
                        <div class="form-group row">
                           <label class="col-form-label pull-right col-md-5" for="date_khs_penetapan">Tanggal Penetapan KHS</label>
                           <div class="col-md-7">
                              <input type="text" id="date_khs_penetapan" name="date_khs_penetapan" class="form-control date-picker" required="required" value="{{ @$mitra_me->date_khs_penetapan ?? date('Y-m-d') }}" />
                           </div>
                        </div>
                        <div class="form-group row">
                           <label class="col-form-label pull-right col-md-5" for="no_kesanggupan_khs">Nomor Kesanggupan KHS</label>
                           <div class="col-md-7">
                              <input type="text" id="no_kesanggupan_khs" name="no_kesanggupan_khs" class="form-control input-transparent" required="required" value="{{ !empty($mitra_me) ? $mitra_me->no_kesanggupan_khs : '' }}" />
                           </div>
                        </div>
                        <div class="form-group row">
                           <label class="col-form-label pull-right col-md-5" for="date_khs_kesanggupan">Tanggal Kesanggupan KHS</label>
                           <div class="col-md-7">
                              <input type="text" id="date_khs_kesanggupan" name="date_khs_kesanggupan" class="form-control date-picker" required="required" value="{{ @$mitra_me->date_khs_kesanggupan ?? date('Y-m-d') }}" />
                           </div>
                        </div>
                        <hr>
                        <div class="form-group row">
                           <label class="col-form-label pull-right col-md-5" for="no_khs_maintenance">Nomor KHS Maintenance</label>
                           <div class="col-md-7">
                              <input type="text" id="no_khs_maintenance" name="no_khs_maintenance" class="form-control input-transparent" required="required" value="{{ !empty($mitra_me) ? $mitra_me->no_khs_maintenance : '' }}" />
                              <code>*Contoh Nomor KHS: 12345/HK.800/TA-123456/08(/ atau . atau -)2021</code>
                           </div>
                        </div>
                        <div class="form-group row">
                           <label class="col-form-label pull-right col-md-5" for="tgl_khs_maintenance">Tanggal KHS Maintenance</label>
                           <div class="col-md-7">
                              @php
                                 $bulan = array ( 'Januari' => '01', 'Februari' => '02', 'Maret' => '03', 'April' => '04', 'Mei' => '05', 'Juni' => '06', 'Juli' => '07', 'Agustus' => '08', 'September' => '09', 'Oktober' => '10','November' => '11', 'Desember' => '12' );
                                 
                                 $tgl_conv = date('Y-m-d');
                                 if (!empty($mitra_me) && $mitra_me->tgl_khs_maintenance)
                                 {
                                    //21 desember 2021
                                    $tgl = explode(' ', $mitra_me->tgl_khs_maintenance);
                                    $tgl_conv = $tgl[2] .'-'. $bulan[$tgl[1] ] .'-'. $tgl[0];
                                 }
                              @endphp
                              <input type="text" id="tgl_khs_maintenance" name="tgl_khs_maintenance" class="form-control input-transparent date-picker" required="required" value="{{ !empty($mitra_me) ? $tgl_conv : '' }}" />
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-6 row">
                  <div class="card shadow mb-4">
                     <div class="card-header">
                        <strong class="card-title">Data Nomor Amandemen ( PSB )</strong>
                     </div>
                     <div class="card-body">
                        <div class="form-group row">
                           <label class="col-form-label pull-right col-md-5" for="no_amandemen1">Nomor Amandemen 1</label>
                           <div class="col-md-7">
                              <input type="text" id="no_amandemen1" name="no_amandemen1" class="form-control input-transparent" value="{{ !empty($mitra_me) ? $mitra_me->no_amandemen1 : '' }}" />
                           </div>
                        </div>
                        <div class="form-group row">
                           <label class="col-form-label pull-right col-md-3" for="date_amandemen1">Tgl Amd 1</label>
                           <div class="col-md-3">
                              <input type="text" id="date_amandemen1" name="date_amandemen1" class="form-control date-picker" value="{{ @$mitra_me->date_amandemen1 }}" />
                           </div>

                           <label class="col-form-label pull-right col-md-3" for="date_expired_amandemen1">Tgl Masa Laku Amd 1</label>
                           <div class="col-md-3">
                              <input type="text" id="date_expired_amandemen1" name="date_expired_amandemen1" class="form-control date-picker" value="{{ @$mitra_me->date_expired_amandemen1 }}" />
                           </div>
                        </div>
                        <hr>

                        <div class="form-group row">
                           <label class="col-form-label pull-right col-md-5" for="no_amandemen2">Nomor Amandemen 2</label>
                           <div class="col-md-7">
                              <input type="text" id="no_amandemen2" name="no_amandemen2" class="form-control input-transparent" value="{{ !empty($mitra_me) ? $mitra_me->no_amandemen2 : '' }}" />
                           </div>
                        </div>
                        <div class="form-group row">
                           <label class="col-form-label pull-right col-md-3" for="date_amandemen2">Tgl Amd 2</label>
                           <div class="col-md-3">
                              <input type="text" id="date_amandemen2" name="date_amandemen2" class="form-control date-picker" value="{{ @$mitra_me->date_amandemen2 }}" />
                           </div>

                           <label class="col-form-label pull-right col-md-3" for="date_expired_amandemen2">Tgl Masa Laku Amd 2</label>
                           <div class="col-md-3">
                              <input type="text" id="date_expired_amandemen2" name="date_expired_amandemen2" class="form-control date-picker" value="{{ @$mitra_me->date_expired_amandemen2 }}" />
                           </div>
                        </div>
                        <hr>

                        <div class="form-group row">
                           <label class="col-form-label pull-right col-md-5" for="no_amandemen3">Nomor Amandemen 3</label>
                           <div class="col-md-7">
                              <input type="text" id="no_amandemen3" name="no_amandemen3" class="form-control input-transparent" value="{{ !empty($mitra_me) ? $mitra_me->no_amandemen3 : '' }}" />
                           </div>
                        </div>
                        <div class="form-group row">
                           <label class="col-form-label pull-right col-md-3" for="date_amandemen3">Tgl Amd 3</label>
                           <div class="col-md-3">
                              <input type="text" id="date_amandemen3" name="date_amandemen3" class="form-control date-picker" value="{{ @$mitra_me->date_amandemen3 }}" />
                           </div>

                           <label class="col-form-label pull-right col-md-3" for="date_expired_amandemen3">Tgl Masa Laku Amd 3</label>
                           <div class="col-md-3">
                              <input type="text" id="date_expired_amandemen3" name="date_expired_amandemen3" class="form-control date-picker" value="{{ @$mitra_me->date_expired_amandemen3 }}" />
                           </div>
                        </div>
                        <hr>

                        <div class="form-group row">
                           <label class="col-form-label pull-right col-md-5" for="no_amandemen4">Nomor Amandemen 4</label>
                           <div class="col-md-7">
                              <input type="text" id="no_amandemen4" name="no_amandemen4" class="form-control input-transparent" value="{{ !empty($mitra_me) ? $mitra_me->no_amandemen4 : '' }}" />
                           </div>
                        </div>
                        <div class="form-group row">
                           <label class="col-form-label pull-right col-md-3" for="date_amandemen4">Tgl Amd 4</label>
                           <div class="col-md-3">
                              <input type="text" id="date_amandemen4" name="date_amandemen4" class="form-control date-picker" value="{{ @$mitra_me->date_amandemen4 }}" />
                           </div>

                           <label class="col-form-label pull-right col-md-3" for="date_expired_amandemen4">Tgl Masa Laku Amd 4</label>
                           <div class="col-md-3">
                              <input type="text" id="date_expired_amandemen4" name="date_expired_amandemen4" class="form-control date-picker" value="{{ @$mitra_me->date_expired_amandemen4 }}" />
                           </div>
                        </div>
                        <hr>

                        <div class="form-group row">
                           <label class="col-form-label pull-right col-md-5" for="no_amandemen5">Nomor Amandemen 5</label>
                           <div class="col-md-7">
                              <input type="text" id="no_amandemen5" name="no_amandemen5" class="form-control input-transparent" value="{{ !empty($mitra_me) ? $mitra_me->no_amandemen5 : '' }}" />
                           </div>
                        </div>
                        <div class="form-group row">
                           <label class="col-form-label pull-right col-md-3" for="date_amandemen5">Tgl Amd 5</label>
                           <div class="col-md-3">
                              <input type="text" id="date_amandemen5" name="date_amandemen5" class="form-control date-picker" value="{{ @$mitra_me->date_amandemen5 }}" />
                           </div>

                           <label class="col-form-label pull-right col-md-3" for="date_expired_amandemen5">Tgl Masa Laku Amd 5</label>
                           <div class="col-md-3">
                              <input type="text" id="date_expired_amandemen5" name="date_expired_amandemen5" class="form-control date-picker" value="{{ @$mitra_me->date_expired_amandemen5 }}" />
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               {{-- <div class="col-md-6">
                  <div class="card shadow mb-4">
                     <div class="card-header">
                        <strong class="card-title">Dokumen Pelengkap</strong>
                     </div>
                     <div class="card-body">
                        @foreach ($file_mitra as $key => $input)
						@php
							$paths = public_path()."/upload_mitra2/{$id}/";
							$file = @preg_grep('~^'.$key.'.*$~', scandir($paths));
							$path = null;
						@endphp
                        <div class="form-group row">
                           <label class="col-form-label pull-right col-md-5" for="{{ $key }}">{{ $input }}</label>
                           <div class="col-md-7">
                              @if (count($file) != 0 && $id != 'input')
							  @php
								$files = array_values($file);
								$path = "/upload_mitra2/" . $id . "/" . $files[0];
							  @endphp
                              <a href="{{ $path }}">
                                 <h5>
                                    <span class="badge badge-pill badge-info"><i class="fe fe-download"></i>&nbsp; {{ $input }}</span>
                                 </h5>
                              </a>
                              @endif
                              <input type="file" id="{{ $key }}" name="{{ $key }}" title="{{ $input }}" class="form-control-file" />
                           </div>
                        </div>
                        @endforeach
                     </div>
                  </div>
               </div> --}}
               @else
               <div class="col-md-12">
                  <div class="card shadow mb-4">
                     <div class="card-header">
                        <strong class="card-title">Pilih Perusahaan Mitra</strong>
                     </div>
                     <div class="card-body">
                        <div class="form-group row">
                           <label class="col-form-label pull-right col-md-2" for="nama_company">Mitra</label>
                           <div class="col-md-10">
                              <select type="text" id="nama_company" name="nama_company" class="form-control nam_comp_sel input-transparent">
                                 @foreach ($all_mitra as $v)
                                 <option value="{{ $v->nama_company }}">{{ $v->nama_company }}</option>
                                 @endforeach
                              </select>
                           </div>
                        </div>
                        <div class="form-group row">
                           <label class="col-form-label pull-right col-md-2" for="alias_mitra">Alias</label>
                           <div class="col-md-10">
                              <input type="text" id="alias_mitra" name="alias_mitra" class="form-control input-transparent" required="required" value="{{ Request::old('alias_mitra') ?? @$mitra_me->mitra_amija }}" />
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               @endif
               <div class="col-md-12">
                  <div class="card shadow mb-4">
                     <button type="submit" class="btn btn-primary" type="submit">Submit</button>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
@endsection @section('footerS')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/jquery.inputmask.bundle.js"></script>
<script src="/js/moment.min.js"></script>
<script src="/js/jquery.timepicker.js"></script>
<script src="/js/daterangepicker.js"></script>
<script type="text/javascript">
   $(function(){
   	$(":input").inputmask();

   	$('.witel').select2({
   		width: '100%',
   		placeholder: 'Silahkan Pilih Witel'
   	});
   	$('.witel').val(null).change();

      $('#pph').select2({
   		width: '100%',
   		placeholder: 'Silahkan Pilih Nilai Pph'
   	});
   	$('#pph').val(null).change();

   	$('#rek').val(function(index, value) {
   		return value
   		.replace(/\D/g, "")
   		.replace(/\B(?=(\d{3})+(?!\d))/g, "-")
   		;
   	});

   	var data = {!! json_encode($mitra_me) !!};

   	if(!$.isEmptyObject(data)){
   		$('#nama_company').attr("readonly", "readonly")
   		$('#pph').val(data.pph).change()
   	}

   	$('#nama_company').on("keypress keyup",function (e){
   		return $(this).val($(this).val().toUpperCase());
   	});

   	$('#nama_company').on("keypress keyup",function (e){
   		$(this).val(function(index, value) {
   			return value
   			.replace(/PT+(\.| \.)/g, "PT ");
   		});
   	});

   	$('.date-picker').daterangepicker(
   		{
   			singleDatePicker: true,
   			timePicker: false,
   			showDropdowns: true,
   			locale:
   			{
   				format: 'YYYY-MM-DD',
   			}
   		}
   	);
   });
</script>
@endsection