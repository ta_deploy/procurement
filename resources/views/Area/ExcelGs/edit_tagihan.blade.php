@extends('layout')
@section('title', 'Input Tagihan 2020')
@section('style')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<style type="text/css">
	.select2-results__option{
		color: black;
	}

</style>
@endsection
@section('content')
<div class="body">
	<div class="row">
		<form id="validation-form" class="form-horizontal form-label-left" method="post">
		<div class="col-md-12">
			<section class="widget">
				<header>
					<h4>
						<i class="fa fa-check-square-o"></i>
						Pengisian Tagihan Tahun 2020
					</h4>
				</header>
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<fieldset>
					<legend class="section">Tagihan 2020</legend>
					<div class="form-group">
						<label class="control-label col-md-3" for="p_kerja">Portofolio Pekerjaan</label>
						<div class="col-md-9">
							<select id="p_kerja" name="p_kerja">
								<option {{ $data[2] == 'ALKER & SAKER' ? 'selected' : '' }} value="ALKER & SAKER">ALKER & SAKER</option>
								<option {{ $data[2] == 'AMS' ? 'selected' : '' }} value="AMS">AMS</option>
								<option {{ $data[2] == 'GAMAS' ? 'selected' : '' }} value="GAMAS">GAMAS</option>
								<option {{ $data[2] == 'MAINTENANCE' ? 'selected' : '' }} value="MAINTENANCE">MAINTENANCE</option>
								<option {{ $data[2] == 'OPTIMA' ? 'selected' : '' }} value="OPTIMA">OPTIMA</option>
								<option {{ $data[2] == 'PSB' ? 'selected' : '' }} value="PSB">PSB</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3" for="jus_keb">Justifikasi Kebutuhan</label>
						<div class="col-md-9">
							<select id="jus_keb" name="jus_keb">
								<option value="OK" {{ $data[3] == 'OK' ? 'selected' : '' }}>OK</option>
								<option value="NOK" {{ $data[3] == 'NOK' ? 'selected' : '' }}>NOK</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3" for="mitra_select">Mitra</label>
						<div class="col-md-9">
							<select id="mitra_select" name="mitra_select"></select>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3" for="no_surat_p">Nomor Surat Pesanan</label>
						<div class="col-md-9">
							<input type="text" id="no_surat_p" name="no_surat_p" class="form-control input-transparent" value="{{ $data[5] }}">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3" for="date_sp">Tanggal SP</label>
						<div class="col-md-9">
							<input rows="2" style="resize:none;" cols="50" id="date_sp" name="date_sp" class="form-control input-transparent date-picker" value="{{ gmdate("d/m/Y ", (25569 + (($data_raw[6] - 25569) * 86400 / 86400) - 25569) * 86400) }}">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3" for="toc">TOC</label>
						<div class="col-md-9">
							<input type="text" id="toc" name="toc" class="form-control input-transparent" value="{{ $data[7] }}">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3" for="uraian_p">Uraian Pekerjaan</label>
						<div class="col-md-9">
							<input type="text" id="uraian_p" name="uraian_p" class="form-control input-transparent" value="{{ $data[8] }}">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3" for="nilai_sp">Nilai SP</label>
						<div class="col-md-9">
							<input type="text" id="nilai_sp" name="nilai_sp" class="form-control input-transparent price" value="{{ $data_raw[9] }}">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3" for="nilai_rek">Nilai Rekon</label>
						<div class="col-md-9">
							<input type="text" id="nilai_rek" name="nilai_rek" class="form-control input-transparent price" value="{{ $data_raw[10] }}">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3" for="pic_pr">PIC PR</label>
						<div class="col-md-9">
							<input type="text" id="pic_pr" name="pic_pr" class="form-control input-transparent" value="{{ $data[11] }}">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3" for="no_pic_pr">Nomor PR SAP</label>
						<div class="col-md-9">
							<input type="text" id="no_pic_pr" name="no_pic_pr" class="form-control input-transparent" value="{{ $data[12] }}">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3" for="date_input_pr">Tanggal Input PR</label>
						<div class="col-md-9">
							<input rows="2" style="resize:none;" cols="50" id="date_input_pr" name="date_input_pr" class="form-control input-transparent date-picker" value="{{ gmdate("d/m/Y ", (25569 + (($data_raw[13] - 25569) * 86400 / 86400) - 25569) * 86400) }}">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3" for="pic_po">PIC PO</label>
						<div class="col-md-9">
							<input type="text" id="pic_po" name="pic_po" class="form-control input-transparent" value="{{ $data[14] }}">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3" for="no_pic_po">Nomor PO SAP</label>
						<div class="col-md-9">
							<input type="text" id="no_pic_po" name="no_pic_po" class="form-control input-transparent" value="{{ $data[15] }}">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3" for="date_input_po">Tanggal Input PO</label>
						<div class="col-md-9">
							<input rows="2" style="resize:none;" cols="50" id="date_input_po" name="date_input_po" class="form-control input-transparent date-picker" value="{{ gmdate("d/m/Y ", (25569 + (($data_raw[16] - 25569) * 86400 / 86400) - 25569) * 86400) }}">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3" for="no_gr_sap">Nomor GR SAP</label>
						<div class="col-md-9">
							<input type="text" id="no_gr_sap" name="no_gr_sap" class="form-control input-transparent" value="{{ $data[17] }}">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3" for="date_input_gr">Tanggal Input GR</label>
						<div class="col-md-9">
							<input rows="2" style="resize:none;" cols="50" id="date_input_gr" name="date_input_gr" class="form-control input-transparent date-picker" value="{{ gmdate("d/m/Y ", (25569 + (($data_raw[18] - 25569) * 86400 / 86400) - 25569) * 86400) }}">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3" for="date_gr">Tanggal GR</label>
						<div class="col-md-9">
							<input rows="2" style="resize:none;" cols="50" id="date_gr" name="date_gr"
								class="form-control input-transparent date-picker" value="{{ gmdate("d/m/Y ", (25569 + (($data_raw[19] - 25569) * 86400 / 86400) - 25569) * 86400) }}">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3" for="job_stts">Status Pekerjaan</label>
						<div class="col-md-9">
							<select id="job_stts" name="job_stts">
								<option {{ $data[20] == 'CANCEL' ? 'selected' : '' }} value="CANCEL">CANCEL</option>
								<option {{ $data[20] == 'OGP' ? 'selected' : '' }} value="OGP">OGP</option>
								<option {{ $data[20] == 'SELESAI' ? 'selected' : '' }} value="SELESAI">SELESAI</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3" for="posisi_tgh">Posisi Tagihan</label>
						<div class="col-md-9">
							<select id="posisi_tgh" name="posisi_tgh">
								<option {{ $data[21] == 'CANCEL' ? 'selected' : '' }} value="CANCEL">CANCEL</option>
								<option {{ $data[21] == 'FINANCE' ? 'selected' : '' }} value="FINANCE">FINANCE</option>
								<option {{ $data[21] == 'MITRA' ? 'selected' : '' }} value="MITRA">MITRA</option>
								<option {{ $data[21] == 'OTW AREA' ? 'selected' : '' }} value="OTW AREA">OTW AREA</option>
								<option {{ $data[21] == 'PROC. AREA' ? 'selected' : '' }} value="PROC. AREA">PROC. AREA</option>
								<option {{ $data[21] == 'PROC. REG' ? 'selected' : '' }} value="PROC. REG">PROC. REG</option>
								<option {{ $data[21] == 'USER' ? 'selected' : '' }} value="USER">USER</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3" for="date_terima_tag_mit">Tanggal Terima Tagihan Mitra</label>
						<div class="col-md-9">
							<input rows="2" style="resize:none;" cols="50" id="date_terima_tag_mit" name="date_terima_tag_mit" class="form-control input-transparent date-picker" value="{{  gmdate("d/m/Y ", (25569 + (($data_raw[22] - 25569) * 86400 / 86400) - 25569) * 86400) }}">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3" for="no_berkas">Nomor Berkas</label>
						<div class="col-md-9">
							<input type="text" id="no_berkas" name="no_berkas" class="form-control input-transparent" value="{{ $data[23] }}">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3" for="proc_area">Procurement Area</label>
						<div class="col-md-9">
							<input type="text" id="proc_area" name="proc_area" class="form-control input-transparent" value="{{ $data[24] }}">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3" for="area_ket">Keterangan Area</label>
						<div class="col-md-9">
							<input type="text" id="area_ket" name="area_ket" class="form-control input-transparent" value="{{ $data[25] }}">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3" for="area_posisi">Posisi Area</label>
						<div class="col-md-9">
							<input type="text" id="area_posisi" name="area_posisi" class="form-control input-transparent" value="{{ $data[26] }}">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3" for="pembayaran">Status Pembayaran</label>
						<div class="col-md-9">
							<select id="pembayaran" name="pembayaran">
								<option {{ $data[35] == 'BELUM APM' ? 'selected' : '' }} value="BELUM APM">BELUM APM</option>
								<option {{ $data[35] == 'CASH & BANK' ? 'selected' : '' }} value="CASH & BANK">CASH & BANK</option>
								<option {{ $data[35] == 'SUDAH APM' ? 'selected' : '' }} value="SUDAH APM">SUDAH APM</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3" for="no_apm">Nomor APM</label>
						<div class="col-md-9">
							<input type="text" id="no_apm" name="no_apm" class="form-control input-transparent" value="{{ $data[36] }}">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3" for="date_account">Tanggal Accounting</label>
						<div class="col-md-9">
							<input rows="2" style="resize:none;" cols="50" id="date_account" name="date_account" class="form-control input-transparent date-picker" value="{{ gmdate("d/m/Y ", (25569 + (($data_raw[37] - 25569) * 86400 / 86400) - 25569) * 86400) }}">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3" for="date_byr">Tanggal Terbayar</label>
						<div class="col-md-9">
							<input rows="2" style="resize:none;" cols="50" id="date_byr" name="date_byr" class="form-control input-transparent date-picker" value="{{ gmdate("d/m/Y ", (25569 + (($data_raw[38] - 25569) * 86400 / 86400) - 25569) * 86400) }}">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3" for="myta">Input PO MYTA</label>
						<div class="col-md-9">
							<select id="myta" name="myta">
								<option {{ $data[39] == 'BERMASALAH' ? 'selected' : '' }} value="BERMASALAH">BERMASALAH</option>
								<option {{ $data[39] == 'OK' ? 'selected' : '' }} value="OK">OK</option>
								<option {{ $data[39] == 'OK - NON KHS' ? 'selected' : '' }} value="OK - NON KHS<">OK - NON KHS</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3" for="chk_sheet">Program Kerja</label>
						<div class="col-md-9">
							<select id="chk_sheet" name="chk_sheet">
								<option value="IOAN">IOAN</option>
								<option value="WILSUS">WILSUS</option>
								<option value="PSB">PSB</option>
								<option value="Maintenance">Maintenance</option>
								<option value="Konstrusi">Konstrusi</option>
								<option value="PSB">PSB</option>
							</select>
							<textarea id="ket_pk" name="ket_pk" rows="2" cols="50" class="form-control"></textarea>
						</div>
					</div>
				</fieldset>
				<div class="form-actions">
					<div class="row">
						<div class="col-md-8 col-md-offset-4">
							<button type="submit" class="btn btn-danger">Validate &amp; Submit</button>
							<button type="button" class="btn btn-default">Cancel</button>
						</div>
					</div>
				</div>
			</section>
		</div>
	</form>
</div>
@endsection
@section('footerS')
<script src="/js/moment.min.js"></script>
<script src='/js/daterangepicker.js'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/jquery.inputmask.bundle.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script type="text/javascript">
	$(function() {
		$("#p_kerja, #jus_keb, #select, #pembayaran, #myta, #job_stts, #posisi_tgh").select2({
			width: '100%',
		});

		$('#chk_sheet').select2({
			width: '100%',
			placeholder: "Masukkan Program Kerja",
			allowClear: true,
		});

		$('#mitra_select').select2({
			width: '100%',
			placeholder: "Masukkan Nama Perusahaan Mitra",
			allowClear: true,
			minimumInputLength: 4,
			ajax: {
				url: "/get_ajx/mitra/search/tag",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						searchTerm: params.term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true,
				success: function(value) {
					console.log(value)
				}
			}
		});

		var getd = <?= json_encode($data) ?>;
		if(getd[4] != ''){
			$('#mitra_select').html("<option value='" + getd[4] + "'>" + getd[4] + "</option>")
		}

		$('.date-picker').datetimepicker({
			format: 'DD/MM/YYYY',
		});

		$('.price').val(function(index, value) {
			return value
			.replace(/\D/g, "")
			.replace(/\B(?=(\d{3})+(?!\d))/g, ".")
			;
		});

		$('.price').keyup(function(event) {

	// skip for arrow keys
			if(event.which >= 37 && event.which <= 40) return;

	// format number
			$(this).val(function(index, value) {
				return value
				.replace(/\D/g, "")
				.replace(/\B(?=(\d{3})+(?!\d))/g, ".")
				;
			});
		});
	})
</script>
@endsection