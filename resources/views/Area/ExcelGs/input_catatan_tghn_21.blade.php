@extends('layout')
@section('title', 'Input Catatan')
@section('style')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<style type="text/css">
	.select2-results__option {
		color: black;
	}
</style>
@endsection
@section('content')
<div class="body">
<div class="row">
	<form id="validation-form" class="form-horizontal form-label-left" method="post">
		<div class="col-md-12">
			<section class="widget">
				<header>
					<h4>
						<i class="fa fa-check-square-o"></i>
						Catatan / Keterangan
						<small>IOAN / WILSUS / PSB / Maintenance / Konstruksi / SPBU</small>
					</h4>
				</header>
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				{{-- pra kontrak --}}
				<fieldset>
					<legend class="section">Catatan <b>{{ $data[12] }}</b></legend>
					<div class="form-group">
						<label class="control-label col-md-3" for="chk_sheet">Program Kerja</label>
						<div class="col-md-9">
							<select id="chk_sheet" name="chk_sheet">
								<option value="IOAN">IOAN</option>
								<option value="WILSUS">WILSUS</option>
								<option value="PSB">PSB</option>
								<option value="Maintenance">Maintenance</option>
								<option value="Konstrusi">Konstrusi</option>
								<option value="SPBU">SPBU</option>
							</select>
							<textarea id="ket_pk" name="ket_pk" rows="2" cols="50" class="form-control"></textarea>
						</div>
					</div>
				</fieldset>
				<div class="form-actions">
					<div class="row">
						<div class="col-md-8 col-md-offset-4">
							<button type="submit" class="btn btn-danger">Validate &amp; Submit</button>
							<button type="button" class="btn btn-default">Cancel</button>
						</div>
					</div>
				</div>
			</section>
		</div>
		{{-- isi Data  Perusahaan --}}
	</form>
</div>
@endsection
@section('footerS')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script type="text/javascript">
$(function(){
	$('#chk_sheet').select2({
		width: '100%',
		placeholder: "Masukkan Program Kerja",
	});
})
</script>
@endsection