@extends('layout')
@section('title', 'Input Nomor BA' )
@section('style')
@endsection
@section('headerS')
<link rel="stylesheet" href="/css/daterangepicker.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

<style type="text/css">

  .pull-right {
		text-align: right;
	}

  @media (min-width: 768px) {
		.modal-xl {
			width: 100%;
		max-width:1200px;
		}
	}

	.select2-search--inline {
    display: contents;
	}

	.select2-search__field:placeholder-shown {
		width: 100% !important;
	}
</style>
@endsection
@section('content')
@if (Session::has('alerts'))
	@foreach(Session::get('alerts') as $alert)
		<div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
	@endforeach
@endif
<div class="modal fade" id="detail_be" tabindex="-1" role="dialog" aria-labelledby="judul_mdl" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body data_mdl" style=" overflow: hidden;">
        <form class="form-horizontal form-label-right" method="POST" action="/Admin/Rekon/budgetP/{{ $data->id }}">
					<div class="col-md-12">
						<section class="widget">
							<header>
								<h4>
									<i class="fa fa-check-square-o"></i>
									Form Budget Exceeded
								</h4>
							</header>
							<fieldset>
								{{ csrf_field() }}
								<div class="form-group">
									<label class="col-form-label col-md-2" for="pid_be">PID:</label>
									<div class="col-md-12">
										<select class="form-control pid_be" id="pid_be" multiple name="pid_be[]" required>
											@foreach ($get_pid as $v)
												<option value="{{ $v }}">{{ $v }}</option>
											@endforeach
										</select>
									</div>
								</div>
								<input type="hidden" id="BE" name="status_pid" value="BE">
								<div class="form-group">
									<label class="col-form-label col-md-2" for="detail">Catatan:</label>
									<div class="col-md-12">
										<textarea style="resize:none; font-weight: bold" required cols='50' rows="2" class="form-control input-transparent" id="detail" name="detail"></textarea>
									</div>
								</div>
                <div class="form-actions">
                  <div class="row">
                    <div class="col-md-12">
                      <button type="submit" class="btn btn-block btn-danger">Submit PID</button>
                    </div>
                  </div>
                </div>
							</fieldset>
						</section>
					</div>
				</form>
			</div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-md-12">
			<h2 class="page-title">{{$data->judul}}</h2>
			<div class="card-deck" style="display: block">
				<div class="card-deck" style="display: block">
					<form id="input_spen" class="row" method="post">
						{{ csrf_field() }}
						<div class="col-md-12">
							<div class="card shadow mb-4">
								<div class="card-body">
									<div class="form-group row">
										<div class="col-md-12">
											<a class="btn btn-primary btn-block" style="color: #fff" target="_blank" href="/get_detail_laporan/{{$data->id}}"><i class="fe fe-eye fe-16"></i> Detail</a>
										</div>
									</div>
									<div class="form-group row approve_me" style="display: none">
										<label class="col-form-label col-md-2 pull-right" for="upload_boq">Keputusan :</label>
										<div class="col-md-10">
											<select class="form-control" name="status" id="status">
												<option value='APPROVE'>Lanjut Mitra</option>
												{{-- <option value='REJECT'>Kembalikan Ke Mitra</option> --}}
											</select>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-form-label col-md-2 pull-right" for="upload_boq">Tanggal TOC :</label>
										<div class="col-md-10">
											@php
												$toc_tgl = (date('Y-m-d', strtotime($data->tgl_sp .' +'. ($data->tgl_jatuh_tmp - 1) .' day' ) ) );
											@endphp
											<input class="form-control input-transparent" readonly value="{{ $toc_tgl }}">
										</div>
									</div>
									<div class="row border-top py-3">
										<div class="col-md-6">
											<div class="form-group">
												<label class="col-form-label" for="plan_mat">Plan Material</label>
												<input type="text" readonly class="form-control input-transparent price" name="plan_mat" id="plan_mat" value="{{ number_format($data->total_material_sp, 0, '.', '.') }}">
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="col-form-label" for="plan_jasa">Plan Jasa</label>
												<input type="text" readonly class="form-control input-transparent" name="plan_jasa" id="plan_jasa" value="{{ number_format($data->total_jasa_sp, 0, '.', '.') }}">
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="col-form-label" for="rekon_mat">Rekon Material</label>
												<input type="text" readonly class="form-control input-transparent price" name="rekon_mat" id="rekon_mat" value="{{ number_format($data->total_material_rekon, 0, '.', '.') }}">
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="col-form-label" for="rekon_jasa">Rekon Jasa</label>
												<input type="text" readonly class="form-control input-transparent" name="rekon_jasa" id="rekon_jasa" value="{{ number_format($data->total_jasa_rekon, 0, '.', '.') }}">
											</div>
										</div>
									</div>
									<div class="row border-top py-3">
										<div class="card shadow mb-4">
											<div class="card-header">
												<strong class="card-title">Amandemen Perjanjian Kerja Sama</strong>
											</div>
											<div class="card-body">
												<div class="form-group row">
													<label class="col-form-label col-md-3" for="nomor_pks">Nomor</label>
													<div class="col-md-9">
														<input rows="2" style="resize:none;" cols="50" id="nomor_khs" name="nomor_khs" class="form-control input-transparent" readonly value="{{ $data->pks }}">
													</div>
												</div>
												<div class="form-group row">
													<label class="col-form-label col-md-3" for="tgl_pks">Tanggal</label>
													<div class="col-md-9">
														<input rows="2" style="resize:none;" cols="50" id="tgl_pks" name="tgl_pks" class="form-control date-picker input-transparent" readonly value="{{ $data->tgl_pks }}">
													</div>
												</div>
												<div class="form-group row">
													<label class="col-form-label col-md-3" for="amd_pks">Amandemen</label>
													<div class="col-md-9">
														<input rows="2" style="resize:none;" cols="50" id="amd_pks" name="amd_pks" class="form-control input-transparent" value="{{ $data->amd_pks ?? '' }}">
													</div>
												</div>
												<div class="form-group row">
													<label class="col-form-label col-md-3" for="tgl_amd_pks">Tanggal</label>
													<div class="col-md-9">
														<input rows="2" style="resize:none;" cols="50" id="tgl_amd_pks" name="tgl_amd_pks" class="form-control date-picker input-transparent" required="required" value="{{ $data->tgl_amd_pks ?? date('Y-m-d') }}">
													</div>
												</div>
											</div>
										</div>
										<div class="card shadow mb-4">
											<div class="card-header">
												<strong class="card-title">Amandemen SP</strong>
											</div>
											<div class="card-body">
												<div class="form-group row">
													<label class="col-form-label col-md-3" for="nomor_sp">Nomor</label>
													<div class="col-md-9">
														<input rows="2" style="resize:none;" cols="50" id="nomor_sp" name="nomor_sp" class="form-control input-transparent" readonly value="{{ $data->no_sp }}">
													</div>
												</div>
												<div class="form-group row">
													<label class="col-form-label col-md-3" for="tgl_sp">Tanggal</label>
													<div class="col-md-9">
														<input rows="2" style="resize:none;" cols="50" id="tgl_sp" name="tgl_sp" class="form-control date-picker input-transparent" readonly value="{{ $data->tgl_sp }}">
													</div>
												</div>
												<div class="form-group row">
													<label class="col-form-label col-md-3" for="amd_sp">Amandemen</label>
													<div class="col-md-9">
														<input rows="2" style="resize:none;" cols="50" id="amd_sp" name="amd_sp" class="form-control input-transparent" value="{{ $data->amd_sp ?? '' }}">
													</div>
												</div>
												<div class="form-group row">
													<label class="col-form-label col-md-3" for="tgl_amd_sp">Tanggal</label>
													<div class="col-md-9">
														<input rows="2" style="resize:none;" cols="50" id="tgl_amd_sp" name="tgl_amd_sp" class="form-control date-picker input-transparent" required="required" value="{{ $data->tgl_amd_sp ?? date('Y-m-d') }}">
													</div>
												</div>
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="col-form-label" for="bast_price_wo_ppn">Nilai BAST Tanpa PPN</label>
															<input type="text" id="bast_price_wo_ppn" name="bast_price_wo_ppn" class="form-control input-transparent price" disabled value="{{ number_format(@$data->gd_rekon, 0, '.', '.') }}">
														</div>
													</div>
													<div class="col-md-6">
														<div class="form-group">
															<label class="col-form-label" for="bast_price_w_ppn">Nilai BAST dengan PPN</label>
															<input type="text" id="bast_price_w_ppn" name="bast_price_w_ppn" class="form-control input-transparent price" disabled value="{{ number_format(@$data->total_rekon, 0, '.', '.') }}">
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="row border-top py-3">
										@php
												$toc_tgl = date('Y-m-d', strtotime($data->tgl_sp . ' + '. ($data->tgl_jatuh_tmp - 1) .' days') );
												// H -1 dari toc
										@endphp
										<div class="card shadow mb-4">
											<div class="card-header">
												<strong class="card-title">BAUT</strong>
											</div>
											<div class="card-body row">
												<div class="col-md-6">
													<div class="form-group">
														<label class="col-form-label" for="baut">Nomor</label>
														<input type="text" class="form-control input-transparent price" name="baut" id="baut" value="{{ $data->BAUT }}">
														<code>*Contoh No BAUT: 12345/HK.800/TA-123456/08(/ atau . atau -)2021</code>
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														<label class="col-form-label" for="tgl_baut">Tanggal</label>
														<input type="text" class="form-control date-picker input-transparent" name="tgl_baut" id="tgl_baut" value="{{ $data->tgl_baut ?? $toc_tgl }}">
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="card shadow mb-4">
											<div class="card-header">
												<strong class="card-title">BAST</strong>
											</div>
											<div class="card-body row">
												<div class="col-md-6">
													<div class="form-group">
													<label class="col-form-label" for="bast">Nomor</label>
													<input type="text" class="form-control input-transparent price" name="bast" id="bast" value="{{ $data->BAST }}">
													</div>
													<code>*Contoh No BAST: 12345/HK.800/TA-123456/08(/ atau . atau -)2021</code>

												</div>
												<div class="col-md-6">
													<div class="form-group">
														<label class="col-form-label" for="tgl_bast">Tanggal</label>
														<input type="text" class="form-control date-picker input-transparent" name="tgl_bast" id="tgl_bast" value="{{ $data->tgl_bast ?? date('Y-m-d') }}">
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="card shadow mb-4">
											<div class="card-header">
												<strong class="card-title">BA ABD</strong>
											</div>
											<div class="card-body row">
												<div class="col-md-6">
													<div class="form-group">
														<label class="col-form-label" for="ba_abd">Nomor</label>
														<input type="text" class="form-control input-transparent price" name="ba_abd" id="ba_abd" value="{{ $data->ba_abd }}">
														<code>*Contoh No BA ABD: 12345/HK.800/TA-123456/08(/ atau . atau -)2021</code>
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														<label class="col-form-label" for="tgl_ba_abd">Tanggal</label>
														<input type="text" class="form-control input date-picker input-transparent" name="tgl_ba_abd" id="tgl_ba_abd" value="{{ $data->tgl_ba_abd ?? $toc_tgl }}">
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="card shadow mb-4">
											<div class="card-header">
												<strong class="card-title">BA Rekon</strong>
											</div>
											<div class="card-body row">
												<div class="col-md-6">
													<div class="form-group">
														<label class="col-form-label" for="ba_rekon">Nomor BA Rekon</label>
														<input type="text" class="form-control input-transparent price" name="ba_rekon" id="ba_rekon" value="{{ $data->ba_rekon }}">
														<code>*Contoh No BA REKON: 12345/HK.800/TA-123456/08(/ atau . atau -)2021</code>
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														<label class="col-form-label" for="tgl_ba_rekon">Tanggal BA Rekon</label>
														<input type="text" class="form-control input date-picker input-transparent" name="tgl_ba_rekon" id="tgl_ba_rekon" value="{{ $data->tgl_ba_rekon ?? $toc_tgl }}">
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="card shadow mb-4">
											<div class="card-header">
												<strong class="card-title">BAPP</strong>
											</div>
											<div class="card-body row">
												<div class="col-md-6">
													<div class="form-group">
														<label class="col-form-label" for="bapp">Nomor</label>
														<input type="text" class="form-control input-transparent price" name="bapp" id="bapp" value="{{ $data->BAPP }}">
														<code>*Contoh No BAPP: 12345/HK.800/TA-123456/08(/ atau . atau -)2021</code>
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														<label class="col-form-label" for="tgl_bapp">Tanggal</label>
														<input type="text" class="form-control input date-picker input-transparent" name="tgl_bapp" id="tgl_bapp" value="{{ $data->tgl_bapp ?? $toc_tgl }}">
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="form-group row reject_reg" style="display: none">
										<label class="col-form-label col-md-2 pull-right" for='detail_reg'>Keterangan Regional:</label>
										<div class="col-md-10">
											<textarea style="resize: none;" rows="2" name="detail_reg" id="detail_reg" class="form-control input-transparent" disabled></textarea>
										</div>
									</div>
									<div class="form-group row procc_mit">
										<label class="col-form-label col-md-2 pull-right alasan_tl" for='detail'>Keterangan :</label>
										<div class="col-md-10">
											<textarea style="resize: none;" rows="2" name="detail" id="detail" class="form-control input-transparent" required></textarea>
										</div>
									</div>
									<input type="hidden" name="jns_btn" value="submit">
									<div class="form-group procc_mit">
										<div class="row">
											<div class="col-md-4">
												<button type="submit" class="btn btn_submit btn-primary btn-block"><i class="fe fe-check fe-16">&nbsp;Submit</i></button>
											</div>
											<div class="col-md-4">
												<button type="submit" class="btn btn-info btn_save btn-block"><i class="fe fe-save fe-16">&nbsp;Simpan</i></button>
											</div>
											<div class="col-md-4">
												<a type="button" data-toggle="modal" data-target="#detail_be" class="btn btn-danger btn-block" style="color: white;"><i class="fe fe-alert-triangle fe-16">&nbsp;Budget Exceeded</i></a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('footerS')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="/js/moment.min.js"></script>
<script src='/js/jquery.timepicker.js'></script>
<script src='/js/daterangepicker.js'></script>
<script type="text/javascript">

$(function(){
  $('.modal_reject').click( function(){
    console.log($(this).attr('data-judul'))
    $('#judul_reject').html($(this).attr('data-judul'));
  });

	$('.btn_save').click(function(){
		$("input[name='jns_btn']").val('save_me');
	})

	$('.btn_submit').click(function(){
		$("input[name='jns_btn']").val('submit');
	})

	// $('select[id="mitra_select"]').append(new Option('ini text', 7, true, true)).trigger('change');

	$('.date-picker').daterangepicker(
		{
			singleDatePicker: true,
			timePicker: false,
			showDropdowns: true,
			locale:
			{
				format: 'YYYY-MM-DD',
			}
		}
	);

	$('.pid_be').select2({
		width: '100%',
		placeholder: 'Masukkan PID OverBudget',
		allowClear: true
	});

	var data = {!! json_encode($data) !!};

	if(data.step_id == 18){
		$('.reject_reg').css({
			'display': 'flex'
		});

		// $('.approve_me, .procc_mit').detach()
		$('.approve_me').detach()

		$('.alasan_tl').html('Tambahkan Alasan :');

		$('#detail_reg').val(data.detail)
	}else{
		$('.approve_me').css({
			'display': 'flex'
		});

		$('.reject_reg').detach()
	}

});
</script>
@endsection