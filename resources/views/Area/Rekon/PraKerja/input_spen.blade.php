@extends('layout')
@section('title', @$data->step_id == 3 ? 'Input Dokumen Surat Penetapan' : (@$data->step_id == 5 ?'Input Dokumen SP' : 'Update SP') )
@section('style')
@endsection
@section('headerS')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="/css/daterangepicker.css">
<style type="text/css">
  .select2-results__option, select{
      color: black;
  }

  .pull-right {
		text-align: right;
	}

  @media (min-width: 768px) {
		.modal-xl {
			width: 100%;
		max-width:1200px;
		}
	}

	.select2-search--inline {
    display: contents;
	}

	.select2-search__field:placeholder-shown {
		width: 100% !important;
	}
</style>
@endsection
@section('content')
@if (Session::has('alerts'))
	@foreach(Session::get('alerts') as $alert)
		<div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
	@endforeach
@endif
<div class="modal fade" id="detail_be" tabindex="-1" role="dialog" aria-labelledby="judul_mdl" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body data_mdl" style=" overflow: hidden;">
        <form class="form-horizontal form-label-right" method="POST" action="/Admin/sp/budgetP/{{ $data->id }}">
					<div class="col-md-12">
						<section class="widget">
							<header>
								<h4>
									<i class="fa fa-check-square-o"></i>
									Form Budget Exceeded
								</h4>
							</header>
							<fieldset>
								{{ csrf_field() }}
								<div class="form-group">
									<label class="col-form-label col-md-2" for="pid_be">PID:</label>
									<div class="col-md-12">
										<select class="form-control pid_be" id="pid_be" multiple name="pid_be[]" required>
											@foreach ($get_pid as $v)
												<option value="{{ $v }}">{{ $v }}</option>
											@endforeach
										</select>
									</div>
								</div>
								{{-- <div class="form-group">
									<label class="col-form-label col-md-2" for="pid_be">Status PID:</label>
									<div class="col-md-12">
											<div class="custom-control custom-radio">
													<input type="radio" id="BE" name="status_pid" class="custom-control-input" value="BE">
													<label class="custom-control-label" for="BE" checked>Budget Exceeded</label>
											</div>
									</div>
									<div class="col-md-12">
											<div class="custom-control custom-radio">
													<input type="radio" id="salah_pid" name="status_pid" class="custom-control-input" value="SALAH PID">
													<label class="custom-control-label" for="salah_pid">Salah PID</label>
											</div>
									</div>
								</div> --}}
								<input type="hidden" id="BE" name="status_pid" value="BE">
								<div class="form-group">
									<label class="col-form-label col-md-2" for="detail">Catatan:</label>
									<div class="col-md-12">
										<textarea style="resize:none; font-weight: bold" required cols='50' rows="2" class="form-control input-transparent" id="detail" name="detail"></textarea>
									</div>
								</div>
                <div class="form-actions">
                  <div class="row">
                    <div class="col-md-12">
                      <button type="submit" class="btn btn-block btn-danger">Submit PID</button>
                    </div>
                  </div>
                </div>
							</fieldset>
						</section>
					</div>
				</form>
			</div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-md-12"> <h2 class="page-title">Data Penetapan</h2>
			<p class="text-muted">Untuk Menghasilkan Excel Yang Diperlukan</p>
			<div class="card-deck" style="display: block">
				<form id="input_spen" class="row" method="post">
					{{ csrf_field() }}
					<div class="col-md-12">
						<div class="card shadow mb-4">
							<div class="card-header">
								<strong class="card-title">{{ $data->judul }}</strong>
							</div>
							<div class="card-body">
								<div class="row">
									<div class="form-group mb-3 col-md-6">
										<label class="col-form-label pull-right" for="title">PID</label>
										<textarea type="text" style="resize: none;" rows="4" class="form-control" name="title" id="title" readonly>{{ $data->id_project }}</textarea>
									</div>
									<div class="form-group mb-3 col-md-6">
										<label class="col-form-label pull-right" for="beforeppn">Harga Sebelum PPN</label>
										<input type="text" class="form-control" name="beforeppn" required id="beforeppn" readonly value="{{ $data->gd_sp ?? '' }}">
									</div>
								</div>
								<div class="row border-top py-3">
									<div class="form-group mb-1 col-md-4">
										<label class="col-form-label" for="pks">Nomor Kontrak</label>
										<input type="text" class="form-control" name="pks" required id="pks" value="{{ $data->no_khs ?? '' }}">
										<code>*Contoh No Kontrak: 12345/HK.810/TA-123456/08-2020</code>
									</div>
									<div class="form-group mb-1 col-md-4">
										<label class="col-form-label" for="tgl_pks">Tanggal Kontrak</label>
										@php
											$bulan = array (
												'Januari' => '01',
												'Februari' => '02',
												'Maret' => '03',
												'April' => '04',
												'Mei' => '05',
												'Juni' => '06',
												'Juli' => '07',
												'Agustus' => '08',
												'September' => '09',
												'Oktober' => '10',
												'November' => '11',
												'Desember' => '12'
											);

											$tgl_conv = date('Y-m-d');

											if($data->tgl_khs_maintenance)
											{
												$tgl = explode(' ', $data->tgl_khs_maintenance);
												$tgl_conv = $tgl[2] .'-'. $bulan[$tgl[1] ] .'-'. $tgl[0];
											}

											if($data->tgl_pks == 0000-00-00 || empty($data->tgl_pks) )
											{
												$data->tgl_pks = $tgl_conv;
											}
										@endphp
										<input type="text" id="tgl_pks" name="tgl_pks" class="form-control date-picker" required value="{{ $data->tgl_pks }}">
									</div>
									<div class="form-group mb-1 col-md-4">
										<label class="col-form-label" for="lokasi_project">Lokasi Kerja</label>
										<input type="text" class="form-control" name="lokasi_project" required id="lokasi_project" value="{{ $data->lokasi ?? 'BANJARMASIN WITEL KALSEL' }}">
									</div>
								</div>
								<div class="row">
									<div class="form-group mb-3 col-md-4">
										<label class="col-form-label" for="s_pen">Nomor Penetapan</label>
										<input type="text" id="s_pen" name="s_pen" class="form-control" value="{{ $data->surat_penetapan ?? '' }}" required>
										<code>*Contoh No Penetapan: 12345/HK.810/TA-123456/08-2020</code>
									</div>
									<div class="form-group mb-3 col-md-4">
										<label class="col-form-label" for="tgl_s_pen">Tanggal Penetapan</label>
											@php
												$date = date('Y-m-d', strtotime(date('Y-m-d') . "last day of previous month") );
											@endphp
											<input type="text" id="tgl_s_pen" name="tgl_s_pen" class="form-control date-picker" required value="{{ date('Y-m-d', strtotime($date . "- 1 days") ) }}">
									</div>
									<div class="form-group mb-3 col-md-4">
										<label class="col-form-label" for="toc">TOC</label>
										<input type="text" id="toc" name="toc" class="form-control" autocomplete="off" required="required" value="{{ !empty($data->toc) ? $data->toc : '' }}">
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="card shadow mb-4 col-md-4">
						<button class="btn btn-primary" type="submit"><i class="fe fe-check fe-16"></i>&nbsp;Submit Form</button>
					</div>
					<div class="card shadow mb-4 col-md-4">
						<a class="btn btn-info" href="/download/full_rar/{{ Request::segment(2) }}" type="submit"><i class="fe fe-download fe-16"></i>&nbsp;Download PO</a>
					</div>
					@if ($data->step_id != 26)
						<div class="card shadow mb-4 col-md-4">
							<a class="btn btn-danger" type="button" data-toggle="modal" data-target="#detail_be" style="color: white;"><i class="fe fe-alert-triangle fe-16"></i>&nbsp;PID Over Budget</a>
						</div>
					@endif
				</form>
			</div>
		</div>
	</div>
</div>
@endsection
@section('footerS')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="/js/moment.min.js"></script>
<script src='/js/jquery.timepicker.js'></script>
<script src='/js/daterangepicker.js'></script>
<script type="text/javascript">

$(function(){
	var data = {!! json_encode($data) !!},
	get_data;

	$('.pid_be').select2({
		width: '100%',
		placeholder: 'Masukkan PID OverBudget',
		allowClear: true
	});

	$('.date-picker').daterangepicker(
		{
			singleDatePicker: true,
			timePicker: false,
			showDropdowns: true,
			locale:
			{
				format: 'YYYY-MM-DD',
			}
		}
	);

  $('#beforeppn').val(function(index, value) {
		return value
		.replace(/\D/g, "")
		.replace(/\B(?=(\d{3})+(?!\d))/g, ".")
		;
	});
});
</script>
@endsection