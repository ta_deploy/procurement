@extends('layout')
@section('title', @$data->step_id == 3 ? 'Input Dokumen Surat Penetapan' : (@$data->step_id == 5 ? 'Input Dokumen SP' : 'Update SP') )
@section('style')
@endsection
@section('headerS')
<link rel="stylesheet" href="/css/daterangepicker.css">
<style type="text/css">

  .pull-right {
		text-align: right;
	}

  @media (min-width: 768px) {
		.modal-xl {
			width: 100%;
		max-width:1200px;
		}
	}
</style>
@endsection
@section('content')
@if (Session::has('alerts'))
	@foreach(Session::get('alerts') as $alert)
		<div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
	@endforeach
@endif
<div class="modal fade" id="detail_reject" tabindex="-1" role="dialog" aria-labelledby="judul_mdl" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body data_mdl" style=" overflow: hidden;">
        <form class="form-horizontal form-label-right" method="POST" action="/Admin/sp/reject_data/{{ Request::segment(2) }}">
					<div class="col-md-12">
						<section class="widget">
							<header>
								<h4>
									<i class="fa fa-check-square-o"></i>
									Detail Reject <span style="color: red" id="judul_reject"></span>
								</h4>
							</header>
							<fieldset>
								<div class="form-group">
									{{ csrf_field() }}
									<label class="col-form-label col-md-2" for="alasan_reject">Alasan Reject:</label>
									<div class="col-md-12">
										<textarea style="resize:none; font-weight: bold" cols='50' rows="2" class="form-control input-transparent" id="alasan_reject" name="alasan_reject"></textarea>
									</div>
								</div>
                <div class="form-actions">
                  <div class="row">
                    <div class="col-md-12">
                      <button type="submit" class="btn btn-block btn-danger">Reject Dokumen</button>
                    </div>
                  </div>
                </div>
							</fieldset>
						</section>
					</div>
				</form>
			</div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-md-12"> <h2 class="page-title">{{ @$data->step_id == 3 ? 'Input Dokumen Surat Penetapan' : (@$data->step_id == 5 ? 'Input Dokumen SP' : 'Update SP') }}</h2>
			<p class="text-muted">Untuk Menghasilkan Excel Yang Diperlukan</p>
			<div class="card-deck" style="display: block">
				<form id="input_spen" class="row" method="post">
					{{ csrf_field() }}
					<div class="col-md-12">
						<div class="card shadow mb-4">
							<div class="card-header">
								<strong class="card-title">Data SP</strong>
							</div>
							<div class="card-body row">
								<div class="form-group col-md-4">
									<label class="col-form-label" for="pid">PID</label>
									@if($data->pekerjaan == "PSB")
										<input type="text" class="form-control" name="pid" id="pid" disabled value="{{ $data->id_project ?? '' }}">
									@else
										<textarea rows="2" style="resize: none;" class="form-control" name="pid" id="pid" disabled>{{ $data->id_project ?? '' }}</textarea>
									@endif
								</div>
								<div class="form-group col-md-6">
									<label class="col-form-label" for="title">Uraian</label>
										<textarea rows="2" style="resize: none;" class="form-control" name="title" id="title" disabled>{{ $data->judul ?? '' }}</textarea>
								</div>
								<div class="form-group col-md-2">
									<label class="col-form-label" for="mitra_select">Jenis Pekerjaan</label>
										<input type="hidden" name="mitra_id" value="{{ $data->mitra_id }}">
										<input type="text" class="form-control" name="mitra_select" id="mitra_select" value="{{ $data->jenis_work ?? '' }}" disabled>
								</div>
								<div class="form-group col-md-4 group_pks">
									<label class="col-form-label" for="pks">PKS</label>
									<input type="text" class="form-control" name="pks" id="pks" value="{{ (in_array($get_khs->khs, ['Maintenance', 'QE']) ? $data->pks : $data->no_kontrak_ta) }}">
								</div>
								<div class="form-group col-md-2 group_pks">
									<label class="col-form-label" for="tgl_pks">Tanggal PKS</label>
									<input type="text" class="form-control date-picker" id="tgl_pks" name="tgl_pks" value="{{ (in_array($get_khs->khs, ['Maintenance', 'QE']) ? $data->tgl_pks : $data->date_kontrak_ta ) }}">
								</div>
								<div class="form-group col-md-4">
									<label class="col-form-label" for="s_pen">Nomor Penetapan KHS</label>
									<input type="text" id="s_pen" name="s_pen" class="form-control" readonly value="{{ $data->surat_penetapan ? : $data->no_penetapan_khs ?? '' }}">
								</div>
								<div class="form-group col-md-2">
									<label class="col-form-label" for="tgl_s_pen">Tanggal Penetapan KHS</label>
									@if($data->pekerjaan == "PSB")
										<input type="hidden" name="tgl_s_pen" value="{{ $data->date_khs_penetapan }}">
										<input type="text" id="tgl_s_pen" class="form-control" value="{{ $data->tgl_khs_penetapan ?? '' }}" disabled>
									@else
										<input type="text" id="tgl_s_pen" name="tgl_s_pen" class="form-control date-picker" readonly value="{{ ($data->tgl_s_pen ?? date('Y-m-d') ) }}">
									@endif
								</div>
								<div class="form-group col-md-4 group_pks">
									<label class="col-form-label" for="no_kesanggupan_khs">Nomor Kesanggupan KHS</label>
									<input type="text" class="form-control" name="no_kesanggupan_khs" id="no_kesanggupan_khs" value="{{( $data->surat_kesanggupan  ??$data->no_kesanggupan_khs) }}" readonly>
								</div>
								<div class="form-group col-md-2 group_pks">
									<label class="col-form-label" for="tgl_khs_kesanggupan" style="font-size: 13px;">Tanggal Kesanggupan KHS</label>
									<input type="hidden" name="tgl_surat_sanggup" value="{{ ($data->tgl_surat_sanggup ?? $data->date_khs_kesanggupan) }}">
									<input type="text" class="form-control" name="tgl_khs_kesanggupan" id="tgl_khs_kesanggupan" value="{{ $data->tgl_surat_sanggup ?? $data->tgl_khs_kesanggupan }}" readonly>
								</div>
								<div class="form-group col-md-4">
									<label class="col-form-label" for="no_sp">Nomor Pesanan</label>
									<input type="text" id="no_sp" name="no_sp" class="form-control" value="{{ $data->no_sp }}" required>
									<code>*Contoh No Pesanan: 12345/HK.810/TA-123456/08-2020</code>
								</div>
								<div class="form-group col-md-2">
									<label class="col-form-label" for="tgl_sp">Tanggal Pesanan</label>
									@if($data->pekerjaan == "PSB")
										<input type="text" id="tgl_sp" name="tgl_sp" class="form-control date-picker" required="required" value="{{ $data->tgl_sp }}">
									@else
										@php
											$date = date('Y-m-d', strtotime(date($data->bulan_pengerjaan.'-01') . "first day of this month") );
										@endphp
										<input type="text" id="tgl_sp" name="tgl_sp" class="form-control date-picker" required="required" value="{{ $date }}">
									@endif
								</div>
								<div class="form-group col-md-3">
									<label class="col-form-label" for="toc">TOC</label>
									<input type="text" id="toc" name="toc" class="form-control" autocomplete="off" required="required" value="{{ $data->tgl_jatuh_tmp }}">
									<span id="durasi"></span>
								</div>
								<div class="form-group col-md-5">
									<label class="col-form-label" for="lokasi_project">Lokasi Kerja</label>
									<input type="text" class="form-control" name="lokasi_project" id="lokasi_project" required value="{{ $data->lokasi ? : $data->witel ? : 'BANJARMASIN WITEL KALSEL' }}">
								</div>
								<div class="form-group col-md-4">
									<label class="col-form-label" for="beforeppn">Harga Sebelum PPN</label>
									@if($data->pekerjaan == "PSB")
										<input type="text" class="form-control" name="beforeppn" id="beforeppn" value="0" readonly>
									@else
										<input type="text" class="form-control" name="beforeppn" id="beforeppn" value="{{ $data->gd_sp ?? '' }}" readonly>
									@endif
								</div>
								<div class="form-group col-md-12">
									<label class="col-form-label">Download File</label>
									<br />
										@if($data->pekerjaan <> "PSB")
											<a type="button" href="/download/full_rar/{{ $data->id }}" class="btn btn-sm btn-primary" style="color: #fff"><i class="fe fe-download"></i>&nbsp; Full RAR</a>
										&nbsp;
										@elseif($data->pekerjaan == "PSB")
											<a class="btn btn-sm btn-primary" href="/Admin/download/filepsb/justifikasi/{{ $data->id }}/download"><i class="fe fe-download"></i>&nbsp; Justifikasi & Rincian SSL (PSB)</a>
										&nbsp;
										@endif
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="card-body row">
							<div class="form-group col-md-6">
								<button class="btn btn-primary btn-block" type="submit"><i class="fe fe-check fe-16"></i>&nbsp;Submit</button>
							</div>
							<div class="form-group col-md-6">
								<a type="button" class="btn btn-warning modal_reject btn-block" data-judul="{{ $data->judul }}" style="color: black" data-toggle="modal" data-target="#detail_reject"><i class="fe fe-x fe-16"></i>&nbsp;Reject</a>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection
@section('footerS')
<script src="/js/moment.min.js"></script>
<script src='/js/jquery.timepicker.js'></script>
<script src='/js/daterangepicker.js'></script>
<script type="text/javascript">

$(function(){
  $('.modal_reject').click( function(){
    console.log($(this).attr('data-judul'))
    $('#judul_reject').html($(this).attr('data-judul'));
  });

	$('.date-picker').daterangepicker(
		{
			singleDatePicker: true,
			timePicker: false,
			showDropdowns: true,
			locale:
			{
				format: 'YYYY-MM-DD',
			}
		}
	);

  $('#beforeppn').val(function(index, value) {
		return value
		.replace(/\D/g, "")
		.replace(/\B(?=(\d{3})+(?!\d))/g, ".")
		;
	});

	$('#beforeppn').keyup(function(event) {
		if(event.which >= 37 && event.which <= 40) return;
		$(this).val(function(index, value) {
			return value
			.replace(/\D/g, "")
			.replace(/\B(?=(\d{3})+(?!\d))/g, ".")
			;
		});
	});

	$('#tgl_sp').on('change', function(e) {
		var kalkulasi_date = new Date(new Date($('#tgl_sp').val()).setDate(new Date($('#tgl_sp').val()).getDate() + parseInt($('#toc').val().length != '0' ? parseInt($('#toc').val() ) - 1 : 0))).toISOString().slice(0, 10)
		$('#durasi').html('<code>*Tanggal TOC Adalah '+kalkulasi_date+'</code>');
	});

	$('#toc').on('keypress keyup', function(e) {
		if (/\D/g.test(this.value))
		{
			this.value = this.value.replace(/\D/g, '');
		}
			var kalkulasi_date = new Date(new Date($('#tgl_sp').val()).setDate(new Date($('#tgl_sp').val()).getDate() + parseInt($('#toc').val().length != '0' ? parseInt($('#toc').val() )  - 1 : 0))).toISOString().slice(0, 10)
			$('#durasi').html('<code>*Tanggal TOC Adalah '+kalkulasi_date+'</code>');
	});

	if (/\D/g.test($('#toc').val()))
	{
		$('#toc').val() = $('#toc').val().replace(/\D/g, '');
	}

	var kalkulasi_date = new Date(new Date($('#tgl_s_pen').val()).setDate(new Date($('#tgl_s_pen').val()).getDate() + parseInt($('#toc').val().length != '0' ? parseInt($('#toc').val() )  - 1 : 0))).toISOString().slice(0, 10)
	$('#durasi').html('<code>*Tanggal TOC Adalah '+kalkulasi_date+'</code>');

});
</script>
@endsection