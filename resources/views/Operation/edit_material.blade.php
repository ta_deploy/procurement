@extends('layout')
@section('title', !is_numeric($id) ? 'Tambah Material' : 'Edit Material')
@section('headerS')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="/css/daterangepicker.css">
<link rel="stylesheet" href="/css/dataTables.bootstrap4.css">
<style type="text/css">
	.pull-right {
		text-align: right;
	}
</style>
@endsection
@section('content')
@if (Session::has('alerts'))
	@foreach(Session::get('alerts') as $alert)
		<div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
	@endforeach
@endif
<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-md-12">
			<h2 class="page-title">Form Material</h2>
			<div class="card-deck" style="display: block">
				<form method="POST">
					{{ csrf_field() }}
					<div class="col-md-12">
						<div class="card shadow mb-4">
							<div class="card-body">
								<div class="form-group row">
									<label class="col-form-label col-md-2 pull-right" for="pekerjaan">Pekerjaan</label>
									<div class="col-md-10">
										<select name="pekerjaan" class="form-control input-transparent" id="pekerjaan">
											@foreach ($kerjaan[2] as $val)
												<option value="{{ $val->id }}">{{ $val->text }}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-md-2 pull-right" for="mitra">Mitra</label>
									<div class="col-md-10">
										<select name="mitra" class="form-control input-transparent" id="mitra">
											{{-- @foreach ($mitra as $val)
												<option value="{{ $val->id }}">{{ $val->nama_company }}</option>
											@endforeach --}}
											<option value="{{ $mitra->id }}">{{ $mitra->nama_company }}</option>
											<option value="0">MITRA</option>
										</select>
									</div>
								</div>
								<div class="form-group row">
									<div class="col-md-5 offset-md-2">
										<p class="mb-2"><strong>Tanggal Durasi</strong></p>
										<div class="custom-control custom-radio">
											<input type="radio" id="yes" name="tgl_durasi" class="custom-control-input chck" value="yes" checked>
											<label class="custom-control-label" for="yes">Ada</label>
										</div>
										<div class="custom-control custom-radio">
											<input type="radio" id="nein" name="tgl_durasi" class="custom-control-input chck" value="nein">
											<label class="custom-control-label" for="nein">Tidak Ada</label>
										</div>
									</div>
								</div>
								<div class="form-group row tgl_field_r">
									<label class="col-form-label col-md-2 pull-right">Tanggal Awal</label>
									<div class="col-md-5">
										<select id="tahun_start" class="form-control tahun" name="tahun_start">
											@for ($i = date('Y'); $i >= date('Y', strtotime('-3 year') ); $i--)
												<option value="{{ $i }}">{{ $i }}</option>
											@endfor
										</select>
									</div>
									<div class="col-md-5">
										<select id="bulan_start" class="form-control bulan" name="bulan_start">
											<option value="01">Januari</option>
											<option value="02">Februari</option>
											<option value="03">Maret</option>
											<option value="04">April</option>
											<option value="05">Mei</option>
											<option value="06">Juni</option>
											<option value="07">Juli</option>
											<option value="08">Agustus</option>
											<option value="09">September</option>
											<option value="10">Oktober</option>
											<option value="11">November</option>
											<option value="12">Desember</option>
										</select>
									</div>
								</div>
								<div class="form-group row tgl_field_r">
									<label class="col-form-label col-md-2 pull-right">Tanggal Akhir</label>
									<div class="col-md-5">
										<select id="tahun_end" class="form-control tahun" name="tahun_end">
											@php
												// for ($i = date('Y'); $i >= date('Y', strtotime('-5 year') ); $i--){
												// 	$tahun[$i] = $i;
												// }

												for ($i = date('Y'); $i < date('Y', strtotime('+5 year') ); ++$i){
													$tahun[$i] = $i;
												}
												rsort($tahun);
											@endphp
											@foreach ($tahun as $i)
												<option value="{{ $i }}">{{ $i }}</option>
											@endforeach
										</select>
									</div>
									<div class="col-md-5">
										<select id="bulan_end" class="form-control bulan" name="bulan_end">
											<option value="01">Januari</option>
											<option value="02">Februari</option>
											<option value="03">Maret</option>
											<option value="04">April</option>
											<option value="05">Mei</option>
											<option value="06">Juni</option>
											<option value="07">Juli</option>
											<option value="08">Agustus</option>
											<option value="09">September</option>
											<option value="10">Oktober</option>
											<option value="11">November</option>
											<option value="12">Desember</option>
										</select>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-md-2 pull-right" for="relasi_mat">Relasi Material</label>
									<div class="col-md-10">
										<select id="relasi_mat" name="relasi_mat" class="relasi_mat"></select>
										<code>* Jika tidak ada relasi, tida perlu diisi</code>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label pull-right col-md-2" for="designator">Nama Material</label>
									<div class="col-md-10">
										<input type="text" class="form-control" name="designator" id="designator" value="{{ @$data->designator ?? '' }}">
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label pull-right col-md-2" for="uraian">Uraian</label>
									<div class="col-md-10">
										<textarea rows="2" style="resize: none;" required class="form-control" name="uraian" id="uraian">{{ @$data->uraian ? html_entity_decode($data->uraian, ENT_QUOTES, 'UTF-8') : '' }}</textarea>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label pull-right col-md-2" for="satuan">Satuan</label>
									<div class="col-md-10">
										<input type="text" class="form-control" name="satuan" id="satuan" value="{{ @$data->satuan ?? '' }}">
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label pull-right col-md-2" for="material">Material</label>
									<div class="col-md-10">
										<input type="text" class="form-control price" name="material" id="material" value="{{ @$data->material ?? '' }}">
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label pull-right col-md-2" for="jasa">Jasa</label>
									<div class="col-md-10">
										<input type="text" class="form-control price" name="jasa" id="jasa" value="{{ @$data->jasa ?? '' }}">
									</div>
								</div>
								<div class="form-group row">
									<div class="col-md-12">
										<button type="submit" class="btn btn-block btn-primary">Save</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection
@section('footerS')
<script src="/js/moment.min.js"></script>
<script src="/js/daterangepicker.js"></script>
<script src='/js/select2.min.js'></script>
<script src='/js/jquery.timepicker.js'></script>
<script>
	$('#pekerjaan').select2({
		width: '100%',
		placeholder: 'Pilih jenis pekerjaan',
	});

	$('#mitra').select2({
		width: '100%',
		placeholder: 'Pilih Nama Perusahaan',
	});

	$("#yes").prop('checked', true);

	$('.price').val(function(index, value) {
		return value
		.replace(/\D/g, "")
		.replace(/\B(?=(\d{3})+(?!\d))/g, ".")
		;
	});

	$('#relasi_mat').select2({
		width: '100%',
		placeholder: "Masukkan Nama Material",
		allowClear: true,
		ajax: {
			url: "/get_ajx/find_material",
			dataType: 'json',
			delay: 250,
			data: function (params) {
				isi = params.term;
				return {
					searchTerm: params.term,
					pekerjaan : $('#pekerjaan').val(),
					tahun : $('#tahun_end').val(),
					bulan : $('#bulan_end').val(),
					witel : {!! json_encode(session('auth')->Witel_New) !!},
				};
			},
			processResults: function (response) {
				return {
					results: response
				};
			},
			cache: true,
		},
		escapeMarkup: function (markup) {
			return markup;
		},
		templateSelection: function(data) {
			var $result = $(
				"<span data-id=" +data.id+ " data-id_mitra=" +data.id_mitra+ ">" + data.text + "</span>"
			);
			return $result;
		}
	});

	var data = {!! json_encode($data) !!},
	data_ref = {!! json_encode($data_ref) !!};

	if(!$.isEmptyObject(data) ){
		if(data.tgl_start){
			var tgl_start = data.tgl_start.split('-');
			var tgl_end = data.tgl_end.split('-');

			$('#tahun_start').val(tgl_start[0]).change();
			$('#bulan_start').val(tgl_start[1]).change();
			$('#tahun_end').val(tgl_end[0]).change();
			$('#bulan_end').val(tgl_end[1]).change();

			if(!$.isEmptyObject(data_ref) ){
				$("#relasi_mat").append('<option value="'+ data_ref.id +'" selected>'+  data_ref.designator +' (' + data_ref.jenis + ' | ' + data_ref.namcomp + ')</option>').change();
			}
		}
	}

	$("input[name='tgl_durasi']").on('change', function (){
		if($(this).val() == 'yes'){
			$(".tgl_field_r").css({
				'display': 'flex'
			});

			$('.tgl_field_r select').attr('required', 'required');
		}else{
			$(".tgl_field_r").css({
				'display': 'none'
			});

			$('.tgl_field_r select').removeAttr('required');
		}
	})

	if(data.tgl_start == null){
		$("input[name='tgl_durasi'][value='nein']").prop("checked", true).trigger("change");
	}

	$('.price').keyup(function(event) {
		if(event.which >= 37 && event.which <= 40) return;
		$(this).val(function(index, value) {
			return value
			.replace(/\D/g, "")
			.replace(/\B(?=(\d{3})+(?!\d))/g, ".")
			;
		});
	});
</script>
@endsection