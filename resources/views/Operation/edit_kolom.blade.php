@extends('layout')
@section('title', 'Edit Pekerjaan')
@section('headerS')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" rel="stylesheet" />
<link rel="stylesheet" href="/css/dataTables.bootstrap4.css">
<style type="text/css">
	.pull-right {
		text-align: right;
	}

  .toggleCard{
		display: block;
	}
  .card-body{
		display: block;
	}

	.template_boq{
		font-size: 0.8em!important;
	}

	.field_material_input{
		width: 60px!important;
		height: 32px;
	}

	@media (min-width: 768px) {
		.modal-xl {
			width: 100%;
			max-width:1200px;
		}
	}
</style>
@endsection
@section('content')
@if (Session::has('alerts'))
	@foreach(Session::get('alerts') as $alert)
		<div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
	@endforeach
@endif
<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-md-12">
			<h2 class="page-title">Request PID</h2>
			<div class="card-deck" style="display: block">
				<form id="submit_lampiran_boq" class="row" method="post">
					{{ csrf_field() }}
					<div class="col-md-12">
						<div class="card shadow mb-4">
							<div class="card-body">
								<div class="form-group row">
									<label class="col-form-label col-md-2 pull-right" for="pekerjaan">Pekerjaan</label>
									<div class="col-md-10">
										<select name="pekerjaan" class="form-control input-transparent" id="pekerjaan" required>
											@foreach ($kerjaan[0] as $val)
												<option value="{{ $val->id }}">{{ $val->text }}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-md-2 pull-right" for="jenis_work">Jenis Pekerjaan</label>
									<div class="col-md-5">
										<select name="jenis_work" class="form-control input-transparent" id="jenis_work" required>
										</select>
									</div>
									<div class="col-md-5">
										<select name="jenis_kontrak" class="form-control input-transparent" id="jenis_kontrak" required>
											<option value="Kontrak Putus">Kontrak Putus</option>
											<option value="Kontrak Tetap">Kontrak Tetap</option>
										</select>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-md-2 pull-right" for="mitra_id">Mitra</label>
									<div class="col-md-10">
										<select id="mitra_id" name="mitra_id" required></select>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-md-2 pull-right" for="judul">Judul</label>
									<div class="col-md-10">
										<input type="text" class="form-control input-transparent" name="judul" id="judul" required>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-md-2 pull-right">Pengerjaan</label>
									<div class="col-md-5">
										<select id="tahun" name="tahun" required>
											@for ($i = date('Y'); $i >= date('Y', strtotime('-1 year') ); $i--)
												<option value="{{ $i }}">{{ $i }}</option>
											@endfor
										</select>
									</div>
									<div class="col-md-5">
										<select id="bulan" name="bulan" required>
											<option value="01">Januari</option>
											<option value="02">Februari</option>
											<option value="03">Maret</option>
											<option value="04">April</option>
											<option value="05">Mei</option>
											<option value="06">Juni</option>
											<option value="07">Juli</option>
											<option value="08">Agustus</option>
											<option value="09">September</option>
											<option value="10">Oktober</option>
											<option value="11">November</option>
											<option value="12">Desember</option>
										</select>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-md-2 pull-right" for="material_ta">Material TA</label>
									<div class="col-md-10">
										<select id="material_ta" name="material_ta" data-design_id="16" class="ini_material" multiple></select>
										<code><b>*Material sesuai yang dipakai di RFC</b></code>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-md-2 pull-right" for="material_mitra">Material Mitra</label>
									<div class="col-md-10">
										<select id="material_mitra" name="material_mitra" data-design_id="0" class="ini_material" multiple></select>
										<code><b>*Material sesuai yang dipakai dan tidak tertera di RFC</b></code><br/>
									</div>
								</div>
								<div class="row">
									<div class="col-md-4">
										<div class="form-group">
											<label class="col-form-label" for="sto">Total Material:</label>
											<input type="text" class="form-control" disabled id="total_mat">
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label class="col-form-label" for="sto">Total Jasa:</label>
											<input type="text" class="form-control" disabled id="total_jasa">
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label class="col-form-label" for="sto">Grand Total:</label>
											<input type="text" class="form-control" disabled id="total_grand">
										</div>
									</div>
								</div>
								<input type="hidden" name="material_input" value="{{ Request::old('material_input') }}">
								<div class="form-group mb-3 row">
									<div class="custom-file col-md-12">
										<button type="submit" data-jenis_btn="submit" class="btn btn-block submit_btn btn-success" style="color: white"><i class="fe fe-check fe-16"></i>&nbsp;submit</button>
									</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
    <div class="template_boq row" style="display: flex; flex: 0 0 100%; max-width: 100%"></div>
	</div>
</div>
@endsection
@section('footerS')
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src='/js/jquery.dataTables.min.js'></script>
<script src='/js/dataTables.bootstrap4.min.js'></script>
<script type="text/javascript">
	$(function(){
		$("body").tooltip({ selector: '[data-toggle=tooltip]' });

		$('#tahun, #bulan').select2({
			width: '100%'
		})

		$(document).on('keyup', '.number', function(event){
			if(event.which >= 37 && event.which <= 40) return;
			$(this).val(function(index, value) {
        return value.replace(/\D/g, "");
      });
		})

		$(document).on('click', '.toggleCard', function(){
			$(this).html((i, t) => t === '▲' ? '▼' : '▲');
			$(this).parent().next('.card-body').slideToggle();
		})

		$('select[id="mitra_id"]').select2({
			width: '100%',
			allowClear: true,
		});

		$('#pekerjaan').val(null).change();

		$('#pekerjaan').select2({
			width: '100%',
		});

		$('#jumlah_klm').select2({
			width: '100%',
		});

		$('#jenis_kontrak').select2({
			width: '100%',
		});

		var design = {!! json_encode($design) !!}

		$('#pekerjaan').on('change', function(){
			var jenis_kerja = {!! json_encode($kerjaan[1]) !!},
			kerja = $(this).val(),
			cari = [];
			$.each(jenis_kerja, function(key, val) {
				if(val.pekerjaan == kerja){
					cari.push({id: val.id, text: val.text})
				}
			});

			$('#jenis_work').select2({
				width: '100%',
				data: cari
			})
		})

		$('#tb_rekon').DataTable({
			autoWidth: true,
			lengthMenu: [
				[16, 32, 64, -1],
				[16, 32, 64, "All"]
			]
		});

		function terbilang(nilai)
    {
			var hasil;
			if (nilai < 0){
				hasil = "minus " + penyebut(nilai);
			}else{
				hasil = penyebut(nilai);
			}

			return hasil;
    }

		var data_pbu = {!! json_encode($data_pbu) !!},
		jenis_work = data_pbu.jenis_work,
		sub_unit;

    function penyebut(nilai)
    {
			var nilai = Math.abs(Math.floor(nilai)),
			huruf = ["", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas"],
			temp;
			if(nilai < 12){
				temp = " " + huruf[nilai];
			}else if(nilai < 20){
				temp = penyebut(nilai - 10) + " belas";
			}else if(nilai < 100){
				temp = penyebut(nilai / 10) + " puluh" + penyebut(nilai % 10);
			}
			return temp;
    }

		var load_material = [];
		var last_used_design = {!! json_encode($data_item) !!};

		last_stp_id = {!! json_encode($step) !!};

		$(document).on('click', '.copy_detail_tool', function(){
			var data = $(this).data('original-title');
			var $temp = $("<input>");
			$("body").append($temp);
			$temp.val(data).select();
			document.execCommand("copy");
			$temp.remove();

			toastr.options = {
				"debug": false,
				"newestOnTop": false,
				"progressBar": true,
				"positionClass": "toast-top-center",
				"preventDuplicates": false,
				"onclick": null,
				"showDuration": "300",
				"hideDuration": "1000",
				"timeOut": "3000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut"
			}
			toastr["success"]("Designator Berhasil Disalin!", "Sukses")
		});

		function add_material(mm){
			var td_material = '';
			sukses_filter = [];

			$.each(mm, function(k, v){
				var value = $(this).val(),
				jenis_khs = parseInt($(this).data('design_id'));
				var isi_load = value;
				var material = [];

				if(isi_load.length){
					var filter_lm = [];
					$.each(load_material, function(k, vx){
						if(vx.design_mitra_id == jenis_khs){
							filter_lm.push(vx);
							sukses_filter.push(vx);
						}
					})

					var diff = filter_lm.filter(o1 => !isi_load.some(o2 => o1.id === parseInt(o2) ) );

					if(diff){
						$.each(diff, function(key, val){
							$("tr[data-id_khs='" + val.id +"_"+val.design_mitra_id+"']").remove();
						});
					}
					//ini kalau ada materialnya
					var intersection = filter_lm.filter(o1 => isi_load.some(o2 => o1.id === parseInt(o2) ) );
					var remove_dup = isi_load.filter(o1 => !intersection.some(o2 => parseInt(o1) === o2.id) );
					material = remove_dup;
				}

				$.each(material, function(key2, val2) {
					$.each(design, function(key, val) {
						if(val.id == val2){
							var loop_me = true;
							if(last_used_design.length != 0){
								var tbl_m = $(".table_material").find('tbody');
								$.each(last_used_design, function(kx, vx){
									$.each(vx, function(kk, vv ){
										if(vv.id_design == val2){
											var total_material_sp = 0,
											total_jasa_sp = 0,
											total_material_rekon = 0,
											total_jasa_rekon = 0;

											td_material += "<tr data-id='"+vv.id_design+"' data-id_khs='"+vv.id_design+"_"+jenis_khs+"'>";
											td_material += "<td>"+val.designator+"</td>";

											var word = val.uraian.split(' '),
											text = '';

											if(word.length > 6){
												text = "...<a style='color:blue; cursor:pointer' data-toggle='tooltip' class='copy_detail_tool' title='"+val.uraian+"'>Lihat Detail</a>";
											}

											td_material += "<td>"+word.slice(0, 6).join(' ') + text +"</td>";
											td_material += "<td>"+(jenis_khs == 0 ? 'Mitra' : 'Telkom Akses')+"</td>";
											td_material += "<td>"+val.material.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".")+"</td>";
											td_material += "<td>"+val.jasa.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".")+"</td>";

											var spB = 0;

											if(vv.material == val.material && vv.jasa == val.jasa){
												spB = (vv.sp || 0);
											}

											total_material_sp += (val.material * spB );
											total_jasa_sp += (val.jasa * spB );

											td_material += "<td><input type='text' style='width:80px;' class='form-control number input-transparent espe_val field_material_input' readonly data-val2='"+ (vv.rekon || 0) + "' data-id_mat='"+val.id_design+"' data-material='"+val.material+"' data-jasa='"+val.jasa+"' data-design_mitra_id="+jenis_khs+" placeholder='0' value='"+ (spB != 0 ? spB : '') +"'></td>";

											td_material += "</tr>";

											if($(".table_material").length){
												$(".table_material").append(td_material)
												td_material = '';
											}

											loop_me = false;
										}
									});

									if($(".table_material").length){
										tbl_m.eq(0).append(td_material)
										tbl_m = tbl_m.slice(1);
										td_material = '';
									}
								});
							}

							if(loop_me){
								td_material += "<tr data-id="+val.id+" data-id_khs='"+val.id+"_"+jenis_khs+"'>";
								td_material += "<td>"+val.designator+"</td>";

								var word = val.uraian.split(' '),
								text = '';

								if(word.length > 6){
									text = "...<a style='color:blue; cursor:pointer' data-toggle='tooltip' class='copy_detail_tool' title='"+val.uraian+"'>Lihat Detail</a>";
								}

								td_material += "<td>"+word.slice(0, 6).join(' ') + text +"</td>";
								td_material += "<td>"+(jenis_khs == 0 ? 'Mitra' : 'Telkom Akses')+"</td>";
								td_material += "<td>"+val.material.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".")+"</td>";
								td_material += "<td>"+val.jasa.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".")+"</td>";
								td_material += "<td><input type='text' style='width:80px;' required class='form-control number input-transparent espe_val field_material_input' required data-id_mat="+val.id+" data-material="+val.material+" placeholder='0' data-jasa="+val.jasa+" data-design_mitra_id="+jenis_khs+" value=0></td>";

								td_material += "</tr>";

								if($(".table_material").length){
									// var data = [];
									// $(".table_material tr").each(function(key, val){
									// 	data[key] = [];
									// 	$(this).children('td').each(function(key2, val2){
									// 		data[key][key2] = $(this).text();
									// 	});
									// });
									$(".table_material").find('tbody').append(td_material)
									td_material = '';
								}
							}
						}
					});
				});
			});

			var diff = load_material.filter(o1 => !sukses_filter.some(o2 => o1.id + '_' + o1.design_mitra_id === o2.id + '_' + o2.design_mitra_id ) );

			if(diff){
				$.each(diff, function(key, val){
					$("tr[data-id_khs='" + val.id +"_"+val.design_mitra_id+"']").remove();
				});
			}

			load_material = [];

			$.each(mm, function(k, v){
				var value = $(this).val(),
				jenis_khs = parseInt($(this).data('design_id'));
				var isi_load = value;

				$.each(isi_load, function(key2, val2) {
					$.each(design, function(key, val) {
						if(val2 == val.id){
							load_material.push({
								id: val.id,
								mat: (val.material_load !== undefined ? val.material_load : val.material),
								jasa: (val.jasa_load !== undefined ? val.jasa_load : val.jasa),
								design_mitra_id: jenis_khs
							});
						}
					});
				});

				$.each(last_used_design[0], function(k, v){
					$.each(load_material, function(kk, vv){
						if(vv.id == v.id_design && vv.design_mitra_id == v.design_mitra_id){
							load_material[kk].mat = v.material;
							load_material[kk].jasa = v.jasa;
						}
					});
				});
			});

			var material_tot_chck = 0,
			jasa_tot_chck = 0;

			$('.field_material_input').each(function(){
				var tfoot = $(this).parent().parent().parent().next(),
				body = $(this).parent().parent().parent();
				// console.log(tfoot, body);
				var material_in = 0,
				jasa_in = 0,
				total_sp = 0;

				body.each(function(){
					$(this).find('.field_material_input').each(function(){
						var id_material = parseInt($(this).data('id_mat') ),
						material = parseInt($(this).attr('data-material') ) || 0,
						jasa = parseInt($(this).attr('data-jasa') ) || 0,
						urutan = parseInt($(this).parent().parent().parent().data('nomor') ),
						isi_material = parseInt($(this).val() ) || 0;

						material_in += material * isi_material;
						jasa_in += jasa * isi_material;

						if(last_stp_id >= 7){
							espe_val = parseInt($(this).attr('data-val2') )|| 0,
							rekon_val = parseInt($(this).val() )|| 0;
						}else{
							rekon_val = parseInt($(this).attr('data-val2') )|| 0,
							espe_val = parseInt($(this).val() )|| 0;
						}

						total_sp += (material * espe_val) + (jasa * espe_val);

						tfoot.find('.hrg_material').text(material_in.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
						tfoot.find('.hrg_jasa').text(jasa_in.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
						tfoot.find('.total_sp').text(total_sp.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
					})
				});

				var id_material = parseInt($(this).data('id_mat') ),
				material = parseInt($(this).attr('data-material') ) || 0,
				jasa = parseInt($(this).attr('data-jasa') ) || 0,
				urutan = parseInt($(this).parent().parent().parent().data('nomor') ),
				isi = parseInt($(this).val() ) || 0;
				material_tot_chck += material * isi;
				jasa_tot_chck += jasa * isi;
			});

			if($('.field_material_input').length == 0 ){
				$('.hrg_material').text(0);
				$('.hrg_jasa').text(0);
				$('.total_sp').text(0);
			}

			$('#total_mat').val(material_tot_chck.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
			$('#total_jasa').val(jasa_tot_chck.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
			$('#total_grand').val((material_tot_chck + jasa_tot_chck).toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
		}

		var no_tbl = 0,
		sto_i = {!! json_encode($all_sto) !!};

		if(last_used_design.length != 0){
			table_temp = ''
			var tbl_load = 0,
			no_count = 0;
			var load_material_ada = {!! json_encode($data_material) !!}

			$.each(load_material_ada, function(k1, v1){
				var val = $.grep(design, function(e){ return e.id == v1.id_design; });

				if(v1.design_mitra_id == 16){
					$("#material_ta").append('<option data-id="'+ v1.id_design +'" data-id_mitra="' +v1.design_mitra_id+ '" value="'+ v1.id_design +'" selected>'+  val[0].designator +' (' + v1.jenis + ' | ' + v1.namcomp + ')</option>').change();
				}else{
					$("#material_mitra").append('<option data-id="'+ v1.id_design +'" data-id_mitra="' +v1.design_mitra_id+ '" value="'+ v1.id_design +'" selected>'+  val[0].designator +' (' + v1.jenis + ' | ' + v1.namcomp + ')</option>').change();
				}
			});

			$.each(last_used_design, function(k, v){
				tbl_load = ++k;
				table_temp += "<div class='col-md-12'>";
				table_temp += "<div class='card shadow mb-4'>";
				table_temp += "<div class='card-header'>";
				table_temp += "<strong class='card-title nomor_kol'>Kolom Nomor "+ terbilang(tbl_load) +"</strong>";
				table_temp += "<a class='float-left toggleCard minimize' href='#!' style='color:#17a2b8; text-decoration:none;'>▲</a>";
				table_temp += "</div>";
				table_temp += "<div class='card-body table-responsive'>";
				table_temp += "</select>&nbsp;";
				table_temp += "<input type='hidden' class='form-control input-transparent new_boq_hid' name='new_boq[]' value='"+v[0].new_boq+"'>&nbsp;";
				table_temp += "<table class='table table_material table-striped table-bordered table-hover' style='width:100%'>";
				table_temp += "<thead class='thead-dark'>";
				table_temp += "<tr>";
				table_temp += "<th rowspan='2'>Designator</th>";
				table_temp += "<th rowspan='2'>Uraian</th>";
				table_temp += "<th rowspan='2'>Jenis Material</th>";
				table_temp += "<th colspan='2'>Paket 7</th>";
				table_temp += "<th rowspan='2'>SP</th>";
				table_temp += "<th rowspan='2'>HSS TA</th>";
				table_temp += "</tr>";
				table_temp += "<tr>";
				table_temp += "<th>Material</th>";
				table_temp += "<th>Jasa</th>";
				table_temp += "</tr>";
				table_temp += "</thead>";
				table_temp += "<tbody data-nomor='"+tbl_load+"'>";

				var total_material = 0,
				total_jasa = 0,
				total_sp = 0;

				v.sort(function(a, b) {
					return a.designator > b.designator
				});

				$.each(v, function(k1, v1){
					var val = $.grep(design, function(e){ return e.id == v1.id_design; });

					if(val.length != 0){
						val[0].material_load = v1.material || 0;
						val[0].jasa_load = v1.jasa || 0;

						table_temp += "<tr data-id='"+v1.id_design+"' data-id_khs='"+v1.id_design+"_"+v1.design_mitra_id+"'>";
						table_temp += "<td>"+val[0].designator+"</td>";
						var word = val[0].uraian.split(' '),
						text = '';

						if(word.length > 6){
							text = "...<a style='color:blue; cursor:pointer' data-toggle='tooltip' class='copy_detail_tool' title='"+val[0].uraian+"'>Lihat Detail</a>";
						}

						table_temp += "<td>"+word.slice(0, 6).join(' ') + text +"</td>";
						table_temp += "<td>"+v1.namcomp+"</td>";
						table_temp += "<td>"+(v1.material || 0).toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".")+"</td>";

						total_material += (v1.material * v1.sp);
						total_jasa += (v1.jasa * v1.sp);

						total_sp += (v1.material * v1.sp) + (v1.jasa * v1.sp);

						table_temp += "<td>"+(v1.jasa || 0).toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".")+"</td>";

						table_temp += "<td><input type='text' style='width:80px;' class='form-control number input-transparent espe_val field_material_input' required data-id_mat='"+v1.id_design+"' data-material='"+v1.material+"' placeholder='0' data-jasa='"+v1.jasa+"' data-val2='"+(v1.rekon || 0) +"' readonly data-design_mitra_id="+v1.design_mitra_id+" value='"+ (v1.sp != 0 ? v1.sp : '') +"'></td>";
						table_temp += "<td><input type='checkbox' class='check_khs' data-material='"+v1.material+"' data-jasa='"+v1.jasa+"' data-id_mat="+v1.id_design+" " + (v1.design_mitra_id == 16 ? 'checked' : '' ) + "></td>";

						table_temp += "</tr>";
					}
				})

				table_temp += "</tbody>";
				table_temp += "<tfoot>";
				table_temp += "<tr data-nomor_foot='"+tbl_load+"'>";
				table_temp += "<td colspan='3'>Total Harga</td>";
				table_temp += "<td class='hrg_material'>" + total_material.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") + "</td>";
				table_temp += "<td class='hrg_jasa'>" + total_jasa.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") + "</td>";
				table_temp += "<td colspan='2' style='text-align:center' class='total_sp'>" + total_sp.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") + "</td>";
				table_temp += "</tr>";
				table_temp += "</tfoot>";
				table_temp += "</table>";
				table_temp += "</div>";
				table_temp += "</div>";
				table_temp += "</div>";
			})

			no_tbl += tbl_load;
			add_material($('.ini_material') );

      $('.template_boq').append(table_temp);

			$('.check_khs').on('click change', function(e){
				var id_mat = $(this).data('id_mat'),
				material = $(this).data('material'),
				jasa = $(this).data('jasa'),
				chck = 'nok',
				vali = [];

				var isi_data = `${id_mat}_${chck}_${material}_${jasa}`;

				if($(this).is(':checked') ){
					chck = 'ok';
				}

				$.each($('.check_khs'), function(k, v){
					var id_mat = $(this).data('id_mat'),
					material = $(this).data('material'),
					jasa = $(this).data('jasa'),
					chck = 'nok';

					var isi_data = `${id_mat}_${chck}_${material}_${jasa}`;

					if($(this).is(':checked') ){
						chck = 'ok';
					}
					vali.push(isi_data)
				})

				var re = $.map(vali, function(v, i) {
					if(v == isi_data) return 1;
				});

				var sum = re.reduce( (a, b) => a + b, 0);

				if(sum > 1){
					e.preventDefault();
					e.stopPropagation();
				}
			});

			var material_tot = 0,
			jasa_tot = 0;

			$('.field_material_input').each(function(){
				var id_material = parseInt($(this).data('id_mat') ),
				material = parseInt($(this).attr('data-material') ) || 0,
				jasa = parseInt($(this).attr('data-jasa') ) || 0,
				urutan = parseInt($(this).parent().parent().parent().data('nomor') ),
				isi = parseInt($(this).val() || 0 );
				material_tot += material * isi;
				jasa_tot += jasa * isi;
			});

			$('#total_mat').val(material_tot.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
			$('#total_jasa').val(jasa_tot.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
			$('#total_grand').val((material_tot + jasa_tot).toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
		}

		pekerjaan = data_pbu.pekerjaan,
		jenis_kontrak = data_pbu.jenis_kontrak,
		mitra_id = data_pbu.mitra_id,
		mitra_nm = data_pbu.mitra_nm,
		judul = data_pbu.judul;
		tahun_kerja = data_pbu.bulan_pengerjaan.split('-');

		$('#pekerjaan').val(pekerjaan).change();
		$('#jenis_work').val(jenis_work).trigger('change');
		$('#mitra_id').append(new Option(mitra_nm, mitra_id, true, true) ).trigger('change');

		$('#judul').val(judul).change();
		$('#jenis_kontrak').val(jenis_kontrak).change();
		$('#tahun').val(tahun_kerja[0]).change();
		$('#bulan').val(tahun_kerja[1]).change();

		$('.submit_btn').on('click', function(e){
			var submit = [],
			final_ls = {},
			ls = [];

			$('.field_material_input').each(function(){
				var id_material = parseInt($(this).data('id_mat') ),
				material = parseInt($(this).attr('data-material') ) || 0,
				jasa = parseInt($(this).attr('data-jasa') ) || 0,
				urutan = parseInt($(this).parent().parent().parent().data('nomor') ),
				checkbox_isi = $(this).parent().next().children().eq(0).is(":checked"),
				jenis_khs = parseInt($(this).attr('data-design_mitra_id') ),
				new_boq = $(this).parent().parent().parent().parent().parent().find('.new_boq_hid').val();

				rekon_val = parseInt($(this).attr('data-val2') ) || 0,
				espe_val = parseInt($(this).val() ) || 0;

				var get_design = design.find(o => o.id == id_material);

				submit.push({
					id: id_material,
					material: material,
					jasa: jasa,
					sp: espe_val,
					rekon: rekon_val,
					urutan: urutan,
					jenis_khs: jenis_khs,
					new_boq: new_boq,
					id_design: id_material,
					namcomp: (jenis_khs == 16 ? 'Telkom Akses' : 'Mitra'),
					checkbox_isi: checkbox_isi,
					tambah: 0,
					kurang: 0,
					design_mitra_id: jenis_khs,
					jenis: get_design.jenis,
				});
			});
			let hasil = JSON.stringify(submit)
			$("input[name='material_input']").val(hasil)
		})

		$('#pekerjaan, #jenis_work, #mitra_id, #judul, #jenis_kontrak, #tahun, #bulan').prop( "disabled", true );

		$('#material_ta').select2({
			width: '100%',
			allowClear: true,
			disabled: true,
		});

		$('#material_mitra').select2({
			width: '100%',
			allowClear: true,
			disabled: true,
		});
  });
</script>
@endsection