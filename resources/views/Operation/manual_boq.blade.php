@extends('layout')
@section('title', 'Request PID Pekerjaan Baru')
@section('headerS')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" rel="stylesheet" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.css" integrity="sha512-6S2HWzVFxruDlZxI3sXOZZ4/eJ8AcxkQH1+JjSe/ONCEqR9L4Ysq5JdT5ipqtzU7WHalNwzwBv+iE51gNHJNqQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<style type="text/css">
	.pull-right {
		text-align: right;
	}

  .toggleCard{
		display: block;
	}
  .card-body{
		display: block;
	}

	.field_material_input{
		width: 60px!important;
		height: 32px;
	}

	@media (min-width: 768px) {
		.modal-xl {
			width: 100%;
			max-width:1200px;
		}
	}

	.opac {
    opacity: 25%;
  }
	/* ini css======================================= */
	.cssload-loader {
		position: absolute;
		left: 50%;
		width: 47.284271247462px;
		height: 47.284271247462px;
		margin-left: -23.142135623731px;
		margin-top: -23.142135623731px;
		border-radius: 100%;
		animation-name: cssload-loader;
			-o-animation-name: cssload-loader;
			-ms-animation-name: cssload-loader;
			-webkit-animation-name: cssload-loader;
			-moz-animation-name: cssload-loader;
		animation-iteration-count: infinite;
			-o-animation-iteration-count: infinite;
			-ms-animation-iteration-count: infinite;
			-webkit-animation-iteration-count: infinite;
			-moz-animation-iteration-count: infinite;
		animation-timing-function: linear;
			-o-animation-timing-function: linear;
			-ms-animation-timing-function: linear;
			-webkit-animation-timing-function: linear;
			-moz-animation-timing-function: linear;
		animation-duration: 4.6s;
			-o-animation-duration: 4.6s;
			-ms-animation-duration: 4.6s;
			-webkit-animation-duration: 4.6s;
			-moz-animation-duration: 4.6s;
	}
	.cssload-loader .cssload-side {
		display: block;
		width: 6px;
		height: 19px;
		background-color: rgb(27,103,255);
		margin: 2px;
		position: absolute;
		border-radius: 50%;
		animation-duration: 1.73s;
			-o-animation-duration: 1.73s;
			-ms-animation-duration: 1.73s;
			-webkit-animation-duration: 1.73s;
			-moz-animation-duration: 1.73s;
		animation-iteration-count: infinite;
			-o-animation-iteration-count: infinite;
			-ms-animation-iteration-count: infinite;
			-webkit-animation-iteration-count: infinite;
			-moz-animation-iteration-count: infinite;
		animation-timing-function: ease;
			-o-animation-timing-function: ease;
			-ms-animation-timing-function: ease;
			-webkit-animation-timing-function: ease;
			-moz-animation-timing-function: ease;
	}
	.cssload-loader .cssload-side:nth-child(1),
	.cssload-loader .cssload-side:nth-child(5) {
		transform: rotate(0deg);
			-o-transform: rotate(0deg);
			-ms-transform: rotate(0deg);
			-webkit-transform: rotate(0deg);
			-moz-transform: rotate(0deg);
		animation-name: cssload-rotate0;
			-o-animation-name: cssload-rotate0;
			-ms-animation-name: cssload-rotate0;
			-webkit-animation-name: cssload-rotate0;
			-moz-animation-name: cssload-rotate0;
	}
	.cssload-loader .cssload-side:nth-child(3),
	.cssload-loader .cssload-side:nth-child(7) {
		transform: rotate(90deg);
			-o-transform: rotate(90deg);
			-ms-transform: rotate(90deg);
			-webkit-transform: rotate(90deg);
			-moz-transform: rotate(90deg);
		animation-name: cssload-rotate90;
			-o-animation-name: cssload-rotate90;
			-ms-animation-name: cssload-rotate90;
			-webkit-animation-name: cssload-rotate90;
			-moz-animation-name: cssload-rotate90;
	}
	.cssload-loader .cssload-side:nth-child(2),
	.cssload-loader .cssload-side:nth-child(6) {
		transform: rotate(45deg);
			-o-transform: rotate(45deg);
			-ms-transform: rotate(45deg);
			-webkit-transform: rotate(45deg);
			-moz-transform: rotate(45deg);
		animation-name: cssload-rotate45;
			-o-animation-name: cssload-rotate45;
			-ms-animation-name: cssload-rotate45;
			-webkit-animation-name: cssload-rotate45;
			-moz-animation-name: cssload-rotate45;
	}
	.cssload-loader .cssload-side:nth-child(4),
	.cssload-loader .cssload-side:nth-child(8) {
		transform: rotate(135deg);
			-o-transform: rotate(135deg);
			-ms-transform: rotate(135deg);
			-webkit-transform: rotate(135deg);
			-moz-transform: rotate(135deg);
		animation-name: cssload-rotate135;
			-o-animation-name: cssload-rotate135;
			-ms-animation-name: cssload-rotate135;
			-webkit-animation-name: cssload-rotate135;
			-moz-animation-name: cssload-rotate135;
	}
	.cssload-loader .cssload-side:nth-child(1) {
		top: 23.142135623731px;
		left: 47.284271247462px;
		margin-left: -3px;
		margin-top: -10px;
		animation-delay: 0;
			-o-animation-delay: 0;
			-ms-animation-delay: 0;
			-webkit-animation-delay: 0;
			-moz-animation-delay: 0;
	}
	.cssload-loader .cssload-side:nth-child(2) {
		top: 40.213203431093px;
		left: 40.213203431093px;
		margin-left: -3px;
		margin-top: -10px;
		animation-delay: 0;
			-o-animation-delay: 0;
			-ms-animation-delay: 0;
			-webkit-animation-delay: 0;
			-moz-animation-delay: 0;
	}
	.cssload-loader .cssload-side:nth-child(3) {
		top: 47.284271247462px;
		left: 23.142135623731px;
		margin-left: -3px;
		margin-top: -10px;
		animation-delay: 0;
			-o-animation-delay: 0;
			-ms-animation-delay: 0;
			-webkit-animation-delay: 0;
			-moz-animation-delay: 0;
	}
	.cssload-loader .cssload-side:nth-child(4) {
		top: 40.213203431093px;
		left: 7.0710678163691px;
		margin-left: -3px;
		margin-top: -10px;
		animation-delay: 0;
			-o-animation-delay: 0;
			-ms-animation-delay: 0;
			-webkit-animation-delay: 0;
			-moz-animation-delay: 0;
	}
	.cssload-loader .cssload-side:nth-child(5) {
		top: 23.142135623731px;
		left: 0px;
		margin-left: -3px;
		margin-top: -10px;
		animation-delay: 0;
			-o-animation-delay: 0;
			-ms-animation-delay: 0;
			-webkit-animation-delay: 0;
			-moz-animation-delay: 0;
	}
	.cssload-loader .cssload-side:nth-child(6) {
		top: 7.0710678163691px;
		left: 7.0710678163691px;
		margin-left: -3px;
		margin-top: -10px;
		animation-delay: 0;
			-o-animation-delay: 0;
			-ms-animation-delay: 0;
			-webkit-animation-delay: 0;
			-moz-animation-delay: 0;
	}
	.cssload-loader .cssload-side:nth-child(7) {
		top: 0px;
		left: 23.142135623731px;
		margin-left: -3px;
		margin-top: -10px;
		animation-delay: 0;
			-o-animation-delay: 0;
			-ms-animation-delay: 0;
			-webkit-animation-delay: 0;
			-moz-animation-delay: 0;
	}
	.cssload-loader .cssload-side:nth-child(8) {
		top: 7.0710678163691px;
		left: 40.213203431093px;
		margin-left: -3px;
		margin-top: -10px;
		animation-delay: 0;
			-o-animation-delay: 0;
			-ms-animation-delay: 0;
			-webkit-animation-delay: 0;
			-moz-animation-delay: 0;
	}

	@keyframes cssload-rotate0 {
		0% {
			transform: rotate(0deg);
		}
		60% {
			transform: rotate(180deg);
		}
		100% {
			transform: rotate(180deg);
		}
	}

	@-o-keyframes cssload-rotate0 {
		0% {
			-o-transform: rotate(0deg);
		}
		60% {
			-o-transform: rotate(180deg);
		}
		100% {
			-o-transform: rotate(180deg);
		}
	}

	@-ms-keyframes cssload-rotate0 {
		0% {
			-ms-transform: rotate(0deg);
		}
		60% {
			-ms-transform: rotate(180deg);
		}
		100% {
			-ms-transform: rotate(180deg);
		}
	}

	@-webkit-keyframes cssload-rotate0 {
		0% {
			-webkit-transform: rotate(0deg);
		}
		60% {
			-webkit-transform: rotate(180deg);
		}
		100% {
			-webkit-transform: rotate(180deg);
		}
	}

	@-moz-keyframes cssload-rotate0 {
		0% {
			-moz-transform: rotate(0deg);
		}
		60% {
			-moz-transform: rotate(180deg);
		}
		100% {
			-moz-transform: rotate(180deg);
		}
	}

	@keyframes cssload-rotate90 {
		0% {
			transform: rotate(90deg);
							transform: rotate(90deg);
		}
		60% {
			transform: rotate(270deg);
							transform: rotate(270deg);
		}
		100% {
			transform: rotate(270deg);
							transform: rotate(270deg);
		}
	}

	@-o-keyframes cssload-rotate90 {
		0% {
			-o-transform: rotate(90deg);
							transform: rotate(90deg);
		}
		60% {
			-o-transform: rotate(270deg);
							transform: rotate(270deg);
		}
		100% {
			-o-transform: rotate(270deg);
							transform: rotate(270deg);
		}
	}

	@-ms-keyframes cssload-rotate90 {
		0% {
			-ms-transform: rotate(90deg);
							transform: rotate(90deg);
		}
		60% {
			-ms-transform: rotate(270deg);
							transform: rotate(270deg);
		}
		100% {
			-ms-transform: rotate(270deg);
							transform: rotate(270deg);
		}
	}

	@-webkit-keyframes cssload-rotate90 {
		0% {
			-webkit-transform: rotate(90deg);
							transform: rotate(90deg);
		}
		60% {
			-webkit-transform: rotate(270deg);
							transform: rotate(270deg);
		}
		100% {
			-webkit-transform: rotate(270deg);
							transform: rotate(270deg);
		}
	}

	@-moz-keyframes cssload-rotate90 {
		0% {
			-moz-transform: rotate(90deg);
							transform: rotate(90deg);
		}
		60% {
			-moz-transform: rotate(270deg);
							transform: rotate(270deg);
		}
		100% {
			-moz-transform: rotate(270deg);
							transform: rotate(270deg);
		}
	}

	@keyframes cssload-rotate45 {
		0% {
			transform: rotate(45deg);
							transform: rotate(45deg);
		}
		60% {
			transform: rotate(225deg);
							transform: rotate(225deg);
		}
		100% {
			transform: rotate(225deg);
							transform: rotate(225deg);
		}
	}

	@-o-keyframes cssload-rotate45 {
		0% {
			-o-transform: rotate(45deg);
							transform: rotate(45deg);
		}
		60% {
			-o-transform: rotate(225deg);
							transform: rotate(225deg);
		}
		100% {
			-o-transform: rotate(225deg);
							transform: rotate(225deg);
		}
	}

	@-ms-keyframes cssload-rotate45 {
		0% {
			-ms-transform: rotate(45deg);
							transform: rotate(45deg);
		}
		60% {
			-ms-transform: rotate(225deg);
							transform: rotate(225deg);
		}
		100% {
			-ms-transform: rotate(225deg);
							transform: rotate(225deg);
		}
	}

	@-webkit-keyframes cssload-rotate45 {
		0% {
			-webkit-transform: rotate(45deg);
							transform: rotate(45deg);
		}
		60% {
			-webkit-transform: rotate(225deg);
							transform: rotate(225deg);
		}
		100% {
			-webkit-transform: rotate(225deg);
							transform: rotate(225deg);
		}
	}

	@-moz-keyframes cssload-rotate45 {
		0% {
			-moz-transform: rotate(45deg);
			transform: rotate(45deg);
		}
		60% {
			-moz-transform: rotate(225deg);
			transform: rotate(225deg);
		}
		100% {
			-moz-transform: rotate(225deg);
			transform: rotate(225deg);
		}
	}

	@keyframes cssload-rotate135 {
		0% {
			transform: rotate(135deg);
			transform: rotate(135deg);
		}
		60% {
			transform: rotate(315deg);
			transform: rotate(315deg);
		}
		100% {
			transform: rotate(315deg);
			transform: rotate(315deg);
		}
	}

	@-o-keyframes cssload-rotate135 {
		0% {
			-o-transform: rotate(135deg);
			transform: rotate(135deg);
		}
		60% {
			-o-transform: rotate(315deg);
			transform: rotate(315deg);
		}
		100% {
			-o-transform: rotate(315deg);
			transform: rotate(315deg);
		}
	}

	@-ms-keyframes cssload-rotate135 {
		0% {
			-ms-transform: rotate(135deg);
							transform: rotate(135deg);
		}
		60% {
			-ms-transform: rotate(315deg);
							transform: rotate(315deg);
		}
		100% {
			-ms-transform: rotate(315deg);
							transform: rotate(315deg);
		}
	}

	@-webkit-keyframes cssload-rotate135 {
		0% {
			-webkit-transform: rotate(135deg);
							transform: rotate(135deg);
		}
		60% {
			-webkit-transform: rotate(315deg);
							transform: rotate(315deg);
		}
		100% {
			-webkit-transform: rotate(315deg);
							transform: rotate(315deg);
		}
	}

	@-moz-keyframes cssload-rotate135 {
		0% {
			-moz-transform: rotate(135deg);
							transform: rotate(135deg);
		}
		60% {
			-moz-transform: rotate(315deg);
							transform: rotate(315deg);
		}
		100% {
			-moz-transform: rotate(315deg);
							transform: rotate(315deg);
		}
	}

	@keyframes cssload-loader {
		0% {
			transform: rotate(0deg);
							transform: rotate(0deg);
		}
		100% {
			transform: rotate(360deg);
							transform: rotate(360deg);
		}
	}

	@-o-keyframes cssload-loader {
		0% {
			-o-transform: rotate(0deg);
							transform: rotate(0deg);
		}
		100% {
			-o-transform: rotate(360deg);
							transform: rotate(360deg);
		}
	}

	@-ms-keyframes cssload-loader {
		0% {
			-ms-transform: rotate(0deg);
							transform: rotate(0deg);
		}
		100% {
			-ms-transform: rotate(360deg);
							transform: rotate(360deg);
		}
	}

	@-webkit-keyframes cssload-loader {
		0% {
			-webkit-transform: rotate(0deg);
							transform: rotate(0deg);
		}
		100% {
			-webkit-transform: rotate(360deg);
							transform: rotate(360deg);
		}
	}

	@-moz-keyframes cssload-loader {
		0% {
			-moz-transform: rotate(0deg);
							transform: rotate(0deg);
		}
		100% {
			-moz-transform: rotate(360deg);
							transform: rotate(360deg);
		}
	}
</style>
@endsection
@section('content')
@if (Session::has('alerts'))
	@foreach(Session::get('alerts') as $alert)
		<div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
	@endforeach
@endif
<div class="modal fade" id="modal_edit_material" tabindex="-1" role="dialog" aria-labelledby="modal_edit_materialTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
				<h5 class="modal-title" id="defaultModalLabel">List Material Yang Dipilih</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body data_mdl" style=" overflow: hidden;">
			</div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-md-12">
			<h2 class="page-title">Request PID Pekerjaan Baru</h2>
			<div class="card-deck" style="display: block">
				<form id="submit_lampiran_boq" class="row" method="post">
					{{ csrf_field() }}
					<div class="col-md-12">
						<div class="card shadow mb-4">
							<div class="card-header">
								<strong class="card-title">Tambah Pekerjaan</strong>
							</div>
							<div class="card-body">
								<div class="form-group row">
									<label class="col-form-label col-md-2 pull-right" for="pekerjaan">Pekerjaan</label>
									<div class="col-md-3">
										<select name="pekerjaan" class="form-control input-transparent generate_title" id="pekerjaan" required>
											@foreach ($kerjaan[0] as $val)
												<option value="{{ $val->id }}">{{ $val->text }}</option>
											@endforeach
										</select>
									</div>
									<div class="col-md-4">
										<select name="jenis_work" class="form-control input-transparent generate_title" id="jenis_work" required>
										</select>
									</div>
									<div class="col-md-3">
										<select name="jenis_kontrak" class="form-control input-transparent" id="jenis_kontrak" required>
											<option value="Kontrak Putus">Kontrak Putus</option>
											<option value="Kontrak Tetap" selected>Kontrak Tetap</option>
										</select>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-md-2 pull-right" for="mitra_id">Mitra</label>
									<div class="col-md-10">
										<select id="mitra_id" class="form-control input-transparent generate_title" name="mitra_id" required></select>
									</div>
								</div>
								{{-- <div class="form-group row">
									<label class="col-form-label col-md-2 pull-right" for="lokasi_pekerjaan">Lokasi Pekerjaan</label>
									<div class="col-md-5">
										<input type="text" class="form-control input-transparent" id="lokasi_pekerjaan">
										<code><a style="cursor: pointer" class="aply_all">Gunakan Ke semua Lokasi</a></code>
									</div>
									<div class="col-md-5">
										<select id="sto_obj" multiple>
											@foreach ($all_sto as $v)
												<option value="{{ $v->kode_area }}">{{ $v->kode_area }}</option>
											@endforeach
										</select>
									</div>
								</div> --}}
								<div class="form-group row">
									<label class="col-form-label col-md-2 pull-right">Pengerjaan</label>
									<div class="col-md-5">
										<select id="tahun" class="form-control input-transparent generate_title" name="tahun" required>
											@for ($i = date('Y'); $i >= date('Y', strtotime('-1 year') ); $i--)
												<option value="{{ $i }}">{{ $i }}</option>
											@endfor
										</select>
									</div>
									<div class="col-md-5">
										<select id="bulan" class="form-control input-transparent generate_title" name="bulan" required>
											<option value="01">Januari</option>
											<option value="02">Februari</option>
											<option value="03">Maret</option>
											<option value="04">April</option>
											<option value="05">Mei</option>
											<option value="06">Juni</option>
											<option value="07">Juli</option>
											<option value="08">Agustus</option>
											<option value="09">September</option>
											<option value="10">Oktober</option>
											<option value="11">November</option>
											<option value="12">Desember</option>
										</select>
									</div>
								</div>
								<div class="form-group row toc_cons" style="display: none">
									<label class="col-form-label col-md-2 pull-right" for="batch">Batch</label>
									<div class="col-md-10">
										<select id="batch" name="batch" class="form-control input-transparent generate_title">
											<option value="1">1</option>
											<option value="2">2</option>
											<option value="3">3</option>
											<option value="4">4</option>
										</select>
									</div>
								</div>
								<div class="form-group row toc_cons" style="display: none">
									<label class="col-form-label col-md-2 pull-right" for="toc_edit">TOC</label>
									<div class="col-md-10">
										<input type="text" class="form-control input-transparent number" id="toc_edit" name="toc_edit">
									</div>
								</div>
								<div class="form-group row toc_cons" style="display: none">
									<label class="col-form-label col-md-2 pull-right" for="jenis_po">Jenis PO</label>
									<div class="col-md-10">
										<select id="jenis_po" name="jenis_po" class="form-control input-transparent">
											<option value="Jasa Only">Jasa Only</option>
											<option value="Material & Jasa">Material & Jasa</option>
											<option value="Turnkey">Turnkey</option>
										</select>
									</div>
								</div>
								<div class="form-group row">
									<div class="col-md-10 offset-md-2">
										<div class="custom-control custom-switch">
											<input type="checkbox" class="custom-control-input" id="use_proaktif" name="use_proaktif" value="1">
											<label class="custom-control-label" for="use_proaktif"><h5>Gunakan Judul Proaktif</h5></label>
										</div>
									</div>
								</div>
								<div class="form-group row proaktif_use" style="display: none;">
									<label class="col-form-label col-md-2 pull-right" for="proaktif_id[]">Judul Proaktif</label>
									<div class="col-md-10">
										<select id="proaktif_id" class="form-control input-transparent" name="proaktif_id[]" multiple>
											@foreach ($proaktif_all as $v)
												<option value="{{ $v->id }}">{{ $v->nama_project }}</option>
											@endforeach
										</select>
										<code><b>*Jika Pekerjaan Berasal Dari Proaktif, Silahkan Pilih Judulnya!</b></code>
									</div>
									<div class="ldg">
										<div class="cssload-side"></div>
										<div class="cssload-side"></div>
										<div class="cssload-side"></div>
										<div class="cssload-side"></div>
										<div class="cssload-side"></div>
										<div class="cssload-side"></div>
										<div class="cssload-side"></div>
										<div class="cssload-side"></div>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-md-2 pull-right" for="material_ta">Material TA</label>
									<div class="col-md-10">
										<select id="material_ta" name="material_ta" data-design_id="16" class="form-control input-transparent ini_material" multiple></select>
										<code><b>*Material sesuai yang dipakai di RFC</b></code>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-md-2 pull-right" for="material_mitra">Material Mitra</label>
									<div class="col-md-10">
										<select id="material_mitra" name="material_mitra" data-design_id="0" class="form-control input-transparent ini_material" multiple></select>
										<code><b>*Material sesuai yang dipakai dan tidak tertera di RFC</b></code><br/>
										<a type="button" class="btn add_material btn-secondary" style="margin-top: 7px; color: white"><i class="fe fe-plus fe-16"></i>&nbsp;Update material</a>
										<a type="button" class="btn edit_material btn-primary" data-toggle="modal" data-target="#modal_edit_material" style="margin-top: 7px; color: white"><i class="fe fe-tool fe-16"></i>&nbsp;Atur Material</a>
										<div id="unknown_item">
										</div>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-md-2 pull-right" for="jumlah_klm" required>Tambah Kolom</label>
									<div class="col-md-10">
										<select id="jumlah_klm" name="jumlah_klm">
											@for ($i = 1; $i <= 10; $i++)
												<option value="{{ $i }}">{{ $i }}</option>
											@endfor
										</select>
										<a type="button" class="btn add_boq btn-info" style="margin-top: 7px; color: white"><i class="fe fe-tool fe-16"></i>&nbsp;Tambah Kolom</a>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-md-2 pull-right" for="judul">Judul</label>
									<div class="col-md-10">
										<textarea type="text" class="form-control input-transparent" style="resize: none" required rows="2" name="judul" id="judul">{{ Request::old('judul') }}</textarea>
										<code class="check_jdl"></code>
									</div>
								</div>
								<div class="row">
									<div class="col-md-4">
										<div class="form-group">
											<label class="col-form-label" for="sto">Total Material:</label>
											<input type="text" class="form-control" disabled id="total_mat">
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label class="col-form-label" for="sto">Total Jasa:</label>
											<input type="text" class="form-control" disabled id="total_jasa">
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label class="col-form-label" for="sto">Grand Total:</label>
											<input type="text" class="form-control" disabled id="total_grand">
										</div>
									</div>
								</div>
								<input type="hidden" name="material_input" value="{{ Request::old('material_input') }}">
								<input type="hidden" name="jns_btn">
								<div class="form-group mb-3 row">
									<div class="custom-file col-md-6">
										<button type="submit" class="btn btn-block submit_btn btn-success" data-jenis_btn='submit' style="color: white"><i class="fe fe-check fe-16"></i>&nbsp;submit</button>
									</div>
									<div class="custom-file col-md-6">
										<button type="submit" class="btn btn-block submit_btn btn-info" data-jenis_btn='draft' style="color: white"><i class="fe fe-save fe-16"></i>&nbsp;Draft</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
			<ul class="nav nav-tabs mb-3" id="myTab" role="tablist">
			</ul>
			<div id="myTabContentLeft" class="tab-content">
			</div>
		</div>
	</div>
</div>
@endsection
@section('footerS')
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.js" integrity="sha512-lbwH47l/tPXJYG9AcFNoJaTMhGvYWhVM9YI43CT+uteTRRaiLCui8snIgyAN8XWgNjNhCqlAUdzZptso6OCoFQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script type="text/javascript">
	$(function(){
		var last_used_design = {!! json_encode($data_item) !!},
		material_before = $("input[name='material_input']").val(),
		design = {!! json_encode($design) !!},
		load_material_ada = {!! json_encode($data_material) !!};

		$(document).on('click', '.copy_detail_tool', function(){
			var data = $(this).data('original-title');
			var $temp = $("<input>");
			$("body").append($temp);
			$temp.val(data).select();
			document.execCommand("copy");
			$temp.remove();

			toastr.options = {
				"debug": false,
				"newestOnTop": false,
				"progressBar": true,
				"positionClass": "toast-top-center",
				"preventDuplicates": false,
				"onclick": null,
				"showDuration": "300",
				"hideDuration": "1000",
				"timeOut": "3000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut"
			}
			toastr["success"]("Designator Berhasil Disalin!", "Sukses")
		});

		if(material_before){
			material_before = JSON.parse(material_before)
			var final_ls = [],
			material_new = [];
			$.each(material_before, function(k, v){
				if(material_new.map(x => x.id_renew).indexOf(v.id_design +'_'+ v.design_mitra_id) === -1){
					var val = $.grep(design, function(e){ return e.id == v.id_design; })

					material_new.push({
						id_renew: v.id_design +'_'+ v.design_mitra_id,
						design_mitra_id: v.design_mitra_id,
						designator: val[0].designator,
						id_design: v.id_design,
						jasa: v.jasa,
						jenis: v.jenis,
						jenis_khs: v.jenis_khs,
						material: v.material,
						namcomp: v.namcomp,
						rekon: v.rekon,
						sp: v.sp,
						uraian: val[0].uraian,
					});
				}

				if(typeof(final_ls[v.urutan]) == 'undefined'){
					final_ls[v.urutan] = [];
				}

				final_ls[v.urutan].push(v);
			});

			load_mat = final_ls.filter(function (e) {return e != null;});
			last_used_design = load_mat

			load_mat_all = material_new.filter(function (e) {return e != null;});
			load_material_ada = load_mat_all
		}

		$('#use_proaktif').on('click', function(){
			$('.ini_material').empty().change()

			add_material($('.ini_material') );

			$('#myTab > li').remove()
			$('#myTabContentLeft > div').remove()

			$('#proaktif_id').val(null).change();
			load_prev_proaktif = [];
			diff_proaktif = [];
			tbl_load = 0;

			if(!$(this).is(':checked') ){
				$('.proaktif_use').css({
					display: 'none'
				});
			}else{
				$('.proaktif_use').css({
					display: 'flex'
				});
			}
		});

		var currentTab,
		already_open_tab = [];
		//initilize tabs
		$("#myTab").on("click", "a", function (e) {
			var href_data = $(this).attr('href');

			if(already_open_tab.indexOf(href_data.split('#')[1]) === -1){
				var get_tab_data = $.grep(load_data_html, function(e){ return e.tabId == href_data.split('#')[1]; });
				if(get_tab_data.length != 0){
					// showTab(href_data.split('#')[1]);
					registerCloseEvent();

					var key_object = (parseInt(get_tab_data[0].tabId.replace( /^\D+/g, '') ) - 1),
					data_lop = last_used_design[key_object][0],
					table_temp = '';

					table_temp += "<div class='col-md-12'>";
					table_temp += "<div class='card shadow mb-4'>";
					table_temp += "<div class='card-header'>";
					table_temp += "<strong class='card-title nomor_kol'>Kolom Nomor "+ terbilang(parseInt(get_tab_data[0].tabId.replace( /^\D+/g, '') ) ) +"</strong>";
					// table_temp += "<a class='float-left toggleCard minimize' href='#!' style='color:#17a2b8; text-decoration:none;'>▲</a><a class='float-right delete_buatan' href='#!' style='color:red; text-decoration:none;'>✖</a>";
					table_temp += "</div>";
					table_temp += "<div class='card-body table-responsive'>";
					table_temp += "<input type='text' class='form-control lok_per input-transparent' name='lokasi_pekerjaan[]' placeholder='Silahkan Isi Lokasi' required value='"+(data_lop.lokasi ?? '')+"'>&nbsp;";
					table_temp += "<select class='sto_m generate_title' name='sto[]' required>";
					$.each(sto_i, function(k, v) {
						table_temp += "<option value='"+ v.kode_area +"'>"+ v.kode_area +"</option>"
					})
					table_temp += "</select>&nbsp;";
					table_temp += "<select class='sub_jenis' name='sub_jenis[]' required>";
					table_temp += "</select>&nbsp;";
					table_temp += "<table class='table table_material table-striped table-bordered table-hover' style='width:100%'>";
					table_temp += "<thead class='thead-dark'>";
					table_temp += "<tr>";
					table_temp += "<th rowspan='2'>Designator</th>";
					table_temp += "<th rowspan='2'>Uraian</th>";
					table_temp += "<th rowspan='2'>Jenis Material</th>";
					table_temp += "<th colspan='2'>Paket 7</th>";
					table_temp += "<th rowspan='2'>SP</th>";
					table_temp += "</tr>";
					table_temp += "<tr>";
					table_temp += "<th>Material</th>";
					table_temp += "<th>Jasa</th>";
					table_temp += "</tr>";
					table_temp += "</thead>";
					var prok_d = parseInt(data_lop.proaktif_id) || 0;
					table_temp += "<tbody data-nomor='"+parseInt(get_tab_data[0].tabId.replace( /^\D+/g, '') )+"' data-proaktif_id='"+ prok_d +"'>";

					var total_material = 0,
					total_jasa = 0;

					// load_material.sort(function(a, b) {
					// 	return a.designator > b.designator
					// });

					$.each(load_material, function(k1, v1){
						var val = $.grep(design, function(e){ return e.id == v1.id; }),
						get_data_qty = $.grep(last_used_design[key_object], function(e){ return e.id_design == v1.id && e.design_mitra_id == v1.design_mitra_id; });

						val[0].material_load = v1.mat || 0;
						val[0].jasa_load = v1.jasa || 0;

						var nilai_sp = get_data_qty[0] ? get_data_qty[0].sp : 0;

						table_temp += "<tr data-id='"+v1.id+"' data-id_khs='"+v1.id+"_"+v1.design_mitra_id+"'>";
						table_temp += "<td>"+val[0].designator+"</td>";

						var word = val[0].uraian.split(' '),
						text = '';

						if(word.length > 6){
							text = "...<a style='color:blue; cursor:pointer' class='copy_detail_tool' data-toggle='tooltip' title='"+val[0].uraian+"'>Lihat Detail</a>";
						}

						table_temp += "<td>"+word.slice(0, 6).join(' ') + text +"</td>";
						// table_temp += "<td>"+v1.namcomp+"</td>";
						table_temp += "<td>"+(parseInt(v1.design_mitra_id) == 16 ? 'Telkom Akses' : 'Mitra')+"</td>";
						table_temp += "<td>"+v1.mat.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".")+"</td>";

						total_material += (v1.mat * parseInt(nilai_sp) || 0 );
						total_jasa += (v1.jasa * parseInt(nilai_sp) || 0 );

						material_tot += (v1.mat * parseInt(nilai_sp) || 0 );
						jasa_tot += (v1.jasa * parseInt(nilai_sp) || 0 );

						table_temp += "<td>"+v1.jasa.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".")+"</td>";
						table_temp += "<td><input type='text' style='width:80px;' class='form-control number input-transparent required field_material_input' data-nomor_input='"+parseInt(get_tab_data[0].tabId.replace( /^\D+/g, '') )+"' data-id_mat='"+v1.id+"' data-material='"+v1.mat+"' data-jasa='"+v1.jasa+"' placeholder='0' value='"+ (nilai_sp != 0 ? nilai_sp : '') +"' data-design_mitra_id="+v1.design_mitra_id+"></td>";
						table_temp += "</tr>";
					})

					table_temp += "</tbody>";
					table_temp += "<tfoot>";
					table_temp += "<tr data-nomor_foot='"+parseInt(get_tab_data[0].tabId.replace( /^\D+/g, '') )+"'>";
					table_temp += "<td colspan='3'>Total Harga</td>";
					table_temp += "<td class='hrg_material'>" + total_material.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") + "</td>";
					table_temp += "<td class='hrg_jasa'>" + total_jasa.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") + "</td>";
					table_temp += "<td class='sum_me'>" + (total_material + total_jasa).toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") + "</td>";
					table_temp += "</tr>";
					table_temp += "</tfoot>";
					table_temp += "</table>";
					table_temp += "</div>";
					table_temp += "</div>";
					table_temp += "</div>";

					$(href_data).html(table_temp);

					var material_in = 0,
					jasa_in = 0;

					$('.field_material_input').each(function(){
						var id_material = parseInt($(this).data('id_mat') ),
						material = parseInt($(this).attr('data-material') ),
						jasa = parseInt($(this).attr('data-jasa') ),
						urutan = parseInt($(this).parent().parent().parent().data('nomor') ),
						isi = parseInt($(this).val() ) || 0;
						material_in += material * isi;
						jasa_in += jasa * isi;

						$('#total_mat').val(material_in.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
						$('#total_jasa').val(jasa_in.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
						$('#total_grand').val((material_in + jasa_in).toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
					})

					$('.lok_per').on("keypress keyup",function (e){
						return $(this).val($(this).val().toUpperCase());
					});

					$('.sto_m').each(function() {
						$(this).select2({
							width: '100%',
							placeholder: 'Sto Bisa Dipilih'
						});
					});

					$('.sub_jenis').each(function() {
						$(this).select2({
							width: '100%',
							data: sub_unit,
							placeholder: 'Sub Pekerjaan Bisa Dipilih'
						});
					});

					already_open_tab.push(href_data.split('#')[1]);
				}
			}

			$(this).tab('show');
			$currentTab = $(this);
			e.preventDefault();

		});
		//this method will demonstrate how to add tab dynamically
		var no_tbl = 0,
		sto_i = {!! json_encode($all_sto) !!};

		function registerComposeButtonEvent() {
			/* just for this demo */
			$('.add_boq').on('click', function(e){
				e.preventDefault();
				var jml = $('#jumlah_klm').val();
				// craeteNewTabAndLoadUrl("", "./SamplePage.html", "#" + tabId);

				for (let a = 1; a <= jml; a++) {
					++no_tbl;
					table_temp = '';
					var tabId = "compose" + no_tbl;

					$('.nav-tabs').append('<li><a href="#' + tabId + '" class="nav-link" data-toggle="tab">LOP Nomor <span class="badge badge-primary"">'+no_tbl+'</span><button type="button" class="close closeTab">×</button></a></li>');
					$('.tab-content').append('<div class="tab-pane" id="' + tabId + '"></div>');

					table_temp += "<div class='col-md-12'>";
					table_temp += "<div class='card shadow mb-4'>";
					table_temp += "<div class='card-header'>";
					table_temp += "<strong class='card-title nomor_kol'>Kolom Nomor "+ terbilang(no_tbl) +"</strong>";
					// table_temp += "<a class='float-left toggleCard minimize' href='#!' style='color:#17a2b8; text-decoration:none;'>▲</a><a class='float-right delete_buatan' href='#!' style='color:red; text-decoration:none;'>✖</a>";
					table_temp += "</div>";
					table_temp += "<div class='card-body table-responsive'>";
					table_temp += "<input type='text' class='form-control lok_per input-transparent' name='lokasi_pekerjaan[]' placeholder='Silahkan Isi Lokasi' required>&nbsp;";
					table_temp += "<select class='sto_m generate_title' name='sto[]' required>";

					$.each(sto_i, function(k, v) {
						table_temp += "<option value='"+ v.kode_area +"'>"+ v.kode_area +"</option>"
					})

					table_temp += "</select>&nbsp;";
					table_temp += "<select class='sub_jenis' name='sub_jenis[]' required>";
					table_temp += "</select>&nbsp;";
					table_temp += "<table class='table table_material table-striped table-bordered table-hover' style='width:100%'>";
					table_temp += "<thead class='thead-dark'>";
					table_temp += "<tr>";
					table_temp += "<th rowspan='2'>Designator</th>";
					table_temp += "<th rowspan='2'>Uraian</th>";
					table_temp += "<th rowspan='2'>Jenis Material</th>";
					table_temp += "<th colspan='2'>Paket 7</th>";
					table_temp += "<th rowspan='2'>SP</th>";
					table_temp += "</tr>";
					table_temp += "<tr>";
					table_temp += "<th>Material</th>";
					table_temp += "<th>Jasa</th>";
					table_temp += "</tr>";
					table_temp += "</thead>";
					table_temp += "<tbody data-nomor="+no_tbl+" data-proaktif_id= '0'>";

					load_material.sort(function(a, b) {
						var val1 = $.grep(design, function(e){ return e.id == a.id; });
						var val2 = $.grep(design, function(e){ return e.id == b.id; });

						return val1[0].designator > val2[0].designator;
					});

					if(load_material.length){
						$.each(load_material, function(key2, val2) {
							$.each(design, function(key, val) {
								if(val.id == val2.id){
									table_temp += "<tr data-id="+val.id+" data-id_khs='"+val.id+"_"+val2.design_mitra_id+"'>";
									table_temp += "<td>"+val.designator+"</td>";

									var word = val.uraian.split(' '),
									text = '';

									if(word.length > 6){
										text = "...<a style='color:blue; cursor:pointer' class='copy_detail_tool' data-toggle='tooltip' title='"+val.uraian+"'>Lihat Detail</a>";
									}

									table_temp += "<td>"+word.slice(0, 6).join(' ') + text +"</td>";
									table_temp += "<td>"+(parseInt(val2.design_mitra_id) == 0 ? 'Mitra' : 'Telkom Akses')+"</td>";
									table_temp += "<td>"+val2.mat.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".")+"</td>";
									table_temp += "<td>"+val2.jasa.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".")+"</td>";
									table_temp += "<td><input type='text' style='width:80px;' class='form-control number input-transparent required field_material_input' data-nomor_input='"+tbl_load+"' data-id_mat='"+val.id+"' data-material='"+val2.mat+"' data-jasa="+val2.jasa+" placeholder='0' data-design_mitra_id="+val2.design_mitra_id+"></td>";
									table_temp += "</tr>";
								}
							});
						});
					}

					table_temp += "</tbody>";
					table_temp += "<tfoot>";
					table_temp += "<tr class='foooter_sum'>";
					table_temp += "<td colspan='3'>Total Harga</td>";
					table_temp += "<td class='hrg_material'>0</td>";
					table_temp += "<td class='hrg_jasa'>0</td>";
					table_temp += "<td class='sum_me'>0</td>";
					table_temp += "</tr>";
					table_temp += "</tfoot>";
					table_temp += "</table>";
					table_temp += "</div>";
					table_temp += "</div>";
					table_temp += "</div>";

					showTab(tabId);
					registerCloseEvent();
					$(`#${tabId}`).html(table_temp);
				}

				$('.sto_m').each(function() {
					$(this).select2({
						width: '100%',
						placeholder: 'Sto Bisa Dipilh'
					});
				});

				$('.sub_jenis').each(function() {
					$(this).select2({
						width: '100%',
						data: sub_unit,
						placeholder: 'Sub Pekerjaan Bisa Dipilih'
					});
				});

				$('.lok_per').on("keypress keyup",function (e){
					return $(this).val($(this).val().toUpperCase());
				});

				toastr.options = {
					"closeButton": false,
					"debug": false,
					"newestOnTop": false,
					"progressBar": true,
					"positionClass": "toast-top-center",
					"preventDuplicates": false,
					"onclick": null,
					"showDuration": "300",
					"hideDuration": "1000",
					"timeOut": "5000",
					"extendedTimeOut": "1000",
					"showEasing": "swing",
					"hideEasing": "linear",
					"showMethod": "fadeIn",
					"hideMethod": "fadeOut"
				}
				toastr["success"]("Jumlah Kolom Sudah Ditambahkan!", "Sukses")
			});
		}
		//this method will register event on close icon on the tab..
		function registerCloseEvent() {
			$(".closeTab").click(function () {
				var this_me = $(this),
				text = $(this).parent().text().substring(0, $(this).parent().text().length - 1);

				Swal.fire({
					title: `Apakah Kamu Ingin Menghapus ${text}`,
					icon: 'warning',
					showDenyButton: true,
					confirmButtonText: 'Ya, Hapus!',
					denyButtonText: `Jangan!`,
				}).then((result) => {
					/* Read more about isConfirmed, isDenied below */
					if (result.isConfirmed) {
						//there are multiple elements which has .closeTab icon so close the tab whose close icon is clicked
						var tabContentId = this_me.parent().attr("href");
						this_me.parent().parent().remove(); //remove li of tab
						$('#myTab a:last').click(); // Select first tab
						$(tabContentId).remove(); //remove respective tab content
					} else if (result.isDenied) {
						Swal.fire(`${text} Tidak Dihapus!`, '', 'info')
					}
				})
			});
		}
		//shows the tab with passed content div id..paramter tabid indicates the div where the content resides
		function showTab(tabId) {
			$('#myTab a[href="#' + tabId + '"]').tab('show');
		}
		//return current active tab
		function getCurrentTab() {
			return currentTab;
		}
		//This function will create a new tab here and it will load the url content in tab content div.
		// function craeteNewTabAndLoadUrl(parms, url, loadDivSelector) {
		// 	$("" + loadDivSelector).load(url, function (response, status, xhr) {
		// 		if (status == "error") {
		// 			var msg = "Sorry but there was an error getting details ! ";
		// 			$("" + loadDivSelector).html(msg + xhr.status + " " + xhr.statusText);
		// 			$("" + loadDivSelector).html("Load Ajax Content Here...");
		// 		}
		// 	});
		// }
		//this will return element from current tab
		//example : if there are two tabs having  textarea with same id or same class name then when $("#someId") whill return both the text area from both tabs
		//to take care this situation we need get the element from current tab.
		function getElement(selector) {
			var tabContentId = $currentTab.attr("href");
			return $("" + tabContentId).find("" + selector);
		}

		function removeCurrentTab() {
			var tabContentId = $currentTab.attr("href");
			$currentTab.parent().remove(); //remove li of tab
			$('#myTab a:last').tab('show'); // Select first tab
			$(tabContentId).remove(); //remove respective tab content
		}

		registerComposeButtonEvent();
		registerCloseEvent();

		$("body").tooltip({ selector: '[data-toggle=tooltip]' });

		$('#tahun, #bulan').select2({
			width: '100%'
		})

		$('#proaktif_id').select2({
			placeholder: 'Silahkan Pilih Judul Proaktif',
			allowClear: true,
			width: '100%'
		});

		$('#proaktif_id').val(null).change();

		$('#proaktif_id').on('select2:clear', function() {
			// $('#unknown_item').html('');
			// $('.ini_material').empty().change();
		});

		var load_prev_proaktif = [],
		diff_proaktif = [];

		$('#proaktif_id').on('select2:select, change', function(){
			var _isi = $(this).val();

			if(load_prev_proaktif.length != 0){
				diff_proaktif = _isi.filter(o1 => !load_prev_proaktif.some(o2 => o1 === o2) );
			}else{
				diff_proaktif = _isi;
			}
			// console.log(_isi, load_prev_proaktif, diff_proaktif)
			if(diff_proaktif.length != 0){
				$.each(diff_proaktif, function(k, isi){
					$.ajax({
						url:"/get_ajx/get_proaktif",
						type:"GET",
						data: {
							id : isi
						},
						beforeSend: function (res) {
							$('.ldg').toggleClass('cssload-loader')
							$('.container-fluid').toggleClass('opac')
						},
						dataType: 'json',
						success: (function(dadd){
							// console.log(dadd)
							var find_sto_proaktif = sto_i.filter(function (item) {
								return dadd[0].nama_project.toUpperCase().indexOf(item.kode_area) !== -1;
							})[0];

							sto_pro = '';

							if(typeof(find_sto_proaktif) != 'undefined' && find_sto_proaktif.length != 0){
								sto_pro = find_sto_proaktif.kode_area;
							}

							$('.ldg').toggleClass('cssload-loader')
							$('.container-fluid').toggleClass('opac')
							var redesign_proaktif_lurus = design.filter(o2 => dadd.some(o1 => o1.designator.replace(/\s+/g, '-').toLowerCase() == o2.designator.replace(/\s+/g, '-').toLowerCase() ) ),
							proaktif_material = [],
							pn = {};

							$.each(redesign_proaktif_lurus, function(k, v){

								var get_sp_proaktif = dadd.filter(o1 => o1.designator.replace(/\s+/g, '-').toLowerCase() == v.designator.replace(/\s+/g, '-').toLowerCase() );

								pn[v.id] = {};
								pn[v.id].id = v.id;
								pn[v.id].id_boq_lokasi = 'NEW';
								pn[v.id].id_design = v.id;
								pn[v.id].jenis_khs = v.namcomp;
								pn[v.id].designator = v.designator;
								pn[v.id].material = 0;
								pn[v.id].jasa = 0;
								pn[v.id].sp = parseInt(get_sp_proaktif[0].volume);
								pn[v.id].rekon = 0;
								pn[v.id].tambah = 0;
								pn[v.id].kurang = 0;
								pn[v.id].status_design = 0;
								pn[v.id].jenis = v.jenis;
								pn[v.id].namcomp = v.namcomp;
								pn[v.id].design_mitra_id = v.design_mitra_id;
								pn[v.id].uraian = v.uraian;
							})

							proaktif_material.push(Object.keys(pn).map((k) => pn[k]) );

							var redesign_proaktif_NOK = dadd.filter(o1 => !design.some(o2 => o1.designator.replace(/\s+/g, '-').toLowerCase() == o2.designator.replace(/\s+/g, '-').toLowerCase() ) );

							$.each(design, function(k, v){
								var find_me = dadd.filter(o1 => o1.designator.replace(/\s+/g, '-').toLowerCase() == v.designator.replace(/\s+/g, '-').toLowerCase() );

								if(find_me.length > 0 ){
									$.each(find_me, function(kk, vv){
										if(vv.designator.replace(/\s+/g, '-') == v.designator.replace(/\s+/g, '-') ){
											var val = $.grep(design, function(e){ return e.id == v.id; });
											$.each(proaktif_material[0], function(k, v){
												if(v.id == val[0].id){
													if(v.design_mitra_id != 16){
														proaktif_material[0][k].material = val[0].material;
													}

													proaktif_material[0][k].jasa = val[0].jasa;
												}
											})
										}

										if(vv.designator.substring(0, 2) == 'J-'){
											var val = $.grep(design, function(e){ return e.id == v.id; });
											$.each(proaktif_material[0], function(k, v){
												if(v.id == val[0].id){
													proaktif_material[0][k].jasa = val[0].jasa;
												}
											})
										}

										if(vv.designator.substring(0, 2) == 'M-'){
											var val = $.grep(design, function(e){ return e.id == v.id; });
											$.each(proaktif_material[0], function(k, v){
												if(v.id == val[0].id){
													proaktif_material[0][k].material = val[0].material;
												}
											})
										}
									})
								}
							})
							// console.log(redesign_proaktif_NOK, load_material, last_used_design, proaktif_material)
							// console.log(load_material, proaktif_material[0])
							var unknown_item = '';
							if(redesign_proaktif_NOK.length != 0){
								unknown_item += `Terdapat ${redesign_proaktif_NOK.length} Buah Item Yang Tidak Ada Di KHS</br>`;
								$.each(redesign_proaktif_NOK, function(k, v){
									unknown_item += `•${v.designator}</br>`;
								})
								unknown_item += 'Segera Update KHS di Promise, Kemudian Tambahkan Material!</br>';
							}else{
								unknown_item += '';
							}

							$('#unknown_item').html(unknown_item);
							// $('.ini_material').empty().change()
							$.each(proaktif_material[0], function(k1, v1){
								var val = $.grep(design, function(e){ return e.id == v1.id_design; });
								var check_dup = $.grep(load_material, function(e){ return e.id == v1.id_design; });

								if(typeof(check_dup[0]) == 'undefined')
								{
									if(v1.design_mitra_id == 16){
										$("#material_ta").append('<option data-id="'+ v1.id_design +'" data-id_mitra="' +v1.design_mitra_id+ '" value="'+ v1.id_design +'" selected>'+  val[0].designator +' (' + v1.jenis + ' | ' + v1.namcomp + ')</option>').change();
									}else{
										$("#material_mitra").append('<option data-id="'+ v1.id_design +'" data-id_mitra="' +v1.design_mitra_id+ '" value="'+ v1.id_design +'" selected>'+  val[0].designator +' (' + v1.jenis + ' | ' + v1.namcomp + ')</option>').change();
									}
								}
							});

							add_material($('.ini_material') );

							$.each(proaktif_material, function(k, v){
								table_temp = ''
								tbl_load += 1;

								var tabId = "compose" + tbl_load,
								delete_btn = '';

								if(v[0].id_boq_lokasi == 'NEW'){
									delete_btn = '<button type="button" class="close closeTab">×</button>';
								}

								$('.nav-tabs').append('<li><a href="#' + tabId + '" class="nav-link" data-toggle="tab">LOP Proaktif Nomor <span class="badge badge-primary"">'+tbl_load+'</span>'+delete_btn+'</a></li>');
								$('.tab-content').append('<div class="tab-pane" id="' + tabId + '"></div>');

								table_temp += "<div class='col-md-12'>";
								table_temp += "<div class='card shadow mb-4'>";
								table_temp += "<div class='card-header'>";
								table_temp += "<strong class='card-title nomor_kol'>Kolom Nomor "+ terbilang(tbl_load) +"</strong>";
								table_temp += "</div>";
								table_temp += "<div class='card-body table-responsive'>";

								table_temp += `<input type='text' class='form-control lok_per input-transparent' name='lokasi_pekerjaan[]' placeholder='Silahkan Isi Lokasi' required value='${ dadd[0].nama_project }'>&nbsp;`;
								table_temp += "<select class='sto_m generate_title' name='sto[]' required>";

								$.each(sto_i, function(k, v) {
									table_temp += "<option value='"+ v.kode_area +"'>"+ v.kode_area +"</option>"
								})

								table_temp += "</select>&nbsp;";
								table_temp += "<select class='sub_jenis' name='sub_jenis[]' required>";
								table_temp += "</select>&nbsp;";
								table_temp += "<table class='table table_material table-striped table-bordered table-hover' style='width:100%'>";
								table_temp += "<thead class='thead-dark'>";
								table_temp += "<tr>";
								table_temp += "<th rowspan='2'>Designator</th>";
								table_temp += "<th rowspan='2'>Uraian</th>";
								table_temp += "<th rowspan='2'>Jenis Material</th>";
								table_temp += "<th colspan='2'>Paket 7</th>";
								table_temp += "<th rowspan='2'>SP</th>";
								table_temp += "</tr>";
								table_temp += "<tr>";
								table_temp += "<th>Material</th>";
								table_temp += "<th>Jasa</th>";
								table_temp += "</tr>";
								table_temp += "</thead>";
								table_temp += "<tbody data-nomor='"+tbl_load+"' data-proaktif_id= '"+isi+"'>";

								var total_material = 0,
								total_jasa = 0;

								$.each(v, function(k1, v1){
									total_material += (v1.material * v1.sp);
									total_jasa += (v1.jasa * v1.sp);
								})

								if(load_material.length){
									$.each(load_material, function(key2, val2) {
										$.each(design, function(key, val) {
											if(val.id == val2.id){
												table_temp += "<tr data-id="+val.id+" data-id_khs='"+val.id+"_"+val.design_mitra_id+"'>";
												table_temp += "<td>"+val.designator+"</td>";

												var word = val.uraian.split(' '),
												text = '';

												if(word.length > 6){
													text = "...<a style='color:blue; cursor:pointer' class='copy_detail_tool' data-toggle='tooltip' title='"+val.uraian+"'>Lihat Detail</a>";
												}

												table_temp += "<td>"+word.slice(0, 6).join(' ') + text +"</td>";
												table_temp += "<td>"+val.namcomp+"</td>";
												table_temp += "<td>"+val2.mat.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".")+"</td>";
												table_temp += "<td>"+val2.jasa.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".")+"</td>";
												var val = $.grep(v, function(e){ return e.id == val2.id; }),
												val_sp = val.length != 0 ? val[0].sp : '0';

												table_temp += "<td><input type='text' style='width:80px;' class='form-control number input-transparent required field_material_input' data-nomor_input='"+tbl_load+"' data-id_mat='"+val.id+"' data-material='"+val2.mat+"' data-jasa="+val2.jasa+" placeholder='0' value="+val_sp+" data-design_mitra_id="+val2.design_mitra_id+"></td>";
												table_temp += "</tr>";
											}
										});
									});
								}

								table_temp += "</tbody>";
								table_temp += "<tfoot>";
								table_temp += "<tr data-nomor_foot='"+tbl_load+"'>";
								table_temp += "<td colspan='3'>Total Harga</td>";
								table_temp += "<td class='hrg_material'>" + total_material.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") + "</td>";
								table_temp += "<td class='hrg_jasa'>" + total_jasa.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") + "</td>";
								table_temp += "<td class='sum_me'>" + (total_material + total_jasa).toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") + "</td>";
								table_temp += "</tr>";
								table_temp += "</tfoot>";
								table_temp += "</table>";
								table_temp += "</div>";
								table_temp += "</div>";
								table_temp += "</div>";

								// $(this).tab('show');
								showTab(tabId);
								registerCloseEvent();

								$(`#${tabId}`).html(table_temp);
							})

							no_tbl += tbl_load;

							$('.lok_per').on("keypress keyup",function (e){
								return $(this).val($(this).val().toUpperCase());
							});

							$('.sto_m').each(function() {
								$(this).select2({
									width: '100%',
									placeholder: 'Sto Bisa Dipilih'
								});
							});

							$(".sto_m").val(sto_pro).change();

							$('.sub_jenis').each(function() {
								$(this).select2({
									width: '100%',
									data: sub_unit,
									placeholder: 'Sub Pekerjaan Bisa Dipilih'
								});
							});

							var material_tot = 0,
							jasa_tot = 0;

							$('.field_material_input').each(function(){
								var id_material = parseInt($(this).data('id_mat') ),
								material = parseInt($(this).attr('data-material') ),
								jasa = parseInt($(this).attr('data-jasa') ),
								urutan = parseInt($(this).parent().parent().parent().data('nomor') ),
								isi = parseInt($(this).val() ) || 0;
								material_tot += material * isi;
								jasa_tot += jasa * isi;
							});

							$('#total_mat').val(material_tot.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
							$('#total_jasa').val(jasa_tot.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
							$('#total_grand').val((material_tot + jasa_tot).toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
						})
					})
				})
				load_prev_proaktif = [..._isi]
			}
		})

		$('#sto_obj').select2({
			width: '100%',
			placeholder: 'Sto Bisa Dipilih'
		})

		$('#pekerjaan').on('change', function(){
			if($(this).val() == 'Construction'){
				$('.toc_cons').css({
					'display': 'flex'
				});

				$('#toc_edit').attr('required', 'required');
				$('#batch').attr('required', 'required');
				$('#jenis_po').attr('required', 'required');
			}else{
				$('.toc_cons').css({
					'display': 'none'
				});

				$('#toc_edit').removeAttr('required');
				$('#batch').removeAttr('required');
				$('#jenis_po').removeAttr('required');
			}
		})

		$('#tahun').val({!! json_encode($tahun) !!}).change();
		$('#bulan').val({!! json_encode($bulan) !!}).change();

		$('.aply_all').on('click', function(){
			$('.lok_per').val( $('#lokasi_pekerjaan').val() ).change();
			$('.sto_m').val( $('#sto_obj').val() ).change();

			toastr.options = {
				"closeButton": false,
				"debug": false,
				"newestOnTop": false,
				"progressBar": true,
				"positionClass": "toast-top-center",
				"preventDuplicates": false,
				"onclick": null,
				"showDuration": "300",
				"hideDuration": "1000",
				"timeOut": "5000",
				"extendedTimeOut": "1000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut"
			}
			toastr["success"]("Lokasi dan STO Berhasil Disamakan!", "Sukses")
		})

		$(document).on('keyup', '.number', function(event){
			if(event.which >= 37 && event.which <= 40) return;
			$(this).val(function(index, value) {
        return value.replace(/\D/g, "");
      });
		})

		$(document).on('click', '.toggleCard', function(){
			$(this).html((i, t) => t === '▲' ? '▼' : '▲');
			$(this).parent().next('.card-body').slideToggle();
		})

		$(document).on('click', '.delete_buatan', function(){
			$(this).parent().parent().parent().remove();

			var material_tot_chck = 0,
			jasa_tot_chck = 0;

			$('.field_material_input').each(function(){
				var tfoot = $(this).parent().parent().parent().next(),
				body = $(this).parent().parent().parent();
				// console.log(tfoot, body);
				var material_in = 0,
				jasa_in = 0;
				body.each(function(){
					$(this).find('.field_material_input').each(function(){
						var id_material = parseInt($(this).data('id_mat') ),
						material = parseInt($(this).attr('data-material') ),
						jasa = parseInt($(this).attr('data-jasa') ),
						urutan = parseInt($(this).parent().parent().parent().data('nomor') ),
						isi_material = parseInt($(this).val() ) || 0;
						material_in += material * isi_material;
						jasa_in += jasa * isi_material;
						tfoot.find('.hrg_material').text(material_in.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
						tfoot.find('.hrg_jasa').text(jasa_in.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
						tfoot.find('.sum_me').text((material_in + jasa_in).toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
					})
				});

				var id_material = parseInt($(this).data('id_mat') ),
				material = parseInt($(this).attr('data-material') ),
				jasa = parseInt($(this).attr('data-jasa') ),
				urutan = parseInt($(this).parent().parent().parent().data('nomor') ),
				isi = parseInt($(this).val() ) || 0;
				material_tot_chck += material * isi;
				jasa_tot_chck += jasa * isi;
			});

			$('#total_mat').val(material_tot_chck.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
			$('#total_jasa').val(jasa_tot_chck.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
			$('#total_grand').val((material_tot_chck + jasa_tot_chck).toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
		});

		$('select[id="mitra_id"]').select2({
			width: '100%',
			placeholder: "Masukkan Nama Perusahaan Mitra",
			allowClear: true,
			ajax: {
				url: "/get_ajx/mitra/search/per_witel",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						searchTerm: params.term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true,
			},
		});

		$('#pekerjaan').val(null).change();

		var sto_i = {!! json_encode($all_sto) !!};

		$('#pekerjaan').select2({
			width: '100%',
			placeholder: 'Isi jenis pekerjaan',
		});

		$('#jumlah_klm').select2({
			width: '100%',
		});

		$('#jenis_kontrak').select2({
			width: '100%',
			placeholder: 'Isi jenis Kontrak',
		});

		var isi;

		$('#material_ta').select2({
			width: '100%',
			placeholder: "Masukkan Nama Material",
			allowClear: true,
			ajax: {
				url: "/get_ajx/find_material",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					isi = params.term;
					return {
						searchTerm: params.term,
						pekerjaan : $('#pekerjaan').val(),
						tahun : $('#tahun').val(),
						bulan : $('#bulan').val(),
						jenis : 'all',
						witel : {!! json_encode(session('auth')->Witel_New) !!},
					};
				},
				processResults: function (response) {
					if(isi == null && response.length != 0){
						design = $.grep(design, function(element, index){return $.inArray(parseInt(element.design_mitra_id), [0, 16]) !== -1 }, true);
						design.push(...response);
					}
					return {
						results: response
					};
				},
				cache: true,
			},
			// language: {
			// 	noResults: function(){
			// 		return "Tidak Menemukan Material? Buat Material New Item</b>&nbsp;<a type='button' class='btn btn-info btn-sm' href='/Report/material_manual/input' target='_blank'><span class='fe fe-plus-circle fe-16'></span> Tambah No PKS Baru</a>";
			// 	}
			// },
			escapeMarkup: function (markup) {
				return markup;
			},
			templateSelection: function(data) {
				var $result = $(
					"<span data-id=" +data.id+ ">" + data.text + "</span>"
				);
				return $result;
			}
		});

		$('#material_mitra').select2({
			width: '100%',
			placeholder: "Masukkan Nama Material",
			allowClear: true,
			ajax: {
				url: "/get_ajx/find_material",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					isi = params.term;
					return {
						searchTerm: params.term,
						pekerjaan : $('#pekerjaan').val(),
						tahun : $('#tahun').val(),
						bulan : $('#bulan').val(),
						jenis : 'all',
						witel : {!! json_encode(session('auth')->Witel_New) !!},
					};
				},
				processResults: function (response) {
					if(isi == null && response.length != 0){
						design = $.grep(design, function(element, index){return $.inArray(parseInt(element.design_mitra_id), [0, 16]) !== -1 }, true);
						design.push(...response);
					}
					return {
						results: response
					};
				},
				cache: true,
			},
			// language: {
			// 	noResults: function(){
			// 		return "Tidak Menemukan Material? Buat Material New Item</b>&nbsp;<a type='button' class='btn btn-info btn-sm' href='/Report/material_manual/input' target='_blank'><span class='fe fe-plus-circle fe-16'></span> Tambah No PKS Baru</a>";
			// 	}
			// },
			escapeMarkup: function (markup) {
				return markup;
			},
			templateSelection: function(data) {
				var $result = $(
					"<span data-id=" +data.id+ ">" + data.text + "</span>"
				);
				return $result;
			}
		});

		$('#pekerjaan').on('change', function(){
			var jenis_kerja = {!! json_encode($kerjaan[1]) !!},
			kerja = $(this).val(),
			cari = [];

			$.each(jenis_kerja, function(key, val) {
				if(val.pekerjaan == kerja){
					cari.push({id: val.id, text: val.text})
				}
			});

			$("#jenis_work").empty().trigger('change')

			$('#jenis_work').select2({
				width: '100%',
				data: cari,
				placeholder: "Silahkan Pilih Jenis Pekerjaan"
			})

			$('#jenis_work').val(null).change();
		})

		var sub_unit;

		$('#jenis_work').on('change', function(){
			var sub = $(this).val();

			if(sub){
				$.ajax({
					url:"/get_ajx/check_sub_pekerjaan",
					type:"GET",
					data: {
						pekerjaan : sub
					},
					dataType: 'json',
					success: (function(data){
						sub_unit = data;

						$(".sub_jenis").empty().trigger('change')

						$('.sub_jenis').each(function() {
							$(this).select2({
								width: '100%',
								data: sub_unit,
								placeholder: 'Sub Pekerjaan Bisa Dipilih'
							});
						});
					})
				})
			}
		})

		function terbilang(nilai)
    {
			var hasil;
			if (nilai < 0){
				hasil = "minus " + penyebut(nilai);
			}else{
				hasil = penyebut(nilai);
			}

			return hasil;
    }

    function penyebut(nilai)
    {
			var nilai = Math.abs(Math.floor(nilai)),
			huruf = ["", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas"],
			temp;
			if(nilai < 12){
				temp = " " + huruf[nilai];
			}else if(nilai < 20){
				temp = penyebut(nilai - 10) + " belas";
			}else if(nilai < 100){
				temp = penyebut(nilai / 10) + " puluh" + penyebut(nilai % 10);
			} else if (nilai < 200) {
				temp = " seratus" + penyebut(nilai - 100);
			} else if (nilai < 1000) {
				temp = penyebut(nilai / 100) + " ratus" + penyebut(nilai % 100);
			}
			return temp;
    }

		var load_material = [];

		function add_material(mm){
			var td_material = '';
			sukses_filter = [];
			$.each(mm, function(k, v){
				var value = $(this).val(),
				jenis_khs = parseInt($(this).data('design_id'));
				var material = [];
				var isi_load = value;

				if(isi_load.length){
					var filter_lm = [];
					$.each(load_material, function(k, vx){
						if(vx.design_mitra_id == jenis_khs){
							filter_lm.push(vx);
							sukses_filter.push(vx);
						}
					})

					var diff = filter_lm.filter(o1 => !isi_load.some(o2 => o1.id === parseInt(o2) ) );

					if(diff){
						$.each(diff, function(key, val){
							$("tr[data-id_khs='" + val.id +"_"+val.design_mitra_id+"']").remove();
						});
					}
					//ini kalau ada materialnya
					var intersection = filter_lm.filter(o1 => isi_load.some(o2 => o1.id === parseInt(o2) ) );
					var remove_dup = isi_load.filter(o1 => !intersection.some(o2 => parseInt(o1) === o2.id) );
					material = remove_dup;
				}

				var reselect_load = last_used_design;

				$.each(material, function(key2, val2) {
					$.each(design, function(key, val) {
						if(val.id == val2){
							var loop_me = true;

							if(jenis_khs == 16){
								var material_new = 0,
								material_text = 0;
							}else{
								var material_new = val.material,
								material_text = val.material.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".");
							}

							if(reselect_load.length != 0){
								var tbl_m = $(".table_material").find('tbody');
								$.each(reselect_load, function(kx, vx){
									$.each(vx, function(kk, vv){
										if(vv.id_design == val2){

											var total_material_sp = 0,
											total_jasa_sp = 0;

											td_material += "<tr data-id='"+vv.id_design+"' data-id_khs='"+vv.id_design+"_"+jenis_khs+"'>";
											td_material += "<td>"+val.designator+"</td>";

											var word = val.uraian.split(' '),
											text = '';

											if(word.length > 6){
												text = "...<a style='color:blue; cursor:pointer' class='copy_detail_tool' data-toggle='tooltip' title='"+val.uraian+"'>Lihat Detail</a>";
											}

											td_material += "<td>"+word.slice(0, 6).join(' ') + text +"</td>";
											td_material += "<td>"+(jenis_khs == 0 ? 'Mitra' : 'Telkom Akses')+"</td>";
											td_material += "<td>"+material_text+"</td>";
											td_material += "<td>"+val.jasa.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".")+"</td>";

											total_material_sp += (material_new * vv.sp);
											total_jasa_sp     += (val.jasa * vv.sp);

											td_material += "<td><input type='text' style='width:80px;' class='form-control number input-transparent required field_material_input' data-nomor_input='"+tbl_load+"' data-id_mat='"+val.id+"' data-material='"+material_new+"' data-sp='"+vv.sp+"' data-jasa='"+val.jasa+"' data-design_mitra_id="+jenis_khs+" placeholder='0' value='"+ (vv.sp ?? '') +"'></td>";
											td_material += "</tr>";
											loop_me = false;
										}
									});

									// if($(".table_material").length){
									// 	var data = [];
									// 	tbl_m.each(function(key, val){
									// 		data[key] = [];
									// 		$(this).children('td').each(function(key2, val2){
									// 			data[key][key2] = $(this).text();
									// 		});
									// 	});
									// 	tbl_m.eq(0).append(td_material)
									// 	tbl_m = tbl_m.slice(1);
									// 	td_material = '';
									// }
									if($(".table_material").length){
										// var data = [];
										// $(".table_material tr").each(function(key, val){
										// 	data[key] = [];
										// 	$(this).children('td').each(function(key2, val2){
										// 		data[key][key2] = $(this).text();
										// 	});
										// });
										tbl_m.eq(0).append(td_material)
										tbl_m = tbl_m.slice(1);
										td_material = '';
									}
								});
							}

							if(loop_me){
								td_material += "<tr data-id="+val.id+" data-id_khs='"+val.id+"_"+jenis_khs+"'>";
								td_material += "<td>"+val.designator+"</td>";

								var word = val.uraian.split(' '),
								text = '';

								if(word.length > 6){
									text = "...<a style='color:blue; cursor:pointer' class='copy_detail_tool' data-toggle='tooltip' title='"+val.uraian+"'>Lihat Detail</a>";
								}

								td_material += "<td>"+word.slice(0, 6).join(' ') + text +"</td>";
								td_material += "<td>"+(jenis_khs == 0 ? 'Mitra' : 'Telkom Akses')+"</td>";
								td_material += "<td>"+material_text+"</td>";
								td_material += "<td>"+val.jasa.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".")+"</td>";
								td_material += "<td><input type='text' style='width:80px;' class='form-control number input-transparent required field_material_input' data-nomor_input='"+tbl_load+"' data-id_mat="+val.id+" data-material="+material_new+" data-jasa="+val.jasa+" placeholder='0' data-design_mitra_id="+jenis_khs+"></td>";
								td_material += "</tr>";

								if($(".table_material").length){
									// var data = [];
									// $(".table_material tr").each(function(key, val){
									// 	data[key] = [];
									// 	$(this).children('td').each(function(key2, val2){
									// 		data[key][key2] = $(this).text();
									// 	});
									// });
									$(".table_material").find('tbody').append(td_material)
									td_material = '';
								}
							}
						}
					});
				});
			});

			var diff = load_material.filter(o1 => !sukses_filter.some(o2 => o1.id + '_' + o1.design_mitra_id === o2.id + '_' + o2.design_mitra_id ) );

			if(diff){
				$.each(diff, function(key, val){
					$("tr[data-id_khs='" + val.id +"_"+val.design_mitra_id+"']").remove();
				});
			}

			load_material = [];

			$.each(mm, function(k, v){
				var value = $(this).val(),
				jenis_khs = parseInt($(this).data('design_id'));
				var isi_load = value;
				$.each(isi_load, function(key2, val2) {
					$.each(design, function(key, val) {
						if(val2 == val.id){
							if(jenis_khs == 16){
								var material_new = 0;
							}else{
								var material_new = (val.material_load !== undefined ? val.material_load : val.material);
							}

							load_material.push({
								id: val.id,
								mat: material_new,
								jasa: (val.jasa_load !== undefined ? val.jasa_load : val.jasa),
								design_mitra_id: jenis_khs
							});
						}
					});
				});
			});

			var material_tot_chck = 0,
			jasa_tot_chck = 0;

			$('.field_material_input').each(function(){
				var tfoot = $(this).parent().parent().parent().next(),
				body = $(this).parent().parent().parent();
				// console.log(tfoot, body);
				var material_in = 0,
				jasa_in = 0;

				body.each(function(){
					$(this).find('.field_material_input').each(function(){
						var id_material = parseInt($(this).data('id_mat') ),
						material = parseInt($(this).attr('data-material') ) || 0,
						jasa = parseInt($(this).attr('data-jasa') ) || 0,
						isi_material = parseInt($(this).val() ) || 0;

						material_in += material * isi_material;
						jasa_in += jasa * isi_material;
						tfoot.find('.hrg_material').text(material_in.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") );
						tfoot.find('.hrg_jasa').text(jasa_in.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") );
						tfoot.find('.sum_me').text( (material_in + jasa_in).toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") );
					})
				});

				var id_material = parseInt($(this).data('id_mat') ),
				material = parseInt($(this).attr('data-material') ) || 0,
				jasa = parseInt($(this).attr('data-jasa') ) || 0,
				urutan = parseInt($(this).parent().parent().parent().data('nomor') ),
				isi = parseInt($(this).val() ) || 0;
				material_tot_chck += material * isi;
				jasa_tot_chck += jasa * isi;
			});

			if($('.field_material_input').length == 0 ){
				$('.hrg_material').text(0);
				$('.hrg_jasa').text(0);
				$('.sum_me').text(0);
			}

			$('#total_mat').val(material_tot_chck.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
			$('#total_jasa').val(jasa_tot_chck.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
			$('#total_grand').val((material_tot_chck + jasa_tot_chck).toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
		}

		$('.add_material').on('click', function(){
			add_material($('.ini_material') );

			toastr.options = {
				"closeButton": false,
				"debug": false,
				"newestOnTop": false,
				"progressBar": true,
				"positionClass": "toast-top-center",
				"preventDuplicates": false,
				"onclick": null,
				"showDuration": "300",
				"hideDuration": "1000",
				"timeOut": "5000",
				"extendedTimeOut": "1000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut"
			}
			toastr["success"]("Material Berhasil Diubah!", "Sukses")
		})

		$('#modal_edit_material').on('show.bs.modal', function(){
			table_temp = ''
			table_temp += "<table class='table table_material table-striped table-bordered table-hover' style='width:100%'>";
			table_temp += "<thead class='thead-dark'>";
			table_temp += "<tr>";
			table_temp += "<th rowspan='2'>Designator</th>";
			table_temp += "<th rowspan='2'>Uraian</th>";
			table_temp += "<th colspan='3'>Paket 7</th>";
			table_temp += "</tr>";
			table_temp += "<tr>";
			table_temp += "<th>Jenis Material</th>";
			table_temp += "<th>Material</th>";
			table_temp += "<th>Jasa</th>";
			table_temp += "</tr>";
			table_temp += "</thead>";
			table_temp += "<tbody>";
				// console.log(load_material)
			if(load_material.length){
				$.each(load_material, function(key2, val2) {
					$.each(design, function(key, val) {
						if(val.id == val2.id){
							table_temp += "<tr data-id="+val.id+" data-id_khs='"+val.id+"_"+val.design_mitra_id+"'>";
							table_temp += "<td>"+val.designator+"</td>";
							table_temp += "<td>"+val.uraian+"</td>";
							table_temp += "<td>"+(parseInt(val2.design_mitra_id) == 16 ? 'Telkom Akses' : 'Mitra')+"</td>";
							// table_temp += "<td><input type='checkbox' class='check_khs' data-id="+val.id+" " + (val2.design_mitra_id == 16 ? 'checked' : '' ) + "></td>";
							table_temp += "<td><input type='checkbox' class='check_mat' data-jenis_khs='"+val2.design_mitra_id+"' data-jenis='material' data-id="+val.id+" data-val_harga=" + (parseInt(val.material) || 0) + " " + (val2.mat != 0 ? 'checked' : '' ) + "> &nbsp;"+val.material.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".")+"</td>";
							table_temp += "<td><input type='checkbox' class='check_mat' data-jenis_khs='"+val2.design_mitra_id+"' data-jenis='jasa' data-id="+val.id+" data-val_harga=" + (parseInt(val.jasa) || 0) + " " + (val2.jasa != 0 ? 'checked' : '' ) + "> &nbsp;"+val.jasa.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".")+"</td>";
							table_temp += "</tr>";
						}
					});
				});
			}

			table_temp += "</tbody>";
			table_temp += "</table>";

			$('.data_mdl').html(table_temp)

			$('.check_mat').on('change', function(){
				var jenis = $(this).data('jenis'),
				val = parseInt($(this).data('val_harga') ) || 0;
				id = $(this).data('id'),
				jenis_khs = $(this).data('jenis_khs'),
				isi = 0;

				if($(this).is(':checked') ){
					isi = val;
				}

				$.each(load_material, function(k, v){
					if(v.id == id && jenis_khs == v.design_mitra_id){
						var val = $.grep(design, function(e){ return e.id == v.id; });
						if(jenis == 'material'){
							load_material[k].mat = isi;
							val[0].material_load = isi;
						}

						if(jenis == 'jasa'){
							load_material[k].jasa = isi;
							val[0].jasa_load = isi;
						}
					}
				})

				$('.field_material_input').each(function(){
					var id_material = parseInt($(this).data('id_mat') ),
					material_input = parseInt($(this).attr('data-material') ),
					design_mitra_id = parseInt($(this).attr('data-design_mitra_id') ),
					jasa_input = parseInt($(this).attr('data-jasa') );

					if(id_material == id && design_mitra_id == jenis_khs)
					{
						if(jenis == 'material'){
							$(this).attr('data-material', isi);
							$(this).parent().prev().prev().text(isi.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") );
						}
						if(jenis == 'jasa'){
							$(this).attr('data-jasa', isi);
							$(this).parent().prev().text(isi.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") );
						}
					}
				});

				var material_tot_chck = 0,
				jasa_tot_chck = 0;

				$('.field_material_input').each(function(){
					var tfoot = $(this).parent().parent().parent().next(),
					body = $(this).parent().parent().parent();
					// console.log(tfoot, body);
					var material_in = 0,
					jasa_in = 0;
					body.each(function(){
						$(this).find('.field_material_input').each(function(){
							var id_material = parseInt($(this).data('id_mat') ),
							material = parseInt($(this).attr('data-material') ),
							jasa = parseInt($(this).attr('data-jasa') ),
							urutan = parseInt($(this).parent().parent().parent().data('nomor') ),
							isi_material = parseInt($(this).val() ) || 0;
							material_in += material * isi_material;
							jasa_in += jasa * isi_material;
							tfoot.find('.hrg_material').text(material_in.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
							tfoot.find('.hrg_jasa').text(jasa_in.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
							tfoot.find('.sum_me').text((material_in + jasa_in).toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
						})
					});

					var id_material = parseInt($(this).data('id_mat') ),
					material = parseInt($(this).attr('data-material') ),
					jasa = parseInt($(this).attr('data-jasa') ),
					urutan = parseInt($(this).parent().parent().parent().data('nomor') ),
					isi = parseInt($(this).val() ) || 0;
					material_tot_chck += material * isi;
					jasa_tot_chck += jasa * isi;
				});

				$('#total_mat').val(material_tot_chck.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
				$('#total_jasa').val(jasa_tot_chck.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
				$('#total_grand').val((material_tot_chck + jasa_tot_chck).toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
			});

			// $('.check_khs').on('change', function(){
			// 	var isi = 0,
			// 	id = $(this).data('id');

			// 	if($(this).is(':checked') ){
			// 		isi = 16;
			// 	}

			// 	$.each(load_material, function(k, v){
			// 		if(v.id == id){
			// 			load_material[k].design_mitra_id = isi;
			// 		}
			// 	})

			// 	$('.field_material_input').each(function(){
			// 		var id_material = parseInt($(this).data('id_mat') );

			// 		if(id_material == id)
			// 		{
			// 			$(this).attr('data-design_mitra_id', isi);
			// 		}
			// 	})
			// });
		});

		var no_tbl = 0;

		$.each(load_material_ada, function(k1, v1){
			var val = $.grep(design, function(e){ return e.id == v1.id_design; });
			if(v1.design_mitra_id == 16){
				$("#material_ta").append('<option data-id="'+ v1.id_design +'" data-id_mitra="' +v1.design_mitra_id+ '" value="'+ v1.id_design +'" selected>'+  val[0].designator +' (' + v1.jenis + ' | ' + v1.namcomp + ')</option>').change();
			}else{
				$("#material_mitra").append('<option data-id="'+ v1.id_design +'" data-id_mitra="' +v1.design_mitra_id+ '" value="'+ v1.id_design +'" selected>'+  val[0].designator +' (' + v1.jenis + ' | ' + v1.namcomp + ')</option>').change();
			}
		});

		if(last_used_design.length != 0){
			var tbl_load = 0,
			load_data_html = [],
			material_tot = 0,
			jasa_tot = 0;

			$.each(last_used_design, function(k, v){
				tbl_load = ++k;

				add_material($('.ini_material') );

				var tabId = "compose" + tbl_load;

				$('.nav-tabs').append('<li><a href="#' + tabId + '" class="nav-link" data-toggle="tab">LOP Nomor <span class="badge badge-primary"">'+tbl_load+'</span><button type="button" class="close closeTab">×</button></a></li>');
				$('.tab-content').append('<div class="tab-pane" id="' + tabId + '"></div>');

				// $(this).tab('show');
				if(load_data_html.map(x => x.tabId).indexOf(tabId) === -1){
					load_data_html.push({
						'tabId': tabId,
					});
				}
				// $(`#${tabId}`).html(table_temp);
			});

			no_tbl += tbl_load;

			$('#myTab a[href="#compose1"]').click()

			$('#total_mat').val(material_tot.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
			$('#total_jasa').val(jasa_tot.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
			$('#total_grand').val((material_tot + jasa_tot).toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
		}

		$("#judul").focusout(function(){
			var url = window.location.href.split('/')[4];
			var id = url;

			if(url == 'input'){
				id = null;
			}

			var nama_judul = $(this).val();
			if(nama_judul){
				$.ajax({
					url:"/get_ajx/check_judul_pekerjaan",
					type:"POST",
					data: {
						_token: "{{ csrf_token() }}",
						nama_judul : nama_judul,
						id: id
					},
					dataType: 'json',
					success: (function(data){
						$(".check_jdl").html(data[0])
						$(".check_jdl").css({
							'color' : data[1]
						});
						if(data[1] == 'red'){
							$('.submit_btn').attr('disabled', 'disabled')
						}else{
							$('.submit_btn').removeAttr('disabled')
						}
					})
				})
			}
		});

		$('.submit_btn').on('click', function(e){
			var submit = [];
			var nama_judul = $(this).val(),
			jenis_btn = $(this).attr('data-jenis_btn');

			$('#myTab a').each(function(k, v){
				var attr_href = $(this).attr('href').split('#')[1];

				if($.inArray(attr_href, already_open_tab) == -1 ){
					$("#myTab a[href='#"+attr_href+"']").click()
				}
			})

			// e.preventDefault();
			$.each($('.lok_per'), function(k, v){
				if($(this).val().trim().length == 0){
					e.preventDefault();
					$(this).css({
						'border-color': 'red'
					});
					toastr.options = {
						"closeButton": false,
						"debug": false,
						"newestOnTop": false,
						"progressBar": true,
						"positionClass": "toast-top-center",
						"preventDuplicates": false,
						"onclick": null,
						"showDuration": "300",
						"hideDuration": "1000",
						"timeOut": "5000",
						"extendedTimeOut": "1000",
						"showEasing": "swing",
						"hideEasing": "linear",
						"showMethod": "fadeIn",
						"hideMethod": "fadeOut"
					}
					toastr["warning"]("Lokasi Tidak Boleh Kosong!", "Gagal")
				}
			})

			if(nama_judul){
				var url = window.location.href.split('/')[4]
				var id = url;

				if(url == 'input'){
					id = null;
				}

				$.ajax({
					url:"/get_ajx/check_judul_pekerjaan",
					type:"POST",
					data: {
						_token: "{{ csrf_token() }}",
						nama_judul : nama_judul,
						id: id
					},
					dataType: 'json',
					success: (function(data){
						$(".check_jdl").html(data[0])
						$(".check_jdl").css({
							'color' : data[1]
						});
						if(data[1] == 'red'){
							$('.submit_btn').attr('disabled', 'disabled')
							e.preventDefault();
						}else{
							$('.submit_btn').removeAttr('disabled')
						}
					}),
				})
			}

			$('.field_material_input').each(function(){
				var id_material = parseInt($(this).data('id_mat') ),
				material = parseInt($(this).attr('data-material') ) || 0,
				jasa = parseInt($(this).attr('data-jasa') ) || 0,
				jenis_khs = parseInt($(this).attr('data-design_mitra_id') ),
				urutan = parseInt($(this).parent().parent().parent().data('nomor') ),
				proaktif_id = parseInt($(this).parent().parent().parent().data('proaktif_id') ) || 0,
				lokasi = $(this).parent().parent().parent().parent().parent().find('.lok_per').val(),
				sto = $(this).parent().parent().parent().parent().parent().find('.sto_m').val(),
				sub_jenis = $(this).parent().parent().parent().parent().parent().find('.sub_jenis').val(),
				isi = parseInt($(this).val() ) || 0;

				var get_design = design.find(o => o.id == id_material);

				submit.push({
					id: id_material,
					material: material,
					jasa: jasa,
					val: isi,
					sp: isi,
					rekon: 0,
					jenis_khs: jenis_khs,
					urutan: urutan,
					lokasi: lokasi,
					sto: sto,
					sub_jenis: sub_jenis,
					id_design: id_material,
					proaktif_id: proaktif_id,
					// designator: get_design.designator,
					namcomp: (jenis_khs == 16 ? 'Telkom Akses' : 'Mitra'),
					design_mitra_id: jenis_khs,
					jenis: get_design.jenis,
				});
			});
			let hasil = JSON.stringify(submit);

			$("input[name='material_input']").val(hasil)
			$("input[name='jns_btn']").val(jenis_btn)
		})

		$(document).on('keyup', '.field_material_input', function(event){
			var material_in = 0,
			jasa_in = 0;
			$('.field_material_input').each(function(){
				var id_material = parseInt($(this).data('id_mat') ),
				material = parseInt($(this).attr('data-material') ),
				jasa = parseInt($(this).attr('data-jasa') ),
				urutan = parseInt($(this).parent().parent().parent().data('nomor') ),
				isi = parseInt($(this).val() ) || 0;
				material_in += material * isi;
				jasa_in += jasa * isi;

				$('#total_mat').val(material_in.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
				$('#total_jasa').val(jasa_in.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
				$('#total_grand').val((material_in + jasa_in).toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
			})

			var tfoot = $(this).parent().parent().parent().next(),
			body = $(this).parent().parent().parent();
			// console.log(tfoot, body);
			var material_in = 0,
			jasa_in = 0;

			body.each(function(){
				$(this).find('.field_material_input').each(function(){
					var id_material = parseInt($(this).data('id_mat') ),
					material = parseInt($(this).attr('data-material') ),
					jasa = parseInt($(this).attr('data-jasa') ),
					urutan = parseInt($(this).parent().parent().parent().data('nomor') ),
					isi = parseInt($(this).val() ) || 0;
					material_in += material * isi;
					jasa_in += jasa * isi;

					tfoot.find('.hrg_material').text(material_in.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
					tfoot.find('.hrg_jasa').text(jasa_in.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
					tfoot.find('.sum_me').text((material_in + jasa_in).toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
				})
			})
		})

		$('#pekerjaan, #tahun, #bulan').on('change', function(){
			var material = [];

			$.each($('.ini_material'), function(k, v){
				material.push(...$(this).val())
			})

			if(material.length){
				$.ajax({
					url: "/get_ajx/find_material",
					type:"GET",
					data: {
						isi : null,
						pekerjaan : $('#pekerjaan').val(),
						tahun : $('#tahun').val(),
						bulan : $('#bulan').val(),
						jenis : 'all',
						witel : {!! json_encode(session('auth')->Witel_New) !!},
					},
					dataType: 'json',
					success: (function(data){
						design = data;
						$.each(material, function(k, v){
							var chck = $.grep(data, function(element, index){return parseInt(element.id) == v });

							if($.isEmptyObject(chck))
							{
								$(".ini_material option[value='"+v+"']").remove();
							}
						})
					})
				});
			}
		});

		var get_jenis = {!! json_encode($jenis ?? '') !!};

		if(get_jenis.length != 0){
			var data_pbu = {!! json_encode($last_data_work ?? '') !!};

			pekerjaan = data_pbu.pekerjaan,
			jenis_kontrak = data_pbu.jenis_kontrak,
			jenis_work = data_pbu.jenis_work,
			mitra_id = data_pbu.mitra_id,
			mitra_nm = data_pbu.mitra_nm,
			judul = data_pbu.judul;
			toc_edit = data_pbu.toc;
			tahun_kerja = data_pbu.bulan_pengerjaan.split('-');

			$('#pekerjaan').val(pekerjaan).change();
			$('#jenis_work').val(jenis_work).trigger('change');
			$('#mitra_id').append(new Option(mitra_nm, mitra_id, true, true) ).trigger('change');

			var judul_load = {!! json_encode(Request::old('judul') ) !!};

			$('#judul').val(judul_load ?? judul);
			$('#jenis_kontrak').val(jenis_kontrak).change();
			$('#tahun').val(tahun_kerja[0]).change();
			$('#bulan').val(tahun_kerja[1]).change();
			$('#toc_edit').val(toc_edit);

			$('#jenis_work').on('change', function(){
				var sub = $(this).val();
				if(sub){
					$.ajax({
						url:"/get_ajx/check_sub_pekerjaan",
						type:"GET",
						data: {
							pekerjaan : sub
						},
						dataType: 'json',
						success: (function(data){
							sub_unit = data;

							$(".sub_jenis").empty().trigger('change')

							$('.sub_jenis').each(function() {
								$(this).select2({
									width: '100%',
									data: sub_unit,
									placeholder: 'Sub Pekerjaan Bisa Dipilih'
								});
							});
						})
					})
				}
			})
		}

		var pekerjaan = {!! json_encode(Request::old('pekerjaan') ) !!};
		jenis_kontrak = {!! json_encode(Request::old('jenis_kontrak') ) !!},
		tahun = {!! json_encode(Request::old('tahun') ) !!},
		bulan = {!! json_encode(Request::old('bulan') ) !!},
		toc_edit = {!! json_encode(Request::old('toc_edit') ) !!},
		jenis_po = {!! json_encode(Request::old('jenis_po') ) !!};

		var arr_load = [
			{dom : "select[name='pekerjaan']", val: pekerjaan},
			{dom : "select[name='jenis_work']", val: jenis_work},
			{dom : "select[name='jenis_kontrak']", val: jenis_kontrak},
			{dom : "select[name='tahun']", val: tahun},
			{dom : "select[name='bulan']", val: bulan},
			{dom : "select[name='toc_edit']", val: toc_edit},
			{dom : "select[name='jenis_po']", val: jenis_po}
		]

		$.each(arr_load, function(k, v){
			if(v.val){
				$(v.dom).val(v.val).change();
			}
		});

		var title_generate;
		// construction
		// Penarikan Fiber Optic Paket 1 STO Feederisasi & OSP Tahap-2 Tahun-2022 - STO BBR, Batch-2 WITEL KALSEL PT. TATA BERLIAN NUSANTARA
		//QE
		// PENGADAAN PEKERJAAN ALL QE AKSES UPATEK STO ULI 07 2022 WITEL KALSEL
		var mitra_data = {!! json_encode($mitra) !!}

		$('.generate_title').on('change', function(){
			var sub = $('#jenis_work').val(),
			tahun = $('#tahun').val(),
			bulan = $('#bulan').val(),
			batch = $('#batch').val(),
			witel = {!! json_encode(session('auth')->Witel_New) !!},
			mitra = $('select[id="mitra_id"]').text(),
			pekerjaan = '',
			sub_ = '',
			collect_sto = {},
			data_mitra = '';

			var alias = $.grep(mitra_data, function(v){ return v.text == mitra});
			data_mitra = alias[0];

			$.each($('.sto_m'), function(k, v){
				var sto = $(this).val();
				collect_sto[sto] = sto;
			});

			switch (sub) {
				case 'STTF Tahap-1':
					sub_ += ' & OSP Tahap-1';
				break;
				case 'STTF Tahap-2':
					sub_ += ' & OSP Tahap-2';
				break;
				case 'STTF Tahap-3':
					sub_ += ' & OSP Tahap-3';
				break;
				case 'STTF Tahap-4':
					sub_ += ' & OSP Tahap-4';
				break;
				case 'QE Akses':
					sub_ += ' ALL QE Akses';
				break;
				default:
					sub_ += `${(sub ?? '')}`;
				break;
			}

			collect_sto = Object.keys(collect_sto)

			if($('#pekerjaan').val() == 'Construction'){
				pekerjaan = '';

				pekerjaan += `Penarikan Fiber Optic Paket ${collect_sto.length} STO`;

				pekerjaan += ` Tahun-${tahun} - STO ${collect_sto.join(', ')} & ${sub_} Batch ${batch ?? ''} Witel ${witel} ${mitra ? mitra.replace('PT ', 'PT. ') : ''}`;

			}else if($('#pekerjaan').val() == 'QE'){
				pekerjaan = '';

				pekerjaan = 'Pengadaan Pekerjaan';

				pekerjaan += ` ${sub_} ${data_mitra ? data_mitra.alias_nama_company : ''} STO ${collect_sto.join(', ')} ${bulan} ${tahun} Witel ${witel ?? ''}`;
			}

			$('#judul').val(pekerjaan);
		});

  });
</script>
@endsection