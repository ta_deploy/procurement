@extends('layout')
@section('title', 'Setting WBS Pekerjaan')
@section('headerS')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" rel="stylesheet" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.css" integrity="sha512-6S2HWzVFxruDlZxI3sXOZZ4/eJ8AcxkQH1+JjSe/ONCEqR9L4Ysq5JdT5ipqtzU7WHalNwzwBv+iE51gNHJNqQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<link rel="stylesheet" href="/css/dataTables.bootstrap4.css">
<style type="text/css">
	.pull-right {
		text-align: right;
	}

  .toggleCard{
		display: block;
	}
  .card-body{
		display: block;
	}

	.field_material_input{
		width: 60px!important;
		height: 32px;
	}

	@media (min-width: 768px) {
		.modal-xl {
			width: 100%;
			max-width:1200px;
		}
	}
</style>
@endsection
@section('content')
@if (Session::has('alerts'))
	@foreach(Session::get('alerts') as $alert)
		<div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
	@endforeach
@endif
<div class="modal fade" id="detail_be" tabindex="-1" role="dialog" aria-labelledby="judul_mdl" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body data_mdl" style=" overflow: hidden;">
        <form class="form-horizontal form-label-right" method="POST" action="/Admin/sp/budgetP/{{ $data->id }}">
					<div class="col-md-12">
						<section class="widget">
							<header>
								<h4>
									<i class="fa fa-check-square-o"></i>
									Form PID Bermasalah
								</h4>
							</header>
							<fieldset>
								{{ csrf_field() }}
								<div class="form-group">
									<label class="col-form-label col-md-2" for="pid_be">PID:</label>
									<div class="col-md-12">
										<select class="form-control pid_be" id="pid_be" multiple name="pid_be[]" required>
											@foreach ($get_pid as $v)
												<option value="{{ $v->id }}">{{ $v->pid }}</option>
											@endforeach
										</select>
									</div>
								</div>
								<input type="hidden" id="BE" name="status_pid" value="Salah PID">
								<div class="form-group">
									<label class="col-form-label col-md-2" for="detail">Catatan:</label>
									<div class="col-md-12">
										<textarea style="resize:none; font-weight: bold" cols='50' rows="2" required class="form-control input-transparent" id="detail" name="detail"></textarea>
									</div>
								</div>
                <div class="form-actions">
                  <div class="row">
                    <div class="col-md-12">
                      <button type="submit" class="btn btn-block btn-danger">Submit PID</button>
                    </div>
                  </div>
                </div>
							</fieldset>
						</section>
					</div>
				</form>
			</div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal_edit_material" tabindex="-1" role="dialog" aria-labelledby="modal_edit_materialTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
				<h5 class="modal-title" id="defaultModalLabel">List Material Yang Dipilih</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body data_mdl" style=" overflow: hidden;">
			</div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-md-12">
			<h2 class="page-title">Request PID</h2>
			<div class="card-deck" style="display: block">
				<form id="submit_lampiran_boq" class="row" method="post">
					{{ csrf_field() }}
					<div class="col-md-12">
						<div class="card shadow mb-4">
							<div class="card-body">
								<div class="form-group row">
									<label class="col-form-label col-md-2 pull-right" for="pekerjaan">Pekerjaan</label>
									<div class="col-md-10">
										<select name="pekerjaan" class="form-control input-transparent" id="pekerjaan" required>
											@foreach ($kerjaan[0] as $val)
												<option value="{{ $val->id }}">{{ $val->text }}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-md-2 pull-right" for="jenis_work">Jenis Pekerjaan</label>
									<div class="col-md-5">
										<select name="jenis_work" class="form-control input-transparent" id="jenis_work" required>
										</select>
									</div>
									<div class="col-md-5">
										<select name="jenis_kontrak" class="form-control input-transparent" id="jenis_kontrak" required>
											<option value="Kontrak Putus">Kontrak Putus</option>
											<option value="Kontrak Tetap">Kontrak Tetap</option>
										</select>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-md-2 pull-right" for="mitra_id">Mitra</label>
									<div class="col-md-10">
										<select id="mitra_id" name="mitra_id" required></select>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-md-2 pull-right" for="judul">Judul</label>
									<div class="col-md-10">
										<input type="text" class="form-control input-transparent" name="judul" id="judul" required>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-md-2 pull-right">Pengerjaan</label>
									<div class="col-md-5">
										<select id="tahun" name="tahun" required>
											@for ($i = date('Y'); $i >= date('Y', strtotime('-1 year') ); $i--)
												<option value="{{ $i }}">{{ $i }}</option>
											@endfor
										</select>
									</div>
									<div class="col-md-5">
										<select id="bulan" name="bulan" required>
											<option value="01">Januari</option>
											<option value="02">Februari</option>
											<option value="03">Maret</option>
											<option value="04">April</option>
											<option value="05">Mei</option>
											<option value="06">Juni</option>
											<option value="07">Juli</option>
											<option value="08">Agustus</option>
											<option value="09">September</option>
											<option value="10">Oktober</option>
											<option value="11">November</option>
											<option value="12">Desember</option>
										</select>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-md-2 pull-right" for="material_ta">Material TA</label>
									<div class="col-md-10">
										<select id="material_ta" name="material_ta" data-design_id="16" class="ini_material" multiple></select>
										<code><b>*Material sesuai yang dipakai di RFC</b></code>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-md-2 pull-right" for="material_mitra">Material Mitra</label>
									<div class="col-md-10">
										<select id="material_mitra" name="material_mitra" data-design_id="0" class="ini_material" multiple></select>
										<code><b>*Material sesuai yang dipakai dan tidak tertera di RFC</b></code><br/>
										<a type="button" class="btn add_material btn-secondary" style="margin-top: 7px; color: white"><i class="fe fe-plus fe-16"></i>&nbsp;Update material</a>
										<a type="button" class="btn edit_material btn-primary" data-toggle="modal" data-target="#modal_edit_material" style="margin-top: 7px; color: white"><i class="fe fe-tool fe-16"></i>&nbsp;Atur Material</a>
									</div>
								</div>
								{{-- <div class="form-group row">
									<label class="col-form-label col-md-2 pull-right" for="jumlah_klm">Tambah Kolom</label>
									<div class="col-md-10">
										<select id="jumlah_klm" name="jumlah_klm">
											@for ($i = 1; $i <= 10; $i++)
												<option value="{{ $i }}">{{ $i }}</option>
											@endfor
										</select>
										<a type="button" class="btn add_boq btn-info" style="margin-top: 7px; color: white"><i class="fe fe-tool fe-16"></i>&nbsp;Kolom</a>
									</div>
								</div> --}}
								<div class="row">
									<div class="col-md-4">
										<div class="form-group">
											<label class="col-form-label" for="sto">Total Material:</label>
											<input type="text" class="form-control" disabled id="total_mat">
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label class="col-form-label" for="sto">Total Jasa:</label>
											<input type="text" class="form-control" disabled id="total_jasa">
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label class="col-form-label" for="sto">Grand Total:</label>
											<input type="text" class="form-control" disabled id="total_grand">
										</div>
									</div>
								</div>
								<input type="hidden" name="material_input" value="{{ Request::old('material_input') }}">
								<input type="hidden" name="lokasi_input">
								<input type="hidden" name="jenis_btn">
								<div class="form-group mb-3 row">
									<div class="custom-file col-md-6">
										<button type="submit" data-jenis_btn="submit" class="btn btn-block submit_btn btn-success" style="color: white"><i class="fe fe-check fe-16"></i>&nbsp;submit</button>
									</div>
									<div class="custom-file col-md-6">
										<a class="btn btn-danger btn-block" type="button" data-toggle="modal" data-target="#detail_be" style="color: white;"><i class="fe fe-alert-triangle fe-16"></i>&nbsp;Nomor PID Salah</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
			<ul class="nav nav-tabs mb-3" id="myTab" role="tablist">
			</ul>
			<div id="myTabContentLeft" class="tab-content">
			</div>
		</div>
	</div>
</div>
@endsection
@section('footerS')
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src='/js/jquery.dataTables.min.js'></script>
<script src='/js/dataTables.bootstrap4.min.js'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.js" integrity="sha512-lbwH47l/tPXJYG9AcFNoJaTMhGvYWhVM9YI43CT+uteTRRaiLCui8snIgyAN8XWgNjNhCqlAUdzZptso6OCoFQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script type="text/javascript">
	$(function(){
		var currentTab,
		already_open_tab = [];
		//initilize tabs
		$("#myTab").on("click", "a", function (e) {

			var href_data = $(this).attr('href');

			if(already_open_tab.indexOf(href_data.split('#')[1]) === -1){
				var get_tab_data = $.grep(load_data_html, function(e){ return e.tabId == href_data.split('#')[1]; });
				if(get_tab_data.length != 0){
					// showTab(href_data.split('#')[1]);
					registerCloseEvent();

					var key_object = (parseInt(get_tab_data[0].tabId.replace( /^\D+/g, '') ) - 1),
					data_lop = last_used_design[key_object][0],
					table_temp = '',
					lokasi_load = data_lop.lokasi,
					id_lokk = data_lop.id_boq_lokasi,
					sto_load = data_lop.sto,
					sub_jenis_load = data_lop.sub_jenis_p;

					table_temp += "<div class='col-md-12'>";
					table_temp += "<div class='card shadow mb-4'>";
					table_temp += "<div class='card-header'>";
					table_temp += "<strong class='card-title nomor_kol'>Kolom Nomor "+ terbilang(parseInt(get_tab_data[0].tabId.replace( /^\D+/g, '') ) ) +"</strong>";
					// table_temp += "<a class='float-left toggleCard minimize' href='#!' style='color:#17a2b8; text-decoration:none;'>▲</a>";
					table_temp += "</div>";
					table_temp += "<div class='card-body table-responsive'>";
					table_temp += "<select data-no_lokasi=" + parseInt(get_tab_data[0].tabId.replace( /^\D+/g, '') ) + " data-id_lokasi=" + id_lokk + " class='form-control sel_pid' multiple></select>&nbsp;";
					table_temp += "<input type='text' class='form-control lok_per input-transparent' name='lokasi_pekerjaan[]' placeholder='Silahkan Isi Lokasi' required value='"+lokasi_load+"'>&nbsp;";
					table_temp += "<select class='sto_m' name='sto[]'  required>";
					$.each(sto_i, function(k, v) {
						table_temp += "<option value='"+ v.kode_area +"' "+ (sto_load == v.kode_area ? "selected" : '') +">"+ v.kode_area +"</option>"
					})
					table_temp += "</select>&nbsp;";
					table_temp += "<select class='sub_jenis' name='sub_jenis[]' required>";
					$.each(sub_jenis_i, function(k, v) {
						table_temp += "<option value='"+ v.sub_jenis +"' "+ (sub_jenis_load == v.sub_jenis ? "selected" : '') +">"+ v.sub_jenis +"</option>"
					})
					table_temp += "</select>&nbsp;";
					table_temp += "<table class='table table_material table-striped table-bordered table-hover' style='width:100%'>";
					table_temp += "<thead class='thead-dark'>";
					table_temp += "<tr>";
					table_temp += "<th rowspan='2'>Designator</th>";
					table_temp += "<th rowspan='2'>Uraian</th>";
					table_temp += "<th rowspan='2'>Jenis Material</th>";
					table_temp += "<th colspan='2'>Paket 7</th>";
					table_temp += "<th rowspan='2'>SP</th>";
					table_temp += "</tr>";
					table_temp += "<tr>";
					table_temp += "<th>Material</th>";
					table_temp += "<th>Jasa</th>";
					table_temp += "</tr>";
					table_temp += "</thead>";
					table_temp += "<tbody data-lokasi_id='"+data_lop.id_boq_lokasi+"' data-nomor='"+parseInt(get_tab_data[0].tabId.replace( /^\D+/g, '') )+"'>";

					var total_material = 0,
					total_jasa = 0;

					// v.sort(function(a, b) {
					// 	return a.designator > b.designator
					// });

					$.each(load_material, function(k1, v1){
						var val = $.grep(design, function(e){ return e.id == v1.id; }),
						get_data_qty = $.grep(last_used_design[key_object], function(e){ return e.id_design == v1.id && e.design_mitra_id == v1.design_mitra_id; });

						var nilai_sp = get_data_qty[0] ? get_data_qty[0].sp : 0;

						val[0].material_load = v1.mat || 0;
						val[0].jasa_load = v1.jasa || 0;

						table_temp += "<tr data-id='"+v1.id+"' data-id_khs='"+v1.id+"_"+v1.design_mitra_id+"'>";
						table_temp += "<td>"+val[0].designator+"</td>";

						var word = val[0].uraian.split(' '),
						text = '';

						if(word.length > 6){
							text = "...<a style='color:blue; cursor:pointer' class='copy_detail_tool' data-toggle='tooltip' title='"+val[0].uraian+"'>Lihat Detail</a>";
						}

						table_temp += "<td>"+word.slice(0, 6).join(' ') + text +"</td>";
						table_temp += "<td>"+(parseInt(v1.design_mitra_id) == 16 ? 'Telkom Akses' : 'Mitra')+"</td>";
						table_temp += "<td>"+(v1.mat || 0).toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".")+"</td>";

						total_material += (v1.mat * parseInt(nilai_sp) || 0 );
						total_jasa += (v1.jasa * parseInt(nilai_sp) || 0 );

						material_tot += (v1.mat * parseInt(nilai_sp) || 0 );
						jasa_tot += (v1.jasa * parseInt(nilai_sp) || 0 );

						table_temp += "<td>"+(v1.jasa || 0).toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".")+"</td>";
						table_temp += "<td><input type='text' style='width:80px;' class='form-control number input-transparent required field_material_input' data-nomor_input='"+parseInt(get_tab_data[0].tabId.replace( /^\D+/g, '') )+"' data-id_mat='"+v1.id+"' data-material='"+v1.mat+"' data-jasa='"+v1.jasa+"' placeholder='0' data-design_mitra_id="+v1.design_mitra_id+" value='"+ (nilai_sp != 0 ? nilai_sp : '') +"'></td>";
						table_temp += "</tr>";
					})

					table_temp += "</tbody>";
					table_temp += "<tfoot>";
					table_temp += "<tr data-nomor_foot='"+parseInt(get_tab_data[0].tabId.replace( /^\D+/g, '') )+"'>";
					table_temp += "<td colspan='3'>Total Harga</td>";
					table_temp += "<td class='hrg_material'>" + total_material.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") + "</td>";
					table_temp += "<td class='hrg_jasa'>" + total_jasa.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") + "</td>";
					table_temp += "<td class='sum_me'>" + (total_material + total_jasa).toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") + "</td>";
					table_temp += "</tr>";
					table_temp += "</tfoot>";
					table_temp += "</table>";
					table_temp += "</div>";
					table_temp += "</div>";
					table_temp += "</div>";

					$(href_data).html(table_temp);

					var material_in = 0,
					jasa_in = 0;

					$('.field_material_input').each(function(){
						var id_material = parseInt($(this).data('id_mat') ),
						material = parseInt($(this).attr('data-material') ) || 0,
						jasa = parseInt($(this).attr('data-jasa') ) || 0,
						urutan = parseInt($(this).parent().parent().parent().data('nomor') ),
						isi = parseInt($(this).val() ) || 0;
						material_in += material * isi;
						jasa_in += jasa * isi;
						// console.log(material, jasa);
						$('#total_mat').val(material_in.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
						$('#total_jasa').val(jasa_in.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
						$('#total_grand').val((material_in + jasa_in).toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
					});

					$('.lok_per').on("keypress keyup",function (e){
						return $(this).val($(this).val().toUpperCase());
					});

					$('.sub_jenis').each(function() {
						$(this).select2({
							width: '100%',
							placeholder: 'Sub Pekerjaan Bisa Dipilih'
						});
					});

					$('.sto_m').each(function() {
						$(this).select2({
							width: '100%',
							placeholder: 'Sto Bisa Dipilh'
						});
					});

					$('.sel_pid').each(function() {
						var lokasi_id = $(this).data('id_lokasi'),
						list_all_pid = {!! json_encode($pid) !!}
						data = [],
						lokasinya = {!! json_encode($save_pid_per_lok) !!};

						if(lokasi_id == id_lokk){
							if(lokasinya[lokasi_id]){
								$.each(list_all_pid, function(k, v){
									data.push({
										id: v,
										text: v
									});
								});
							}

							$(this).select2({
								width: '100%',
								placeholder: "Ketik pid",
								theme: 'bootstrap4',
								allowClear: true,
								data: data
							});

							if(lokasinya[lokasi_id]){
								$(this).val(lokasinya[lokasi_id]).change()
							}
						}
					});

					already_open_tab.push(href_data.split('#')[1]);
				}
			}
			e.preventDefault();
			$(this).tab('show');
			$currentTab = $(this);
		});
		//this method will demonstrate how to add tab dynamically
		var no_tbl = 0,
		sto_i = {!! json_encode($all_sto) !!};

		function registerComposeButtonEvent() {
			/* just for this demo */
			$('.add_boq').on('click', function(e){
				e.preventDefault();
				var jml = $('#jumlah_klm').val();
				// craeteNewTabAndLoadUrl("", "./SamplePage.html", "#" + tabId);

				for (let a = 1; a <= jml; a++) {
					++no_tbl;
					table_temp = '';
					var tabId = "compose" + no_tbl;

					$('.nav-tabs').append('<li><a href="#' + tabId + '" class="nav-link" data-toggle="tab">LOP Nomor <span class="badge badge-primary"">'+no_tbl+'</span><button type="button" class="close closeTab">×</button></a></li>');
					$('.tab-content').append('<div class="tab-pane" id="' + tabId + '"></div>');

					table_temp += "<div class='col-md-12'>";
					table_temp += "<div class='card shadow mb-4'>";
					table_temp += "<div class='card-header'>";
					table_temp += "<strong class='card-title nomor_kol'>Kolom Nomor "+ terbilang(no_tbl) +"</strong>";
					table_temp += "</div>";
					table_temp += "<div class='card-body table-responsive'>";
					table_temp += "<select data-no_lokasi=" + no_tbl + " data-id_lokasi='lokasi_baru' class='form-control sel_pid' multiple></select>&nbsp;";
					table_temp += "<input type='text' class='form-control lok_per input-transparent' name='lokasi_pekerjaan[]' placeholder='Silahkan Isi Lokasi' required>&nbsp;";
					table_temp += "<select class='sto_m' name='sto[]' required multiple>";
					$.each(sto_i, function(k, v) {
						table_temp += "<option value='"+ v.kode_area +"'>"+ v.kode_area +"</option>"
					});
					table_temp += "</select>&nbsp;";
					table_temp += "<table class='table table_material table-striped table-bordered table-hover' style='width:100%'>";
					table_temp += "<thead class='thead-dark'>";
					table_temp += "<tr>";
					table_temp += "<th rowspan='2'>Designator</th>";
					table_temp += "<th rowspan='2'>Uraian</th>";
					table_temp += "<th colspan='2'>Paket 7</th>";
					table_temp += "<th rowspan='2'>SP</th>";
					table_temp += "</tr>";
					table_temp += "<tr>";
					table_temp += "<th>Material</th>";
					table_temp += "<th>Jasa</th>";
					table_temp += "</tr>";
					table_temp += "</thead>";
					table_temp += "<tbody data-nomor="+no_tbl+">";

					if(load_material.length){
						$.each(load_material, function(key2, val2) {
								$.each(design, function(key, val) {
								if(val.id == val2.id){
									table_temp += "<tr data-lokasi_id= 'NEW' data-id="+val.id+">";
									table_temp += "<td>"+val.designator+"</td>";
									table_temp += "<td>"+val.uraian+"</td>";
									table_temp += "<td>"+val2.mat.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".")+"</td>";
									table_temp += "<td>"+val2.jasa.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".")+"</td>";
									table_temp += "<td><input type='text' style='width:80px;' class='form-control number input-transparent required field_material_input' data-nomor_input='"+tbl_load+"' data-id_mat='"+val.id+"' data-material='"+val2.mat+"' data-jasa="+val2.jasa+" data-design_mitra_id="+val2.design_mitra_id+"></td>";
									table_temp += "</tr>";
								}
							});
						});
					}

					table_temp += "</tbody>";
					table_temp += "<tfoot>";
					table_temp += "<tr class='foooter_sum'>";
					table_temp += "<td colspan='2'>Total Harga</td>";
					table_temp += "<td class='hrg_material'>0</td>";
					table_temp += "<td class='hrg_jasa'>0</td>";
					table_temp += "<td class='sum_me'>0</td>";
					table_temp += "</tr>";
					table_temp += "</tfoot>";
					table_temp += "</table>";
					table_temp += "</div>";
					table_temp += "</div>";
					table_temp += "</div>";

					showTab(tabId);
					registerCloseEvent();
					$(`#${tabId}`).html(table_temp);
				}

				$('.lok_per').on("keypress keyup",function (e){
					return $(this).val($(this).val().toUpperCase());
				});

				$('.sto_m').each(function() {
					$(this).select2({
						width: '100%',
						placeholder: 'Sto Bisa Dipilh'
					});
				});

				toastr.options = {
					"closeButton": false,
					"debug": false,
					"newestOnTop": false,
					"progressBar": true,
					"positionClass": "toast-top-center",
					"preventDuplicates": false,
					"onclick": null,
					"showDuration": "300",
					"hideDuration": "1000",
					"timeOut": "5000",
					"extendedTimeOut": "1000",
					"showEasing": "swing",
					"hideEasing": "linear",
					"showMethod": "fadeIn",
					"hideMethod": "fadeOut"
				}
				toastr["success"]("Jumlah Kolom Sudah Ditambahkan!", "Sukses")

				$('.sel_pid').each(function() {
					list_all_pid = {!! json_encode($pid) !!}
					data = []
					$.each(list_all_pid, function(k, v){
						data.push({
							id: v,
							text: v
						});
					});
					$(this).select2({
						width: '100%',
						placeholder: "Ketik pid",
						theme: 'bootstrap4',
						allowClear: true,
						data: data
					});
				});
			});
		}
		//this method will register event on close icon on the tab..
		function registerCloseEvent() {
			$(".closeTab").click(function () {
				var this_me = $(this),
				text = $(this).parent().text().substring(0, $(this).parent().text().length - 1);

				Swal.fire({
					title: `Apakah Kamu Ingin Menghapus ${text}`,
					icon: 'warning',
					showDenyButton: true,
					confirmButtonText: 'Ya, Hapus!',
					denyButtonText: `Jangan!`,
				}).then((result) => {
					/* Read more about isConfirmed, isDenied below */
					if (result.isConfirmed) {
						//there are multiple elements which has .closeTab icon so close the tab whose close icon is clicked
						var tabContentId = this_me.parent().attr("href");
						this_me.parent().parent().remove(); //remove li of tab
						$('#myTab a:last').click(); // Select first tab
						$(tabContentId).remove(); //remove respective tab content
					} else if (result.isDenied) {
						Swal.fire(`${text} Tidak Dihapus!`, '', 'info')
					}
				})
			});
		}
		//shows the tab with passed content div id..paramter tabid indicates the div where the content resides
		function showTab(tabId) {
			$('#myTab a[href="#' + tabId + '"]').tab('show');
		}
		//return current active tab
		function getCurrentTab() {
			return currentTab;
		}
		//This function will create a new tab here and it will load the url content in tab content div.
		// function craeteNewTabAndLoadUrl(parms, url, loadDivSelector) {
		// 	$("" + loadDivSelector).load(url, function (response, status, xhr) {
		// 		if (status == "error") {
		// 			var msg = "Sorry but there was an error getting details ! ";
		// 			$("" + loadDivSelector).html(msg + xhr.status + " " + xhr.statusText);
		// 			$("" + loadDivSelector).html("Load Ajax Content Here...");
		// 		}
		// 	});
		// }
		//this will return element from current tab
		//example : if there are two tabs having  textarea with same id or same class name then when $("#someId") whill return both the text area from both tabs
		//to take care this situation we need get the element from current tab.
		function getElement(selector) {
			var tabContentId = $currentTab.attr("href");
			return $("" + tabContentId).find("" + selector);
		}

		function removeCurrentTab() {
			var tabContentId = $currentTab.attr("href");
			$currentTab.parent().remove(); //remove li of tab
			$('#myTab a:last').tab('show'); // Select first tab
			$(tabContentId).remove(); //remove respective tab content
		}

		registerComposeButtonEvent();
		registerCloseEvent();

		$("body").tooltip({ selector: '[data-toggle=tooltip]' });

		$('#tahun, #bulan').select2({
			width: '100%'
		})

		$(document).on('keyup', '.number', function(event){
			if(event.which >= 37 && event.which <= 40) return;
			$(this).val(function(index, value) {
        return value.replace(/\D/g, "");
      });
		})

		$(document).on('click', '.toggleCard', function(){
			$(this).html((i, t) => t === '▲' ? '▼' : '▲');
			$(this).parent().next('.card-body').slideToggle();
		})

		$(document).on('click', '.delete_buatan', function(){
			$(this).parent().parent().parent().remove();

			var material_tot_chck = 0,
			jasa_tot_chck = 0;

			$('.field_material_input').each(function(){
				var tfoot = $(this).parent().parent().parent().next(),
				body = $(this).parent().parent().parent();
				// console.log(tfoot, body);
				var material_in = 0,
				jasa_in = 0;
				body.each(function(){
					$(this).find('.field_material_input').each(function(){
						var id_material = parseInt($(this).data('id_mat') ),
						material = parseInt($(this).attr('data-material') ) || 0,
						jasa = parseInt($(this).attr('data-jasa') ) || 0,
						urutan = parseInt($(this).parent().parent().parent().data('nomor') ),
						isi_material = parseInt($(this).val() ) || 0;
						material_in += material * isi_material;
						jasa_in += jasa * isi_material;
						tfoot.find('.hrg_material').text(material_in.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
						tfoot.find('.hrg_jasa').text(jasa_in.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
						tfoot.find('.sum_me').text((material_in + jasa_in).toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
					})
				});

				var id_material = parseInt($(this).data('id_mat') ),
				material = parseInt($(this).attr('data-material') ) || 0,
				jasa = parseInt($(this).attr('data-jasa') ) || 0,
				urutan = parseInt($(this).parent().parent().parent().data('nomor') ),
				isi = parseInt($(this).val() ) || 0;
				material_tot_chck += material * isi;
				jasa_tot_chck += jasa * isi;
			});

			$('#total_mat').val(material_tot_chck.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
			$('#total_jasa').val(jasa_tot_chck.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
			$('#total_grand').val((material_tot_chck + jasa_tot_chck).toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
		});

		// $('select[id="mitra_id"]').on('select2:select', function(){
		// 	var data = $("#material").select2("data"),
		// 	isi = $(this).val();

		// 	// $.each(data, function(key, val){
		// 	// 	if(val.id_mitra != 16 && isi != val.id_mitra){
		// 	// 		val.element.remove()
		// 	// 	}
		// 	// });
		// });

		$(document).on('click', '.copy_detail_tool', function(){
			var data = $(this).data('original-title');
			var $temp = $("<input>");
			$("body").append($temp);
			$temp.val(data).select();
			document.execCommand("copy");
			$temp.remove();

			toastr.options = {
				"debug": false,
				"newestOnTop": false,
				"progressBar": true,
				"positionClass": "toast-top-center",
				"preventDuplicates": false,
				"onclick": null,
				"showDuration": "300",
				"hideDuration": "1000",
				"timeOut": "3000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut"
			}
			toastr["success"]("Designator Berhasil Disalin!", "Sukses")
		});

		$('select[id="mitra_id"]').select2({
			width: '100%',
			placeholder: "Masukkan Nama Perusahaan Mitra",
			allowClear: true,
			ajax: {
				url: "/get_ajx/mitra/search/per_witel",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						searchTerm: params.term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true,
			},
		});

		$('#pekerjaan').val(null).change();

		$('#pekerjaan').select2({
			width: '100%',
			placeholder: 'Isi jenis pekerjaan',
		});

		$('#jumlah_klm').select2({
			width: '100%',
		});

		$('#jenis_kontrak').select2({
			width: '100%',
			placeholder: 'Isi jenis Kontrak',
		});

		var design = {!! json_encode($design) !!},
		isi_design = [],
		isi;

		$.each(design, function(key, val) {
			isi_design.push({
				id: val.id, text: val.designator
			})
		});

		$('#pekerjaan').on('change', function(){

			var jenis_kerja = {!! json_encode($kerjaan[1]) !!},
			kerja = $(this).val(),
			cari = [];
			$.each(jenis_kerja, function(key, val) {
				if(val.pekerjaan == kerja){
					cari.push({id: val.id, text: val.text})
				}
			});
			// console.log(cari)
			$("#jenis_work").empty().trigger('change')

			$('#jenis_work').select2({
				width: '100%',
				data: cari
			})
		})

		$('#tb_rekon').DataTable({
			autoWidth: true,
			lengthMenu: [
				[16, 32, 64, -1],
				[16, 32, 64, "All"]
			]
		});

		function terbilang(nilai)
    {
			var hasil;
			if (nilai < 0){
				hasil = "minus " + penyebut(nilai);
			}else{
				hasil = penyebut(nilai);
			}

			return hasil;
    }

    function penyebut(nilai)
    {
			var nilai = Math.abs(Math.floor(nilai)),
			huruf = ["", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas"],
			temp;
			if(nilai < 12){
				temp = " " + huruf[nilai];
			}else if(nilai < 20){
				temp = penyebut(nilai - 10) + " belas";
			}else if(nilai < 100){
				temp = penyebut(nilai / 10) + " puluh" + penyebut(nilai % 10);
			} else if (nilai < 200) {
				temp = " seratus" + penyebut(nilai - 100);
			} else if (nilai < 1000) {
				temp = penyebut(nilai / 100) + " ratus" + penyebut(nilai % 100);
			}
			return temp;
    }

		var load_material = [];

		function add_material(mm){
			var td_material = '';
			sukses_filter = [];

			$.each(mm, function(k, v){
				var value = $(this).val(),
				jenis_khs = parseInt($(this).data('design_id'));
				var material = [];
				var isi_load = value;

				if(isi_load.length){
					var filter_lm = [];
					$.each(load_material, function(k, vx){
						if(vx.design_mitra_id == jenis_khs){
							filter_lm.push(vx);
							sukses_filter.push(vx);
						}
					})

					var diff = filter_lm.filter(o1 => !isi_load.some(o2 => o1.id === parseInt(o2) ) );

					if(diff){
						$.each(diff, function(key, val){
							$("tr[data-id_khs='" + val.id +"_"+val.design_mitra_id+"']").remove();
						});
					}
					//ini kalau ada materialnya
					var intersection = filter_lm.filter(o1 => isi_load.some(o2 => o1.id === parseInt(o2) ) );
					var remove_dup = isi_load.filter(o1 => !intersection.some(o2 => parseInt(o1) === o2.id) );
					material = remove_dup;
				}

				$.each(material, function(key2, val2) {
					$.each(design, function(key, val) {
						if(val.id == val2){
							var loop_me = true;

							if(jenis_khs == 16){
								var material_new = 0,
								material_text = 0;
							}else{
								var material_new = val.material,
								material_text = val.material.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".");
							}

							if(last_used_design.length != 0){
								var tbl_m = $(".table_material").find('tbody');
								$.each(last_used_design, function(kx, vx){
									$.each(vx, function(kk, vv){
										if(vv.id_design == val2){
											td_material += "<tr data-id='"+vv.id_design+"' data-id_khs='"+vv.id_design+"_"+jenis_khs+"'>";
											td_material += "<td>"+val.designator+"</td>";

											var word = val.uraian.split(' '),
											text = '';

											if(word.length > 6){
												text = "...<a style='color:blue; cursor:pointer' class='copy_detail_tool' data-toggle='tooltip' title='"+val.uraian+"'>Lihat Detail</a>";
											}

											td_material += "<td>"+word.slice(0, 6).join(' ') + text +"</td>";
											td_material += "<td>"+(jenis_khs == 0 ? 'Mitra' : 'Telkom Akses')+"</td>";
											td_material += "<td>"+material_text+"</td>";
											td_material += "<td>"+val.jasa.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".")+"</td>";
											td_material += "<td><input type='text' style='width:80px;' class='form-control number input-transparent required field_material_input' data-nomor_input='"+tbl_load+"' data-id_mat='"+val.id+"' data-material='"+material_new+"' data-sp='"+vv.sp+"' data-jasa='"+val.jasa+"' placeholder='0' data-design_mitra_id="+jenis_khs+" value='"+ (vv.sp != 0 ? vv.sp : '') +"'></td>";
											td_material += "</tr>";
											loop_me = false;
										}
									});

									// if($(".table_material").length){
									// 	var data = [];
									// 	tbl_m.each(function(key, val){
									// 		data[key] = [];
									// 		$(this).children('td').each(function(key2, val2){
									// 			data[key][key2] = $(this).text();
									// 		});
									// 	});
									// 	tbl_m.eq(0).append(td_material)
									// 	tbl_m = tbl_m.slice(1);
									// 	td_material = '';
									// }
									if($(".table_material").length){
										// var data = [];
										// $(".table_material tr").each(function(key, val){
										// 	data[key] = [];
										// 	$(this).children('td').each(function(key2, val2){
										// 		data[key][key2] = $(this).text();
										// 	});
										// });
										tbl_m.eq(0).append(td_material)
										tbl_m = tbl_m.slice(1);
										td_material = '';
									}
								});
							}

							if(loop_me){
								td_material += "<tr data-id="+val.id+" data-id_khs='"+val.id+"_"+jenis_khs+"'>";
								td_material += "<td>"+val.designator+"</td>";

								var word = val.uraian.split(' '),
								text = '';

								if(word.length > 6){
									text = "...<a style='color:blue; cursor:pointer' class='copy_detail_tool' data-toggle='tooltip' title='"+val.uraian+"'>Lihat Detail</a>";
								}

								td_material += "<td>"+word.slice(0, 6).join(' ') + text +"</td>";
								td_material += "<td>"+(jenis_khs == 0 ? 'Mitra' : 'Telkom Akses')+"</td>";
								td_material += "<td>"+material_text+"</td>";
								td_material += "<td>"+val.jasa.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".")+"</td>";
								td_material += "<td><input type='text' style='width:80px;' class='form-control number input-transparent required field_material_input' data-nomor_input='"+tbl_load+"' data-id_mat="+val.id+" data-material="+material_new+" data-jasa="+val.jasa+" data-design_mitra_id="+jenis_khs+"></td>";
								td_material += "</tr>";

								if($(".table_material").length){
									// var data = [];
									// $(".table_material tr").each(function(key, val){
									// 	data[key] = [];
									// 	$(this).children('td').each(function(key2, val2){
									// 		data[key][key2] = $(this).text();
									// 	});
									// });
									$(".table_material").find('tbody').append(td_material)
									td_material = '';
								}
							}
						}
					});
				});
			});

			var diff = load_material.filter(o1 => !sukses_filter.some(o2 => o1.id + '_' + o1.design_mitra_id === o2.id + '_' + o2.design_mitra_id ) );
			if(diff){
				$.each(diff, function(key, val){
					$("tr[data-id_khs='" + val.id +"_"+val.design_mitra_id+"']").remove();
				});
			}

			load_material = [];

			$.each(mm, function(k, v){
				var value = $(this).val(),
				jenis_khs = parseInt($(this).data('design_id'));
				var isi_load = value;

				$.each(isi_load, function(key2, val2) {
					$.each(design, function(key, val) {
						if(val2 == val.id){
							if(jenis_khs == 16){
								var material_new = 0;
							}else{
								var material_new = (val.material_load !== undefined ? val.material_load : val.material);
							}

							load_material.push({
								id: val.id,
								mat: material_new,
								jasa: (val.jasa_load !== undefined ? val.jasa_load : val.jasa),
								design_mitra_id: jenis_khs
							});
						}
					});
				});
			});

			// console.log(load_material, material)
			var material_tot_chck = 0,
			jasa_tot_chck = 0;

			$('.field_material_input').each(function(){
					var tfoot = $(this).parent().parent().parent().next(),
					body = $(this).parent().parent().parent();
					// console.log(tfoot, body);
					var material_in = 0,
					jasa_in = 0,
					material_sp = 0,
					jasa_sp = 0;
					body.each(function(){
						$(this).find('.field_material_input').each(function(){
							var id_material = parseInt($(this).data('id_mat') ),
							material = parseInt($(this).attr('data-material') ) || 0,
							jasa = parseInt($(this).attr('data-jasa') ) || 0,
							urutan = parseInt($(this).parent().parent().parent().data('nomor') ),
							isi_material = parseInt($(this).val() ) || 0;
							material_in += material * isi_material;
							jasa_in += jasa * isi_material;

							tfoot.find('.hrg_material').text(material_in.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
							tfoot.find('.hrg_jasa').text(jasa_in.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
							tfoot.find('.sum_me').text((material_in + jasa_in).toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
						})
					});

					var id_material = parseInt($(this).data('id_mat') ),
					material = parseInt($(this).attr('data-material') ) || 0,
					jasa = parseInt($(this).attr('data-jasa') ) || 0,
					urutan = parseInt($(this).parent().parent().parent().data('nomor') ),
					isi = parseInt($(this).val() ) || 0;
					material_tot_chck += material * isi;
					jasa_tot_chck += jasa * isi;
				});

				if($('.field_material_input').length == 0 ){
					$('.hrg_material').text(0);
					$('.hrg_jasa').text(0);
					$('.sum_me').text(0);
				}

				$('#total_mat').val(material_tot_chck.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
				$('#total_jasa').val(jasa_tot_chck.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
				$('#total_grand').val((material_tot_chck + jasa_tot_chck).toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
		}

		$('.add_material').on('click', function(){
			add_material($('.ini_material') );

			toastr.options = {
				"closeButton": false,
				"debug": false,
				"newestOnTop": false,
				"progressBar": true,
				"positionClass": "toast-top-center",
				"preventDuplicates": false,
				"onclick": null,
				"showDuration": "300",
				"hideDuration": "1000",
				"timeOut": "5000",
				"extendedTimeOut": "1000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut"
			}
			toastr["success"]("Material Berhasil Diubah!", "Sukses")
		})

		$('#modal_edit_material').on('show.bs.modal', function(){
			table_temp = ''
			table_temp += "<table class='table table_material table-striped table-bordered table-hover' style='width:100%'>";
			table_temp += "<thead class='thead-dark'>";
			table_temp += "<tr>";
			table_temp += "<th rowspan='2'>Designator</th>";
			table_temp += "<th rowspan='2'>Uraian</th>";
			table_temp += "<th colspan='3'>Paket 7</th>";
			table_temp += "</tr>";
			table_temp += "<tr>";
			table_temp += "<th>Jenis Material</th>";
			table_temp += "<th>Material</th>";
			table_temp += "<th>Jasa</th>";
			table_temp += "</tr>";
			table_temp += "</thead>";
			table_temp += "<tbody data-nomor="+no_tbl+">";

			if(load_material.length){
				console.log(load_material)
				$.each(load_material, function(key2, val2) {
					$.each(design, function(key, val) {
						if(val.id == val2.id){
							table_temp += "<tr data-id="+val.id+" data-id_khs='"+val.id+"_"+val.design_mitra_id+"'>";
							table_temp += "<td>"+val.designator+"</td>";
							table_temp += "<td>"+val.uraian+"</td>";
							table_temp += "<td>"+(parseInt(val2.design_mitra_id) == 16 ? 'Telkom Akses' : 'Mitra')+"</td>";
							// table_temp += "<td><input type='checkbox' class='check_khs' data-id="+val.id+" " + (val2.design_mitra_id == 16 ? 'checked' : '' ) + "></td>";
							table_temp += "<td><input type='checkbox' class='check_mat' data-jenis_khs='"+val2.design_mitra_id+"' data-jenis='material' data-id="+val.id+" data-val_harga=" + (parseInt(val.material) || 0) + " " + (val2.mat != 0 ? 'checked' : '' ) + "> &nbsp;"+val.material.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".")+"</td>";
							table_temp += "<td><input type='checkbox' class='check_mat' data-jenis_khs='"+val2.design_mitra_id+"' data-jenis='jasa' data-id="+val.id+" data-val_harga=" + (parseInt(val.jasa) || 0) + " " + (val2.jasa != 0 ? 'checked' : '' ) + "> &nbsp;"+val.jasa.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".")+"</td>";
							table_temp += "</tr>";
						}
					});
				});
			}

			table_temp += "</tbody>";
			table_temp += "</table>";

			$('.data_mdl').html(table_temp)

			$('.check_mat').on('change', function(){
				var jenis = $(this).data('jenis'),
				val = parseInt($(this).data('val_harga') ) || 0;
				id = $(this).data('id'),
				jenis_khs = $(this).data('jenis_khs'),
				isi = 0;

				if($(this).is(':checked') ){
					isi = val;
				}

				$.each(load_material, function(k, v){
					if(v.id == id && jenis_khs == v.design_mitra_id){
						var val = $.grep(design, function(e){ return e.id == v.id; });
						if(jenis == 'material'){
							load_material[k].mat = isi;
							val[0].material_load = isi;
						}

						if(jenis == 'jasa'){
							load_material[k].jasa = isi;
							val[0].jasa_load = isi;
						}
					}
				})

				$('.field_material_input').each(function(){
					var id_material = parseInt($(this).data('id_mat') ),
					material_input = parseInt($(this).attr('data-material') ) || 0,
					design_mitra_id = parseInt($(this).attr('data-design_mitra_id') ),
					jasa_input = parseInt($(this).attr('data-jasa') ) || 0;

					if(id_material == id && design_mitra_id == jenis_khs)
					{
						if(jenis == 'material'){
							$(this).attr('data-material', isi);
							$(this).parent().prev().prev().text(isi.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") );
						}
						if(jenis == 'jasa'){
							$(this).attr('data-jasa', isi);
							$(this).parent().prev().text(isi.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") );
						}
					}
				});

				var material_tot_chck = 0,
				jasa_tot_chck = 0;

				$('.field_material_input').each(function(){
					var tfoot = $(this).parent().parent().parent().next(),
					body = $(this).parent().parent().parent();
					// console.log(tfoot, body);
					var material_in = 0,
					jasa_in = 0;
					body.each(function(){
						$(this).find('.field_material_input').each(function(){
							var id_material = parseInt($(this).data('id_mat') ),
							material = parseInt($(this).attr('data-material') ) || 0,
							jasa = parseInt($(this).attr('data-jasa') ) || 0,
							urutan = parseInt($(this).parent().parent().parent().data('nomor') ),
							isi_material = parseInt($(this).val() ) || 0;
							material_in += material * isi_material;
							jasa_in += jasa * isi_material;
							tfoot.find('.hrg_material').text(material_in.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
							tfoot.find('.hrg_jasa').text(jasa_in.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
							tfoot.find('.sum_me').text((material_in + jasa_in).toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
						})
					});

					var id_material = parseInt($(this).data('id_mat') ),
					material = parseInt($(this).attr('data-material') ) || 0,
					jasa = parseInt($(this).attr('data-jasa') ) || 0,
					urutan = parseInt($(this).parent().parent().parent().data('nomor') ),
					isi = parseInt($(this).val() ) || 0;
					material_tot_chck += material * isi;
					jasa_tot_chck += jasa * isi;
				});

				$('#total_mat').val(material_tot_chck.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
				$('#total_jasa').val(jasa_tot_chck.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
				$('#total_grand').val((material_tot_chck + jasa_tot_chck).toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
			});

			// $('.check_khs').on('change', function(){
			// 	var isi = 0,
			// 	id = $(this).data('id');

			// 	if($(this).is(':checked') ){
			// 		isi = 16;
			// 	}

			// 	$.each(load_material, function(k, v){
			// 		if(v.id == id){
			// 			load_material[k].design_mitra_id = isi;
			// 		}
			// 	})

			// 	$('.field_material_input').each(function(){
			// 		var id_material = parseInt($(this).data('id_mat') );

			// 		if(id_material == id)
			// 		{
			// 			$(this).attr('data-design_mitra_id', isi);
			// 		}
			// 	})
			// });
		});

		var no_tbl = 0,
		sto_i = {!! json_encode($all_sto) !!},
		load_material_ada = {!! json_encode($data_material) !!},
		last_used_design = {!! json_encode($data_item) !!},
		data_pbu = {!! json_encode($data_pbu) !!},
		last_stp_id = last_used_design[0][0].step_id,
		material_before = $("input[name='material_input']").val();

		if(material_before){
			material_before = JSON.parse(material_before)
			var final_ls = [],
			material_new = [];

			$.each(material_before, function(k, v){
				if(material_new.map(x => x.id_renew).indexOf(v.id_design +'_'+ v.design_mitra_id) === -1){
					var val = $.grep(design, function(e){ return e.id == v.id_design; })

					material_new.push({
						id_renew: v.id_design +'_'+ v.design_mitra_id,
						design_mitra_id: v.design_mitra_id,
						designator: val[0].designator,
						id_design: v.id_design,
						jasa: v.jasa,
						jenis: v.jenis,
						jenis_khs: v.jenis_khs,
						material: v.material,
						namcomp: v.namcomp,
						rekon: v.rekon,
						sp: v.sp,
						uraian: val[0].uraian,
					});
				}

				if(typeof(final_ls[v.urutan]) === 'undefined'){
					final_ls[v.urutan] = [];
				}

				final_ls[v.urutan].push(v);
			})

			load_mat = final_ls.filter(function (e) {return e != null;});
			last_used_design = load_mat

			load_mat_all = material_new.filter(function (e) {return e != null;});
			load_material_ada = load_mat_all
		}

		var sub_jenis_i = {!! json_encode($sub_jenis_all) !!};

		$.each(load_material_ada, function(k1, v1){
			var val = $.grep(design, function(e){ return e.id == v1.id_design; });

			if(v1.design_mitra_id == 16){
				$("#material_ta").append('<option data-id="'+ v1.id_design +'" data-id_mitra="' +v1.design_mitra_id+ '" value="'+ v1.id_design +'" selected>'+  val[0].designator +' (' + v1.jenis + ' | ' + v1.namcomp + ')</option>').change();
			}else{
				$("#material_mitra").append('<option data-id="'+ v1.id_design +'" data-id_mitra="' +v1.design_mitra_id+ '" value="'+ v1.id_design +'" selected>'+  val[0].designator +' (' + v1.jenis + ' | ' + v1.namcomp + ')</option>').change();
			}
		});

		if(last_used_design.length != 0){
			var tbl_load = 0,
			load_data_html = [],
			material_tot = 0,
			jasa_tot = 0;

			$.each(last_used_design, function(k, v){
				table_temp = ''
				tbl_load = ++k;

				add_material($('.ini_material') );

				var tabId = "compose" + tbl_load,
				delete_btn = '';

				if(v[0].lokasi.length == 0){
					delete_btn = '<button type="button" class="close closeTab">×</button>';
				}

				$('.nav-tabs').append('<li><a href="#' + tabId + '" class="nav-link" data-toggle="tab">LOP Nomor <span class="badge badge-primary"">'+tbl_load+'</span>'+delete_btn+'</a></li>');
				$('.tab-content').append('<div class="tab-pane" id="' + tabId + '"></div>');

				if(load_data_html.map(x => x.tabId).indexOf(tabId) === -1){
					load_data_html.push({
						'tabId': tabId,
					});
				}
			})

			no_tbl += tbl_load;

			$('#myTab a[href="#compose1"]').click()

			add_material($('.ini_material') );

			$('#total_mat').val(material_tot.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
			$('#total_jasa').val(jasa_tot.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
			$('#total_grand').val((material_tot + jasa_tot).toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
		}

		$('.sel_pid').each(function() {
			list_all_pid = {!! json_encode($pid) !!}
			data = []
			$.each(list_all_pid, function(k, v){
				data.push({
					id: v,
					text: v
				});
			});

			$(this).select2({
				width: '100%',
				placeholder: "Ketik pid",
				theme: 'bootstrap4',
				allowClear: true,
				data: data
			});
		});

		$('.submit_btn').on('click', function(e){

			$('#myTab a').each(function(k, v){
				var attr_href = $(this).attr('href').split('#')[1];

				if($.inArray(attr_href, already_open_tab) == -1 ){
					$("#myTab a[href='#"+attr_href+"']").click()
				}
			})
			// e.preventDefault()
			$.each($('.lok_per'), function(k, v){
				if($(this).val().trim().length == 0){
					e.preventDefault();
					$(this).css({
						'border-color': 'red'
					});
					toastr.options = {
						"closeButton": false,
						"debug": false,
						"newestOnTop": false,
						"progressBar": true,
						"positionClass": "toast-top-center",
						"preventDuplicates": false,
						"onclick": null,
						"showDuration": "300",
						"hideDuration": "1000",
						"timeOut": "5000",
						"extendedTimeOut": "1000",
						"showEasing": "swing",
						"hideEasing": "linear",
						"showMethod": "fadeIn",
						"hideMethod": "fadeOut"
					}
					toastr["warning"]("Lokasi Tidak Boleh Kosong!", "Gagal")
				}
			})
			var submit = [],
			ls = [],
			lokasi_pid = [];

			$('.sel_pid').each(function(){
				var nomor = $(this).data('no_lokasi'),
				id = $(this).data('id_lokasi');
				lokasi_pid.push({
					no_lok: nomor,
					id_lok: id,
					pid: $(this).val()
				});
			});

			$('.field_material_input').each(function(){
				var id_material = parseInt($(this).data('id_mat') ),
				material = parseInt($(this).attr('data-material') ) || 0,
				jasa = parseInt($(this).attr('data-jasa') ) || 0,
				urutan = parseInt($(this).parent().parent().parent().data('nomor') ),
				jenis_khs = parseInt($(this).attr('data-design_mitra_id') ),
				lokasi = $(this).parent().parent().parent().parent().parent().parent().find('.lok_per').val(),
				id_lokasi_lok = $(this).parent().parent().parent().data('lokasi_id'),
				sto = $(this).parent().parent().parent().parent().parent().find('.sto_m').val(),
				sub_jenis = $(this).parent().parent().parent().parent().parent().find('.sub_jenis').val(),
				isi = parseInt($(this).val() ) || 0;

				var get_design = design.find(o => o.id == id_material);

				submit.push({
					id: id_material,
					material: material,
					jasa: jasa,
					val: isi,
					urutan: urutan,
					jenis_khs: jenis_khs,
					lokasi: lokasi,
					id_lokasi: id_lokasi_lok,
					sto: sto,
					sub_jenis: sub_jenis,
					id_design: id_material,
					// designator: get_design.designator,
					namcomp: (jenis_khs ? 'Telkom Akses' : 'Mitra'),
					sp: isi,
					rekon: 0,
					tambah: 0,
					kurang: 0,
					design_mitra_id: jenis_khs,
					id_boq_lokasi: id_lokasi_lok,
					jenis: get_design.jenis,
				});
			});

			let hasil = JSON.stringify(submit);

			let hasil2 = JSON.stringify(lokasi_pid)
			$("input[name='material_input']").val(hasil)
			$("input[name='lokasi_input']").val(hasil2)
			var jenis_btn= $(this).data('jenis_btn');
			$("input[name='jenis_btn']").val(jenis_btn)

		})

		$(document).on('keyup', '.field_material_input', function(event){
			var material_in = 0,
			jasa_in = 0;
			$('.field_material_input').each(function(){
				var id_material = parseInt($(this).data('id_mat') ),
				material = parseInt($(this).attr('data-material') ) || 0,
				jasa = parseInt($(this).attr('data-jasa') ) || 0,
				urutan = parseInt($(this).parent().parent().parent().data('nomor') ),
				isi = parseInt($(this).val() ) || 0;
				material_in += material * isi;
				jasa_in += jasa * isi;
				// console.log(material, jasa);
				$('#total_mat').val(material_in.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
				$('#total_jasa').val(jasa_in.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
				$('#total_grand').val((material_in + jasa_in).toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
			});

			var tfoot = $(this).parent().parent().parent().next(),
			body = $(this).parent().parent().parent();
			// console.log(tfoot, body);
			var material_in = 0,
			jasa_in = 0;
			body.each(function(){
				$(this).find('.field_material_input').each(function(){
					var id_material = parseInt($(this).data('id_mat') ),
					material = parseInt($(this).attr('data-material') ) || 0,
					jasa = parseInt($(this).attr('data-jasa') ) || 0,
					urutan = parseInt($(this).parent().parent().parent().data('nomor') ),
					isi = parseInt($(this).val() ) || 0;
					material_in += material * isi;
					jasa_in += jasa * isi;

					tfoot.find('.hrg_material').text(material_in.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
					tfoot.find('.hrg_jasa').text(jasa_in.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
					tfoot.find('.sum_me').text((material_in + jasa_in).toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
				})
			});
		});

		var pekerjaan = data_pbu.pekerjaan,
		jenis_work = data_pbu.jenis_work,
		jenis_kontrak = data_pbu.jenis_kontrak,
		mitra_id = data_pbu.mitra_id,
		mitra_nm = data_pbu.mitra_nm,
		judul = data_pbu.judul;
		tahun_kerja = data_pbu.bulan_pengerjaan.split('-');
		// console.log(jenis_work)
		$('#pekerjaan').val(pekerjaan).change();
		$('#jenis_work').append(new Option(jenis_work, jenis_work, true, true) ).trigger('change');
		$('#mitra_id').append(new Option(mitra_nm, mitra_id, true, true) ).trigger('change');
		$('#judul').val(judul).change();
		$('#jenis_kontrak').val(jenis_kontrak).change();
		$('#tahun').val(tahun_kerja[0]).change();
		$('#bulan').val(tahun_kerja[1]).change();
		// var sub_unit;

		// $.ajax({
		// 	url:"/get_ajx/check_sub_pekerjaan",
		// 	type:"GET",
		// 	data: {
		// 		pekerjaan : jenis_work
		// 	},
		// 	dataType: 'json',
		// 	success: (function(data){
		// 		sub_unit = data;

		// 		$(".sub_jenis").empty().trigger('change')

		// 		$('.sub_jenis').each(function() {
		// 			$(this).select2({
		// 				width: '100%',
		// 				data: sub_unit,
		// 				placeholder: 'Sub Pekerjaan Bisa Dipilih'
		// 			});
		// 		});
		// 	})
		// })

		$('#pekerjaan, #jenis_work, #mitra_id, #judul, #jenis_kontrak, #tahun, #bulan').prop( "disabled", true );

		$('.pid_be').select2({
			width: '100%',
			placeholder: 'Masukkan PID OverBudget',
			allowClear: true
		});

		$('#material_ta').select2({
			width: '100%',
			placeholder: "Masukkan Nama Material",
			allowClear: true,
			ajax: {
				url: "/get_ajx/find_material",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					isi = params.term;
					return {
						searchTerm: params.term,
						pekerjaan : $('#pekerjaan').val(),
						tahun : $('#tahun').val(),
						bulan : $('#bulan').val(),
						jenis : 'all',
						witel : {!! json_encode(session('auth')->Witel_New) !!},
					};
				},
				processResults: function (response) {
					if(isi == null && response.length != 0){
						design = $.grep(design, function(element, index){return $.inArray(parseInt(element.design_mitra_id), [0, 16]) !== -1 }, true);
						design.push(...response);
					}
					return {
						results: response
					};
				},
				cache: true,
			},
			// language: {
			// 	noResults: function(){
			// 		return "Tidak Menemukan Material? Buat Material New Item</b>&nbsp;<a type='button' class='btn btn-info btn-sm' href='/Report/material_manual/input' target='_blank'><span class='fe fe-plus-circle fe-16'></span> Tambah No PKS Baru</a>";
			// 	}
			// },
			escapeMarkup: function (markup) {
				return markup;
			},
			templateSelection: function(data) {
				var $result = $(
					"<span data-id=" +data.id+ " data-id_mitra=" +data.id_mitra+ ">" + data.text + "</span>"
				);
				return $result;
			}
		});

		$('#material_mitra').select2({
			width: '100%',
			placeholder: "Masukkan Nama Material",
			allowClear: true,
			ajax: {
				url: "/get_ajx/find_material",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					isi = params.term;
					return {
						searchTerm: params.term,
						pekerjaan : $('#pekerjaan').val(),
						tahun : $('#tahun').val(),
						bulan : $('#bulan').val(),
						jenis : 'all',
						witel : {!! json_encode(session('auth')->Witel_New) !!},
					};
				},
				processResults: function (response) {
					if(isi == null && response.length != 0){
						design = $.grep(design, function(element, index){return $.inArray(parseInt(element.design_mitra_id), [0, 16]) !== -1 }, true);
						design.push(...response);
					}
					return {
						results: response
					};
				},
				cache: true,
			},
			// language: {
			// 	noResults: function(){
			// 		return "Tidak Menemukan Material? Buat Material New Item</b>&nbsp;<a type='button' class='btn btn-info btn-sm' href='/Report/material_manual/input' target='_blank'><span class='fe fe-plus-circle fe-16'></span> Tambah No PKS Baru</a>";
			// 	}
			// },
			escapeMarkup: function (markup) {
				return markup;
			},
			templateSelection: function(data) {
				var $result = $(
					"<span data-id=" +data.id+ " data-id_mitra=" +data.id_mitra+ ">" + data.text + "</span>"
				);
				return $result;
			}
		});
  });
</script>
@endsection