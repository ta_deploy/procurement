@extends('layout')
@section('title', 'Upload Justifikasi')
@section('headerS')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" rel="stylesheet" />
<link rel="stylesheet" href="/css/dataTables.bootstrap4.css">
<style type="text/css">
	.pull-right {
		text-align: right;
	}
</style>
@endsection
@section('content')
@if (Session::has('alerts'))
	@foreach(Session::get('alerts') as $alert)
		<div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
	@endforeach
@endif
<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-md-12">
			<h2 class="page-title">Upload Justifikasi</h2>
			<div class="card-deck" style="display: block">
				<div class="col-md-12">
					<form id="form_req_pid" class="row" method="post" enctype="multipart/form-data">
						{{ csrf_field() }}
						<div class="card shadow mb-4">
							<div class="card-body">
								<div class="form-group row">
									<label class="col-form-label col-md-2 pull-right" for="pid">PID</label>
									<div class="col-md-10">
										<textarea class="form-control" id="pid_mod" name="pid_mod" required style="resize: none;" rows="3" disabled>{{ $data->id_project }}</textarea>
												<code>Jika PID Tidak Sesuai Bisa Setting <a type="button" href="/opt/setting/wbs/{{ Request::segment(2) }}" class="btn btn-sm btn-primary" style="color: #fff"><i class="fe fe-tool fe-16"></i>&nbsp;Setting WBS</a></code>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-md-2 pull-right" for="upload_justifikasi">Download Justifikasi</label>
									<div class="col-md-10">
										<a class="pull-right btn btn-info btn-sm rar_down" href="/download/full_rar/{{ $data->id }}"><i class="fe fe-download fe-16"></i>&nbsp;Download Justifikasi</a>
									</div>
								</div>
								<div class="form-group row">
                  <label class="col-form-label col-md-2 pull-right" for="waspang_id">Waspang TTD</label>
                  <div class="col-md-10">
                    <select name="waspang_id" class="form-control input-transparent" id="waspang_id">
											@foreach ($find_job_dok as $val)
												<option value="{{ $val->id }}">{{ $val->user }} ({{ $val->nik }})</option>
											@endforeach
										</select>
                  </div>
                </div>
								<div class="form-group row">
									<label class="col-form-label col-md-2 pull-right" for="upload_justifikasi">Upload Justifikasi</label>
									<div class="col-md-10">
										<input type="file" id="upload_justifikasi" required name="upload_justifikasi" accept="application/pdf" class="form-control-file">
									</div>
								</div>
								<div class="form-group mb-3">
									<div class="custom-file">
										<button type="submit" class="btn btn-block btn_submit btn-primary"><i class="fe fe-download fe-16"></i>&nbsp;Upload Justifikasi</button>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('footerS')
<script src='/js/select2.min.js'></script>
<script type="text/javascript">
	$(function(){
		$('#waspang_id').select2({
			width: '100%',
			placeholder: 'Silahkan Pilih Waspang'
		});
	})
</script>
@endsection