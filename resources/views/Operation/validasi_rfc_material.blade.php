@extends('layout')
@section('title', 'Verifikasi Rekon')
@section('headerS')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" rel="stylesheet" />
<link rel="stylesheet" href="/css/dataTables.bootstrap4.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.css" integrity="sha512-6S2HWzVFxruDlZxI3sXOZZ4/eJ8AcxkQH1+JjSe/ONCEqR9L4Ysq5JdT5ipqtzU7WHalNwzwBv+iE51gNHJNqQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />

<style type="text/css">
	.pull-right {
		text-align: right;
	}

  .toggleCard{
		display: block;
	}
  .card-body{
		display: block;
	}

	.field_material_input{
		width: 60px!important;
		height: 32px;
	}

	@media (min-width: 768px) {
		.modal-xl {
			width: 100%;
			max-width:1200px;
		}
	}
</style>
@endsection
@section('content')
@if (Session::has('alerts'))
	@foreach(Session::get('alerts') as $alert)
		<div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
	@endforeach
@endif
<div class="modal fade" id="modal_check_rfc" tabindex="-1" role="dialog" aria-labelledby="modal_check_rfcTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
				<a class="btn btn-info dwn_rfc" style="color: #fff" href="#"><i class="fe fe-download"></i>&nbsp;PDF</a>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style=" overflow: hidden;">
				<form class="row" method="post">
					{{ csrf_field() }}
					<input type="hidden" name="rfc_id" id="rfc_id">
					<div class="col-md-4">
						<div class="form-group row">
							<label class="col-form-label col-md-4 pull-right" for="wbs_element">PID:</label>
							<div class="col-md-8">
								<p class="form-control input-transparent" readonly><b class="wbs_element"></b></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-md-4 pull-right" for="no_rfc">Nomor RFC:</label>
							<div class="col-md-8">
								<p class="form-control input-transparent" readonly><b class="no_rfc"></b></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-md-4 pull-right" for="plant">Gudang:</label>
							<div class="col-md-8">
								<p class="form-control input-transparent" readonly><b class="plant"></b></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-md-4 pull-right" for="type">Jenis:</label>
							<div class="col-md-8">
								<p class="form-control input-transparent" readonly><b class="type"></b></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-md-4 pull-right" for="mitra">Mitra:</label>
							<div class="col-md-8">
								<textarea class="form-control input-transparent mitra" rows="2" readonly></textarea>
							</div>
						</div>
					</div>
					<div class="col-md-8">
						<div class="table_rfc_material">
						</div>
					</div>
					<div class="col-md-12 submit_btn_rfc">
						<div class="form-group mb-3">
							<div class="custom-file">
								<button type="submit" class="btn btn-block btn-primary">Submit Nomor RFC</button>
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
			</div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal_edit_material" tabindex="-1" role="dialog" aria-labelledby="modal_edit_materialTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
				<h5 class="modal-title" id="defaultModalLabel">List Material Yang Dipilih</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body data_mdl" style=" overflow: hidden;">
			</div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal_copas_material" tabindex="-1" role="dialog" aria-labelledby="modal_copas_materialTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
				<h5 class="modal-title" id="defaultModalLabel">Table Material</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
			<a type="button" class="btn btn-info btn-sm copy_table" style="color: white; float: right"><i class="fe fe-alert-copy fe-16"></i>&nbsp;Copy Table!</a>
      <div class="modal-body data_copas_tbl" style=" overflow: hidden;">
			</div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="submit_design_rfc" tabindex="-1" role="dialog" aria-labelledby="judul_mdl" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style=" overflow: hidden;">
        <form class="form-horizontal form-label-right" method="POST" action="/opt/update_rfc/material/{{ Request::segment(2) }}">
					<div class="col-md-12">
						<section class="widget">
							<header>
								<h4>
									<i class="fa fa-check-square-o"></i>
									Tambah Designator Untuk <span id="judul_rfc"></span>
								</h4>
							</header>
							<fieldset>
								<div class="form-group">
                  {{ csrf_field() }}
									<input type="hidden" name="dessign_rfc" id="dessign_rfc">
									<input type="hidden" name="id_pbu" id="id_pbu" value="{{ Request::segment(2) }}">
									<label class="col-form-label col-md-2" for="material_rfc">Material:</label>
									<div class="col-md-12">
										<select name="material_rfc[]" class="form-control input-transparent" id="material_rfc" multiple required>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-form-label col-md-2" for="qty_per_item">Jumlah Per Item</label>
									<div class="col-md-12">
										<input type="text" class="form-control input-transparent qty_per_item" name="qty_per_item" id="qty_per_item" placeholder="1">
									</div>
								</div>
								<div class="form-group">
									<label class="col-form-label col-md-2" for="ket_rfc">Keterangan</label>
									<div class="col-md-12">
										<textarea type="text" class="form-control input-transparent ket_rfc" name="ket_rfc" id="ket_rfc" disabled></textarea>
									</div>
								</div>
								<code id="ket_kecil"></code>
								<input type="hidden" id="tot_kecil">
                <div class="form-actions">
                  <div class="row">
                    <div class="col-md-12">
                      <button type="submit" class="btn btn-block btn-danger">Submit Designator</button>
                    </div>
                  </div>
                </div>
							</fieldset>
						</section>
					</div>
				</form>
			</div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-md-12">
			@if($count_download != 0)
			<h2 class="page-title"><a class='btn btn-info dwn_rfc' style='color: #fff' href='/download/all_rfc/{{ $data_sp->id }}'><i class='fe fe-download'></i>&nbsp;Semua PDF</a>
			</h2>
			@endif
			<div class="card shadow mb-4">
				<div class="card-header">
					<strong class="card-title">History RFC</strong>
				</div>
				<div class="card-body table-responsive">
					<table id ="tb_rekon" class="table table-striped table-bordered table-hover">
						<thead class="thead-dark">
							<tr>
								<th class="hidden-xs">#</th>
								<th>Nomor RFC</th>
								<th>status RFC</th>
								<th>Jenis</th>
								<th>Mitra</th>
								<th>Created At</th>
								<th>Created By</th>
								<th>Tanggal Gudang</th>
								<th>status</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							@php
								$no = 0
							@endphp
              @forelse ($history_rfc as $key => $val)
                <tr>
                  <td>{{ ++$no }}</td>
                  <td>{{ $val->no_rfc }}</td>
                  <td>{{ $val->status_rfc }}</td>
                  <td>{{ ($val->type == 'Out' ? 'Pengeluaran' : 'Pengembalian') }}</td>
                  <td>{{ $val->mitra }}</td>
                  <td>{{ $val->created_at }}</td>
                  <td>{{ $val->nama }} ({{ $val->created_by }})</td>
                  <td>{{ $val->posting_datex }}</td>
									<td>{!! ($val->download != 'kdd' ? "<a class='btn btn-sm btn-info dwn_rfc' style='color: #fff' href='/download/rfc/$val->id_upload/$val->no_rfc'><i class='fe fe-download'></i>&nbsp;Download PDF</a>" : "" ) !!}</td>
                  <td>
										<a class="btn btn-sm btn-info lihat_detail_rfc" data-rfc="{{ $val->no_rfc }}" style="color: white; margin-bottom: 5px;"><i class="fe fe-eye fe-16"></i>&nbsp;Lihat Detail</a>
										<a class="btn btn-sm btn-danger delete" data-rfc="{{ $val->no_rfc }}" href="/delete/rfc/pr/{{ $val->id }}" style="color: white; margin-bottom: 5px;"><i class="fe fe-trash fe-16"></i>&nbsp;Delete RFC</a>
									</td>
                </tr>
							@empty
              @endforelse
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="col-md-12">
			@if(count($tanpa_rfc) )
				<div class="alert alert-danger">Ada {{ count($tanpa_rfc) }} Buah Material Tanpa RFC, Tetapi Ada RFR</div>
				<ul>
					@foreach($tanpa_rfc as $v)
						<li><div class="alert alert-warning">{{ $v }}</div></li>
					@endforeach
				</ul>
			@endif
			<div class="card shadow mb-4">
				<div class="card-header">
					<strong class="card-title">List RFC</strong>
				</div>
				<div class="card-body table-responsive">
					<table id ="tb_pid_l" class="table table-striped table-bordered" style="width: 100%">
						<thead class="thead-dark">
							<tr>
								<th class="hidden-xs">#</th>
								<th>ID Material</th>
								<th>Total Material</th>
								<th>NO. RFC/GI</th>
								<th>Status</th>
								<th>Volume Diberikan</th>
								<th>Volume Pemakaian</th>
								<th>Total Pemakaian</th>
								<th>NO. RFC/Pengembalian</th>
								<th>Status</th>
								<th>Volume Dikembalian</th>
								<th>Sisa Volume</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							@php
								$no = 0;
							@endphp
							@foreach($rfc as $key => $val)
								@php
									$first = true;
								@endphp
								@foreach ($val['isi_m'] as $k2 => $valc1_1)
									@php
										$second = true;
										$no_row = 0;
										array_map(function($x) use(&$no_row){
											$no_row += !isset($x['return']) ? 1 : count($x['return']);
										}, $val['isi_m']);
									@endphp
									@foreach($valc1_1 as $kkk => $valc1)
										@if ($kkk =='out')
											<tr>
												@if ($first == true)
													<td rowspan="{{ $no_row }}">{{ ++$no }}</td>
													<td rowspan="{{ $no_row }}">{{ $key }}</td>
													<td rowspan="{{ $no_row }}">{{ $val['total_m'] }}</td>
												@endif
												@php
													$hitung_row_c = !isset($valc1_1['return']) ? 1 : count($valc1_1['return']);
												@endphp
												@if ($second == true)
													<td rowspan="{{ $hitung_row_c }}">{{ $valc1['nomor_rfc_gi'] }}</td>
													<td rowspan="{{ $hitung_row_c }}" style="background-color: {{ @$valc1['status_rfc_gi'] == 'LURUS' ? '' : 'red' }}; color: {{ @$valc1['status_rfc_gi'] == 'LURUS' ? '' : 'black' }}">{{ $valc1['status_rfc_gi'] }}</td>
													<td rowspan="{{ $hitung_row_c }}">{{ $valc1['vol_give'] }}</td>
													<td rowspan="{{ $hitung_row_c }}">
														<a href="#" id="edit_{{ $valc1['id_pro'] }}" class="edit_used_rfc" data-type="text" data-pk="{'id_pro': {{ $valc1['id_pro'] }}, 'id_upload': {{ Request::segment(2) }} }" data-url="/submit_rfc_used" data-title="Masukkan Nomor Material Yang Dipakai">{{ ($valc1['use'] != 0 ? $valc1['use'] : '' ) }}</a>
													</td>
												@endif
												@if ($first == true)
													<td rowspan="{{ $no_row }}">{{ $val['total_use'] }}</td>
												@endif
												@if(isset($valc1_1['return']) )
													<td>{{ current($valc1_1['return'])['nomor_rfc_return'] ?? '-' }}</td>
													<td style="background-color: {{ @current($valc1_1['return'])['status_rfc_return'] == 'TIDAK LURUS' ? 'red' : '' }}; color: {{ @current($valc1_1['return'])['status_rfc_return'] == 'TIDAK LURUS' ? 'black' : '' }}">{{ current($valc1_1['return'])['status_rfc_return'] ?? '-' }}</td>
													<td>{{ current($valc1_1['return'])['return'] ?? '-' }}</td>
													@if ($second == true)
														@php
															$used_rfc = 0;

															if(isset($valc1_1['return']) )
															{
																$r = 0;
																foreach($valc1_1['return'] as $kx => $vx)
																{
																	$r += $vx['return'];
																}

																$used_rfc = $valc1['vol_give'] - $r;
															}
														@endphp
														<td rowspan="{{ $hitung_row_c }}">{{ $used_rfc }}</td>
														@php
															$second = false;
														@endphp
													@endif
												@else
													<td>-</td>
													<td>-</td>
													<td>-</td>
													<td>0</td>
												@endif
												@if ($first == true)
													<td rowspan="{{ $no_row }}">
														<a data-toggle="modal" data-rfc="{{ $valc1['nomor_rfc_gi'] }}" data-design="{{ $key }}" data-total_design="{{ $val['total_m'] }}" data-target="#submit_design_rfc" class="btn btn-sm edit_rfc_design btn-warning" style="color: white;"><i class="fe fe-tool fe-16"></i>&nbsp;Edit</a>
													</td>
													@php
														$first = false;
													@endphp
												@endif
											</tr>
										@else
											@php
												$new_valc1 = $valc1;
												array_shift($new_valc1);
											@endphp
											@foreach (@$new_valc1 as $k4 => $v4)
												<tr>
													<td>{{ $v4['nomor_rfc_return'] ?? '-' }}</td>
													<td style="background-color: {{ @$v4['status_rfc_return'] == 'TIDAK LURUS' ? 'red' : '' }}; color: {{ @$v4['status_rfc_return'] == 'TIDAK LURUS' ? 'black' : '' }}">{{ $v4['status_rfc_return'] ?? '-' }}</td>
													<td>{{ $v4['return'] ?? '-' }}</td>
												</tr>
											@endforeach
										@endif
										</tr>
									@endforeach
								@endforeach
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="col-md-12">
			<h2 class="page-title">Check Rekon</h2>
			<div class="card-deck" style="display: block">
				<form id="submit_lampiran_boq" class="row" method="post" enctype="multipart/form-data">
					{{ csrf_field() }}
					<div class="col-md-12">
						<div class="card shadow mb-4">
							<div class="card-body">
								<div class="form-group row">
									<label class="col-form-label col-md-2 pull-right" for="pekerjaan">Pekerjaan</label>
									<div class="col-md-10">
										<select name="pekerjaan" class="form-control input-transparent" id="pekerjaan" required>
											@foreach ($kerjaan[0] as $val)
												<option value="{{ $val->id }}">{{ $val->text }}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-md-2 pull-right" for="jenis_work">Jenis Pekerjaan</label>
									<div class="col-md-5">
										<select name="jenis_work" class="form-control input-transparent" id="jenis_work">
										</select>
									</div>
									<div class="col-md-5">
										<select name="jenis_kontrak" class="form-control input-transparent" id="jenis_kontrak" required>
											<option value="Kontrak Putus">Kontrak Putus</option>
											<option value="Kontrak Tetap">Kontrak Tetap</option>
										</select>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-md-2 pull-right" for="mitra_id">Mitra</label>
									<div class="col-md-10">
										<select id="mitra_id" name="mitra_id" required></select>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-md-2 pull-right" for="judul">Judul</label>
									<div class="col-md-10">
										<textarea type="text" class="form-control input-transparent" style="resize: none" rows="2" name="judul" id="judul" required></textarea>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-md-2 pull-right" for="id_project">ID Project</label>
									<div class="col-md-10">
										<textarea type="text" class="form-control input-transparent" style="resize: none" rows="2" name="id_project" id="id_project" required></textarea>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-md-2 pull-right">Pengerjaan</label>
									<div class="col-md-5">
										<select id="tahun" name="tahun" required>
											@for ($i = date('Y'); $i >= date('Y', strtotime('-1 year') ); $i--)
												<option value="{{ $i }}">{{ $i }}</option>
											@endfor
										</select>
									</div>
									<div class="col-md-5">
										<select id="bulan" name="bulan" required>
											<option value="01">Januari</option>
											<option value="02">Februari</option>
											<option value="03">Maret</option>
											<option value="04">April</option>
											<option value="05">Mei</option>
											<option value="06">Juni</option>
											<option value="07">Juli</option>
											<option value="08">Agustus</option>
											<option value="09">September</option>
											<option value="10">Oktober</option>
											<option value="11">November</option>
											<option value="12">Desember</option>
										</select>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-md-2 pull-right" for="nominal_plan">Download BOQ</label>
									<div class="col-md-10">
										<a type="button" class="btn btn-primary" href="/download/boq_op/{{ request::segment(2) }}"><i class="fe fe-download fe-16"></i>&nbsp;Download BOQ</a>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-md-2 pull-right" for="material_ta">Material TA</label>
									<div class="col-md-10">
										<select id="material_ta" name="material_ta" data-design_id="16" class="ini_material" multiple></select>
										<code><b>*Material sesuai yang dipakai di RFC</b></code>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-md-2 pull-right" for="material_mitra">Material Mitra</label>
									<div class="col-md-10">
										<select id="material_mitra" name="material_mitra" data-design_id="0" class="ini_material" multiple></select>
										<code><b>*Material sesuai yang dipakai dan tidak tertera di RFC</b></code><br/>
										<a type="button" class="btn add_material btn-secondary" style="margin-top: 7px; color: white"><i class="fe fe-plus fe-16"></i>&nbsp;Update material</a>
										<a type="button" class="btn edit_material btn-primary" data-toggle="modal" data-target="#modal_edit_material" style="margin-top: 7px; color: white"><i class="fe fe-tool fe-16"></i>&nbsp;Atur Material</a>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-md-2 pull-right" for="jumlah_klm" required>Jumlah Kolom</label>
									<div class="col-md-10">
										<select id="jumlah_klm" name="jumlah_klm">
											@for ($i = 1; $i <= 10; $i++)
												<option value="{{ $i }}">{{ $i }}</option>
											@endfor
										</select>
										<a type="button" class="btn add_boq btn-info" style="margin-top: 7px; color: white"><i class="fe fe-tool fe-16"></i>&nbsp;Tambah Kolom</a>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-md-2 pull-right" for="status">Keputusan</label>
									<div class="col-md-10">
										<input type="hidden" id="id_boq_up" name="id_boq_up" value="">
										<select name="status" id="status" style="width: 100%;">
										</select>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-md-2 pull-right" for='detail'>Catatan</label>
									<div class="col-md-10">
										<textarea style="resize: none;" rows="2" name="detail" id="detail" class="form-control input-transparent"></textarea>
									</div>
								</div>
								<div class="form-group row">
									<div class="col-md-3">
										<div class="form-group">
											<label class="col-form-label" for="ba_kpi">BA KPI</label>
											<input type="file" id="ba_kpi" name="ba_kpi" title="Upload Dokumen BA KPI" class="form-control-file">
										</div>
									</div>
									<div class="col-md-3">
										<div class="form-group">
											<label class="col-form-label" for="ba_gr">BA GR (TTD)</label>
											<input type="file" id="ba_gr" name="ba_gr" title="Upload Dokumen BA GR (TTD)" class="form-control-file">
										</div>
									</div>
									<div class="col-md-3">
										<div class="form-group">
											<label class="col-form-label" for="scan_boq_rek">Scan BOQ REKON</label>
											<input type="file" id="scan_boq_rek" name="scan_boq_rek" title="Upload Dokumen Scan BOQ REKON" class="form-control-file">
										</div>
									</div>
									<div class="col-md-3">
										<div class="form-group">
											<label class="col-form-label" for="bar_telkom">BAR REKON TA ke TELKOM (TTD)</label>
											<input type="file" id="bar_telkom" name="bar_telkom" title="Upload Dokumen BAR REKON TA ke TELKOM (TTD)" class="form-control-file">
										</div>
									</div>
									<div class="col-md-3">
										<div class="form-group">
											<label class="col-form-label" for="bar_mitratel">BAR REKON TA ke MITRATEL (TTD)</label>
											<input type="file" id="bar_mitratel" name="bar_mitratel" title="Upload Dokumen BAR REKON TA ke MITRATEL (TTD)" class="form-control-file">
										</div>
									</div>
									<div class="col-md-3">
										<div class="form-group">
											<label class="col-form-label" for="npk">Nota Perhitungan Keuangan (NPK)</label>
											<input type="file" id="npk" name="npk" title="Upload Dokumen Nota Perhitungan Keuangan (NPK)" class="form-control-file">
										</div>
									</div>
									<div class="col-md-3">
										<div class="form-group">
											<label class="col-form-label" for="ba_cut">BA Pemotongan</label>
											<input type="file" id="ba_cut" name="ba_cut" title="Upload Dokumen BA Pemotongan" class="form-control-file">
										</div>
									</div>
									<div class="col-md-3">
										<div class="form-group">
											<label class="col-form-label" for="ba_ct">BA CT</label>
											<input type="file" id="ba_ct" name="ba_ct" title="Upload Dokumen BA CT" class="form-control-file">
										</div>
									</div>
									<div class="col-md-3">
										<div class="form-group">
											<label class="col-form-label" for="ba_ut">BA UT</label>
											<input type="file" id="ba_ut" name="ba_ut" title="Upload Dokumen BA UT" class="form-control-file">
										</div>
									</div>
									<div class="col-md-3">
										<div class="form-group">
											<label class="col-form-label" for="ba_redesign">BA Redesign</label>
											<input type="file" id="ba_redesign" name="ba_redesign" title="Upload Dokumen BA Redesign" class="form-control-file">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-4">
										<div class="form-group">
											<label class="col-form-label" for="sto">Total Material:</label>
											<input type="text" class="form-control" disabled id="total_mat">
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label class="col-form-label" for="sto">Total Jasa:</label>
											<input type="text" class="form-control" disabled id="total_jasa">
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label class="col-form-label" for="sto">Grand Total:</label>
											<input type="text" class="form-control" disabled id="total_grand">
										</div>
									</div>
								</div>
								<input type="hidden" name="material_input" value="{{ Request::old('material_input') }}">
								<input type="hidden" name="jns_btn">
								<div class="form-group mb-3">
									<div class="custom-file">
										<button type="submit" class="btn submit_btn btn_stts btn-success" data-jns='sukses' style="color: white"><i class="fe fe-check fe-16"></i>&nbsp;submit</button>
										<button type="submit" class="btn submit_btn btn_stts btn-danger" data-jns='danger' style="color: white; float: right"><i class="fe fe-alert-triangle fe-16"></i>&nbsp;Pelurusan!</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
			<a type="button" class="btn btn-sm btn-info copy_tbl" data-toggle="modal" data-target="#modal_copas_material" style="color: white;margin-bottom: 10px;"><i class="fe fe-copy fe-16"></i>&nbsp;Copy Tabel Rekon</a>
			<ul class="nav nav-tabs mb-3" id="myTab" role="tablist">
			</ul>
			<div id="myTabContentLeft" class="tab-content">
			</div>
		</div>
	</div>
</div>
@endsection
@section('footerS')
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src='/js/jquery.dataTables.min.js'></script>
<script src='/js/dataTables.bootstrap4.min.js'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.js" integrity="sha512-lbwH47l/tPXJYG9AcFNoJaTMhGvYWhVM9YI43CT+uteTRRaiLCui8snIgyAN8XWgNjNhCqlAUdzZptso6OCoFQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script type="text/javascript">
	$(function(){
		var currentTab,
		already_open_tab = [];
		//initilize tabs
		$("#myTab").on("click", "a", function (e) {
			var href_data = $(this).attr('href');

			if(already_open_tab.indexOf(href_data.split('#')[1]) === -1){
				var get_tab_data = $.grep(load_data_html, function(e){ return e.tabId == href_data.split('#')[1]; });
				if(get_tab_data.length != 0){
					var key_object = (parseInt(get_tab_data[0].tabId.replace( /^\D+/g, '') ) - 1),
					data_lop = last_used_design[key_object][0],
					table_temp = '',
					sto_load = data_lop.sto,
					sub_jenis_load = data_lop.sub_jenis_p,
					id_lokk = data_lop.id_boq_lokasi;

					// showTab(href_data.split('#')[1]);
					registerCloseEvent();

					if(!$.isEmptyObject(rfc_per_lop[id_lokk]) ){
						table_temp += "<div class='col-md-12 witho_rfc'>";
						table_temp += "<div class='card-deck' style='display: block'>";
						table_temp += "<div class='card shadow mb-4'>";
						table_temp += "<div class='card-body'>";
						table_temp += "<table id ='tb_pid_l' class='table table-striped table-bordered' style='width: 100%'>";
						table_temp += "<thead class='thead-dark'>";
						table_temp += "<tr>";
						table_temp += "<th class='hidden-xs'>#</th>";
						table_temp += "<th>ID Material</th>";
						table_temp += "<th>Total Material</th>";
						table_temp += "<th>NO. RFC/GI</th>";
						table_temp += "<th>Status</th>";
						table_temp += "<th>Volume Diberikan</th>";
						table_temp += "<th>Volume Pemakaian</th>";
						table_temp += "<th>Total Pemakaian</th>";
						table_temp += "<th>NO. RFC/Pengembalian</th>";
						table_temp += "<th>Status</th>";
						table_temp += "<th>Volume Dikembalian</th>";
						table_temp += "<th>Sisa Volume</th>";
						table_temp += "<th>Keterangan</th>";
						table_temp += "</tr>";
						table_temp += "</thead>";
						table_temp += '<tbody>';

						var no = 0;
						$.each(rfc_per_lop[id_lokk], function(k, v){
							var first = true;
							$.each(v.isi_m, function(kk, vv){
								var second = true;
								var no_row = 0;
								$.each(v.isi_m, function(index, x) {
									no_row += !x.return ? 1 : Object.keys(x.return).length;
								});

								// console.log(vv);

								$.each(vv, function(kkk, vvv){
									if(kkk == 'out'){
										table_temp += '<tr>';

										if(first == true){
											table_temp +=`<td rowspan="${ no_row }">${ ++no }</td>`;
											table_temp +=`<td rowspan="${ no_row }">${ k }</td>`;
											table_temp +=`<td rowspan="${ no_row }">${ v.total_m }</td>`;
										}

										if(second == true){
											var hitung_row_c = Object.keys(vv).length;
											hitung_row_c = !vv.return ? 1 : Object.keys(vv.return).length;

											table_temp += `<td rowspan="${ hitung_row_c }">${ vvv.nomor_rfc_gi }</td>`;
											table_temp += `<td rowspan="${ hitung_row_c }" style='background-color: ${vvv.status_rfc_gi == 'LURUS' ? '' : 'red'}; color: ${vvv.status_rfc_gi == 'LURUS' ? '' : 'black'}'>${ vvv.status_rfc_gi }</td>`;
											table_temp += `<td rowspan="${ hitung_row_c }">${ vvv.vol_give }</td>`;
											table_temp += `<td rowspan="${ hitung_row_c }">`;
											table_temp += `${(vvv.use != 0 ? vvv.use : '' )}`;
											table_temp += '</td>';
										}

										if(first == true){
											table_temp += `<td rowspan="${ no_row }">${ v.total_use }</td>`;
										}

										if(typeof vv.return != 'undefined'){
											const keys = Object.keys(vv.return).shift();
											const new_re = vv.return[keys];

											table_temp += `<td>${ new_re.nomor_rfc_return ?? '-'}</td>`;
											table_temp += `<td style='background-color: ${new_re.status_rfc_return == 'TIDAK LURUS' ? 'red' : ''}; color: ${new_re.status_rfc_return == 'TIDAK LURUS' ? 'black' : ''}'>${ new_re.status_rfc_return ?? '-' }</td>`;
											table_temp += `<td>${ new_re.return ?? '-' }</td>`;

											if(second == true){
												var used_rfc = 0;

												if(typeof vv.return != 'undefined'){
													var r = 0;

													$.each(vv.return, function(k2, v2){
														r += v2['return'] ?? 0;
													});

													used_rfc = vvv.vol_give - r;
												}

												table_temp += `<td rowspan="${ hitung_row_c }">${ used_rfc }</td>`;

												$second = false;
											}
										}else{
											table_temp += `<td>-</td><td>-</td><td>-</td><td>0</td>`;
										}

										if(first == true){
											table_temp += `<td rowspan="${ no_row }"> ${ v.keterangan ?? 'Tidak Ada Keterangan' } </td>`;
											first = false;
										}
										table_temp += '</tr>';
									}else{
										var conv_vvv = Object.entries(vvv);
										var new_vvv = conv_vvv.map(obj => ({ ...obj }));

										new_vvv.shift();

										console.log(new_vvv);

										if(typeof new_vvv != 'undefined'){
											$.each(new_vvv, function(k2, v2){
												table_temp += '<tr>';
												table_temp += `<td>${ v2[1].nomor_rfc_return ?? '-'}</td>`;
												table_temp += `<td style='background-color: ${v2[1].status_rfc_return == 'TIDAK LURUS' ? 'red' : ''}; color: ${v2[1].status_rfc_return == 'TIDAK LURUS' ? 'black' : ''}'>${ v2[1].status_rfc_return ?? '-' }</td>`;
												table_temp += `<td>${ v2[1].return ?? '-' }</td>`;
												table_temp += '</tr>';
											})
										}
									}
								});
							})
						})

						table_temp += '</tbody>';
						table_temp += "</table>";
						table_temp += "</div>";
						table_temp += "</div>";
						table_temp += "</div>";
						table_temp += "</div>";
					}

					table_temp += "<div class='col-md-12'>";
					table_temp += "<div class='card shadow mb-4'>";
					table_temp += "<div class='card-header'>";
					table_temp += "<strong class='card-title nomor_kol'>Kolom Nomor "+ terbilang(parseInt(get_tab_data[0].tabId.replace( /^\D+/g, '') ) ) +"</strong>";
					// table_temp += "<a class='float-left toggleCard minimize' href='#!' style='color:#17a2b8; text-decoration:none;'>▲</a>" + tbl_hapus;
					table_temp += "</div>";
					table_temp += "<div class='card-body table-responsive'>";
					table_temp += "<select data-no_lokasi=" + parseInt(get_tab_data[0].tabId.replace( /^\D+/g, '') ) + " data-id_lokasi=" + id_lokk + " class='form-control sel_pid' disabled multiple></select>&nbsp;";
					table_temp += "<input type='text' class='form-control lok_per input-transparent' name='lokasi_pekerjaan[]' placeholder='Silahkan Isi Lokasi' value='"+data_lop.lokasi+"'>&nbsp;";
					table_temp += "<select class='sto_m' name='sto[]'>";

					$.each(sto_i, function(k, v) {
						table_temp += "<option value='"+ v.kode_area +"' "+ (sto_load == v.kode_area ? "selected" : '') +">"+ v.kode_area +"</option>"
					})

					table_temp += "</select>&nbsp;";
					table_temp += "<select class='sub_jenis' name='sub_jenis[]'>";

					$.each(sub_jenis_i, function(k, v) {
						table_temp += "<option value='"+ v.sub_jenis +"' "+ (sub_jenis_load == v.sub_jenis ? "selected" : '') +">"+ v.sub_jenis +"</option>"
					})

					table_temp += "</select>&nbsp;";
					table_temp += "<table class='table table_material table-striped table-bordered table-hover' style='width:100%'>";
					table_temp += "<thead class='thead-dark'>";
					table_temp += "<tr>";
					table_temp += "<th rowspan='2'>Designator</th>";
					table_temp += "<th rowspan='2'>Uraian</th>";
					table_temp += "<th rowspan='2'>Jenis Material</th>";
					table_temp += "<th colspan='2'>Paket 7</th>";
					table_temp += "<th rowspan='2'>SP</th>";
					table_temp += "<th rowspan='2'>REKON</th>";
					if(!$.isEmptyObject(rfc_per_lop[id_lokk]) ){
						table_temp += "<th colspan='3'>Material</th>";
					}
					table_temp += "</tr>";
					table_temp += "<tr>";
					table_temp += "<th>Material</th>";
					table_temp += "<th>Jasa</th>";
					if(!$.isEmptyObject(rfc_per_lop[id_lokk]) ){
						table_temp += "<th>Bon Material</th>";
						table_temp += "<th>No RFC</th>";
						table_temp += "<th>No RFR</th>";
					}
					table_temp += "</tr>";
					table_temp += "</thead>";
					table_temp += "<tbody data-lokasi_id='"+ id_lokk +"' data-nomor='"+parseInt(get_tab_data[0].tabId.replace( /^\D+/g, '') )+"'>";

					var total_material_sp = 0,
					total_jasa_sp = 0,
					total_material_rekon = 0,
					total_jasa_rekon = 0;

					// v.sort(function(a, b) {
					// 	return a.designator > b.designator
					// });

					var material_bon = {};

					if(!$.isEmptyObject(rfc_per_lop[id_lokk]) ){
						$.each(rfc_per_lop[id_lokk], function(kr, vr){
							$.each(vr.designator, function(kkr, vvr){
								if (typeof material_bon[vvr] == "undefined") {
									material_bon[vvr] = {};

									material_bon[vvr].total = 0;
									material_bon[vvr].no_rfc = {};
									material_bon[vvr].no_rfr = {};
								}

								material_bon[vvr].total += vr.total_use;

								var isi_m = Object.values(vr.isi_m);

								var rfc_gi = isi_m.map(function(v, k){
									return v[0].nomor_rfc_gi;
								});

								var rfc_return = isi_m.map(function(v, k){
									return v[0].nomor_rfc_return;
								});

								material_bon[vvr].no_rfc_gi = rfc_gi;
								material_bon[vvr].no_rfc_return = rfc_return;
							});
						});
					}

					$.each(load_material, function(k1, v1){
						var val = $.grep(design, function(e){ return e.id == v1.id; }),
						get_data_qty = $.grep(last_used_design[key_object], function(e){ return e.id_design == v1.id && e.design_mitra_id == v1.design_mitra_id; });

						var nilai_sp = get_data_qty[0] ? get_data_qty[0].sp : 0,
						nilai_rekon = get_data_qty[0] ? get_data_qty[0].rekon : 0;

						val[0].material_load = v1.mat || 0;
						val[0].jasa_load = v1.jasa || 0;

						table_temp += "<tr data-id='"+v1.id+"' data-id_khs='"+v1.id+"_"+v1.design_mitra_id+"'>";
						table_temp += "<td>"+val[0].designator+"</td>";

						var word = val[0].uraian.split(' '),
						text = '';

						if(word.length > 6){
							text = "...<a style='color:blue; cursor:pointer' class='copy_detail_tool' data-toggle='tooltip' title='"+val[0].uraian+"'>Lihat Detail</a>";
						}

						table_temp += "<td>"+word.slice(0, 6).join(' ') + text +"</td>";
						table_temp += "<td>"+(parseInt(v1.design_mitra_id) == 16 ? 'Telkom Akses' : 'Mitra')+"</td>";
						table_temp += "<td>"+v1.mat.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".")+"</td>";
						table_temp += "<td>"+v1.jasa.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".")+"</td>";
						table_temp += "<td " + (nilai_sp != nilai_rekon ? "style='background-color: #f26d70; color: black;'" : "") + ">"+(nilai_sp || 0).toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".")+"</td>";

						total_material_sp += (v1.mat * parseInt(nilai_sp) || 0 );
						total_jasa_sp += (v1.jasa * parseInt(nilai_sp) || 0 );
						total_material_rekon += (v1.mat * parseInt(nilai_rekon) || 0 );
						total_jasa_rekon += (v1.jasa * parseInt(nilai_rekon) || 0 );

						material_tot += (v1.mat * parseInt(nilai_rekon) || 0 );
						jasa_tot += (v1.jasa * parseInt(nilai_rekon) || 0 );

						table_temp += "<td><input type='text' style='width:80px;' class='form-control number input-transparent required field_material_input' data-nomor_input='"+parseInt(get_tab_data[0].tabId.replace( /^\D+/g, '') )+"' data-id_mat='"+v1.id+"' data-material='"+v1.mat+"' data-sp='"+nilai_sp+"' data-jasa='"+v1.jasa+"' placeholder='0' data-design_mitra_id="+v1.design_mitra_id+" value='"+ (parseInt(nilai_rekon) || 0 != 0 ? nilai_rekon : '') +"'></td>";

						if(!$.isEmptyObject(rfc_per_lop[id_lokk]) ){
							if(!$.isEmptyObject(material_bon[val[0].designator]) ){
								table_temp += `<td>${material_bon[val[0].designator].total}</td>`;
								table_temp += `<td>${Object.keys(material_bon[val[0].designator].no_rfc_gi.filter(function(el) { return el; }) ).length != 0 ? material_bon[val[0].designator].no_rfc_gi.filter(function(el) { return el; }).join(', ') : '-'}</td>`;
								table_temp += `<td>${Object.keys(material_bon[val[0].designator].no_rfc_return.filter(function(el) { return el; }) ).length != 0 ? material_bon[val[0].designator].no_rfc_return.filter(function(el) { return el; }).join(', ') : '-'}</td>`;
							}else{
								table_temp += `<td>-</td>`;
								table_temp += `<td>-</td>`;
								table_temp += `<td>-</td>`;
							}
						}

						table_temp += "</tr>";
					})

					table_temp += "</tbody>";
					table_temp += "<tfoot>";
					table_temp += "<tr data-nomor_foot='"+parseInt(get_tab_data[0].tabId.replace( /^\D+/g, '') )+"'>";
					table_temp += "<td colspan='3'>Total Harga</td>";
					table_temp += "<td class='hrg_material'>" + total_material_rekon.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") + "</td>";
					table_temp += "<td class='hrg_jasa'>" + total_jasa_rekon.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") + "</td>";
					table_temp += "<td class='total_sp_plan'>" + (total_material_sp + total_jasa_sp).toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") + "</td>";
					table_temp += "<td class='sum_me'>" + (total_material_rekon + total_jasa_rekon).toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") + "</td>";
					table_temp += "</tr>";
					table_temp += "</tfoot>";
					table_temp += "</table>";
					table_temp += "</div>";
					table_temp += "</div>";
					table_temp += "</div>";

					$(href_data).html(table_temp);

					var material_in = 0,
					jasa_in = 0;

					$('.field_material_input').each(function(){
						var id_material = parseInt($(this).data('id_mat') ),
						material = parseInt($(this).attr('data-material') ),
						jasa = parseInt($(this).attr('data-jasa') ),
						urutan = parseInt($(this).parent().parent().parent().data('nomor') ),
						isi = parseInt($(this).val() ) || 0;
						material_in += material * isi;
						jasa_in += jasa * isi;
						// console.log(material, jasa);
						$('#total_mat').val(material_in.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
						$('#total_jasa').val(jasa_in.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
						$('#total_grand').val((material_in + jasa_in).toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
					});

					$('.lok_per').on("keypress keyup",function (e){
						return $(this).val($(this).val().toUpperCase());
					});

					$('.sto_m').each(function() {
						$(this).select2({
							width: '100%',
							placeholder: 'Sto Bisa Dipilih'
						});
					});

					$('.sel_pid').each(function() {
						var lokasi_id = $(this).data('id_lokasi'),
						list_all_pid = {!! json_encode($pid) !!}
						data = [],
						lokasinya = {!! json_encode($save_pid_per_lok) !!};

						if(lokasi_id == id_lokk){
							$.each(list_all_pid, function(k, v){
								data.push({
									id: v,
									text: v
								});
							});

							$(this).select2({
								width: '100%',
								placeholder: "Ketik pid",
								data: data
							});

							if(lokasinya[lokasi_id]){
								var lokasinya = {!! json_encode($save_pid_per_lok) !!};
								// console.log(lokasinya[lokasi_id])
								$(this).val(lokasinya[lokasi_id]).change()
							}
						}
					});

					$('.sub_jenis').each(function() {
						$(this).select2({
							width: '100%',
							placeholder: 'Sto Bisa Dipilih'
						});
					});

					already_open_tab.push(href_data.split('#')[1]);
				}
			}

			e.preventDefault();
			$(this).tab('show');
			$currentTab = $(this);
		});
		//this method will demonstrate how to add tab dynamically
		var no_tbl = 0,
		sto_i = {!! json_encode($all_sto) !!};

		function registerComposeButtonEvent() {
			/* just for this demo */
			$('.add_boq').on('click', function(e){
				e.preventDefault();
				var jml = $('#jumlah_klm').val();
				// craeteNewTabAndLoadUrl("", "./SamplePage.html", "#" + tabId);

				for (let a = 1; a <= jml; a++) {
					++no_tbl;
					table_temp = '';
					var tabId = "compose" + no_tbl;

					$('.nav-tabs').append('<li><a href="#' + tabId + '" class="nav-link" data-toggle="tab">LOP Nomor <span class="badge badge-primary"">'+no_tbl+'</span><button type="button" class="close closeTab">×</button></a></li>');
					$('.tab-content').append('<div class="tab-pane" id="' + tabId + '"></div>');

					table_temp += "<div class='col-md-12'>";
					table_temp += "<div class='card shadow mb-4'>";
					table_temp += "<div class='card-header'>";
					table_temp += "<strong class='card-title nomor_kol'>Kolom Nomor "+ terbilang(no_tbl) +"</strong>";
					// table_temp += "<a class='float-left toggleCard minimize' href='#!' style='color:#17a2b8; text-decoration:none;'>▲</a><a class='float-right delete_buatan' href='#!' style='color:red; text-decoration:none;'>✖</a>";
					table_temp += "</div>";
					table_temp += "<div class='card-body table-responsive'>";
					table_temp += "<input type='text' class='form-control lok_per input-transparent' name='lokasi_pekerjaan[]' placeholder='Silahkan Isi Lokasi' required>&nbsp;";
					table_temp += "<select class='sto_m' name='sto[]' required>";

					$.each(sto_i, function(k, v) {
						table_temp += "<option value='"+ v.kode_area +"'>"+ v.kode_area +"</option>"
					})

					table_temp += "</select>&nbsp;";
					table_temp += "<select class='sub_jenis' name='sub_jenis[]' required>";

					$.each(sub_jenis_i, function(k, v) {
						table_temp += "<option value='"+ v.sub_jenis +"'>"+ v.sub_jenis +"</option>"
					})

					table_temp += "</select>&nbsp;";
					table_temp += "<table class='table table_material table-striped table-bordered table-hover' style='width:100%'>";
					table_temp += "<thead class='thead-dark'>";
					table_temp += "<tr>";
					table_temp += "<th rowspan='2'>Designator</th>";
					table_temp += "<th rowspan='2'>Uraian</th>";
					table_temp += "<th rowspan='2'>Jenis Material</th>";
					table_temp += "<th colspan='2'>Paket 7</th>";
					table_temp += "<th rowspan='2'>SP</th>";
					table_temp += "<th rowspan='2'>REKON</th>";
					table_temp += "</tr>";
					table_temp += "<tr>";
					table_temp += "<th>Material</th>";
					table_temp += "<th>Jasa</th>";
					table_temp += "</tr>";
					table_temp += "</thead>";
					table_temp += "<tbody data-lokasi_id='NEW' data-nomor="+no_tbl+">";

					load_material.sort(function(a, b) {
						var val1 = $.grep(design, function(e){ return e.id == a.id; });
						var val2 = $.grep(design, function(e){ return e.id == b.id; });

						return val1[0].designator > val2[0].designator;
					});

					if(load_material.length){
						$.each(load_material, function(key2, val2) {
							$.each(design, function(key, val) {
								if(val.id == val2.id){
									table_temp += "<tr data-id="+val.id+" data-id_khs='"+val.id+"_"+val.design_mitra_id+"'>";
									table_temp += "<td>"+val.designator+"</td>";
									var word = val.uraian.split(' '),
									text = '';

									if(word.length > 6){
										text = "...<a style='color:blue; cursor:pointer' class='copy_detail_tool' data-toggle='tooltip' title='"+val.uraian+"'>Lihat Detail</a>";
									}

									table_temp += "<td>"+word.slice(0, 6).join(' ') + text +"</td>";
									table_temp += "<td>"+(parseInt(val2.design_mitra_id) == 0 ? 'Mitra' : 'Telkom Akses')+"</td>";
									table_temp += "<td>"+val2.mat.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".")+"</td>";
									table_temp += "<td>"+val2.jasa.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".")+"</td>";
									table_temp += "<td>0</td>"
									table_temp += "<td><input type='text' style='width:80px;' class='form-control number input-transparent required field_material_input' data-nomor_input='"+tbl_load+"' data-id_mat='"+val.id+"' data-material='"+val2.mat+"' data-sp='0' data-jasa="+val2.jasa+" placeholder='0' data-design_mitra_id="+val2.design_mitra_id+"></td>";
									table_temp += "</tr>";
								}
							});
						});
					}

					table_temp += "</tbody>";
					table_temp += "<tfoot>";
					table_temp += "<tr class='foooter_sum'>";
					table_temp += "<td colspan='3'>Total Harga</td>";
					table_temp += "<td class='hrg_material'>0</td>";
					table_temp += "<td class='hrg_jasa'>0</td>";
					table_temp += "<td class='total_sp_plan'>0</td>";
					table_temp += "<td class='sum_me'>0</td>";
					table_temp += "<td>-</td>";
					table_temp += "<td>-</td>";
					table_temp += "<td>-</td>";
					table_temp += "</tr>";
					table_temp += "</tfoot>";
					table_temp += "</table>";
					table_temp += "</div>";
					table_temp += "</div>";
					table_temp += "</div>";

					showTab(tabId);
					registerCloseEvent();
					$(`#${tabId}`).html(table_temp);
				}

				$('.sto_m').each(function() {
					$(this).select2({
						width: '100%',
						placeholder: 'Sto Bisa Dipilh'
					});
				});

				$('.sub_jenis').each(function() {
					$(this).select2({
						width: '100%',
						placeholder: 'Sub Pekerjaan Bisa Dipilih'
					});
				});

				$('.lok_per').on("keypress keyup",function (e){
					return $(this).val($(this).val().toUpperCase());
				});

				toastr.options = {
					"closeButton": false,
					"debug": false,
					"newestOnTop": false,
					"progressBar": true,
					"positionClass": "toast-top-center",
					"preventDuplicates": false,
					"onclick": null,
					"showDuration": "300",
					"hideDuration": "1000",
					"timeOut": "5000",
					"extendedTimeOut": "1000",
					"showEasing": "swing",
					"hideEasing": "linear",
					"showMethod": "fadeIn",
					"hideMethod": "fadeOut"
				}
				toastr["success"]("Jumlah Kolom Sudah Ditambahkan!", "Sukses")
			});
		}
		//this method will register event on close icon on the tab..
		function registerCloseEvent() {
			$(".closeTab").click(function () {
				var this_me = $(this),
				text = $(this).parent().text().substring(0, $(this).parent().text().length - 1);

				Swal.fire({
					title: `Apakah Kamu Ingin Menghapus ${text}`,
					icon: 'warning',
					showDenyButton: true,
					confirmButtonText: 'Ya, Hapus!',
					denyButtonText: `Jangan!`,
				}).then((result) => {
					/* Read more about isConfirmed, isDenied below */
					if (result.isConfirmed) {
						//there are multiple elements which has .closeTab icon so close the tab whose close icon is clicked
						var tabContentId = this_me.parent().attr("href");
						this_me.parent().parent().remove(); //remove li of tab
						$('#myTab a:last').click(); // Select first tab
						$(tabContentId).remove(); //remove respective tab content
					} else if (result.isDenied) {
						Swal.fire(`${text} Tidak Dihapus!`, '', 'info')
					}
				})
			});
		}
		//shows the tab with passed content div id..paramter tabid indicates the div where the content resides
		function showTab(tabId) {
			$('#myTab a[href="#' + tabId + '"]').tab('show');
		}
		//return current active tab
		function getCurrentTab() {
			return currentTab;
		}
		//This function will create a new tab here and it will load the url content in tab content div.
		// function craeteNewTabAndLoadUrl(parms, url, loadDivSelector) {
		// 	$("" + loadDivSelector).load(url, function (response, status, xhr) {
		// 		if (status == "error") {
		// 			var msg = "Sorry but there was an error getting details ! ";
		// 			$("" + loadDivSelector).html(msg + xhr.status + " " + xhr.statusText);
		// 			$("" + loadDivSelector).html("Load Ajax Content Here...");
		// 		}
		// 	});
		// }
		//this will return element from current tab
		//example : if there are two tabs having  textarea with same id or same class name then when $("#someId") whill return both the text area from both tabs
		//to take care this situation we need get the element from current tab.
		function getElement(selector) {
			var tabContentId = $currentTab.attr("href");
			return $("" + tabContentId).find("" + selector);
		}

		function removeCurrentTab() {
			var tabContentId = $currentTab.attr("href");
			$currentTab.parent().remove(); //remove li of tab
			$('#myTab a:last').tab('show'); // Select first tab
			$(tabContentId).remove(); //remove respective tab content
		}

		registerComposeButtonEvent();
		registerCloseEvent();

		$("body").tooltip({ selector: '[data-toggle=tooltip]' });

		$('.copy_tbl').click(function(){
			var copas = {},
			count_isi = {};

			$('.field_material_input').each(function(){
				var id_material = $(this).data('id_mat'),
				material = parseInt($(this).attr('data-material') ) || 0,
				jasa = parseInt($(this).attr('data-jasa') ) || 0,
				sp = parseInt($(this).data('sp') ) || 0,
				urutan = $(this).parent().parent().parent().data('nomor'),
				lokasi = $(this).parent().parent().parent().parent().parent().find('.lok_per').val(),
				id_lokasi_lok = $(this).parent().parent().parent().data('lokasi_id'),
				sto = $(this).parent().parent().parent().parent().parent().find('.sto_m').val()
				sub_jenis = $(this).parent().parent().parent().parent().parent().find('.sub_jenis').val()
				jenis_khs = parseInt($(this).attr('data-design_mitra_id') ),
				isi = parseInt($(this).val() ) || 0;

				var get_design = design.find(o => o.id == id_material);
				// copas.push({
				// 	id: id_material,
				// 	material: material,
				// 	jasa: jasa,
				// 	sp: sp,
				// 	lokasi: lokasi,
				// 	sto: sto,
				// 	lokasi_id: id_lokasi_lok,
				// 	rekon: isi,
				// 	jenis_khs: jenis_khs,
				// 	urutan: urutan,
				// 	sub_jenis: sub_jenis,
				// 	id_design: id_material,
				// 	// designator: get_design.designator,
				// 	namcomp: (jenis_khs == 16 ? 'Telkom Akses' : 'Mitra'),
				// 	tambah: 0,
				// 	kurang: 0,
				// 	design_mitra_id: jenis_khs,
				// 	id_boq_lokasi: id_lokasi_lok,
				// 	jenis: get_design.jenis,
				// });

				if(typeof count_isi[id_material + '_' + jenis_khs] === 'undefined'){
					count_isi[id_material + '_' + jenis_khs] = 0;
				}

				count_isi[id_material + '_' + jenis_khs] += isi;

				copas[id_material + '_' + jenis_khs] = {
					'designator': get_design.designator,
					'mat': material,
					'design_mitra_id': jenis_khs,
					'jasa': jasa,
					'sp': sp,
					'rekon': count_isi[id_material + '_' + jenis_khs]
				};
			});

			var copas_table = "<div class='col-md-12'>";
			copas_table += "<div class='card shadow mb-4'>";
			copas_table += "<div class='card-body table-responsive'>";
			copas_table += "<table class='table table_copas_m table-striped table-bordered table-hover' style='width:100%'>";
			copas_table += "<thead class='thead-dark'>";
			copas_table += "<tr>";
			copas_table += "<th rowspan='2'>Designator</th>";
			copas_table += "<th rowspan='2'>Jenis Material</th>";
			copas_table += "<th colspan='2'>Paket 7</th>";
			copas_table += "<th rowspan='2'>SP</th>";
			copas_table += "<th rowspan='2'>REKON</th>";
			copas_table += "</tr>";
			copas_table += "<tr>";
			copas_table += "<th>Material</th>";
			copas_table += "<th>Jasa</th>";
			copas_table += "</tr>";
			copas_table += "</thead>";
			copas_table += "<tbody>";

			if(!$.isEmptyObject(copas) ){
				$.each(copas, function(key, val) {
					copas_table += "<tr>";
					copas_table += "<td>"+val.designator+"</td>";
					copas_table += "<td>"+(parseInt(val.design_mitra_id) == 0 ? 'Mitra' : 'Telkom Akses')+"</td>";
					copas_table += "<td>"+val.mat.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".")+"</td>";
					copas_table += "<td>"+val.jasa.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".")+"</td>";
					copas_table += "<td>"+val.sp+"</td>"
					copas_table += "<td>"+val.rekon+"</td>"
					copas_table += "</tr>";
				});
			}

			copas_table += "</tbody>";
			copas_table += "</table>";
			copas_table += "</div>";
			copas_table += "</div>";
			copas_table += "</div>";
			// console.log(copas_table)
			$('.data_copas_tbl').html(copas_table)
		});

		$(".copy_table").on('click', function(){
			var urlField = document.querySelector('.table_copas_m'),
			range = document.createRange();

			range.selectNode(urlField);
			window.getSelection().addRange(range);
			document.execCommand('copy');

			toastr.options = {
				"debug": false,
				"newestOnTop": false,
				"progressBar": true,
				"positionClass": "toast-top-center",
				"preventDuplicates": false,
				"onclick": null,
				"showDuration": "300",
				"hideDuration": "1000",
				"timeOut": "3000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut"
			}
			toastr["success"]("Tabel Berhasil Disalin!", "Sukses")
		})

		$('#status').select2({
			data: {!! json_encode($select2) !!},
			placeholder: 'Select Status',
			width: '100%'
		});

		$('.btn_stts').on('click', function(){
			var data = $(this).data('jns');
			$("input[name='jns_btn']").val(data)
		})

		$('#tahun, #bulan').select2({
			width: '100%'
		})

		$('.aply_all').on('click', function(){
			$('.lok_per').val( $('#lokasi_pekerjaan').val() ).change();
			$('.sto_m').val( $('#sto_obj').val() ).change();

			toastr.options = {
				"closeButton": false,
				"debug": false,
				"newestOnTop": false,
				"progressBar": true,
				"positionClass": "toast-top-center",
				"preventDuplicates": false,
				"onclick": null,
				"showDuration": "300",
				"hideDuration": "1000",
				"timeOut": "5000",
				"extendedTimeOut": "1000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut"
			}
			toastr["success"]("Lokasi dan STO Berhasil Disamakan!", "Sukses")
		})

		$(document).on('keyup', '.number', function(event){
			if(event.which >= 37 && event.which <= 40) return;
			$(this).val(function(index, value) {
        return value.replace(/\D/g, "");
      });
		})

		$(document).on('click', '.toggleCard', function(){
			$(this).html((i, t) => t === '▲' ? '▼' : '▲');
			$(this).parent().next('.card-body').slideToggle();
		})

		$('#sto_obj').select2({
			width: '100%',
			placeholder: 'Sto Bisa Dipilih'
		})

		data_sp = {!! json_encode($data_sp) !!};

		$('.lihat_detail_rfc').click(function(){
			$('#modal_check_rfc').modal('toggle');
			$('.submit_btn_rfc').hide();
			$.ajax({
				url:"/get_ajx/inventory",
				type:"GET",
				data: {
					isi: $(this).data('rfc'),
					id_pbu: data_sp.id
				},
				dataType: 'json',
				success: (function(data){
					var data_rfc = data.inventory[0]
					// console.log(data)
					if(data.download){
						$('.dwn_rfc').show();
						$('.dwn_rfc').attr('href', '/download/rfc/'+data_sp.id+'/'+data_rfc.no_rfc);
					}else{
						$('.dwn_rfc').hide();
					}

					$('.no_rfc').html(data_rfc.no_rfc)
					$('.plant').html(data_rfc.plant)
					$('.wbs_element').html(data_rfc.wbs_element)
					$('.type').html( (data_rfc.type == 'Out' ? 'Pengeluaran' : 'Pengembalian') )
					$('.mitra').html(data_rfc.mitra)

					var table_html = "<table class='tbl_material table table-striped table-bordered table-hover'>";
					table_html += "<thead class='thead-dark'>";
					table_html += "<tr>";
					table_html += "<th>Material</th>";
					table_html += "<th>Satuan</th>";
					table_html += "<th>Jumlah</th>";
					table_html += "</tr>";
					table_html += "</thead>";
					table_html += "<tbody>";

					$.each(data.inventory, function(key, val){
						table_html += "<tr>";
						table_html += "<td>"+ val.material +"</td>";
						table_html += "<td>"+ val.satuan +"</td>";
						table_html += "<td>"+ val.quantity +"</td>";
						table_html += "</tr>";
					});

					table_html += "</tbody>";
					table_html += "</table>";

					$('.table_rfc_material').html(table_html)
					var data_select = [];

					$.each(data.pid, function(key, val){
						data_select.push({
							id: key,
							text: val.lokasi +' (' + val.pid + ')'
						})
					});

					// console.log(data_select)

					$('.tbl_material').DataTable({
						autoWidth: true,
        		bFilter: false,
						lengthMenu: [
							[16, 32, 64, -1],
							[16, 32, 64, "All"]
						]
					});

				})
			})
		})

		$('select[id="mitra_id"]').on('select2:select', function(){
			var data = $("#material").select2("data"),
			isi = $(this).val();

			// $.each(data, function(key, val){
			// 	if(val.id_mitra != 16 && isi != val.id_mitra){
			// 		val.element.remove()
			// 	}
			// });
		});

		$('select[id="mitra_id"]').select2({
			width: '100%',
			placeholder: "Masukkan Nama Perusahaan Mitra",
			allowClear: true,
			ajax: {
				url: "/get_ajx/mitra/search/per_witel",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						searchTerm: params.term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true,
			},
		});

		$('#pekerjaan').val(null).change();

		$('#pekerjaan').select2({
			width: '100%',
			placeholder: 'Isi jenis pekerjaan',
		});

		$('#jumlah_klm').select2({
			width: '100%',
		});

		$('#jenis_kontrak').select2({
			width: '100%',
			placeholder: 'Isi jenis Kontrak',
		});

		$('#pekerjaan').on('change', function(){

			var jenis_kerja = {!! json_encode($kerjaan[1]) !!},
			kerja = $(this).val(),
			cari = [];
			$.each(jenis_kerja, function(key, val) {
				if(val.pekerjaan == kerja){
					cari.push({id: val.id, text: val.text})
				}
			});
			// console.log(cari)
			$("#jenis_work").empty().trigger('change')

			$('#jenis_work').select2({
				width: '100%',
				data: cari
			})
		})

		$('#tb_rekon').DataTable({
			autoWidth: true,
			lengthMenu: [
				[16, 32, 64, -1],
				[16, 32, 64, "All"]
			]
		});

		function terbilang(nilai)
    {
			var hasil;
			if (nilai < 0){
				hasil = "minus " + penyebut(nilai);
			}else{
				hasil = penyebut(nilai);
			}

			return hasil;
    }

    function penyebut(nilai)
    {
			var nilai = Math.abs(Math.floor(nilai)),
			huruf = ["", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas"],
			temp;
			if(nilai < 12){
				temp = " " + huruf[nilai];
			}else if(nilai < 20){
				temp = penyebut(nilai - 10) + " belas";
			}else if(nilai < 100){
				temp = penyebut(nilai / 10) + " puluh" + penyebut(nilai % 10);
			} else if (nilai < 200) {
				temp = " seratus" + penyebut(nilai - 100);
			} else if (nilai < 1000) {
				temp = penyebut(nilai / 100) + " ratus" + penyebut(nilai % 100);
			}
			return temp;
    }

		var load_material = [];

		function add_material(mm){
			var td_material = '';
			sukses_filter = [];

			$.each(mm, function(k, v){
				var value = $(this).val(),
				jenis_khs = parseInt($(this).data('design_id'));
				var isi_load = value;
				var material = [];

				if(isi_load.length){
					var filter_lm = [];
					$.each(load_material, function(k, vx){
						if(vx.design_mitra_id == jenis_khs){
							filter_lm.push(vx);
							sukses_filter.push(vx);
						}
					})

					var diff = filter_lm.filter(o1 => !isi_load.some(o2 => o1.id === parseInt(o2) ) );

					if(diff){
						$.each(diff, function(key, val){
							$("tr[data-id_khs='" + val.id +"_"+val.design_mitra_id+"']").remove();
						});
					}
					//ini kalau ada materialnya
					var intersection = filter_lm.filter(o1 => isi_load.some(o2 => o1.id === parseInt(o2) ) );
					var remove_dup = isi_load.filter(o1 => !intersection.some(o2 => parseInt(o1) === o2.id) );
					material = remove_dup;
				}
				// console.log(design)
				$.each(material, function(key2, val2) {
					$.each(design, function(key, val) {
						if(val.id == val2){
							var loop_me = true;

							if(jenis_khs == 16){
								var material_new = 0,
								material_text = 0;
							}else{
								var material_new = val.material,
								material_text = val.material.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".");
							}

							if(last_used_design.length != 0){
								var tbl_m = $(".table_material").find('tbody');
								$.each(last_used_design, function(kx, vx){
									$.each(vx, function(kk, vv){
										if(vv.id_design == val2){
											var total_material_sp = 0,
											total_jasa_sp = 0,
											total_material_rekon = 0,
											total_jasa_rekon = 0;

											td_material += "<tr data-id='"+vv.id_design+"' data-id_khs='"+vv.id_design+"_"+jenis_khs+"'>";
											td_material += "<td>"+val.designator+"</td>";

											var word = val.uraian.split(' '),
											text = '';

											if(word.length > 6){
												text = "...<a style='color:blue; cursor:pointer' class='copy_detail_tool' data-toggle='tooltip' title='"+val.uraian+"'>Lihat Detail</a>";
											}

											td_material += "<td>"+word.slice(0, 6).join(' ') + text +"</td>";
											td_material += "<td " + (vv.namcomp != jenis_khs ? "style='background-color: #f26d70; color: black;'" : "") + ">"+(jenis_khs == 0 ? 'Mitra' : 'Telkom Akses')+"</td>";
											td_material += "<td>"+material_text+"</td>";
											td_material += "<td>"+val.jasa.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".")+"</td>";

											var spB = 0;

											if(vv.material == material_new && vv.jasa == val.jasa){
												spB = (vv.sp || 0);
											}

											td_material += "<td>"+spB.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".")+"</td>";

											total_material_sp += (material_new * spB );
											total_jasa_sp += (val.jasa * spB );
											total_material_rekon += (material_new * (vv.rekon || 0) );
											total_jasa_rekon += (val.jasa * (vv.rekon || 0) );

											td_material += "<td><input type='text' style='width:80px;' class='form-control number input-transparent required field_material_input' data-nomor_input='"+tbl_load+"' data-id_mat='"+val.id_design+"' data-material='"+material_new+"' data-sp='"+spB+"' placeholder='0' data-jasa='"+val.jasa+"' data-design_mitra_id="+jenis_khs+" value='"+ (parseInt(vv.rekon) || 0 != 0 ? vv.rekon : (spB != 0 ? spB : '' ) ) +"'></td>";

											td_material += "</tr>";

											if($(".table_material").length){
												$(".table_material").find("tbody[data-lokasi_id='"+vv.id_boq_lokasi+"']").append(td_material)
												td_material = '';
											}

											loop_me = false;
										}
									});

									if($(".table_material").length){
										// var data = [];
										// $(".table_material tr").each(function(key, val){
										// 	data[key] = [];
										// 	$(this).children('td').each(function(key2, val2){
										// 		data[key][key2] = $(this).text();
										// 	});
										// });
										tbl_m.eq(0).append(td_material)
										tbl_m = tbl_m.slice(1);
										td_material = '';
									}
								});
							}

							if(loop_me){
								td_material += "<tr data-id="+val.id+" data-id_khs='"+val.id+"_"+jenis_khs+"'>";
								td_material += "<td>"+val.designator+"</td>";

								var word = val.uraian.split(' '),
								text = '';

								if(word.length > 6){
									text = "...<a style='color:blue; cursor:pointer' class='copy_detail_tool' data-toggle='tooltip' title='"+val.uraian+"'>Lihat Detail</a>";
								}

								td_material += "<td>"+word.slice(0, 6).join(' ') + text +"</td>";
								td_material += "<td>"+(jenis_khs == 0 ? 'Mitra' : 'Telkom Akses')+"</td>";
								td_material += "<td>"+material_text+"</td>";
								td_material += "<td>"+val.jasa.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".")+"</td>";
								var espe= 0;
								td_material += "<td>"+espe+"</td>";
								td_material += "<td><input type='text' style='width:80px;' required class='form-control number input-transparent field_material_input' placeholder='0' data-nomor_input='"+tbl_load+"' data-id_mat="+val.id+" data-material="+material_new+" data-sp="+espe+" data-jasa="+val.jasa+" data-design_mitra_id="+jenis_khs+"></td>";
								td_material += "</tr>";

								if($(".table_material").length){
									// var data = [];
									// $(".table_material tr").each(function(key, val){
									// 	data[key] = [];
									// 	$(this).children('td').each(function(key2, val2){
									// 		data[key][key2] = $(this).text();
									// 	});
									// });
									$(".table_material").find('tbody').append(td_material)
									td_material = '';
								}
							}
						}
					});
				});
			});

			var diff = load_material.filter(o1 => !sukses_filter.some(o2 => o1.id + '_' + o1.design_mitra_id === o2.id + '_' + o2.design_mitra_id ) );

			if(diff){
				$.each(diff, function(key, val){
					$("tr[data-id_khs='" + val.id +"_"+val.design_mitra_id+"']").remove();
				});
			}

			load_material = [];
			var load_mat_rfc = [];

			$.each(mm, function(k, v){
				var value = $(this).val(),
				jenis_khs = parseInt($(this).data('design_id'));
				var isi_load = value;

				$.each(isi_load, function(key2, val2) {
					$.each(design, function(key, val) {
						if(val2 == val.id){
							if(jenis_khs == 16){
								var material_new = 0;
							}else{
								var material_new = (val.material_load !== undefined ? val.material_load : val.material);
							}

							load_material.push({
								id: val.id,
								mat: material_new,
								jasa: (val.jasa_load !== undefined ? val.jasa_load : val.jasa),
								design_mitra_id: jenis_khs
							});

							load_mat_rfc.push({
								id: val.id, text: val.designator
							})
						}
					});
				});


				$.each(last_used_design[0], function(k, v){
					$.each(load_material, function(kk, vv){
						if(vv.id == v.id_design && vv.design_mitra_id == v.design_mitra_id){
							load_material[kk].mat = v.material;
							load_material[kk].jasa = v.jasa;
						}
					});
				});

			});

			$("#material_rfc").empty().trigger('change');

			$('#material_rfc').select2({
				width: '100%',
				placeholder: 'Masukkan Material!',
				data: load_mat_rfc,
			});

			var material_tot_chck = 0,
			jasa_tot_chck = 0;

			$('.field_material_input').each(function(){
				var tfoot = $(this).parent().parent().parent().next(),
				body = $(this).parent().parent().parent();
				// console.log(tfoot, body);
				var material_in = 0,
				jasa_in = 0,
				material_sp = 0,
				jasa_sp = 0;
				body.each(function(){
					$(this).find('.field_material_input').each(function(){
						var id_material = parseInt($(this).data('id_mat') ),
						material = parseInt($(this).attr('data-material') ) || 0,
						jasa = parseInt($(this).attr('data-jasa') ) || 0,
						sp = parseInt($(this).attr('data-sp') ) || 0,
						urutan = parseInt($(this).parent().parent().parent().data('nomor') ),
						isi_material = parseInt($(this).val() ) || 0;
						material_in += material * isi_material;
						jasa_in += jasa * isi_material;

						material_sp += material * sp;
						jasa_sp += jasa * sp;
						tfoot.find('.hrg_material').text(material_in.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
						tfoot.find('.hrg_jasa').text(jasa_in.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
						tfoot.find('.total_sp_plan').text((material_sp + jasa_sp).toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
						tfoot.find('.sum_me').text((material_in + jasa_in).toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
					})
				});

				var id_material = parseInt($(this).data('id_mat') ),
				material = parseInt($(this).attr('data-material') )|| 0,
				jasa = parseInt($(this).attr('data-jasa') )|| 0,
				urutan = parseInt($(this).parent().parent().parent().data('nomor') ),
				isi = parseInt($(this).val() )|| 0;
				material_tot_chck += material * isi;
				jasa_tot_chck += jasa * isi;
			});

			if($('.field_material_input').length == 0 ){
				$('.hrg_material').text(0);
				$('.hrg_jasa').text(0);
				$('.total_sp_plan').text(0);
				$('.sum_me').text(0);
			}

			$('#total_mat').val(material_tot_chck.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
			$('#total_jasa').val(jasa_tot_chck.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
			$('#total_grand').val((material_tot_chck + jasa_tot_chck).toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
		}

		var load_sp = {!! json_encode($load_sp) !!};

		$('.add_material').on('click', function(){
			add_material($('.ini_material') );

			toastr.options = {
				"closeButton": false,
				"debug": false,
				"newestOnTop": false,
				"progressBar": true,
				"positionClass": "toast-top-center",
				"preventDuplicates": false,
				"onclick": null,
				"showDuration": "300",
				"hideDuration": "1000",
				"timeOut": "5000",
				"extendedTimeOut": "1000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut"
			}
			toastr["success"]("Material Berhasil Diubah!", "Sukses")

			var load_data_ = [];
			$('.field_material_input').each(function(){
				var id_material = $(this).data('id_mat'),
				material = parseInt($(this).attr('data-material') ) || 0,
				jasa = parseInt($(this).attr('data-jasa') ) || 0,
				sp = parseInt($(this).data('sp') ) || 0,
				urutan = $(this).parent().parent().parent().data('nomor'),
				lokasi = $(this).parent().parent().parent().parent().parent().find('.lok_per').val(),
				id_lokasi_lok = $(this).parent().parent().parent().data('lokasi_id'),
				sto = $(this).parent().parent().parent().parent().parent().find('.sto_m').val()
				sub_jenis = $(this).parent().parent().parent().parent().parent().find('.sub_jenis').val()
				jenis_khs = parseInt($(this).attr('data-design_mitra_id') ),
				isi = parseInt($(this).val() ) || 0;

				var get_design = design.find(o => o.id == id_material);

				load_data_.push({
					id: id_material,
					material: material,
					jasa: jasa,
					sp: sp,
					lokasi: lokasi,
					sto: sto,
					lokasi_id: id_lokasi_lok,
					rekon: isi,
					jenis_khs: jenis_khs,
					urutan: urutan,
					sub_jenis: sub_jenis,
					id_design: id_material,
					// designator: get_design.designator,
					namcomp: (jenis_khs == 16 ? 'Telkom Akses' : 'Mitra'),
					tambah: 0,
					kurang: 0,
					design_mitra_id: jenis_khs,
					id_boq_lokasi: id_lokasi_lok,
					jenis: get_design.jenis,
				});
			});
		})

		$('#modal_edit_material').on('show.bs.modal', function(){
			table_temp = ''
			table_temp += "<table class='table table_material table-striped table-bordered table-hover' style='width:100%'>";
			table_temp += "<thead class='thead-dark'>";
			table_temp += "<tr>";
			table_temp += "<th rowspan='2'>Designator</th>";
			table_temp += "<th rowspan='2'>Uraian</th>";
			table_temp += "<th colspan='3'>Paket 7</th>";
			table_temp += "</tr>";
			table_temp += "<tr>";
			table_temp += "<th>Jenis Material</th>";
			table_temp += "<th>Material</th>";
			table_temp += "<th>Jasa</th>";
			table_temp += "</tr>";
			table_temp += "</thead>";
			table_temp += "<tbody>";

			if(load_material.length){
				$.each(load_material, function(key2, val2) {
					$.each(design, function(key, val) {
						if(val.id == val2.id){
							table_temp += "<tr data-id="+val.id+" data-id_khs='"+val.id+"_"+val.design_mitra_id+"'>";
							table_temp += "<td>"+val.designator+"</td>";
							table_temp += "<td>"+val.uraian+"</td>";
							// table_temp += "<td><input type='checkbox' class='check_khs' data-id="+val.id+" " + (val2.design_mitra_id == 16 ? 'checked' : '' ) + "></td>";
							table_temp += "<td>"+(parseInt(val2.design_mitra_id) == 16 ? 'Telkom Akses' : 'Mitra')+"</td>";
							table_temp += "<td><input type='checkbox' class='check_mat' data-jenis_khs='"+val2.design_mitra_id+"' data-jenis='material' data-id="+val.id+" data-val_harga=" + (parseInt(val.material) || 0) + " " + (val2.mat != 0 ? 'checked' : '' ) + "> &nbsp;"+val.material.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".")+"</td>";
							table_temp += "<td><input type='checkbox' class='check_mat' data-jenis_khs='"+val2.design_mitra_id+"' data-jenis='jasa' data-id="+val.id+" data-val_harga=" + (parseInt(val.jasa) || 0) + " " + (val2.jasa != 0 ? 'checked' : '' ) + "> &nbsp;"+val.jasa.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".")+"</td>";
							table_temp += "</tr>";
						}
					});
				});
			}

			table_temp += "</tbody>";
			table_temp += "</table>";

			$('.data_mdl').html(table_temp)

			$('.check_mat').on('change', function(){
				var jenis = $(this).data('jenis'),
				val = parseInt($(this).data('val_harga') ) || 0;
				id = $(this).data('id'),
				jenis_khs = $(this).data('jenis_khs'),
				isi = 0;

				if($(this).is(':checked') ){
					isi = val;
				}

				$.each(load_material, function(k, v){
					if(v.id == id && jenis_khs == v.design_mitra_id){
						var val = $.grep(design, function(e){ return e.id == v.id; });
						if(jenis == 'material'){
							load_material[k].mat = isi;
							val[0].material_load = isi;
						}

						if(jenis == 'jasa'){
							load_material[k].jasa = isi;
							val[0].jasa_load = isi;
						}
					}
				})

				$('.field_material_input').each(function(){
					var id_material = parseInt($(this).data('id_mat') ),
					ini = $(this),
					material_input = parseInt($(this).attr('data-material') ) || 0,
					design_mitra_id = parseInt($(this).attr('data-design_mitra_id') ),
					id_lokasi_lok = $(this).parent().parent().parent().data('lokasi_id'),
					jasa_input = parseInt($(this).attr('data-jasa') ) || 0;

					if(id_material == id && design_mitra_id == jenis_khs)
					{
						if(jenis == 'material'){
							$(this).attr('data-material', isi);
							material_input = isi;
							$(this).parent().prev().prev().prev().text(isi.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") );
						}

						if(jenis == 'jasa'){
							$(this).attr('data-jasa', isi);
							jasa_input = isi;
							$(this).parent().prev().prev().text(isi.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") );
						}

						$.each(last_used_design, function(k, v){
							var find_filter_mat = v.filter(function (vv) { return vv.id_design == id_material && vv.id_boq_lokasi == id_lokasi_lok });

							if(find_filter_mat.length != 0){
								var sp = 0;
								if(find_filter_mat[0].material == material_input && find_filter_mat[0].jasa == jasa_input){
									sp = find_filter_mat[0].sp;
								}
								ini.parent().prev().text(sp)
								ini.attr('data-sp', sp)
							}
						});
					}
				});

				var material_tot_chck = 0,
				jasa_tot_chck = 0;

				$('.field_material_input').each(function(){
					var tfoot = $(this).parent().parent().parent().next(),
					body = $(this).parent().parent().parent();
					// console.log(tfoot, body);
					var material_in = 0,
					jasa_in = 0;
					body.each(function(){
						$(this).find('.field_material_input').each(function(){
							var id_material = parseInt($(this).data('id_mat') ),
							material = parseInt($(this).attr('data-material') ) || 0,
							jasa = parseInt($(this).attr('data-jasa') ) || 0,
							urutan = parseInt($(this).parent().parent().parent().data('nomor') ),
							isi_material = parseInt($(this).val() ) || 0;
							material_in += material * isi_material;
							jasa_in += jasa * isi_material;
							tfoot.find('.hrg_material').text(material_in.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
							tfoot.find('.hrg_jasa').text(jasa_in.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
							tfoot.find('.sum_me').text((material_in + jasa_in).toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
						})
					});

					var id_material = parseInt($(this).data('id_mat') ),
					material = parseInt($(this).attr('data-material') ) || 0,
					jasa = parseInt($(this).attr('data-jasa') ) || 0,
					urutan = parseInt($(this).parent().parent().parent().data('nomor') ),
					isi = parseInt($(this).val() ) || 0;
					material_tot_chck += material * isi;
					jasa_tot_chck += jasa * isi;
				});

				$('#total_mat').val(material_tot_chck.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
				$('#total_jasa').val(jasa_tot_chck.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
				$('#total_grand').val((material_tot_chck + jasa_tot_chck).toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
			});

			// $('.check_khs').on('change', function(){
			// 	var isi = 0,
			// 	id = $(this).data('id');

			// 	if($(this).is(':checked') ){
			// 		isi = 16;
			// 	}

			// 	$.each(load_material, function(k, v){
			// 		if(v.id == id){
			// 			load_material[k].design_mitra_id = isi;
			// 		}
			// 	})

			// 	$('.field_material_input').each(function(){
			// 		var id_material = parseInt($(this).data('id_mat') );

			// 		if(id_material == id)
			// 		{
			// 			$(this).attr('data-design_mitra_id', isi);
			// 		}
			// 	})
			// });
		});

		var sub_jenis_i = {!! json_encode($sub_jenis_all) !!},
		no_tbl = 0,
		last_used_design = {!! json_encode($data_item) !!},
		material_before = $("input[name='material_input']").val(),
		design = {!! json_encode($design) !!},
		load_material_ada = {!! json_encode($data_material) !!};

		if(material_before){
			material_before = JSON.parse(material_before)
			var final_ls = [],
			material_new = [];

			$.each(material_before, function(k, v){
				if(material_new.map(x => x.id_renew).indexOf(v.id_design +'_'+ v.design_mitra_id) === -1){
					var val = $.grep(design, function(e){ return e.id == v.id_design; })

					material_new.push({
						id_renew: v.id_design +'_'+ v.design_mitra_id,
						design_mitra_id: v.design_mitra_id,
						designator: val[0].designator,
						id_design: v.id_design,
						jasa: v.jasa,
						jenis: v.jenis,
						jenis_khs: v.jenis_khs,
						material: v.material,
						namcomp: v.namcomp,
						rekon: v.rekon,
						sp: v.sp,
						uraian: val[0].uraian,
					});
				}

				if(typeof(final_ls[v.urutan]) === 'undefined'){
					final_ls[v.urutan] = [];
				}

				final_ls[v.urutan].push(v);
			})

			load_mat = final_ls.filter(function (e) {return e != null;});
			last_used_design = load_mat;

			load_mat_all = material_new.filter(function (e) {return e != null;});
			load_material_ada = load_mat_all;
		}

		$(document).on('click', '.delete_buatan', function(){
			$(this).parent().parent().parent().remove();

			var material_tot_chck = 0,
			jasa_tot_chck = 0;

			$('.field_material_input').each(function(){
				var tfoot = $(this).parent().parent().parent().next(),
				body = $(this).parent().parent().parent();
				// console.log(tfoot, body);
				var material_in = 0,
				jasa_in = 0;
				body.each(function(){
					$(this).find('.field_material_input').each(function(){
						var id_material = parseInt($(this).data('id_mat') ),
						material = parseInt($(this).attr('data-material') ) || 0,
						jasa = parseInt($(this).attr('data-jasa') ) || 0,
						urutan = parseInt($(this).parent().parent().parent().data('nomor') ),
						isi_material = parseInt($(this).val() ) || 0;
						material_in += material * isi_material;
						jasa_in += jasa * isi_material;
						tfoot.find('.hrg_material').text(material_in.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
						tfoot.find('.hrg_jasa').text(jasa_in.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
						tfoot.find('.sum_me').text((material_in + jasa_in).toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
					})
				});

				var id_material = parseInt($(this).data('id_mat') ),
				material = parseInt($(this).attr('data-material') ) || 0,
				jasa = parseInt($(this).attr('data-jasa') ) || 0,
				urutan = parseInt($(this).parent().parent().parent().data('nomor') ),
				isi = parseInt($(this).val() ) || 0;
				material_tot_chck += material * isi;
				jasa_tot_chck += jasa * isi;
			});

			$('#total_mat').val(material_tot_chck.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
			$('#total_jasa').val(jasa_tot_chck.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
			$('#total_grand').val((material_tot_chck + jasa_tot_chck).toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
		});

		sto_i = {!! json_encode($all_sto) !!};

		$.each(load_material_ada, function(k1, v1){
			var val = $.grep(design, function(e){ return e.id == v1.id_design; });

			if(v1.design_mitra_id == 16){
				$("#material_ta").append('<option data-id="'+ v1.id_design +'" data-id_mitra="' +v1.design_mitra_id+ '" value="'+ v1.id_design +'" selected>'+  val[0].designator +' (' + v1.jenis + ' | ' + v1.namcomp + ')</option>').change();
			}else{
				$("#material_mitra").append('<option data-id="'+ v1.id_design +'" data-id_mitra="' +v1.design_mitra_id+ '" value="'+ v1.id_design +'" selected>'+  val[0].designator +' (' + v1.jenis + ' | ' + v1.namcomp + ')</option>').change();
			}
		});
		// console.log(last_used_design)
		rfc_per_lop = {!! json_encode($rfc_lop) !!};
		if(last_used_design.length != 0){
			var tbl_load = 0,
			load_data_html = [],
			material_tot = 0,
			jasa_tot = 0;

			$.each(last_used_design, function(k, v){
				tbl_load = ++k;

				add_material($('.ini_material') );

				var tabId = "compose" + tbl_load,
				delete_btn = '';

				if(v[0].lokasi.length == 0){
					delete_btn = '<button type="button" class="close closeTab">×</button>';
				}

				$('.nav-tabs').append('<li><a href="#' + tabId + '" class="nav-link" data-toggle="tab">LOP Nomor <span class="badge badge-primary"">'+tbl_load+'</span>'+delete_btn+'</a></li>');
				$('.tab-content').append('<div class="tab-pane" id="' + tabId + '"></div>');

				// $(this).tab('show');
				if(load_data_html.map(x => x.tabId).indexOf(tabId) === -1){
					load_data_html.push({
						'tabId': tabId,
					});
				}
			})

			no_tbl += tbl_load;

			$('#myTab a[href="#compose1"]').click()

			$('#total_mat').val(material_tot.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
			$('#total_jasa').val(jasa_tot.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
			$('#total_grand').val((material_tot + jasa_tot).toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
		}

		var data_pbu = {!! json_encode($data_pbu) !!},
		pekerjaan = data_pbu.pekerjaan,
		jenis_work = data_pbu.jenis_work,
		jenis_kontrak = data_pbu.jenis_kontrak,
		mitra_id = data_pbu.mitra_id,
		mitra_nm = data_pbu.mitra_nm,
		judul = data_pbu.judul,
		id_p = data_pbu.id_project,
		tahun_kerja = data_pbu.bulan_pengerjaan.split('-'),
		isi;
		// console.log(data_pbu)
		$('#material_ta').select2({
			width: '100%',
			placeholder: "Masukkan Nama Material",
			allowClear: true,
			ajax: {
				url: "/get_ajx/find_material",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					isi = params.term;
					return {
						searchTerm: params.term,
						pekerjaan : $('#pekerjaan').val(),
						tahun : $('#tahun').val(),
						bulan : $('#bulan').val(),
						jenis : 'all',
						witel : {!! json_encode(session('auth')->Witel_New) !!},
					};
				},
				processResults: function (response) {
					if(isi == null ){
						design = $.grep(design, function(element, index){return $.inArray(parseInt(element.design_mitra_id), [0, 16]) !== -1 }, true);
						design.push(...response);
					}
					return {
						results: response
					};
				},
				cache: true,
			},
			// language: {
			// 	noResults: function(){
			// 		return "Tidak Menemukan Material? Buat Material New Item</b>&nbsp;<a type='button' class='btn btn-info btn-sm' href='/Report/material_manual/input' target='_blank'><span class='fe fe-plus-circle fe-16'></span> Tambah No PKS Baru</a>";
			// 	}
			// },
			escapeMarkup: function (markup) {
				return markup;
			},
			templateSelection: function(data) {
				var $result = $(
					"<span data-id=" +data.id+ " data-id_mitra=" +data.id_mitra+ ">" + data.text + "</span>"
				);
				return $result;
			}
		});

		$('#material_mitra').select2({
			width: '100%',
			placeholder: "Masukkan Nama Material",
			allowClear: true,
			ajax: {
				url: "/get_ajx/find_material",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					isi = params.term;
					return {
						searchTerm: params.term,
						pekerjaan : $('#pekerjaan').val(),
						tahun : $('#tahun').val(),
						bulan : $('#bulan').val(),
						jenis : 'all',
						witel : {!! json_encode(session('auth')->Witel_New) !!},
					};
				},
				processResults: function (response) {
					if(isi == null && response.length != 0){
						design = $.grep(design, function(element, index){return $.inArray(parseInt(element.design_mitra_id), [0, 16]) !== -1 }, true);
						design.push(...response);
					}
					return {
						results: response
					};
				},
				cache: true,
			},
			// language: {
			// 	noResults: function(){
			// 		return "Tidak Menemukan Material? Buat Material New Item</b>&nbsp;<a type='button' class='btn btn-info btn-sm' href='/Report/material_manual/input' target='_blank'><span class='fe fe-plus-circle fe-16'></span> Tambah No PKS Baru</a>";
			// 	}
			// },
			escapeMarkup: function (markup) {
				return markup;
			},
			templateSelection: function(data) {
				var $result = $(
					"<span data-id=" +data.id+ " data-id_mitra=" +data.id_mitra+ ">" + data.text + "</span>"
				);
				return $result;
			}
		});

		// if(data_pbu.sto_pekerjaan != null){
		// 	var sto = data_pbu.sto_pekerjaan.split(','),
		// 	sto_load = [];
		// 	$.each(sto, function(k, v){
		// 		sto_load.push(v);
		// 	});

		// 	$('#sto').val(sto_load).change();
		// }

		$('#pekerjaan').val(pekerjaan).change();
		$('#jenis_work').val(jenis_work).trigger('change');
		$('#mitra_id').append(new Option(mitra_nm, mitra_id, false, false) ).trigger('change');
		$('#judul').val(judul);
		$('#id_project').val(id_p);
		$('#jenis_kontrak').val(jenis_kontrak).change();
		$('#tahun').val(tahun_kerja[0]).change();
		$('#bulan').val(tahun_kerja[1]).change();
		$('#jumlah_klm').val(last_used_design.length).change();

		$('#pekerjaan, #jenis_work, #mitra_id, #judul, #id_project, #jenis_kontrak, #tahun, #bulan').prop( "disabled", true );

		$('.submit_btn').on('click', function(e){

			$('#myTab a').each(function(k, v){
				var attr_href = $(this).attr('href').split('#')[1];

				if($.inArray(attr_href, already_open_tab) == -1 ){
					$("#myTab a[href='#"+attr_href+"']").click()
				}
			})
			// e.preventDefault();
			$.each($('.lok_per'), function(k, v){
				if($(this).val().trim().length == 0){
					e.preventDefault();
					$(this).css({
						'border-color': 'red'
					});
					toastr.options = {
						"closeButton": false,
						"debug": false,
						"newestOnTop": false,
						"progressBar": true,
						"positionClass": "toast-top-center",
						"preventDuplicates": false,
						"onclick": null,
						"showDuration": "300",
						"hideDuration": "1000",
						"timeOut": "5000",
						"extendedTimeOut": "1000",
						"showEasing": "swing",
						"hideEasing": "linear",
						"showMethod": "fadeIn",
						"hideMethod": "fadeOut"
					}
					toastr["warning"]("Lokasi Tidak Boleh Kosong!", "Gagal")
				}
			})

			var submit = [];

			$('.field_material_input').each(function(){
				var id_material = $(this).data('id_mat'),
				material = parseInt($(this).attr('data-material') ) || 0,
				jasa = parseInt($(this).attr('data-jasa') ) || 0,
				sp = parseInt($(this).data('sp') ) || 0,
				urutan = $(this).parent().parent().parent().data('nomor'),
				lokasi = $(this).parent().parent().parent().parent().parent().find('.lok_per').val(),
				id_lokasi_lok = $(this).parent().parent().parent().data('lokasi_id'),
				sto = $(this).parent().parent().parent().parent().parent().find('.sto_m').val()
				sub_jenis = $(this).parent().parent().parent().parent().parent().find('.sub_jenis').val()
				jenis_khs = parseInt($(this).attr('data-design_mitra_id') ),
				isi = parseInt($(this).val() ) || 0;

				var get_design = design.find(o => o.id == id_material);

				submit.push({
					id: id_material,
					material: material,
					jasa: jasa,
					sp: sp,
					lokasi: lokasi,
					sto: sto,
					lokasi_id: id_lokasi_lok,
					rekon: isi,
					jenis_khs: jenis_khs,
					urutan: urutan,
					sub_jenis: sub_jenis,
					id_design: id_material,
					// designator: get_design.designator,
					namcomp: (jenis_khs == 16 ? 'Telkom Akses' : 'Mitra'),
					tambah: 0,
					kurang: 0,
					design_mitra_id: jenis_khs,
					id_boq_lokasi: id_lokasi_lok,
					jenis: get_design.jenis,
				});
			});
			let hasil = JSON.stringify(submit);
			$("input[name='material_input']").val(hasil)
		})

		var load_mat_rfc = [];
		$.each(design, function(key, val) {
			$.each($('#material').val(), function(key2, val2) {
				if(val.id == val2){
					load_mat_rfc.push({
						id: val.id, text: val.designator
					})
				}
			});
		});

		// console.log(load_mat_rfc, $('#material').val())

		$('#material_rfc').select2({
			width: '100%',
			placeholder: 'Masukkan Material!',
			data: load_mat_rfc,
		});

		var rfc_load = {!! json_encode($rfc) !!};

		$.each(rfc_load, function(k, v){
			$.each(v.isi_m, function(kk, vv){
			})
		})

		$('#material_rfc').on('select2:select select2:unselect', function() {
			var count_selected_item = 0,
			isi_rfc_mat = $(this).val();
			$.each(isi_rfc_mat, function(kk, vv) {
				$('tr').each( function(kkk, vvv){
					if($(this).data('id') == vv){
						count_selected_item += parseInt(($(this).children().eq(5).children().val() ?? 0));
					}
				})
			})
			// console.log(count_selected_item)
			var text = "Jumlah Material Designator yang Diisi Adalah " + count_selected_item.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") + " Buah";

			$('#ket_kecil').text(text)
		})

		$('.edit_rfc_design').on('click', function(){
			var rfc = $(this).data('rfc'),
			design_nm = $(this).data('design'),
			jml_design = $(this).data('total_design');
			$('#judul_rfc').html('</u> Material <u>' + design_nm + '</u>' + ' Sebanyak <u>' + jml_design + '</u> Buah')
			$('#dessign_rfc').val(design_nm);
			var selected_val = [],
			count_all_item = 0;

			$.each(rfc_load, function(k, v){
				if(k == design_nm){
					$('#ket_rfc').val(v.keterangan);
					$.each(v.design, function(kk, vv){
						selected_val.push(vv.id_design);
						$('tr').each( function(kkk, vvv){
							if($(this).data('id') == vv.id_design){
								count_all_item += parseInt($(this).children().eq(5).children().val());
							}
						})
					})
				}
			})

			$('#ket_kecil').text("Jumlah Material Designator yang Dipilih Adalah " + count_all_item + " Buah")
			$('#tot_kecil').val(count_all_item)
			$('#material_rfc').val(selected_val).change();
		})

		$(document).on('keyup', '.field_material_input', function(event){
			var material_in = 0,
			jasa_in = 0;
			if( (parseInt($(this).val() ) || 0) != 0 && (parseInt($(this).val() ) || 0) != $(this).data('sp') )
			{
				$(this).parent().prev().eq(0).css({"background-color": "#f26d70", "color": "black"})
			}else{
				$(this).parent().prev().eq(0).css({"background-color": "", "color": ""})
			}

			$('.field_material_input').each(function(){
				var id_material = parseInt($(this).data('id_mat') ),
				material = parseInt($(this).attr('data-material') ) || 0,
				jasa = parseInt($(this).attr('data-jasa') ) || 0,
				urutan = parseInt($(this).parent().parent().parent().data('nomor') ),
				isi = parseInt($(this).val() ) || 0;
				material_in += material * isi;
				jasa_in += jasa * isi;

				$('#total_mat').val(material_in.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
				$('#total_jasa').val(jasa_in.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
				$('#total_grand').val((material_in + jasa_in).toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
			});

			var tfoot = $(this).parent().parent().parent().next(),
			body = $(this).parent().parent().parent();
			// console.log(tfoot, body);
			var material_in = 0,
			jasa_in = 0;
			body.each(function(){
				$(this).find('.field_material_input').each(function(){
					var id_material = parseInt($(this).data('id_mat') ),
					material = parseInt($(this).attr('data-material') ) || 0,
					jasa = parseInt($(this).attr('data-jasa') ) || 0,
					urutan = parseInt($(this).parent().parent().parent().data('nomor') ),
					isi = parseInt($(this).val() ) || 0;
					material_in += material * isi;
					jasa_in += jasa * isi;

					tfoot.find('.hrg_material').text(material_in.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
					tfoot.find('.hrg_jasa').text(jasa_in.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
					tfoot.find('.sum_me').text((material_in + jasa_in).toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
				})
			})
		})
  });
</script>
@endsection