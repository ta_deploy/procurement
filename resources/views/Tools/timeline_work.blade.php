@extends('layout')
@section('title', 'Dokumen Mitra')
@section('headerS')
<link rel="stylesheet" href="/css/dataTables.bootstrap4.css">
<style>
  th, td {
    text-align: center;
  }

  .centered{
    text-align: center;
  }
</style>
@endsection
@section('content')
@if (Session::has('alerts'))
  @foreach(Session::get('alerts') as $alert)
    <div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
  @endforeach
@endif
<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-12">
			<div class="row">
				<div class="col-md-12 my-4">
          <div class="mb-2 align-items-center">
            <div class="card shadow mb-4">
              <div class="card-body table-responsive">
                <h5 class="card-title">Tabel Tracking Pekerjaan</h5>
                <table id ="tb_rekon" class="table table-sm table-striped table-bordered table-hover">
                  <thead class="thead-dark">
                    <tr>
                      <th rowspan="2">Tanggal Mulai</th>
                      <th rowspan="2">Tanggal Selesai</th>
                      <th rowspan="2">Durasi</th>
                      <th colspan="6">Proses</th>
                    </tr>
                    <tr>
                      <th>Operation</th>
                      <th>Admin Mitra</th>
                      <th>Proc. Area</th>
                      <th>Commerce</th>
                      <th>TL</th>
                      <th>Proc. Regional</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($data_boq as $key => $val)
                      @php
                        $badge = 'badge-'. (empty($val['created_at']) ?  'secondary' : ($val['selisih_waktu'] ? 'success' : (empty($val['created_end']) ? 'info' : ($val['action'] == 'backward' ? 'warning' : '') ) ) );
                      @endphp
                      <tr>
                        <td>{{ $val['created_at'] ?? '-' }}</td>
                        <td>{{ $val['created_end'] ?? '-' }}</td>
                        <td>{{ $val['selisih_waktu'] ?? '-' }}</td>
                        <td><span class="badge badge-pill {{ $badge }}">{{ $val['operation'] }}</span></td>
                        <td><span class="badge badge-pill {{ $badge }}">{{ $val['mitra'] }}</span></td>
                        <td><span class="badge badge-pill {{ $badge }}">{{ $val['proc_area'] }}</span></td>
                        <td><span class="badge badge-pill {{ $badge }}">{{ $val['commerce'] }}</span></td>
                        <td><span class="badge badge-pill {{ $badge }}">{{ $val['tl'] }}</span></td>
                        <td><span class="badge badge-pill {{ $badge }}">{{ $val['proc_req'] }}</span></td>
                      </tr>
                    @endforeach
                    <tfoot>
                      <tr>
                        <td colspan="4"></td>
                        <td colspan="5"><span class="badge badge-pill badge-{{ $val['urutan'] == 99 ? 'success' : 'secondary' }}">Finish</span></td>
                      </tr>
                    </tfoot>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
				</div>
        <div class="col-md-12">
          <div class="card shadow">
            <div class="card-header">
              <strong class="card-title">Logging</strong>
            </div>
            <div class="card-body">
              <div class="list-group list-group-flush my-n3" style="overflow-y: auto; height:400px;">
                @forelse ($log as $l)
                  <div class="list-group-item">
                    <div class="row align-items-center">
                      <div class="col-auto">
                        <span class="circle circle-sm bg-{{ is_float($l->step_id) || $l->ss == 'Rollback' ? 'warning' : ($l->step_id < 14 ? 'info' : ($l->step_id == 14 ? 'success' : 'primary' ) ) }}">
                          <i class="fe fe-{{ $l->ss == 'Rollback' ? 'arrow-down' : (is_float($l->step_id) ? 'alert-triangle' : ($l->step_id < 14 ? 'arrow-up' : ($l->step_id == 14 ? 'check' : 'archive' ) ) ) }} fe-16 text-white"></i>
                        </span>
                      </div>
                      <div class="col">
                        <small><strong>{{ $l->mitra_nm }} | {{ $l->created_at }}</strong></small>
                        <div class="mb-2">Pekerjaan : <strong>{{ $l->jenis_work }}</strong><br/>Witel: <strong>{{ $l->witel_new }}</strong><br/> Judul: <strong>{{ $l->judul }}</strong> <br/>Status: <strong>{{ $l->ss == 'Rollback' ? 'Dikembalikan' : (is_float($l->step_id) ? 'DITOLAK' : ($l->step_id < 14 ? 'LANJUT' : ($l->step_id == 14 ? 'SELESAI' : 'DIPROSES' ) ) ) }} </strong>
                          @if($l->keterangan != 'Data Rekon Selesai!')
                            <br/>Loker: <strong>{{ $l->keterangan }}</strong>
                          @endif
                        </div>
                        <span class="badge badge-pill badge-{{ is_float($l->step_id) ? 'warning' : 'success' }}">{{ $l->aktor }}</span>
                      </div>
                      {{-- <div class="col-auto pr-0">
                        <small class="fe fe-more-vertical fe-16 text-muted"></small>
                      </div> --}}
                    </div>
                  </div>
                @empty
                @endforelse
              </div>
            </div>
          </div>
        </div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('footerS')

@endsection