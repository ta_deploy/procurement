@extends('layout')
@section('title', 'Detail Pekerjaan')
@section('headerS')
<link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" rel="stylesheet" />
<link rel="stylesheet" href="/css/dataTables.bootstrap4.css">
<style>
	@media (min-width: 768px) {
		.modal-xl {
			width: 100%;
		max-width:1200px;
		}
	}
</style>
@endsection
@section('content')
@if (Session::has('alerts'))
  @foreach(Session::get('alerts') as $alert)
    <div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
  @endforeach
@endif
@if ($data->active == 0)
<div class="alert alert-danger">Pekerjaan Ini Sudah <b>dihapus!</b></div>
@endif
<div class="modal fade" id="modal_check_rfc" tabindex="-1" role="dialog" aria-labelledby="modal_check_rfcTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body data_mdl" style=" overflow: hidden;">
				<form class="row" method="post">
					{{ csrf_field() }}
					<input type="hidden" name="rfc_id" id="rfc_id">
					<div class="col-md-4">
						<div class="form-group row">
							<label class="col-form-label col-md-4 pull-right" for="wbs_element">PID:</label>
							<div class="col-md-8">
								<p class="form-control input-transparent" readonly><b class="wbs_element"></b></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-md-4 pull-right" for="no_rfc">Nomor RFC:</label>
							<div class="col-md-8">
								<p class="form-control input-transparent" readonly><b class="no_rfc"></b></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-md-4 pull-right" for="plant">Gudang:</label>
							<div class="col-md-8">
								<p class="form-control input-transparent" readonly><b class="plant"></b></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-md-4 pull-right" for="type">Jenis:</label>
							<div class="col-md-8">
								<p class="form-control input-transparent" readonly><b class="type"></b></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-md-4 pull-right" for="mitra">Mitra:</label>
							<div class="col-md-8">
								<p class="form-control input-transparent" readonly><b class="mitra"></b></p>
							</div>
						</div>
					</div>
					<div class="col-md-8">
						<div class="table_rfc_material">
						</div>
					</div>
					<div class="col-md-12 submit_btn_rfc">
						<div class="form-group mb-3">
							<div class="custom-file">
								<button type="submit" class="btn btn-block btn-primary">Submit Nomor RFC</button>
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
			</div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row justify-content-center">
    <div class="col-12">
      <h2 class="page-title">Detail Pekerjaan <u>{{ $data->judul }} </u></h2>
      <p class="lead text-muted">{{ $data->pekerjaan }} Jenis {{ $data->jenis_work }} dengan {{ $data->jenis_kontrak }}</p>
      <div class="row">
        <div class="col-md-12">
          <div class="card shadow">
            <div class="card-body table-responsive">
							<div class="row">
								<div class="col-md-12">
									<div class="form-group row">
										<label class="col-form-label col-md-2" for="sto">Step Sebelumnya:</label>
										<div class="col-md-7">
											@php
												$ket_next = '-';

												if($step_information['current_step'])
												{
													$ket_next = $step_information['current_step']->nama .' ('. $step_information['current_step']->aktor_nama .' || '. ($step_information['current_step']->main_menu == 'Finishing' ? 'Pasca Kerja' : 'Pra Kerja');
												}
											@endphp
											<textarea style="resize:none; font-weight: bold;" cols='50' rows="1" class="form-control input-transparent pid" readonly>{{ $ket_next }}</textarea>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-form-label col-md-2" for="sto">Step Sekarang:</label>
										<div class="col-md-7">
											@php
												$ket_next = '-';

												if($step_information['step_selanjutnya'])
												{
													$ket_next = $step_information['step_selanjutnya']->nama .' ('. $step_information['step_selanjutnya']->aktor_nama .' || '. ($step_information['step_selanjutnya']->main_menu == 'Finishing' ? 'Pasca Kerja' : 'Pra Kerja') . ')';
												}
											@endphp
											<textarea style="resize:none; font-weight: bold;" cols='50' rows="1" class="form-control input-transparent pid" readonly>{{ $ket_next }}</textarea>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-form-label col-md-2" for="sto">Step Selanjutnya:</label>
										<div class="col-md-7">
											@php
												$ket_next = '-';

												if($step_information['next_next_step'])
												{
													$ket_next = $step_information['next_next_step']->nama .' ('. $step_information['next_next_step']->aktor_nama .' || '. ($step_information['next_next_step']->main_menu == 'Finishing' ? 'Pasca Kerja' : 'Pra Kerja') . ')';
												}
											@endphp
											<textarea style="resize:none; font-weight: bold;" cols='50' rows="2" class="form-control input-transparent pid" readonly>{{ $ket_next }}</textarea>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-form-label col-md-2" for="sto">Project ID:</label>
										<div class="col-md-7">
											<textarea style="resize:none; font-weight: bold;" cols='50' rows="4" class="form-control input-transparent pid" readonly>{{ $data->id_project }}</textarea>
										</div>
									</div>
									@if(in_array($data->step_id, [26, 28, 32]) )
										<div class="form-group row">
											<label class="col-form-label col-md-2" for="sto">Project ID Exceeded:</label>
											<div class="col-md-7">
												<textarea style="resize:none; font-weight: bold;" cols='50' rows="4" class="form-control input-transparent pid" readonly>{{ implode(', ', $pid_ex) }}</textarea>
											</div>
										</div>
									@endif
									<div class="form-group row">
										<label class="col-form-label col-md-2" for="sto">Keterangan:</label>
										<div class="col-md-7">
											<textarea style="resize:none; font-weight: bold;" cols='50' rows="2" class="form-control input-transparent pid" readonly>{{ $data->detail }}</textarea>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-form-label col-md-2" for="sto">User Terakhir Update:</label>
										<div class="col-md-4">
											<p class="form-control input-transparent" readonly>{{ $data->nama_modif ? $data->nama_modif .' ('. $data->modified_by .')' : '' }}</b></p>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-form-label col-md-2" for="sto">User Yang Membuat:</label>
										<div class="col-md-4">
											<p class="form-control input-transparent" readonly>{{ $data->nama_created ? $data->nama_created .' ('. $data->created_by .')' : '' }}</b></p>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-form-label col-md-2" for="sto">Terakhir Disubmit:</label>
										<div class="col-md-4">
											<p class="form-control input-transparent" readonly>{{ $data->tgl_last_update }}</b></p>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-form-label col-md-2" for="sto">Terakhir Diubah:</label>
										<div class="col-md-4">
											<p class="form-control input-transparent" readonly>{{ $data->modified_at }}</b></p>
										</div>
									</div>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="col-form-label" for="sto">Umur:</label>
												@php
													$selisih = date_diff(date_create($data->created_at), date_create(date('Y-m-d H:i:s') ) );
													$text = [];

													if( $selisih->format('%m') != 0 )
													{
														$text[] = $selisih->format('%m Bulan');
													}

													if( $selisih->format('%d') != 0 )
													{
														$text[] = $selisih->format('%d Hari');
													}

													if( $selisih->format('%h') != 0 )
													{
														$text[] = $selisih->format('%h Jam');
													}

													if( $selisih->format('%i') != 0 )
													{
														$text[] = $selisih->format('%i Menit');
													}
												@endphp
												<p class="form-control input-transparent" readonly>{{ implode(' ', $text) }}</b></p>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="col-form-label" for="sto">Lama Pekerjaan DiLoker <b>{{ $step_information['step_selanjutnya']->nama }}</b> :</label>
												@php
													$selisih = date_diff(date_create($data->tgl_last_update), date_create(date('Y-m-d H:i:s') ) );
													$text = [];

													if( $selisih->format('%m') != 0 )
													{
														$text[] = $selisih->format('%m Bulan');
													}

													if( $selisih->format('%d') != 0 )
													{
														$text[] = $selisih->format('%d Hari');
													}

													if( $selisih->format('%h') != 0 )
													{
														$text[] = $selisih->format('%h Jam');
													}

													if( $selisih->format('%i') != 0 )
													{
														$text[] = $selisih->format('%i Menit');
													}
												@endphp
												<p class="form-control input-transparent" readonly>{{ implode(' ', $text) }}</b></p>
											</div>
										</div>
									</div>
									@if($data->pekerjaan <> "PSB")
									<div class="form-group row">
										<div class="col-md-12">
											<a class="btn btn-primary btn-block rar_down" href="/download/full_rar/{{ $data->id }}"><i class="fe fe-download fe-16"></i>&nbsp;Generate Doc. Tagihan</a>
											<code>*Kata Sandi Untuk Dokumen Adalah: {{ $key }}</code>
										</div>
									</div>
									@endif
								</div>
								<div class="col-md-6 border-top">
									<div class="row">
										<div class="col-md-5">
											<div class="form-group">
												<label class="col-form-label" for="sto">Tanggal Surat Penetapan:</label>
												<p class="form-control input-transparent" readonly>{{ $data->tgl_s_pen }}</b></p>
											</div>
										</div>
										<div class="col-md-7">
											<div class="form-group">
												<label class="col-form-label" for="sto">Surat Penetapan:</label>
												<textarea style="resize:none; font-weight: bold;" cols='50' rows="2" class="form-control input-transparent no_sp" readonly>{{ $data->surat_penetapan }}</textarea>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-5">
											<div class="form-group">
												<label class="col-form-label" for="sto">Tanggal Surat pesanan:</label>
												<p class="form-control input-transparent" readonly>{{ $data->tgl_sp }}</b></p>
											</div>
										</div>
										<div class="col-md-7">
											<div class="form-group">
												<label class="col-form-label" for="sto">Surat Pesanan:</label>
												<textarea style="resize:none; font-weight: bold;" cols='50' rows="2" class="form-control input-transparent no_sp" readonly>{{ $data->no_sp }}</textarea>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="col-form-label" for="sto">Due Date</label>
												<p class="form-control input-transparent" readonly>{{ $data->durasi }}</b></p>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="col-form-label" for="sto">Durasi</label>
												<p class="form-control input-transparent" readonly>{{ $data->tgl_jatuh_tmp ?? 0 }} Hari</p>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6 border-top py-3">
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="col-form-label" for="sto">Mitra:</label>
												<p class="form-control input-transparent" readonly>{{ $data->nama_company }}</p>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="col-form-label" for="sto">PKS:</label>
												<textarea class="form-control input-transparent" style="resize:none; font-weight: bold;" cols='50' rows="2" readonly>{{ $data->pks }}</textarea>
											</div>
										</div>
									</div>
									<div class="form-group">
										@if($data->active == 1)
											<div class="row">
												@if(in_array(session('auth')->proc_level, explode(',', $step_information['step_selanjutnya']->aksesible) ) )
													@if(!in_array($data->step_id, [26, 32]))
														<div class="col-md-6">
															<a class="btn btn-block rar_down" style="background-color: aqua; color:black; margin-bottom: 10px;" href="/progress/{{ $data->id }}"><i class="fe fe-edit-3 fe-16"></i>&nbsp;Kerjakan!</a>
														</div>
													@endif
													@if($data->urutan > 1)
														<div class="col-md-6">
															<a class="btn btn-warning btn-block rar_down" style="margin-bottom: 10px;" href="/rollback/{{ $data->id }}"><i class="fe fe-corner-up-left fe-16"></i>&nbsp;Rollback!</a>
														</div>
													@endif
												@endif
											</div>
											@if(in_array(session('auth')->proc_level, [4, 99, 44]) || in_array(session('auth')->proc_level, [3]) && !in_array($data->step_id, [13, 20]) )
												<div class="row">
													<div class="col-md-6">
														<a class="btn btn-info btn-block rar_down" style="margin-bottom: 10px;" href="/opt/edit/po/{{ $data->id }}"><i class="fe fe-tool fe-16"></i>&nbsp;Edit Pekerjaan!</a>
													</div>
													@if(in_array(session('auth')->proc_level, [4, 99, 44]) || $data->step_id >= 7)
														<div class="col-md-6">
															<a class="btn btn-secondary btn-block rar_down" style="margin-bottom: 10px;" href="/opt/edit/kolom/{{ $data->id }}"><i class="fe fe-list fe-16"></i>&nbsp;Edit Kolom Material SP!</a>
														</div>
													@endif
												</div>
											@endif
											<div class="row">
												<div class="col-md-6">
													<a class="btn btn-primary btn-block rar_down" href="/edit_pekerjaan/{{ $data->id }}"><i class="fe fe-settings fe-16"></i>&nbsp;Edit Nomor!</a>
												</div>
												@if(in_array(session('auth')->proc_level, [4, 99, 44]) || in_array(session('auth')->proc_level, [3]) && !in_array($data->step_id, [13, 20]) )
													<div class="col-md-6">
														<a class="btn btn-danger btn-block rar_down" href="/opt/delete/upload/{{ $data->id }}"><i class="fe fe-trash fe-16"></i>&nbsp;Hapus WO!</a>
													</div>
												@endif
											</div>
										@endif
									</div>
								</div>
								@if($data->pekerjaan == 'PSB')
								<div class="col-md-12 border-top py-3 row">
									<div class="col-md-3">
										<a class="btn btn-primary" href="/Admin/download/filepsb/justifikasi/{{ $data->id }}/download"><i class="fe fe-download"></i>&nbsp; Justifikasi & Rincian SSL (PSB)</a>
									</div>
									@if(!empty($data->no_sp))
									<div class="col-md-3">
										<a class="btn btn-primary" href="/Admin/download/filepsb/surat_pesanan/{{ $data->id }}/download"><i class="fe fe-download"></i>&nbsp; Surat Pesanan (PSB)</a>
									</div>
									@endif
									@if(!empty($data->BAST))
									<div class="col-md-3">
										<a class="btn btn-primary" href="/Admin/download/filepsb/dokumen_pelengkap/{{ $data->id }}/download"><i class="fe fe-download"></i>&nbsp; Dokumen Pelengkap (PSB)</a>
									</div>
									@endif
								</div>
								@endif
								<div class="col-md-12 col-lg-12">
									<div class="card shadow eq-card mb-4">
										<div class="card-header">
											<strong class="card-title">Nilai SP</strong>
										</div>
										<div class="card-body">
											<div class="row items-align-center">
												<div class="col-4 text-center">
													<p class="text-muted mb-1">Material</p>
													<h6 class="mb-1">Rp. {{ number_format($data->total_material_sp, 0, '', '.') }}</h6>
												</div>
												<div class="col-4 text-center">
													<p class="text-muted mb-1">Jasa</p>
													<h6 class="mb-1">Rp. {{ number_format($data->total_jasa_sp , 0, '', '.') }}</h6>
												</div>
												<div class="col-4 text-center">
													<p class="text-muted mb-1">Total</p>
													<h6 class="mb-1">Rp. {{ number_format( ($data->total_material_sp + ($data->total_jasa_sp)), 0, '', '.') }}</h6>
												</div>
											</div>
										</div> <!-- .card-body -->
									</div> <!-- .card -->
								</div>
								@if ( ($data->total_material_rekon_plan + $data->total_jasa_rekon_plan) != 0)
									<div class="col-md-12 col-lg-12">
										<div class="card shadow eq-card mb-4">
											<div class="card-header">
												<strong class="card-title">Nilai Rekon Plan</strong>
											</div>
											<div class="card-body">
												<div class="row items-align-center">
													<div class="col-4 text-center">
														<p class="text-muted mb-1">SP Material</p>
														<h6 class="mb-1">Rp. {{ number_format($data->total_material_rek_sp_plan, 0, '', '.') }}</h6>
													</div>
													<div class="col-4 text-center">
														<p class="text-muted mb-1">SP Jasa</p>
														<h6 class="mb-1">Rp. {{ number_format($data->total_jasa_rek_sp_plan, 0, '', '.') }}</h6>
													</div>
													<div class="col-4 text-center">
														<p class="text-muted mb-1">Total sp</p>
														<h6 class="mb-1">Rp. {{ number_format( ($data->total_material_rek_sp_plan + $data->total_jasa_rek_sp_plan), 0, '', '.') }}</h6>
													</div>
												</div>
												<div class="row items-align-center border-top" style="margin-top: 15px; padding-top: 15px;">
													<div class="col-4 text-center">
														<p class="text-muted mb-1">Material</p>
														<h6 class="mb-1">Rp. {{ number_format($data->total_material_rekon_plan, 0, '', '.') }}</h6>
													</div>
													<div class="col-4 text-center">
														<p class="text-muted mb-1">Jasa</p>
														<h6 class="mb-1">Rp. {{ number_format($data->total_jasa_rekon_plan, 0, '', '.') }}</h6>
													</div>
													<div class="col-4 text-center">
														<p class="text-muted mb-1">Total Rekon</p>
														<h6 class="mb-1">Rp. {{ number_format( ($data->total_material_rekon_plan + $data->total_jasa_rekon_plan), 0, '', '.') }}</h6>
													</div>
												</div>
												<div class="row items-align-center border-top" style="margin-top: 15px; padding-top: 15px;">
													<div class="col-4 text-center">
														<p class="text-muted mb-1">Material Tambah</p>
														<h6 class="mb-1">Rp. {{ number_format($data->total_materialT_rek_plan, 0, '', '.') }}</h6>
													</div>
													<div class="col-4 text-center">
														<p class="text-muted mb-1">Material Kurang</p>
														<h6 class="mb-1">Rp. {{ number_format($data->total_materialK_rek_plan, 0, '', '.') }}</h6>
													</div>
													<div class="col-4 text-center">
														<p class="text-muted mb-1">Total Material</p>
														<h6 class="mb-1">Rp. {{ number_format( ($data->total_materialT_rek_plan + $data->total_materialK_rek_plan), 0, '', '.') }}</h6>
													</div>
												</div>
												<div class="row items-align-center" style="margin-top: 15px;">
													<div class="col-4 text-center">
														<p class="text-muted mb-1">Jasa Tambah</p>
														<h6 class="mb-1">Rp. {{ number_format($data->total_jasaT_rek_plan, 0, '', '.') }}</h6>
													</div>
													<div class="col-4 text-center">
														<p class="text-muted mb-1">Jasa Kurang</p>
														<h6 class="mb-1">Rp. {{ number_format($data->total_jasaK_rek_plan, 0, '', '.') }}</h6>
													</div>
													<div class="col-4 text-center">
														<p class="text-muted mb-1">Total Jasa</p>
														<h6 class="mb-1">Rp. {{ number_format( ($data->total_jasaT_rek_plan + $data->total_jasaK_rek_plan), 0, '', '.') }}</h6>
													</div>
												</div>
											</div> <!-- .card-body -->
										</div> <!-- .card -->
									</div>
								@endif
								@if ( ($data->total_material_rekon + $data->total_jasa_rekon) != 0)
									<div class="col-md-12 col-lg-12">
										<div class="card shadow eq-card mb-4">
											<div class="card-header">
												<strong class="card-title">Nilai Rekon</strong>
											</div>
											<div class="card-body">
												<div class="row items-align-center">
													<div class="col-4 text-center">
														<p class="text-muted mb-1">SP Material</p>
														<h6 class="mb-1">Rp. {{ number_format($data->total_material_rek_sp, 0, '', '.') }}</h6>
													</div>
													<div class="col-4 text-center">
														<p class="text-muted mb-1">SP Jasa</p>
														<h6 class="mb-1">Rp. {{ number_format($data->total_jasa_rek_sp, 0, '', '.') }}</h6>
													</div>
													<div class="col-4 text-center">
														<p class="text-muted mb-1">Total sp</p>
														<h6 class="mb-1">Rp. {{ number_format( ($data->total_material_rek_sp + $data->total_jasa_rek_sp), 0, '', '.') }}</h6>
													</div>
												</div>
												<div class="row items-align-center border-top" style="margin-top: 15px; padding-top: 15px;">
													<div class="col-4 text-center">
														<p class="text-muted mb-1">Material</p>
														<h6 class="mb-1">Rp. {{ number_format($data->total_material_rekon, 0, '', '.') }}</h6>
													</div>
													<div class="col-4 text-center">
														<p class="text-muted mb-1">Jasa</p>
														<h6 class="mb-1">Rp. {{ number_format($data->total_jasa_rekon, 0, '', '.') }}</h6>
													</div>
													<div class="col-4 text-center">
														<p class="text-muted mb-1">Total Rekon</p>
														<h6 class="mb-1">Rp. {{ number_format( ($data->total_material_rekon + $data->total_jasa_rekon), 0, '', '.') }}</h6>
													</div>
												</div>
												<div class="row items-align-center border-top" style="margin-top: 15px; padding-top: 15px;">
													<div class="col-4 text-center">
														<p class="text-muted mb-1">Material Tambah</p>
														<h6 class="mb-1">Rp. {{ number_format($data->total_materialT_rek, 0, '', '.') }}</h6>
													</div>
													<div class="col-4 text-center">
														<p class="text-muted mb-1">Material Kurang</p>
														<h6 class="mb-1">Rp. {{ number_format($data->total_materialK_rek, 0, '', '.') }}</h6>
													</div>
													<div class="col-4 text-center">
														<p class="text-muted mb-1">Total Material</p>
														<h6 class="mb-1">Rp. {{ number_format( ($data->total_materialT_rek + $data->total_materialK_rek), 0, '', '.') }}</h6>
													</div>
												</div>
												<div class="row items-align-center" style="margin-top: 15px;">
													<div class="col-4 text-center">
														<p class="text-muted mb-1">Jasa Tambah</p>
														<h6 class="mb-1">Rp. {{ number_format($data->total_jasaT_rek, 0, '', '.') }}</h6>
													</div>
													<div class="col-4 text-center">
														<p class="text-muted mb-1">Jasa Kurang</p>
														<h6 class="mb-1">Rp. {{ number_format($data->total_jasaK_rek, 0, '', '.') }}</h6>
													</div>
													<div class="col-4 text-center">
														<p class="text-muted mb-1">Total Jasa</p>
														<h6 class="mb-1">Rp. {{ number_format( ($data->total_jasaT_rek + $data->total_jasaK_rek), 0, '', '.') }}</h6>
													</div>
												</div>
											</div> <!-- .card-body -->
										</div> <!-- .card -->
									</div>
								@endif
								@if ($rfc)
									<div class="col-md-12 border-top py-3">
										@if(count($tanpa_rfc) )
											<div class="alert alert-danger">Ada {{ count($tanpa_rfc) }} Buah Material Tanpa RFC, Tetapi Ada RFR</div>
											<ul>
												@foreach($tanpa_rfc as $v)
													<li><div class="alert alert-warning">{{ $v }}</div></li>
												@endforeach
											</ul>
										@endif
										<table id ="tb_pid_l" class="table table-striped table-bordered" style="width: 100%">
											<thead class="thead-dark">
												<tr>
													<th class="hidden-xs">#</th>
													<th>ID Material</th>
													<th>Total Material</th>
													<th>NO. RFC/GI</th>
													<th>Status</th>
													<th>Volume Diberikan</th>
													<th>Volume Pemakaian</th>
													<th>Total Pemakaian</th>
													<th>NO. RFC/Pengembalian</th>
													<th>Status</th>
													<th>Volume Dikembalian</th>
													<th>Sisa Volume</th>
													<th>Keterangan</th>
												</tr>
											</thead>
											<tbody>
												@php
													$no = 0;
												@endphp
												@foreach($rfc as $key => $val)
													@php
														$first = true;
													@endphp
													@foreach ($val['isi_m'] as $k2 => $valc1_1)
														@php
															$second = true;
															$no_row = 0;
															array_map(function($x) use(&$no_row){
																$no_row += !isset($x['return']) ? 1 : count($x['return']);
															}, $val['isi_m']);
														@endphp
														@foreach($valc1_1 as $kkk => $valc1)
															@if ($kkk =='out')
																<tr>
																	@if ($first == true)
																		<td rowspan="{{ $no_row }}">{{ ++$no }}</td>
																		<td rowspan="{{ $no_row }}">{{ $key }}</td>
																		<td rowspan="{{ $no_row }}">{{ $val['total_m'] }}</td>
																	@endif
																	@php
																		$hitung_row_c = !isset($valc1_1['return']) ? 1 : count($valc1_1['return']);
																	@endphp
																	@if ($second == true)
																		<td rowspan="{{ $hitung_row_c }}">{{ $valc1['nomor_rfc_gi'] }}</td>
																		<td rowspan="{{ $hitung_row_c }}" style="background-color: {{ @$valc1['status_rfc_gi'] == 'LURUS' ? '' : 'red' }}; color: {{ @$valc1['status_rfc_gi'] == 'LURUS' ? '' : 'black' }}">{{ $valc1['status_rfc_gi'] }}</td>
																		<td rowspan="{{ $hitung_row_c }}">{{ $valc1['vol_give'] }}</td>
																		<td rowspan="{{ $hitung_row_c }}">
																			<a href="#" id="edit_{{ $valc1['id_pro'] }}" class="edit_used_rfc" data-type="text" data-pk="{'id_pro': {{ $valc1['id_pro'] }}, 'id_upload': {{ Request::segment(2) }} }" data-url="/submit_rfc_used" data-title="Masukkan Nomor Material Yang Dipakai">{{ ($valc1['use'] != 0 ? $valc1['use'] : '' ) }}</a>
																		</td>
																	@endif
																	@if ($first == true)
																		<td rowspan="{{ $no_row }}">{{ $val['total_use'] }}</td>
																	@endif
																	@if(isset($valc1_1['return']) )
																		<td>{{ current($valc1_1['return'])['nomor_rfc_return'] ?? '-' }}</td>
																		<td style="background-color: {{ @current($valc1_1['return'])['status_rfc_return'] == 'TIDAK LURUS' ? 'red' : '' }}; color: {{ @current($valc1_1['return'])['status_rfc_return'] == 'TIDAK LURUS' ? 'black' : '' }}">{{ current($valc1_1['return'])['status_rfc_return'] ?? '-' }}</td>
																		<td>{{ current($valc1_1['return'])['return'] ?? '-' }}</td>
																		@if ($second == true)
																			@php
																				$used_rfc = 0;

																				if(isset($valc1_1['return']) )
																				{
																					$r = 0;
																					foreach($valc1_1['return'] as $kx => $vx)
																					{
																						$r += $vx['return'];
																					}

																					$used_rfc = $valc1['vol_give'] - $r;
																				}
																			@endphp
																			<td rowspan="{{ $hitung_row_c }}">{{ $used_rfc }}</td>
																			@php
																				$second = false;
																			@endphp
																		@endif
																	@else
																		<td>-</td>
																		<td>-</td>
																		<td>-</td>
																		<td>0</td>
																	@endif
																	@if ($first == true)
																		<td rowspan="{{ $no_row }}">
																			<a href="#" id="edit_{{ $valc1['id_pro'] }}" class="edit_ket" data-type="text" data-pk="{'material': '{{ $key }}', 'id_upload': {{ Request::segment(2) }} }" data-url="/submit_ket_rfc" data-title="Masukkan Keterangan">{{ $val['keterangan'] ?? '' }}</a>
																		</td>
																		@php
																			$first = false;
																		@endphp
																	@endif
																</tr>
															@else
																@php
																	$new_valc1 = $valc1;
																	array_shift($new_valc1);
																@endphp
																@foreach (@$new_valc1 as $k4 => $v4)
																	<tr>
																		<td>{{ $v4['nomor_rfc_return'] ?? '-' }}</td>
																		<td style="background-color: {{ @$v4['status_rfc_return'] == 'TIDAK LURUS' ? 'red' : '' }}; color: {{ @$v4['status_rfc_return'] == 'TIDAK LURUS' ? 'black' : '' }}">{{ $v4['status_rfc_return'] ?? '-' }}</td>
																		<td>{{ $v4['return'] ?? '-' }}</td>
																	</tr>
																@endforeach
															@endif
														@endforeach
													@endforeach
												@endforeach
											</tbody>
										</table>
									</div>
								@endif
								<div class="col-md-12">
									<div class="row">
										<div class="col-md-6 col-xl-6 mb-6">
											<div class="card shadow">
												<div class="card-header">
													<span class="card-title">Proses Paling Lambat</span>
													{{-- <a class="float-right small text-muted" href="#!"><i class="fe fe-eye fe-12"></i></a> --}}
												</div>
												<div class="card-body my-n2">
													<div class="d-flex">
														<div class="flex-fill">
															<h4 class="mb-0">{{ !$log_timeline ? '' : $log_timeline[0]['selisih_waktu'] }}</h4>
														</div>
														<div class="flex-fill text-right">
															<p class="mb-0 small"><b>{{ !$log_timeline ? '' : $log_timeline[0]['nama_kerjaan'] }}</b></p>
															<p class="text-muted mb-0 small">{{ !$log_timeline ? '' : $log_timeline[0]['aktor_nama'] }}</p>
														</div>
													</div>
												</div> <!-- .card-body -->
											</div> <!-- .card -->
										</div> <!-- .col -->
										<div class="col-md-6 col-xl-3 mb-3">
											<div class="card shadow">
												<div class="card-body">
													<div class="row align-items-center my-1">
														<div class="col">
															<h4 class="mb-0">{{ count($step_information['remain_step']) }}</h4>
															<p class="small text-muted mb-0">Step Tersisa</p>
														</div>
														<div class="col-5">
															<div id="gauge2" class="gauge-container">
															</div>
														</div>
													</div>
												</div> <!-- .card-body -->
											</div> <!-- .card -->
										</div>
										<div class="col-md-6 col-xl-3 mb-3">
											<div class="card shadow mb-4">
												<div class="card-header">
													<span class="card-title">Jumlah Revisi dari</span>
													<a class="float-right small text-muted"><span>{{ count($step_information['lewat']) }} Step</span></a>
												</div>
												<div class="card-body my-n1">
													<div class="d-flex">
														<div class="flex-fill">
															<h4 class="mb-0">{{ count($step_information['revisi']) }} Buah</h4>
														</div>
														<div class="flex-fill text-right">
															<span class="sparkline inlinebar"><canvas style="display: inline-block; width: 40px; height: 32px; vertical-align: top;" width="40" height="32"></canvas></span></div>
													</div>
												</div> <!-- .card-body -->
											</div> <!-- .card -->
										</div>
									</div>
								</div>
								<div class="col-md-12">
									<div class="card shadow">
										<div class="card-header">
											<strong class="card-title">Logging</strong>&nbsp;<a type="button" target="_blank" href="/timeline/{{ Request::Segment(2) }}" style="color: white;" class="btn btn-info btn-sm"><i class="fe fe-eye fe-16"></i>&nbsp;Timeline</a>
										</div>
										<div class="card-body">
											<div class="list-group list-group-flush my-n3" style="overflow-y: auto; height:400px;">
												@forelse ($log as $l)
													<div class="list-group-item">
														<div class="row align-items-center">
															<div class="col-auto">
																<span class="circle circle-sm bg-{{ is_float($l->step_id) || $l->ss == 'Rollback' ? 'warning' : ($l->step_id < 13 ? 'info' : ($l->step_id == 14 ? 'success' : 'primary' ) ) }}">
																	<i class="fe fe-{{ $l->ss == 'Rollback' ? 'arrow-down' : (is_float($l->step_id) ? 'alert-triangle' : ($l->step_id < 14 ? 'arrow-up' : ($l->step_id == 14 ? 'check' : 'archive' ) ) ) }} fe-16 text-white"></i>
																</span>
															</div>
															<div class="col">
																<small><strong>{{ $l->mitra_nm }} | {{ $l->created_at }}</strong></small>
																<div class="mb-2">Pekerjaan : <strong>{{ $l->jenis_work }}</strong><br/>Witel: <strong>{{ $l->witel_new }}</strong><br/> Judul: <strong>{{ $l->judul }}</strong> <br/>Status: <strong>{{ $l->ss == 'Rollback' ? 'Dikembalikan' : (is_float($l->step_id) ? 'DITOLAK' : ($l->step_id < 14 ? 'LANJUT' : ($l->step_id == 14 ? 'SELESAI' : 'DIPROSES' ) ) ) }}</strong>
																	@if($l->keterangan != 'Data Rekon Selesai!')
																		<br/>Loker: <strong>{{ $l->keterangan }}</strong>
																	@endif
																</div>
																<span class="badge badge-pill badge-{{ is_float($l->step_id) ? 'warning' : 'success' }}">{{ $l->aktor }}</span>
															</div>
															{{-- <div class="col-auto pr-0">
																<small class="fe fe-more-vertical fe-16 text-muted"></small>
															</div> --}}
														</div>
													</div>
												@empty
												@endforelse
											</div>
										</div>
									</div>
								</div>
							</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('footerS')
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src='/js/jquery.dataTables.min.js'></script>
<script src='/js/dataTables.bootstrap4.min.js'></script>
<script src="/js/jquery.sparkline.min.js"></script>
<script src="/js/gauge.min.js"></script>
<script type="text/javascript">
	$(function(){
		var data_step = {!! json_encode($step_information) !!},
		data_sp = {!! json_encode($data) !!};

		var gauge2,
		svgg2=document.getElementById("gauge2");
		svgg2 && ( gauge2= Gauge(svgg2,
		{
			max:data_step['remain_step'].length + data_step['lewat'].length,
			value:0,
			dialStartAngle:-0,
			dialEndAngle:-90.001
		}),function e(){
			gauge2.setValue(0),
			gauge2.setValueAnimated(data_step['lewat'].length,1),
			window.setTimeout(e,6e3)
		}());

		// $('.table').DataTable({
		// 	autoWidth: true,
		// 	columnDefs: [
		// 		{
		// 			targets: 'no-sort',
		// 			orderable: false
		// 		}
		// 	],
		// 	lengthMenu: [
		// 		[16, 32, 64, -1],
		// 		[16, 32, 64, "All"]
		// 	]
		// });

		$('.lihat_detail_rfc').click(function(){
			$('#modal_check_rfc').modal('toggle');
			$('.submit_btn_rfc').hide();
			$.ajax({
				url:"/get_ajx/inventory",
				type:"GET",
				data: {
					isi: $(this).data('no_rfc'),
					id_pbu: data_sp.id
				},
				dataType: 'json',
				success: (function(data){
					var data_rfc = data.inventory[0]
					console.log(data_rfc)
					$('.no_rfc').html(data_rfc.no_rfc)
					$('.plant').html(data_rfc.plant)
					$('.wbs_element').html(data_rfc.wbs_element)
					$('.type').html( (data_rfc.type == 'Out' ? 'Pengeluaran' : 'Pengembalian') )
					$('.mitra').html(data_rfc.mitra)

					var table_html = "<table class='tbl_material table table-striped table-bordered table-hover'>";
					table_html += "<thead class='thead-dark'>";
					table_html += "<tr>";
					table_html += "<th>Material</th>";
					table_html += "<th>Satuan</th>";
					table_html += "<th>Jumlah</th>";
					table_html += "</tr>";
					table_html += "</thead>";
					table_html += "<tbody>";

					$.each(data.inventory, function(key, val){
						table_html += "<tr>";
						table_html += "<td>"+ val.material +"</td>";
						table_html += "<td>"+ val.satuan +"</td>";
						table_html += "<td>"+ val.quantity +"</td>";
						table_html += "</tr>";
					});

					table_html += "</tbody>";
					table_html += "</table>";

					$('.table_rfc_material').html(table_html)
					var data_select = [];

					$.each(data.pid, function(key, val){
						data_select.push({
							id: key,
							text: val.lokasi +' (' + val.pid + ')'
						})
					});

					console.log(data_select)

					$('.tbl_material').DataTable({
						autoWidth: true,
        		bFilter: false,
						lengthMenu: [
							[16, 32, 64, -1],
							[16, 32, 64, "All"]
						]
					});

				})
			})
		})
	});
</script>
@endsection