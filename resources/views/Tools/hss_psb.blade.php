@extends('layout')
@section('title', 'HSS PSB')
@section('headerS')
<link rel="stylesheet" href="/css/dataTables.bootstrap4.css">
<style>
	th, td {
		text-align: center;
	}
</style>
@endsection
@section('content')
<div class="container-fluid">
	@if (Session::has('alerts'))
		@foreach(Session::get('alerts') as $alert)
			<div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
		@endforeach
	@endif
	<div class="row justify-content-center">
		<div class="col-12">

			<button type="button" class="btn mb-2 btn-primary" data-toggle="modal" data-target="#verticalModal-input"><i class="fe fe-edit"></i> Input HSS PSB & Migrasi</button>
			<div class="modal fade" id="verticalModal-input" tabindex="-1" role="dialog" aria-labelledby="verticalModalTitle-input" aria-hidden="true">
				<div class="modal-dialog modal-dialog-centered modal-xl" role="document">
					<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="verticalModalTitle-input">Harga Satuan Pekerjaan Pasang Baru (PSB) & Migrasi</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<form id="form_hss_psb" class="row" method="post" action="/tools/hss_psb/input" enctype="multipart/form-data" autocomplete="off">
							{{ csrf_field() }}
							<div class="col-md-12">
								<div class="form-row">

									<div class="form-group col-md-3">
										<label class="col-form-label pull-right" for="project_id">Project ID</label>
										<input type="text" class="form-control input-transparent" name="project_id" required>
									</div>
									<div class="form-group col-md-3">
										<label class="col-form-label pull-right" for="p1_ku">1P (PSB)</label>
										<input type="text" class="form-control input-transparent" name="p1_ku" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" required>
									</div>
									<div class="form-group col-md-3">
										<label class="col-form-label pull-right" for="p2_inet_voice_ku">2P Inet+Voice (PSB)</label>
										<input type="text" class="form-control input-transparent" name="p2_inet_voice_ku" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" required>
									</div>
									<div class="form-group col-md-3">
										<label class="col-form-label pull-right" for="p2_inet_iptv_ku">2P Inet+IPTV (PSB)</label>
										<input type="text" class="form-control input-transparent" name="p2_inet_iptv_ku" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" required>
									</div>
									<div class="form-group col-md-3">
										<label class="col-form-label pull-right" for="p3_ku">3P (PSB)</label>
										<input type="text" class="form-control input-transparent" name="p3_ku" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" required>
									</div>
									<div class="form-group col-md-3">
										<label class="col-form-label pull-right" for="p1">1P (PSB + Survey)</label>
										<input type="text" class="form-control input-transparent" name="p1" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" required>
									</div>
									<div class="form-group col-md-3">
										<label class="col-form-label pull-right" for="p2_inet_voice">2P Inet+Voice (PSB + Survey)</label>
										<input type="text" class="form-control input-transparent" name="p2_inet_voice" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" required>
									</div>
									<div class="form-group col-md-3">
										<label class="col-form-label pull-right" for="p2_inet_iptv">2P Inet+IPTV (PSB + Survey)</label>
										<input type="text" class="form-control input-transparent" name="p2_inet_iptv" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" required>
									</div>
									<div class="form-group col-md-3">
										<label class="col-form-label pull-right" for="p3">3P (PSB + Survey)</label>
										<input type="text" class="form-control input-transparent" name="p3" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" required>
									</div>
									<div class="form-group col-md-3">
										<label class="col-form-label pull-right" for="migrasi_1p">Migrasi Service 1P2P</label>
										<input type="text" class="form-control input-transparent" name="migrasi_1p" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" required>
									</div>
									<div class="form-group col-md-3">
										<label class="col-form-label pull-right" for="migrasi_2p">Migrasi Service 1P3P</label>
										<input type="text" class="form-control input-transparent" name="migrasi_2p" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" required>
									</div>
									<div class="form-group col-md-3">
										<label class="col-form-label pull-right" for="migrasi_3p">Migrasi Service 2P3P</label>
										<input type="text" class="form-control input-transparent" name="migrasi_3p" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" required>
									</div>
									<div class="form-group col-md-3">
										<label class="col-form-label pull-right" for="stb_tambahan">IKR Addon STB</label>
										<input type="text" class="form-control input-transparent" name="stb_tambahan" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" required>
									</div>
									<div class="form-group col-md-3">
										<label class="col-form-label pull-right" for="smartcam_only">IndiHome Smart</label>
										<input type="text" class="form-control input-transparent" name="smartcam_only" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" required>
									</div>
									<div class="form-group col-md-3">
										<label class="col-form-label pull-right" for="wifiext">WiFi Extender</label>
										<input type="text" class="form-control input-transparent" name="wifiext" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" required>
									</div>
									<div class="form-group col-md-3">
										<label class="col-form-label pull-right" for="plc">Power Line Communication (PLC)</label>
										<input type="text" class="form-control input-transparent" name="plc" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" required>
									</div>
									<div class="form-group col-md-3">
										<label class="col-form-label pull-right" for="lme_wifi">LME-WiFi PT-1</label>
										<input type="text" class="form-control input-transparent" name="lme_wifi" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" required>
									</div>
									<div class="form-group col-md-3">
										<label class="col-form-label pull-right" for="lme_ap_indoor">LME-AP-Indoor</label>
										<input type="text" class="form-control input-transparent" name="lme_ap_indoor" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" required>
									</div>
									<div class="form-group col-md-3">
										<label class="col-form-label pull-right" for="lme_ap_outdoor">LME-AP-Outdoor</label>
										<input type="text" class="form-control input-transparent" name="lme_ap_outdoor" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" required>
									</div>
									<div class="form-group col-md-3">
										<label class="col-form-label pull-right" for="ont_premium">ONT Premium</label>
										<input type="text" class="form-control input-transparent" name="ont_premium" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" required>
									</div>
									<div class="form-group col-md-3">
										<label class="col-form-label pull-right" for="migrasi_stb">Migrasi STB</label>
										<input type="text" class="form-control input-transparent" name="migrasi_stb" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" required>
									</div>
									<div class="form-group col-md-3">
										<label class="col-form-label pull-right" for="change_stb">Change STB</label>
										<input type="text" class="form-control input-transparent" name="change_stb" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" required>
									</div>
									<div class="form-group col-md-3">
										<label class="col-form-label pull-right" for="instalasi_ipcam">IP Camera</label>
										<input type="text" class="form-control input-transparent" name="instalasi_ipcam" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" required>
									</div>
									<div class="form-group col-md-3">
										<label class="col-form-label pull-right" for="insert_tiang">PU-S7.0-140</label>
										<input type="text" class="form-control input-transparent" name="insert_tiang" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" required>
									</div>
									<div class="form-group col-md-3">
										<label class="col-form-label pull-right" for="insert_tiang_9">PU-S9.0-140</label>
										<input type="text" class="form-control input-transparent" name="insert_tiang_9" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" required>
									</div>

									<div class="form-group col-md-12">
										<button class="btn btn-block btn_submit btn-primary">Create</button>
									</div>
								</div>
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn mb-2 btn-secondary" data-dismiss="modal">Close</button>
					</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12 my-4">
					<div class="card shadow mb-4">
						<div class="card-body table-responsive">
							<h5 class="card-title">Daftar Harga Satuan Pekerjaan Pasang Baru (PSB) & Migrasi</h5>

							<table id="tb_hss" class="table table-striped table-bordered table-hover">
								<thead class="thead-dark">
									<tr>
										<th class="hidden-xs">#</th>
										<th>ID Regional</th>
										<th>Regional</th>
										<th>Witel MS2N</th>
										<th>Witel </th>
										<th class="hidden-xs">Action</th>
									</tr>
								</thead>
								<tbody id="data_table">
									@php $num = 1; @endphp
									@foreach($data as $d)
									<tr>
										<td class="hidden-xs">{{ $num++ }}</td>
										<td>{{ $d->id_regional }}</td>
										<td>{{ $d->regional }}</td>
										<td>{{ $d->witel }}</td>
										<td>{{ $d->witel_ms2n }}</td>
										<td>
											<button type="button" class="btn mb-2 btn-primary" data-toggle="modal" data-target="#verticalModal-edit-{{ $d->id_hss_witel }}"><i class="fe fe-edit"></i>&nbsp;Edit</button>
											<div class="modal fade" id="verticalModal-edit-{{ $d->id_hss_witel }}" tabindex="-1" role="dialog" aria-labelledby="verticalModalTitle-edit-{{ $d->id_hss_witel }}" aria-hidden="true">
												<div class="modal-dialog modal-dialog-centered modal-xl" role="document">
													<div class="modal-content">
													<div class="modal-header">
														<h5 class="modal-title" id="verticalModalTitle-edit-{{ $d->id_hss_witel }}">Harga Satuan Pekerjaan Pasang Baru (PSB) & Migrasi [{{ $d->id_regional }}-{{ $d->regional }}-{{ $d->witel }}]</h5>
														<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<span aria-hidden="true">&times;</span>
														</button>
													</div>
													<div class="modal-body">
														<form id="form_hss_psb" class="row" method="post" action="/tools/hss_psb/{{ $d->witel }}" enctype="multipart/form-data" autocomplete="off">
															{{ csrf_field() }}
															<div class="col-md-12">
																<div class="form-row">

																	<div class="form-group col-md-3">
																		<label class="col-form-label pull-right" for="project_id">Project ID</label>
																		<input type="text" class="form-control input-transparent" name="project_id" value="{{ $d->project_id }}" required>
																	</div>
																	<div class="form-group col-md-3">
																		<label class="col-form-label pull-right" for="p1_ku">1P (PSB)</label>
																		<input type="text" class="form-control input-transparent" name="p1_ku" value="{{ $d->p1_ku }}" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" required>
																	</div>
																	<div class="form-group col-md-3">
																		<label class="col-form-label pull-right" for="p2_inet_voice_ku">2P Inet+Voice (PSB)</label>
																		<input type="text" class="form-control input-transparent" name="p2_inet_voice_ku" value="{{ $d->p2_inet_voice_ku }}" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" required>
																	</div>
																	<div class="form-group col-md-3">
																		<label class="col-form-label pull-right" for="p2_inet_iptv_ku">2P Inet+IPTV (PSB)</label>
																		<input type="text" class="form-control input-transparent" name="p2_inet_iptv_ku" value="{{ $d->p2_inet_iptv_ku }}" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" required>
																	</div>
																	<div class="form-group col-md-3">
																		<label class="col-form-label pull-right" for="p3_ku">3P (PSB)</label>
																		<input type="text" class="form-control input-transparent" name="p3_ku" value="{{ $d->p3_ku }}" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" required>
																	</div>
																	<div class="form-group col-md-3">
																		<label class="col-form-label pull-right" for="p1">1P (PSB + Survey)</label>
																		<input type="text" class="form-control input-transparent" name="p1" value="{{ $d->p1 }}" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" required>
																	</div>
																	<div class="form-group col-md-3">
																		<label class="col-form-label pull-right" for="p2_inet_voice">2P Inet+Voice (PSB + Survey)</label>
																		<input type="text" class="form-control input-transparent" name="p2_inet_voice" value="{{ $d->p2_inet_voice }}" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" required>
																	</div>
																	<div class="form-group col-md-3">
																		<label class="col-form-label pull-right" for="p2_inet_iptv">2P Inet+IPTV (PSB + Survey)</label>
																		<input type="text" class="form-control input-transparent" name="p2_inet_iptv" value="{{ $d->p2_inet_iptv }}" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" required>
																	</div>
																	<div class="form-group col-md-3">
																		<label class="col-form-label pull-right" for="p3">3P (PSB + Survey)</label>
																		<input type="text" class="form-control input-transparent" name="p3" value="{{ $d->p3 }}" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" required>
																	</div>
																	<div class="form-group col-md-3">
																		<label class="col-form-label pull-right" for="migrasi_1p">Migrasi Service 1P2P</label>
																		<input type="text" class="form-control input-transparent" name="migrasi_1p" value="{{ $d->migrasi_1p }}" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" required>
																	</div>
																	<div class="form-group col-md-3">
																		<label class="col-form-label pull-right" for="migrasi_2p">Migrasi Service 1P3P</label>
																		<input type="text" class="form-control input-transparent" name="migrasi_2p" value="{{ $d->migrasi_2p }}" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" required>
																	</div>
																	<div class="form-group col-md-3">
																		<label class="col-form-label pull-right" for="migrasi_3p">Migrasi Service 2P3P</label>
																		<input type="text" class="form-control input-transparent" name="migrasi_3p" value="{{ $d->migrasi_3p }}" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" required>
																	</div>
																	<div class="form-group col-md-3">
																		<label class="col-form-label pull-right" for="stb_tambahan">IKR Addon STB</label>
																		<input type="text" class="form-control input-transparent" name="stb_tambahan" value="{{ $d->stb_tambahan }}" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" required>
																	</div>
																	<div class="form-group col-md-3">
																		<label class="col-form-label pull-right" for="smartcam_only">IndiHome Smart</label>
																		<input type="text" class="form-control input-transparent" name="smartcam_only" value="{{ $d->smartcam_only }}" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" required>
																	</div>
																	<div class="form-group col-md-3">
																		<label class="col-form-label pull-right" for="wifiext">WiFi Extender</label>
																		<input type="text" class="form-control input-transparent" name="wifiext" value="{{ $d->wifiext }}" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" required>
																	</div>
																	<div class="form-group col-md-3">
																		<label class="col-form-label pull-right" for="plc">Power Line Communication (PLC)</label>
																		<input type="text" class="form-control input-transparent" name="plc" value="{{ $d->plc }}" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" required>
																	</div>
																	<div class="form-group col-md-3">
																		<label class="col-form-label pull-right" for="lme_wifi">LME-WiFi PT-1</label>
																		<input type="text" class="form-control input-transparent" name="lme_wifi" value="{{ $d->lme_wifi }}" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" required>
																	</div>
																	<div class="form-group col-md-3">
																		<label class="col-form-label pull-right" for="lme_ap_indoor">LME-AP-Indoor</label>
																		<input type="text" class="form-control input-transparent" name="lme_ap_indoor" value="{{ $d->lme_ap_indoor }}" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" required>
																	</div>
																	<div class="form-group col-md-3">
																		<label class="col-form-label pull-right" for="lme_ap_outdoor">LME-AP-Outdoor</label>
																		<input type="text" class="form-control input-transparent" name="lme_ap_outdoor" value="{{ $d->lme_ap_outdoor }}" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" required>
																	</div>
																	<div class="form-group col-md-3">
																		<label class="col-form-label pull-right" for="ont_premium">ONT Premium</label>
																		<input type="text" class="form-control input-transparent" name="ont_premium" value="{{ $d->ont_premium }}" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" required>
																	</div>
																	<div class="form-group col-md-3">
																		<label class="col-form-label pull-right" for="migrasi_stb">Migrasi STB</label>
																		<input type="text" class="form-control input-transparent" name="migrasi_stb" value="{{ $d->migrasi_stb }}" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" required>
																	</div>
																	<div class="form-group col-md-3">
																		<label class="col-form-label pull-right" for="change_stb">Change STB</label>
																		<input type="text" class="form-control input-transparent" name="change_stb" value="{{ $d->change_stb }}" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" required>
																	</div>
																	<div class="form-group col-md-3">
																		<label class="col-form-label pull-right" for="instalasi_ipcam">IP Camera</label>
																		<input type="text" class="form-control input-transparent" name="instalasi_ipcam" value="{{ $d->instalasi_ipcam }}" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" required>
																	</div>
																	<div class="form-group col-md-3">
																		<label class="col-form-label pull-right" for="insert_tiang">PU-S7.0-140</label>
																		<input type="text" class="form-control input-transparent" name="insert_tiang" value="{{ $d->insert_tiang }}" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" required>
																	</div>
																	<div class="form-group col-md-3">
																		<label class="col-form-label pull-right" for="insert_tiang_9">PU-S9.0-140</label>
																		<input type="text" class="form-control input-transparent" name="insert_tiang_9" value="{{ $d->insert_tiang_9 }}" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" required>
																	</div>

																	<div class="form-group col-md-12">
																		<button class="btn btn-block btn_submit btn-primary">Create</button>
																	</div>
																</div>
															</div>
														</form>
													</div>
													<div class="modal-footer">
														<button type="button" class="btn mb-2 btn-secondary" data-dismiss="modal">Close</button>
													</div>
													</div>
												</div>
											</div>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('footerS')
<script src='/js/jquery.dataTables.min.js'></script>
<script src="/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript">
	$(function(){
		$('#tb_hss').DataTable();
	});
</script>
@endsection