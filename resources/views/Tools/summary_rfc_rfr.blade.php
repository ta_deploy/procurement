@extends('layout')
@section('title', 'Setting Pekerjaan')
@section('headerS')
<link rel="stylesheet" href="/css/dataTables.bootstrap4.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.css" integrity="sha512-6S2HWzVFxruDlZxI3sXOZZ4/eJ8AcxkQH1+JjSe/ONCEqR9L4Ysq5JdT5ipqtzU7WHalNwzwBv+iE51gNHJNqQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<style type="text/css">
	.pull-right {
		text-align: right;
	}
</style>
@endsection
@section('content')
@if (Session::has('alerts'))
  @foreach(Session::get('alerts') as $alert)
    <div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
  @endforeach
@endif
<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-md-12">
			<h2 class="page-title">Summary PID</h2>
			<div class="card-deck" style="display: block">
        <div class="card-body">
          <div class="form-group row">
            <label class="col-form-label col-md-2 pull-right" for="pid">Masukkan PID</label>
            <div class="col-md-10">
              <select class="form-control input-transparent" id="pid" multiple></select>
            </div>
          </div>
        </div>
				<a type="button" class="btn btn-info btn-sm copy_table" style="color: white; float: right; margin-bottom:15px;"><i class="fe fe-alert-copy fe-16"></i>&nbsp;Copy Table!</a>
				<div class="isi_data"></div>
		  </div>
	  </div>
  </div>
</div>
@endsection
@section('footerS')
<script src='/js/jquery.dataTables.min.js'></script>
<script src='/js/dataTables.bootstrap4.min.js'></script>
<script src='/js/select2.min.js'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.js" integrity="sha512-lbwH47l/tPXJYG9AcFNoJaTMhGvYWhVM9YI43CT+uteTRRaiLCui8snIgyAN8XWgNjNhCqlAUdzZptso6OCoFQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script type="text/javascript">
	$(function(){
		$('#pid').select2({
			width: '100%',
			placeholder: 'PID Bisa Dipilih Lebih Dari Satu',
			allowClear: true,
      minimumInputLength: 4,
			ajax: {
				url: "/get_ajx/search_inventory_pid",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						searchTerm: params.term,
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true,
				success: function(value) {
					// console.log(value)
				}
			}
		});

		$(".copy_table").on('click', function(){
			var urlField = document.querySelector('.table_copas_m'),
			range = document.createRange();

			toastr.options = {
				"debug": false,
				"newestOnTop": false,
				"progressBar": true,
				"positionClass": "toast-top-center",
				"preventDuplicates": false,
				"onclick": null,
				"showDuration": "300",
				"hideDuration": "1000",
				"timeOut": "3000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut"
			}

			if(urlField == null)
			{
				toastr["warning"]("Tabel Tidak Ada!", "Gagal!");
				return false;
			}

			range.selectNode(urlField);
			window.getSelection().addRange(range);
			document.execCommand('copy');



			toastr["success"]("Tabel Berhasil Disalin!", "Sukses")
		})

		$('#pid').on('select2:select select2:unselect', function(){
			let isi = $(this).val();

			$.ajax({
				url:"/get_ajx/collect_inventory_material",
				type:"GET",
				data: {
					id : isi,
				},
				dataType: 'json',
				async: false
			}).done(function(data){
				let load_data = {};
				var material_load = [],
				material_load_all = [],
				material_load_out = [],
				material_load_return = [],
				total_return = 0,
				total_out = 0;

				$.each(data, function(k, v){
					if(material_load.map(x => x.material).indexOf(v.material) === -1){
						material_load.push({
							material: v.material,
							quantity: 0
						});

						material_load_all.push({
							material: v.material,
							quantity: 0
						});

						material_load_out.push({
							material: v.material,
							quantity: 0
						});

						material_load_return.push({
							material: v.material,
							quantity: 0
						});
					}
				});

				$.each(data, function(k, v){
					if(typeof(load_data[v.no_rfc]) == 'undefined'){
						load_data[v.no_rfc] = {};

						load_data[v.no_rfc].no_rfc      = v.no_rfc;
						load_data[v.no_rfc].program     = v.program;
						load_data[v.no_rfc].plant       = v.plant;
						load_data[v.no_rfc].wbs_element = v.wbs_element;
						load_data[v.no_rfc].mitra       = v.mitra;
						load_data[v.no_rfc].type        = v.type;

						load_data[v.no_rfc].list_material = material_load.map(i => Object.assign({}, i));
					}

					let key = load_data[v.no_rfc].list_material.findIndex(x => x.material == v.material);

					if(key !== -1){
						if(v.type == 'Out'){
							material_load_all[key].quantity += v.quantity;
							material_load_out[key].quantity += v.quantity;
							total_out += v.quantity;
						}else{
							material_load_all[key].quantity -= v.quantity;
							material_load_return[key].quantity += v.quantity;
							total_return += v.quantity;
						}

						load_data[v.no_rfc].list_material[key].quantity = v.quantity.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
					}
				});
				// console.log(material_load_all)
				//4901384331

				var html_table = `<table class="table table-bordered table-striped table_copas_m table-responsive" style="width: 100%">
        <thead class='thead-dark'>
				<tr>
				<td>WBS Element</td>
				<td>No RFC</td>
				<td>Type</td>
				<td>Program</td>
				<td>Plant</td>
				<td>Mitra</td>`;

				$.each(material_load, function(kk, vv){
					html_table += `<td>${vv.material}</td>`;
				});

				html_table += `</tr></thead><tbody>`;

				$.each(load_data, function(k, v){
					html_table += `<tr>`;
					html_table += `<td>${v.wbs_element}</td>`;
					html_table += `<td>${v.no_rfc}</td>`;
					html_table += `<td>${v.type}</td>`;
					html_table += `<td>${v.program}</td>`;
					html_table += `<td>${v.plant}</td>`;
					html_table += `<td>${v.mitra}</td>`;

					$.each(v.list_material, function(k4, v4){
						html_table += `<td>${v4.quantity != 0 ? v4.quantity : '-'}</td>`;
					})

					html_table += `</tr>`;
				});

				html_table += `</tbody>`;
				html_table += `<tfoot>`;
				html_table += `<tr>`;
				html_table += `<td colspan='6'>Total Out</td>`;

				$.each(material_load_out, function(k, v){
					html_table += `<td>${v.quantity != 0 ? v.quantity.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") : '-'}</td>`
				})

				html_table += `</tr>`;
				html_table += `<tr>`;
				html_table += `<td colspan='6'>Total Return</td>`;

				$.each(material_load_return, function(k, v){
					html_table += `<td>${v.quantity != 0 ? v.quantity.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") : '-'}</td>`
				})

				html_table += `</tr>`;
				html_table += `<tr>`;
				html_table += `<td colspan='6'>Total Semua</td>`;

				$.each(material_load_all, function(k, v){
					html_table += `<td>${v.quantity != 0 ? v.quantity.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") : '-'}</td>`
				})

				html_table += `</tr>`;
				html_table += `</tfoot>`;
				html_table += `</table> <code>*Jika data tidak ditemukan, maka bisa ditunggu hari besok, atau hubungi programmer <br/> *Segala perhitungan <b>VALID</b> dan tidak dibuat - buat <br/> *Data 100% berasal dari RFC/RFR yang di <i>Submit</i></code>`;

				$('.isi_data').html(html_table);
			});
		});
	})
</script>
@endsection