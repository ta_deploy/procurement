<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/favicon.ico">
    <title>@yield('title')</title>
    <!-- Simple bar CSS -->
    <link rel="stylesheet" href="/css/simplebar.css">
    <!-- Fonts CSS -->
    <link href="https://fonts.googleapis.com/css2?family=Overpass:ital,wght@0,100;0,200;0,300;0,400;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    <!-- Icons CSS -->
    <link rel="stylesheet" href="/css/feather.css">
    <!-- Date Range Picker CSS -->
    <!-- App CSS -->
    <link rel="stylesheet" href="/css/app-light.css" id="lightTheme">
    <link rel="stylesheet" href="/css/select2.css">
    <link rel="stylesheet" href="/css/dropzone.css">
    <link rel="stylesheet" href="/css/uppy.min.css">
    <link rel="stylesheet" href="/css/jquery.steps.css">
    <link rel="stylesheet" href="/css/jquery.timepicker.css">
    <link rel="stylesheet" href="/css/quill.snow.css">
    <link rel="stylesheet" href="/css/app-dark.css" id="darkTheme" disabled>
    @yield('headerS')
  </head>
  <body class="vertical light">
    <div class="wrapper">
      <nav class="topnav navbar navbar-light">
        <button type="button" class="navbar-toggler text-muted mt-2 p-0 mr-3 collapseSidebar">
          <i class="fe fe-menu navbar-toggler-icon"></i>
        </button>
        {{-- <form class="form-inline mr-auto searchform text-muted">
          <input class="form-control mr-sm-2 bg-transparent border-0 pl-4 text-muted" type="search" placeholder="Type something..." aria-label="Search">
        </form> --}}
        <ul class="nav">
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle text-muted pr-0" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              {{-- <span class="avatar avatar-sm mt-2">
                <img src="https://apps.telkomakses.co.id/wimata/photo/crop_{{ session('auth')->id_karyawan }}.jpg" alt="..." class="avatar-img rounded-circle">
              </span> --}}
              &nbsp;
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
              <a class="dropdown-item" href="/logout">Logout</a>
              {{-- <a class="dropdown-item" href="#">Activities</a> --}}
            </div>
          </li>
        </ul>
      </nav>
      <aside class="sidebar-left border-right bg-white shadow" id="leftSidebar" data-simplebar>
        <a href="#" class="btn collapseSidebar toggle-btn d-lg-none text-muted ml-2 mt-3" data-toggle="toggle">
          <i class="fe fe-x"><span class="sr-only"></span></i>
        </a>
        <nav class="vertnav navbar navbar-light">
          <!-- nav bar -->
          <div class="w-100 mb-4 d-flex">
            {{-- <a class="navbar-brand mx-auto mt-2 flex-fill text-center" href="/">
              <svg version="1.1" id="logo" class="navbar-brand-img brand-sm" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 120 120" xml:space="preserve">
                <g>
                  <polygon class="st0" points="78,105 15,105 24,87 87,87  " />
                  <polygon class="st0" points="96,69 33,69 42,51 105,51   " />
                  <polygon class="st0" points="78,33 15,33 24,15 87,15  " />
                </g>
              </svg>
            </a> --}}
            <a href="/"><img style="vertical-align:middle; width: 100%; height: auto;" src="/images/logo.png"></a>
          </div>
        </nav>
      </aside>
      <main role="main" class="main-content">
        @if (Session::has('alerts'))
          @foreach(Session::get('alerts') as $alert)
            <div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
          @endforeach
        @endif
        <div class="container-fluid">
          <div class="row justify-content-center">
            <div class="col-md-12">
              <h2 class="page-title">Register User SSO</h2>
              <div class="card-deck" style="display: block">
                <form class="row" method="post" action='/sso_saved_updated'>
                  {{ csrf_field() }}
                  <input type="hidden" value="{{ $user_detail }}" name="user_detail">
                  <div class="card shadow mb-4">
                    <div class="card-body">
                      <div class="form-group row">
                        <label class="col-form-label col-md-2 pull-right" for="regional">Regional</label>
                        <div class="col-md-10">
                          <select id="regional" name="regional" class="form-control">
                          </select>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-form-label col-md-2 pull-right" for="witel">Witel</label>
                        <div class="col-md-10">
                          <select id="witel" name="witel" class="form-control">
                          </select>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-form-label col-md-2 pull-right" for="mitra">Mitra</label>
                        <div class="col-md-10">
                          <select id="mitra" name="mitra" class="form-control">
                          </select>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-form-label col-md-2 pull-right" for="level">Level</label>
                        <div class="col-md-10">
                          <select id="level" name="level" class="form-control">
                            @foreach($get_user as $v)
                              <option value="{{ $v->aktor_nama }}">{{ $v->aktor_nama }}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                  <button type="submit" class="btn btn-lg btn-primary btn-block">Register</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </main>
    </div>
    <script src="/js/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/simplebar.min.js"></script>
    <script src="/js/jquery.stickOnScroll.js"></script>
    <script src="/js/tinycolor-min.js"></script>
    <script src="/js/config.js"></script>
    <script src="/js/apps.js"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src='/js/jquery.dataTables.min.js'></script>
    <script src='/js/dataTables.bootstrap4.min.js'></script>
    <script src='/js/select2.min.js'></script>
    <script src="/js/moment.min.js"></script>
    <script src="/js/jquery.timepicker.js"></script>
    <script src="/js/daterangepicker.js"></script>
    <script type="text/javascript">
      $(function(){
        var data = {!! json_encode($all_regional) !!},
        regional= [];

        $.each(data, function(k, v){
          regional[v.reg] = v.reg;
        });

        var raw_reg_object = Object.assign({}, regional);
        var reg_object = Object.values(raw_reg_object);
        reg_object.sort();

        $('#regional').select2({
          width: '100%',
          placeholder: 'Silahkan Pilih Regional',
          data: reg_object
        });

        $('#regional').val(null).change();

        $('#regional').on('select2:select change', function(){
          var isi = $(this).val();

          $('#witel').empty().change();

          var val = $.grep(data, function(e){ return e.reg == isi; }),
          witel= [];

          $.each(val, function(k, v){
            witel[v.witel_ms2n] = v.witel_ms2n;
          });

          var raw_witel_obj = Object.assign({}, witel);
          var witel_obj = Object.values(raw_witel_obj);
          witel_obj.sort();

          $('#witel').select2({
            width: '100%',
            placeholder: 'Silahkan Pilih Witel',
            data: witel_obj
          });

          $('#witel').val(null).change();
        })

        $('#witel').on('select2:select change', function(){
          var isi = $(this).val();

          $('#mitra').empty().change();

          var val = $.grep(data, function(e){ return e.witel_ms2n == isi; }),
          mitra= [];

          console.log(val)

          $.each(val, function(k, v){
            mitra[v.mitra] = v.mitra;
          });

          var raw_mitra_obj = Object.assign({}, mitra);
          var mitra_obj = Object.values(raw_mitra_obj);
          mitra_obj.sort();

          $('#mitra').select2({
            width: '100%',
            placeholder: 'Silahkan Pilih Mitra',
            data: mitra_obj
          });
        })

        $('#level').select2({
          width: '100%',
          placeholder: 'Silahkan Pilih Level'
        });
      });
    </script>
  </body>
</html>