@extends('layout')
@section('title', 'Bank TTD Dokumen')
@section('headerS')
<link rel="stylesheet" href="/css/dataTables.bootstrap4.css">
<link rel="stylesheet" href="/css/daterangepicker.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<style type="text/css">
	.pull-right {
		text-align: right;
	}
</style>
@endsection
@section('content')
@if (Session::has('alerts'))
  @foreach(Session::get('alerts') as $alert)
    <div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
  @endforeach
@endif
<div class="modal fade" id="history_user" tabindex="-1" role="dialog" aria-labelledby="history_userTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-xl" role="document">
    <div class="modal-content">
			<div class="modal-header">
				<strong style="color:black;">List History User <b><u><span class="title"></span></u></b></strong>
				<button type="button" class="btn btn-default btn-xs" style="float:right;" data-dismiss="modal">Close</button>
			</div>
			<div class="modal-body tbl_hs">

			</div>
		</div>
  </div>
</div>
<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-md-12">
			<h2 class="page-title">{{ empty($data) ? 'Tambah User Baru' : 'Edit User' }} {{ $title }}</h2>
			<div class="card-deck" style="display: block">
				<code>*Data TTD ini dibuat berdasarkan untuk witel {{ session('auth')->Witel_New }}</code>
				<form class="row" method="post" enctype="multipart/form-data">
					{{ csrf_field() }}
					<div class="card shadow mb-4">
						<div class="card-body">
							<div class="form-group row">
								<label class="col-form-label col-md-2 pull-right" for="witel">Witel</label>
								<div class="col-md-10">
									<select id="witel" name="witel" class="form-control">
										@foreach ($all_witel as $v)
											<option value="{{ $v->Witel }}">{{ $v->Witel }}</option>
										@endforeach
									</select>
								</div>
							</div>
							@foreach ($data[1] as $v)
								<div class="form-group row">
									<label class="col-form-label col-md-2 pull-right" for="{{ $v['id'] }}">{{ $v['text'] }}</label>
									<div class="col-md-10">
										{{-- <select id="{{ $v['id'] }}" name="{{ $v['id'] }}" class="form-control find_nik"></select> --}}
										<a type="button" class="btn btn-info btn-sm btn-block modal_h" data-toggle="modal" data-target="#history_user" data-title="{{ $v['text'] }}" data-id_f="{{ $v['id'] }}" data-work="{{ $title }}" style="color: #ffffff"><i class="fe fe-eye fe-16"></i>&nbsp;Lihat User</a>
										<code>{{ $v['avail'] == 0 && $v['expired'] == 0 ? '*Data Kosong!' : ($v['avail'] != 0 ? '' : '*Terdeteksi User Yang Expired, Tidak Ada Pengganti!') }}</code>
									</div>
								</div>
							@endforeach
						</div>
					</div>
					<div class="col-md-12">
						<div class="row">
							<div class="card shadow col-md-6 mb-4">
								<div class="card-header">
									<strong class="card-title">Dokumen Berdasarkan Nilai Pekerjaan</strong>
								</div>
								<div class="card-body">
									@foreach ($data[2] as $v)
										<div class="form-group">
											<label class="col-form-label" for="{{ $v['id'] }}">{{ $v['text'] }}</label>
											{{-- <select id="{{ $v['id'] }}" name="{{ $v['id'] }}" class="form-control find_nik"></select> --}}
											<a type="button" class="btn btn-info btn-sm btn-block modal_h" data-toggle="modal" data-target="#history_user" data-title="{{ $v['text'] }}" data-id_f="{{ $v['id'] }}" data-work="{{ $title }}" style="color: #ffffff"><i class="fe fe-eye fe-16"></i>&nbsp;Lihat User</a>
											<code>{{ $v['avail'] == 0 && $v['expired'] == 0 ? '*Data Kosong!' : ($v['avail'] != 0 ? '' : '*Terdeteksi User Yang Expired, Tidak Ada Pengganti!') }}</code>
										</div>
									@endforeach
								</div>
							</div>
							<div class="card shadow col-md-6 mb-4">
								<div class="card-header">
									<strong class="card-title">Dokumen BA</strong>
								</div>
								<div class="card-body">
									@foreach ($data[3] as $v)
										<div class="form-group">
											<label class="col-form-label" for="{{ $v['id'] }}">{{ $v['text'] }}</label>
											{{-- <select id="{{ $v['id'] }}" name="{{ $v['id'] }}" class="form-control find_nik"></select> --}}
											<a type="button" class="btn btn-info btn-sm btn-block modal_h" data-toggle="modal" data-target="#history_user" data-title="{{ $v['text'] }}" data-id_f="{{ $v['id'] }}" data-work="{{ $title }}" style="color: #ffffff"><i class="fe fe-eye fe-16"></i>&nbsp;Lihat User</a>
											<code>{{ $v['avail'] == 0 && $v['expired'] == 0 ? '*Data Kosong!' : ($v['avail'] != 0 ? '' : '*Terdeteksi User Yang Expired, Tidak Ada Pengganti!') }}</code>
										</div>
									@endforeach
								</div>
							</div>
						</div>
					</div>
					{{-- <div class="card shadow mb-4">
						<div class="card-body">
							<div class="form-group mb-3">
								<div class="custom-file">
									<button type='submit' class="btn btn-block btn-primary">Submit</button>
								</div>
							</div>
						</div>
					</div> --}}
				</form>
			</div>
		</div>
	</div>
</div>
@endsection
@section('footerS')
<meta name="csrf-token" content="{{ csrf_token() }}">
<script src='/js/jquery.dataTables.min.js'></script>
<script src='/js/dataTables.bootstrap4.min.js'></script>
<script src='/js/select2.min.js'></script>
<script src="/js/moment.min.js"></script>
<script src="/js/jquery.timepicker.js"></script>
<script src="/js/daterangepicker.js"></script>
<script type="text/javascript">
	$(function(){
		$('#pekerjaan').select2({
			width: '100%',
			placeholder: 'Silahkan Pilih Pekerjaan'
		});

		$('#witel').select2({
			width: '100%',
			placeholder: 'Silahkan Pilih Witel'
		});

		function initializeSelect2(selectElementObj) {
      selectElementObj.select2({
        width: '100%',
				placeholder: "Masukkan Nik",
				allowClear: true,
				minimumInputLength: 4,
				ajax: {
					url: "/get_ajx/user_find",
					dataType: 'json',
					delay: 250,
					data: function (params) {
						return {
							searchTerm: params.term
						};
					},
					processResults: function (response) {
						return {
							results: response
						};
					},
					cache: true,
					success: function(value) {
						console.log(value)
					}
				}
      });
    }

		$('.modal_h').on('click', function(){
			var id_field = $(this).attr('data-id_f'),
			title = $(this).attr('data-title'),
			work = $(this).attr('data-work'),
			witel = $('#witel').val();

			$.ajax({
				url:"/get_ajx/history_user",
				type:"GET",
				data: {
					id: id_field,
					work: work,
					witel: witel
				},
				dataType: 'json',
				success: (function(data){
					$('.title').html(title);

					var tbl_history = "<form method='POST' id='submit_user' class='row' action='/Admin/dok_ttd/insup'>";
					tbl_history += '{!! csrf_field() !!}';
					tbl_history += "<input type='hidden' name='rule_dok' value='" + id_field +"'>";
					tbl_history += "<input type='hidden' name='pekerjaan' value='" + work +"'>";
					tbl_history += "<input type='hidden' name='witel' value='" + witel +"'>";
					tbl_history += "<input type='hidden' name='title' value='" + title +"'>";
					tbl_history += "<div class='col-md-6'>";
					tbl_history += "<select id='get_nik' name='get_nik' class='form-control find_nik'></select>";
					tbl_history += "</div>";
					tbl_history += "<div class='col-md-6'>";
					tbl_history += "<input type='text' id='date_jbtn' name='date_jbtn' class='form-control input-sm date-picker' required='required' value=''>";
					tbl_history += "</div>";
					tbl_history += "</form>";
					tbl_history += "<div class='col-md-12'>";
					tbl_history += "<div class='card-body table-responsive'>";
					tbl_history += "<table id ='tb_history' class='table table-striped table-bordered table-hover'>";
					tbl_history += "<thead class='thead-dark'>";
					tbl_history += "<tr>";
					tbl_history += "<th>Nama User</th>";
					tbl_history += "<th>Nik</th>";
					tbl_history += "<th>Jabatan</th>";
					tbl_history += "<th>Masa Jabatan</th>";
					tbl_history += "<th>Status Jabatan</th>";
					tbl_history += "<th class='no-sort'>Action</th>";
					tbl_history += "</tr>";
					tbl_history += "</thead>";
					tbl_history += "<tbody>";

					$.each(data, function(k, v){
						console.log(v)
						tbl_history += "<tr>";
						tbl_history += "<td>" + v.user + "</td>";
						tbl_history += "<td>" + v.nik + "</td>";
						tbl_history += "<td>" + v.jabatan + "</td>";
						tbl_history += "<td>" + (v.tgl_start ?? '-') + " s/d " + (v.tgl_end ?? '-') + "</td>";
						tbl_history += "<td><span style='color: #ffffff' class='badge badge-pill badge-" + (v.status_user == 'Aktif' ? 'success' : 'danger') + "'>" + v.status_user + "</span></td>";
						tbl_history += "<td><a href='/Admin/delete_user/dok/"+v.id+"' style='color: #ffffff' class='btn btn-sm btn-danger'><i class='fe fe-trash fe-16'></i>&nbsp;Hapus</a></td>";
						tbl_history += "</tr>";
					});

					tbl_history += "</tbody>";
					tbl_history += "</table>";
					tbl_history += '</div>';
					tbl_history += '</div>';

					tbl_history += "<div class='form-group mb-3'>";
					tbl_history += "<div class='custom-file'>";
					tbl_history += "<button type='submit' class='btn btn-block btn_submit btn-primary'><i class='fe fe-file-plus fe-16'></i>&nbsp;Submit User TTD</button>";
					tbl_history += "</div>";
					tbl_history += "</div>";

					$('.tbl_hs').html(tbl_history);

					$('.btn_submit').on('click', function(){
						$('#submit_user').submit();
					})

					$('#tb_history').DataTable({
						autoWidth: true,
						aaSorting: [],
						columnDefs: [
							{
								targets: 'no-sort',
								orderable: false
							}
						],
						lengthMenu: [
							[16, 32, 64, -1],
							[16, 32, 64, "All"]
						]
					});

					$('.date-picker').daterangepicker({
						timePicker: false,
						showDropdowns: true,
						locale:{
							format: 'YYYY-MM-DD',
						}
					});

					$('.find_nik').each(function (){
						initializeSelect2($(this) );
					});
				})
			})
		});

		$('#witel').on('change', function(){
			$.ajax({
				url:"/get_ajx/load_user_ttd",
				type:"GET",
				data: {
					isi: $(this).val(),
					job: {!! json_encode($title) !!}
				},
				dataType: 'json',
				success: (function(data){
					$('.find_nik').empty().change();

					if(data.length != 0){
						$.each(data, function(k, v){
							$('#'+v.rule_dok).append(new Option(v.user + ' (' + v.nik + ')', v.id_user, false, false) ).trigger('change');
						});
					}
				})
			})
		});

		$("#witel").val({!!json_encode($witel) !!}).trigger('change');
	})
</script>
@endsection