@extends('layout')
@section('title', 'Pekerjaan Budget Exceeded')
@section('headerS')
<link rel="stylesheet" href="/css/dataTables.bootstrap4.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" rel="stylesheet" />
<style>
	@media (min-width: 768px) {
		.modal-xl {
			width: 100%;
		  max-width:1200px;
		}
	}

  .fade{
    z-index: 34000 !important;
  }

  .modal{
    z-index: 35000 !important;
  }

  .show_rfc{
    color: white !important;
    margin-bottom: 10px !important;
  }
</style>
@endsection
@section('content')
@if (Session::has('alerts'))
  @foreach(Session::get('alerts') as $alert)
    <div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
  @endforeach
@endif
<div class="modal fade" id="modal_check_rfc" tabindex="-1" role="dialog" aria-labelledby="modal_check_rfcTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
				<a class="btn btn-info dwn_rfc" style="color: #fff" href="#"><i class="fe fe-download"></i>&nbsp;PDF</a>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style=" overflow: hidden;">
				<form class="row" method="post">
					{{ csrf_field() }}
					<input type="hidden" name="rfc_id" id="rfc_id">
					<div class="col-md-4">
						<div class="form-group row">
							<label class="col-form-label col-md-4 pull-right" for="wbs_element">PID:</label>
							<div class="col-md-8">
								<p class="form-control input-transparent" readonly><b class="wbs_element"></b></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-md-4 pull-right" for="no_rfc">Nomor RFC:</label>
							<div class="col-md-8">
								<p class="form-control input-transparent" readonly><b class="no_rfc"></b></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-md-4 pull-right" for="plant">Gudang:</label>
							<div class="col-md-8">
								<p class="form-control input-transparent" readonly><b class="plant"></b></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-md-4 pull-right" for="type">Jenis:</label>
							<div class="col-md-8">
								<p class="form-control input-transparent" readonly><b class="type"></b></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-md-4 pull-right" for="mitra">Mitra:</label>
							<div class="col-md-8">
								<textarea class="form-control input-transparent mitra" rows="2" readonly></textarea>
							</div>
						</div>
					</div>
					<div class="col-md-8">
						<div class="table_rfc_material">
						</div>
					</div>
					<div class="col-md-12 submit_btn_rfc">
						<div class="form-group mb-3">
							<div class="custom-file">
								<button type="submit" class="btn btn-block btn-primary">Submit Nomor RFC</button>
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
			</div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="col-12">
    <div class="row items-align-baseline">
      <div class="col-md-12 col-lg-12">
        <div class="card shadow eq-card mb-4">
          <div class="card-header">
            <strong class="card-title">Nilai Rekon</strong>
          </div>
          <div class="card-body">
            <div class="row items-align-center">
              <div class="col-4 text-center">
                <p class="text-muted mb-1">Material Rekon</p>
                <h6 class="mb-1">Rp. {{ number_format($material_rekon, 0, '', '.') }}</h6>
              </div>
              <div class="col-4 text-center">
                <p class="text-muted mb-1">Jasa Rekon</p>
                <h6 class="mb-1">Rp. {{ number_format($jasa_rekon, 0, '', '.') }}</h6>
              </div>
              <div class="col-4 text-center">
                <p class="text-muted mb-1">Total</p>
                <h6 class="mb-1">Rp. {{ number_format($material_rekon + $jasa_rekon, 0, '', '.') }}</h6>
              </div>
            </div>
            <div class="row items-align-center border-top" style="margin-top: 15px;">
              <div class="col-4 text-center">
                <p class="text-muted mb-1">Material RFC</p>
                <h6 class="mb-1">Rp. {{ number_format($material_rfc, 0, '', '.') }}</h6>
              </div>
              <div class="col-4 text-center">
                <p class="text-muted mb-1">Jasa RFC</p>
                <h6 class="mb-1">Rp. {{ number_format($jasa_rfc, 0, '', '.') }}</h6>
              </div>
              <div class="col-4 text-center">
                <p class="text-muted mb-1">Total</p>
                <h6 class="mb-1">Rp. {{ number_format($material_rfc + $jasa_rfc, 0, '', '.') }}</h6>
              </div>
            </div>
          </div> <!-- .card-body -->
        </div> <!-- .card -->
      </div> <!-- .col -->
      <div class="col-md-12 col-lg-12">
        <div class="card shadow eq-card mb-4">
          <div class="card-header">
            <strong class="card-title">Selisih</strong>
          </div>
          <div class="card-body">
            <div class="row items-align-center">
              <div class="col-4 text-center">
                <p class="text-muted mb-1">Material</p>
                <h6 class="mb-1">Rp. {{ number_format(abs($material_rekon - $material_rfc), 0, '', '.') }}</h6>
              </div>
              <div class="col-4 text-center">
                <p class="text-muted mb-1">Jasa</p>
                <h6 class="mb-1">Rp. {{ number_format(abs($jasa_rekon - $jasa_rfc), 0, '', '.') }}</h6>
              </div>
              <div class="col-4 text-center">
                <p class="text-muted mb-1">Total</p>
                <h6 class="mb-1">Rp. {{ number_format( abs( ($material_rekon + $jasa_rekon) - ($material_rfc + $jasa_rfc) ), 0, '', '.') }}</h6>
              </div>
            </div>
          </div> <!-- .card-body -->
        </div> <!-- .card -->
      </div> <!-- .col -->
    </div>
    <div class="mb-2 align-items-center">
      <h2 class="page-title">Perhitungan Material Sisa RFC vs Rekon</h2>
      <div class="card shadow mb-4">
        <div class="card-body table-responsive">
          <table class="table table-striped table-bordered">
            <thead class="thead-dark">
              <tr>
                <th>Mitra</th>
                <th>Judul Pekerjaan</th>
                <th>Designator Rekon</th>
                <th>Volume Designator Rekon</th>
                <th>Designator</th>
                <th>Quantity</th>
                <th>Catatan Designator RFC</th>
                <th>Volume Designator RFC</th>
                <th>Selisih RFC</th>
              </tr>
            </thead>
            <tbody id="data_table1">
              @foreach($volum_design as $k => $v)
                  @php
                    $first = true;
                  @endphp
                @foreach($v as $kk => $vv)
                  @php
                    $sec = true;
                  @endphp
                  @foreach ($vv as $kkk => $vvv)
                  @php
                    $third = true;
                  @endphp
                    @foreach ($vvv['nama_material_rfc'] as $kkkk => $vvvv)
                      <tr>
                        @if($first == true)
                          <td rowspan="{{ array_sum(array_map(function ($split) { $get_head_row = 0; foreach ($split as $vx) { $get_head_row += (count($vx['nama_material_rfc']) ); } return $get_head_row; }, $v) ) }}"> {{ $k }} </td>
                          @php ($first = false) @endphp
                        @endif
                        @if($sec == true)
                          <td rowspan="{{ array_sum(array_map(function ($split) { return count($split['nama_material_rfc']); }, $vv) ) }}"> {{ $kk }} </td>
                          @php ($sec = false) @endphp
                        @endif
                        @if($third == true)
                          <td rowspan="{{ count($vvv['nama_material_rfc']) }}">{{ $kkk }}</td>
                          <td rowspan="{{ count($vvv['nama_material_rfc']) }}" style="background-color: {{ ($vvv['total_volum_design'] != $vvv['total_volum_rfc'] ? 'Red' : 'Green') }}; color: white;" >{{ $vvv['total_volum_design'] }}</td>
                        @endif
                        <td>{{ $vvvv['nama'] }}</td>
                        <td>
                          @php
                            $data = [];
                            foreach($vvvv['rfc'] as $k => $v)
                            {
                              $data[$k] = '<a type="button" class="btn btn-sm btn-info show_rfc" data-rfc="' . $k . '" data-id_pbu="' . $v['id_boq'] . '" data-material="' . $vvvv['nama'] . '">' . $k .'</a> = '. $v['isi'] . ' buah';
                            }
                            $content = implode('<br/>', $data);
                          @endphp
                          <a type="button" class="btn btn-sm btn-info" data-container="body" data-toggle="popover" style="color: white;" data-placement="right" title="<b>List RFC</b>" data-html="true" data-content="{{ $content }}">
                            {{ $vvvv['qty'] }}
                          </a>
                        </td>
                        <td>{{ @implode(', ', $vvvv['keterangan'] ) }}</td>
                        @if($third == true)
                          <td rowspan="{{ count($vvv['nama_material_rfc']) }}" style="background-color: {{ ($vvv['total_volum_design'] != $vvv['total_volum_rfc'] ? 'Red' : 'Green') }}; color: white;">{{ $vvv['total_volum_rfc'] }}</td>
                        @endif
                        @if($third == true)
                          <td rowspan="{{ count($vvv['nama_material_rfc']) }}" style="background-color: {{ ($vvv['total_volum_design'] != $vvv['total_volum_rfc'] ? 'Red' : 'Green') }}; color: white;">{{ ($vvv['total_volum_rfc'] - $vvv['total_volum_design']) != 0 ? abs($vvv['total_volum_rfc'] - $vvv['total_volum_design']) : '-' }}</td>
                          @php ($third = false) @endphp
                        @endif
                      </tr>
                    @endforeach
                  @endforeach
                @endforeach
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('footerS')
<script src='/js/jquery.dataTables.min.js'></script>
<script src='/js/dataTables.bootstrap4.min.js'></script>
<script type="text/javascript">
	$(function(){
		$.fn.digits = function(){
			return this.each(function(){
					$(this).text( $(this).text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") );
			});
		}

    $("[data-toggle=popover]").popover({
      container: 'body'
    });

    var myDefaultWhiteList = $.fn.tooltip.Constructor.Default.whiteList

    myDefaultWhiteList.a = ['data-rfc', 'data-id_pbu', 'data-material']

    var myCustomRegex = /^data-my-app-[\w-]+/
    myDefaultWhiteList['*'].push(myCustomRegex)

    $(document).on('click', ".show_rfc", function() {
      $('#modal_check_rfc').modal('toggle');
      var id_boq = $(this).attr('data-id_pbu');
      var material = $(this).attr('data-material');

			$('.submit_btn_rfc').hide();
			$.ajax({
				url:"/get_ajx/inventory",
				type:"GET",
				data: {
					isi: $(this).data('rfc'),
					id_pbu: id_boq
				},
				dataType: 'json',
				success: (function(data){
					var data_rfc = data.inventory[0]
					// console.log(data)
					if(data.download){
						$('.dwn_rfc').show();
						$('.dwn_rfc').attr('href', '/download/rfc/'+id_boq+'/'+data_rfc.no_rfc);
					}else{
						$('.dwn_rfc').hide();
					}

					$('.no_rfc').html(data_rfc.no_rfc)
					$('.plant').html(data_rfc.plant)
					$('.wbs_element').html(data_rfc.wbs_element)
					$('.type').html( (data_rfc.type == 'Out' ? 'Pengeluaran' : 'Pengembalian') )
					$('.mitra').html(data_rfc.mitra)

					var table_html = "<table class='tbl_material table table-striped table-bordered table-hover' style='width: 100%'>";
					table_html += "<thead class='thead-dark'>";
					table_html += "<tr>";
					table_html += "<th>Material</th>";
					table_html += "<th>Satuan</th>";
					table_html += "<th>Jumlah</th>";
					table_html += "</tr>";
					table_html += "</thead>";
					table_html += "<tbody>";

          var style = '';

					$.each(data.inventory, function(key, val){
            if(val.material == material){
              style = "style='background-color: yellow; color: black;'"
            }
						table_html += "<tr " + style + ">";
						table_html += "<td>"+ val.material +"</td>";
						table_html += "<td>"+ val.satuan +"</td>";
						table_html += "<td>"+ val.quantity +"</td>";
						table_html += "</tr>";
					});

					table_html += "</tbody>";
					table_html += "</table>";

					$('.table_rfc_material').html(table_html)
					var data_select = [];

					$.each(data.pid, function(key, val){
						data_select.push({
							id: key,
							text: val.lokasi +' (' + val.pid + ')'
						})
					});

					// console.log(data_select)

					$('.tbl_material').DataTable({
						autoWidth: true,
        		bFilter: false,
						lengthMenu: [
							[16, 32, 64, -1],
							[16, 32, 64, "All"]
						]
					});

				})
			})
    });
	});
</script>
@endsection