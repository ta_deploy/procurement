@extends('layout')
@section('title', 'Daftar Planning')
@section('headerS')
<link rel="stylesheet" href="/css/dataTables.bootstrap4.css">
<style>
	th, td {
		text-align: center;
	}
</style>
@endsection
@section('content')
<div class="container-fluid">
	@if (Session::has('alerts'))
		@foreach(Session::get('alerts') as $alert)
			<div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
		@endforeach
	@endif
	<div class="row justify-content-center">
		<div class="col-12">
			<div class="row">
				<div class="col-md-12 my-4">
					<div class="card shadow mb-4">
						<div class="card-body table-responsive">
							<h5 class="card-title">Daftar Planning</h5>
							<a type="button" href="/tools/planning_fixed" class="btn btn-primary pra_kontrak"><i class="fe fe-plus-square"></i>&nbsp;Tambah Planning Baru</a>
							<br /><br />
							<table id="tb_plan" class="table table-striped table-bordered table-hover">
								<thead class="thead-dark">
									<tr>
										<th>No</th>
										<th>Judul</th>
										<th>Material</th>
										<th class="hidden-xs">Action</th>
									</tr>
								</thead>
								<tbody id="data_table">
									@php $num = 1; @endphp
									@forelse ($data as $k => $v)
                    <tr>
                      <td>{{ $num++ }}</td>
                      <td>{{ $v['judul'] }}</td>
											<td>
												@foreach ($v['design'] as $vv)
													<span class="badge badge-info">{{ $vv }}</span>
												@endforeach
											</td>
                      <td>
												<a type="button" href="/tools/planning_rfc/{{ $k }}" class="btn btn-sm btn-info" style="color: white;">Proses</a>
												<a type="button" href="/tools/generate_planning/{{ $k }}" class="btn btn-sm btn-success" style="color: white;">Download</a>
												<a type="button" href="/tools/delete_planning/{{ $k }}" data-judul="{{ $v['judul'] }}" class="btn btn-sm btn-danger btn_delete" style="color: white;">Delete</a>
											</td>
                    </tr>
                  @empty
                    <tr>
                      <td colspan="2">-</td>
                    </tr>
                  @endforelse
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('footerS')
<script src='/js/jquery.dataTables.min.js'></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript">
	$(function(){
		$('#tb_plan').DataTable();

		$('.btn_delete').on('click', function(e){
			e.preventDefault();

			var this_me = $(this),
			hrefnya = $(this).attr('href'),
			judul = $(this).data('judul');
			Swal.fire({
					title: `Apakah Kamu Ingin Menghapus Pekerjaan ${judul}`,
					icon: 'warning',
					showDenyButton: true,
					confirmButtonText: 'Ya, Hapus!',
					denyButtonText: `Jangan!`,
				}).then((result) => {
					/* Read more about isConfirmed, isDenied below */
					if (result.isConfirmed) {
						window.location.replace(hrefnya);
						//there are multiple elements which has .closeTab icon so close the tab whose close icon is clicked
					} else if (result.isDenied) {
						Swal.fire(`${judul} Tidak Dihapus!`, '', 'info')
					}
				})
		});
	});
</script>
@endsection