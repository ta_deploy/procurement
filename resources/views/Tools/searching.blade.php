@extends('layout')
@section('title', 'Pencarian Pekerjaan')
@section('headerS')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="/css/dataTables.bootstrap4.css">
<link rel="stylesheet" href="/css/daterangepicker.css">
<style type="text/css">
	.pull-right {
		text-align: right;
	}
</style>
@endsection
@section('content')
@if (Session::has('alerts'))
  @foreach(Session::get('alerts') as $alert)
    <div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
  @endforeach
@endif
<div class="modal fade" id="modal_check_rfc" tabindex="-1" role="dialog" aria-labelledby="modal_check_rfcTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
				<a class="btn btn-info dwn_rfc" style="color: #fff" href="#"><i class="fe fe-download"></i>&nbsp;PDF</a>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body data_mdl" style=" overflow: hidden;">
				<form class="row" method="post">
					{{ csrf_field() }}
					<input type="hidden" name="rfc_id" id="rfc_id">
					<div class="col-md-4">
						<div class="form-group row">
							<label class="col-form-label col-md-4 pull-right" for="wbs_element">PID:</label>
							<div class="col-md-8">
								<p class="form-control input-transparent" readonly><b class="wbs_element"></b></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-md-4 pull-right" for="no_rfc">Nomor RFC:</label>
							<div class="col-md-8">
								<p class="form-control input-transparent" readonly><b class="no_rfc"></b></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-md-4 pull-right" for="plant">Gudang:</label>
							<div class="col-md-8">
								<p class="form-control input-transparent" readonly><b class="plant"></b></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-md-4 pull-right" for="type">Jenis:</label>
							<div class="col-md-8">
								<p class="form-control input-transparent" readonly><b class="type"></b></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-md-4 pull-right" for="mitra">Mitra:</label>
							<div class="col-md-8">
								<textarea class="form-control input-transparent mitra" rows="2" readonly></textarea>
							</div>
						</div>
					</div>
					<div class="col-md-8">
						<div class="table_rfc_material">
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
			</div>
    </div>
  </div>
</div>
<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-md-12">
			<h2 class="page-title">Cari Pekerjaan</h2>
			<div class="card-deck" style="display: block">
        <div class="col-md-12">
          <form class="row" method="get">
            <div class="col-md-12">
              <div class="card shadow mb-4">
                <div class="card-body">
                  <p class="mb-2"><strong>Pencarian</strong></p>
                  <div class="custom-control custom-radio">
                    <input type="radio" id="simple" name="search" class="custom-control-input chck" value="simple">
                    <label class="custom-control-label" for="simple">Pekerjaan Secara Simpel</label>
                  </div>
                  <div class="custom-control custom-radio">
                    <input type="radio" id="detail" name="search" class="custom-control-input chck" value="detail">
                    <label class="custom-control-label" for="detail">Pekerjaan Secara Detail</label>
                  </div>
                  <div class="custom-control custom-radio">
                    <input type="radio" id="sp_num" name="search" class="custom-control-input chck" value="sp_num">
                    <label class="custom-control-label" for="sp_num">Pekerjaan Dengan SP</label>
                  </div>
                  <div class="custom-control custom-radio">
                    <input type="radio" id="rfc" name="search" class="custom-control-input chck" value="rfc">
                    <label class="custom-control-label" for="rfc">Nomor RFC</label>
                  </div>
                  <div class="custom-control custom-radio">
                    <input type="radio" id="pid" name="search" class="custom-control-input chck" value="pid">
                    <label class="custom-control-label" for="pid">Nomor PID</label>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-12 complex" style="display: none;">
              <div class="card shadow mb-4">
                <div class="card-body">
                  <div class="form-group row">
                    <label class="col-form-label col-md-2 pull-right" for="pekerjaan">Pekerjaan</label>
                    <div class="col-md-10">
                      <select name="pekerjaan" class="form-control input-transparent" id="pekerjaan">
                        @foreach ($kerjaan[0] as $val)
                          <option value="{{ $val->id }}">{{ $val->text }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-form-label col-md-2 pull-right" for="jenis_work">Jenis Pekerjaan</label>
                    <div class="col-md-5">
                      <select name="jenis_work" class="form-control input-transparent" id="jenis_work">
                      </select>
                    </div>
                    <div class="col-md-5">
                      <select name="jenis_kontrak" class="form-control input-transparent" id="jenis_kontrak">
                        <option value="Kontrak Putus">Kontrak Putus</option>
                        <option value="Kontrak Tetap">Kontrak Tetap</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-form-label col-md-2 pull-right" for="witel">Witel</label>
                    <div class="col-md-10">
                      <select class="select2 form-control input-transparent" name="witel" id="witel">
                        @foreach ($witel as $val)
                          <option value="{{ $val }}">{{ $val }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-form-label col-md-2 pull-right">Pengerjaan</label>
                    <div class="col-md-5">
                      <select id="tahun" name="tahun">
                        @for ($i = date('Y'); $i >= date('Y', strtotime('-1 year') ); $i--)
                          <option value="{{ $i }}">{{ $i }}</option>
                        @endfor
                      </select>
                    </div>
                    <div class="col-md-5">
                      <select id="bulan" name="bulan">
                        <option value="01">Januari</option>
                        <option value="02">Februari</option>
                        <option value="03">Maret</option>
                        <option value="04">April</option>
                        <option value="05">Mei</option>
                        <option value="06">Juni</option>
                        <option value="07">Juli</option>
                        <option value="08">Agustus</option>
                        <option value="09">September</option>
                        <option value="10">Oktober</option>
                        <option value="11">November</option>
                        <option value="12">Desember</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-form-label col-md-2 pull-right" for="mitra">Mitra</label>
                    <div class="col-md-10">
                      @if(session('auth')->proc_level != 2)
                        <select class="select2 form-control input-transparent" name="mitra[]" id="mitra" multiple></select>
                      @else
                      <select class="form-control input-transparent" name="mitra[]" id="mitra_manual" readonly>
                        <option value="{{ session('auth')->dalapa_mitra_id }}">{{ session('auth')->nama_company }}</option>
                      </select>
                      @endif
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-12 reguler">
              <div class="card shadow mb-4">
                <div class="card-body">
                  <div class="form-group row">
                    <label class="col-form-label col-md-2 pull-right" for="judul_pekerjaan">Judul Pekerjaan</label>
                    <div class="col-md-10">
                      <input name="judul_pekerjaan" class="form-control input-transparent" placeholder="Masukkan Judul Pekerjaan" id="judul_pekerjaan">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-12 no_sp" style="display: none;">
              <div class="card shadow mb-4">
                <div class="card-body">
                  <div class="form-group row">
                    <label class="col-form-label col-md-2 pull-right" for="sp_num">Nomor SP</label>
                    <div class="col-md-10">
                      <input name="sp_num" class="form-control input-transparent" placeholder="Masukkan Nomor SP" id="sp_num">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-12 pid" style="display: none;">
              <div class="card shadow mb-4">
                <div class="card-body">
                  <div class="form-group row">
                    <label class="col-form-label col-md-2 pull-right" for="pid_num">Nomor PID</label>
                    <div class="col-md-10">
                      <input name="pid_num[]" class="form-control input-transparent pid_num" placeholder="Masukkan Satu Nomor PID">
                      <div class="field_pid"></div>
                      <a type="button" style="color: white; margin-top: 10px;" class="btn add_pid btn-info btn-sm">Tambah PID</a>
                      <a type="button" style="color: white; margin-top: 10px;" class="btn remove_pid btn-danger btn-sm">Hapus PID</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-12 rfc" style="display: none;">
              <div class="card shadow mb-4">
                <div class="card-body">
                  <div class="form-group row">
                    <label class="col-form-label col-md-2 pull-right" for="rfc_num">Nomor RFC</label>
                    <div class="col-md-10">
                      <input name="rfc_num[]" class="form-control input-transparent rfc_num" placeholder="Masukkan Satu Nomor RFC">
                      <div class="field_rfc"></div>
                      <a type="button" style="color: white; margin-top: 10px;" class="btn add_rfc btn-info btn-sm">Tambah rfc</a>
                      <a type="button" style="color: white; margin-top: 10px;" class="btn remove_rfc btn-danger btn-sm">Hapus rfc</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group mb-3">
                <div class="custom-file">
                  <button type="submit" class="btn btn-block btn-primary">Cari Data</button>
                </div>
              </div>
            </div>
          </form>
        </div>
        @if ($result)
          <div class="alert alert-success">Hasil yang ditemukan berdasarkan pencarian adalah {{ count($result[1]) }} buah.</div>
          <div class="col-md-12">
            <div class="card shadow mb-4">
              <div class="card-header">
                <strong class="card-title">Hasil Pencarian</strong>
              </div>
              <div class="card-body table-responsive">
                <table id ="tb_rekon" class="table table-striped table-bordered table-hover">
                  <thead class="thead-dark">
                    <tr>
                      @forelse ($result[0] as $key => $val)
                        <th>{{ $val }}</th>
                      @empty
                      @endforelse
                    </tr>
                  </thead>
                  <tbody>
                    @if (in_array($req->search, ['simple', 'detail']) )
                      @forelse ($result[1] as $key => $val)
                        <tr>
                          <td>{{ ++$key }}</td>
                          <td>{{ $val->judul }}</td>
                          <td>{{ $val->nama_company }}</td>
                          <td>{{ number_format($val->gd_sp, 0, ',', ',') ?? 0 }}</td>
                          <td>
                            @php
                              if($val->active == 1)
                              {
                                $text  = 'Detail';
                                $btn   = 'info';
                              }
                              else
                              {
                                $text  = "Pekerjaan Dihapus<br />(Lihat Detail)";
                                $btn   = 'danger';
                              }
                            @endphp

                            <a type="button" class="btn btn-{{ $btn }}" target="_blank" style="color: white;" href="/get_detail_laporan/{{ $val->id }}">{!! $text !!}</a>

                          </td>
                        </tr>
                      @empty
                      @endforelse
                    @elseif($req->search == 'rfc')
                      @forelse ($result[1] as $key => $val)
                        <tr>
                          <td>{{ ++$key }}</td>
                          <td>{{ $val->rfc }}</td>
                          <td>{{ $val->status_rfc }}</td>
                          <td>
                            @if($val->id_upload)
                              <a style="color: {{ $val->active_pbu == 0 ? 'red' : '' }}" target="_blank" href="/get_detail_laporan/{{ $val->id_upload }}">{{ $val->judul }} {!! $val->active_pbu == 0 ? 'Pekerjaan Dihapus<br/>(Lihat Detail)' : '' !!}</a>
                            @else
                              {{ $val->ket_im }}
                            @endif
                          </td>
										      <td>
                            @if($val->id_upload)
                              <a class="btn btn-info lihat_detail_rfc" dta-pbu_id="{{ $val->id_upload }}" data-rfc="{{ $val->rfc }}" style="color: white;">Lihat Detail</a>
                            @else
                              -
                            @endif
                          </td>
                        </tr>
                      @empty
                      @endforelse
                    @elseif($req->search == 'pid')
                      @forelse ($result[1] as $k => $v)
                        @php ($first = true) @endphp
                        @forelse($v['po'] as $vv)
                          <tr>
                          @if($first == true)
                            <td rowspan="{{ count($v['po']) }}"> {{ ++$k }} </td>
                            <td rowspan="{{ count($v['po']) }}"> {{ $v['pid'] }} </td>
                            <td rowspan="{{ count($v['po']) }}"> {{ @$v['keperluan']['Material : Stock'] }} </td>
                            <td rowspan="{{ count($v['po']) }}"> {{ @$v['keperluan']['Material : Non Stock'] }} </td>
                            <td style="color: {{ $v['status_pid'] == 1 ? 'red' : '' }}" rowspan="{{ count($v['po']) }}"> {{ $v['status_pid'] == 0 ? 'Ada Budget' : 'Budget Exceeded' }} </td>
                            @php ($first = false) @endphp
                          @endif
                            <td>
                              <a style="color: {{ $vv['active_pbu'] == 0 ? 'red' : '' }}" target="_blank" href="/get_detail_laporan/{{ $vv['id_upload'] }}">{{ $vv['judul'] }} {!! $vv['active_pbu'] == 0 ? 'Pekerjaan Dihapus<br/>(Lihat Detail)' : '' !!}</a>
                            </td>
                          </tr>
                        @empty
                        @endforelse
                      @empty
                      @endforelse
                    @endif
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        @endif
		  </div>
	  </div>
  </div>
</div>
@endsection
@section('footerS')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src='/js/jquery.dataTables.min.js'></script>
<script src='/js/dataTables.bootstrap4.min.js'></script>
<script src="/js/moment.min.js"></script>
<script src='/js/jquery.timepicker.js'></script>
<script src='/js/daterangepicker.js'></script>
<script type="text/javascript">
	$(function(){
    $("#simple").prop('checked', true);

    var raw_class_select = [
      {id: 'simple', class: '.reguler'},
      {id: 'detail', class: '.complex'},
      {id: 'rfc', class: '.rfc'},
      {id: 'pid', class: '.pid'},
      {id: 'sp_num', class: '.no_sp'}
    ];

    $("input[name='search']").on('change', function (){
      var isi_search = $(this).val();
      $.each(raw_class_select, function(k, v){
        if(isi_search == v.id){
          var value = v.class;

          class_hid = raw_class_select.filter(function(item) {
              return item.class !== value
          });

          class_hid = class_hid.map(function(v){
            return v.class;
          });

          $(class_hid.join(',')).css({'display': 'none'});

          $.each(class_hid, function(k, v){
            $(v +' input,'+ v +' select').removeAttr('required');
          })

          $(value).css({'display': 'block'});

          $(value +' input,'+ value +' select').attr('required', 'required');
        }
      });
    });

    $('.add_pid').on('click', function(){
      $('.field_pid').append('<input name="pid_num[]" class="form-control input-transparent pid_num" placeholder="Masukkan Satu Nomor PID" style="margin-top: 10px;" required>');
    })

    $('.remove_pid').on('click', function(){
      if(Object.keys($('.pid_num') ).length > 3){
        $('.pid_num').last().remove();
      }
    })

    $('.add_rfc').on('click', function(){
      $('.field_rfc').append('<input name="rfc_num[]" class="form-control input-transparent rfc_num" placeholder="Masukkan Satu Nomor RFC" style="margin-top: 10px;" required>');
    })

    $('.remove_rfc').on('click', function(){
      if(Object.keys($('.rfc_num') ).length > 3){
        $('.rfc_num').last().remove();
      }
    })

    $('.lihat_detail_rfc').click(function(){
			$('#modal_check_rfc').modal('toggle');
			$('.submit_btn_rfc').hide();
			$.ajax({
				url:"/get_ajx/inventory",
				type:"GET",
				data: {
					isi: $(this).data('rfc'),
					id_pbu: $(this).data('pbu_id')
				},
				dataType: 'json',
				success: (function(data){
					console.log(data)
					var data_rfc = data.inventory[0]
					if(data.download){
						$('.dwn_rfc').show();
						$('.dwn_rfc').attr('href', '/download/rfc/'+$(this).data('pbu_id')+'/'+data_rfc.no_rfc);
					}else{
						$('.dwn_rfc').hide();
					}

					$('.no_rfc').html(data_rfc.no_rfc)
					$('.plant').html(data_rfc.plant)
					$('.wbs_element').html(data_rfc.wbs_element)
					$('.type').html( (data_rfc.type == 'Out' ? 'Pengeluaran' : 'Pengembalian') )
					$('.mitra').html(data_rfc.mitra)

					var table_html = "<table class='tbl_material table table-striped table-bordered table-hover'>";
					table_html += "<thead class='thead-dark'>";
					table_html += "<tr>";
					table_html += "<th>Material</th>";
					table_html += "<th>Satuan</th>";
					table_html += "<th>Jumlah</th>";
					table_html += "</tr>";
					table_html += "</thead>";
					table_html += "<tbody>";

					$.each(data.inventory, function(key, val){
						table_html += "<tr>";
						table_html += "<td>"+ val.material +"</td>";
						table_html += "<td>"+ val.satuan +"</td>";
						table_html += "<td>"+ val.quantity +"</td>";
						table_html += "</tr>";
					});

					table_html += "</tbody>";
					table_html += "</table>";

					$('.table_rfc_material').html(table_html)
					var data_select = [];

					$.each(data.pid, function(key, val){
						data_select.push({
							id: key,
							text: val.lokasi +' (' + val.pid + ')'
						})
					});

					// console.log(data_select)

					$('.tbl_material').DataTable({
						autoWidth: true,
        		bFilter: false,
						lengthMenu: [
							[16, 32, 64, -1],
							[16, 32, 64, "All"]
						]
					});

				})
			})
		})

    var mitra = {!! json_encode($mitra_modify) !!};
    // console.log(mitra);
    if($('#witel') )
    {
      $('#witel').val(null).change()
      $('#witel').on('select2:clear change', function() {
        var isi = $(this).val(),
        isi_select = [],
        pra_select = {};
        if(isi == 'KALSEL'){
          pra_select.KALSEL = mitra.KALSEL;
        }else if(isi == 'BALIKPAPAN'){
          pra_select.BALIKPAPAN = mitra.BALIKPAPAN;
        }else{
          pra_select = {};
        }
        console.log(pra_select, mitra.KALSEL)
        if ($("#mitra").hasClass("select2-hidden-accessible")) {
          // $("#mitra").select2('destroy');
          $("#mitra").empty().change();
        }

        if(!$.isEmptyObject(pra_select)){
          $.each(pra_select, function(key, index){
            $.each(index, function(keyc1, indexc1){
              isi_select.push({
                id: indexc1.id,
                text: indexc1.text
              })
            })
          })
          if(isi_select)
          {
            $("#mitra").select2({
              placeholder: 'Pilih Nama Mitra',
              theme: 'bootstrap4',
              width: '100%',
              data: isi_select
            }).change();
          }
        }
        else
        {
          if ($("#mitra").hasClass("select2-hidden-accessible")) {
            $("#mitra").empty().change();
          }
        }
      })
    }

    var pekerjaan = {!! json_encode($req->pekerjaan) !!}
    witel = {!! json_encode($req->witel) !!},
    mitra_load = {!! json_encode($req->mitra) !!};

    // if(witel != null)
    // {
    //   $('#pekerjaan').val(pekerjaan).change();
    //   $('#witel').val(witel).change();
    //   $('#mitra').val(mitra_load).change();
    // }

		$('#pekerjaan').val(null).change();

		$('#pekerjaan').select2({
			width: '100%',
			placeholder: 'Pilih jenis pekerjaan',
		});

    $('#pekerjaan').on('change', function(){
      var jenis_kerja = {!! json_encode($kerjaan[1]) !!},
      kerja = $(this).val(),
      cari = [];
      $.each(jenis_kerja, function(key, val) {
        if(val.pekerjaan == kerja){
          cari.push({id: val.id, text: val.text})
        }
      });
      // console.log(cari)
      $("#jenis_work").empty().trigger('change')

      $('#jenis_work').select2({
        width: '100%',
        data: cari
      })
    })

    $('#tahun, #bulan').select2({
      width: '100%'
    })

		$("#witel").select2({
			width: "100%",
			placeholder: "Pilih Wilayah Pekerjaan",
      allowClear : true
		});

		$("#witel").select2({
			width: "100%",
			placeholder: "Pilih Mitra Pengerjaan",
      allowClear : true
		});

		$('.date-picker').daterangepicker(
      {
        singleDatePicker: true,
        timePicker: false,
        showDropdowns: true,
        locale:
        {
					format: 'YYYY-MM-DD',
        }
      }
		);

		$('#tb_rekon').DataTable({
			autoWidth: true,
			columnDefs: [
				{
					targets: 'no-sort',
					orderable: false
				}
			],
			lengthMenu: [
				[16, 32, 64, -1],
				[16, 32, 64, "All"]
			]
		});

    var session = {!! json_encode(session('auth')->proc_level) !!},
    witel = {!! json_encode(session('auth')->Witel_New) !!};
    console.log(session, witel)
    if(session != 4)
    {
      $("#witel").val(witel).change();
      $("#witel").prop("disabled", true);
    }
	});
</script>
@endsection