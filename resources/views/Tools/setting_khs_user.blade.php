@extends('layout')
@section('title', 'Setting Pekerjaan')
@section('headerS')
<link rel="stylesheet" href="/css/dataTables.bootstrap4.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<style type="text/css">
	.pull-right {
		text-align: right;
	}
</style>
@endsection
@section('content')
@if (Session::has('alerts'))
  @foreach(Session::get('alerts') as $alert)
    <div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
  @endforeach
@endif
<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-md-12">
			<h2 class="page-title">Pekerjaan</h2>
			<div class="card-deck" style="display: block">
				<form class="row" method="post" enctype="multipart/form-data">
					{{ csrf_field() }}
					<div class="col-md-12">
						<div class="card shadow mb-4">
							<div class="card-body">
								<div class="form-group row">
                  <label class="col-form-label col-md-2 pull-right" for="judul">Pekerjaan Yang Ditampilkan</label>
                  <div class="col-md-10">
                    <select name="pekerjaan[]" class="form-control input-transparent" id="pekerjaan" multiple>
											@foreach ($data as $val)
												<option value="{{ $val->id }}">{{ $val->text }}</option>
											@endforeach
										</select>
                  </div>
                </div>
								<div class="form-group mb-3">
									<div class="custom-file">
										<button type='submit' class="btn btn-block btn-primary">Submit</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
		</div>
	</div>
</div>
@endsection
@section('footerS')
<script src='/js/jquery.dataTables.min.js'></script>
<script src='/js/dataTables.bootstrap4.min.js'></script>
<script src='/js/select2.min.js'></script>
<script type="text/javascript">
	$(function(){
		$('#pekerjaan').select2({
			width: '100%',
			placeholder: 'Pekerjaan Bisa Dipilih Lebih Dari Satu'
		});

		var load_data = {!! json_encode($load_data) !!};
    if(!$.isEmptyObject(load_data) ){
			var get_data = load_data.pekerjaan.split(', ');
			$('#pekerjaan').val(get_data).change();
		}
	})
</script>
@endsection