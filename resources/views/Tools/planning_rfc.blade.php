@extends('layout')
@section('title', 'Planning RFC')
@section('headerS')
<link rel="stylesheet" href="/css/dataTables.bootstrap4.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<style>
	th, td {
		text-align: center;
	}

	.select2-search--inline {
    display: contents; /*this will make the container disappear, making the child the one who sets the width of the element*/
	}

	.select2-search__field:placeholder-shown {
		width: 100% !important; /*makes the placeholder to be 100% of the width while there are no options selected*/
	}
</style>
@endsection
@section('content')
<div class="container-fluid">
	@if (Session::has('alerts'))
		@foreach(Session::get('alerts') as $alert)
			<div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
		@endforeach
	@endif
  <div class="modal fade" id="modal_check_rfc" role="dialog" aria-labelledby="modal_check_rfcTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <a class="btn btn-info dwn_rfc" style="color: #fff" href="#"><i class="fe fe-download"></i>&nbsp;PDF</a>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body data_mdl" style=" overflow: hidden;">
          <form class="row" method="post">
            {{ csrf_field() }}
            <input type="hidden" name="rfc_id" id="rfc_id">
            <div class="col-md-5">
              <div class="form-group row">
                <label class="col-form-label col-md-4 pull-right" for="wbs_element">PID:</label>
                <div class="col-md-8">
                  <p class="form-control input-transparent" readonly><b class="wbs_element"></b></p>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-form-label col-md-4 pull-right" for="no_rfc">Nomor RFC:</label>
                <div class="col-md-8">
                  <p class="form-control input-transparent" readonly><b class="no_rfc"></b></p>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-form-label col-md-4 pull-right" for="plant">Gudang:</label>
                <div class="col-md-8">
                  <p class="form-control input-transparent" readonly><b class="plant"></b></p>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-form-label col-md-4 pull-right" for="type">Jenis:</label>
                <div class="col-md-8">
                  <p class="form-control input-transparent" readonly><b class="type"></b></p>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-form-label col-md-4 pull-right" for="mitra">Mitra:</label>
                <div class="col-md-8">
                  <textarea class="form-control input-transparent mitra" rows="2" readonly></textarea>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-form-label col-md-4 pull-right" for="tiket">Tiket:</label>
                <div class="col-md-8">
                  <select class="form-control" name="tiket[]" id="tiket" multiple></select>
                </div>
              </div>
            </div>
            <div class="col-md-7">
              <div class="table_rfc_material">
              </div>
            </div>
            <div class="col-md-12 submit_btn_rfc">
              <div class="form-group mb-3">
                <div class="custom-file">
                  <button type="submit" class="btn btn-block btn-primary">Submit Nomor RFC</button>
                </div>
              </div>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
	<div class="row justify-content-center">
		<div class="col-12">
			<div class="row">
				<div class="col-md-12 my-4">
					<div class="card shadow mb-4">
						<div class="card-body table-responsive">
							<h5 class="card-title">Planning RFC</h5>
							<div class="form-group row">
								<label class="col-form-label col-md-2 pull-right" for="rfc">RFC</label>
								<div class="col-md-10">
									<select class="form-control" name="rfc" id="rfc"></select>
								</div>
							</div>
							{{-- <a type="button" class="btn btn-primary lihat_detail_rfc" style="color: #ffffff"><i class="fe fe-plus-square"></i>&nbsp;Tambah RFC/RFR</a> --}}
							<br /><br />
							<table id="tb_plan" class="table table-striped table-bordered table-hover">
								<thead class="thead-dark">
									<tr>
										<th>Nomor RFC/RFR</th>
										<th>Jenis</th>
										<th>Tiket</th>
									</tr>
								</thead>
								<tbody id="data_table">
									@foreach ($rfc_tiket as $k => $v)
										<tr>
											<td>{{ $k }}</td>
											<td>{{ $v['jenis'] }}</td>
											<td>
												@foreach ($v['tiket'] as $vv)
													<span class="badge badge-info">{{ $vv }}</span>
												@endforeach
											</td>
										</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('footerS')
<script src='/js/jquery.dataTables.min.js'></script>
<script src="/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script type="text/javascript">
	$(function(){
		$('#tb_plan').DataTable();

		$('#rfc').select2({
			width: '100%',
			placeholder: "Masukkan Nomor RFC Yang Digunakan",
			allowClear: true,
			minimumInputLength: 7,
			ajax: {
				url: "/get_ajx/search_rfc/base_plan",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						searchTerm: params.term,
						id_judul: window.location.href.split('/').slice(-1).pop()
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true,
				success: function(value) {
					// console.log(value)
				}
			}
		});

		var tiket = {!! json_encode($arr_tiket) !!},
		obj_tiket = [];

		$.each(tiket, function(k, v){
			obj_tiket.push({
				id: k,
				text: v
			});
		})

		//4901299974
		//4901300675
		//4901300535

		$('#rfc').on('select2:select', function(){
			isi_m = $(this).val();
			$('#rfc_id').val($(this).val() )

			$('.submit_btn_rfc').show();

			console.log(tiket)

			$('#tiket').select2({
				width: '100%',
				placeholder: 'Tiket Bisa Dipilih Lebih Dari Satu',
				data: obj_tiket,
			});

			$('#modal_check_rfc').modal('toggle');

			$('#rfc_old').val(null).change();
			$('.rfc_lama_field').hide();

			$.ajax({
				url:"/get_ajx/inventory",
				type:"GET",
				data: {
					isi: $(this).val(),
					id_pbu: 'all'
				},
				dataType: 'json',
				success: (function(data){
					var data_rfc = data.inventory[0]
					$('.no_rfc').html(data_rfc.no_rfc)
					$('.plant').html(data_rfc.plant)
					$('.wbs_element').html(data_rfc.wbs_element)
					$('.type').html( (data_rfc.type == 'Out' ? 'Pengeluaran' : 'Pengembalian') )
					$('.mitra').html(data_rfc.mitra)

					var table_html = "<table class='tbl_material table table-striped table-bordered table-hover'>";
					table_html += "<thead class='thead-dark'>";
					table_html += "<tr>";
					table_html += "<th>Material</th>";
					table_html += "<th>Satuan</th>";
					table_html += "<th>Jumlah</th>";
					table_html += "</tr>";
					table_html += "</thead>";
					table_html += "<tbody>";

					$.each(data.inventory, function(key, val){
						table_html += "<tr>";
						table_html += "<td>"+ val.material +"</td>";
						table_html += "<td>"+ val.satuan +"</td>";
						table_html += "<td>"+ val.quantity +"</td>";
						table_html += "</tr>";
					});

					table_html += "</tbody>";
					table_html += "</table>";

					$('.table_rfc_material').html(table_html)
					var data_select = [];

					$.each(data.pid, function(key, val){
						data_select.push({
							id: key,
							text: val.lokasi +' (' + val.pid + ')'
						})
					});

					// console.log(data_select)

					$('.tbl_material').DataTable({
						autoWidth: true,
        		bFilter: false,
						lengthMenu: [
							[16, 32, 64, -1],
							[16, 32, 64, "All"]
						]
					});

				})
			})
		})

    $('.lihat_detail_rfc').click(function(){
			isi_m = $(this).val();
			$('#rfc_id').val($(this).val() )
			// console.log('teprilih')
			$('.submit_btn_rfc').show();
			$('#tiket').select2({
				width: '100%',
				placeholder: 'Tiket Bisa Dipilih Lebih Dari Satu',
				allowClear: true
			});

			$('#modal_check_rfc').modal('toggle');

			$('#tiket').val('').change();
			$('.rfc_lama_field').hide();
			$.ajax({
				url:"/get_ajx/inventory",
				type:"GET",
				data: {
					isi: $(this).val(),
					id_pbu: data_sp.id
				},
				dataType: 'json',
				success: (function(data){
					var data_rfc = data.inventory[0]
					// console.log(data_rfc)
					$('.no_rfc').html(data_rfc.no_rfc)
					$('.plant').html(data_rfc.plant)
					$('.wbs_element').html(data_rfc.wbs_element)
					$('.type').html( (data_rfc.type == 'Out' ? 'Pengeluaran' : 'Pengembalian') )
					$('.mitra').html(data_rfc.mitra)

					var table_html = "<table class='tbl_material table table-striped table-bordered table-hover'>";
					table_html += "<thead class='thead-dark'>";
					table_html += "<tr>";
					table_html += "<th>Material</th>";
					table_html += "<th>Satuan</th>";
					table_html += "<th>Jumlah</th>";
					table_html += "</tr>";
					table_html += "</thead>";
					table_html += "<tbody>";

					$.each(data.inventory, function(key, val){
						table_html += "<tr>";
						table_html += "<td>"+ val.material +"</td>";
						table_html += "<td>"+ val.satuan +"</td>";
						table_html += "<td>"+ val.quantity +"</td>";
						table_html += "</tr>";
					});

					table_html += "</tbody>";
					table_html += "</table>";

					$('.table_rfc_material').html(table_html)
					var data_select = [];

					$.each(data.pid, function(key, val){
						data_select.push({
							id: key,
							text: val.lokasi +' (' + val.pid + ')'
						})
					});

					// console.log(data_select)

					$('.tbl_material').DataTable({
						autoWidth: true,
        		bFilter: false,
						lengthMenu: [
							[16, 32, 64, -1],
							[16, 32, 64, "All"]
						]
					});

				})
			})
		})
	});
</script>
@endsection