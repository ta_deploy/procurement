@extends('layout')
@section('title', 'Rollback Pekerjaan')
@section('headerS')
<link rel="stylesheet" href="/css/dataTables.bootstrap4.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<style type="text/css">
	.pull-right {
		text-align: right;
	}
</style>
@endsection
@section('content')
@if (Session::has('alerts'))
  @foreach(Session::get('alerts') as $alert)
    <div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
  @endforeach
@endif
<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-md-12">
			<h2 class="page-title">Kembalikan Pekerjaan</h2>
			<div class="card-deck" style="display: block">
				<form class="row" method="post" enctype="multipart/form-data">
					{{ csrf_field() }}
					<div class="col-md-12">
						<div class="card shadow mb-4">
							<div class="card-body">
								<div class="form-group row">
                  <label class="col-form-label col-md-2 pull-right" for="judul">Nama Pekerjaan</label>
                  <div class="col-md-10">
                    <input type="text" class="form-control input-transparent" name="judul" id="judul" value="{{ $data_sp->judul ?? '' }}" disabled>
										<code>*Secara Default, pekerjaan akan di kembalikan ke loker {{ $data_sp->nama .' ('. $data_sp->aktor_nama .')' }}</code>
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-md-12 offset-md-2">
										@if($data_sp->pekerjaan <> "PSB")
											<a href="/download/full_rar/{{ $data_sp->id }}" id="download" class="btn btn-info btn-sm" style="color: #fff"><i class="fe fe-download fe-16"></i>&nbsp;Generate Doc. Tagihan</a>
										@endif
										@if($data_sp->pekerjaan == "PSB")
											<a href="/download/full_zip_psb/khs_2021/{{ $data_sp->id }}" id="download" class="btn btn-info btn-sm" style="color: #fff"><i class="fe fe-download fe-16"></i>&nbsp;Generate Doc. Tagihan</a>
											&nbsp;
											<a class="btn btn-primary" style="color: #fff" href="/Admin/download/filepsb/dokumen_pelengkap/{{ $data_sp->id }}/download"><i class="fe fe-download"></i>&nbsp; Dokumen Pelengkap</a>
										@endif
									</div>
								</div>
								<div class="form-group row">
									<label for="opsi_rollback" class="col-form-label col-md-2 pull-right">Opsi Rollback</label>
									<div class="col-md-10">
										<select class="form-control" id="opsi_rollback" name="opsi_rollback">
											<option value="prev_loker">Loker Sebelumnya</option>
											{{-- @if($data_sp->step_id > 2)
												<option value="2">Revisi Harga SP (Step Operation)</option>
											@endif
											@if($data_sp->step_id > 6)
												<option value="6">Revisi RFC & Material (Step Mitra)</option>
											@endif
											@if($data_sp->step_id > 8)
												<option value="8">Input RFC Lurus (Step Mitra)</option>
											@endif
											@php
												$step = 0;

												if($data_sp->step_id >= 2 && $data_sp->step_id < 6)
												{
													$step = 27;
												}

												if($data_sp->step_id > 9)
												{
													$step = 9;
												}
											@endphp
											@if ($step != 0)
												<option value="{{ $step }}">Submit Ulang PID (Step Commerce)</option>
											@endif
											@if($data_sp->step_id > 7)
												<option value="7">Verifikasi Input Mitra (Step Operation)</option>
											@endif --}}
											@foreach ($get_prev_step_all as $v)
												<option value="{{ $v->true_id }}">{{ $v->nama }} ({{ $v->aktor_nama }})</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-md-2 pull-right" for="ket">Detail Rollback:</label>
									<div class="col-md-10">
										<textarea id="ket" name="ket" class="form-control" style="resize: none;" rows="4" required></textarea>
									</div>
								</div>
								<div class="form-group mb-3">
									<div class="custom-file">
										<button type='submit' class="btn btn-block btn-primary">Submit</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
		</div>
	</div>
</div>
@endsection
@section('footerS')
<script src='/js/jquery.dataTables.min.js'></script>
<script src='/js/dataTables.bootstrap4.min.js'></script>
<script src='/js/select2.min.js'></script>
<script type="text/javascript">
	$(function(){
		$("#opsi_rollback").select2();

		$('#status_step_id').on('change', function(){
			var isi = $(this).val();
			if(isi == 20){
				$('.pdf_upload').show();

				$('#upload_ttd').attr('required', true);
			}else{
				$('.pdf_upload').hide();

				$('#upload_ttd').removeAttr('required');
			}
		})
	})
</script>
@endsection