@extends('layout')
@section('title', 'Edit Pasca Kerja')
@section('style')
@endsection
@section('headerS')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="/css/daterangepicker.css">
<style type="text/css">
  .select2-results__option, select{
      color: black;
  }

  .pull-right {
		text-align: right;
	}

  @media (min-width: 768px) {
		.modal-xl {
			width: 100%;
		max-width:1200px;
		}
	}

	.select2-search--inline {
    display: contents;
	}

	.select2-search__field:placeholder-shown {
		width: 100% !important;
	}
</style>
@endsection
@section('content')
@if (Session::has('alerts'))
	@foreach(Session::get('alerts') as $alert)
		<div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
	@endforeach
@endif
<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-md-12"> <h2 class="page-title">Edit Pekerjaan Pasca Kerja</h2>
			<div class="card-deck" style="display: block">
				<form id="input_spen" class="row" method="post">
					{{ csrf_field() }}
					<div class="col-md-12">
						<div class="card shadow mb-4">
							<div class="card-header">
								<strong class="card-title">{{ $data->judul }}</strong>
							</div>
							<div class="card-body">
                <div class="form-group row">
                  <label class="col-form-label col-md-2 pull-right" for="upload_boq">Tanggal TOC :</label>
                  <div class="col-md-10">
                    @php
                      $toc_tgl = (date('Y-m-d', strtotime($data->tgl_sp .' +'. ($data->tgl_jatuh_tmp - 1) .' day' ) ) );
                    @endphp
                    <input class="form-control input-transparent" readonly value="{{ $toc_tgl }}">
                  </div>
                </div>
                <div class="row border-top py-3">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="col-form-label" for="plan_mat">Plan Material</label>
                      <input type="text" readonly class="form-control input-transparent price" name="plan_mat" id="plan_mat" value="{{ number_format($data->total_material_sp, 0, '.', '.') }}">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="col-form-label" for="plan_jasa">Plan Jasa</label>
                      <input type="text" readonly class="form-control input-transparent" name="plan_jasa" id="plan_jasa" value="{{ number_format($data->total_jasa_sp, 0, '.', '.') }}">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="col-form-label" for="rekon_mat">Rekon Material</label>
                      <input type="text" readonly class="form-control input-transparent price" name="rekon_mat" id="rekon_mat" value="{{ number_format($data->total_material_rekon, 0, '.', '.') }}">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="col-form-label" for="rekon_jasa">Rekon Jasa</label>
                      <input type="text" readonly class="form-control input-transparent" name="rekon_jasa" id="rekon_jasa" value="{{ number_format($data->total_jasa_rekon, 0, '.', '.') }}">
                    </div>
                  </div>
                </div>
                @if (session('auth')->proc_level == 1 && $data->step_id >= 10 || in_array(session('auth')->proc_level, [4, 99, 44]) )
                  <div class="row border-top py-3">
                    <div class="card shadow mb-4">
                      <div class="card-header">
                        <strong class="card-title">Amandemen Perjanjian Kerja Sama</strong>
                      </div>
                      <div class="card-body">
                        <div class="form-group row">
                          <label class="col-form-label col-md-3" for="nomor_pks">Nomor</label>
                          <div class="col-md-9">
                            <input rows="2" style="resize:none;" cols="50" id="nomor_pks" name="nomor_pks" class="form-control input-transparent" value="{{ $data->pks }}">
                          </div>
                        </div>
                        <div class="form-group row">
                          <label class="col-form-label col-md-3" for="tgl_pks">Tanggal</label>
                          <div class="col-md-9">
                            <input rows="2" style="resize:none;" cols="50" id="tgl_pks" name="tgl_pks" class="form-control date-picker input-transparent" value="{{ $data->tgl_pks }}">
                          </div>
                        </div>
                        <div class="form-group row">
                          <label class="col-form-label col-md-3" for="amd_pks">Amandemen</label>
                          <div class="col-md-9">
                            <input rows="2" style="resize:none;" cols="50" id="amd_pks" name="amd_pks" class="form-control input-transparent" value="{{ $data->amd_pks ?? '' }}">
                          </div>
                        </div>
                        <div class="form-group row">
                          <label class="col-form-label col-md-3" for="tgl_amd_pks">Tanggal</label>
                          <div class="col-md-9">
                            <input rows="2" style="resize:none;" cols="50" id="tgl_amd_pks" name="tgl_amd_pks" class="form-control date-picker input-transparent" required="required" value="{{ $data->tgl_amd_pks ?? date('Y-m-d') }}">
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="card shadow mb-4">
                      <div class="card-header">
                        <strong class="card-title">Amandemen SP</strong>
                      </div>
                      <div class="card-body">
                        <div class="form-group row">
                          <label class="col-form-label col-md-3" for="nomor_sp">Nomor</label>
                          <div class="col-md-9">
                            <input rows="2" style="resize:none;" cols="50" id="nomor_sp" name="nomor_sp" class="form-control input-transparent" value="{{ $data->no_sp }}">
                          </div>
                        </div>
                        <div class="form-group row">
                          <label class="col-form-label col-md-3" for="tgl_sp">Tanggal</label>
                          <div class="col-md-9">
                            <input rows="2" style="resize:none;" cols="50" id="tgl_sp" name="tgl_sp" class="form-control date-picker input-transparent" value="{{ $data->tgl_sp }}">
                          </div>
                        </div>
                        <div class="form-group row">
                          <label class="col-form-label col-md-3" for="amd_sp">Amandemen</label>
                          <div class="col-md-9">
                            <input rows="2" style="resize:none;" cols="50" id="amd_sp" name="amd_sp" class="form-control input-transparent" value="{{ $data->amd_sp ?? '' }}">
                          </div>
                        </div>
                        <div class="form-group row">
                          <label class="col-form-label col-md-3" for="tgl_amd_sp">Tanggal</label>
                          <div class="col-md-9">
                            <input rows="2" style="resize:none;" cols="50" id="tgl_amd_sp" name="tgl_amd_sp" class="form-control date-picker input-transparent" required="required" value="{{ $data->tgl_amd_sp ?? date('Y-m-d') }}">
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label class="col-form-label" for="bast_price_wo_ppn">Nilai BAST Tanpa PPN</label>
                              <input type="text" id="bast_price_wo_ppn" name="bast_price_wo_ppn" class="form-control input-transparent price" disabled value="{{ number_format(@$data->gd_rekon, 0, '.', '.') }}">
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label class="col-form-label" for="bast_price_w_ppn">Nilai BAST dengan PPN</label>
                              <input type="text" id="bast_price_w_ppn" name="bast_price_w_ppn" class="form-control input-transparent price" disabled value="{{ number_format(@$data->total_rekon, 0, '.', '.') }}">
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="border-top py-3">
                    @php
                      $toc_tgl = date('Y-m-d', strtotime($data->tgl_sp . ' + '. ($data->tgl_jatuh_tmp - 1) .' days') );
                    @endphp
                    <div class="card shadow mb-4">
                      <div class="card-header">
                        <strong class="card-title">BAUT</strong>
                      </div>
                      <div class="card-body row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label class="col-form-label" for="baut">Nomor</label>
                            <input type="text" class="form-control input-transparent price" name="baut" id="baut" value="{{ $data->BAUT }}">
                            <code>*Contoh No BAUT: 12345/HK.800/TA-123456/08(/ atau . atau -)2021</code>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label class="col-form-label" for="tgl_baut">Tanggal</label>
                            <input type="text" class="form-control date-picker input-transparent" name="tgl_baut" id="tgl_baut" value="{{ $data->tgl_baut ?? $toc_tgl }}">
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="card shadow mb-4">
                      <div class="card-header">
                        <strong class="card-title">BAST</strong>
                      </div>
                      <div class="card-body row">
                        <div class="col-md-6">
                          <div class="form-group">
                          <label class="col-form-label" for="bast">Nomor</label>
                          <input type="text" class="form-control input-transparent price" name="bast" id="bast" value="{{ $data->BAST }}">
                          </div>
                          <code>*Contoh No BAST: 12345/HK.800/TA-123456/08(/ atau . atau -)2021</code>

                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label class="col-form-label" for="tgl_bast">Tanggal</label>
                            <input type="text" class="form-control date-picker input-transparent" name="tgl_bast" id="tgl_bast" value="{{ $data->tgl_bast ?? date('Y-m-d') }}">
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="card shadow mb-4">
                      <div class="card-header">
                        <strong class="card-title">BA ABD</strong>
                      </div>
                      <div class="card-body row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label class="col-form-label" for="ba_abd">Nomor</label>
                            <input type="text" class="form-control input-transparent price" name="ba_abd" id="ba_abd" value="{{ $data->ba_abd }}">
                            <code>*Contoh No BA ABD: 12345/HK.800/TA-123456/08(/ atau . atau -)2021</code>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label class="col-form-label" for="tgl_ba_abd">Tanggal</label>
                            <input type="text" class="form-control input date-picker input-transparent" name="tgl_ba_abd" id="tgl_ba_abd" value="{{ $data->tgl_ba_abd ?? $toc_tgl }}">
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="card shadow mb-4">
                      <div class="card-header">
                        <strong class="card-title">BA Rekon</strong>
                      </div>
                      <div class="card-body row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label class="col-form-label" for="ba_rekon">Nomor BA Rekon</label>
                            <input type="text" class="form-control input-transparent price" name="ba_rekon" id="ba_rekon" value="{{ $data->ba_rekon }}">
                            <code>*Contoh No BA REKON: 12345/HK.800/TA-123456/08(/ atau . atau -)2021</code>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label class="col-form-label" for="tgl_ba_rekon">Tanggal BA Rekon</label>
                            <input type="text" class="form-control input date-picker input-transparent" name="tgl_ba_rekon" id="tgl_ba_rekon" value="{{ $data->tgl_ba_rekon ?? $toc_tgl }}">
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="card shadow mb-4">
                      <div class="card-header">
                        <strong class="card-title">BAPP</strong>
                      </div>
                      <div class="card-body row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label class="col-form-label" for="bapp">Nomor</label>
                            <input type="text" class="form-control input-transparent price" name="bapp" id="bapp" value="{{ $data->BAPP }}">
                            <code>*Contoh No BAPP: 12345/HK.800/TA-123456/08(/ atau . atau -)2021</code>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label class="col-form-label" for="tgl_bapp">Tanggal</label>
                            <input type="text" class="form-control input date-picker input-transparent" name="tgl_bapp" id="tgl_bapp" value="{{ $data->tgl_bapp ?? $toc_tgl }}">
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                @endif
                @if (in_array(session('auth')->proc_level, [2, 3]) && $data->step_id >= 11 || in_array(session('auth')->proc_level, [4, 99, 44]) )
                  <div class="border-top py-3">
                    <div class="form-group row">
                      <label class="col-form-label col-md-2 pull-right" for="no_invoice">Nomor Invoice</label>
                      <div class="col-md-10">
                        <input type="text" class="form-control input-transparent" name="no_invoice" id="no_invoice" value="{{ Request::old('no_invoice') ?? @$data->invoice }}">
                        <code>*Contoh No Invoice: 123.a/PT.MITRA/BJB-BJM/VIII(/ atau . atau -)2021</code>
                        <div class="custom-control custom-checkbox">
                          <input type="checkbox" class="custom-control-input" id="chck_rmv_invoice" name="chck_rmv_invoice" value="rmv_invoice">
                          <label class="custom-control-label" for="chck_rmv_invoice">Jangan Pakai Karakter Spesial Untuk Nomor Invoice</label>
                        </div>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-form-label col-md-2 pull-right" for="spp_num">Nomor SPP</label>
                      <div class="col-md-10">
                        <input type="text" id="spp_num" name="spp_num" class="form-control input-transparent" value="{{ Request::old('spp_num') ?? @$data->spp_num }}">
                        <code>*Contoh No SPP: 123.a/PT.MITRA/BJB-BJM/VIII(/ atau . atau -)2021</code>
                        <div class="custom-control custom-checkbox">
                          <input type="checkbox" class="custom-control-input" id="chck_rmv_spp" name="chck_rmv_spp" value="rmv_spp">
                          <label class="custom-control-label" for="chck_rmv_spp">Jangan Pakai Karakter Spesial Untuk Nomor SPP</label>
                        </div>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-form-label col-md-2 pull-right" for="receipt_num">Nomor Kwitansi</label>
                      <div class="col-md-10">
                        <input type="text" id="receipt_num" name="receipt_num" class="form-control input-transparent" value="{{ Request::old('receipt_num') ?? @$data->receipt_num }}">
                        <code>*Contoh No Kwitansi: 123.a/PT.MITRA/BJB-BJM/VIII(/ atau . atau -)2021</code>
                        <div class="custom-control custom-checkbox">
                          <input type="checkbox" class="custom-control-input" id="chck_rmv_kwitansi" name="chck_rmv_kwitansi" value="rmv_kwitansi">
                          <label class="custom-control-label" for="chck_rmv_kwitansi">Jangan Pakai Karakter Spesial Untuk Nomor Kwitansi</label>
                        </div>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-form-label col-md-2 pull-right" for="no_faktur">Nomor Seri Faktur Pajak</label>
                      <div class="col-md-10">
                        <input type="text" class="form-control input-transparent" name="no_faktur" id="no_faktur" value="{{ Request::old('no_faktur') ?? @$data->faktur }}">
                        {{-- <code>*Contoh No Faktur: 123.a/PT.MITRA/BJB-BJM/VIII(/ atau . atau -)2021</code>
                        <div class="custom-control custom-checkbox">
                          <input type="checkbox" class="custom-control-input" id="chck_rmv_faktur" name="chck_rmv_faktur" value="rmv_faktur">
                          <label class="custom-control-label" for="chck_rmv_faktur">Jangan Pakai Karakter Spesial Untuk Nomor Faktur</label>
                        </div> --}}
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-form-label col-md-2 pull-right" for="tgl_faktur">Tanggal Surat</label>
                      <div class="col-md-10">
                        <input type="text" class="form-control date-picker" name="tgl_faktur" id="tgl_faktur" value="{{ (Request::old('tgl_faktur') ? Request::old('tgl_faktur') : ( @$data->tgl_faktur ?? date('Y-m-d') ) ) }}" aria-describedby="button-addon2">
                      </div>
                    </div>
                  </div>
                @endif
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="card shadow mb-4">
							<button type="submit" class="btn btn-primary"><i class="fe fe-check fe-16"></i>&nbsp;Submit Form</button>
						</div>
					</div>
					<div class="col-md-6">
						<div class="card shadow mb-4">
							<a class="btn btn-info" href="/download/full_rar/{{ Request::segment(2) }}" type="submit"><i class="fe fe-download fe-16"></i>&nbsp;Download PO</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection
@section('footerS')
<script src="/js/moment.min.js"></script>
<script src='/js/jquery.timepicker.js'></script>
<script src='/js/daterangepicker.js'></script>
<script type="text/javascript">

$(function(){
	var data = {!! json_encode($data) !!},
	get_data;

	$('.date-picker').daterangepicker({
    singleDatePicker: true,
    timePicker: false,
    showDropdowns: true,
    locale:{
      format: 'YYYY-MM-DD',
    }
  });

});
</script>
@endsection