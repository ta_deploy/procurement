@extends('layout')
@section('title', 'Dashboard Comparing RFC')
@section('headerS')
<link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" rel="stylesheet" />
<style>
	@media (min-width: 768px) {
		.modal-xl {
			width: 100%;
		max-width:1200px;
		}
	}
</style>
@endsection
@section('content')
@if (Session::has('alerts'))
  @foreach(Session::get('alerts') as $alert)
    <div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
  @endforeach
@endif
<div class="container-fluid">
  <div class="col-12">
    <div class="row items-align-baseline">
      <div class="col-md-12 col-lg-12">
        <div class="card shadow eq-card mb-4">
          <div class="card-header">
            <strong class="card-title">Nilai Rekon</strong>
          </div>
          <div class="card-body">
            <div class="row items-align-center">
              <div class="col-4 text-center">
                <p class="text-muted mb-1">Material Rekon</p>
                <h6 class="mb-1">Rp. {{ number_format($material_rekon, 0, '', '.') }}</h6>
              </div>
              <div class="col-4 text-center">
                <p class="text-muted mb-1">Jasa Rekon</p>
                <h6 class="mb-1">Rp. {{ number_format($jasa_rekon, 0, '', '.') }}</h6>
              </div>
              <div class="col-4 text-center">
                <p class="text-muted mb-1">Total</p>
                <h6 class="mb-1">Rp. {{ number_format($material_rekon + $jasa_rekon, 0, '', '.') }}</h6>
              </div>
            </div>
            <div class="row items-align-center border-top" style="margin-top: 15px;">
              <div class="col-4 text-center">
                <p class="text-muted mb-1">Material RFC</p>
                <h6 class="mb-1">Rp. {{ number_format($material_rfc, 0, '', '.') }}</h6>
              </div>
              <div class="col-4 text-center">
                <p class="text-muted mb-1">Jasa RFC</p>
                <h6 class="mb-1">Rp. {{ number_format($jasa_rfc, 0, '', '.') }}</h6>
              </div>
              <div class="col-4 text-center">
                <p class="text-muted mb-1">Total</p>
                <h6 class="mb-1">Rp. {{ number_format($material_rfc + $jasa_rfc, 0, '', '.') }}</h6>
              </div>
            </div>
          </div> <!-- .card-body -->
        </div> <!-- .card -->
      </div> <!-- .col -->
      <div class="col-md-12 col-lg-12">
        <div class="card shadow eq-card mb-4">
          <div class="card-header">
            <strong class="card-title">Selisih</strong>
          </div>
          <div class="card-body">
            <div class="row items-align-center">
              <div class="col-4 text-center">
                <p class="text-muted mb-1">Material</p>
                <h6 class="mb-1">Rp. {{ number_format(abs($material_rekon - $material_rfc), 0, '', '.') }}</h6>
              </div>
              <div class="col-4 text-center">
                <p class="text-muted mb-1">Jasa</p>
                <h6 class="mb-1">Rp. {{ number_format(abs($jasa_rekon - $jasa_rfc), 0, '', '.') }}</h6>
              </div>
              <div class="col-4 text-center">
                <p class="text-muted mb-1">Total</p>
                <h6 class="mb-1">Rp. {{ number_format( abs( ($material_rekon + $jasa_rekon) - ($material_rfc + $jasa_rfc) ), 0, '', '.') }}</h6>
              </div>
            </div>
          </div> <!-- .card-body -->
        </div> <!-- .card -->
      </div> <!-- .col -->
    </div>
    <div class="mb-2 align-items-center">
      <h2 class="page-title">Perhitungan Material Sisa RFC vs Rekon</h2>
      <div class="card shadow mb-4">
        <div class="card-body table-responsive">
          <table class="table table-striped table-bordered">
            <thead class="thead-dark">
              <tr>
                <th>Mitra</th>
                @foreach($header_mat as $v)
                  <th>{{ $v }}</th>
                @endforeach
              </tr>
            </thead>
            <tbody id="data_table1">
              @foreach($dm as $k => $v)
                <tr>
                  <td><a href="/Admin/list/compare_rfc/{{ urlencode($k) }}">{{ $k }}</a></td>
                  @foreach($v as $vv)
                    <td>{{ $vv['selisih'] == 0 ? '-' : $vv['selisih'] }}</td>
                  @endforeach
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('footerS')
<script type="text/javascript">
	$(function(){
		$.fn.digits = function(){
			return this.each(function(){
					$(this).text( $(this).text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") );
			});
		}
    $("[data-toggle=popover]").popover({ container: 'body' });
	});
</script>
@endsection