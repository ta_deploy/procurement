@extends('layout')
@section('title', 'Form Planning')
@section('headerS')
<link rel="stylesheet" href="/css/dataTables.bootstrap4.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<style>
	th, td {
		text-align: center;
	}
</style>
@endsection
@section('content')
<div class="container-fluid">
	@if (Session::has('alerts'))
		@foreach(Session::get('alerts') as $alert)
			<div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
		@endforeach
	@endif
	<div class="row justify-content-center">
    <div class="col-12">
      <div class="card-deck" style="display: block;">
        <div class="col-md-12 my-4">
          <form id="planning" class="row" method="post" autocomplete="off">
            {{ csrf_field() }}
            <div class="col-md-12">
              <div class="form-row">
                <div class="form-group col-md-12">
                  <input type="text" class="form-control input-transparent" name="judul" placeholder="Judul Pekerjaan" required>
                  <input type="hidden" name="field_table">
                </div>
              </div>
            </div>
          </form>
        </div>
        <div class="col-md-12 my-4">
          <div class="card shadow mb-4">
            <div class="card-body table-responsive">
              <button type="button" class="btn mb-2 btn-primary btn_row"><i class="fe fe-edit"></i>&nbsp;Tambah Baris Tabel</button>
              <h5 class="card-title">Table Rancangan BOQ</h5>
              <table id="tb_hss" class="table table-striped table-bordered table-hover">
                <thead class="thead-dark">
                  <tr>
                    <th>Designator</th>
                    <th>Uraian</th>
                    <th>Satuan</th>
                    <th>Material</th>
                    <th>Jasa</th>
                    <th style="width: 25%">Tiket</th>
                    <th>Jumlah</th>
                    <th class="hidden-xs">Action</th>
                  </tr>
                </thead>
                <tbody id="body_boq_plan"></tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
	</div>
  <button type="button" class="btn mb-2 btn_submit btn-primary btn-block"><i class="fe fe-edit"></i>&nbsp;Simpan Data</button>

</div>
@endsection
@section('footerS')
<script src='/js/jquery.dataTables.min.js'></script>
<script src="/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script type="text/javascript">
  $(function(){
    let material = {!! json_encode($material) !!},
    data_select2 = [];

    $.each(material, function(k, v){
      data_select2.push({
        id: v.id,
        text: v.designator
      });
    });

    $(document).on('click', '.copy_detail_tool', function(){
			var data = $(this).data('original-title');
			var $temp = $("<input>");
			$("body").append($temp);
			$temp.val(data).select();
			document.execCommand("copy");
			$temp.remove();

			toastr.options = {
				"debug": false,
				"newestOnTop": false,
				"progressBar": true,
				"positionClass": "toast-top-center",
				"preventDuplicates": false,
				"onclick": null,
				"showDuration": "300",
				"hideDuration": "1000",
				"timeOut": "3000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut"
			}
			toastr["success"]("Designator Berhasil Disalin!", "Sukses")
		});

		$("body").tooltip({ selector: '[data-toggle=tooltip]' });

    $('.btn_row').on('click', function(e){
      let html = "<tr class='content_design'>";
      html += "<td class='field_designator'><select class='form-control input-transparent designator'><option></option></select></td>";
      html += "<td class='uraian'></td>";
      html += "<td class='satuan'></td>";
      html += "<td class='material'></td>";
      html += "<td class='jasa'></td>";
      html += "<td class='field_no_tiket'><select class='form-control input-transparent no_tiket' multiple></select></td>";
      html += "<td class='jml'></td>";
      html += "<td><button type='button' class='btn mb-2 btn-danger delete_row'><i class='fe fe-trash'></i>&nbsp;Hapus</button></td>";
      html += '</tr>';

      $('#body_boq_plan').append(html);

      $('.designator').each(function() {
        $(this).select2({
          width: '100%',
          data: data_select2,
          placeholder: 'Pilih Material'
        });
      });

      $('.no_tiket').each(function() {
        $(this).select2({
          width: '100%',
          placeholder: 'Nomor Tiket Lebih Dari Satu',
          allowClear: true,
          minimumInputLength: 2,
          ajax: {
            url: "/get_ajx/get_tiket",
            dataType: 'json',
            delay: 250,
            data: function (params) {
              return {
                searchTerm: params.term,
                design: $(this).parent().parent().find('.designator').val()
              };
            },
            processResults: function (response) {
              return {
                results: response
              };
            },
            cache: true,
            success: function(value) {
              // console.log(value)
            }
          }
        });
      });
    });

    $(document).on('click', '.delete_row', function(){
      $(this).parent().parent().remove();
    })

    $(document).on('select2:select', '.designator', function(){
      let isi = $(this).val(),
      get_data = $.grep(material, function(e){ return e.id == isi}),
      word = get_data[0].uraian.split(' '),
      text = '';

      if(word.length > 6){
        text = "...<a style='color:blue; cursor:pointer' class='copy_detail_tool' data-toggle='tooltip' title='"+get_data[0].uraian+"'>Lihat Detail</a>";
      }

      $(this).parent().parent().find('.uraian').html(word.slice(0, 6).join(' ') + text)
      $(this).parent().parent().find('.satuan').html(get_data[0].satuan)
      $(this).parent().parent().find('.material').html(get_data[0].material)
      $(this).parent().parent().find('.jasa').html(get_data[0].jasa)
      $(this).parent().parent().find('.no_tiket').val(null).change();
    });

    $('.btn_submit').on('click', function(e){
      var final_data = [];

      $('.content_design').each(function(){
        let design = $(this).find('.designator').val(),
        tiket = $(this).find('.no_tiket').val();

        final_data.push({
          design: design,
          no_tiket: tiket
        });

      });

      $("input[name='field_table']").val(JSON.stringify(final_data) );

      $('#planning').submit();
    })
  });
</script>
@endsection