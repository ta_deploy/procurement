@extends('layout')
@section('title', 'Upload Massal')
@section('headerS')
<link rel="stylesheet" href="/css/dataTables.bootstrap4.css">
{{-- <link rel="stylesheet" href="https://unpkg.com/dropzone@5/dist/min/dropzone.min.css" type="text/css" /> --}}
<style type="text/css">
	.pull-right {
		text-align: right;
	}
</style>
@endsection
@section('content')
@if (Session::has('alerts'))
  @foreach(Session::get('alerts') as $alert)
    <div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
  @endforeach
@endif
<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-md-12">
			<h2 class="page-title">Pekerjaan</h2>
			<div class="card-deck">
        <form id="form_temuan" class="dropzone" enctype="multipart/form-data">
					{{ csrf_field() }}
        </form>
        {{-- <button type="submit" class="btn btn-primary pencet"><i class="ion ion-upload"></i>&nbsp;&nbsp;Submit Data</button> --}}
      </div>
		</div>
	</div>
</div>
@endsection
@section('footerS')
<script src="https://unpkg.com/dropzone@5/dist/min/dropzone.min.js"></script>
<script type="text/javascript">
  Dropzone.autoDiscover = false;
	$(function(){
		var myDropzone = new Dropzone("#form_temuan", {
      // autoProcessQueue: false,
      parallelUploads: 2,
      url: '/upload_massal',
      uploadMultiple: true,
      filesizeBase:    1000,
      init: function() {
        var mydropzone = this;

        $('.pencet').on("click", function(e) {
          e.preventDefault();
          e.stopPropagation();
          mydropzone.processQueue();
        });

        this.on("sendingmultiple", function(data, xhr, formData) {
          formData.append("id", status.id );
        });

        this.on("successmultiple", function(data, xhr, formData) {
          console.log('tes')
        });
      },
      success: function(file, response){

      }
    });
	})
</script>
@endsection