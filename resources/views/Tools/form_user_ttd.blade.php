@extends('layout')
@section('title', empty($data) ? 'Tambah User' : 'Edit User')
@section('headerS')
<link rel="stylesheet" href="/css/dataTables.bootstrap4.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<style type="text/css">
	.pull-right {
		text-align: right;
	}
</style>
@endsection
@section('content')
@if (Session::has('alerts'))
  @foreach(Session::get('alerts') as $alert)
    <div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
  @endforeach
@endif
<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-md-12">
			<h2 class="page-title">{{ empty($data) ? 'Tambah User Baru' : 'Edit User' }}</h2>
			<div class="card-deck" style="display: block">
				<form class="row" method="post" enctype="multipart/form-data">
					{{ csrf_field() }}
					<div class="col-md-12">
						<div class="card shadow mb-4">
							<div class="card-body">
                <div class="form-group row">
                  <label class="col-form-label col-md-2 pull-right" for="nik">Nik</label>
                  <div class="col-md-10">
                      <input type="text" id="nik" name="nik" class="form-control input-transparent" value="{{ !empty($data->nik) ? $data->nik : '' }}">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-form-label col-md-2 pull-right" for="nama">Nama</label>
                  <div class="col-md-10">
                      <input type="text" id="nama" name="nama" class="form-control input-transparent" value="{{ !empty($data->user) ? $data->user : '' }}">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-form-label col-md-2 pull-right" for="jabatan">Jabatan</label>
                  <div class="col-md-10">
                      <input type="text" id="jabatan" name="jabatan" class="form-control input-transparent" value="{{ !empty($data->jabatan) ? $data->jabatan : '' }}">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-form-label col-md-2 pull-right" for="pekerjaan">Pekerjaan</label>
                  <div class="col-md-10">
                    <select name="pekerjaan" class="form-control input-transparent" id="pekerjaan">
											@foreach ($kerjaan[0] as $val)
												<option value="{{ $val->id }}">{{ $val->text }}</option>
											@endforeach
										</select>
                  </div>
                </div>
								{{-- <div class="form-group row">
                  <label class="col-form-label col-md-2 pull-right" for="status">Status Dokumen</label>
                  <div class="col-md-10">
                    <select name="status" class="form-control input-transparent" id="status">
										</select>
                  </div>
                </div> --}}
								<div class="form-group mb-3">
									<div class="custom-file">
										<button type='submit' class="btn btn-block btn-primary">Submit</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
		</div>
	</div>
</div>
@endsection
@section('footerS')
<script src='/js/jquery.dataTables.min.js'></script>
<script src='/js/dataTables.bootstrap4.min.js'></script>
<script src='/js/select2.min.js'></script>
<script type="text/javascript">
	$(function(){
		$('#pekerjaan').select2({
			width: '100%',
			placeholder: 'Silahkan Pilih Pekerjaan'
		});

    $('#pekerjaan').val(null).change();

    var status_dok = {!! json_encode($status_dok) !!},
    load_data = {!! json_encode($data) !!};

    // $('#pekerjaan').on('select2:select change', function(){
    //   var isi = $(this).val().replace(/[^\w\s]/gi, '');

    //   $("#status").empty().trigger('change');

    //   $('#status').select2({
    //     width: '100%',
    //     placeholder: 'Silahkan Pilih Status',
    //     data: status_dok[isi]
    //   });
    // })

    if(!$.isEmptyObject(load_data) ){
			$('#pekerjaan').val(load_data.pekerjaan).trigger('change');
			$('#status').val(load_data.status).trigger('change');
		}
	})
</script>
@endsection