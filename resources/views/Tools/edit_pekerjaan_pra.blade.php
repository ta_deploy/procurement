@extends('layout')
@section('title', 'Edit Pra Kerja')
@section('style')
@endsection
@section('headerS')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="/css/daterangepicker.css">
<style type="text/css">
  .select2-results__option, select{
      color: black;
  }

  .pull-right {
		text-align: right;
	}

  @media (min-width: 768px) {
		.modal-xl {
			width: 100%;
		max-width:1200px;
		}
	}

	.select2-search--inline {
    display: contents;
	}

	.select2-search__field:placeholder-shown {
		width: 100% !important;
	}
</style>
@endsection
@section('content')
@if (Session::has('alerts'))
	@foreach(Session::get('alerts') as $alert)
		<div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
	@endforeach
@endif
<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-md-12"> <h2 class="page-title">Edit Pekerjaan Pra Kerja</h2>
			<div class="card-deck" style="display: block">
				<form id="input_spen" class="row" method="post">
					{{ csrf_field() }}
					<div class="col-md-12">
						<div class="card shadow mb-4">
							<div class="card-header">
								<strong class="card-title">{{ $data->judul }}</strong>
							</div>
							<div class="card-body">
                <div class="row">
                  <div class="form-group mb-3 col-md-6">
                    <label class="col-form-label pull-right" for="title">PID</label>
                    <textarea type="text" style="resize: none;" rows="4" class="form-control" name="title" id="title" readonly>{{ $data->id_project }}</textarea>
                  </div>
                  <div class="form-group mb-3 col-md-6">
                    <label class="col-form-label pull-right" for="beforeppn_spen">Harga Sebelum PPN</label>
                    <input type="text" class="form-control beforeppn" name="beforeppn_spen" required id="beforeppn_spen" readonly value="{{ $data->gd_sp ?? '' }}">
                  </div>
                </div>
                <div class="row">
                  <div class="form-group col-md-6">
                    <label class="col-form-label" for="sp">Surat Penetapan:</label>
                    <p class="form-control input-transparent" readonly>{{ $data->surat_penetapan }}</p>
                  </div>
                  <div class="form-group col-md-6">
                    <label class="col-form-label" for="tngl_s_pen">Tanggal Surat Penetapan:</label>
                    <p class="form-control input-transparent" readonly>{{ $data->tgl_s_pen }}</p>
                  </div>
                </div>
                <div class="row">
                  <div class="form-group col-md-6">
                    <label class="col-form-label">Due Date:</label>
                    <p class="form-control input-transparent" readonly>{{ date('Y-m-d', strtotime('+'. @$data->tgl_jatuh_tmp. ' day', strtotime(@$data->tgl_s_pen))) }}</p>
                  </div>
                  <div class="form-group col-md-6">
                    <label class="col-form-label">Durasi:</label>
                    <p class="form-control input-transparent" readonly>{{ $data->tgl_jatuh_tmp }} Hari</p>
                  </div>
                </div>
                @if (session('auth')->proc_level == 1 && $data->step_id >= 3 || in_array(session('auth')->proc_level, [4, 99, 44]) )
                  <div class="row border-top py-3">
                    <div class="col-md-4">
                      <div class="form-group">
                        <label class="col-form-label" for="pks">Nomor Kontrak</label>
                        <input type="text" class="form-control" name="pks" required id="pks" value="{{ $data->no_khs ?? '' }}">
                        <code>*Contoh No Kontrak: 12345/HK.810/TA-123456/08-2020</code>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label class="col-form-label" for="tgl_pks">Tanggal Kontrak</label>
                        @php
                          $bulan = array (
                            'Januari' => '01',
                            'Februari' => '02',
                            'Maret' => '03',
                            'April' => '04',
                            'Mei' => '05',
                            'Juni' => '06',
                            'Juli' => '07',
                            'Agustus' => '08',
                            'September' => '09',
                            'Oktober' => '10',
                            'November' => '11',
                            'Desember' => '12'
                          );

                          $tgl_conv = date('Y-m-d');

                          if($data->tgl_khs_maintenance)
                          {
                            $tgl = explode(' ', $data->tgl_khs_maintenance);
                            $tgl_conv = $tgl[2] .'-'. $bulan[$tgl[1] ] .'-'. $tgl[0];
                          }

                          if($data->tgl_pks == 0000-00-00 || empty($data->tgl_pks) )
                          {
                            $data->tgl_pks = $tgl_conv;
                          }
                        @endphp
                        <input type="text" id="tgl_pks" name="tgl_pks" class="form-control date-picker" required value="{{ $data->tgl_pks }}">
                      </div>
                    </div>
                    <div class="form-group mb-3 col-md-4">
                      <label class="col-form-label" for="lokasi_project">Lokasi Kerja</label>
                      <input type="text" class="form-control" name="lokasi_project" required id="lokasi_project" value="{{ $data->lokasi ?? 'BANJARMASIN WITEL KALSEL' }}">
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group mb-3 col-md-4">
                      <label class="col-form-label" for="s_pen">Nomor Penetapan</label>
                      <input type="text" id="s_pen" name="s_pen" class="form-control" value="{{ $data->surat_penetapan ?? '' }}" required>
                      <code>*Contoh No Penetapan: 12345/HK.810/TA-123456/08-2020</code>
                    </div>
                    <div class="form-group mb-3 col-md-4">
                      <label class="col-form-label" for="tgl_s_pen">Tanggal Penetapan</label>
                      <input type="text" id="tgl_s_pen" name="tgl_s_pen" class="form-control date-picker" required value="{{ !empty($data->tgl_s_pen) ? $data->tgl_s_pen : '' }}">
                    </div>
                    <div class="form-group mb-3 col-md-4">
                      <label class="col-form-label" for="toc">TOC</label>
                      <input type="text" id="toc" name="toc" class="form-control" autocomplete="off" required="required" value="{{ !empty($data->toc) ? $data->toc : '' }}">
                    </div>
                  </div>
                @endif
                <div class="border-top py-3">
                  <div class="form-group row">
                    <label class="col-form-label col-md-2 pull-right" for="pks">PKS</label>
                    <div class="col-md-10">
                      <input type="text" class="form-control" name="pks" required id="pks" value="{{ $data->pks }}">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-form-label col-md-2 pull-right" for="tgl_pks">Tanggal PKS</label>
                    <div class="col-md-10">
                      <input type="text" id="tgl_pks" name="tgl_pks" class="form-control date-picker" required value="{{ $data->tgl_pks }}">
                    </div>
                  </div>
                </div>
                @if (in_array(session('auth')->proc_level, [2, 3]) && $data->step_id >= 4 || in_array(session('auth')->proc_level, [4, 99, 44]) )
                  <div class="form-group row">
                    <label class="col-form-label col-md-2 pull-right" for="surat_kesanggupan">No. Kesanggupan</label>
                    <div class="col-md-10">
                      <input type="text" class="form-control input-transparent" name="surat_kesanggupan" id="surat_kesanggupan" value="{{ @$data->surat_kesanggupan ?? '' }}">
                      <code>*Contoh No Kesanggupan: 123.a/PT.MITRA/BJB-BJM/(VIII atau 8)(/ atau . atau -)2021</code>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-form-label col-md-2 pull-right" for="tgl_sggp">Tanggal Kesanggupan</label>
                    <div class="col-md-10">
                      <select class="form-control input-transparent" name="tgl_sggp" id="tgl_sggp">
                        <option value="{{ date('Y-m-d', strtotime("+1 days", strtotime($data->tgl_s_pen))) }}">{{ date('Y-m-d', strtotime("+1 days", strtotime($data->tgl_s_pen))) }} (H +1 Dari Tanggal Penetapan)</option>
                        <option value="{{ date('Y-m-d', strtotime($data->tgl_s_pen)) }}">{{ date('Y-m-d', strtotime($data->tgl_s_pen)) }} (Tanggal Penetapan)</option>
                      </select>
                    </div>
                  </div>
                @endif
                @if (session('auth')->proc_level == 1 && $data->step_id == 5 || in_array(session('auth')->proc_level, [4, 99, 44]) )
                  <div class="form-group row border-top py-3">
                    <div class="form-group col-md-4">
                      <label class="col-form-label" for="no_sp">Nomor Pesanan</label>
                      <input type="text" id="no_sp" name="no_sp" class="form-control" value="{{ $data->no_sp }}" required>
                      <code>*Contoh No Pesanan: 12345/HK.810/TA-123456/08-2020</code>
                    </div>
                    <div class="form-group col-md-4">
                      <label class="col-form-label" for="tgl_sp">Tanggal Pesanan</label>
                      @php
                        $date = date('Y-m-d', strtotime(date($data->bulan_pengerjaan.'-01') . "first day of this month") );
                      @endphp
                      @if($data->pekerjaan == "PSB")
                        <input type="text" id="tgl_sp" name="tgl_sp" class="form-control date-picker" required="required" value="{{ $data->tanggal_boq }}">
                      @else
                        <input type="text" id="tgl_sp" name="tgl_sp" class="form-control date-picker" required="required" value="{{ $data->tgl_sp ?? $date }}">
                      @endif
                    </div>
                    <div class="form-group col-md-4">
                      <label class="col-form-label" for="toc_sp">TOC</label>
                      <input type="text" id="toc_sp" name="toc_sp" class="form-control" autocomplete="off" required="required" value="{{ $data->toc }}">
                      <span id="durasi"></span>
                    </div>
                    <div class="form-group col-md-6">
                      <label class="col-form-label" for="lokasi_project">Lokasi Kerja</label>
                      <input type="text" class="form-control" name="lokasi_project" id="lokasi_project" required value="{{ $data->lokasi ?? 'BANJARMASIN WITEL KALSEL' }}">
                    </div>
                    @php
                      $total_nilai = $data->psb_hss_1p * $data->psb_ssl_1p + $data->psb_hss_2p * $data->psb_ssl_2p + $data->psb_hss_3p * $data->psb_ssl_3p + $data->setting_hss_stb * $data->setting_ssl_stb + $data->setting_hss_wifiext * $data->setting_ssl_wifiext + $data->ontpremium_hss * $data->ontpremium_ssl + $data->inserttiang_hss * $data->inserttiang_ssl;
                    @endphp
                    <div class="form-group col-md-6">
                      <label class="col-form-label" for="beforeppn">Harga Sebelum PPN</label>
                      @if($data->pekerjaan == "PSB")
                        <input type="text" class="form-control beforeppn" name="beforeppn" id="beforeppn" value="{{ $total_nilai ?? '' }}" readonly>
                      @else
                        <input type="text" class="form-control beforeppn" name="beforeppn" id="beforeppn" value="{{ $data->gd_sp ?? '' }}" required>
                      @endif
                    </div>
                  </div>
                @endif
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="card shadow mb-4">
							<button type="submit" class="btn btn-primary"><i class="fe fe-check fe-16"></i>&nbsp;Submit Form</button>
						</div>
					</div>
					<div class="col-md-6">
						<div class="card shadow mb-4">
							<a type="submit" class="btn btn-info" href="/download/full_rar/{{ Request::segment(2) }}"><i class="fe fe-download fe-16"></i>&nbsp;Download PO</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection
@section('footerS')
<script src="/js/moment.min.js"></script>
<script src='/js/jquery.timepicker.js'></script>
<script src='/js/daterangepicker.js'></script>
<script type="text/javascript">

$(function(){
	var data = {!! json_encode($data) !!},
	get_data;
  $('#tgl_sggp').val(data.tgl_surat_sanggup).change();

	$('.date-picker').daterangepicker({
    singleDatePicker: true,
    timePicker: false,
    showDropdowns: true,
    locale:{
      format: 'YYYY-MM-DD',
    }
  });

  $('.beforeppn').val(function(index, value) {
		return value
		.replace(/\D/g, "")
		.replace(/\B(?=(\d{3})+(?!\d))/g, ".")
		;
	});

  $('.beforeppn').val(function(index, value) {
		return value
		.replace(/\D/g, "")
		.replace(/\B(?=(\d{3})+(?!\d))/g, ".")
		;
	});

	$('.beforeppn').keyup(function(event) {
		if(event.which >= 37 && event.which <= 40) return;
		$(this).val(function(index, value) {
			return value
			.replace(/\D/g, "")
			.replace(/\B(?=(\d{3})+(?!\d))/g, ".")
			;
		});
	});

	$('#tgl_sp').on('change', function(e) {
		var kalkulasi_date = new Date(new Date($('#tgl_sp').val()).setDate(new Date($('#tgl_sp').val()).getDate() + parseInt($('#toc_sp').val().length != '0' ? parseInt($('#toc_sp').val() ) - 1 : 0))).toISOString().slice(0, 10)
		$('#durasi').html('<code>*Tanggal toc_sp Adalah '+kalkulasi_date+'</code>');
	});

	$('#toc_sp').on('keypress keyup', function(e) {
    console.log($(this).val())
		if (/\D/g.test(this.value))
		{
			this.value = this.value.replace(/\D/g, '');
		}
			var kalkulasi_date = new Date(new Date($('#tgl_sp').val()).setDate(new Date($('#tgl_sp').val()).getDate() + parseInt($('#toc_sp').val().length != '0' ? parseInt($('#toc_sp').val() )  - 1 : 0))).toISOString().slice(0, 10)
			$('#durasi').html('<code>*Tanggal TOC Adalah '+kalkulasi_date+'</code>');
	});

	if (/\D/g.test($('#toc_sp').val()))
	{
		$('#toc_sp').val() = $('#toc_sp').val().replace(/\D/g, '');
	}

	var kalkulasi_date = new Date(new Date($('#tgl_s_pen').val()).setDate(new Date($('#tgl_s_pen').val()).getDate() + parseInt($('#toc_sp').val().length != '0' ? parseInt($('#toc_sp').val() )  - 1 : 0))).toISOString().slice(0, 10)
	$('#durasi').html('<code>*Tanggal TOC Adalah '+kalkulasi_date+'</code>');
});
</script>
@endsection