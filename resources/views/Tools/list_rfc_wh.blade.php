@extends('layout')
@section('title', 'List Material WH dan RFC')
@section('headerS')
<link rel="stylesheet" href="/css/dataTables.bootstrap4.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" rel="stylesheet" />

<style>
	@media (min-width: 768px) {
		.modal-xl {
			width: 100%;
		max-width:1200px;
		}
	}
</style>
@endsection
@section('content')
@if (Session::has('alerts'))
  @foreach(Session::get('alerts') as $alert)
    <div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
  @endforeach
@endif
<div class="container-fluid">
  <div class="row justify-content-center">
    <div class="col-12">
      <h2 class="page-title">List Sync Material WH dan RFC</h2>
      <div class="row">
        <div class="col-md-12">
          <div class="card shadow">
            <div class="card-body table-responsive">
              <table class="table table-striped table-bordered table-hover">
                <thead class="thead-dark">
                  <tr>
                    <th>#</th>
                    <th>Material WH</th>
                    <th>Material RFC</th>
                  </tr>
                </thead>
                <tbody id="data_table1">
                  @php
                    $no = 0;
                  @endphp
                  @foreach($parent_wh as $k => $v)
                    @php
                      $first = true;
                    @endphp
                    @foreach($v as $vv)
                      <tr>
                      @if($first == true)
                        <td rowspan="{{ count($v) }}"> {{ ++$no }} </td>
                        <td rowspan="{{ count($v) }}"> {{ $k }} </td>
                        @php
                          $first = false
                        @endphp
                      @endif
                        <td> {{ $vv->design_rfc}} </td>
                      </tr>
                    @endforeach
                  @endforeach
                  @foreach($parent_rfc as $k => $v)
                    @php
                      $first = true;
                    @endphp
                    @foreach($v as $vv)
                      <tr>
                        @if($first == true)
                          <td rowspan="{{ count($v) }}"> {{ ++$no }} </td>
                        @endif
                        <td> {{ $vv}} </td>
                        @if($first == true)
                          <td rowspan="{{ count($v) }}"> {{ $k }} </td>
                          @php
                            $first = false
                          @endphp
                        @endif
                      </tr>
                    @endforeach
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div> <!-- end section -->
    </div> <!-- .col-12 -->
  </div> <!-- .row -->
</div>
@endsection
@section('footerS')
<script src='/js/jquery.dataTables.min.js'></script>
<script src='/js/dataTables.bootstrap4.min.js'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script type="text/javascript">
	$(function(){
		$.fn.digits = function(){
			return this.each(function(){
					$(this).text( $(this).text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") );
			});
		}

		$('select').select2();

		$('.table').DataTable({
			autoWidth: true,
			columnDefs: [
				{
					targets: 'no-sort',
					orderable: false
				}
			],
			lengthMenu: [
				[16, 32, 64, -1],
				[16, 32, 64, "All"]
			]
		});
	});
</script>
@endsection