@extends('layout')
@section('title', 'Dashboard Rekon PS Mitra')
@section('headerS')
<style>
	tr, th, td {
        text-align: center;
    }
    .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
      padding: 1px 1px;
    }
  .bg-red {
    background-color: #dd4b39;
    font-weight: bold;
    color: white !important;
  }
  .bg-red-rows {
    background-color: #dd4c39be;
    color: white !important;
  }
</style>
<link rel="stylesheet" href="/css/daterangepicker.css">
<link rel="stylesheet" href="/css/dataTables.bootstrap4.css">
@endsection
@section('content')
<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-12">
      @if (Session::has('alerts'))
        @foreach(Session::get('alerts') as $alert)
          <div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!} <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span></div>
        @endforeach
      @endif
      <h2 class="page-title">Dashboard Rekon PS Mitra</h2>
			  <div class="row">

          <div class="col-md-12">
            <p class="text-muted" style="float: left;">Last Update K-PRO {{ $log_kpro->LAST_SYNC }} WITA</p>
            <p class="text-muted" style="float: right;">Last Update UT Online {{ @$log_utonline->last_updated_at }} WITA</p>
          </div>

          <div class="col-md-12">
            <div class="card shadow mb-4">
              <div class="card-body">
                <form method="GET">
                  <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="witel">WITEL</label>
                        <select class="form-control select2" id="witel" readonly>
                          <option aria-readonly="">{{ $witel }}</option>
                        </select>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="startDate">START DATE</label>
                        <div class="input-group">
                          <input type="text" class="form-control drgpicker" id="startDate" name="startDate" value="{{ $startDate ? : date('Y-m-d') }}" aria-describedby="button-addon2" readonly>
                          <div class="input-group-append">
                            <div class="input-group-text" id="button-addon-date"><span class="fe fe-calendar fe-16"></span></div>
                          </div>
                        </div>
                      </div>
                      <div class="form-group col-md-3">
                        <label for="endDate">END DATE</label>
                        <div class="input-group">
                          <input type="text" class="form-control drgpicker" id="endDate" name="endDate" value="{{ $endDate ? : date('Y-m-d') }}" aria-describedby="button-addon2" readonly>
                          <div class="input-group-append">
                            <div class="input-group-text" id="button-addon-date"><span class="fe fe-calendar fe-16"></span></div>
                          </div>
                        </div>
                      </div>
                    <div class="form-group col-md-2">
                        <label for="search">&nbsp;</label>
                        <button type="submit" class="btn btn-primary btn-block">Search</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>

          <div class="col-md-12">
            @if(in_array(session('auth')->proc_level, [3, 4, 99, 44]))
              <a class="btn mb-2 btn-primary" href="/Rekon/create/provisioning/order_based" target="_blank"><i class="fe fe-edit"></i> Create Rekon Order Based (PSB)</a>
              
              <button type="button" class="btn mb-2 btn-primary" data-toggle="modal" data-target="#modalUploadCutoff"><i class="fe fe-edit"></i> Upload Data Cutoff Rekon (PSB)</button>
              <div class="modal fade" id="modalUploadCutoff" tabindex="-1" role="dialog" aria-labelledby="modalUploadCutoffTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                  <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="modalUploadCutoffTitle">Upload Data Cutoff Rekon (PSB)</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <form id="form_justifikasi" class="row" method="post" action="/Admin/save/filepsb" enctype="multipart/form-data" autocomplete="off">
                      {{ csrf_field() }}
                      <input type="hidden" name="id_file" value="cutoff_psb">
                      <div class="col-md-12">
                        <div class="form-row">
                          <div class="form-group col-md-4">
                            <label class="col-form-label pull-right" for="periode_kegiatan">Periode Kegiatan</label>
                              <select class="periode_kegiatan" name="periode_kegiatan" required>
                              @foreach ($bulan_format as $bulan)
                                <option value="{{ $bulan->id }}">{{ $bulan->text }}</option>
                              @endforeach
                              </select>
                          </div>
                          <div class="form-group col-md-4">
                            <label class="col-form-label pull-right" for="date_periode1">Waktu Pelaksanaan Awal</label>
                              <input type="text" class="form-control input-transparent date_periode1" name="periode1" readonly>
                          </div>
                          <div class="form-group col-md-4">
                            <label class="col-form-label pull-right" for="date_periode2">Waktu Pelaksanaan Akhir</label>
                              <input type="text" class="form-control input-transparent date_periode2" name="periode2" readonly>
                          </div>
                          <div class="form-group col-md-6">
                            <label class="col-form-label pull-right" for="mitra">Mitra</label>
                              <select class="mitra" name="mitra" required>
                              @foreach ($get_mitra as $mitra)
                                <option value="{{ $mitra->id }}">{{ $mitra->text }}</option>
                              @endforeach
                              </select>
                          </div>
                          <div class="form-group col-md-6">
                            <label class="col-form-label pull-right" for="layanan">Layanan</label>
                              <select class="layanan" name="layanan" required>
                              @foreach ($get_layanan as $layanan)
                                <option value="{{ $layanan->id }}">{{ $layanan->text }}</option>
                              @endforeach
                              </select>
                          </div>
                          <div class="form-group col-md-12">
                            <label for="sc_cutoff">Masukan SC</label>
                            <textarea class="form-control" id="sc_cutoff" rows="4" name="sc_cutoff"></textarea>
                            <span class="help-block"><small>Pisahkan dengan simbol ( ; )</small></span>
                          </div>
                          <div class="form-group col-md-12">
                            <button type="submit" class="btn btn-block btn_submit btn-primary">Upload</button>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn mb-2 btn-secondary" data-dismiss="modal">Close</button>
                  </div>
                  </div>
                </div>
              </div>
            @endif

            {{-- @if(in_array(session('auth')->proc_level, [3, 4]))
              <button type="button" class="btn mb-2 btn-primary" data-toggle="modal" data-target="#modalUploadRekon"><i class="fe fe-edit"></i> Upload Data Rekon (PSB)</button>
              <div class="modal fade" id="modalUploadRekon" tabindex="-1" role="dialog" aria-labelledby="modalUploadRekonTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                  <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="modalUploadRekonTitle">Upload Data Rekon (PSB)</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <form id="saveDataRekonPsb" class="row" method="post" action="/Admin/save/filepsb" enctype="multipart/form-data" autocomplete="off">
                      {{ csrf_field() }}
                      <input type="hidden" name="id_file" value="saveDataRekonPsb">
                      <input type="hidden" name="witel" value="{{ session('auth')->Witel_New }}">
                      <div class="col-md-12">
                        <div class="form-row">
                          <div class="form-group col-md-6">
                            <label class="col-form-label pull-right" for="mitra">Mitra</label>
                              <select class="mitra" name="mitra" required>
                              @foreach ($get_mitra as $mitra)
                                <option value="{{ $mitra->id }}">{{ $mitra->text }}</option>
                              @endforeach
                              </select>
                          </div>
                          <div class="form-group col-md-6">
                            <label class="col-form-label pull-right" for="layanan">Layanan</label>
                              <select class="layanan" name="layanan" required>
                              @foreach ($get_layanan as $layanan)
                                <option value="{{ $layanan->id }}">{{ $layanan->text }}</option>
                              @endforeach
                              </select>
                          </div>
                          <div class="form-group col-md-12">
                            <label for="sc_cutoff">Masukan SC</label>
                            <textarea class="form-control" id="sc_cutoff" rows="4" name="sc_cutoff" required></textarea>
                            <span class="help-block"><small>* Pisahkan dengan simbol ( ; )</small></span>
                          </div>
                          <div class="form-group col-md-12">
                            <button type="submit" class="btn btn-block btn_submit btn-primary">Upload</button>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn mb-2 btn-secondary" data-dismiss="modal">Close</button>
                  </div>
                  </div>
                </div>
              </div>
            @endif --}}
          </div>

          <div class="col-md-12">
            <div class="card shadow mb-4">
              <div class="card-body table-responsive">
                <table class="table table-hover table-striped table-bordered table-sm datatables" id="rekonKproMonth">
                  <thead>
                    <tr>
                      <th class="bg-red" rowspan="2">WITEL</th>
                      <th class="bg-red" colspan="12">K-PRO PS BULANAN : {{ date('Y') }}</th>
                      <th class="bg-red" rowspan="2">TOTAL</th>
                    </tr>
                    <tr>
                      @php
                        $months = ['januari' => '01', 'februari' => '02', 'maret' => '03', 'april' => '04', 'mei' => '05', 'juni' => '06', 'juli' => '07', 'agustus' => '08', 'september' => '09', 'oktober' => '10', 'november' => '11', 'desember' => '12'];
                      @endphp
                      @foreach ($months as $key => $value)
                      <th class="bg-red">{{ strtoupper($key) }}</th>
                      @endforeach
                    </tr>
                  </thead>
                </table>
              </div>
            </div>
          </div>

          <div class="col-md-12">
            <div class="card shadow mb-4">
              <div class="card-body table-responsive">
                <table class="table table-hover table-striped table-bordered table-sm datatables" id="rekonKproDay">
                  <thead>
                    <tr>
                      <th class="bg-red" rowspan="2">WITEL</th>
                      <th class="bg-red" colspan="{{ date('t') }}">K-PRO PS HARIAN : {{ strtoupper(date('F Y')) }}</th>
                    </tr>
                    <tr>
                      @for ($i = 1; $i < date('t') + 1; $i ++)
                        @if ($i < 10)
                            @php
                              $keys = '0'.$i;
                            @endphp
                        @else
                            @php
                              $keys = $i;
                            @endphp
                        @endif
                        <th class="bg-red">{{ $keys }}</th>
                      @endfor
                    </tr>
                  </thead>
                </table>
              </div>
            </div>
          </div>

          <div class="col-md-12">
            <div class="card shadow mb-4">
              <div class="card-body table-responsive">
                <table class="table table-hover table-striped table-bordered table-sm datatables" id="rekonKproOrder">
                  <thead>
                    <tr>
                        <th style="color: #6c757d; font-weight: bold;" rowspan="2">REG</th>
                        <th style="color: #6c757d; font-weight: bold;" rowspan="2">WITEL</th>
                        <th style="color: #6c757d; font-weight: bold;" rowspan="2">MITRA</th>
                        <th style="color: #6c757d; font-weight: bold;" colspan="8">PSB</th>
                        <th style="color: #6c757d; font-weight: bold;" colspan="8">PSB + SURVEY</th>
                        <th style="color: #6c757d; font-weight: bold;" colspan="22">ADDON</th>
                        <th style="color: #6c757d; font-weight: bold;" colspan="2">TOTAL</th>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">1P</td>
                        <td style="font-weight: bold;">Jasa (Rp)</td>
                        <td style="font-weight: bold;">2P<br />(Tlp+Int)</td>
                        <td style="font-weight: bold;">Jasa (Rp)</td>
                        <td style="font-weight: bold;">2P<br />(Int+IPTV)</td>
                        <td style="font-weight: bold;">Jasa (Rp)</td>
                        <td style="font-weight: bold;">3P</td>
                        <td style="font-weight: bold;">Jasa (Rp)</td>
                        <td style="font-weight: bold;">1P</td>
                        <td style="font-weight: bold;">Jasa (Rp)</td>
                        <td style="font-weight: bold;">2P<br />(Tlp+Int)</td>
                        <td style="font-weight: bold;">Jasa (Rp)</td>
                        <td style="font-weight: bold;">2P<br />(Int+IPTV)</td>
                        <td style="font-weight: bold;">Jasa (Rp)</td>
                        <td style="font-weight: bold;">3P</td>
                        <td style="font-weight: bold;">Jasa (Rp)</td>
                        <td style="font-weight: bold;">WMS</td>
                        <td style="font-weight: bold;">Jasa (Rp)</td>
                        <td style="font-weight: bold;">WMSL</td>
                        <td style="font-weight: bold;">Jasa (Rp)</td>
                        <td style="font-weight: bold;">Addon<br />STB</td>
                        <td style="font-weight: bold;">Jasa (Rp)</td>
                        <td style="font-weight: bold;">Second<br />STB</td>
                        <td style="font-weight: bold;">Jasa (Rp)</td>
                        <td style="font-weight: bold;">ONT<br />Premium</td>
                        <td style="font-weight: bold;">Jasa (Rp)</td>
                        <td style="font-weight: bold;">ONT<br />Premium<br />(DNS)</td>
                        <td style="font-weight: bold;">Migrasi<br />STB<br />(DNS)</td>
                        <td style="font-weight: bold;">WiFiExt</td>
                        <td style="font-weight: bold;">Jasa (Rp)</td>
                        <td style="font-weight: bold;">MeshWiFi</td>
                        <td style="font-weight: bold;">Jasa (Rp)</td>
                        <td style="font-weight: bold;">PLC</td>
                        <td style="font-weight: bold;">Jasa (Rp)</td>
                        <td style="font-weight: bold;">IndiHome<br />Smart</td>
                        <td style="font-weight: bold;">Jasa (Rp)</td>
                        <td style="font-weight: bold;">Insert<br />Tiang</td>
                        <td style="font-weight: bold;">Jasa (Rp)</td>
                        <td style="font-weight: bold;">SSL</td>
                        <td style="font-weight: bold;">Jasa (Rp)</td>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                        <td style="font-weight: bold;" colspan="3">TOTAL</td>
                        <td style="font-weight: bold;">1P</td>
                        <td style="font-weight: bold;">Jasa (Rp)</td>
                        <td style="font-weight: bold;">2P (Tlp+Int)</td>
                        <td style="font-weight: bold;">Jasa (Rp)</td>
                        <td style="font-weight: bold;">2P (Int+IPTV)</td>
                        <td style="font-weight: bold;">Jasa (Rp)</td>
                        <td style="font-weight: bold;">3P</td>
                        <td style="font-weight: bold;">Jasa (Rp)</td>
                        <td style="font-weight: bold;">1P</td>
                        <td style="font-weight: bold;">Jasa (Rp)</td>
                        <td style="font-weight: bold;">2P (Tlp+Int)</td>
                        <td style="font-weight: bold;">Jasa (Rp)</td>
                        <td style="font-weight: bold;">2P (Int+IPTV)</td>
                        <td style="font-weight: bold;">Jasa (Rp)</td>
                        <td style="font-weight: bold;">3P</td>
                        <td style="font-weight: bold;">Jasa (Rp)</td>
                        <td style="font-weight: bold;">WMS</td>
                        <td style="font-weight: bold;">Jasa (Rp)</td>
                        <td style="font-weight: bold;">WMSL</td>
                        <td style="font-weight: bold;">Jasa (Rp)</td>
                        <td style="font-weight: bold;">Addon<br />STB</td>
                        <td style="font-weight: bold;">Jasa (Rp)</td>
                        <td style="font-weight: bold;">Second<br />STB</td>
                        <td style="font-weight: bold;">Jasa (Rp)</td>
                        <td style="font-weight: bold;">ONT<br />Premium</td>
                        <td style="font-weight: bold;">Jasa (Rp)</td>
                        <td style="font-weight: bold;">ONT<br />Premium<br />(DNS)</td>
                        <td style="font-weight: bold;">Migrasi<br />STB<br />(DNS)</td>
                        <td style="font-weight: bold;">WiFiExt</td>
                        <td style="font-weight: bold;">Jasa (Rp)</td>
                        <td style="font-weight: bold;">MeshWiFi</td>
                        <td style="font-weight: bold;">Jasa (Rp)</td>
                        <td style="font-weight: bold;">PLC</td>
                        <td style="font-weight: bold;">Jasa (Rp)</td>
                        <td style="font-weight: bold;">IndiHomeSmart</td>
                        <td style="font-weight: bold;">Jasa (Rp)</td>
                        <td style="font-weight: bold;">Insert Tiang</td>
                        <td style="font-weight: bold;">Jasa (Rp)</td>
                        <td style="font-weight: bold;">SSL</td>
                        <td style="font-weight: bold;">Jasa (Rp)</td>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
          </div>

          @if ($witel == 'BANJARMASIN')
          <div class="col-md-12">
            <div class="card shadow mb-4">
              <div class="card-body table-responsive">
                <table class="table table-hover table-striped table-bordered table-sm datatables" id="dashboardrekon">
                  <thead>
                    <tr>
                      <th style="color: #6c757d;" rowspan="3">MITRA</th>
                      <th style="color: #6c757d;" colspan="9">DATA SIAP REKON</th>
                    </tr>
                    <tr>
                      <td style="font-weight: bold;" colspan="3">STARCLICK NCX</td>
                      <td style="font-weight: bold;" colspan="5">K-PRO</td>
                      <th style="font-weight: bold; color: #6c757d;" rowspan="2">INSERT<br />TIANG</th>
                    </tr>
                    <tr>
                      <td style="font-weight: bold;">NOK</td>
                      <td style="font-weight: bold;">OK</td>
                      <td style="font-weight: bold;">JML</td>
                      <td style="font-weight: bold;">NOK</td>
                      <td style="font-weight: bold;">OK</td>
                      <td style="font-weight: bold;">JML</td>
                      <td style="font-weight: bold;">WMS</td>
                      <td class="bg-red">UT</td>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <td style="font-weight: bold;">TOTAL</td>
                      <td style="font-weight: bold;">NOK</td>
                      <td style="font-weight: bold;">OK</td>
                      <td style="font-weight: bold;">JML</td>
                      <td style="font-weight: bold;">NOK</td>
                      <td style="font-weight: bold;">OK</td>
                      <td style="font-weight: bold;">JML</td>
                      <td style="font-weight: bold;">WMS</td>
                      <td class="bg-red">UT</td>
                      <td style="font-weight: bold;">TOTAL</td>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
          </div>
          @endif

			  </div>
		</div>
	</div>
</div>
@endsection
@section('footerS')
<script src="/js/jquery.dataTables.min.js"></script>
<script src="/js/dataTables.bootstrap4.min.js"></script>
<script src="/js/moment.min.js"></script>
<script src="/js/daterangepicker.js"></script>
<script src="/js/select2.min.js"></script>
<script src="/js/jquery.timepicker.js"></script>

<script type = "text/javascript">
  $(document).ready(function () {

      $('.drgpicker').daterangepicker(
        {
          singleDatePicker: true,
          timePicker: false,
          showDropdowns: true,
          locale:
          {
            format: 'YYYY-MM-DD'
          }
      });

      var witel = {!! json_encode($witel) !!}
      var startDate = {!! json_encode($startDate) !!}
      var endDate = {!! json_encode($endDate) !!}

      $('#rekonKproMonth').DataTable({
        lengthChange: false,
        searching: false,
        paging: false,
        info: false,
        autoWidth: true,
        ordering: false,
        ajax: `/get_ajx/rekonProvisioningKPRO?view=MONTH&witel=${witel}`
      });

      $('#rekonKproDay').DataTable({
        lengthChange: false,
        searching: false,
        paging: false,
        info: false,
        autoWidth: true,
        ordering: false,
        ajax: `/get_ajx/rekonProvisioningKPRO?view=DAY&witel=${witel}`
      });

      $('#rekonKproOrder').DataTable({
        lengthChange: false,
        searching: false,
        paging: false,
        info: false,
        autoWidth: true,
        ordering: false,
        ajax: `/get_ajx/rekonProvisioningKPRO?view=ORDER&witel=${witel}&startDate=${startDate}&endDate=${endDate}`,
        'footerCallback': function( tfoot, data, start, end, display ) {
          var response = this.api().ajax.json();
          if(response){
            var $td = $(tfoot).find('td');
            $.each(response.footer, function(k, v){
              var kk = k + 1;
              $td.eq(kk).html(v);
            })
          }
        }
      });

      $('#dashboardrekon').DataTable({
        ajax: `/get_ajx/rekonDashboard?startDate=${startDate}&endDate=${endDate}`,
        'footerCallback': function( tfoot, data, start, end, display ) {
          var response = this.api().ajax.json();
          if(response){
            var $td = $(tfoot).find('td');
            $.each(response.footer, function(k, v){
              var kk = k + 1;
              $td.eq(kk).html(v);
            })
          }
        }
      });

      $('.periode_kegiatan').val(null).change();

      $('.periode_kegiatan').select2({
          placeholder: "Pilih Periode Kegiatan",
          width: '100%'
      });

      var periode = {!! json_encode($bulan_format) !!}

      $('.periode_kegiatan').on( 'select2 change', function(){
            var perd = this.value,
            d = new Date(),
            years = d.getFullYear()
            $.each(periode, function(key, index) {
                if(index.id == perd ) {
                    if(perd == '01') {
                        year = d.getFullYear() - 1
                    } else {
                        year = d.getFullYear()
                    }
                    $('.date_periode1').val(year + '-' + index.date_periode1)
                    $('.date_periode2').val(years + '-' + index.date_periode2)
                }
            })
      });

      $('.mitra').val(null).change();

      $('.mitra').select2({
          placeholder: "Pilih Mitra",
          width: '100%'
      });

      $('.layanan').val(null).change();

      $('.layanan').select2({
          placeholder: "Pilih Layanan",
          // multiple: true,
          width: '100%'
      });

  });
</script>
@endsection