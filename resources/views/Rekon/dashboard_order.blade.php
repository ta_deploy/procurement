@extends('layout')
@section('title', 'Dashboard Rekon PS Mitra')
@section('headerS')
<style>
	th, td {
		text-align: center;
		vertical-align: middle;
	}
  .table thead th {
    text-align: center;
    vertical-align: middle;
    font-weight: bold;
  }
  .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
    padding: 2px 1px;
  }
</style>
<link rel="stylesheet" href="/css/daterangepicker.css">
<link rel="stylesheet" href="/css/dataTables.bootstrap4.css">
<link rel="stylesheet" href="/css/jquery.steps.css">
<link rel="stylesheet" href="/css/dropzone.min.css">
<link rel="stylesheet" href="/css/quill.snow.css">
@endsection
@section('content')
<div class="container-fluid">
  @if (Session::has('alerts'))
    @foreach(Session::get('alerts') as $alert)
      <div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
    @endforeach
  @endif
	<div class="row justify-content-center">
		<div class="col-12">
      <h2 class="page-title">Dashboard Rekon PS Mitra</h2>
			  <div class="row">

          <div class="col-md-12">
            <div class="card shadow mb-4">
              <div class="card-body">
                <form method="GET">
                  <div class="form-row">
                    <div class="form-group col-md-2">
                        <label for="regional">REGIONAL</label>
                        <select class="form-control select2" name="regional" id="regional">
                        </select>
                    </div>
                    <div class="form-group col-md-2">
                        <label for="witel">WITEL</label>
                        <select class="form-control select2" name="witel" id="witel">
                        </select>
                    </div>
                    <div class="form-group col-md-2">
                        <label for="mitra">MITRA</label>
                        <select class="form-control select2" name="mitra" id="mitra">
                        </select>
                    </div>
                    <div class="form-group col-md-2">
                        <label for="startDate">START DATE</label>
                        <div class="input-group">
                          <input type="text" class="form-control drgpicker" id="startDate" name="startDate" value="{{ $startDate ? : date('Y-m-d') }}" aria-describedby="button-addon2" readonly>
                          <div class="input-group-append">
                            <div class="input-group-text" id="button-addon-date"><span class="fe fe-calendar fe-16"></span></div>
                          </div>
                        </div>
                    </div>
                    <div class="form-group col-md-2">
                        <label for="endDate">END DATE</label>
                        <div class="input-group">
                          <input type="text" class="form-control drgpicker" id="endDate" name="endDate" value="{{ $endDate ? : date('Y-m-d') }}" aria-describedby="button-addon2" readonly>
                          <div class="input-group-append">
                            <div class="input-group-text" id="button-addon-date"><span class="fe fe-calendar fe-16"></span></div>
                          </div>
                        </div>
                    </div>
                    <div class="form-group col-md-2">
                        <label for="search">&nbsp;</label>
                        <button type="submit" class="btn btn-primary btn-block">Search</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>

          <div class="col-md-12">
            <a class="btn mb-2 btn-primary" href="/Rekon/create/provisioning/order_based" target="_blank"><i class="fe fe-edit"></i> Create Rekon</a>
          </div>

          <div class="col-md-12">
            <div class="card shadow mb-4">
              <div class="card-body table-responsive">
                <h5 class="card-title">PERIODE ORDER REGIONAL {{ str_replace('REG', '', $regional) }} WITEL {{ $witel }} MITRA {{ $mitra }} PERIODE {{ $startDate }} S/D {{ $endDate }}</h5>
                <table class="table table-striped table-bordered" id="dashboardrekon">
                  <thead>
                    <tr>
                        <th style="color: #6c757d;" rowspan="2">REG</th>
                        <th style="color: #6c757d;" rowspan="2">WITEL</th>
                        <th style="color: #6c757d;" rowspan="2">MITRA</th>
                        <th style="color: #6c757d;" colspan="4">PSB</th>
                        <th style="color: #6c757d;" colspan="4">PSB + SURVEY</th>
                        <th style="color: #6c757d;" colspan="3">MIGRASI SERVICE</th>
                        <th style="color: #6c757d;" colspan="2">ADDON</th>
                        <th style="color: #6c757d;" colspan="2">TOTAL REKON</th>
                        <th style="color: #6c757d;" rowspan="2">ACTION</th>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">1P</td>
                        <td style="font-weight: bold;">2P<br />(Tlp+Int)</td>
                        <td style="font-weight: bold;">2P<br />(Int+IPTV)</td>
                        <td style="font-weight: bold;">3P</td>
                        <td style="font-weight: bold;">1P</td>
                        <td style="font-weight: bold;">2P<br />(Tlp+Int)</td>
                        <td style="font-weight: bold;">2P<br />(Int+IPTV)</td>
                        <td style="font-weight: bold;">3P</td>
                        <td style="font-weight: bold;">1P2P</td>
                        <td style="font-weight: bold;">1P3P</td>
                        <td style="font-weight: bold;">2P3P</td>
                        <td style="font-weight: bold;">NOK</td>
                        <td style="font-weight: bold;">OK</td>
                        <td style="font-weight: bold;">BELUM</td>
                        <td style="font-weight: bold;">SUDAH</td>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                        <td colspan="3">TOTAL</td>
                        <td style="font-weight: bold;">1P</td>
                        <td style="font-weight: bold;">2P (Tlp+Int)</td>
                        <td style="font-weight: bold;">2P (Int+IPTV)</td>
                        <td style="font-weight: bold;">3P</td>
                        <td style="font-weight: bold;">1P</td>
                        <td style="font-weight: bold;">2P (Tlp+Int)</td>
                        <td style="font-weight: bold;">2P (Int+IPTV)</td>
                        <td style="font-weight: bold;">3P</td>
                        <td style="font-weight: bold;">1P2P</td>
                        <td style="font-weight: bold;">1P3P</td>
                        <td style="font-weight: bold;">2P3P</td>
                        <td style="font-weight: bold;">NOK</td>
                        <td style="font-weight: bold;">OK</td>
                        <td style="font-weight: bold;">BELUM</td>
                        <td style="font-weight: bold;">SUDAH</td>
                        <td>TOOLS</td>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
          </div>

			  </div>
		</div>
	</div>
</div>
@endsection
@section('footerS')
<script src="/js/jquery.dataTables.min.js"></script>
<script src="/js/dataTables.bootstrap4.min.js"></script>
<script src="/js/moment.min.js"></script>
<script src="/js/daterangepicker.js"></script>
<script src="/js/select2.min.js"></script>
<script src="/js/jquery.timepicker.js"></script>

<script type = "text/javascript">
  $(document).ready(function () {

    $('.drgpicker').daterangepicker(
    {
        singleDatePicker: true,
        timePicker: false,
        showDropdowns: true,
        locale:
        {
            format: 'YYYY-MM-DD'
        }
    });

    var regional = {!! json_encode($regional) !!}
    var witel = {!! json_encode($witel) !!}
    var mitra = {!! json_encode($mitra) !!}
    var startDate = {!! json_encode($startDate) !!}
    var endDate = {!! json_encode($endDate) !!}
    var get_regional = {!! json_encode($get_regional) !!};
    var load_data = {!! json_encode($load_data) !!};

    var regional_only = [];

    $.each(get_regional, function(k, v) {
      regional_only[v.reg] = v.reg
    });

    var convert_reg_object = Object.assign({}, regional_only);
    var object_array_reg = Object.values(convert_reg_object);

    var renew_reg = ['ALL'];

    renew_reg.push(...object_array_reg);

    $('#regional').select2({
      placeholder: 'Silahkan Pilih Regional',
      data: renew_reg
    });

    $('#regional').on('select2:select change', function(){
    var isi = $(this).val();
      if(isi !=  null)
      {
          var data = $.grep(get_regional, function(e){ return e.reg == isi});
          var witel_only = [];
          var renew_witel = ['ALL'];

          $.each(data, function(k, v){
              witel_only[v.witel] = v.witel
          });

          var convert_witel_obj = Object.assign({}, witel_only);
          var object_array_witel = Object.values(convert_witel_obj);

          renew_witel.push(...object_array_witel);

          $('#witel').empty().trigger('change');

          $('#witel').select2({
              placeholder: 'Silahkan Pilih Witel',
              data: renew_witel
          });

          var mitra_only = [];
          var renew_mitra = ['ALL'];

          $.each(data, function(k, v){
              mitra_only[v.mitra] = v.mitra
          });

          var convert_mitra_obj = Object.assign({}, mitra_only);
          var object_array_mitra = Object.values(convert_mitra_obj);

          renew_mitra.push(...object_array_mitra);

          $('#mitra').empty().trigger('change');

          $('#mitra').select2({
              placeholder: 'Silahkan Pilih mitra',
              data: renew_mitra
          });
      }
    })

    $('#regional').val(null).change();

    console.log(regional,witel,mitra,startDate,endDate)

    var table = $('#dashboardrekon').DataTable({
      // lengthChange: false,
      // paging: false,
      ordering: false,
      // columnDefs: [
			// 	{
			// 		targets: 'no-sort',
			// 		orderable: false
			// 	}
			// ],
      info: false,
      autoWidth: true,
      ajax: `/get_ajx/rekonDashboardOrder?regional=${regional}&witel=${witel}&mitra=${mitra}&startDate=${startDate}&endDate=${endDate}`,
      'footerCallback': function( tfoot, data, start, end, display ) {
        var response = this.api().ajax.json();
        if(response)
        {
          // console.log(response.data[0], response)
          var $td = $(tfoot).find('td');
          $.each(response.footer, function(k, v){
              var kk = k + 1;
              $td.eq(kk).html(v);
          })
        }
      }
    })

    if(load_data.length != 0)
    {
        $('#regional').val(load_data.regional).change();
        $('#witel').val(load_data.witel).change();
        $('#periode').val(load_data.periode).change();
    }
  });
</script>
@endsection