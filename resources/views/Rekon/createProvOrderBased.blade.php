@extends('layout')
@section('title', 'Create Rekon Provisioning')
@section('headerS')
<link rel="stylesheet" href="/css/daterangepicker.css">
<link rel="stylesheet" href="/css/dataTables.bootstrap4.css">
<link rel="stylesheet" href="/css/jquery.steps.css">
<link rel="stylesheet" href="/css/dropzone.min.css">
<link rel="stylesheet" href="/css/quill.snow.css">
<style>
  textarea {
    width: 100%;
    padding: 1em;
    text-align: left;
    border: 1px solid #000;
    resize: none;
    box-sizing: border-box;
  }
  table td input {
    width: 100%;
    text-align: center;
    vertical-align: middle;
  }
</style>
@endsection
@section('content')
<div class="container-fluid">
  @if (Session::has('alerts'))
    @foreach(Session::get('alerts') as $alert)
      <div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
    @endforeach
  @endif
  <div class="card my-4">
    <div class="card-header">
      <strong>Pekerjaan Pasang Sambungan Baru (PSB) - Skema Order Based</strong>
    </div>
    <div class="card-body">
      <form id="form-create" method="post" action="/Admin/save/filepsb" enctype="multipart/form-data" autocomplete="off">
        {{ csrf_field() }}
        <input type="hidden" name="id_file" value="doc_generate_new">
        <input type="hidden" name="flag_dok_psb" value="beta_ho_v2_manual">

        <div class="col-md-12">

          <h3>Periode</h3>
          <section class="form-row" id="section_periode">
              <div class="form-group col-md-1">
                <label class="col-form-label pull-right" for="regional">Regional</label>
                <select class="form-control select2" name="regional" id="regional">
                </select>
              </div>
              <div class="form-group col-md-3">
                <label class="col-form-label pull-right" for="witel">Witel</label>
                  <select class="form-control select2" name="witel" id="witel">
                  </select>
              </div>
              <div class="form-group col-md-3">
                <label class="col-form-label pull-right" for="mitra">Mitra</label>
                  <select class="form-control select2" name="id_mitra" id="mitra">
                  </select>
              </div>
              <div class="form-group col-md-1">
                  <label class="col-form-label pull-right" for="ppn_numb">PPN (%)</label>
                  <input type="text" class="form-control input-transparent required" name="ppn_numb" value="11" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" minlength="2" maxlength="2">
              </div>
              <div class="form-group col-md-2">
                  <label class="col-form-label pull-right" for="masa_berlaku_pks">Masa Berlaku PKS</label>
                  <input type="text" class="form-control tgl-picker text-center required" id="masa-berlaku-pks" name="masa_berlaku_pks" value="{{ date('Y-m-d') }}" readonly>
                  <span class="help-block"><small>* Amandemen Terakhir</small></span>
              </div>
              <div class="form-group col-md-2">
                  <label class="col-form-label pull-right" for="tanggal_terbit">Tanggal Terbit</label>
                  <input type="text" class="form-control text-center tgl-picker required" name="tanggal_terbit" value="{{ date('Y-m-d') }}" readonly>
              </div>
              <div class="form-group col-md-3">
                  <label class="col-form-label" for="toc">TOC</label>
                  <input type="text" id="toc" name="toc" class="form-control input-transparent required" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" autocomplete="off">
                  <span id="durasi"></span>
              </div>
              <div class="form-group col-md-3">
                <label class="col-form-label pull-right" for="periode_kegiatan">Periode Kegiatan</label>
                  <select class="text-center required" id="periode-kegiatan" name="periode_kegiatan">
                  @foreach ($bulan_format as $bf)
                  <option class="tex-center" value="{{ $bf->id }}">{{ $bf->text }}</option>                    
                  @endforeach
                  </select>
              </div>
              <div class="form-group col-md-3">
                <label class="col-form-label pull-right" for="date_periode1">Pelaksanaan Awal</label>
                  <input type="text" class="form-control input-transparent text-center" id="date-periode1" name="rekon_periode1" readonly>
              </div>
              <div class="form-group col-md-3">
                <label class="col-form-label pull-right" for="date_periode2">Pelaksanaan Akhir</label>
                  <input type="text" class="form-control input-transparent text-center" id="date-periode2" name="rekon_periode2" readonly>
              </div>
              <div class="form-group col-md-4">
                  <label class="col-form-label pull-right" for="tgl_sp">Tanggal Surat Pesanan</label>
                  <input type="text" class="form-control tgl-picker text-center required" id="tgl-sp" name="tgl_sp" value="{{ date('Y-m-d') }}" readonly>
              </div>
              <div class="form-group col-md-4">
                  <label class="col-form-label pull-right" for="no_sp">Nomor Surat Pesanan</label>
                  <input type="text" class="form-control input-transparent text-center required" name="no_sp">
              </div>
              <div class="form-group col-md-4">
                  <label class="col-form-label pull-right" for="id_project">ID Project</label>
                  <input type="text" class="form-control input-transparent text-center required" name="id_project" value="{{ $hss_witel->project_id }}">
              </div>
          </section>

          <h3>KPI 7 PSB</h3>
          <section class="form-row" id="section_kpi">
              <div class="form-group col-md-3">
                  <label class="col-form-label pull-right" for="kpi_real_pspi">KPI Real PS/PI (%)</label>
                  <input type="text" class="form-control input-transparent required" name="kpi_real_pspi" minlength="2" maxlength="5">
              </div>
              <div class="form-group col-md-3">
                  <label class="col-form-label pull-right" for="kpi_real_tticomply">KPI Real TTI Comply (%)</label>
                  <input type="text" class="form-control input-transparent required" name="kpi_real_tticomply" minlength="2" maxlength="5">
              </div>
              <div class="form-group col-md-3">
                  <label class="col-form-label pull-right" for="kpi_real_ffg">KPI Real FFG (%)</label>
                  <input type="text" class="form-control input-transparent required" name="kpi_real_ffg" minlength="2" maxlength="5">
              </div>
              <div class="form-group col-md-3">
                  <label class="col-form-label pull-right" for="kpi_real_validasicore">KPI Real Validasi Core (%)</label>
                  <input type="text" class="form-control input-transparent required" name="kpi_real_validasicore" minlength="2" maxlength="5">
              </div>
              <div class="form-group col-md-3">
                  <label class="col-form-label pull-right" for="kpi_real_ttrffg">KPI Real TTR FFG (%)</label>
                  <input type="text" class="form-control input-transparent required" name="kpi_real_ttrffg" minlength="2" maxlength="5">
              </div>
              <div class="form-group col-md-3">
                  <label class="col-form-label pull-right" for="kpi_real_ujipetik">KPI Real Uji Petik (%)</label>
                  <input type="text" class="form-control input-transparent required" name="kpi_real_ujipetik" minlength="2" maxlength="5">
              </div>
              <div class="form-group col-md-3">
                  <label class="col-form-label pull-right" for="kpi_real_qc2">KPI Real QC2 (%)</label>
                  <input type="text" class="form-control input-transparent required" name="kpi_real_qc2" minlength="2" maxlength="5">
              </div>
          </section>

          <h3>SSL</h3>
          <section class="form-row" id="section_ssl">
            <div class="table-responsive">
              <table class="table table-hover table-sm table-bordered tableJustif" style="text-align: center;">
                <thead class="table-primary">
                <tr>
                  <th style="color: black">TYPE TRANSAKSI</th>
                  <th style="color: black">JENIS LAYANAN</th>
                  <th style="color: black">HSS JASA + INSENTIF</th>
                  <th style="color: black;">JUMLAH SSL</th>
                </tr>
                </thead>
                <tbody>
                  <tr>
                    <td rowspan="5">Infrastructure New Sales Fiber<br />KU + SURVEY</td>
                  </tr>
                  <tr>
                    <td>0P - 1P (Tlp/Int)</td>
                    <td>
                      <input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->p1)) }}" name="p1_survey_hss" id="p1_survey_hss" readonly>
                    </td>
                    <td>
                      <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" name="p1_survey_ssl" id="p1_survey_ssl" required>
                    </td>
                  </tr>
                  <tr>
                    <td>0P - 2P (Tlp+Int)</td>
                    <td>
                      <input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->p2_inet_voice)) }}" name="p2_tlpint_survey_hss" id="p2_tlpint_survey_hss" readonly>
                    </td>
                    <td>
                      <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" name="p2_tlpint_survey_ssl" id="p2_tlpint_survey_ssl" required>
                    </td>
                  </tr>
                  <tr>
                    <td>0P - 2P (Int+IPTV)</td>
                    <td>
                      <input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->p2_inet_iptv)) }}" name="p2_intiptv_survey_hss" id="p2_intiptv_survey_hss" readonly>
                    </td>
                    <td>
                      <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" name="p2_intiptv_survey_ssl" id="p2_intiptv_survey_ssl" required>
                    </td>
                  </tr>
                  <tr>
                    <td>0P - 3P (Tlp+Int+IPTV)</td>
                    <td>
                      <input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->p3)) }}" name="p3_survey_hss" id="p3_survey_hss" readonly>
                    </td>
                    <td>
                      <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" name="p3_survey_ssl" id="p3_survey_ssl" required>
                    </td>
                  </tr>
                  <tr>
                    <td rowspan="5">Infrastructure New Sales Fiber<br />KU</td>
                  </tr>
                  <tr>
                    <td>0P - 1P (Tlp/Int)</td>
                    <td>
                      <input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->p1_ku)) }}" name="p1_hss" id="p1_hss" readonly>
                    </td>
                    <td>
                      <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" name="p1_ssl" id="p1_ssl" required>
                    </td>
                  </tr>
                  <tr>
                    <td>0P - 2P (Tlp+Int)</td>
                    <td>
                      <input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->p2_inet_voice_ku)) }}" name="p2_tlpint_hss" id="p2_tlpint_hss" readonly>
                    </td>
                    <td>
                      <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" name="p2_tlpint_ssl" id="p2_tlpint_ssl" required>
                    </td>
                  </tr>
                  <tr>
                    <td>0P - 2P (Int+IPTV)</td>
                    <td>
                      <input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->p2_inet_iptv_ku)) }}" name="p2_intiptv_hss" id="p2_intiptv_hss" readonly>
                    </td>
                    <td>
                      <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" name="p2_intiptv_ssl" id="p2_intiptv_ssl" required>
                    </td>
                  </tr>
                  <tr>
                    <td>0P - 3P (Tlp+Int+IPTV)</td>
                    <td>
                      <input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->p3_ku)) }}" name="p3_hss" id="p3_hss" readonly>
                    </td>
                    <td>
                      <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" name="p3_ssl" id="p3_ssl" required>
                    </td>
                  </tr>
                  <tr>
                    <td rowspan="4">MIGRASI</td>
                  </tr>
                  <tr>
                    <td>1P ke 2P</td>
                    <td>
                      <input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->migrasi_1p)) }}" name="migrasi_service_1p2p_hss" id="migrasi_service_1p2p_hss" readonly>
                    </td>
                    <td>
                      <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" name="migrasi_service_1p2p_ssl" id="migrasi_service_1p2p_ssl" required>
                    </td>
                  </tr>
                  <tr>
                    <td>1P ke 3P</td>
                    <td>
                      <input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->migrasi_2p)) }}" name="migrasi_service_1p3p_hss" id="migrasi_service_1p3p_hss" readonly>
                    </td>
                    <td>
                      <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" name="migrasi_service_1p3p_ssl" id="migrasi_service_1p3p_ssl" required>
                    </td>
                  </tr>
                  <tr>
                    <td>2P ke 3P</td>
                    <td>
                      <input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->migrasi_3p)) }}" name="migrasi_service_2p3p_hss" id="migrasi_service_2p3p_hss" readonly>
                    </td>
                    <td>
                      <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" name="migrasi_service_2p3p_ssl" id="migrasi_service_2p3p_ssl" required>
                    </td>
                  </tr>
                  <tr>
                    <td rowspan="12">ADDON</td>
                  </tr>
                  <tr>
                    <td>LME WiFi Provisioning Type-1 (PT1) KU</td>
                    <td>
                      <input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->lme_wifi)) }}" name="lme_wifi_pt1_hss" id="lme_wifi_pt1_hss" readonly>
                    </td>
                    <td>
                      <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" name="lme_wifi_pt1_ssl" id="lme_wifi_pt1_ssl" required>
                    </td>
                  </tr>
                  <tr>
                    <td>LME-AP-Indoor</td>
                    <td>
                      <input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->lme_ap_indoor)) }}" name="lme_ap_indoor_hss" id="lme_ap_indoor_hss" readonly>
                    </td>
                    <td>
                      <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" name="lme_ap_indoor_ssl" id="lme_ap_indoor_ssl" required>
                    </td>
                  </tr>
                  <tr>
                    <td>LME-AP-Outdoor</td>
                    <td>
                      <input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->lme_ap_outdoor)) }}" name="lme_ap_outdoor_hss" id="lme_ap_outdoor_hss" readonly>
                    </td>
                    <td>
                      <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" name="lme_ap_outdoor_ssl" id="lme_ap_outdoor_ssl" required>
                    </td>
                  </tr>
                  <tr>
                    <td>Smart Camera Only</td>
                    <td>
                      <input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->smartcam_only)) }}" name="indihome_smart_hss" id="indihome_smart_hss" readonly>
                    </td>
                    <td>
                      <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" name="indihome_smart_ssl" id="indihome_smart_ssl" required>
                    </td>
                  </tr>
                  <tr>
                    <td>Instalasi PLC</td>
                    <td>
                      <input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->plc)) }}" name="plc_hss" id="plc_hss" readonly>
                    </td>
                    <td>
                      <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" name="plc_ssl" id="plc_ssl" required>
                    </td>
                  </tr>
                  <tr>
                    <td>IKR Add-on STB Tambahan</td>
                    <td>
                      <input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->stb_tambahan)) }}" name="ikr_addon_stb_hss" id="ikr_addon_stb_hss" readonly>
                    </td>
                    <td>
                      <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" name="ikr_addon_stb_ssl" id="ikr_addon_stb_ssl" required>
                    </td>
                  </tr>
                  <tr>
                    <td>Penggantian ONT Premium</td>
                    <td>
                      <input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->ont_premium)) }}" name="ont_premium_hss" id="ont_premium_hss" readonly>
                    </td>
                    <td>
                      <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" name="ont_premium_ssl" id="ont_premium_ssl" required>
                    </td>
                  </tr>
                  <tr>
                    <td>Instalalasi Wifi Extender</td>
                    <td>
                      <input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->wifiext)) }}" name="wifiextender_hss" id="wifiextender_hss" readonly>
                    </td>
                    <td>
                      <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" name="wifiextender_ssl" id="wifiextender_ssl" required>
                    </td>
                  </tr>
                  <tr>
                    <td>Insert Tiang 7</td>
                    <td>
                      <input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->insert_tiang)) }}" name="pu_s7_140_hss" id="pu_s7_140_hss" readonly>
                    </td>
                    <td>
                      <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" name="pu_s7_140_ssl" id="pu_s7_140_ssl" required>
                    </td>
                  </tr>
                  <tr>
                    <td>Insert Tiang 9</td>
                    <td>
                      <input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->insert_tiang_9)) }}" name="pu_s9_140_hss" id="pu_s9_140_hss" readonly>
                    </td>
                    <td>
                      <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" name="pu_s9_140_ssl" id="pu_s9_140_ssl" required>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </section>

          <h3>Nomor Surat Dokumen</h3>
          <section class="form-row" id="section_no_surat">
              <div class="form-group col-md-4">
                  <label class="col-form-label pull-right" for="invoice">Nomor Invoice</label>
                  <input type="text" class="form-control input-transparent text-center required" name="invoice">
              </div>
              <div class="form-group col-md-4">
                  <label class="col-form-label pull-right" for="spp_num">Nomor SPP</label>
                  <input type="text" class="form-control input-transparent text-center required" name="spp_num">
              </div>
              <div class="form-group col-md-4">
                  <label class="col-form-label pull-right" for="receipt_num">Nomor Kwitansi</label>
                  <input type="text" class="form-control input-transparent text-center required" name="receipt_num">
              </div>
              <div class="form-group col-md-4">
                  <label class="col-form-label pull-right" for="faktur">Nomor Seri Faktur Pajak</label>
                  <input type="text" class="form-control input-transparent text-center required" name="faktur">
              </div>
              <div class="form-group col-md-4">
                <label class="col-form-label pull-right" for="surat_penetapan">Nomor Surat Penetapan</label>
                <input type="text" class="form-control input-transparent text-center required" name="surat_penetapan">
              </div>
              <div class="form-group col-md-4">
                <label class="col-form-label pull-right" for="surat_kesanggupan">Nomor Surat Kesanggupan</label>
                <input type="text" class="form-control input-transparent text-center required" name="surat_kesanggupan">
              </div>
              <div class="form-group col-md-4">
                  <label class="col-form-label pull-right" for="tgl_faktur">Tanggal Faktur</label>
                  <input type="text" class="form-control tgl-picker text-center required" id="tgl-faktur" name="tgl_faktur" value="{{ date('Y-m-d') }}">
              </div>
              <div class="form-group col-md-4">
                  <label class="col-form-label pull-right" for="tgl_s_pen">Tanggal Surat Penetapan</label>
                  <input type="text" class="form-control tgl-picker text-center required" id="tgl-s-pen" name="tgl_s_pen" value="{{ date('Y-m-d') }}">
              </div>
              <div class="form-group col-md-4">
                  <label class="col-form-label pull-right" for="tgl_surat_sanggup">Tanggal Surat Kesanggupan</label>
                  <input type="text" class="form-control tgl-picker text-center required" id="tgl-surat-sanggup" name="tgl_surat_sanggup" value="{{ date('Y-m-d') }}">
              </div>
              <div class="form-group col-md-4">
                <label class="col-form-label pull-right" for="no_baij">Nomor Surat BAR&IJ</label>
                <input type="text" class="form-control input-transparent text-center required" name="no_baij">
              </div>
              <div class="form-group col-md-4">
                  <label class="col-form-label pull-right" for="BAUT">Nomor Surat BAUT</label>
                  <input type="text" class="form-control input-transparent text-center required" name="BAUT">
              </div>
              <div class="form-group col-md-4">
                  <label class="col-form-label pull-right" for="BAST">Nomor Surat BAST</label>
                  <input type="text" class="form-control input-transparent text-center required" name="BAST">
              </div>
              <div class="form-group col-md-4">
                  <label class="col-form-label pull-right" for="no_bap">Nomor Surat BA Performansi</label>
                  <input type="text" class="form-control input-transparent text-center required" name="no_bap">
              </div>
              <div class="form-group col-md-4">
                  <label class="col-form-label pull-right" for="no_ba_pemeliharaan">Nomor BA Pemeliharaan</label>
                  <input type="text" class="form-control input-transparent text-center required" name="no_ba_pemeliharaan">
              </div>
          </section>

          <h3>Finish</h3>
          <section id="section_finish">
              <label for="acceptTerms" style="text-align: left"> <input id="acceptTerms" name="acceptTerms" type="checkbox" class="required" /> Saya sebagai perwakilan mitra mengkonfirmasi bahwa data yang dimasukan ke dalam aplikasi ini <strong>PROMISE</strong> benar dan dapat dipertanggungjawabkan kebenarannya.</label>
          </section>

        </div>
      </form>
    </div>
    <!-- .card-body -->
  </div>
</div>
@endsection
@section('footerS')
<script src="/js/jquery.dataTables.min.js"></script>
<script src="/js/dataTables.bootstrap4.min.js"></script>
<script src="/js/moment.min.js"></script>
<script src="/js/daterangepicker.js"></script>
<script src="/js/select2.min.js"></script>
<script src="/js/jquery.timepicker.js"></script>

<script type="text/javascript">
  $(document).ready(function () {
    var form = $("#form-create");
    form.length &&
      (form.children("div").steps({
          headerTag: "h3",
          bodyTag: "section",
          transitionEffect: "slideLeft",
          onStepChanging: function (e, a, o) {
            return (form.validate().settings.ignore = ":disabled,:hidden"), form.valid();
          },
          onFinishing: function (e, a) {
            return (form.validate().settings.ignore = ":disabled"), form.valid();
          },
          onFinished: function (e, a) {
            alert("Data rekon akan segera di proses ...");
            form.submit()
          },
      }));
      
      var get_regional = {!! json_encode($get_regional) !!}
      var regional_only = [];

      $.each(get_regional, function(k, v) {
        regional_only[v.id_reg] = v.reg
      });

      var convert_reg_object = Object.assign({}, regional_only);
      var object_array_reg = Object.values(convert_reg_object);

      var renew_reg = ['ALL'];

      renew_reg.push(...object_array_reg);

      $('#regional').select2({
        placeholder: 'Pilih Regional',
        data: renew_reg
      });

      $('#regional').on('select2:select change', function(){
        var isi = $(this).val();
        if(isi !=  null){
          var data = $.grep(get_regional, function(e){ return e.reg == isi});
          var witel_only = [];
          var renew_witel = ['ALL'];

          $.each(data, function(k, v){
              witel_only[v.witel] = v.witel
          });

          var convert_witel_obj = Object.assign({}, witel_only);
          var object_array_witel = Object.values(convert_witel_obj);

          renew_witel.push(...object_array_witel);

          $('#witel').empty().trigger('change');

          $('#witel').select2({
              placeholder: 'Pilih Witel',
              data: renew_witel
          });
        }
      })

      $('#witel').on('select2:select change', function(){
        var isi = $(this).val();

        if(isi !=  null){
          var data = $.grep(get_regional, function(e){ return e.witel == isi}),
          mitra_only = [];

          $.each(data, function(k, v){
            mitra_only.push({
              id: v.id_mitra,
              text: v.mitra
            });
          });

          $('#mitra').empty().trigger('change');

          $('#mitra').select2({
              placeholder: 'Pilih mitra',
              data: mitra_only
          });
        }
      })

      $('#regional').val(null).change();

      $(".tgl-picker").daterangepicker(
        {
            singleDatePicker: true,
            timePicker: false,
            showDropdowns: true,
            locale:
            {
                format: "YYYY-MM-DD",
            }
        }
      );

      $("#periode-kegiatan").val(null).change();

      $("#periode-kegiatan").select2({
          placeholder: "Pilih Periode Kegiatan",
          width: "100%"
      });

      var periode = {!! json_encode($bulan_format) !!}

      $("#periode-kegiatan").on( "select2 change", function(){
          var perd = this.value,
          d = new Date(),
          years = d.getFullYear()
          $.each(periode, function(key, index) {
              if(index.id == perd ) {
                  if(perd == "01") {
                      year = d.getFullYear() - 1
                  } else {
                      year = d.getFullYear()
                  }
                  $("#date-periode1").val(year + "-" + index.date_periode1)
                  $("#date-periode2").val(years + "-" + index.date_periode2)
              }
          })
      })

      $("#tgl-sp").on("change", function(e) {

        var kalkulasi_date = new Date(new Date($("#tgl-sp").val()).setDate(new Date($("#tgl-sp").val()).getDate() + parseInt($("#toc").val().length != 0 ? parseInt($("#toc").val() ) - 1 : 0))).toISOString().slice(0, 10)
        $("#durasi").html("<code>*Tanggal TOC Adalah "+kalkulasi_date+"</code>");
      });

      $("#toc").on("keypress keyup", function(e) {
          var isi = $(this).val()
          if (/\D/g.test(isi))
          {
            isi = isi.replace(/\D/g, "");
          }

          var kalkulasi_date = new Date(new Date($("#tgl-sp").val()).setDate(new Date($("#tgl-sp").val()).getDate() + parseInt(isi.length != 0 ? parseInt(isi )  - 1 : 0))).toISOString().slice(0, 10)
          $("#durasi").html("<code>*Tanggal TOC Adalah "+kalkulasi_date+"</code>");
      });

      if (/\D/g.test($("#toc").val()))
      {
        $("#toc").val() = $("#toc").val().replace(/\D/g, "");
      }
  });
  </script>
@endsection