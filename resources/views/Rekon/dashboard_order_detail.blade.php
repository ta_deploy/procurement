@extends('layout')
@section('title', 'Rekon Material Provisioning Mitra')
@section('headerS')
<style>
	th, td {
		text-align: center;
		vertical-align: middle;
	}

    .table thead th {
        text-align: center;
		vertical-align: middle;
        color: #6c757d;
    }

    th {
        color: #6c757d;
    }
    
    .no-wrap {
        white-space: nowrap;
    }
</style>
<link href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="https://cdn.datatables.net/buttons/2.0.1/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
@endsection
@section('content')
@if (Session::has('alerts'))
  @foreach(Session::get('alerts') as $alert)
    <div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
  @endforeach
@endif
<div class="container-fluid">
    <div class="row justify-content-center">
    <div class="col-12">
        <h2 class="mb-2 page-title">Rekon Provisioning Mitra {{ $mitra }}</h2>
        <p class="card-text">Regional {{ $regional }} Witel {{ $witel }}</b></p>
        <div class="row my-4">
        <div class="col-md-12">
            <div class="card shadow">
            <div class="card-body table-responsive">
                <table class="table datatables no-wrap display" id="data_table">
                    <thead>
                        <tr>
                            <th>NO</th>
                            <th>NIK_TEKNISI1</th>
                            <th>NIK_TEKNISI2</th>
                            <th>LAYANAN</th>
                            <th>REGIONAL</th>
                            <th>WITEL</th>
                            <th>MITRA</th>
                            <th>DATEL</th>
                            <th>STO</th>
                            <th>ORDER_ID</th>
                            <th>PACKAGE_NAME</th>
                            <th>FLAG_DEPOSIT</th>
                            <th>TYPE_TRANSAKSI</th>
                            <th>TYPE_LAYANAN</th>
                            <th>NCLI</th>
                            <th>POTS</th>
                            <th>SPEEDY</th>
                            <th>STATUS_RESUME</th>
                            <th>STATUS_MESSAGE</th>
                            <th>ORDER_DATE</th>
                            <th>LAST_UPDATED_DATE</th>
                            <th>CUSTOMER_NAME</th>
                            <th>NO_HP</th>
                            <th>INSTALL_ADDRESS</th>
                            <th>K-CONTACT</th>
                            <th>GPS_LONGITUDE</th>
                            <th>GPS_LATITUDE</th>
                            <th>STATUS_ONLINE</th>
                            <th>SERIAL_NUMBER_ONT</th>
                            <th>SERIAL_NUMBER_STB</th>
                            <th>SERIAL_NUMBER_OTHER</th>
                            <th>DROP_CORE</th>
                            <th>PRECON</th>
                            <th>TIANG</th>
                            <th>KLEM RING</th>
                            <th>OTP</th>
                            <th>PREKSO</th>
                            <th>S-CLAMP</th>
                            <th>SOC</th>
                            <th>CLAMP_HOOK</th>
                            <th>TRAY_CABLE</th>
                        </tr>
                    </thead>
                </table>
            </div>
            </div>
        </div>
        </div>
    </div>
    </div>
</div>
@endsection
@section('footerS')
<script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.0.1/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.print.min.js"></script>
<script type = "text/javascript">
    $(document).ready(function () {
        var regional = {!! json_encode($regional) !!}
        var witel = {!! json_encode($witel) !!}
        var mitra = {!! json_encode($mitra) !!}
        var startDate = {!! json_encode($startDate) !!}
        var endDate = {!! json_encode($endDate) !!}
        var layanan = {!! json_encode($layanan) !!}

        // console.log(regional, witel, mitra, startDate, endDate, layanan)
        
        $('#data_table').DataTable({
                ajax: `/get_ajx/rekonDashboardOrderDetail?regional=${regional}&witel=${witel}&mitra=${mitra}&startDate=${startDate}&endDate=${endDate}&layanan=${layanan}`,
                select: true,
                dom: 'Blfrtip',
                lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
                buttons: [
                    {
                        extend: 'excel',
                        text: 'Export to Excel',
                        title: 'REKON MATERIAL PROVISIONING MITRA - PROMISE'
                    }
                ]
        });
    });
</script>
@endsection