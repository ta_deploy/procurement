@extends('layout')
@section('title', 'Rekon Material Provisioning Mitra')
@section('headerS')
<style>
	th, td {
		text-align: center;
		vertical-align: middle;
	}

    .table thead th {
        text-align: center;
		vertical-align: middle;
        color: #6c757d;
    }

    th {
        color: #6c757d;
    }
    
    .no-wrap {
        white-space: nowrap;
    }
</style>
<link href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="https://cdn.datatables.net/buttons/2.0.1/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
@endsection
@section('content')
@if (Session::has('alerts'))
  @foreach(Session::get('alerts') as $alert)
    <div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
  @endforeach
@endif
<div class="container-fluid">
    <div class="row justify-content-center">
    <div class="col-12">
        <h2 class="mb-2 page-title">Rekon Provisioning Witel {{ $witel }} Mitra {{ $area }}</h2>
        <p class="card-text">Layanan {{ $layanan }} Periode {{ $startDate }} s/d {{ $endDate }}</b></p>
        <div class="row my-4">
            <div class="col-md-12">
                <div class="card shadow">
                    <div class="card-body table-responsive">
                        <table class="table datatables no-wrap display" id="data_table">
                            <thead>
                                <tr>
                                    <th>NO</th>
                                    <th>AREA</th>
                                    <th>LAYANAN</th>
                                    <th>ORDER_ID</th>
                                    <th>REGIONAL</th>
                                    <th>WITEL</th>
                                    <th>DATEL</th>
                                    <th>STO</th>
                                    <th>EXTERN_ORDER_ID</th>
                                    <th>JENIS_PSB</th>
                                    <th>TYPE_TRANSAKSI</th>
                                    <th>FLAG_DEPOSIT</th>
                                    <th>STATUS_RESUME</th>
                                    <th>STATUS_MESSAGE</th>
                                    <th>KCONTACT</th>
                                    <th>ORDER_DATE</th>
                                    <th>NCLI</th>
                                    <th>NDEM</th>
                                    <th>SPEEDY</th>
                                    <th>POTS</th>
                                    <th>CUSTOMER_NAME</th>
                                    <th>NO_HP</th>
                                    <th>EMAIL</th>
                                    <th>INSTALL_ADDRESS</th>
                                    <th>CUSTOMER_ADDRESS</th>
                                    <th>CITY_NAME</th>
                                    <th>GPS_LATITUDE</th>
                                    <th>GPS_LONGITUDE</th>
                                    <th>PACKAGE_NAME</th>
                                    <th>LOC_ID</th>
                                    <th>DEVICE_ID</th>
                                    <th>AGENT_ID</th>
                                    <th>WFM_ID</th>
                                    <th>SCHEDSTART</th>
                                    <th>SCHEDFINISH</th>
                                    <th>ACTSTART</th>
                                    <th>ACTFINISH</th>
                                    <th>SCHEDULE_LABOR</th>
                                    <th>FINISH_LABOR</th>
                                    <th>LAST_UPDATED_DATE</th>
                                    <th>TYPE_LAYANAN</th>
                                    <th>ISI_COMMENT</th>
                                    <th>TINDAK_LANJUT</th>
                                    <th>USER_ID_TL</th>
                                    <th>TL_DATE</th>
                                    <th>TANGGAL_PROSES</th>
                                    <th>TANGGAL_MANJA</th>
                                    <th>HIDE</th>
                                    <th>CATEGORY</th>
                                    <th>PROVIDER</th>
                                    <th>NPER</th>
                                    <th>AMCREW</th>
                                    <th>STATUS_WO</th>
                                    <th>STATUS_TASK</th>
                                    <th>CHANNEL</th>
                                    <th>GROUP_CHANNEL</th>
                                    <th>PRODUCT</th>
                                    <th>JENIS_ORDER</th>
                                    <th>DEVICE_ID_INT</th>
                                    <th>DROP_CORE</th>
                                    <th>PRECON</th>
                                    <th>TIANG</th>
                                    <th>KLEM_RING</th>
                                    <th>OTP</th>
                                    <th>PREKSO</th>
                                    <th>ROSET</th>
                                    <th>S_CLAMP</th>
                                    <th>SOC</th>
                                    <th>CLAMP_HOOK</th>
                                    <th>SEGITIGA_BRACKET</th>
                                    <th>TRAY_CABLE</th>
                                    <th>STATUS UTONLINE</th>
                                    <th>TGL CREATE BA</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</div>
@endsection
@section('footerS')
<script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.0.1/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.print.min.js"></script>
<script type = "text/javascript">
    $(document).ready(function () {
        var view = {!! json_encode($view) !!}
        var witel = {!! json_encode($witel) !!}
        var area = {!! json_encode($area) !!}
        var layanan = {!! json_encode($layanan) !!}
        var startDate = {!! json_encode($startDate) !!}
        var endDate = {!! json_encode($endDate) !!}
        var nper = {!! json_encode($nper) !!}

        // console.log(regional, witel, mitra, startDate, endDate, layanan)
        
        $('#data_table').DataTable({
                ajax: `/get_ajx/rekonOrderKproDetail?view=${view}&witel=${witel}&area=${area}&layanan=${layanan}&startDate=${startDate}&endDate=${endDate}&nper=${nper}`,
                select: true,
                dom: 'Blfrtip',
                lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
                buttons: [
                    {
                        extend: 'excel',
                        text: 'Export to Excel',
                        title: 'REKON MATERIAL PROVISIONING MITRA - PROMISE'
                    }
                ]
        });
    });
</script>
@endsection