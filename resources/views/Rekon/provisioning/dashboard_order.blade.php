@extends('layout')
@section('title', 'Dashboard Rekon PS')
@section('headerS')
<style>
	tr, th, td {
        text-align: center;
    }
    .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
      padding: 1px 1px;
    }
  .bg-red {
    background-color: #dd4b39;
    font-weight: bold;
    color: white !important;
  }
  .bg-red-rows {
    background-color: #dd4c39be;
    color: white !important;
  }
</style>
<link rel="stylesheet" href="/css/daterangepicker.css">
<link rel="stylesheet" href="/css/dataTables.bootstrap4.css">
@endsection
@section('content')
<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-12">
      @if (Session::has('alerts'))
        @foreach(Session::get('alerts') as $alert)
          <div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!} <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span></div>
        @endforeach
      @endif
      <h2 class="page-title">PERIODE BY ORDER {{ date('d/m/Y', strtotime($startDate)) }} - {{ date('d/m/Y', strtotime($endDate)) }}</h2>
			  <div class="row">

                <div class="col-md-12">
                    <p class="text-muted" style="float: left;">Last Update K-PRO {{ $log_kpro->LAST_SYNC }} WITA</p>
                    <p class="text-muted" style="float: right;">Last Update UT Online {{ @$log_utonline->last_updated_at }} WITA</p>
                </div>

                <div class="col-md-12">
                    <div class="card shadow mb-4">
                    <div class="card-body">
                        <form method="GET">
                        <div class="form-row">
                            <div class="form-group col-md-5">
                                <label for="startDate">START DATE</label>
                                <div class="input-group">
                                <input type="text" class="form-control drgpicker" id="startDate" name="startDate" value="{{ $startDate ? : date('Y-m-d') }}" aria-describedby="button-addon2" readonly>
                                <div class="input-group-append">
                                    <div class="input-group-text" id="button-addon-date"><span class="fe fe-calendar fe-16"></span></div>
                                </div>
                                </div>
                            </div>
                            <div class="form-group col-md-5">
                                <label for="endDate">END DATE</label>
                                <div class="input-group">
                                <input type="text" class="form-control drgpicker" id="endDate" name="endDate" value="{{ $endDate ? : date('Y-m-d') }}" aria-describedby="button-addon2" readonly>
                                <div class="input-group-append">
                                    <div class="input-group-text" id="button-addon-date"><span class="fe fe-calendar fe-16"></span></div>
                                </div>
                                </div>
                            </div>
                            <div class="form-group col-md-2">
                                <label for="search">&nbsp;</label>
                                <button type="submit" class="btn btn-primary btn-block">Search</button>
                            </div>
                        </div>
                        </form>
                    </div>
                    </div>
                </div>

                <div class="col-md-12">
                  <a href="/Rekon/provisioning/cutoff/order-based" class="btn mb-2 btn-primary" target="_blank"><i class="fe fe-edit"></i> Cutoff Rekon (Order Based)</a>
                  <a href="/Rekon/provisioning/create/order-based" class="btn mb-2 btn-primary" target="_blank"><i class="fe fe-edit"></i> Create Rekon (Order Based)</a>
                </div>

                <div class="col-md-12">
                    <div class="card shadow mb-4">
                        <div class="card-body table-responsive">
                            <table class="table table-hover table-striped table-bordered table-sm datatables" id="rekonKproMonth">
                                <thead>
                                    <tr>
                                    <th class="bg-red" rowspan="2">REG</th>
                                    <th class="bg-red" rowspan="2">WITEL</th>
                                    <th class="bg-red" colspan="12">K-PRO PS BULANAN : {{ date('Y') }}</th>
                                    <th class="bg-red" rowspan="2">TOTAL</th>
                                    </tr>
                                    <tr>
                                    @php
                                        $months = ['januari' => '01', 'februari' => '02', 'maret' => '03', 'april' => '04', 'mei' => '05', 'juni' => '06', 'juli' => '07', 'agustus' => '08', 'september' => '09', 'oktober' => '10', 'november' => '11', 'desember' => '12'];
                                    @endphp
                                    @foreach ($months as $key => $value)
                                    <th class="bg-red">{{ strtoupper($key) }}</th>
                                    @endforeach
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <td style="font-weight: bold;" colspan="2">TOTAL</td>
                                        <td style="font-weight: bold;">-</td>
                                        <td style="font-weight: bold;">-</td>
                                        <td style="font-weight: bold;">-</td>
                                        <td style="font-weight: bold;">-</td>
                                        <td style="font-weight: bold;">-</td>
                                        <td style="font-weight: bold;">-</td>
                                        <td style="font-weight: bold;">-</td>
                                        <td style="font-weight: bold;">-</td>
                                        <td style="font-weight: bold;">-</td>
                                        <td style="font-weight: bold;">-</td>
                                        <td style="font-weight: bold;">-</td>
                                        <td style="font-weight: bold;">-</td>
                                        <td style="font-weight: bold;">-</td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="card shadow mb-4">
                        <div class="card-body table-responsive">
                            <table class="table table-hover table-striped table-bordered table-sm datatables" id="rekonKproDay">
                                <thead>
                                    <tr>
                                    <th class="bg-red" rowspan="2">REG</th>
                                    <th class="bg-red" rowspan="2">WITEL</th>
                                    <th class="bg-red" colspan="{{ date('t') }}">K-PRO PS HARIAN : {{ strtoupper(date('F Y')) }}</th>
                                    </tr>
                                    <tr>
                                    @for ($i = 1; $i < date('t') + 1; $i ++)
                                        @if ($i < 10)
                                            @php
                                            $keys = '0'.$i;
                                            @endphp
                                        @else
                                            @php
                                            $keys = $i;
                                            @endphp
                                        @endif
                                        <th class="bg-red">{{ $keys }}</th>
                                    @endfor
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="card shadow mb-4">
                        <div class="card-body table-responsive">
                            <table class="table table-hover table-striped table-bordered table-sm datatables" id="rekonKproOrder">
                            <thead>
                                <tr>
                                    <th style="color: #6c757d; font-weight: bold;" rowspan="2">REG</th>
                                    <th style="color: #6c757d; font-weight: bold;" rowspan="2">WITEL</th>
                                    <th style="color: #6c757d; font-weight: bold;" colspan="8">PSB</th>
                                    <th style="color: #6c757d; font-weight: bold;" colspan="8">PSB + SURVEY</th>
                                    <th style="color: #6c757d; font-weight: bold;" colspan="20">ADDON</th>
                                    <th style="color: #6c757d; font-weight: bold;" colspan="2">TOTAL</th>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold;">1P</td>
                                    <td style="font-weight: bold;">Jasa (Rp)</td>
                                    <td style="font-weight: bold;">2P<br />(Tlp+Int)</td>
                                    <td style="font-weight: bold;">Jasa (Rp)</td>
                                    <td style="font-weight: bold;">2P<br />(Int+IPTV)</td>
                                    <td style="font-weight: bold;">Jasa (Rp)</td>
                                    <td style="font-weight: bold;">3P</td>
                                    <td style="font-weight: bold;">Jasa (Rp)</td>
                                    <td style="font-weight: bold;">1P</td>
                                    <td style="font-weight: bold;">Jasa (Rp)</td>
                                    <td style="font-weight: bold;">2P<br />(Tlp+Int)</td>
                                    <td style="font-weight: bold;">Jasa (Rp)</td>
                                    <td style="font-weight: bold;">2P<br />(Int+IPTV)</td>
                                    <td style="font-weight: bold;">Jasa (Rp)</td>
                                    <td style="font-weight: bold;">3P</td>
                                    <td style="font-weight: bold;">Jasa (Rp)</td>
                                    <td style="font-weight: bold;">WMS</td>
                                    <td style="font-weight: bold;">Jasa (Rp)</td>
                                    <td style="font-weight: bold;">WMSL</td>
                                    <td style="font-weight: bold;">Jasa (Rp)</td>
                                    <td style="font-weight: bold;">Addon<br />STB</td>
                                    <td style="font-weight: bold;">Jasa (Rp)</td>
                                    <td style="font-weight: bold;">Second<br />STB</td>
                                    <td style="font-weight: bold;">Jasa (Rp)</td>
                                    <td style="font-weight: bold;">Change<br />STB</td>
                                    <td style="font-weight: bold;">Jasa (Rp)</td>
                                    <td style="font-weight: bold;">ONT<br />Premium</td>
                                    <td style="font-weight: bold;">Jasa (Rp)</td>
                                    <td style="font-weight: bold;">WiFiExt</td>
                                    <td style="font-weight: bold;">Jasa (Rp)</td>
                                    <td style="font-weight: bold;">MeshWiFi</td>
                                    <td style="font-weight: bold;">Jasa (Rp)</td>
                                    <td style="font-weight: bold;">PLC</td>
                                    <td style="font-weight: bold;">Jasa (Rp)</td>
                                    <td style="font-weight: bold;">IndiHome<br />Smart</td>
                                    <td style="font-weight: bold;">Jasa (Rp)</td>
                                    <td style="font-weight: bold;">SSL</td>
                                    <td style="font-weight: bold;">Jasa (Rp)</td>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <td style="font-weight: bold;" colspan="2">TOTAL</td>
                                    <td style="font-weight: bold;">1P</td>
                                    <td style="font-weight: bold;">Jasa (Rp)</td>
                                    <td style="font-weight: bold;">2P (Tlp+Int)</td>
                                    <td style="font-weight: bold;">Jasa (Rp)</td>
                                    <td style="font-weight: bold;">2P (Int+IPTV)</td>
                                    <td style="font-weight: bold;">Jasa (Rp)</td>
                                    <td style="font-weight: bold;">3P</td>
                                    <td style="font-weight: bold;">Jasa (Rp)</td>
                                    <td style="font-weight: bold;">1P</td>
                                    <td style="font-weight: bold;">Jasa (Rp)</td>
                                    <td style="font-weight: bold;">2P (Tlp+Int)</td>
                                    <td style="font-weight: bold;">Jasa (Rp)</td>
                                    <td style="font-weight: bold;">2P (Int+IPTV)</td>
                                    <td style="font-weight: bold;">Jasa (Rp)</td>
                                    <td style="font-weight: bold;">3P</td>
                                    <td style="font-weight: bold;">Jasa (Rp)</td>
                                    <td style="font-weight: bold;">WMS</td>
                                    <td style="font-weight: bold;">Jasa (Rp)</td>
                                    <td style="font-weight: bold;">WMSL</td>
                                    <td style="font-weight: bold;">Jasa (Rp)</td>
                                    <td style="font-weight: bold;">Addon<br />STB</td>
                                    <td style="font-weight: bold;">Jasa (Rp)</td>
                                    <td style="font-weight: bold;">Second<br />STB</td>
                                    <td style="font-weight: bold;">Jasa (Rp)</td>
                                    <td style="font-weight: bold;">Change<br />STB</td>
                                    <td style="font-weight: bold;">Jasa (Rp)</td>
                                    <td style="font-weight: bold;">ONT<br />Premium</td>
                                    <td style="font-weight: bold;">Jasa (Rp)</td>
                                    <td style="font-weight: bold;">WiFiExt</td>
                                    <td style="font-weight: bold;">Jasa (Rp)</td>
                                    <td style="font-weight: bold;">MeshWiFi</td>
                                    <td style="font-weight: bold;">Jasa (Rp)</td>
                                    <td style="font-weight: bold;">PLC</td>
                                    <td style="font-weight: bold;">Jasa (Rp)</td>
                                    <td style="font-weight: bold;">IndiHomeSmart</td>
                                    <td style="font-weight: bold;">Jasa (Rp)</td>
                                    <td style="font-weight: bold;">SSL</td>
                                    <td style="font-weight: bold;">Jasa (Rp)</td>
                                </tr>
                            </tfoot>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                  <div class="card shadow mb-4">
                      <div class="card-body table-responsive">
                          <table class="table table-hover table-striped table-bordered table-sm datatables" id="rekonKproOrderCutoff">
                          <thead>
                              <tr>
                                  <th style="color: #6c757d; font-weight: bold;" rowspan="2">REG</th>
                                  <th style="color: #6c757d; font-weight: bold;" rowspan="2">WITEL</th>
                                  <th style="color: #6c757d; font-weight: bold;" rowspan="2">MITRA</th>
                                  <th style="color: #6c757d; font-weight: bold;" colspan="8">PSB</th>
                                  <th style="color: #6c757d; font-weight: bold;" colspan="8">PSB + SURVEY</th>
                                  <th style="color: #6c757d; font-weight: bold;" colspan="20">ADDON</th>
                                  <th style="color: #6c757d; font-weight: bold;" colspan="2">TOTAL</th>
                              </tr>
                              <tr>
                                  <td style="font-weight: bold;">1P</td>
                                  <td style="font-weight: bold;">Jasa (Rp)</td>
                                  <td style="font-weight: bold;">2P<br />(Tlp+Int)</td>
                                  <td style="font-weight: bold;">Jasa (Rp)</td>
                                  <td style="font-weight: bold;">2P<br />(Int+IPTV)</td>
                                  <td style="font-weight: bold;">Jasa (Rp)</td>
                                  <td style="font-weight: bold;">3P</td>
                                  <td style="font-weight: bold;">Jasa (Rp)</td>
                                  <td style="font-weight: bold;">1P</td>
                                  <td style="font-weight: bold;">Jasa (Rp)</td>
                                  <td style="font-weight: bold;">2P<br />(Tlp+Int)</td>
                                  <td style="font-weight: bold;">Jasa (Rp)</td>
                                  <td style="font-weight: bold;">2P<br />(Int+IPTV)</td>
                                  <td style="font-weight: bold;">Jasa (Rp)</td>
                                  <td style="font-weight: bold;">3P</td>
                                  <td style="font-weight: bold;">Jasa (Rp)</td>
                                  <td style="font-weight: bold;">WMS</td>
                                  <td style="font-weight: bold;">Jasa (Rp)</td>
                                  <td style="font-weight: bold;">WMSL</td>
                                  <td style="font-weight: bold;">Jasa (Rp)</td>
                                  <td style="font-weight: bold;">Addon<br />STB</td>
                                  <td style="font-weight: bold;">Jasa (Rp)</td>
                                  <td style="font-weight: bold;">Second<br />STB</td>
                                  <td style="font-weight: bold;">Jasa (Rp)</td>
                                  <td style="font-weight: bold;">Change<br />STB</td>
                                  <td style="font-weight: bold;">Jasa (Rp)</td>
                                  <td style="font-weight: bold;">ONT<br />Premium</td>
                                  <td style="font-weight: bold;">Jasa (Rp)</td>
                                  <td style="font-weight: bold;">WiFiExt</td>
                                  <td style="font-weight: bold;">Jasa (Rp)</td>
                                  <td style="font-weight: bold;">MeshWiFi</td>
                                  <td style="font-weight: bold;">Jasa (Rp)</td>
                                  <td style="font-weight: bold;">PLC</td>
                                  <td style="font-weight: bold;">Jasa (Rp)</td>
                                  <td style="font-weight: bold;">IndiHome<br />Smart</td>
                                  <td style="font-weight: bold;">Jasa (Rp)</td>
                                  <td style="font-weight: bold;">SSL</td>
                                  <td style="font-weight: bold;">Jasa (Rp)</td>
                              </tr>
                          </thead>
                          <tfoot>
                              <tr>
                                  <td style="font-weight: bold;" colspan="3">TOTAL</td>
                                  <td style="font-weight: bold;">1P</td>
                                  <td style="font-weight: bold;">Jasa (Rp)</td>
                                  <td style="font-weight: bold;">2P (Tlp+Int)</td>
                                  <td style="font-weight: bold;">Jasa (Rp)</td>
                                  <td style="font-weight: bold;">2P (Int+IPTV)</td>
                                  <td style="font-weight: bold;">Jasa (Rp)</td>
                                  <td style="font-weight: bold;">3P</td>
                                  <td style="font-weight: bold;">Jasa (Rp)</td>
                                  <td style="font-weight: bold;">1P</td>
                                  <td style="font-weight: bold;">Jasa (Rp)</td>
                                  <td style="font-weight: bold;">2P (Tlp+Int)</td>
                                  <td style="font-weight: bold;">Jasa (Rp)</td>
                                  <td style="font-weight: bold;">2P (Int+IPTV)</td>
                                  <td style="font-weight: bold;">Jasa (Rp)</td>
                                  <td style="font-weight: bold;">3P</td>
                                  <td style="font-weight: bold;">Jasa (Rp)</td>
                                  <td style="font-weight: bold;">WMS</td>
                                  <td style="font-weight: bold;">Jasa (Rp)</td>
                                  <td style="font-weight: bold;">WMSL</td>
                                  <td style="font-weight: bold;">Jasa (Rp)</td>
                                  <td style="font-weight: bold;">Addon<br />STB</td>
                                  <td style="font-weight: bold;">Jasa (Rp)</td>
                                  <td style="font-weight: bold;">Second<br />STB</td>
                                  <td style="font-weight: bold;">Jasa (Rp)</td>
                                  <td style="font-weight: bold;">Change<br />STB</td>
                                  <td style="font-weight: bold;">Jasa (Rp)</td>
                                  <td style="font-weight: bold;">ONT<br />Premium</td>
                                  <td style="font-weight: bold;">Jasa (Rp)</td>
                                  <td style="font-weight: bold;">WiFiExt</td>
                                  <td style="font-weight: bold;">Jasa (Rp)</td>
                                  <td style="font-weight: bold;">MeshWiFi</td>
                                  <td style="font-weight: bold;">Jasa (Rp)</td>
                                  <td style="font-weight: bold;">PLC</td>
                                  <td style="font-weight: bold;">Jasa (Rp)</td>
                                  <td style="font-weight: bold;">IndiHomeSmart</td>
                                  <td style="font-weight: bold;">Jasa (Rp)</td>
                                  <td style="font-weight: bold;">SSL</td>
                                  <td style="font-weight: bold;">Jasa (Rp)</td>
                              </tr>
                          </tfoot>
                          </table>
                      </div>
                  </div>
              </div>

			  </div>
		</div>
	</div>
</div>
@endsection
@section('footerS')
<script src="/js/jquery.dataTables.min.js"></script>
<script src="/js/dataTables.bootstrap4.min.js"></script>
<script src="/js/moment.min.js"></script>
<script src="/js/daterangepicker.js"></script>
<script src="/js/select2.min.js"></script>
<script src="/js/jquery.timepicker.js"></script>

<script type = "text/javascript">
  $(document).ready(function () {

      var get_regional = {!! json_encode($get_regional) !!}
      var regional_only = [];

      $.each(get_regional, function(k, v) {
        regional_only[v.id_reg] = v.reg
      });

      var convert_reg_object = Object.assign({}, regional_only);
      var object_array_reg = Object.values(convert_reg_object);

      var renew_reg = ['ALL'];

      renew_reg.push(...object_array_reg);

      $('#regional').select2({
        placeholder: 'Pilih Regional',
        allowClear: true,
        data: renew_reg
      });

      $('#regional').on('select2:select change', function(){
        var isi = $(this).val();
        if(isi !=  null){
          var data = $.grep(get_regional, function(e){ return e.reg == isi});
          var witel_only = [];
          var renew_witel = ['ALL'];

          $.each(data, function(k, v){
              witel_only[v.witel] = v.witel
          });

          var convert_witel_obj = Object.assign({}, witel_only);
          var object_array_witel = Object.values(convert_witel_obj);

          renew_witel.push(...object_array_witel);

          $('#witel').empty().trigger('change');

          $('#witel').select2({
              placeholder: 'Pilih Witel',
              allowClear: true,
              data: renew_witel
          });
        }
      })

      $('#witel').on('select2:select change', function(){
        var isi = $(this).val();

        if(isi !=  null){
          var data = $.grep(get_regional, function(e){ return e.witel == isi}),
          mitra_only = [];

          $.each(data, function(k, v){
            mitra_only.push({
              id: v.id_mitra,
              text: v.mitra
            });
          });

          $('#mitra').empty().trigger('change');

          $('#mitra').select2({
              placeholder: 'Pilih mitra',
              allowClear: true,
              data: mitra_only
          });
        }
      })

      $('#regional').val(null).change();

      $("#periode-kegiatan").val(null).change();

      $("#periode-kegiatan").select2({
          placeholder: "Pilih Periode Kegiatan",
          width: "100%"
      });

      var periode = {!! json_encode($bulan_format) !!}

      $("#periode-kegiatan").on( "select2 change", function() {
          var perd = this.value,
          d = new Date(),
          years = d.getFullYear()
          $.each(periode, function(key, index) {
              if(index.id == perd ) {
                  if(perd == "01") {
                      year = d.getFullYear() - 1
                  } else {
                      year = d.getFullYear()
                  }
                  $("#date-periode1").val(year + "-" + index.date_periode1)
                  $("#date-periode2").val(years + "-" + index.date_periode2)
              }
          })
      })

      $('.drgpicker').daterangepicker(
        {
          singleDatePicker: true,
          timePicker: false,
          showDropdowns: true,
          locale:
          {
            format: 'YYYY-MM-DD'
          }
      });

      var startDate = {!! json_encode($startDate) !!}
      var endDate = {!! json_encode($endDate) !!}

      $('#rekonKproMonth').DataTable({
        lengthChange: false,
        searching: false,
        paging: false,
        info: false,
        autoWidth: true,
        ordering: false,
        ajax: `/get_ajx/rekon/provisioning/dashboard/order?view=MONTH&startDate=${startDate}&endDate=${endDate}`,
        'footerCallback': function( tfoot, data, start, end, display ) {
          var response = this.api().ajax.json();
          if(response){
            var $td = $(tfoot).find('td');
            $.each(response.footer, function(k, v){
              var kk = k + 1;
              $td.eq(kk).html(v);
            })
          }
        }
      });

      $('#rekonKproDay').DataTable({
        lengthChange: false,
        searching: false,
        paging: false,
        info: false,
        autoWidth: true,
        ordering: false,
        ajax: `/get_ajx/rekon/provisioning/dashboard/order?view=DAY&startDate=${startDate}&endDate=${endDate}`
      });

      $('#rekonKproOrder').DataTable({
        lengthChange: false,
        searching: false,
        paging: false,
        info: false,
        autoWidth: true,
        ordering: false,
        ajax: `/get_ajx/rekon/provisioning/dashboard/order?view=ORDER&startDate=${startDate}&endDate=${endDate}`,
        'footerCallback': function( tfoot, data, start, end, display ) {
          var response = this.api().ajax.json();
          if(response){
            var $td = $(tfoot).find('td');
            $.each(response.footer, function(k, v){
              var kk = k + 1;
              $td.eq(kk).html(v);
            })
          }
        }
      });

      $('#rekonKproOrderCutoff').DataTable({
        lengthChange: false,
        searching: false,
        paging: false,
        info: false,
        autoWidth: true,
        ordering: false,
        ajax: `/get_ajx/rekon/provisioning/dashboard/order?view=ORDER_CUTOFF&startDate=${startDate}&endDate=${endDate}`,
        'footerCallback': function( tfoot, data, start, end, display ) {
          var response = this.api().ajax.json();
          if(response){
            var $td = $(tfoot).find('td');
            $.each(response.footer, function(k, v){
              var kk = k + 1;
              $td.eq(kk).html(v);
            })
          }
        }
      });

  });
</script>
@endsection