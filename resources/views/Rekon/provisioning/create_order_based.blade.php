@extends('layout')
@section('title', 'Create Rekon Provisioning')
@section('headerS')
<link rel="stylesheet" href="/css/daterangepicker.css">
<link rel="stylesheet" href="/css/dataTables.bootstrap4.css">
<link rel="stylesheet" href="/css/jquery.steps.css">
<link rel="stylesheet" href="/css/dropzone.min.css">
<link rel="stylesheet" href="/css/quill.snow.css">
<style>
  textarea {
    width: 100%;
    padding: 1em;
    text-align: left;
    border: 1px solid #000;
    resize: none;
    box-sizing: border-box;
  }
  table td input {
    width: 100%;
    text-align: center;
    vertical-align: middle;
  }
</style>
@endsection
@section('content')
<div class="container-fluid">
    @if (Session::has('alerts'))
      @foreach(Session::get('alerts') as $alert)
        <div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!} <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span></div>
      @endforeach
    @endif

    @if ($is_response == false)
    <div class="card shadow mb-4">
      <div class="card-header">
        <strong class="card-title">Create Rekon - Order Based</strong>
      </div>
      <div class="card-body">
        <form method="post">
        <div class="form-row">
          <div class="col-md-6">
            <div class="form-group mb-3">
              <label class="col-form-label pull-right" for="periode_kegiatan">Periode Kegiatan</label>
              <select class="text-center required" id="periode-kegiatan" name="periode_kegiatan">
              @foreach ($bulan_format as $bf)
              <option class="tex-center" value="{{ $bf->id }}">{{ $bf->text }}</option>                    
              @endforeach
              </select>
            </div>
            <div class="form-group mb-3">
              <label class="col-form-label pull-right" for="date_periode1">Pelaksanaan Awal</label>
              <input type="text" class="form-control input-transparent text-center" id="date-periode1" name="rekon_periode1" readonly>
            </div>
            <div class="form-group mb-3">
              <label class="col-form-label pull-right" for="date_periode2">Pelaksanaan Akhir</label>
              <input type="text" class="form-control input-transparent text-center" id="date-periode2" name="rekon_periode2" readonly>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group mb-3">
              <label class="col-form-label pull-right" for="regional">Regional</label>
              <select class="form-control select2" name="regional" id="regional">
              </select>
            </div>
            <div class="form-group mb-3">
              <label class="col-form-label pull-right" for="witel">Witel</label>
              <select class="form-control select2" name="witel" id="witel">
              </select>
            </div>
            <div class="form-group mb-3">
              <label class="col-form-label pull-right" for="mitra">Mitra</label>
              <select class="form-control select2" name="id_mitra" id="mitra">
              </select>
            </div>
          </div>
          <div class="col-md-12">
            <button type="submit" class="btn btn-block btn_submit btn-primary">Create</button>
          </div>
        </div>
        </form>
      </div>
    </div>
    @endif

    @if ($is_response == true)
    <div class="card shadow my-4">
      <div class="card-header">
        <strong>Pekerjaan Pasang Sambungan Baru (PSB) - Skema Order Based</strong>
      </div>
      <div class="card-body">
        <form id="form-create" method="post" action="/Rekon/provisioning/create/order-based-save" enctype="multipart/form-data" autocomplete="off">
          {{ csrf_field() }}
          <input type="hidden" name="id_regional" value="{{ $regional }}">
          <input type="hidden" name="id_mitra" value="{{ $id_mitra }}">
          <input type="hidden" name="periode_kegiatan" value="{{ $periode_kegiatan }}">
          <input type="hidden" name="rekon_periode1" value="{{ $rekon_periode1 }}">
          <input type="hidden" name="rekon_periode2" value="{{ $rekon_periode2 }}">
          <input type="hidden" name="flag_dok_psb" name="beta_kalimantan">
  
          <div class="col-md-12">

            <h3>SSL</h3>
            <section class="form-row" id="section_ssl">
              <div class="table-responsive">
                <table class="table table-hover table-sm table-bordered tableJustif" style="text-align: center;">
                  <thead class="table-primary">
                  <tr>
                    <th style="color: black">TYPE TRANSAKSI</th>
                    <th style="color: black">JENIS LAYANAN</th>
                    <th style="color: black">HSS JASA (Rupiah)</th>
                    <th style="color: black;">JUMLAH SSL</th>
                    <th style="color: black;">TOTAL (Rupiah)</th>
                  </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td rowspan="5">Infrastructure New Sales Fiber<br />KU + SURVEY</td>
                    </tr>
                    <tr>
                      <td>0P - 1P (Tlp/Int)</td>
                      <td>
                        {{ str_replace(',', '.', number_format($hss_witel->p1)) }}
                      </td>
                      <td>
                        {{ str_replace(',', '.', number_format($fix_rekon->p1)) }}
                      </td>
                      <td>
                        @php
                          $jasa_p1 = $fix_rekon->p1 * $hss_witel->p1;
                        @endphp
                        {{ str_replace(',', '.', number_format($jasa_p1)) }}
                      </td>
                    </tr>
                    <tr>
                      <td>0P - 2P (Tlp+Int)</td>
                      <td>
                        {{ str_replace(',', '.', number_format($hss_witel->p2_inet_voice)) }}
                      </td>
                      <td>
                        {{ str_replace(',', '.', number_format($fix_rekon->p2_inet_voice)) }}
                      </td>
                      <td>
                        @php
                          $jasa_p2_inet_voice = $fix_rekon->p2_inet_voice * $hss_witel->p2_inet_voice;
                        @endphp
                        {{ str_replace(',', '.', number_format($jasa_p2_inet_voice)) }}
                      </td>
                    </tr>
                    <tr>
                      <td>0P - 2P (Int+IPTV)</td>
                      <td>
                        {{ str_replace(',', '.', number_format($hss_witel->p2_inet_iptv)) }}
                      </td>
                      <td>
                        {{ str_replace(',', '.', number_format($fix_rekon->p2_inet_iptv)) }}
                      </td>
                      <td>
                        @php
                          $jasa_p2_inet_iptv = $fix_rekon->p2_inet_iptv * $hss_witel->p2_inet_iptv;
                        @endphp
                        {{ str_replace(',', '.', number_format($jasa_p2_inet_iptv)) }}
                      </td>
                    </tr>
                    <tr>
                      <td>0P - 3P (Tlp+Int+IPTV)</td>
                      <td>
                        {{ str_replace(',', '.', number_format($hss_witel->p3)) }}
                      </td>
                      <td>
                        {{ str_replace(',', '.', number_format($fix_rekon->p3)) }}
                      </td>
                      <td>
                        @php
                          $jasa_p3 = $fix_rekon->p3 * $hss_witel->p3;
                        @endphp
                        {{ str_replace(',', '.', number_format($jasa_p3)) }}
                      </td>
                    </tr>
                    <tr>
                      <td rowspan="5">Infrastructure New Sales Fiber<br />KU</td>
                    </tr>
                    <tr>
                      <td>0P - 1P (Tlp/Int)</td>
                      <td>
                        {{ str_replace(',', '.', number_format($hss_witel->p1_ku)) }}
                      </td>
                      <td>
                        {{ str_replace(',', '.', number_format($fix_rekon->p1_ku)) }}
                      </td>
                      <td>
                        @php
                          $jasa_p1_ku = $fix_rekon->p1_ku * $hss_witel->p1_ku;
                        @endphp
                        {{ str_replace(',', '.', number_format($jasa_p1_ku)) }}
                      </td>
                    </tr>
                    <tr>
                      <td>0P - 2P (Tlp+Int)</td>
                      <td>
                        {{ str_replace(',', '.', number_format($hss_witel->p2_inet_voice_ku)) }}
                      </td>
                      <td>
                        {{ str_replace(',', '.', number_format($fix_rekon->p2_inet_voice_ku)) }}
                      </td>
                      <td>
                        @php
                          $jasa_p2_inet_voice_ku = $fix_rekon->p2_inet_voice_ku * $hss_witel->p2_inet_voice_ku;
                        @endphp
                        {{ str_replace(',', '.', number_format($jasa_p2_inet_voice_ku)) }}
                      </td>
                    </tr>
                    <tr>
                      <td>0P - 2P (Int+IPTV)</td>
                      <td>
                        {{ str_replace(',', '.', number_format($hss_witel->p2_inet_iptv_ku)) }}
                      </td>
                      <td>
                        {{ str_replace(',', '.', number_format($fix_rekon->p2_inet_iptv_ku)) }}
                      </td>
                      <td>
                        @php
                          $jasa_p2_inet_iptv_ku = $fix_rekon->p2_inet_iptv_ku * $hss_witel->p2_inet_iptv_ku;
                        @endphp
                        {{ str_replace(',', '.', number_format($jasa_p2_inet_iptv_ku)) }}
                      </td>
                    </tr>
                    <tr>
                      <td>0P - 3P (Tlp+Int+IPTV)</td>
                      <td>
                        {{ str_replace(',', '.', number_format($hss_witel->p3_ku)) }}
                      </td>
                      <td>
                        {{ str_replace(',', '.', number_format($fix_rekon->p3_ku)) }}
                      </td>
                      <td>
                        @php
                          $jasa_p3_ku = $fix_rekon->p3_ku * $hss_witel->p3_ku;
                        @endphp
                        {{ str_replace(',', '.', number_format($jasa_p3_ku)) }}
                      </td>
                    </tr>
                    <tr>
                      <td rowspan="4">MIGRASI</td>
                    </tr>
                    <tr>
                      <td>1P ke 2P</td>
                      <td>
                        {{ str_replace(',', '.', number_format($hss_witel->migrasi_1p)) }}
                      </td>
                      <td>
                        0
                      </td>
                      <td>
                        @php
                          $jasa_migrasi_1p = 0 * $hss_witel->migrasi_1p;
                        @endphp
                        {{ str_replace(',', '.', number_format($jasa_migrasi_1p)) }}
                      </td>
                    </tr>
                    <tr>
                      <td>1P ke 3P</td>
                      <td>
                        {{ str_replace(',', '.', number_format($hss_witel->migrasi_2p)) }}
                      </td>
                      <td>
                        0
                      </td>
                      <td>
                        @php
                          $jasa_migrasi_2p = 0 * $hss_witel->migrasi_2p;
                        @endphp
                        {{ str_replace(',', '.', number_format($jasa_migrasi_2p)) }}
                      </td>
                    </tr>
                    <tr>
                      <td>2P ke 3P</td>
                      <td>
                        {{ str_replace(',', '.', number_format($hss_witel->migrasi_3p)) }}
                      </td>
                      <td>
                        0
                      </td>
                      <td>
                        @php
                          $jasa_migrasi_3p = 0 * $hss_witel->migrasi_3p;
                        @endphp
                        {{ str_replace(',', '.', number_format($jasa_migrasi_3p)) }}
                      </td>
                    </tr>
                    <tr>
                      <td rowspan="12">ADDON</td>
                    </tr>
                    <tr>
                      <td>LME WiFi Provisioning Type-1 (PT1) KU</td>
                      <td>
                        {{ str_replace(',', '.', number_format($hss_witel->lme_wifi)) }}
                      </td>
                      <td>
                        {{ str_replace(',', '.', number_format($fix_rekon->wms + $fix_rekon->wms_lite)) }}
                      </td>
                      <td>
                        @php
                          $jasa_lme_wifi = ($fix_rekon->wms + $fix_rekon->wms_lite) * $hss_witel->lme_wifi;
                        @endphp
                        {{ str_replace(',', '.', number_format($jasa_lme_wifi)) }}
                      </td>
                    </tr>
                    <tr>
                      <td>LME-AP-Indoor</td>
                      <td>
                        {{ str_replace(',', '.', number_format($hss_witel->lme_ap_indoor)) }}
                      </td>
                      <td>
                        {{ str_replace(',', '.', number_format($fix_rekon->wms)) }}
                      </td>
                      <td>
                        @php
                          $jasa_lme_ap_indoor = $fix_rekon->wms * $hss_witel->lme_ap_indoor;
                        @endphp
                        {{ str_replace(',', '.', number_format($jasa_lme_ap_indoor)) }}
                      </td>
                    </tr>
                    <tr>
                      <td>LME-AP-Outdoor</td>
                      <td>
                        {{ str_replace(',', '.', number_format($hss_witel->lme_ap_outdoor)) }}
                      </td>
                      <td>
                        0
                      </td>
                      <td>
                        @php
                          $jasa_lme_ap_outdoor = 0 * $hss_witel->lme_ap_outdoor;
                        @endphp
                        {{ str_replace(',', '.', number_format($jasa_lme_ap_outdoor)) }}
                      </td>
                    </tr>
                    <tr>
                      <td>Smart Camera Only</td>
                      <td>
                        {{ str_replace(',', '.', number_format($hss_witel->smartcam_only)) }}
                      </td>
                      <td>
                        {{ str_replace(',', '.', number_format($fix_rekon->smartcam_only)) }}
                      </td>
                      <td>
                        @php
                          $jasa_smartcam_only = $fix_rekon->smartcam_only * $hss_witel->smartcam_only;
                        @endphp
                        {{ str_replace(',', '.', number_format($jasa_smartcam_only)) }}
                      </td>
                    </tr>
                    <tr>
                      <td>Instalasi PLC</td>
                      <td>
                        {{ str_replace(',', '.', number_format($hss_witel->plc)) }}
                      </td>
                      <td>
                        {{ str_replace(',', '.', number_format($fix_rekon->plc)) }}
                      </td>
                      <td>
                        @php
                          $jasa_plc = $fix_rekon->plc * $hss_witel->plc;
                        @endphp
                        {{ str_replace(',', '.', number_format($jasa_plc)) }}
                      </td>
                    </tr>
                    <tr>
                      <td>IKR Add-on STB Tambahan</td>
                      <td>
                        {{ str_replace(',', '.', number_format($hss_witel->stb_tambahan)) }}
                      </td>
                      <td>
                        {{ str_replace(',', '.', number_format($fix_rekon->addon_stb + $fix_rekon->second_stb)) }}
                      </td>
                      <td>
                        @php
                          $jasa_stb_tambahan = ($fix_rekon->addon_stb + $fix_rekon->second_stb) * $hss_witel->stb_tambahan;
                        @endphp
                        {{ str_replace(',', '.', number_format($jasa_stb_tambahan)) }}
                      </td>
                    </tr>
                    <tr>
                      <td>Penggantian ONT Premium</td>
                      <td>
                        {{ str_replace(',', '.', number_format($hss_witel->ont_premium)) }}
                      </td>
                      <td>
                        {{ str_replace(',', '.', number_format($fix_rekon->ont_premium)) }}
                      </td>
                      <td>
                        @php
                          $jasa_ont_premium = $fix_rekon->ont_premium * $hss_witel->ont_premium;
                        @endphp
                        {{ str_replace(',', '.', number_format($jasa_ont_premium)) }}
                      </td>
                    </tr>
                    <tr>
                      <td>Instalalasi Wifi Extender</td>
                      <td>
                        {{ str_replace(',', '.', number_format($hss_witel->wifiext)) }}
                      </td>
                      <td>
                        {{ str_replace(',', '.', number_format($fix_rekon->wifiext + $fix_rekon->meshwifi)) }}
                      </td>
                      <td>
                        @php
                          $jasa_wifiext = ($fix_rekon->wifiext + $fix_rekon->meshwifi) * $hss_witel->wifiext;
                        @endphp
                        {{ str_replace(',', '.', number_format($jasa_wifiext)) }}
                      </td>
                    </tr>
                    <tr>
                      <td>Insert Tiang 7</td>
                      <td>
                        {{ str_replace(',', '.', number_format($hss_witel->insert_tiang)) }}
                      </td>
                      <td>
                        0
                      </td>
                      <td>
                        @php
                          $jasa_insert_tiang = 0 * $hss_witel->insert_tiang;
                        @endphp
                        {{ str_replace(',', '.', number_format($jasa_insert_tiang)) }}
                      </td>
                    </tr>
                    <tr>
                      <td>Insert Tiang 9</td>
                      <td>
                        {{ str_replace(',', '.', number_format($hss_witel->insert_tiang_9)) }}
                      </td>
                      <td>
                        0
                      </td>
                      <td>
                        @php
                          $jasa_insert_tiang_9 = 0 * $hss_witel->insert_tiang_9;
                        @endphp
                        {{ str_replace(',', '.', number_format($jasa_insert_tiang_9)) }}
                      </td>
                    </tr>  
                  </tbody>
                  <tfoot>
                    <tr>
                      <td colspan="4"><b>TOTAL</b></td>
                      <td><b>{{ str_replace(',', '.', number_format($jasa_p1 + $jasa_p2_inet_voice + $jasa_p2_inet_iptv + $jasa_p3 + $jasa_p1_ku + $jasa_p2_inet_voice_ku + $jasa_p2_inet_iptv_ku + $jasa_p3_ku + $jasa_migrasi_1p + $jasa_migrasi_2p + $jasa_migrasi_3p + $jasa_lme_wifi + $jasa_lme_ap_indoor + $jasa_lme_ap_outdoor + $jasa_smartcam_only + $jasa_plc + $jasa_stb_tambahan + $jasa_ont_premium + $jasa_wifiext + $jasa_insert_tiang + $jasa_insert_tiang_9)) }}</b></td>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </section>
  
            <h3>Periode</h3>
            <section class="form-row" id="section_periode">
              <div class="form-group col-md-3">
                  <label class="col-form-label pull-right" for="ppn_numb">PPN (%)</label>
                  <input type="text" class="form-control input-transparent text-center required" name="ppn_numb" value="11" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" minlength="2" maxlength="2">
              </div>
              <div class="form-group col-md-3">
                  <label class="col-form-label pull-right" for="id_project">ID Project</label>
                  <input type="text" class="form-control input-transparent text-center required" name="id_project" value="{{ $hss_witel->project_id }}">
              </div>
              <div class="form-group col-md-3">
                  <label class="col-form-label pull-right" for="masa_berlaku_pks">Masa Berlaku PKS</label>
                  <input type="text" class="form-control tgl-picker text-center required" id="masa-berlaku-pks" name="masa_berlaku_pks" value="{{ date('Y-m-d') }}" readonly>
                  <span class="help-block"><small>* Amandemen Terakhir</small></span>
              </div>
              <div class="form-group col-md-3">
                  <label class="col-form-label pull-right" for="tanggal_terbit">Tanggal Terbit</label>
                  <input type="text" class="form-control text-center tgl-picker required" name="tanggal_terbit" value="{{ date('Y-m-d') }}" readonly>
              </div>
              <div class="form-group col-md-4">
                  <label class="col-form-label" for="toc">TOC</label>
                  <input type="text" id="toc" name="toc" class="form-control input-transparent text-center required" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" autocomplete="off">
                  <span id="durasi"></span>
              </div>
              <div class="form-group col-md-4">
                  <label class="col-form-label pull-right" for="tgl_sp">Tanggal Surat Pesanan</label>
                  <input type="text" class="form-control tgl-picker text-center required" id="tgl-sp" name="tgl_sp" value="{{ date('Y-m-d') }}" readonly>
              </div>
              <div class="form-group col-md-4">
                  <label class="col-form-label pull-right" for="no_sp">Nomor Surat Pesanan</label>
                  <input type="text" class="form-control input-transparent text-center required" name="no_sp">
              </div>
            </section>
  
            <h3>KPI 7 PSB</h3>
            <section class="form-row" id="section_kpi">
              <div class="form-group col-md-3">
                  <label class="col-form-label pull-right" for="kpi_real_pspi">KPI Real PS/PI (%)</label>
                  <input type="text" class="form-control input-transparent required" name="kpi_real_pspi" minlength="2" maxlength="5">
              </div>
              <div class="form-group col-md-3">
                  <label class="col-form-label pull-right" for="kpi_real_tticomply">KPI Real TTI Comply (%)</label>
                  <input type="text" class="form-control input-transparent required" name="kpi_real_tticomply" minlength="2" maxlength="5">
              </div>
              <div class="form-group col-md-3">
                  <label class="col-form-label pull-right" for="kpi_real_ffg">KPI Real FFG (%)</label>
                  <input type="text" class="form-control input-transparent required" name="kpi_real_ffg" minlength="2" maxlength="5">
              </div>
              <div class="form-group col-md-3">
                  <label class="col-form-label pull-right" for="kpi_real_validasicore">KPI Real Validasi Core (%)</label>
                  <input type="text" class="form-control input-transparent required" name="kpi_real_validasicore" minlength="2" maxlength="5">
              </div>
              <div class="form-group col-md-3">
                  <label class="col-form-label pull-right" for="kpi_real_ttrffg">KPI Real TTR FFG (%)</label>
                  <input type="text" class="form-control input-transparent required" name="kpi_real_ttrffg" minlength="2" maxlength="5">
              </div>
              <div class="form-group col-md-3">
                  <label class="col-form-label pull-right" for="kpi_real_ujipetik">KPI Real Uji Petik (%)</label>
                  <input type="text" class="form-control input-transparent required" name="kpi_real_ujipetik" minlength="2" maxlength="5">
              </div>
              <div class="form-group col-md-3">
                  <label class="col-form-label pull-right" for="kpi_real_qc2">KPI Real QC2 (%)</label>
                  <input type="text" class="form-control input-transparent required" name="kpi_real_qc2" minlength="2" maxlength="5">
              </div>
            </section>
  
            <h3>Nomor Surat Dokumen</h3>
            <section class="form-row" id="section_no_surat">
              <div class="form-group col-md-4">
                  <label class="col-form-label pull-right" for="invoice">Nomor Invoice</label>
                  <input type="text" class="form-control input-transparent text-center required" name="invoice">
              </div>
              <div class="form-group col-md-4">
                  <label class="col-form-label pull-right" for="spp_num">Nomor SPP</label>
                  <input type="text" class="form-control input-transparent text-center required" name="spp_num">
              </div>
              <div class="form-group col-md-4">
                  <label class="col-form-label pull-right" for="receipt_num">Nomor Kwitansi</label>
                  <input type="text" class="form-control input-transparent text-center required" name="receipt_num">
              </div>
              <div class="form-group col-md-4">
                  <label class="col-form-label pull-right" for="faktur">Nomor Seri Faktur Pajak</label>
                  <input type="text" class="form-control input-transparent text-center required" name="faktur">
              </div>
              <div class="form-group col-md-4">
              <label class="col-form-label pull-right" for="surat_penetapan">Nomor Surat Penetapan</label>
              <input type="text" class="form-control input-transparent text-center required" name="surat_penetapan">
              </div>
              <div class="form-group col-md-4">
              <label class="col-form-label pull-right" for="surat_kesanggupan">Nomor Surat Kesanggupan</label>
              <input type="text" class="form-control input-transparent text-center required" name="surat_kesanggupan">
              </div>
              <div class="form-group col-md-4">
                  <label class="col-form-label pull-right" for="tgl_faktur">Tanggal Faktur</label>
                  <input type="text" class="form-control tgl-picker text-center required" id="tgl-faktur" name="tgl_faktur" value="{{ date('Y-m-d') }}">
              </div>
              <div class="form-group col-md-4">
                  <label class="col-form-label pull-right" for="tgl_s_pen">Tanggal Surat Penetapan</label>
                  <input type="text" class="form-control tgl-picker text-center required" id="tgl-s-pen" name="tgl_s_pen" value="{{ date('Y-m-d') }}">
              </div>
              <div class="form-group col-md-4">
                  <label class="col-form-label pull-right" for="tgl_surat_sanggup">Tanggal Surat Kesanggupan</label>
                  <input type="text" class="form-control tgl-picker text-center required" id="tgl-surat-sanggup" name="tgl_surat_sanggup" value="{{ date('Y-m-d') }}">
              </div>
              <div class="form-group col-md-4">
              <label class="col-form-label pull-right" for="no_baij">Nomor Surat BAR&IJ</label>
              <input type="text" class="form-control input-transparent text-center required" name="no_baij">
              </div>
              <div class="form-group col-md-4">
                  <label class="col-form-label pull-right" for="BAUT">Nomor Surat BAUT</label>
                  <input type="text" class="form-control input-transparent text-center required" name="BAUT">
              </div>
              <div class="form-group col-md-4">
                  <label class="col-form-label pull-right" for="BAST">Nomor Surat BAST</label>
                  <input type="text" class="form-control input-transparent text-center required" name="BAST">
              </div>
              <div class="form-group col-md-4">
                  <label class="col-form-label pull-right" for="no_bap">Nomor Surat BA Performansi</label>
                  <input type="text" class="form-control input-transparent text-center required" name="no_bap">
              </div>
              <div class="form-group col-md-4">
                  <label class="col-form-label pull-right" for="no_ba_pemeliharaan">Nomor BA Pemeliharaan</label>
                  <input type="text" class="form-control input-transparent text-center required" name="no_ba_pemeliharaan">
              </div>
            </section>
  
            <h3>Finish</h3>
            <section id="section_finish">
                <label for="acceptTerms" style="text-align: left"> <input id="acceptTerms" name="acceptTerms" type="checkbox" class="required" /> Saya sebagai perwakilan mitra <strong>{{ $mitra->nama_company }}</strong> mengkonfirmasi bahwa data yang dimasukan ke dalam aplikasi ini <strong>PROMISE</strong> benar dan dapat dipertanggungjawabkan kebenarannya.</label>
            </section>
  
          </div>
        </form>
      </div>
    </div>
    @endif

</div>
@endsection

@section('footerS')
<script src="/js/jquery.dataTables.min.js"></script>
<script src="/js/dataTables.bootstrap4.min.js"></script>
<script src="/js/moment.min.js"></script>
<script src="/js/daterangepicker.js"></script>
<script src="/js/select2.min.js"></script>
<script src="/js/jquery.timepicker.js"></script>

<script type="text/javascript">
    var get_regional = {!! json_encode($get_regional) !!}
    var regional_only = [];

    $.each(get_regional, function(k, v) {
      regional_only[v.id_reg] = v.reg
    });

    var convert_reg_object = Object.assign({}, regional_only);
    var object_array_reg = Object.values(convert_reg_object);

    var renew_reg = ['ALL'];

    renew_reg.push(...object_array_reg);

    $('#regional').select2({
      placeholder: 'Pilih Regional',
      allowClear: true,
      data: renew_reg
    });

    $('#regional').on('select2:select change', function(){
      var isi = $(this).val();
      if(isi !=  null){
        var data = $.grep(get_regional, function(e){ return e.reg == isi});
        var witel_only = [];
        var renew_witel = ['ALL'];

        $.each(data, function(k, v){
            witel_only[v.witel] = v.witel
        });

        var convert_witel_obj = Object.assign({}, witel_only);
        var object_array_witel = Object.values(convert_witel_obj);

        renew_witel.push(...object_array_witel);

        $('#witel').empty().trigger('change');

        $('#witel').select2({
            placeholder: 'Pilih Witel',
            allowClear: true,
            data: renew_witel
        });
      }
    })

    $('#witel').on('select2:select change', function(){
      var isi = $(this).val();

      if(isi !=  null){
        var data = $.grep(get_regional, function(e){ return e.witel == isi}),
        mitra_only = [];

        $.each(data, function(k, v){
          mitra_only.push({
            id: v.id_mitra,
            text: v.mitra
          });
        });

        $('#mitra').empty().trigger('change');

        $('#mitra').select2({
            placeholder: 'Pilih mitra',
            allowClear: true,
            data: mitra_only
        });
      }
    })

    $('#regional').val(null).change();

    $("#periode-kegiatan").val(null).change();

    $("#periode-kegiatan").select2({
        placeholder: "Pilih Periode Kegiatan",
        width: "100%"
    });

    var periode = {!! json_encode($bulan_format) !!}

    $("#periode-kegiatan").on( "select2 change", function() {
        var perd = this.value,
        d = new Date(),
        years = d.getFullYear()
        $.each(periode, function(key, index) {
            if(index.id == perd ) {
                if(perd == "01") {
                    year = d.getFullYear() - 1
                } else {
                    year = d.getFullYear()
                }
                $("#date-periode1").val(year + "-" + index.date_periode1)
                $("#date-periode2").val(years + "-" + index.date_periode2)
            }
        })
    })

    var form = $("#form-create");
    form.length &&
      (form.children("div").steps({
          headerTag: "h3",
          bodyTag: "section",
          transitionEffect: "slideLeft",
          onStepChanging: function (e, a, o) {
            return (form.validate().settings.ignore = ":disabled,:hidden"), form.valid();
          },
          onFinishing: function (e, a) {
            return (form.validate().settings.ignore = ":disabled"), form.valid();
          },
          onFinished: function (e, a) {
            alert("Data rekon akan segera di proses ...");
            form.submit()
          },
      }));

      $(".tgl-picker").daterangepicker(
        {
            singleDatePicker: true,
            timePicker: false,
            showDropdowns: true,
            locale:
            {
                format: "YYYY-MM-DD",
            }
        }
      );

      $("#tgl-sp").on("change", function(e) {

        var kalkulasi_date = new Date(new Date($("#tgl-sp").val()).setDate(new Date($("#tgl-sp").val()).getDate() + parseInt($("#toc").val().length != 0 ? parseInt($("#toc").val() ) - 1 : 0))).toISOString().slice(0, 10)
        $("#durasi").html("<code>*Tanggal TOC Adalah "+kalkulasi_date+"</code>");
      });

      $("#toc").on("keypress keyup", function(e) {
          var isi = $(this).val()
          if (/\D/g.test(isi))
          {
            isi = isi.replace(/\D/g, "");
          }

          var kalkulasi_date = new Date(new Date($("#tgl-sp").val()).setDate(new Date($("#tgl-sp").val()).getDate() + parseInt(isi.length != 0 ? parseInt(isi )  - 1 : 0))).toISOString().slice(0, 10)
          $("#durasi").html("<code>*Tanggal TOC Adalah "+kalkulasi_date+"</code>");
      });

      if (/\D/g.test($("#toc").val()))
      {
        $("#toc").val() = $("#toc").val().replace(/\D/g, "");
      }
</script>
@endsection