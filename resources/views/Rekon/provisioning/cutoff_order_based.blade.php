@extends('layout')
@section('title', 'Cutoff Rekon Provisioning')
@section('headerS')
<link rel="stylesheet" href="/css/daterangepicker.css">
<link rel="stylesheet" href="/css/dataTables.bootstrap4.css">
<link rel="stylesheet" href="/css/jquery.steps.css">
<link rel="stylesheet" href="/css/dropzone.min.css">
<link rel="stylesheet" href="/css/quill.snow.css">
@endsection
@section('content')
<div class="container-fluid">
    @if (Session::has('alerts'))
      @foreach(Session::get('alerts') as $alert)
        <div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!} <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span></div>
      @endforeach
    @endif

    <div class="card shadow mb-4">
      <div class="card-header">
        <strong class="card-title">Upload Data Cutoff - Order Based</strong>
      </div>
      <div class="card-body">
        <form id="formUploadDataCutoffRekonOrderBased" method="post" action="/Rekon/provisioning/cutoff/order-based" enctype="multipart/form-data" autocomplete="off">
        {{ csrf_field() }}
        <div class="form-row">
          <div class="col-md-6">
            <div class="form-group mb-3">
              <label class="col-form-label pull-right" for="periode_kegiatan">Periode Kegiatan</label>
              <select class="text-center required" id="periode-kegiatan" name="periode_kegiatan">
              @foreach ($bulan_format as $bf)
              <option class="tex-center" value="{{ $bf->id }}">{{ $bf->text }}</option>                    
              @endforeach
              </select>
            </div>
            <div class="form-group mb-3">
              <label class="col-form-label pull-right" for="date_periode1">Pelaksanaan Awal</label>
              <input type="text" class="form-control input-transparent text-center" id="date-periode1" name="rekon_periode1" readonly>
            </div>
            <div class="form-group mb-3">
              <label class="col-form-label pull-right" for="date_periode2">Pelaksanaan Akhir</label>
              <input type="text" class="form-control input-transparent text-center" id="date-periode2" name="rekon_periode2" readonly>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group mb-3">
              <label class="col-form-label pull-right" for="regional">Regional</label>
              <select class="form-control select2" name="regional" id="regional">
              </select>
            </div>
            <div class="form-group mb-3">
              <label class="col-form-label pull-right" for="witel">Witel</label>
              <select class="form-control select2" name="witel" id="witel">
              </select>
            </div>
            <div class="form-group mb-3">
              <label class="col-form-label pull-right" for="mitra">Mitra</label>
              <select class="form-control select2" name="id_mitra" id="mitra">
              </select>
            </div>
          </div>
          <div class="col-md-12">
            <label for="file-cutoff">File *xlsx</label>
            <input type="file" name="file_cutoff" class="form-control-file">
          </div>
          <br /><br /><br /><br />
          <div class="col-md-12">
            <button type="submit" class="btn btn-block btn_submit btn-primary">Upload</button>
          </div>
        </div>
        </form>
      </div>
    </div>

    @if (count(array_filter($response_rekon)) > 0)
    <div class="card shadow mb-4">
      <div class="card-header">
        <strong class="card-title">Response</strong>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table datatables table-bordered table-hover" id="table-response-cutoff">
            <thead class="thead-dark">
              <tr>
                <th rowspan="2" class="text-center align-middle">#</th>
                <th rowspan="2" class="text-center align-middle">Order ID</th>
                <th rowspan="2" class="text-center align-middle">Layanan (upload)</th>
                <th rowspan="2" class="text-center align-middle">Layanan (sistem)</th>
                <th colspan="2" class="text-center align-middle">Re-Check</th>
                <th rowspan="2" class="text-center align-middle">Status</th>
              </tr>
              <tr>
                <th class="text-center align-middle">K-PRO</th>
                <th class="text-center align-middle">Duplicate Cutoff</th>
              </tr>
            </thead>
            <tbody>
              @php
                $numb = 0;
              @endphp
              @foreach (array_filter($response_rekon) as $k => $v)
              <tr>
                <td class="text-center align-middle">{{ ++$numb }}</td>
                <td class="text-center align-middle">{{ $v['order_id'] }}</td>
                <td class="text-center align-middle">{{ $v['layanan_upload'] }}</td>
                <td class="text-center align-middle">
                  @if ($v['layanan_fix'] == 'Layanan Tidak Diketahui')
                  <h5><span class="badge badge-pill badge-danger text-center align-middle">{{ $v['layanan_fix'] }}</span></h5>
                  @else
                  {{ $v['layanan_fix'] }}
                  @endif 
                </td>
                <td class="text-center align-middle">
                  @if ($v['is_check'] == 2)
                  <h5><span class="badge badge-pill badge-danger text-center align-middle"><i class="fe fe-x-circle fe-16 text-center align-middle"></i></span></h5>
                  @else
                  <h5><span class="badge badge-pill badge-success text-center align-middle"><i class="fe fe-check-circle fe-16 text-center align-middle"></i></span></h5>
                  @endif
                </td>
                <td class="text-center align-middle">
                  @if ($v['is_check'] == 3)
                  <h5><span class="badge badge-pill badge-danger text-center align-middle"><i class="fe fe-x-circle fe-16 text-center align-middle"></i></span></h5>
                  @else
                  <h5><span class="badge badge-pill badge-success text-center align-middle"><i class="fe fe-check-circle fe-16 text-center align-middle"></i></span></h5>
                  @endif
                </td>
                <td class="text-center align-middle">
                  @if ($v['is_check'] == 1 && $v['layanan_fix'] != 'Layanan Tidak Diketahui')
                  <h5><span class="badge badge-pill badge-success text-center align-middle"><i class="fe fe-check-circle fe-16 text-center align-middle"></i></span></h5>
                  @else
                  <h5><span class="badge badge-pill badge-danger text-center align-middle"><i class="fe fe-x-circle fe-16 text-center align-middle"></i></span></h5>
                  @endif
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
    @endif
</div>
@endsection

@section('footerS')
<script src="/js/jquery.dataTables.min.js"></script>
<script src="/js/dataTables.bootstrap4.min.js"></script>
<script src="/js/moment.min.js"></script>
<script src="/js/daterangepicker.js"></script>
<script src="/js/select2.min.js"></script>
<script src="/js/jquery.timepicker.js"></script>

<script type="text/javascript">
      var get_regional = {!! json_encode($get_regional) !!}
      var regional_only = [];

      $.each(get_regional, function(k, v) {
        regional_only[v.id_reg] = v.reg
      });

      var convert_reg_object = Object.assign({}, regional_only);
      var object_array_reg = Object.values(convert_reg_object);

      var renew_reg = ['ALL'];

      renew_reg.push(...object_array_reg);

      $('#regional').select2({
        placeholder: 'Pilih Regional',
        allowClear: true,
        data: renew_reg
      });

      $('#regional').on('select2:select change', function(){
        var isi = $(this).val();
        if(isi !=  null){
          var data = $.grep(get_regional, function(e){ return e.reg == isi});
          var witel_only = [];
          var renew_witel = ['ALL'];

          $.each(data, function(k, v){
              witel_only[v.witel] = v.witel
          });

          var convert_witel_obj = Object.assign({}, witel_only);
          var object_array_witel = Object.values(convert_witel_obj);

          renew_witel.push(...object_array_witel);

          $('#witel').empty().trigger('change');

          $('#witel').select2({
              placeholder: 'Pilih Witel',
              allowClear: true,
              data: renew_witel
          });
        }
      })

      $('#witel').on('select2:select change', function(){
        var isi = $(this).val();

        if(isi !=  null){
          var data = $.grep(get_regional, function(e){ return e.witel == isi}),
          mitra_only = [];

          $.each(data, function(k, v){
            mitra_only.push({
              id: v.id_mitra,
              text: v.mitra
            });
          });

          $('#mitra').empty().trigger('change');

          $('#mitra').select2({
              placeholder: 'Pilih mitra',
              allowClear: true,
              data: mitra_only
          });
        }
      })

      $('#regional').val(null).change();

      $("#periode-kegiatan").val(null).change();

      $("#periode-kegiatan").select2({
          placeholder: "Pilih Periode Kegiatan",
          width: "100%"
      });

      var periode = {!! json_encode($bulan_format) !!}

      $("#periode-kegiatan").on( "select2 change", function() {
          var perd = this.value,
          d = new Date(),
          years = d.getFullYear()
          $.each(periode, function(key, index) {
              if(index.id == perd ) {
                  if(perd == "01") {
                      year = d.getFullYear() - 1
                  } else {
                      year = d.getFullYear()
                  }
                  $("#date-periode1").val(year + "-" + index.date_periode1)
                  $("#date-periode2").val(years + "-" + index.date_periode2)
              }
          })
      })

      $('#table-response-cutoff').DataTable(
      {
        autoWidth: true,
        "lengthMenu": [
          [16, 32, 64, -1],
          [16, 32, 64, "All"]
        ]
      });
</script>
@endsection