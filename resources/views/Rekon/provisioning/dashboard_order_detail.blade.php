@extends('layout')

@section('title', 'Rekon Material Provisioning Mitra')

@section('headerS')
<style>
	th, td {
		text-align: center;
		vertical-align: middle;
	}

    .table thead th {
        text-align: center;
		vertical-align: middle;
        color: #6c757d;
    }

    th {
        color: #6c757d;
    }
    
    .no-wrap {
        white-space: nowrap;
    }
</style>
<link href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="https://cdn.datatables.net/buttons/2.0.1/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
@endsection

@section('content')

@if (Session::has('alerts'))
  @foreach(Session::get('alerts') as $alert)
    <div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
  @endforeach
@endif

<div class="container-fluid">
    <div class="row justify-content-center">
    <div class="col-12">
        <h2 class="mb-2 page-title">Rekon Provisioning Witel {{ $witel }}</h2>
        <p class="card-text">
            @if ($view == 'MONTH')
                K-PRO NPER <b>{{ $month }}</b>
            @elseif($view == 'DAY')
                K-PRO DAY <b>{{ $day }}</b>
            @elseif($view == 'ORDER')
                Layanan <b>{{ str_replace('_', ' ', strtoupper($layanan)) }}</b> Periode <b>{{ $startDate }}</b> s/d <b>{{ $endDate }}</b>
            @endif
        </p>
        <div class="row my-4">
            <div class="col-md-12">
                <div class="card shadow">
                    <div class="card-body table-responsive">
                        <table class="table datatables no-wrap display" id="data_table">
                            <thead>
                                <tr>
                                    <th>NO</th>
                                    <th>ACTIVATED</th>
                                    <th>LAYANAN</th>
                                    <th>ORDER_ID</th>
                                    <th>REGIONAL</th>
                                    <th>WITEL</th>
                                    <th>MITRA</th>
                                    <th>DATEL</th>
                                    <th>STO</th>
                                    <th>EXTERN_ORDER_ID</th>
                                    <th>JENIS_PSB</th>
                                    <th>TYPE_TRANSAKSI</th>
                                    <th>FLAG_DEPOSIT</th>
                                    <th>STATUS_RESUME</th>
                                    <th>STATUS_MESSAGE</th>
                                    <th>KCONTACT</th>
                                    <th>ORDER_DATE</th>
                                    <th>NCLI</th>
                                    <th>NDEM</th>
                                    <th>SPEEDY</th>
                                    <th>POTS</th>

                                    <th>UT_STATUS</th>
                                    <th>QC_STATUS</th>
                                    <th>CREATED_BA_UT</th>

                                    <th>CUSTOMER_NAME</th>
                                    <th>NO_HP</th>
                                    <th>EMAIL</th>
                                    <th>INSTALL_ADDRESS</th>
                                    <th>CUSTOMER_ADDRESS</th>
                                    <th>CITY_NAME</th>
                                    <th>GPS_LATITUDE</th>
                                    <th>GPS_LONGITUDE</th>
                                    <th>PACKAGE_NAME</th>
                                    <th>LOC_ID</th>
                                    <th>DEVICE_ID</th>
                                    <th>AGENT_ID</th>
                                    <th>WFM_ID</th>
                                    <th>SCHEDSTART</th>
                                    <th>SCHEDFINISH</th>
                                    <th>ACTSTART</th>
                                    <th>ACTFINISH</th>
                                    <th>SCHEDULE_LABOR</th>
                                    <th>FINISH_LABOR</th>
                                    <th>LAST_UPDATED_DATE</th>
                                    <th>TYPE_LAYANAN</th>
                                    <th>ISI_COMMENT</th>
                                    <th>TINDAK_LANJUT</th>
                                    <th>USER_ID_TL</th>
                                    <th>TL_DATE</th>
                                    <th>TANGGAL_PROSES</th>
                                    <th>TANGGAL_MANJA</th>
                                    <th>HIDE</th>
                                    <th>CATEGORY</th>
                                    <th>PROVIDER</th>
                                    <th>NPER</th>
                                    <th>AMCREW</th>
                                    <th>STATUS_WO</th>
                                    <th>STATUS_TASK</th>
                                    <th>CHANNEL</th>
                                    <th>GROUP_CHANNEL</th>
                                    <th>PRODUCT</th>
                                    <th>DC-ROLL</th>
                                    <th>PRECONN-50-NONACC</th>
                                    <th>PRECONN-70-NONACC</th>
                                    <th>PRECONN-80-NONACC</th>
                                    <th>TOTAL-PRECONN</th>
                                    <th>ADAPTOR-SC</th>
                                    <th>OTP-FTTH</th>
                                    <th>PREKSO-15</th>
                                    <th>PREKSO-20</th>
                                    <th>PC-SC-SC-10</th>
                                    <th>PC-SC-SC-15</th>
                                    <th>CABLE-UTP-CAT6</th>
                                    <th>PU-S7.0-140</th>
                                    <th>PU-S9.0-140</th>
                                    <th>TC-2-160</th>
                                    <th>BREKET-A</th>
                                    <th>S-Clamp-Spriner</th>
                                    <th>RJ45-5</th>
                                    <th>RJ45-6</th>
                                    <th>SOC-ILS</th>
                                    <th>SOC-SUM</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</div>
@endsection

@section('footerS')
<script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.0.1/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.print.min.js"></script>
<script type = "text/javascript">
    $(document).ready(function () {
        var view      = {!! json_encode($view) !!}
        var startDate = {!! json_encode($startDate) !!}
        var endDate   = {!! json_encode($endDate) !!}
        var witel     = {!! json_encode($witel) !!}
        var mitra     = {!! json_encode($mitra) !!}
        var month     = {!! json_encode($month) !!}
        var day       = {!! json_encode($day) !!}
        var layanan   = {!! json_encode($layanan) !!}
        
        $('#data_table').DataTable({
                ajax: `/get_ajx/rekon/provisioning/dashboard/order-detail?view=${view}&startDate=${startDate}&endDate=${endDate}&witel=${witel}&mitra=${mitra}&month=${month}&day=${day}&layanan=${layanan}`,
                select: true,
                dom: 'Blfrtip',
                lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
                buttons: [
                    {
                        extend: 'excel',
                        text: 'Export to Excel',
                        title: 'REKON MATERIAL PROVISIONING MITRA - PROMISE'
                    }
                ]
        });
    });
</script>
@endsection