@extends('layout')
@section('title', 'Rekon Order DNS Provisioning Mitra')
@section('headerS')
<style>
	th, td {
		text-align: center;
		vertical-align: middle;
	}

    .table thead th {
        text-align: center;
		vertical-align: middle;
        color: #6c757d;
    }

    th {
        color: #6c757d;
    }
    
    .no-wrap {
        white-space: nowrap;
    }
</style>
<link href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="https://cdn.datatables.net/buttons/2.0.1/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
@endsection
@section('content')
@if (Session::has('alerts'))
  @foreach(Session::get('alerts') as $alert)
    <div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
  @endforeach
@endif
<div class="container-fluid">
    <div class="row justify-content-center">
    <div class="col-12">
        <h2 class="mb-2 page-title">Rekon Provisioning Witel {{ $witel }} Mitra {{ $area }}</h2>
        <p class="card-text">Layanan {{ $layanan }} Periode {{ $startDate }} s/d {{ $endDate }}</b></p>
        <div class="row my-4">
            <div class="col-md-12">
                <div class="card shadow">
                    <div class="card-body table-responsive">
                        <table class="table datatables no-wrap display" id="data_table">
                            <thead>
                                <tr>
                                    <th>NO</th>
                                    <th>AREA</th>
                                    <th>WO_ID</th>
                                    @if ($layanan == 'ont_premium_dns')
                                    <th>NAMA_PELANGGAN</th>
                                    <th>ALAMAT_PELANGGAN</th>
                                    <th>NO_TELP</th>
                                    <th>NO_INTERNET</th>
                                    <th>ONT_TYPE_EKSISTING</th>
                                    <th>SN_ONT_EKSISTING</th>
                                    <th>ONT_TYPE_BARU</th>
                                    <th>SN_ONT_BARU</th>
                                    <th>TYPE</th>
                                    <th>HELPDESK_APPROVE</th>
                                    <th>AMCREW</th>
                                    <th>NIK_TEKNISI_1</th>
                                    <th>NIK_TEKNISI_2</th>
                                    <th>JENIS_DAPROS</th>
                                    <th>TANGGAL_USAGE</th>
                                    @elseif ($layanan == 'migrasi_stb_dns')
                                    <th>STO</th>
                                    <th>NO_INTERNET</th>
                                    <th>NO_POTS</th>
                                    <th>MAC_ADDRESS</th>
                                    <th>NAMA_PELANGGAN</th>
                                    <th>ALAMAT_PELANGGAN</th>
                                    <th>NO_HP_PELANGGAN</th>
                                    <th>HELPDESK_ASSIGN</th>
                                    <th>AMCREW</th>
                                    <th>NIK_TEKNISI_1</th>
                                    <th>NIK_TEKNISI_2</th>
                                    <th>MERK</th>
                                    <th>STB_ID</th>
                                    <th>MAC_ADDRESS_BARU</th>
                                    <th>TANGGAL_ASSIGN</th>
                                    <th>TANGGAL_VISITED</th>
                                    <th>TANGGAL_USAGE</th>
                                    @endif
                                    <th>NIK</th>
                                    <th>NAMA</th>
                                    <th>TIM</th>
                                    <th>SEKTOR</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</div>
@endsection
@section('footerS')
<script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.0.1/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.print.min.js"></script>
<script type = "text/javascript">
    $(document).ready(function () {
        var witel = {!! json_encode($witel) !!}
        var area = {!! json_encode($area) !!}
        var layanan = {!! json_encode($layanan) !!}
        var startDate = {!! json_encode($startDate) !!}
        var endDate = {!! json_encode($endDate) !!}
        
        $('#data_table').DataTable({
                ajax: `/get_ajx/rekonOrderDnsDetail?witel=${witel}&area=${area}&layanan=${layanan}&startDate=${startDate}&endDate=${endDate}`,
                select: true,
                dom: 'Blfrtip',
                lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
                buttons: [
                    {
                        extend: 'excel',
                        text: 'Export to Excel',
                        title: 'REKON ORDER DNS PROVISIONING MITRA - PROMISE'
                    }
                ]
        });
    });
</script>
@endsection