@extends('layout')
@section('title', 'Rekon Material Provisioning Mitra')
@section('headerS')
<style>
	th, td {
		text-align: center;
		vertical-align: middle;
	}

    .table thead th {
        text-align: center;
		vertical-align: middle;
        color: #6c757d;
    }

    th {
        color: #6c757d;
    }
</style>
{{-- <link rel="stylesheet" href="/css/dataTables.bootstrap4.css"> --}}
<link href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="https://cdn.datatables.net/buttons/2.0.1/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
@endsection
@section('content')
@if (Session::has('alerts'))
  @foreach(Session::get('alerts') as $alert)
    <div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
  @endforeach
@endif
<div class="container-fluid">
    <div class="row justify-content-center">
    <div class="col-12">
        @if($source <> 'psbulan_kpro')
        <h2 class="mb-2 page-title">Rekon Provisioning Mitra {{ $mitra }}</h2>
        <p class="card-text">Data dari <b>{{ strtoupper($source) }}</b> Status Laporan <b>{{ $status }}</b> Periode <b>{{ date('d/m/Y', strtotime($startDate)) }} - {{ date('d/m/Y', strtotime($endDate)) }}</b></p>
        @else
        <h2 class="mb-2 page-title">K-PRO PS BULANAN {{ $nper }} WITEL BANJARMASIN</h2>
        @endif
        <div class="row my-4">
            <div class="col-md-12">
                <div class="card shadow">
                    <div class="card-body table-responsive">
                        <table class="table datatables display" id="data_table">
                            <thead>
                                <tr>
                                    <th>NO</th>
                                    <th>SM</th>
                                    <th>TL</th>
                                    <th>MITRA</th>
                                    <th>TIM / TEKNISI</th>
                                    <th>NAMA PELANGGAN</th>
                                    <th>SC</th>
                                    <th>MYIR</th>
                                    <th>ORDER DATE</th>
                                    <th>ORDER DATE PS</th>
                                    <th>FLAG</th>
                                    <th>NPER</th>
                                    <th>TGL DISPATCH</th>
                                    <th>TGL LAPORAN</th>
                                    <th>STO</th>
                                    <th>INET</th>
                                    <th>VOICE</th>
                                    <th>ORDER STATUS</th>
                                    <th>UT ONLINE</th>
                                    <th>TGL CREATE BA</th>
                                    <th>ALAMAT</th>
                                    <th>LAT PELANGGAN</th>
                                    <th>LONG PELANGGAN</th>
                                    <th>LOC ID</th>
                                    <th>ODP TEKNISI</th>
                                    <th>LAT ODP</th>
                                    <th>LONG ODP</th>
                                    <th>DATEL</th>
                                    <th>STATUS TEKNISI</th>
                                    <th>CATATAN TEKNISI</th>
                                    <th>AGENT ID</th>
                                    <th>JENIS MUTASI</th>
                                    <th>PROVIDER</th>
                                    <th>LAYANAN</th>
                                    <th>TIPE TRANSAKSI</th>
                                    <th>TIPE LAYANAN</th>
                                    <th>GROUP CHANNEL</th>
                                    <th>PRODUCT</th>
                                    <th>FLAG DEPOSIT</th>
                                    <th>NO RFC</th>
                                    <th>TIPE MODEM</th>
                                    <th>SN MODEM</th>
                                    <th>TIPE STB</th>
                                    <th>SN / MAC STB</th>
                                    <th>MODE GELARAN</th>
                                    <th>AD-SC</th>
                                    <th>ROSET</th>
                                    <th>PREKSO-INTRA-15-RS</th>
                                    <th>PREKSO-INTRA-20-RS</th>
                                    <th>DC-ROLL</th>
                                    <th>PRECON-50M</th>
                                    <th>PRECON-75M</th>
                                    <th>PRECON-80M</th>
                                    <th>PRECON-100M</th>
                                    <th>PRECON-150M</th>
                                    <th>TOTAL PRECON</th>
                                    <th>DETAIL PRECON</th>
                                    <th>PATCHCORE</th>
                                    <th>KABEL-UTP</th>
                                    <th>TIANG</th>
                                    <th>JUMLAH TIANG</th>
                                    <th>TRAY-CABLE</th>
                                    <th>BREKET</th>
                                    <th>CLAMP-S</th>
                                    <th>PULL-STRAP</th>
                                    <th>CONNECTOR-RJ45</th>
                                    <th>SOC-ILSINTECH</th>
                                    <th>SOC-SUMITOMO</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>NO</th>
                                    <th>SM</th>
                                    <th>TL</th>
                                    <th>MITRA</th>
                                    <th>TIM / TEKNISI</th>
                                    <th>NAMA PELANGGAN</th>
                                    <th>SC</th>
                                    <th>MYIR</th>
                                    <th>ORDER DATE</th>
                                    <th>ORDER DATE PS</th>
                                    <th>FLAG</th>
                                    <th>NPER</th>
                                    <th>TGL DISPATCH</th>
                                    <th>TGL LAPORAN</th>
                                    <th>STO</th>
                                    <th>INET</th>
                                    <th>VOICE</th>
                                    <th>ORDER STATUS</th>
                                    <th>UT ONLINE</th>
                                    <th>TGL CREATE BA</th>
                                    <th>ALAMAT</th>
                                    <th>LAT PELANGGAN</th>
                                    <th>LONG PELANGGAN</th>
                                    <th>LOC ID</th>
                                    <th>ODP TEKNISI</th>
                                    <th>LAT ODP</th>
                                    <th>LONG ODP</th>
                                    <th>DATEL</th>
                                    <th>STATUS TEKNISI</th>
                                    <th>CATATAN TEKNISI</th>
                                    <th>AGENT ID</th>
                                    <th>JENIS MUTASI</th>
                                    <th>PROVIDER</th>
                                    <th>LAYANAN</th>
                                    <th>TIPE TRANSAKSI</th>
                                    <th>TIPE LAYANAN</th>
                                    <th>GROUP CHANNEL</th>
                                    <th>PRODUCT</th>
                                    <th>FLAG DEPOSIT</th>
                                    <th>NO RFC</th>
                                    <th>TIPE MODEM</th>
                                    <th>SN MODEM</th>
                                    <th>TIPE STB</th>
                                    <th>SN / MAC STB</th>
                                    <th>MODE GELARAN</th>
                                    <th>AD-SC</th>
                                    <th>ROSET</th>
                                    <th>PREKSO-INTRA-15-RS</th>
                                    <th>PREKSO-INTRA-20-RS</th>
                                    <th>DC-ROLL</th>
                                    <th>PRECON-50M</th>
                                    <th>PRECON-75M</th>
                                    <th>PRECON-80M</th>
                                    <th>PRECON-100M</th>
                                    <th>PRECON-150M</th>
                                    <th>TOTAL PRECON</th>
                                    <th>DETAIL PRECON</th>
                                    <th>PATCHCORE</th>
                                    <th>KABEL-UTP</th>
                                    <th>TIANG</th>
                                    <th>JUMLAH TIANG</th>
                                    <th>TRAY-CABLE</th>
                                    <th>BREKET</th>
                                    <th>CLAMP-S</th>
                                    <th>PULL-STRAP</th>
                                    <th>CONNECTOR-RJ45</th>
                                    <th>SOC-ILSINTECH</th>
                                    <th>SOC-SUMITOMO</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</div>
@endsection
@section('footerS')
<script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.0.1/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.print.min.js"></script>
<script type = "text/javascript">
    $(document).ready(function () {
        var source = {!! json_encode($source) !!}
        var mitra = {!! json_encode($mitra) !!}
        var status = {!! json_encode($status) !!}
        var startDate = {!! json_encode($startDate) !!}
        var endDate = {!! json_encode($endDate) !!}

        console.log(source, mitra, status, startDate, endDate)
        
        $('#data_table').DataTable({
            ajax: `/get_ajx/rekonProvisioning?source=${source}&mitra=${mitra}&status=${status}&startDate=${startDate}&endDate=${endDate}`,
            select: true,
            dom: 'Blfrtip',
            lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
            buttons: [
                {
                    extend: 'excel',
                    text: 'Export to Excel',
                    title: 'REKON MATERIAL PROVISIONING MITRA - PROMISE'
                }
            ]
        });
    });
</script>
@endsection