@extends('layout')
@section('title', 'Rekon Insert Tiang Provisioning Mitra')
@section('headerS')
<style>
	th, td {
		text-align: center;
		vertical-align: middle;
	}

    .table thead th {
        text-align: center;
		vertical-align: middle;
        color: #6c757d;
    }

    th {
        color: #6c757d;
    }
</style>
{{-- <link rel="stylesheet" href="/css/dataTables.bootstrap4.css"> --}}
<link href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="https://cdn.datatables.net/buttons/2.0.1/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
@endsection
@section('content')
@if (Session::has('alerts'))
  @foreach(Session::get('alerts') as $alert)
    <div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
  @endforeach
@endif
<div class="container-fluid">
    <div class="row justify-content-center">
    <div class="col-12">
        <h2 class="mb-2 page-title">REKON INSERT TIANG MITRA {{ $mitra }} PERIODE {{ $start }} S/D {{ $end }}</h2>
        <div class="row my-4">
            <div class="col-md-12">
                <div class="card shadow">
                    <div class="card-body table-responsive">
                        <table class="table datatables display" id="data_table">
                            <thead>
                                <tr>
                                    <th>NO</th>
                                    <th>MITRA</th>
                                    <th>TIM</th>
                                    <th>VERIFY</th>
                                    <th>ORDER_FROM</th>
                                    <th>NO_TIKET</th>
                                    <th>ORDER_ID</th>
                                    <th>NAMA_PELANGGAN</th>
                                    <th>INTERNET</th>
                                    <th>ORDER_STATUS</th>
                                    <th>ORDER_DATE</th>
                                    <th>ORDER_DATE_PS</th>
                                    <th>ALAMAT_PELANGGAN</th>
                                    <th>TRANSAKSI</th>
                                    <th>LAYANAN</th>
                                    <th>JML_TIANG</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>NO</th>
                                    <th>MITRA</th>
                                    <th>TIM</th>
                                    <th>VERIFY</th>
                                    <th>ORDER_FROM</th>
                                    <th>NO_TIKET</th>
                                    <th>ORDER_ID</th>
                                    <th>NAMA_PELANGGAN</th>
                                    <th>INTERNET</th>
                                    <th>ORDER_STATUS</th>
                                    <th>ORDER_DATE</th>
                                    <th>ORDER_DATE_PS</th>
                                    <th>ALAMAT_PELANGGAN</th>
                                    <th>TRANSAKSI</th>
                                    <th>LAYANAN</th>
                                    <th>JML_TIANG</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</div>
@endsection
@section('footerS')
<script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.0.1/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.print.min.js"></script>
<script type = "text/javascript">
    $(document).ready(function () {

        var mitra = {!! json_encode($mitra) !!}
        var startDate = {!! json_encode($start) !!}
        var endDate = {!! json_encode($end) !!}

        console.log(mitra, startDate, endDate)

        var link = `/get_ajx/rekonTiang?mitra=${mitra}&startDate=${startDate}&endDate=${endDate}`;
        
        $('#data_table').DataTable({
                ajax: link,
                select: true,
                dom: 'Blfrtip',
                lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
                buttons: [
                    {
                        extend: 'excel',
                        text: 'Export to Excel',
                        title: 'REKON INSERT TIANG PROVISIONING MITRA - PROMISE'
                    }
                ]
        });
    });
</script>
@endsection