@extends('layout')
@section('title', 'Input Surat Kesanggupan')
@section('headerS')
<!-- Date Range Picker CSS -->
<link rel="stylesheet" href="/css/daterangepicker.css">
<link rel="stylesheet" href="/css/jquery.timepicker.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" rel="stylesheet" />

@endsection
@section('content')
@if (Session::has('alerts'))
	@foreach(Session::get('alerts') as $alert)
		<div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
	@endforeach
@endif
<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-md-12"> <h2 class="page-title">{{ $data->step_id == 5 ? 'Data Surat Penetapan' : 'Data Surat Kesanggupan' }}</h2>
			<p class="text-muted">Untuk menghasilkan surat kesanggupan</p>
			<div class="card-deck" style="display: block">
        <form id="input_pra" class="row" method="post" enctype="multipart/form-data">
          {{ csrf_field() }}
          <div class="col-md-6">
            <div class="card shadow mb-4">
              <div class="card-header">
                <strong class="card-title">{{ $data->judul ?? '' }}</strong>
              </div>
              <div class="card-body">
                <div class="form-group row">
                  <label class="col-form-label col-md-3 pull-right" for="sp">Nomor SP</label>
                  <div class="col-md-9">
                    <p class="form-control input-transparent" readonly>{{ $data->surat_penetapan }}</p>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-form-label col-md-3 pull-right" for="tngl_s_pen">Tanggal SP</label>
                  <div class="col-md-9">
                    <p class="form-control input-transparent" readonly>{{ $data->tgl_s_pen }}</p>
                  </div>
                </div>
                <div class="row">
                  <div class="form-group col-md-6">
                    <label class="col-form-label">Due Date:</label>
                    <p class="form-control input-transparent" readonly>{{ date('Y-m-d', strtotime('+'. @$data->tgl_jatuh_tmp. ' day', strtotime(@$data->tgl_s_pen))) }}</p>
                  </div>
                  <div class="form-group col-md-6">
                    <label class="col-form-label">Durasi:</label>
                    <p class="form-control input-transparent" readonly>{{ $data->tgl_jatuh_tmp }} Hari</p>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-form-label col-md-3 pull-right" for="total_plan">Total BOQ Plan</label>
                  <div class="col-md-9">
                    <p class="form-control input-transparent" readonly>{{ number_format($data->gd_sp, 0, '', '.') }}</p>
                  </div>
                </div>
                <div class="form-group row border-top py-3">
                  <label class="col-form-label col-md-3 pull-right" for="pks">PKS</label>
                  <div class="col-md-9">
                    <input type="text" class="form-control" name="pks" required id="pks" value="{{ $data->pks }}">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-form-label col-md-3 pull-right" for="tgl_pks">Tanggal PKS</label>
                  <div class="col-md-9">
                    <input type="text" id="tgl_pks" name="tgl_pks" class="form-control drgpicker" required value="{{ $data->tgl_pks }}">
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-md-12">
                    <a type="button" href="/download/full_rar/{{ @$data->id }}" class="btn btn-block btn-primary"><i class="fe fe-download fe-16"></i> Pekerjaan</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="card shadow mb-4">
              <div class="card-header">
                <strong class="card-title">Pengisian Data Kesanggupan</strong>
                <p class="text-muted">{{ @$get_mitra->nama_company }}</p>
              </div>
              <div class="card-body">
                <div class="form-group row">
                  <label class="col-form-label col-md-4 pull-right" for="surat_kesanggupan">No. Kesanggupan</label>
                  <div class="col-md-8">
                    <input type="text" class="form-control input-transparent" name="surat_kesanggupan" id="surat_kesanggupan" value="{{ @$data->surat_kesanggupan ?? '' }}">
											<code>*Contoh No Kesanggupan: 123.a/PT.MITRA/BJB-BJM/(VIII atau 8)(/ atau . atau -)2021</code>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-form-label col-md-4 pull-right" for="tgl_sggp">Tanggal Kesanggupan</label>
                  <div class="col-md-8">
                    <select class="form-control input-transparent" name="tgl_sggp" id="tgl_sggp">
                      <option value="{{ date('Y-m-d', strtotime("+1 days", strtotime($data->tgl_s_pen))) }}">{{ date('Y-m-d', strtotime("+1 days", strtotime($data->tgl_s_pen))) }} (H +1 Dari Tanggal Penetapan)</option>
                      <option value="{{ date('Y-m-d', strtotime($data->tgl_s_pen)) }}">{{ date('Y-m-d', strtotime($data->tgl_s_pen)) }} (Tanggal Penetapan)</option>
                    </select>
                  </div>
                </div>
                @if (@$data->step_id == 15 || !empty(@$data->detail_reject))
                <div class="form-group row">
                  <label class="col-form-label col-md-5 pull-right" for="up_kesanggupan">Alasan Reject</label>
                  <div class="col-md-7">
                    <textarea readonly style="resize: none" rows="2" class="form-control input-transparent">{{ @$data->detail_reject }}</textarea>
                  </div>
                </div>
                @endif
              </div>
            </div>
          </div>
          <input type="hidden" name="jnis_btn" id="jnis_btn">
          <div class="col-md-12">
						<div class="card-body row">
							<div class="form-group col-md-6">
								<button type="submit" data-step_id ="{{ $data->step_id }}" class="btn btn-primary btn-block submit"><i class="fe fe-check fe-16"></i>&nbsp;Submit Kesanggupan</button>
							</div>
							<div class="form-group col-md-6">
                <button type="submit" class="btn btn-block btn-info save_me"><i class="fe fe-save fe-16"></i>&nbsp;Save</button>
							</div>
						</div>
					</div>
        </form>
			</div>
		</div>
	</div>
</div>
@endsection
@section('footerS')
<script src="/js/moment.min.js"></script>
<script src='/js/jquery.timepicker.js'></script>
<script src='/js/daterangepicker.js'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/jquery.inputmask.bundle.js"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script type="text/javascript">
$(function(){
  $('.drgpicker').daterangepicker({
    singleDatePicker: true,
    timePicker: false,
    showDropdowns: true,
    locale:
    {
      format: 'YYYY-MM-DD'
    }
  });

  $(".submit").click( function(e){
    $('#jnis_btn').val('Submit');
    if($(this).data('step_id') == 15){
      e.preventDefault();
      let timerInterval
      swal.fire({
      showClass: {
        popup: 'animate__animated animate__fadeInDown'
      },
      hideClass: {
        popup: 'animate__animated animate__fadeOutUp'
      },
      title: '<strong>Pastikan Data Diperiksa sebelum di<i>Submit</i></strong>',
      timer: 4000,
      icon: 'question',
      html:
        'Pekerjaan terdeteksi pernah mengalami kesalahan, pastikan untuk periksa ulang!',
      showCancelButton: true,
      focusConfirm: false,
      confirmButtonText:'<i class="fe fe-check fe-16"></i> Aman',
      confirmButtonColor: '#3085d6',
      cancelButtonText:'<i class="fe fe-alert-triangle fe-16"></i> Ada Kesalahan',
      cancelButtonColor: '#d33',
      allowOutsideClick: false,
      allowEscapeKey: false,
      didOpen: () => {
        timerInterval = setInterval(() => {
          const left = (Swal.getTimerLeft() / 1000).toFixed(0)
          Swal.getConfirmButton().querySelector('i').textContent = left
          Swal.getConfirmButton().disabled = true;

          if(left == 0){
            Swal.stopTimer()
            clearInterval(timerInterval)
            Swal.getConfirmButton().querySelector('i').textContent = ''
            Swal.getConfirmButton().disabled = false;
          }
        }, 100)
      },
      willClose: () => {
        clearInterval(timerInterval)
      }
    }).then((result) => {
      if (result.isConfirmed) {
        $('#input_pra').submit();
        Swal.fire('Disubmit!', '', 'success')
      }
    })
    }
  });

  $(".save_me").click( function(){
    $('#jnis_btn').val('Save');
  });

})
</script>
@endsection