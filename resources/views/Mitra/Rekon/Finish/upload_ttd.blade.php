@extends('layout')
@section('title', 'Upload TTD')
@section('headerS')
<link rel="stylesheet" href="/css/dataTables.bootstrap4.css">
<style type="text/css">
  .pull-right {
    text-align: right;
  }
</style>
@endsection
@section('content')
@if (Session::has('alerts'))
@foreach(Session::get('alerts') as $alert) <div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>@endforeach
@endif
<div class="container-fluid">
  <div class="row justify-content-center">
    <div class="col-md-12">
      <h2 class="page-title">Upload Tanda Tangan</h2>
      <div class="card-deck" style="display: block">
        <form class="row" method="post" enctype="multipart/form-data">
          {{ csrf_field() }}
          <div class="col-md-12">
            <div class="card shadow mb-4">
              <div class="card-body">
                <div class="form-group row">
                  <label class="col-form-label col-md-2 pull-right" for="judul">Nama Pekerjaan</label>
                  <div class="col-md-10">
                    <input type="text" class="form-control input-transparent" name="judul" id="judul" value="{{ $data_sp->judul ?? '' }}" disabled>
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-md-12 offset-md-2">
                    @if($data_sp->pekerjaan <> "PSB")
                      <a href="/download/full_rar/{{ $data_sp->id }}" id="download" class="btn btn-info" style="color: #fff">
                      <i class="fe fe-download fe-16"></i>&nbsp;Generate Doc. Tagihan </a>
                    @endif
                    @if($data_sp->pekerjaan == "PSB")
                      @if ($data_sp->flag_dok_psb == 'beta_ho_v1')
                      <input type="hidden" name="flag" value="beta_ho_v1">
                      <a href="/download/full_zip_psb/beta_ho_v1/{{ $data_sp->id }}" id="download" class="btn btn-info" style="color: #fff">
                        <i class="fe fe-download fe-16"></i>&nbsp; Generate Doc. Tagihan
                      </a>
                      @elseif ($data_sp->flag_dok_psb == 'beta_ho_v2_manual')
                      <input type="hidden" name="flag" value="beta_ho_v2_manual">
                      <a href="/download/full_zip_psb/beta_ho_v2_manual/{{ $data_sp->id }}" id="download" class="btn btn-info" style="color: #fff">
                        <i class="fe fe-download fe-16"></i>&nbsp; Generate Doc. Tagihan
                      </a>
                      @elseif ($data_sp->flag_dok_psb == 'beta_kalimantan')
                      <input type="hidden" name="flag" value="beta_kalimantan">
                      <a href="/download/full_zip_psb/beta_kalimantan/{{ $data_sp->id }}" id="download" class="btn btn-info" style="color: #fff">
                        <i class="fe fe-download fe-16"></i>&nbsp; Generate Doc. Tagihan
                      </a>
                      @else
                      <a href="/download/full_zip_psb/khs_2021/{{ $data_sp->id }}" id="download" class="btn btn-info" style="color: #fff">
                        <i class="fe fe-download fe-16"></i>&nbsp; Generate Doc. Tagihan
                      </a>
                      @endif
                    @endif
                  </div>
                </div>
                @if($data_sp->gd_rekon >=100000000 && $data_sp->toc > 30)
                  <div class="form-group row">
                    <label class="col-form-label col-md-2 pull-right" for="jaminan_pelaksana">Jaminan Pelaksanaan</label>
                    <div class="col-md-10">
                      <input type="file" id="jaminan_pelaksana" name="jaminan_pelaksana" class="form-control-file">
                    </div>
                  </div>
				        @endif
				        <div class="form-group row">
                  <label class="col-form-label col-md-2 pull-right" for="status_step_id">Status Pekerjaan:</label>
                  <div class="col-md-10">
                    <select class="form-control" id="status_step_id" name="status_step_id">
                    @if (in_array(session('auth')->proc_level, [2, 4, 99, 44]) )
                      @if ($data_sp->flag_dok_psb != 'beta')
                      <option value=29>Proses SCAN</option>
                      @endif
                      <option value=30>Proses TTD</option>
                    @endif
                    @if (in_array(session('auth')->proc_level, [1, 4, 99, 44]) )
                      <option value=17>Revisi</option>
                      <option value=20>OK Lanjut Pengiriman Berkas</option>
                    @endif
                    </select>
                  </div>
                </div>
                <div class="form-group row no_resi_send" style="display: none;">
                  <label class="col-form-label col-md-2 pull-right" for='no_resi_send'>No Resi Pengiriman:</label>
                  <div class="col-md-10">
                    <input name="no_resi_send" id="no_resi_send" class="form-control input-transparent">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-form-label col-md-2 pull-right" for="keterangan_stts">Keterangan:</label>
                  <div class="col-md-10">
                    <textarea id="keterangan_stts" name="keterangan_stts" class="form-control" style="resize: none;" rows="4" required>{{ $data_sp->detail }}</textarea>
                  </div>
                </div>
                <div class="form-group row pdf_upload" style="display: none;">
                  <label class="col-form-label col-md-2 pull-right" for="upload_ttd">Upload SCAN</label>
                  <div class="col-md-10">
                    <input type="file" id="upload_ttd" name="upload_ttd" class="form-control-file">
                  </div>
                </div>
                <div class="form-group mb-3">
                  <div class="custom-file">
                    <button type='submit' class="btn btn-block btn-primary">
                      <i class="fe fe-check fe-16"></i>&nbsp;Submit </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  @endsection
  @section('footerS')
  <script src='/js/jquery.dataTables.min.js'></script>
  <script src='/js/dataTables.bootstrap4.min.js'></script>
  <script type="text/javascript">
    $(function() {
      $('#status_step_id').on('change', function() {
        var isi = $(this).val();
        if (isi == 20) {
          $('.pdf_upload').show();
          $('#upload_ttd, #no_resi_send').attr('required', true);
          $('.no_resi_send').css({
            display: 'flex'
          });
        } else {
          $('.pdf_upload').hide();
          $('#upload_ttd, #no_resi_send').removeAttr('required');
          $('.no_resi_send').hide();
        }
      });

      if ($('#status_step_id').val() == 20) {
        $('.pdf_upload').show();
        $('#upload_ttd, #no_resi_send').attr('required', true);
        $('.no_resi_send').css({
          display: 'flex'
        });
      }
    })
  </script>
  @endsection