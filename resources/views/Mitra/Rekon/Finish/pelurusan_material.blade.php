@extends('layout')
@section('title', 'Pelurusan Material')
@section('headerS')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="/css/dataTables.bootstrap4.css">
<style type="text/css">
	.pull-right {
		text-align: right;
	}

	th {
		text-align: center;
	}
</style>
@endsection
@section('content')
@if (Session::has('alerts'))
  @foreach(Session::get('alerts') as $alert)
    <div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
  @endforeach
@endif
<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-12">
			<h2 class="page-title">List Pekerjaan Input RFC</h2>
			<div class="row">
				<div class="col-md-12">
					<div class="card shadow mb-4">
						<div class="card-body table-responsive">
							<ul class="nav nav-tabs mb-3" id="myTab" role="tablist">
                <li class="active"><a href="#data_ready" id="data1" class="nav-link active" data-toggle="tab">Pelurusan RFC&nbsp;{!! count($data_ready) != 0 ? "<span class='badge badge-primary'>".count($data_ready)."</span>" : '' !!}</a></li>
              </ul>
							<div id="myTabContentLeft" class="tab-content">
								<div class="tab-pane fade active show" id="data_ready">
									<table id ="tb_rekon" class="table table-striped table-bordered table-hover">
										<thead class="thead-dark">
											<tr>
												<th class="hidden-xs">#</th>
												<th class="no-sort">PID</th>
												<th>Jenis Pekerjaan	</th>
												<th>Nama Pekerjaan	</th>
												<th>Tanggal Request PID</th>
												<th>Umur (Hari)</th>
												<th class="no-sort">User Request PID</th>
												<th>Keterangan Terakhir</th>
												<th>Update Terakhir</th>
												<th class="no-sort">Action</th>
											</tr>
										</thead>
										<tbody>
											@php
												$num = 1;
											@endphp
											@forelse($data_ready as $d)
												<tr>
													<td class="hidden-xs">{{ $num++ }}</td>
													<td>{{ $d->id_project }}</td>
													<td>{{ $d->jenis_work }}</td>
													<td>{{ strtoupper($d->judul) }}</td>
													<td>{{ $d->created_at }}</td>
													<td>{{ $d->jml_hri }}</td>
													<td>{{ $d->created_by }}</td>
													<td>{{ $d->detail }}</td>
													<td>{{ $d->nama_modif ? $d->nama_modif .' ('. $d->modified_by .')' : '' }}</td>
													<td>
														<a type="button" href="/download/full_rar/{{ $d->id }}" class="btn btn-sm btn-primary" style="color: #fff; margin-bottom: 5px;"><i class="fe fe-download fe-16"></i>&nbsp;File</a>
														<a type="button" href="/progress/{{ $d->id }}" class="btn btn-sm btn-info" style="color: #fff; margin-bottom: 5px;"><i class="fe fe-edit-2 fe-16"></i>&nbsp;RFC</a>
														<a type="button" class="btn btn-sm btn-modal_me btn-warning" href="/rollback/{{ $d->id }}" style="margin-bottom: 5px;"><i class="fe fe-corner-up-left fe-16"></i>&nbsp;Rollback </a>
														<p></p>
													</td>
												</tr>
											@empty
											@endforelse
										</tbody>
									</table>
								</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('footerS')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src='/js/jquery.dataTables.min.js'></script>
<script src='/js/dataTables.bootstrap4.min.js'></script>
<script type="text/javascript">
	$(function(){
		$('.btn-modal_me').on('click', function () {
			$('#id_pid_req').val($(this).attr('data-pid_req'));
			$('#pid_mod').val($(this).attr('data-pid'));
		});

		$('#tb_rekon').DataTable({
			autoWidth: true,
			columnDefs: [
				{
					targets: 'no-sort',
					orderable: false
				}
			],
			lengthMenu: [
				[16, 32, 64, -1],
				[16, 32, 64, "All"]
			]
		});
	});
</script>
@endsection