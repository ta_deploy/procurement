@extends('layout')
@section('title', $title)
@section('style')
@endsection
@section('headerS')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="/css/daterangepicker.css">

@endsection
@section('content')
@if (Session::has('alerts'))
  @foreach(Session::get('alerts') as $alert)
    <div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
  @endforeach
@endif
<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-md-12"> <h2 class="page-title">{{ $sub_title }}</h2>
			<p class="text-muted">Untuk Menghasilkan Excel Yang Diperlukan</p>
			<div class="card-deck" style="display: block">
				<form id="input_nomor_harga" class="row" method="post" enctype="multipart/form-data">
					{{ csrf_field() }}
          <input type="hidden" name="pekerjaan" value="{{ $data->pekerjaan }}">
          @if($data->pekerjaan <> "PSB")
          <div class="col-md-12">
            <div class="form-group">
              <div class="col-md-12">
                <a class="btn btn-primary btn-block" style="color: #fff" target="_blank" href="/get_detail_laporan/{{$data->id}}"><i class="fe fe-eye fe-16"></i> Detail</a>
              </div>
            </div>
          </div>
          @endif
					<div class="col-md-12">
						<div class="card shadow mb-4">
							<div class="card-header">
								<strong class="card-title">Nomor Surat & Tanggal</strong><br/><code>Jika nomor tidak ada, maka gunakan <i>draft</i> atau sejenisnya</code>
							</div>
							<div class="card-body">
                <div class="form-group row">
                    <label class="col-form-label col-md-2" for="no_invoice">Nomor Invoice</label>
                    <div class="col-md-10">
                      <input type="text" class="form-control input-transparent" name="no_invoice" id="no_invoice" value="{{ Request::old('no_invoice') ?? @$data->invoice }}">
											<code>*Contoh No Invoice: 123.a/PT.MITRA/BJB-BJM/VIII(/ atau . atau -)2021</code>
                      <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="chck_rmv_invoice" name="chck_rmv_invoice" value="rmv_invoice">
                        <label class="custom-control-label" for="chck_rmv_invoice">Jangan Pakai Karakter Spesial Untuk Nomor Invoice</label>
                      </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-md-2" for="spp_num">Nomor SPP</label>
                    <div class="col-md-10">
                        <input type="text" id="spp_num" name="spp_num" class="form-control input-transparent" value="{{ Request::old('spp_num') ?? @$data_osp->spp_num }}">
											  <code>*Contoh No SPP: 123.a/PT.MITRA/BJB-BJM/VIII(/ atau . atau -)2021</code>
                        <div class="custom-control custom-checkbox">
                          <input type="checkbox" class="custom-control-input" id="chck_rmv_spp" name="chck_rmv_spp" value="rmv_spp">
                          <label class="custom-control-label" for="chck_rmv_spp">Jangan Pakai Karakter Spesial Untuk Nomor SPP</label>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-md-2" for="receipt_num">Nomor Kwitansi</label>
                    <div class="col-md-10">
                        <input type="text" id="receipt_num" name="receipt_num" class="form-control input-transparent" value="{{ Request::old('receipt_num') ?? @$data_osp->receipt_num }}">
											  <code>*Contoh No Kwitansi: 123.a/PT.MITRA/BJB-BJM/VIII(/ atau . atau -)2021</code>
                        <div class="custom-control custom-checkbox">
                          <input type="checkbox" class="custom-control-input" id="chck_rmv_kwitansi" name="chck_rmv_kwitansi" value="rmv_kwitansi">
                          <label class="custom-control-label" for="chck_rmv_kwitansi">Jangan Pakai Karakter Spesial Untuk Nomor Kwitansi</label>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                  <label class="col-form-label col-md-2" for="no_faktur">Nomor Seri Faktur Pajak</label>
                  <div class="col-md-10">
                    <input type="text" class="form-control input-transparent" name="no_faktur" id="no_faktur" value="{{ Request::old('no_faktur') ?? @$data->faktur }}">
                    {{-- <code>*Contoh No Faktur: 123.a/PT.MITRA/BJB-BJM/VIII(/ atau . atau -)2021</code>
                    <div class="custom-control custom-checkbox">
                      <input type="checkbox" class="custom-control-input" id="chck_rmv_faktur" name="chck_rmv_faktur" value="rmv_faktur">
                      <label class="custom-control-label" for="chck_rmv_faktur">Jangan Pakai Karakter Spesial Untuk Nomor Faktur</label>
                    </div> --}}
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-form-label col-md-2" for="tgl_faktur">Tanggal Surat</label>
                  <div class="col-md-10">
                    <input type="text" class="form-control date-picker" name="tgl_faktur" id="tgl_faktur" value="{{ (Request::old('tgl_faktur') ? Request::old('tgl_faktur') : ( @$data->tgl_faktur ?? date('Y-m-d') ) ) }}" aria-describedby="button-addon2">
                  </div>
                </div>
							</div>
						</div>
					</div>
          {{-- @if($data->pekerjaan == "PSB")
          <div class="col-md-6">
            <div class="card shadow mb-4">
              <div class="card-header">
                <strong class="card-title">Lampiran Dokumen Pendukung (PSB)</strong>
              </div>
              <div class="card-body">
                @foreach ($lampiran_dokumen as $key => $input)
                @php
                  $paths = public_path()."/upload/{$id}/";
                  $file = @preg_grep('~^'.$key.'.*$~', scandir($paths));
                  $path = null;
                @endphp
                <div class="form-group row">
                  <label class="col-form-label pull-right col-md-5" for="{{ $key }}">{{ $input }}</label>
                  <div class="col-md-7">
                    @if (count($file) != 0)
                    @php
                      $files = array_values($file);
                      $path = public_path() . "/upload2/" . $id . "/" . $files[0];
                    @endphp
                      <a href="{{ $path }}">
                        <h5><span class="badge badge-pill badge-info"><i class="fe fe-download"></i>&nbsp; {{ $input }}</span></h5>
                      </a>
                    @endif
                      <input type="file" id="{{ $key }}" name="{{ $key }}" title="{{ $input }}" class="form-control-file">
                  </div>
                </div>
                @endforeach
              </div>
            </div>
					</div>

          <div class="col-md-6 form-group table-responsive">
            <div class="card shadow mb-4">
              <div class="card-header">
                <strong class="card-title">Berita Pemotongan Material (PSB)</strong>
              </div>
              <div class="card-body">
                <div class="form-group row">
                    <label class="col-form-label col-md-2" for="uraian_npk">Uraian NPK</label>
                    <div class="col-md-10">
                        <input type="text" id="uraian_npk" name="uraian_npk" class="form-control input-transparent" value="{{ Request::old('uraian_npk') ?? $data->uraian_npk }}" placeholder="Tidak Bisa Mengembaikan Material Karena Hilang">
                    </div>
                </div>
                <table class="table table-hover table-bordered table-sm tableData" style="text-align: center;">
											<thead style="background-color: yellow;">
                        <tr>
                          <th style="color: black; font-weight: bold;">MATERIALS</th>
                          <th style="color: black; font-weight: bold;">HSS JASA</th>
                          <th style="color: black; font-weight: bold;">Jumlah SSL</th>
                        </tr>
											</thead>
											<tbody>
                        <tr>
                          <td>RS-IN-SC-1</td>
                          <td>Rp.
                            <input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->rs_in_sc_1)) }}" style="text-align: center;" name="rs_in_sc_1_hss" readonly>
                          </td>
                          <td>
                            <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" style="text-align: center;" name="rs_in_sc_1_ssl" value="{{ $data->rs_in_sc_1_ssl ? : '0' }}" required>
                          </td>
                        </tr>
                        <tr>
                          <td>S-CLAMP-SPRINER</td>
                          <td>Rp.
                            <input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->s_clamp_spriner)) }}" style="text-align: center;" name="s_clamp_spriner_hss" readonly>
                          </td>
                          <td>
                            <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" style="text-align: center;" name="s_clamp_spriner_ssl" value="{{ $data->s_clamp_spriner_ssl ? : '0' }}" required>
                          </td>
                        </tr>
                        <tr>
                          <td>BREKET</td>
                          <td>Rp.
                            <input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->breket)) }}" style="text-align: center;" name="breket_hss" readonly>
                          </td>
                          <td>
                            <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" style="text-align: center;" name="breket_ssl" value="{{ $data->breket_ssl ? : '0' }}" required>
                          </td>
                        </tr>
                        <tr>
                          <td>UTP-C6</td>
                          <td>Rp.
                            <input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->utp_c6)) }}" style="text-align: center;" name="utp_c6_hss" readonly>
                          </td>
                          <td>
                            <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" style="text-align: center;" name="utp_c6_ssl" value="{{ $data->utp_c6_ssl ? : '0' }}" required>
                          </td>
                        </tr>
                        <tr>
                          <td>PRECON-50</td>
                          <td>Rp.
                            <input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->precon_50)) }}" style="text-align: center;" name="precon_50_hss" readonly>
                          </td>
                          <td>
                            <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" style="text-align: center;" name="precon_50_ssl" value="{{ $data->precon_50_ssl ? : '0' }}" required>
                          </td>
                        </tr>
                        <tr>
                          <td>PRECON-80</td>
                          <td>Rp.
                            <input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->precon_80)) }}" style="text-align: center;" name="precon_80_hss" readonly>
                          </td>
                          <td>
                            <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" style="text-align: center;" name="precon_80_ssl" value="{{ $data->precon_80_ssl ? : '0' }}" required>
                          </td>
                        </tr>
                        <tr>
                          <td>PRECON-100</td>
                          <td>Rp.
                            <input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->precon_100)) }}" style="text-align: center;" name="precon_100_hss" readonly>
                          </td>
                          <td>
                            <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" style="text-align: center;" name="precon_100_ssl" value="{{ $data->precon_100_ssl ? : '0' }}" required>
                          </td>
                        </tr>
                        <tr>
                          <td>PRECON-150</td>
                          <td>Rp.
                            <input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->precon_150)) }}" style="text-align: center;" name="precon_150_hss" readonly>
                          </td>
                          <td>
                            <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" style="text-align: center;" name="precon_150_ssl" value="{{ $data->precon_150_ssl ? : '0' }}" required>
                          </td>
                        </tr>
                        <tr>
                          <td>PATCH-CORD-2M</td>
                          <td>Rp.
                            <input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->patch_cord_2m)) }}" style="text-align: center;" name="patch_cord_2m_hss" readonly>
                          </td>
                          <td>
                            <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" style="text-align: center;" name="patch_cord_2m_ssl" value="{{ $data->patch_cord_2m_ssl ? : '0' }}" required>
                          </td>
                        </tr>
                        <tr>
                          <td>RJ-45</td>
                          <td>Rp.
                            <input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->rj_45)) }}" style="text-align: center;" name="rj_45_hss" readonly>
                          </td>
                          <td>
                            <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" style="text-align: center;" name="rj_45_ssl" value="{{ $data->rj_45_ssl ? : '0' }}" required>
                          </td>
                        </tr>
                        <tr>
                          <td>SOC-ILS</td>
                          <td>Rp.
                            <input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->soc_ils)) }}" style="text-align: center;" name="soc_ils_hss" readonly>
                          </td>
                          <td>
                            <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" style="text-align: center;" name="soc_ils_ssl" value="{{ $data->soc_ils_ssl ? : '0' }}" required>
                          </td>
                        </tr>
                        <tr>
                          <td>SOC-SUM</td>
                          <td>Rp.
                            <input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->soc_sum)) }}" style="text-align: center;" name="soc_sum_hss" readonly>
                          </td>
                          <td>
                            <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" style="text-align: center;" name="soc_sum_ssl" value="{{ $data->soc_sum_ssl ? : '0' }}" required>
                          </td>
                        </tr>
                        <tr>
                          <td>PREKSO-INTRA-15-RS</td>
                          <td>Rp.
                            <input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->prekso_intra_15_rs)) }}" style="text-align: center;" name="prekso_intra_15_rs_hss" readonly>
                          </td>
                          <td>
                            <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" style="text-align: center;" name="prekso_intra_15_rs_ssl" value="{{ $data->prekso_intra_15_rs_ssl ? : '0' }}" required>
                          </td>
                        </tr>
                        <tr>
                          <td>PREKSO-INTRA-20-RS</td>
                          <td>Rp.
                            <input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->prekso_intra_20_rs)) }}" style="text-align: center;" name="prekso_intra_20_rs_hss" readonly>
                          </td>
                          <td>
                            <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" style="text-align: center;" name="prekso_intra_20_rs_ssl" value="{{ $data->prekso_intra_20_rs_ssl ? : '0' }}" required>
                          </td>
                        </tr>
                        <tr>
                          <td>PU-S7.0-140 Tiang</td>
                          <td>Rp.
                            <input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->pu_s7_tiang)) }}" style="text-align: center;" name="pu_s7_tiang_hss" readonly>
                          </td>
                          <td>
                            <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" style="text-align: center;" name="pu_s7_tiang_ssl" value="{{ $data->pu_s7_tiang_ssl ? : '0' }}" required>
                          </td>
                        </tr>
                        <tr>
                          <td>OTP-FTTH-1</td>
                          <td>Rp.
                            <input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->otp_ftth_1)) }}" style="text-align: center;" name="otp_ftth_1_hss" readonly>
                          </td>
                          <td>
                            <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" style="text-align: center;" name="otp_ftth_1_ssl" value="{{ $data->otp_ftth_1_ssl ? : '0' }}" required>
                          </td>
                        </tr>
                        <tr>
                          <td>TC-OF-CR-200</td>
                          <td>Rp.
                            <input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->tc_of_cr_200)) }}" style="text-align: center;" name="tc_of_cr_200_hss" readonly>
                          </td>
                          <td>
                            <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" style="text-align: center;" name="tc_of_cr_200_ssl" value="{{ $data->tc_of_cr_200_ssl ? : '0' }}" required>
                          </td>
                        </tr>
                        <tr>
                          <td>AC-OF-SM-1B</td>
                          <td>Rp.
                            <input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->ac_of_sm_1b)) }}" style="text-align: center;" name="ac_of_sm_1b_hss" readonly>
                          </td>
                          <td>
                            <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" style="text-align: center;" name="ac_of_sm_1b_ssl" value="{{ $data->ac_of_sm_1b_ssl ? : '0' }}" required>
                          </td>
                        </tr>
											</tbody>
                </table>
              </div>
            </div>
					</div>
          @endif --}}
          <div class="col-md-12 field_submit">
              <div class="card shadow mb-4">
                  <button class="btn btn-primary" type="submit">Submit</button>
              </div>
          </div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection
@section('footerS')
<script src="/js/moment.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/widgster@1.0.0/widgster.js"></script>
<script src="/js/jquery.timepicker.js"></script>
<script src="/js/daterangepicker.js"></script>
<script src="/js/jquery.dataTables.min.js"></script>
<script src="/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript">

$(function(){
  $('.tableData').DataTable();

  $('.modal_reject').click( function(){
    $('#judul_reject').html($(this).attr('data-judul'));
  });

	$('.date-picker').daterangepicker(
		{
			singleDatePicker: true,
			timePicker: false,
			showDropdowns: true,
			locale:
			{
				format: 'YYYY-MM-DD',
			}
		}
	);

  $('.hss_jasa').keyup(function(event) {

		// skip for arrow keys
			if(event.which >= 37 && event.which <= 40) return;

		// format number
			$(this).val(function(index, value) {
				return value
				.replace(/\D/g, "")
				.replace(/\B(?=(\d{3})+(?!\d))/g, ".")
				;
			});
		});
});
</script>
@endsection