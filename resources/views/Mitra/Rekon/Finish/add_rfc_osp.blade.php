@extends('layout')
@section('title', Request::segment(2) == 'edit_RFC' ? 'Edit RFC' : 'Tambah RFC')
@section('content')
@if (Session::has('alerts'))
  @foreach(Session::get('alerts') as $alert)
    <div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
  @endforeach
@endif
{{-- <div class="modal fade" id="material-modal" tabindex="-1" role="dialog" aria-labelledby="material-modalTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-xl" role="document">
    <div class="modal-content">
			<div class="modal-header">
				<strong style="color:black;">Material Procurement</strong>
				<button type="button" class="btn btn-default btn-xs" style="float:right;" data-dismiss="modal">Close</button>
			</div>
			<div class="modal-body" style="overflow-y:auto; height : 450px">
				<input id="searchinput" class="form-control input-sm" type="search" placeholder="Search..." />
				<ul id="searchlist" class="list-group">
					<li class="list-group-item" v-for="data in list">
						<strong v-text="data.uraian" style="color: white;"></strong>
						<div class="input-group" style="width:150px;float:right;">
							<span class="input-group-btn">
								<button type="button" class="btn btn-default btn-sm" v-on:click="onMinus(data)">
									<span class="glyphicon glyphicon-minus"></span>
								</button>
							</span>

							<input type="tel" v-model="data.qty" v-on:keypress="onlyNumber($event)" style="border-top: 1px solid #eeeeee" class="form-control text-center input-md"/>
							<span class="input-group-btn">
								<button type="button" class="btn btn-default btn-sm" v-on:click="onPlus(data)">
									<span class="glyphicon glyphicon-plus"></span>
								</button>
							</span>
						</div>
						<p v-text="data.uraian" style="font-size:10px; color: white;"></p>
						<p style="font-size:10px; color: white;">Material= <span v-text="mata_uang(data.material)"></span>; Jasa= <span v-text="mata_uang(data.jasa)"></span>; Jumlah= <span v-text="mata_uang(data.jumlah)"></span>;</p>
					</li>
				</ul>
			</div>
			<div class="modal-footer" style="background: #eee; color: black;">
				Limit pemakaian Material diatas sesuai dengan BON di alista.
			</div>
		</div>
  </div>
</div> --}}
<div class="container-fluid">
  <div class="row justify-content-center">
    <div class="col-12">
      <h2 class="page-title">Penambahan RFC Sesuai Lokasi</h2>
      <div class="row">
        <div class="col-md-12">
          <div class="card shadow">
            <div class="card-body">
              <div class="form-group row">
                <label class="col-form-label col-md-3" for="judul">Lokasi</label>
                <div class="col-md-9">
                  <select class="form-control" id ="lokasi" name="lokasi">
                    @forelse ($data as $d)
                      <option value="{{ $d->id }}">{{ $d->lokasi }}</option>
                    @empty

                    @endforelse
                  </select>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-form-label col-md-3" for="nomor_rfc_gi">Nomor RFC / GI</label>
                <div class="col-md-9">
                  <select class="form-control" name="nomor_rfc_gi" id="nomor_rfc_gi" multiple></select>
                </div>
              </div>
							<div class="form-group row">
                <label class="col-form-label col-md-3" for="nomor_rfc_return">Nomor RFC / Pengembalian</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" name="nomor_rfc_return" id="nomor_rfc_return" value="{{ $data_single->nomor_rfc_return ?? '' }}">
                </div>
              </div>
              <div class="form-group row">
                <label class="col-form-label col-md-3" for="rfc">RFC</label>
                <div class="col-md-9">
                  <input type="file" id="rfc" name="rfc" class="form-control">
                </div>
              </div>
							{{-- <div class="form-group row">
                <div class="col-md-1 col-md-offset-5">
                  <button type="button" data-toggle="modal" data-target="#material-modal" class="btn btn-danger search_material"><i class="material-icons" style="color: white;">toc</i>Material</button>
                </div>
                <input type="hidden" name="materials" value="[]" />
              </div>
              <div class="form-group row">
                <ul id="material-list" class="list-group">
                  <li class="list-group-item" v-for="data in filter_data(list)" >
                    <span class="badge" v-text="data.qty" style="color: black;"></span>
                    <strong v-text="data.uraian"></strong>
                    <p v-text="harga(data.jumlah)"></p>
                    <p v-text="qount_total(data.jumlah, data.qty)"></p>
                  </li>
                </ul>
              </div> --}}
              {{-- <div class="form-group row">
                <label class="col-form-label col-md-3" for="total">Total</label>
                <div class="col-md-9">
                  <input type="text" id="total" name="total" v-model="grand_total()" class="form-control" readonly>
                </div>
              </div> --}}
              <div class="form-group">
								<div class="col-md-12">
									<button type="submit" class="btn btn-danger btn-block save_me">Simpan RFC</button>
								</div>
              </div>
            </div>
          </div>
        </div>
      </div> <!-- end section -->
    </div> <!-- .col-12 -->
  </div> <!-- .row -->
</div>
@endsection
@section('footerS')
{{-- <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script> --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script type="text/javascript">
	$(function(){
		$('select').select2();

		var data = {!! json_encode($data_sp) !!};

		console.log(data)

		// var materials = {!!json_encode($material) !!},
		// vueData = {
		// 	list: materials
		// }

		// var modalVm = new Vue({
		// 	el: '#material-modal',
		// 	data: vueData,
		// 	methods: {
		// 		onPlus: function(item) {
		// 			item.qty++;
		// 		},
		// 		onMinus: function(item) {
		// 			if (item.qty <= 0)
		// 				item.qty = 0;
		// 			else
		// 				item.qty--;
		// 		},
		// 		onlyNumber ($event) {
		// 			let keyCode = ($event.keyCode ? $event.keyCode : $event.which);
		// 			if ((keyCode < 48 || keyCode > 57) && keyCode != 8) {
		// 				$event.preventDefault();
		// 			}
		// 		},
		// 		mata_uang(value) {
		// 			var val = value.toFixed(0);
		// 			return val.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		// 		}
		// 	},
		// });

		// new Vue({
		// 	el: '#total',
		// 	methods: {
		// 		grand_total: function(){
		// 			var total = [];
		// 			$.each( materials, function( key, value ) {
		// 				total.push(value.qty * value.jumlah);
		// 			});
		// 			var result= total.reduce(function(total, num){ return total + num }, 0);
		// 			var val = result.toFixed(0);
		// 			return val.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		// 		}
		// 	}
		// })

		// var listVm = new Vue({
		// 	el: '#material-list',
		// 	data: vueData,
		// 	methods: {
		// 		filter_data(item){
		// 			return item.filter(function(a) {
		// 				return a.qty > 0
		// 			});
		// 		},
		// 		harga(harga){
		// 			var val = harga.toFixed(0);
		// 			return 'Harga Per Buah : '+val.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		// 		},
		// 		qount_total(harga,qty){
		// 			var qount = harga * qty;
		// 			var val = qount.toFixed(0);
		// 			return 'Total : '+val.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		// 		}
		// 	}
		// });

		// $("#searchinput").keyup(function() {
		// 	setTimeout(function(){
		// 		var input, filter, ul, li, a, i;
		// 		input = document.getElementById("searchinput");
		// 		filter = input.value.toUpperCase();
		// 		ul = document.getElementById("searchlist");
		// 		li = ul.getElementsByTagName("li");
		// 		for (i = 0; i < li.length; i++) {
		// 			a = li[i].getElementsByTagName("p")[0];
		// 			if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
		// 				li[i].style.display = "";
		// 			} else {
		// 				li[i].style.display = "none";

		// 			}
		// 		}
		// 	}, 2000);
		// },);

		// $('.save_me').click(function(e){
		// 	result = [];
		// 	materials.forEach(function(item) {
		// 		if (item.qty > 0) result.push({id_item: item.id, qty: item.qty});
		// 	});
		// 	$('input[name=materials]').val(JSON.stringify(result));
		// });
	});
</script>
@endsection