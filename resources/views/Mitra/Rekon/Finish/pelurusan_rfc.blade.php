@extends('layout')
@section('title', 'Pelurusan')
@section('headerS')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" rel="stylesheet" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" integrity="sha512-aOG0c6nPNzGk+5zjwyJaoRUgCdOrfSDhmMID2u4+OIslr0GjpLKo7Xm0Ao3xmpM4T8AmIouRkqwj1nrdVsLKEQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/jqueryui-editable/css/jqueryui-editable.css" rel="stylesheet"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.css" integrity="sha512-6S2HWzVFxruDlZxI3sXOZZ4/eJ8AcxkQH1+JjSe/ONCEqR9L4Ysq5JdT5ipqtzU7WHalNwzwBv+iE51gNHJNqQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />

<link rel="stylesheet" href="/css/dataTables.bootstrap4.css">
<style type="text/css">
	.pull-right {
		text-align: right;
	}

	.template_boq{
		font-size: 0.8em!important;
	}

	.field_material_input{
		width: 60px!important;
		height: 32px;
	}

	.select2-search--inline {
    display: contents;
	}

	.select2-search__field:placeholder-shown {
		width: 100% !important;
	}

</style>
@endsection
@section('content')
@if (Session::has('alerts'))
	@foreach(Session::get('alerts') as $alert)
		<div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
	@endforeach
@endif
<div class="modal fade" id="upload_RFC" role="dialog" aria-labelledby="upload_RFCTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="upload_RFCTitle">Upload RFC</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form enctype="multipart/form-data" method="POST" action="/submit_rfc_data">
				{{ csrf_field() }}
				<div class="modal-body">
						<div class="form-group">
							<input type="hidden" name="id_upload" id="id_upload" value="{{ Request::segment(2) }}">
							<input type="hidden" name="no_rfc" id="no_rfc" value="">
						</div>
						<div class="form-group">
							<label for="file_rfc">File</label>
							<input type="file" id="file_rfc" name="file_rfc" class="form-control-file">
						</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn mb-2 btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn mb-2 btn-primary">Save</button>
				</div>
		</form>
		</div>
	</div>
</div>
<div class="modal fade" id="modal_check_rfc" role="dialog" aria-labelledby="modal_check_rfcTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
				<a class="btn btn-info dwn_rfc" style="color: #fff" href="#"><i class="fe fe-download"></i>&nbsp;PDF</a>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body data_mdl" style=" overflow: hidden;">
				<form class="row" method="post">
					{{ csrf_field() }}
					<input type="hidden" name="rfc_id" id="rfc_id">
					<div class="col-md-5">
						<div class="form-group row">
							<label class="col-form-label col-md-4 pull-right" for="wbs_element">PID:</label>
							<div class="col-md-8">
								<p class="form-control input-transparent" readonly><b class="wbs_element"></b></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-md-4 pull-right" for="no_rfc">Nomor RFC:</label>
							<div class="col-md-8">
								<p class="form-control input-transparent" readonly><b class="no_rfc"></b></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-md-4 pull-right" for="plant">Gudang:</label>
							<div class="col-md-8">
								<p class="form-control input-transparent" readonly><b class="plant"></b></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-md-4 pull-right" for="type">Jenis:</label>
							<div class="col-md-8">
								<p class="form-control input-transparent" readonly><b class="type"></b></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-md-4 pull-right" for="mitra">Mitra:</label>
							<div class="col-md-8">
								<textarea class="form-control input-transparent mitra" rows="2" readonly></textarea>
							</div>
						</div>
						<div class="form-group row rfc_lama_field">
							<label class="col-form-label col-md-4 pull-right" for="old_rfc">RFC Lama:</label>
							<div class="col-md-8">
								<textarea class="form-control input-transparent old_rfc" rows="4" style="resize: none;" readonly></textarea>
							</div>
						</div>
						<div class="form-group row rfc_old_field">
							<label class="col-form-label col-md-4 pull-right" for="rfc_old">RFC Lama:</label>
							<div class="col-md-8">
								<select class="form-control" name="rfc_old[]" id="rfc_old" multiple>
									@foreach ($get_belum_lurus as $val)
										<option value="{{ $val->rfc }}">{{ $val->rfc }}</option>
									@endforeach
								</select>
								<code>*Jika Tidak ada, Tidak Perlu Dipilih</code>
							</div>
						</div>
					</div>
					<div class="col-md-7">
						<div class="table_rfc_material">
						</div>
					</div>
					<div class="col-md-12 submit_btn_rfc">
						<div class="form-group mb-3">
							<div class="custom-file">
								<button type="submit" class="btn btn-block btn-primary">Submit Nomor RFC</button>
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
			</div>
    </div>
  </div>
</div>
{{-- <div class="modal fade" id="change_rfc" role="dialog" aria-labelledby="change_rfcTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
				<a class="btn btn-info dwn_rfc" style="color: #fff" href="#"><i class="fe fe-download"></i>&nbsp;PDF</a>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body data_mdl" style=" overflow: hidden;">
				<form class="row" method="post" action="/change_rfc/">
					{{ csrf_field() }}
					<input type="hidden" name="rfc_skg" id="rfc_skg">
					<div class="col-md-4">
						<div class="form-group row">
							<label class="col-form-label col-md-4 pull-right" for="wbs_element">PID:</label>
							<div class="col-md-8">
								<p class="form-control input-transparent" readonly><b class="wbs_element"></b></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-md-4 pull-right" for="no_rfc">Nomor RFC:</label>
							<div class="col-md-8">
								<p class="form-control input-transparent" readonly><b class="no_rfc"></b></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-md-4 pull-right" for="plant">Gudang:</label>
							<div class="col-md-8">
								<p class="form-control input-transparent" readonly><b class="plant"></b></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-md-4 pull-right" for="type">Jenis:</label>
							<div class="col-md-8">
								<p class="form-control input-transparent" readonly><b class="type"></b></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-md-4 pull-right" for="mitra">Mitra:</label>
							<div class="col-md-8">
								<textarea class="form-control input-transparent mitra" rows="2" readonly></textarea>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-md-4 pull-right" for="old_rfc">RFC Lama:</label>
							<div class="col-md-8">
								<p class="form-control input-transparent" readonly><b class="old_rfc"></b></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-md-4 pull-right" for="rfc_new">RFC Pengganti:</label>
							<div class="col-md-8">
								<select class="form-control" name="rfc_new" id="rfc_new">
									@foreach ($get_belum_lurus as $val)
										<option value="{{ $val->rfc }}">{{ $val->rfc }}</option>
									@endforeach
								</select>
							</div>
						</div>
					</div>
					<div class="col-md-8">
						<div class="table_rfc_material">
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group mb-3">
							<div class="custom-file">
								<button type="submit" class="btn btn-block btn-primary">Submit Nomor RFC</button>
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
			</div>
    </div>
  </div>
</div> --}}

<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-md-12">
			<h2 class="page-title">Input RFC Pelurusan</h2>
			<div class="card-deck">
				<div class="card shadow mb-4">
					<div class="card-body">
						<div class="form-group row">
							<label class="col-form-label col-md-2 pull-right">judul</label>
							<div class="col-md-10">
								<textarea class="form-control input-transparent" rows="2" style="resize: none;" disabled>{{ $data_sp->judul }}</textarea>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-12 offset-md-2">
								<a type="button" href="/download/full_rar/{{ $data_sp->id }}" class="btn btn-sm btn-primary" style="color: #fff"><i class="fe fe-download fe-16"></i> File</a>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-md-2 pull-right" for="rfc">RFC</label>
							<div class="col-md-10">
								<select class="form-control" name="rfc" id="rfc"></select>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		@if (count($unclear_rfc))
			<div class="col-md-12">
				<div class="card shadow mb-4">
					<div class="card-header">
						<strong class="card-title">History RFC Tidak Lurus</strong>
					</div>
					<div class="card-body table-responsive">
						<table id ="tb_rekon" class="table table-striped table-bordered table-hover">
							<thead class="thead-dark">
								<tr>
									<th class="hidden-xs">#</th>
									<th>Nomor RFC</th>
									<th>WBS</th>
									<th>Jenis</th>
									<th>Mitra</th>
									<th>Created At</th>
									<th>Created By</th>
									<th>Tanggal Gudang</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								@php
									$no = 0
								@endphp
								@forelse ($unclear_rfc as $key => $val)
									<tr>
										<td>{{ ++$no }}</td>
										<td>{{ $val->no_rfc }}</td>
										<td>{{ $val->wbs_element }}</td>
										<td>{{ ($val->type == 'Out' ? 'Pengeluaran' : 'Pengembalian') }}</td>
										<td>{{ $val->mitra }}</td>
										<td>{{ $val->created_at }}</td>
										<td>{{ $val->nama }} ({{ $val->created_by }})</td>
										<td>{{ $val->posting_datex }}</td>
										<td>
											{!! ($val->download != 'kdd' ? "<a class='btn btn-sm btn-info dwn_rfc' style='color: #fff' href='/download/rfc/$val->id_upload/$val->no_rfc'><i class='fe fe-download'></i>&nbsp;PDF</a>" : "<a class='btn btn-sm btn-warning up_rfc' data-toggle='modal' data-rfc='$val->no_rfc' data-target='#upload_RFC' style='color: #fff'><i class='fe fe-upload'></i>&nbsp;PDF</a>" ) !!}
										</td>
										<td>
											<a class="btn btn-sm btn-info lihat_detail_rfc" data-rfc="{{ $val->no_rfc }}" style="margin-bottom: 5px; color: white;"><i class="fe fe-eye fe-16"></i>&nbsp;Detail</a>
											<a class="btn btn-sm btn-danger delete" data-rfc="{{ $val->no_rfc }}" href="/delete/rfc/pr/{{ $val->id }}" style="color: white; margin-bottom: 5px;"><i class="fe fe-trash fe-16"></i>&nbsp;Hapus RFC</a>
											@if($val->pelurusan != 'ok')
												<a class="btn btn-sm btn-success" data-rfc="{{ $val->no_rfc }}" href="/lurus/rfc/pr/{{ $val->id }}" style="color: white; margin-bottom: 5px;"><i class="fe fe-check fe-16"></i>&nbsp;Lurus RFC</a>
											@endif
										</td>
									</tr>
								@empty
								@endforelse
							</tbody>
						</table>
					</div>
				</div>
			</div>
		@endif
		@if (count($history_rfc))
			<div class="col-md-12">
				@if($count_download != 0)
					<h2 class="page-title"><a class='btn btn-info dwn_rfc' style='color: #fff' href='/download/all_rfc/{{ $data_sp->id }}'><i class='fe fe-download'></i>&nbsp;Semua PDF</a>
					</h2>
				@endif
				<div class="card shadow mb-4">
					<div class="card-header">
						<strong class="card-title">History RFC Lurus</strong>
					</div>
					<div class="card-body table-responsive">
						<table id ="tb_rekon" class="table table-striped table-bordered table-hover">
							<thead class="thead-dark">
								<tr>
									<th class="hidden-xs">#</th>
									<th>Nomor RFC</th>
									<th>RFC Lama</th>
									<th>Jenis</th>
									<th>Mitra</th>
									<th>Created At</th>
									<th>Created By</th>
									<th>Tanggal Gudang</th>
									<th>status</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								@php
									$no = 0
								@endphp
								@forelse ($history_rfc as $key => $val)
									<tr>
										<td>{{ ++$no }}</td>
										<td>{{ $val->no_rfc }}</td>
										<td>{{ $val->rfc_lama }}</td>
										<td>{{ ($val->type == 'Out' ? 'Pengeluaran' : 'Pengembalian') }}</td>
										<td>{{ $val->mitra }}</td>
										<td>{{ $val->created_at }}</td>
										<td>{{ $val->nama }} ({{ $val->created_by }})</td>
										<td>{{ $val->posting_datex }}</td>
										<td>{!! ($val->download != 'kdd' ? "<a class='btn btn-info dwn_rfc' style='color: #fff' href='/download/rfc/$val->id_upload/$val->no_rfc'><i class='fe fe-download'></i>&nbsp;PDF</a>" : "<a class='btn btn-warning up_rfc' data-toggle='modal' data-rfc='$val->no_rfc' data-target='#upload_RFC' style='color: #fff'><i class='fe fe-upload'></i>&nbsp;Upload PDF</a>" ) !!}</td>
										<td>
											<a class="btn btn-sm btn-info lihat_detail_rfc" data-rfc="{{ $val->no_rfc }}" style="margin-bottom: 5px; color: white;"><i class="fe fe-eye fe-16"></i>&nbsp;Lihat Detail</a>
											<a href="/delete_perm/rfc/{{ $val->no_rfc }}" class="btn btn-sm btn-danger" style="margin-bottom: 5px; color: white;"><i class="fe fe-trash fe-16"></i>&nbsp;Hapus RFC</a>
											{{-- <a class="btn btn-sm btn-warning ganti_rfc" data-rfc="{{ $val->no_rfc }}" style="color: white;"><i class="fe fe-edit-2 fe-16"></i>&nbsp;Ganti RFC</a> --}}
										</td>
									</tr>
								@empty
								@endforelse
							</tbody>
						</table>
					</div>
				</div>
			</div>
		@endif
		<div class="col-md-12">
			<h2 class="page-title">Request PID</h2>
			<div class="card-deck" style="display: block">
				<form id="submit_lampiran_boq" class="row" method="post" action="/Mitra/finish_pelurusan_rfc/{{ $data_sp->id }}">
					{{ csrf_field() }}
					<div class="col-md-12">
						<div class="card shadow mb-4">
							<div class="card-body">
								<div class="form-group row">
									<label class="col-form-label col-md-2 pull-right" for="pekerjaan">Pekerjaan</label>
									<div class="col-md-10">
										<select name="pekerjaan" class="form-control input-transparent" id="pekerjaan" required>
											@foreach ($kerjaan[0] as $val)
												<option value="{{ $val->id }}">{{ $val->text }}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-md-2 pull-right" for="jenis_work">Jenis Pekerjaan</label>
									<div class="col-md-5">
										<select name="jenis_work" class="form-control input-transparent" id="jenis_work">
										</select>
									</div>
									<div class="col-md-5">
										<select name="jenis_kontrak" class="form-control input-transparent" id="jenis_kontrak" required>
											<option value="Kontrak Putus">Kontrak Putus</option>
											<option value="Kontrak Tetap">Kontrak Tetap</option>
										</select>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-md-2 pull-right" for="mitra_id">Mitra</label>
									<div class="col-md-10">
										<select id="mitra_id" name="mitra_id" required></select>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-md-2 pull-right" for="judul">Judul</label>
									<div class="col-md-10">
										<textarea type="text" class="form-control input-transparent" style="resize: none" rows="2" name="judul" id="judul" required></textarea>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-md-2 pull-right" for="wbs">WBS</label>
									<div class="col-md-10">
										<textarea type="text" class="form-control input-transparent" style="resize: none" rows="2" name="wbs" id="wbs" required></textarea>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-md-2 pull-right">Pengerjaan</label>
									<div class="col-md-5">
										<select id="tahun" name="tahun" required>
											@for ($i = date('Y'); $i >= date('Y', strtotime('-1 year') ); $i--)
												<option value="{{ $i }}">{{ $i }}</option>
											@endfor
										</select>
									</div>
									<div class="col-md-5">
										<select id="bulan" name="bulan" required>
											<option value="01">Januari</option>
											<option value="02">Februari</option>
											<option value="03">Maret</option>
											<option value="04">April</option>
											<option value="05">Mei</option>
											<option value="06">Juni</option>
											<option value="07">Juli</option>
											<option value="08">Agustus</option>
											<option value="09">September</option>
											<option value="10">Oktober</option>
											<option value="11">November</option>
											<option value="12">Desember</option>
										</select>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-md-2 pull-right" for="material">Material</label>
									<div class="col-md-10">
										<select id="material" name="material" class="ini_material" multiple disabled></select>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-md-2 pull-right" for="jumlah_klm" required>Jumlah Kolom</label>
									<div class="col-md-10">
										<select id="jumlah_klm" name="jumlah_klm">
											@for ($i = 1; $i <= 10; $i++)
												<option value="{{ $i }}">{{ $i }}</option>
											@endfor
										</select>
									</div>
								</div>
								<div class="form-group mb-3">
									<div class="custom-file">
										{{-- <button type="submit" class="btn submit_btn btn-success" style="color: white"><i class="fe fe-check fe-16"></i>&nbsp;submit</button> --}}
										@if($count_download != 0 && $total_rfc <= $count_download)
											<button type="submit" class="btn submit_btn btn-success" style="color: white"><i class="fe fe-check fe-16"></i>&nbsp;submit</button>
										@endif
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
    <div class="template_boq row" style="display: flex; flex: 0 0 100%; max-width: 100%"></div>
	</div>
</div>
@endsection
@section('footerS')
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" integrity="sha512-uto9mlQzrs59VwILcLiRYeLKPPbS/bT71da/OEBYEwcdNUk8jYIy+D176RYoop1Da+f9mvkYrmj5MCLZWEtQuA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/jqueryui-editable/js/jqueryui-editable.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src='/js/jquery.dataTables.min.js'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.js" integrity="sha512-lbwH47l/tPXJYG9AcFNoJaTMhGvYWhVM9YI43CT+uteTRRaiLCui8snIgyAN8XWgNjNhCqlAUdzZptso6OCoFQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src='/js/dataTables.bootstrap4.min.js'></script>
<script type="text/javascript">
	$(function(){
		$("body").tooltip({ selector: '[data-toggle=tooltip]' });

		var session = {!! json_encode(Session('auth')) !!},
		data_sp = {!! json_encode($data_sp) !!};

		$.fn.editable.defaults.mode = 'inline';

		$('.edit_used_rfc').editable({
			type: 'text',
			emptytext: '0',
			inputclass:'lebar',
			title: 'Masukkan Angka',
			validate: function(value) {
				if(parseInt($(this).parent().prev().text() ) < parseInt(value) ){
        	return 'Jumlahnya Lebih Banyak Dari Yang Diisi!';
				}

        if($.trim(value) == '') {
        	return 'Nilai Tidak Boleh Kosong!';
        }

        if ($.isNumeric(value) == '') {
					return 'Harus Numerik/Angka!';
        }
    	},
			error: function(response, newValue) {
        if(response.status === 500) {
            return 'Service unavailable. Please try later.';
        } else {
            return response.responseText;
        }
    	},
			success: function(response, newValue) {
				var kembali = parseInt( ($.isNumeric($(this).parent().next().next().text() ) == '' ? 0 : $(this).parent().next().next().text() ) ),
				diberikan = parseInt( ($.isNumeric($(this).parent().prev().text() ) == '' ? 0 : $(this).parent().prev().text() ) );
				$(this).parent().next().next().next().text( (parseInt(newValue) + kembali) - diberikan )
			}
		});

		$('.edit_ket').editable({
			type: 'text',
			emptytext: 'Isi Keterangan Jika Diperlukan',
			inputclass:'ket',
			title: 'Masukkan Angka',
			error: function(response, newValue) {
        if(response.status === 500) {
            return 'Service unavailable. Please try later.';
        } else {
            return response.responseText;
        }
    	},
			success: function(response, newValue) {
				// console.log(newValue)
			}
		});

		$('.up_rfc').on('click', function(){
			var no_rfc = $(this).data('rfc');
			$('#upload_RFCTitle').text('Upload File RFC Nomor '+ no_rfc)
			$('#no_rfc').val(no_rfc)
		});

		$('a.delete').click(function (e) {
			e.preventDefault();
			let timerInterval
			swal.fire({
				showClass: {
					popup: 'animate__animated animate__fadeInDown'
				},
				hideClass: {
					popup: 'animate__animated animate__fadeOutUp'
				},
				title: '<strong>Menghapus RFC</strong>',
				timer: 4000,
				icon: 'warning',
				html:
					'RFC nomor <b>' + $(this).data('rfc') + '</b>' + ' akan dihapus',
				showCancelButton: true,
				focusConfirm: false,
				confirmButtonText:'<i class="fe fe-check fe-16"></i> Ok',
				confirmButtonColor: '#3085d6',
				cancelButtonText:'<i class="fe fe-alert-triangle fe-16"></i> Tidak Jadi',
				cancelButtonColor: '#d33',
				allowOutsideClick: false,
				allowEscapeKey: false,
				didOpen: () => {
					timerInterval = setInterval(() => {
						const left = (Swal.getTimerLeft() / 1000).toFixed(0)
						Swal.getConfirmButton().querySelector('i').textContent = left
						Swal.getConfirmButton().disabled = true;

						if(left == 0){
							Swal.stopTimer()
							clearInterval(timerInterval)
							Swal.getConfirmButton().querySelector('i').textContent = ''
							Swal.getConfirmButton().disabled = false;
						}
					}, 100)
				},
				willClose: () => {
					clearInterval(timerInterval)
				}
			}).then((result) => {
				if (result.isConfirmed) {
					document.location.href = $(this).attr('href');
					Swal.fire('Dihapus!', '', 'danger')
				}
			})
		});

		$('.submit_btn').click(function (e) {
			e.preventDefault();
			var count_rfc = {!! json_encode($unclear_rfc) !!};
			count_rfc = Object.keys(count_rfc).length;
			if(count_rfc == 0){
				var msg = 'Apakah yakin untuk tetap <i>submit</i>?'
			}else{
				var msg = 'Terhitung ada <b>' + count_rfc + '</b>' + ' yang belum lurus, tetap <i>submit</i>?'
			}
			let timerInterval
			swal.fire({
				showClass: {
					popup: 'animate__animated animate__fadeInDown'
				},
				hideClass: {
					popup: 'animate__animated animate__fadeOutUp'
				},
				title: '<strong>Submit RFC</strong>',
				timer: 4000,
				icon: 'warning',
				html: msg,
				showCancelButton: true,
				focusConfirm: false,
				confirmButtonText:'<i class="fe fe-check fe-16"></i> Ok',
				confirmButtonColor: '#3085d6',
				cancelButtonText:'<i class="fe fe-alert-triangle fe-16"></i> Tidak Jadi',
				cancelButtonColor: '#d33',
				allowOutsideClick: false,
				allowEscapeKey: false,
				didOpen: () => {
					timerInterval = setInterval(() => {
						const left = (Swal.getTimerLeft() / 1000).toFixed(0)
						Swal.getConfirmButton().querySelector('i').textContent = left
						Swal.getConfirmButton().disabled = true;

						if(left == 0){
							Swal.stopTimer()
							clearInterval(timerInterval)
							Swal.getConfirmButton().querySelector('i').textContent = ''
							Swal.getConfirmButton().disabled = false;
						}
					}, 100)
				},
				willClose: () => {
					clearInterval(timerInterval)
				}
			}).then((result) => {
				if (result.isConfirmed) {
					$('#submit_lampiran_boq').submit()
					Swal.fire('Berhasil Submit!', '', 'danger')
				}
			})
		});

		// console.log(data_sp)
		$("select").select2({
			width: "100%",
		});

		$('#rfc').select2({
			width: '100%',
			placeholder: "Masukkan Nomor RFC Yang Digunakan",
			allowClear: true,
			minimumInputLength: 7,
			ajax: {
				url: "/get_ajx/search_rfc/base_comp",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						searchTerm: params.term,
						mitra: data_sp.nama_company,
						pekerjaan: data_sp.pekerjaan,
						id_upload: data_sp.id,
						jenis_grab: 'all'
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true,
				success: function(value) {
					// console.log(value)
				}
			}
		});

		$('.lihat_detail_rfc').click(function(){
			$('#modal_check_rfc').modal('toggle');
			$('.submit_btn_rfc').hide();
			$('.rfc_old_field').hide();
			$('.rfc_lama_field').show();
			$.ajax({
				url:"/get_ajx/inventory",
				type:"GET",
				data: {
					isi: $(this).data('rfc'),
					id_pbu: data_sp.id
				},
				dataType: 'json',
				success: (function(data){
					var data_rfc = data.inventory[0]
					console.log(data)
					if(data.download){
						$('.dwn_rfc').show();
						$('.dwn_rfc').attr('href', '/download/rfc/'+data_sp.id+'/'+data_rfc.no_rfc);
					}else{
						$('.dwn_rfc').hide();
					}

					$('.no_rfc').html(data_rfc.no_rfc)
					$('.plant').html(data_rfc.plant)
					$('.wbs_element').html(data_rfc.wbs_element)
					$('.old_rfc').val(data_rfc.rfc_lama)
					$('.type').html( (data_rfc.type == 'Out' ? 'Pengeluaran' : 'Pengembalian') )
					$('.mitra').html(data_rfc.mitra)

					var table_html = "<table class='tbl_material table table-striped table-bordered table-hover'>";
					table_html += "<thead class='thead-dark'>";
					table_html += "<tr>";
					table_html += "<th>Material</th>";
					table_html += "<th>Satuan</th>";
					table_html += "<th>Jumlah</th>";
					table_html += "</tr>";
					table_html += "</thead>";
					table_html += "<tbody>";

					$.each(data.inventory, function(key, val){
						table_html += "<tr>";
						table_html += "<td>"+ val.material +"</td>";
						table_html += "<td>"+ val.satuan +"</td>";
						table_html += "<td>"+ val.quantity +"</td>";
						table_html += "</tr>";
					});

					table_html += "</tbody>";
					table_html += "</table>";

					$('.table_rfc_material').html(table_html)
					var data_select = [];

					$.each(data.pid, function(key, val){
						data_select.push({
							id: key,
							text: val.lokasi +' (' + val.pid + ')'
						})
					});

					// console.log(data_select)

					$('.tbl_material').DataTable({
						autoWidth: true,
        		bFilter: false,
						lengthMenu: [
							[16, 32, 64, -1],
							[16, 32, 64, "All"]
						]
					});

				})
			})
		})

		// $('.ganti_rfc').click(function(){
		// 	$('#change_rfc').modal('toggle');
		// 	$.ajax({
		// 		url:"/get_ajx/inventory",
		// 		type:"GET",
		// 		data: {
		// 			isi: $(this).data('rfc'),
		// 			id_pbu: data_sp.id
		// 		},
		// 		dataType: 'json',
		// 		success: (function(data){
		// 			var data_rfc = data.inventory[0]
		// 			console.log(data)
		// 			if(data.download){
		// 				$('.dwn_rfc').show();
		// 				$('.dwn_rfc').attr('href', '/download/rfc/'+data_sp.id+'/'+data_rfc.no_rfc);
		// 			}else{
		// 				$('.dwn_rfc').hide();
		// 			}

		// 			$('.no_rfc').html(data_rfc.no_rfc)
		// 			$('#rfc_skg').val(data_rfc.no_rfc)
		// 			$('.plant').html(data_rfc.plant)
		// 			$('.wbs_element').html(data_rfc.wbs_element)
		// 			$('.old_rfc').html(data_rfc.rfc_lama)
		// 			$('.type').html( (data_rfc.type == 'Out' ? 'Pengeluaran' : 'Pengembalian') )
		// 			$('.mitra').html(data_rfc.mitra)

		// 			var table_html = "<table class='tbl_material table table-striped table-bordered table-hover'>";
		// 			table_html += "<thead class='thead-dark'>";
		// 			table_html += "<tr>";
		// 			table_html += "<th>Material</th>";
		// 			table_html += "<th>Satuan</th>";
		// 			table_html += "<th>Jumlah</th>";
		// 			table_html += "</tr>";
		// 			table_html += "</thead>";
		// 			table_html += "<tbody>";

		// 			$.each(data.inventory, function(key, val){
		// 				table_html += "<tr>";
		// 				table_html += "<td>"+ val.material +"</td>";
		// 				table_html += "<td>"+ val.satuan +"</td>";
		// 				table_html += "<td>"+ val.quantity +"</td>";
		// 				table_html += "</tr>";
		// 			});

		// 			table_html += "</tbody>";
		// 			table_html += "</table>";

		// 			$('.table_rfc_material').html(table_html)
		// 			var data_select = [];

		// 			$.each(data.pid, function(key, val){
		// 				data_select.push({
		// 					id: key,
		// 					text: val.lokasi +' (' + val.pid + ')'
		// 				})
		// 			});

		// 			// console.log(data_select)

		// 			$('.tbl_material').DataTable({
		// 				autoWidth: true,
    //     		bFilter: false,
		// 				lengthMenu: [
		// 					[16, 32, 64, -1],
		// 					[16, 32, 64, "All"]
		// 				]
		// 			});

		// 		})
		// 	})
		// })

		$('#rfc').on('select2:select', function(){
			isi_m = $(this).val();
			$('#rfc_id').val($(this).val() )
			// console.log('teprilih')
			$('.submit_btn_rfc').show();
			$('.rfc_old_field').show();
			$('#rfc_old').select2({
				width: '100%',
				placeholder: 'Material Bisa Dipilih Lebih Dari Satu',
				allowClear: true
			});

			$('#modal_check_rfc').modal('toggle');

			$('#modal_check_rfc').on('shown.bs.modal', function (e) {
				$.ajax({
					url:"/get_ajx/find/used_rfc",
					type:"GET",
					data: {
						isi: isi_m,
					},
					dataType: 'json',
					success: (function(data){
						console.log(data)
						if(data.rfc_lama.length != 0){
							var rll = data.rfc_lama.split(', ');
							$('#rfc_old').val(rll).change()
						}
					})
				})
			})

			$('#rfc_old').val('').change();
			$('.rfc_lama_field').hide();
			$.ajax({
				url:"/get_ajx/inventory",
				type:"GET",
				data: {
					isi: $(this).val(),
					id_pbu: data_sp.id
				},
				dataType: 'json',
				success: (function(data){
					var data_rfc = data.inventory[0]
					// console.log(data_rfc)
					$('.no_rfc').html(data_rfc.no_rfc)
					$('.plant').html(data_rfc.plant)
					$('.wbs_element').html(data_rfc.wbs_element)
					$('.type').html( (data_rfc.type == 'Out' ? 'Pengeluaran' : 'Pengembalian') )
					$('.mitra').html(data_rfc.mitra)

					var table_html = "<table class='tbl_material table table-striped table-bordered table-hover'>";
					table_html += "<thead class='thead-dark'>";
					table_html += "<tr>";
					table_html += "<th>Material</th>";
					table_html += "<th>Satuan</th>";
					table_html += "<th>Jumlah</th>";
					table_html += "</tr>";
					table_html += "</thead>";
					table_html += "<tbody>";

					$.each(data.inventory, function(key, val){
						table_html += "<tr>";
						table_html += "<td>"+ val.material +"</td>";
						table_html += "<td>"+ val.satuan +"</td>";
						table_html += "<td>"+ val.quantity +"</td>";
						table_html += "</tr>";
					});

					table_html += "</tbody>";
					table_html += "</table>";

					$('.table_rfc_material').html(table_html)
					var data_select = [];

					$.each(data.pid, function(key, val){
						data_select.push({
							id: key,
							text: val.lokasi +' (' + val.pid + ')'
						})
					});

					// console.log(data_select)

					$('.tbl_material').DataTable({
						autoWidth: true,
        		bFilter: false,
						lengthMenu: [
							[16, 32, 64, -1],
							[16, 32, 64, "All"]
						]
					});

				})
			})
		})

		$('.price').keyup(function(event) {
		// skip for arrow keys
			if(event.which >= 37 && event.which <= 40) return;
		// format number
			$(this).val(function(index, value) {
				return value
				.replace(/\D/g, "")
				.replace(/\B(?=(\d{3})+(?!\d))/g, ".");
			});
		});

		$('#tb_rekon').DataTable({
			autoWidth: true,
			lengthMenu: [
				[16, 32, 64, -1],
				[16, 32, 64, "All"]
			]
		});

		$('#tahun, #bulan').select2({
			width: '100%'
		})

		$(document).on('keyup', '.number', function(event){
			if(event.which >= 37 && event.which <= 40) return;
			$(this).val(function(index, value) {
        return value.replace(/\D/g, "");
      });
		})

		$(document).on('click', '.toggleCard', function(){
			$(this).html((i, t) => t === '▲' ? '▼' : '▲');
			$(this).parent().next('.card-body').slideToggle();
		})

		$('select[id="mitra_id"]').on('select2:select', function(){
			var data = $("#material").select2("data"),
			isi = $(this).val();

			// $.each(data, function(key, val){
			// 	if(val.id_mitra != 16 && isi != val.id_mitra){
			// 		val.element.remove()
			// 	}
			// });
		});

		$('select[id="mitra_id"]').select2({
			width: '100%',
			placeholder: "Masukkan Nama Perusahaan Mitra",
			allowClear: true,
			ajax: {
				url: "/get_ajx/mitra/search/per_witel",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						searchTerm: params.term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true,
			},
		});

		$('#pekerjaan').val(null).change();

		$('#pekerjaan').select2({
			width: '100%',
			placeholder: 'Isi jenis pekerjaan',
		});

		$('#jumlah_klm').select2({
			width: '100%',
		});

		$('#jenis_kontrak').select2({
			width: '100%',
			placeholder: 'Isi jenis Kontrak',
		});

		var design = {!! json_encode($design) !!},
		isi_design = [],
		isi;

		$.each(design, function(key, val) {
			isi_design.push({
				id: val.id, text: val.designator
			})
		});

		$('#pekerjaan').on('change', function(){

			var jenis_kerja = {!! json_encode($kerjaan[1]) !!},
			kerja = $(this).val(),
			cari = [];
			$.each(jenis_kerja, function(key, val) {
				if(val.pekerjaan == kerja){
					cari.push({id: val.id, text: val.text})
				}
			});
			// console.log(cari)
			$("#jenis_work").empty().trigger('change')

			$('#jenis_work').select2({
				width: '100%',
				data: cari
			})
		})

		function terbilang(nilai)
    {
			var hasil;
			if (nilai < 0){
				hasil = "minus " + penyebut(nilai);
			}else{
				hasil = penyebut(nilai);
			}

			return hasil;
    }

    function penyebut(nilai)
    {
			var nilai = Math.abs(Math.floor(nilai)),
			huruf = ["", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas"],
			temp;
			if(nilai < 12){
				temp = " " + huruf[nilai];
			}else if(nilai < 20){
				temp = penyebut(nilai - 10) + " belas";
			}else if(nilai < 100){
				temp = penyebut(nilai / 10) + " puluh" + penyebut(nilai % 10);
			}
			return temp;
    }

		$(document).on('click', '.copy_detail_tool', function(){
			var data = $(this).data('original-title');
			var $temp = $("<input>");
			$("body").append($temp);
			$temp.val(data).select();
			document.execCommand("copy");
			$temp.remove();

			toastr.options = {
				"debug": false,
				"newestOnTop": false,
				"progressBar": true,
				"positionClass": "toast-top-center",
				"preventDuplicates": false,
				"onclick": null,
				"showDuration": "300",
				"hideDuration": "1000",
				"timeOut": "3000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut"
			}
			toastr["success"]("Designator Berhasil Disalin!", "Sukses")
		});

		var load_material = [];

		var no_tbl = 0,
		last_used_design = {!! json_encode($data_item) !!},
		sub_jenis_i = {!! json_encode($sub_jenis_all) !!};
		sto_i = {!! json_encode($all_sto) !!};
		// console.log(last_used_design)
		if(last_used_design.length != 0){
			table_temp = ''
			var tbl_load = 0,
			sto_load,
			sub_jenis_load,
			lokasi_load;
			$.each(last_used_design, function(k, v){
				if(k == 0){
					$.each(v, function(k1, v1){
						$(".ini_material").append('<option data-id="'+ v1.id_design +'" data-id_mitra="' +v1.design_mitra_id+ '" value="'+ v1.id_design +'" selected>'+  v1.designator +'</option>').change();
					})
				}

				lokasi_load = v[0].lokasi;
				sto_load = v[0].sto;
				sub_jenis_load = v[0].sub_jenis_p;

				tbl_load += ++k;
				table_temp += "<div class='col-md-6'>";
				table_temp += "<div class='card shadow mb-4'>";
				table_temp += "<div class='card-header'>";
				table_temp += "<strong class='card-title nomor_kol'>Kolom Nomor "+ terbilang(tbl_load) +"</strong>";
				table_temp += "<a class='float-left toggleCard minimize' href='#!' style='color:#17a2b8; text-decoration:none;'>▲</a>";
				table_temp += "</div>";
				table_temp += "<div class='card-body table-responsive'>";
				table_temp += "<input type='text' disabled class='form-control lok_per input-transparent' name='lokasi_pekerjaan[]' placeholder='Silahkan Isi Lokasi' value='"+lokasi_load+"'>&nbsp;";
				table_temp += "<select class='sto_m' name='sto[]' disabled>";
				$.each(sto_i, function(k, v) {
					table_temp += "<option value='"+ v.kode_area +"'>"+ v.kode_area +"</option>"
				})
				table_temp += "</select>&nbsp;";
				table_temp += "<select class='sub_jenis' name='sub_jenis[]'>";
				$.each(sub_jenis_i, function(k, v) {
					table_temp += "<option value='"+ v.sub_jenis +"' "+ (sub_jenis_load == v.sub_jenis ? "selected" : '') +">"+ v.sub_jenis +"</option>"
				})
				table_temp += "</select>&nbsp;";
				table_temp += "<table class='table table_material table-striped table-bordered table-hover' style='width:100%'>";
				table_temp += "<thead class='thead-dark'>";
				table_temp += "<tr>";
				table_temp += "<th rowspan='2'>Designator</th>";
				table_temp += "<th rowspan='2'>Uraian</th>";
				table_temp += "<th rowspan='2'>Jenis Material</th>";
				table_temp += "<th colspan='2'>Paket 7</th>";
				table_temp += "<th rowspan='2'>SP</th>";
				table_temp += "<th rowspan='2'>REKON</th>";
				table_temp += "</tr>";
				table_temp += "<tr>";
				table_temp += "<th>Material</th>";
				table_temp += "<th>Jasa</th>";
				table_temp += "</tr>";
				table_temp += "</thead>";
				table_temp += "<tbody data-nomor='"+tbl_load+"'>";

				var total_material_sp = 0,
				total_jasa_sp = 0,
				total_material_rekon = 0,
				total_jasa_rekon = 0;

				$.each(v, function(k1, v1){
					table_temp += "<tr data-id='"+v1.id_design+"'>";
					table_temp += "<td>"+v1.designator+"</td>";

					var word = v1.uraian.split(' '),
					text = '';

					if(word.length > 6){
						text = "...<a style='color:blue; cursor:pointer' class='copy_detail_tool' data-toggle='tooltip' title='"+v1.uraian+"'>Lihat Detail</a>";
					}

					table_temp += "<td>"+word.slice(0, 6).join(' ') + text +"</td>";
					table_temp += "<td>"+v1.namcomp+"</td>";
					table_temp += "<td>"+v1.material.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".")+"</td>";
					table_temp += "<td>"+v1.jasa.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".")+"</td>";
					table_temp += "<td>"+v1.sp.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".")+"</td>";

					total_material_sp += (v1.material * v1.sp );
					total_jasa_sp += (v1.jasa * v1.sp );
					total_material_rekon += (v1.material * v1.rekon );
					total_jasa_rekon += (v1.jasa * v1.rekon );

					table_temp += "<td>"+v1.rekon+"</td>";
					table_temp += "</tr>";
				});

				// console.log(total_material_rekon, total_jasa_rekon)

				table_temp += "</tbody>";
				table_temp += "<tfoot>";
				table_temp += "<tr data-nomor_foot='"+tbl_load+"'>";
				table_temp += "<td colspan='3'>Total Harga</td>";
				table_temp += "<td class='hrg_material'>" + total_material_rekon.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") + "</td>";
				table_temp += "<td class='hrg_jasa'>" + total_jasa_rekon.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") + "</td>";
				table_temp += "<td>" + (total_material_sp + total_jasa_sp).toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") + "</td>";
				table_temp += "<td class='sum_me'>" + (total_material_rekon + total_jasa_rekon).toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") + "</td>";
				table_temp += "</tr>";
				table_temp += "</tfoot>";
				table_temp += "</table>";
				table_temp += "</div>";
				table_temp += "</div>";
				table_temp += "</div>";
			})

			no_tbl += tbl_load;
			// add_material($('#material').val() );

      $('.template_boq').append(table_temp)

			$('.sto_m').each(function() {
				$(this).select2({
					width: '100%',
					placeholder: 'Sto Bisa Dipilh'
				});

				$(this).val([sto_load]).change();
			});

			$('.sub_jenis').each(function() {
				$(this).select2({
					width: '100%',
					placeholder: 'Sto Bisa Dipilh'
				});
			});

			var material_tot = 0,
			jasa_tot = 0;

			$('.field_material_input').each(function(){
				var id_material = parseInt($(this).data('id_mat') ),
				material = parseInt($(this).attr('data-material') ),
				jasa = parseInt($(this).attr('data-jasa') ),
				urutan = parseInt($(this).parent().parent().parent().data('nomor') ),
				isi = parseInt($(this).val() );
				material_tot += material * isi;
				jasa_tot += jasa * isi;
			});

			$('#total_mat').val(material_tot.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
			$('#total_jasa').val(jasa_tot.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
			$('#total_grand').val((material_tot + jasa_tot).toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
		}

		var data_pbu = {!! json_encode($data_pbu) !!},
		pekerjaan = data_pbu.pekerjaan,
		jenis_work = data_pbu.jenis_work,
		jenis_kontrak = data_pbu.jenis_kontrak,
		mitra_id = data_pbu.mitra_id,
		mitra_nm = data_pbu.mitra_nm,
		lokasi_pekerjaan = data_pbu.lokasi_pekerjaan,
		judul = data_pbu.judul;
		id_project = data_pbu.id_project;
		tahun_kerja = data_pbu.bulan_pengerjaan.split('-');

		$('#material').select2({
			width: '100%',
			placeholder: "Masukkan Nama Material",
			allowClear: true,
			ajax: {
				url: "/get_ajx/find_material",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					isi = params.term;
					return {
						searchTerm: params.term,
						tahun : tahun_kerja[0],
						bulan : tahun_kerja[1],
						witel : data_pbu.witel,
					};
				},
				processResults: function (response) {
					if(isi == null && response.length != 0){
						design = response;
						// console.log(design)
						$.each(response, function(key, val) {
							isi_design.push({
								id: val.id, text: val.designator
							})
						});
					}
					return {
						results: response
					};
				},
				cache: true,
			},
			// language: {
			// 	noResults: function(){
			// 		return "Tidak Menemukan Material? Buat Material New Item</b>&nbsp;<a type='button' class='btn btn-info btn-sm' href='/Report/material_manual/input' target='_blank'><span class='fe fe-plus-circle fe-16'></span> Tambah No PKS Baru</a>";
			// 	}
			// },
			escapeMarkup: function (markup) {
				return markup;
			},
			templateSelection: function(data) {
				var $result = $(
					"<span data-id=" +data.id+ " data-id_mitra=" +data.id_mitra+ ">" + data.text + "</span>"
				);
				return $result;
			}
		});

		// if(data_pbu.sto_pekerjaan != null){
		// 	var sto = data_pbu.sto_pekerjaan.split(','),
		// 	sto_load = [];
		// 	$.each(sto, function(k, v){
		// 		sto_load.push(v);
		// 	});

		// 	$('#sto').val(sto_load).change();
		// }

		$('#pekerjaan').val(pekerjaan).change();
		$('#jenis_work').val(jenis_work).trigger('change');
		$('#mitra_id').append(new Option(mitra_nm, mitra_id, false, false) ).trigger('change');
		$('#judul').val(judul).change();
		$('#wbs').val(id_project).change();
		$('#lokasi_pekerjaan').val(lokasi_pekerjaan).change();
		$('#jenis_kontrak').val(jenis_kontrak).change();
		$('#tahun').val(tahun_kerja[0]).change();
		$('#bulan').val(tahun_kerja[1]).change();
		$('#jumlah_klm').val(last_used_design.length).change();

		$('#pekerjaan, #jenis_work, #mitra_id, #judul, #wbs, #lokasi_pekerjaan, #jenis_kontrak, #tahun, #bulan, #jumlah_klm').prop( "disabled", true );
	});
</script>
@endsection