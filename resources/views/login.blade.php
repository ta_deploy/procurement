<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">
    <title>Login - PROMISE</title>
    <link rel="stylesheet" href="/css/select2.css">
    <!-- Simple bar CSS -->
    <link rel="stylesheet" href="/css/simplebar.css">
    <!-- Fonts CSS -->
    <link href="https://fonts.googleapis.com/css2?family=Overpass:ital,wght@0,100;0,200;0,300;0,400;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    <!-- Icons CSS -->
    <link rel="stylesheet" href="/css/feather.css">
    <!-- Date Range Picker CSS -->
    <link rel="stylesheet" href="/css/daterangepicker.css">
    <!-- App CSS -->
    <link rel="stylesheet" href="/css/app-light.css" id="lightTheme">
    <link rel="stylesheet" href="/css/app-dark.css" id="darkTheme" disabled>
  </head>
  <body class="light">
    <div class="wrapper vh-100">
      <div class="row align-items-center h-100">
        <form class="col-lg-3 col-md-4 col-10 mx-auto text-center" method="POST">
        @if (Session::has('alerts'))
            @foreach(Session::get('alerts') as $alert)
                <div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
            @endforeach
        @endif
          {{ csrf_field() }}
          {{-- <a class="navbar-brand mx-auto mt-2 flex-fill text-center" href="./index.html">
            <svg version="1.1" id="logo" class="navbar-brand-img brand-md" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 120 120" xml:space="preserve">
              <g>
                <polygon class="st0" points="78,105 15,105 24,87 87,87"/>
                <polygon class="st0" points="96,69 33,69 42,51 105,51"/>
                <polygon class="st0" points="78,33 15,33 24,15 87,15"/>
              </g>
            </svg>
          </a> --}}
          <img style="vertical-align:middle; width: 100%; height: auto;" src="/images/logo.png">
          {{-- <h1 class="h6 mb-3">Sign in</h1> --}}
          <div class="form-group">
            <label for="username" class="sr-only">NIK</label>
            <input type="text" id="username" name="username" class="form-control form-control-lg" placeholder="NIK" style="vertical-align: middle; text-align: center" required="" autofocus="">
          </div>
          <div class="form-group">
            <label for="password" class="sr-only">Password</label>
            <input type="password" id="password" name="password" class="form-control form-control-lg" placeholder="Password" style="vertical-align: middle; text-align: center" required="">
          </div>

          <div class="form-group row" style="vertical-align: middle; text-align: center">
            <div class="col-md-4 captcha">
              <span>{{ captcha_img('math') }}</span>
            </div>
            <div class="col-md-2">
              <button type="button" class="btn btn-danger" class="reload" id="reload"><i class="fe fe-refresh-cw"></i>
            </div>
            <div class="col-md-6">
              <input type="captcha" id="captcha" name="captcha" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');"  class="form-control form-control-lg" style="text-align: right" placeholder="Captcha" required="">
            </div>
          </div>

          <div class="form-group form-check form-check-inline">
            <input class="form-check-input" type="radio" name="type_login" id="lokal" value="lokal" checked>
            <label class="form-check-label" for="lokal">Lokal</label>
          </div>
          <div class="form-group form-check form-check-inline">
            <input class="form-check-input" type="radio" name="type_login" id="sso" value="sso">
            <label class="form-check-label" for="sso">SSO Telkom Akses</label>
          </div>

          {{-- <div class="checkbox mb-3">
            <label>
              <input type="checkbox" value="remember-me"> Stay logged in </label>
          </div> --}}

          <button type="submit" class="btn btn-lg btn-primary btn-block">Login</button>
          <p class="mt-5 mb-3 text-muted">© Est 2021 - {{ date('Y') }}<br />Procurement Management System</p>
        </form>
      </div>
    </div>
    <script src="/js/jquery.min.js"></script>
    <script src="/js/popper.min.js"></script>
    <script src="/js/moment.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/simplebar.min.js"></script>
    <script src="/js/select2.min.js"></script>
    <script src="js/daterangepicker.js"></script>
    <script src="js/jquery.stickOnScroll.js"></script>
    <script src="/js/tinycolor-min.js"></script>
    <script src="/js/config.js"></script>
    <script src="/js/apps.js"></script>
    <script type="text/javascript">
      $('.select2').select2();

      $('#reload').click(function () {
        $.ajax({
          type: 'GET',
          url: 'reload-captcha',
          success: function (data) {
              $(".captcha span").html(data.captcha);
          }
        });
      });
    </script>
  </body>
</html>