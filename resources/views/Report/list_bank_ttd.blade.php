@extends('layout')
@section('title', 'List Dokumen TTD')
@section('headerS')
<link rel="stylesheet" href="/css/dataTables.bootstrap4.css">
<style>
	@media (min-width: 768px) {
		.modal-xl {
			width: 100%;
		max-width:1200px;
		}
	}
</style>
@endsection
@section('content')
@if (Session::has('alerts'))
  @foreach(Session::get('alerts') as $alert)
    <div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
  @endforeach
@endif
<div class="container-fluid">
  <div class="row justify-content-center">
    <div class="col-12">
      <h2 class="page-title">List TTD User Per Pekerjaan</h2>
      <div class="row">
        <div class="col-md-12">
          <div class="card shadow">
            <div class="card-body table-responsive">
              <ul class="nav nav-tabs mb-3" id="myTab" role="tablist">
                <li class="active"><a href="#data_all" id="data1" class="nav-link active" data-toggle="tab">Semua User&nbsp;{!! count($data_all) != 0 ? "<span class='badge badge-info'>".count($data_all)."</span>" : '' !!}</a></li>
                <li class=""><a href="#data_psb" id="data2" class="nav-link" data-toggle="tab">PSB&nbsp;{!! count($data_psb) != 0 ? "<span class='badge badge-primary'>".count($data_psb)."</span>" : '' !!}</a></li>
								<li class=""><a href="#data_main" id="data3" class="nav-link" data-toggle="tab" aria-expanded="false">Maintenance&nbsp;{!! count($data_main) != 0 ? "<span class='badge badge-warning'>".count($data_main)."</span>" : '' !!}</a></li>
								<li class=""><a href="#data_cons" id="data4" class="nav-link" data-toggle="tab" aria-expanded="false">Construction&nbsp;{!! count($data_cons) != 0 ? "<span class='badge badge-warning'>".count($data_cons)."</span>" : '' !!}</a></li>
              </ul>
							<div id="myTabContentLeft" class="tab-content">
								<div class="tab-pane fade active show" id="data_all">
									<a type="button" href="/tools/user/input" class="btn btn-info btn-sm" style="margin-bottom: 5px;"><i class="fe fe-plus fe-16"></i>&nbsp;Tambah User</a>
									<table class="table table-striped table-bordered table-hover">
										<thead class="thead-dark">
											<tr>
												<th class="hidden-xs">#</th>
												<th>NIK</th>
												<th>Nama</th>
												<th>Jabatan</th>
												<th>Pekerjaan</th>
												<th>Witel</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody id="data_table1">
											@php
												$num = 1;
											@endphp
											@foreach($data_all as $d)
											<tr>
												<td class="hidden-xs">{{ $num++ }}</td>
												<td>{{ $d->nik }}</td>
												<td>{{ $d->user }}</td>
												<td>{{ $d->jabatan }}</td>
												<td>{{ $d->pekerjaan }}</td>
												<td>{{ $d->witel }}</td>
												<td>
													<a class="btn btn-info btn-sm" href="/tools/user/{{$d->id}}" target="_blank" style="color: #fff"><i class="fe fe-tool fe-16"></i>&nbsp;Edit</a>
													<a class="btn btn-danger btn-sm" href="/tools/delete_user/{{$d->id}}" target="_blank" style="color: #fff"><i class="fe fe-trash fe-16"></i>&nbsp;Delete</a>
												</td>
											</tr>
											@endforeach
										</tbody>
									</table>
								</div>
								<div class="tab-pane fade" id="data_psb">
									<a type="button" href="/Admin/bank_dokumen_ttd/psb" class="btn btn-warning btn-sm" style="margin-bottom: 5px;"><i class="fe fe-tool fe-16"></i>&nbsp;Edit User TTD</a>
									<table class="table table-striped table-bordered table-hover">
										<thead class="thead-dark">
											<tr>
												<th class="hidden-xs">#</th>
												<th>Nik</th>
												<th>Nama</th>
												<th>Witel</th>
												<th>Jabatan</th>
											</tr>
										</thead>
										<tbody id="data_table1">
											@php
												$num = 1;
											@endphp
											@foreach($data_psb as $d)
											<tr>
												<td class="hidden-xs">{{ $num++ }}</td>
												<td>{{ $d->nik }}</td>
												<td>{{ $d->user }}</td>
												<td>{{ $d->witel_pdt }}</td>
												<td>{{ $d->jabatan }}</td>
											</tr>
											@endforeach
										</tbody>
									</table>
								</div>
								<div class="tab-pane fade" id="data_main">
									<a type="button" href="/Admin/bank_dokumen_ttd/opt" class="btn btn-warning btn-sm" style="margin-bottom: 5px;"><i class="fe fe-tool fe-16"></i>&nbsp;Edit User TTD</a>
									<table class="table table-striped table-bordered table-hover">
										<thead class="thead-dark">
											<tr>
												<th class="hidden-xs">#</th>
												<th>Nik</th>
												<th>Nama</th>
												<th>Witel</th>
												<th>Jabatan</th>
											</tr>
										</thead>
										<tbody id="data_table4">
											@php
											$num = 1;
											@endphp
											@foreach($data_main as $d)
											<tr>
                        <td class="hidden-xs">{{ $num++ }}</td>
												<td>{{ $d->nik }}</td>
												<td>{{ $d->user }}</td>
												<td>{{ $d->witel_pdt }}</td>
												<td>{{ $d->jabatan }}</td>
											</tr>
											@endforeach
										</tbody>
									</table>
								</div>
								<div class="tab-pane fade" id="data_cons">
									<a type="button" href="/Admin/bank_dokumen_ttd/cons" class="btn btn-warning btn-sm" style="margin-bottom: 5px;"><i class="fe fe-tool fe-16"></i>&nbsp;Edit User TTD</a>
									<table class="table table-striped table-bordered table-hover">
										<thead class="thead-dark">
											<tr>
												<th class="hidden-xs">#</th>
												<th>Nik</th>
												<th>Nama</th>
												<th>Witel</th>
												<th>Jabatan</th>
											</tr>
										</thead>
										<tbody id="data_table4">
											@php
											$num = 1;
											@endphp
											@foreach($data_cons as $d)
											<tr>
                        <td class="hidden-xs">{{ $num++ }}</td>
												<td>{{ $d->nik }}</td>
												<td>{{ $d->user }}</td>
												<td>{{ $d->witel_pdt }}</td>
												<td>{{ $d->jabatan }}</td>
											</tr>
											@endforeach
										</tbody>
									</table>
								</div>
							</div>
            </div>
          </div>
        </div>
      </div> <!-- end section -->
    </div> <!-- .col-12 -->
  </div> <!-- .row -->
</div>
@endsection
@section('footerS')
<script src='/js/jquery.dataTables.min.js'></script>
<script src='/js/dataTables.bootstrap4.min.js'></script>
<script type="text/javascript">
	$(function(){
		$.fn.digits = function(){
			return this.each(function(){
					$(this).text( $(this).text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") );
			});
		}
		$('.status_select').val('APPROVE').change();

		$('.status_select').on('change', function(){
			if($(this).val() == 'REJECT'){
				$('.no_resi_return').css({
					display: 'flex'
				});

				$('#no_resi_return').prop('required', true)

			}else{
				$('.no_resi_return').css({
					display: 'none'
				});

				$('#no_resi_return').removeAttr('required')

				$('#no_resi_return').val();
			}
		})

		$('.table').DataTable({
			autoWidth: true,
			columnDefs: [
				{
					targets: 'no-sort',
					orderable: false
				}
			],
			lengthMenu: [
				[16, 32, 64, -1],
				[16, 32, 64, "All"]
			]
		});

		$('.modal_comp').on('click', function (){
			var data_id = $(this).attr('data-id'),
			stts = $(this).attr('data-stts');

			if(stts == 'REJECT'){
				$('#detail').attr('disabled', true);
				$('#detail').html(data.detail);
				$('.check_lurus, .check_lurus_btn').css({
					'display': 'none'
				});
				$('#no_resi_return').prop('required', true)
			}else{
				$('#detail').attr('disabled', false);
				$('.check_lurus').css({
					'display': 'flex'
				});
				$('.check_lurus_btn').css({
					'display': 'block'
				});
				$('#no_resi_return').removeAttr('required')
			}

			var data_id = $(this).attr('data-id');
			$.ajax({
				url:"/get_ajx/get/detail/sp",
				type:"GET",
				data: {
					id : data_id,
					status: 'SP'
				},
				dataType: 'json',
				success: (function(data){
					console.log(data)
					var tgl = new Date(data.tgl_sp);
					tgl.setDate(tgl.getDate() + data.tgl_jatuh_tmp);

					$('#judul_pekerjaan').html(data.judul);
					$('.surat_penetapan').html(data.surat_penetapan);
					$('.tgl_s_pen').html(data.tgl_s_pen);
					$('.no_sp').html(data.no_sp);
					$('.tgl_sp').html(data.tgl_sp);
					$('.jt').html(data.durasi);
					$('.durasi').html(data.tgl_jatuh_tmp+ ' Hari');
					$('.lokasi').html(data.lokasi);
					$('.pks').html(data.pks);
					$('.nama_company').html(data.nama_company);
					$('.total_material').html(data.total_material_sp);
					$('.total_jasa').html(data.total_jasa_sp);
					$('.gd').html(data.gd_sp);
					$('#id_boq_up').val(data.id);
					$('.budget_req').html(data.budget_req);
					$('.gd_ppn').html(data.gd_ppn_sp);
					$('.total').html(data.total_sp);
					$('.pid').html(data.id_project);
					$('.jenis_work').html(data.pekerjaan + ' | ' + data.jenis_work + ' | ' + data.jenis_kontrak);
					if (data.pekerjaan == 'PSB')
					{
					$('.rar_down').attr('href', '/upload/'+data.id+'/dokumen_ttd/File TTD.rar');
					} else {
					$('.rar_down').attr('href', '/download/full_rar/'+data.id);
					}
					$('.btn_roll').attr('href', '/rollback/'+data.id);

					$(".nama_company, .total_material, .total_jasa, .gd, .gd_ppn, .total, .budget_req").digits();
				}),
			})
		});

	});
</script>
@endsection