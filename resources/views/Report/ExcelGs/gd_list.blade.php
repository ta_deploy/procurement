@extends('layout')
@section('title', 'List Data Tagihan')
@section('headerS')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="/bower_component/jPages-master/css/animate.css">
<link rel="stylesheet" href="/bower_component/jPages-master/css/jPages.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/pivottable/2.23.0/pivot.min.css" integrity="sha256-nn3M6N8S2BjPirPhvCF61ZCcgcppdLtnaNOLhiwro7E=" crossorigin="anonymous" />
@endsection
@section('style')
<style type="text/css">
    .holder {
        position: relative;
        display: inline-block;
    }

    .holder a, .holder span {
        color: black;
        float: left;
        font-size: 13px;
        padding: 8px 16px;
        text-decoration: none;
        transition: background-color .3s;
    }

    .holder a.jp-current {
        background-color: #3c4452;
        color: white;
        border-radius: 5px;
    }

    .holder a {
        background-color: #303641;
        color: white;
        border-radius: 5px;
    }

    .holder a:hover {
        background-color: #303641;
        border-radius: 5px;
    }

    .holder a:first-child {
        border-top-left-radius: 5px;
        border-bottom-left-radius: 5px;
    }

    .holder a:last-child {
        border-top-right-radius: 5px;
    }
    th{
        color: black;
    }

    /* Absolute Center Spinner */
    .loading {
        position: fixed;
        z-index: 999;
        height: 2em;
        width: 2em;
        overflow: show;
        margin: auto;
        top: 0;
        left: 0;
        bottom: 0;
        right: 0;
    }

    /* Transparent Overlay */
    .loading:before {
        content: '';
        display: block;
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background: radial-gradient(rgba(20, 20, 20,.8), rgba(0, 0, 0, .8));

        background: -webkit-radial-gradient(rgba(20, 20, 20,.8), rgba(0, 0, 0,.8));
    }

    /* :not(:required) hides these rules from IE9 and below */
    .loading:not(:required) {
        /* hide "loading..." text */
        font: 0/0 a;
        color: transparent;
        text-shadow: none;
        background-color: transparent;
        border: 0;
    }

    .loading:not(:required):after {
        content: '';
        display: block;
        font-size: 10px;
        width: 1em;
        height: 1em;
        margin-top: -0.5em;
        -webkit-animation: spinner 1500ms infinite linear;
        -moz-animation: spinner 1500ms infinite linear;
        -ms-animation: spinner 1500ms infinite linear;
        -o-animation: spinner 1500ms infinite linear;
        animation: spinner 1500ms infinite linear;
        border-radius: 0.5em;
        -webkit-box-shadow: rgba(255,255,255, 0.75) 1.5em 0 0 0, rgba(255,255,255, 0.75) 1.1em 1.1em 0 0, rgba(255,255,255, 0.75) 0 1.5em 0 0, rgba(255,255,255, 0.75) -1.1em 1.1em 0 0, rgba(255,255,255, 0.75) -1.5em 0 0 0, rgba(255,255,255, 0.75) -1.1em -1.1em 0 0, rgba(255,255,255, 0.75) 0 -1.5em 0 0, rgba(255,255,255, 0.75) 1.1em -1.1em 0 0;
        box-shadow: rgba(255,255,255, 0.75) 1.5em 0 0 0, rgba(255,255,255, 0.75) 1.1em 1.1em 0 0, rgba(255,255,255, 0.75) 0 1.5em 0 0, rgba(255,255,255, 0.75) -1.1em 1.1em 0 0, rgba(255,255,255, 0.75) -1.5em 0 0 0, rgba(255,255,255, 0.75) -1.1em -1.1em 0 0, rgba(255,255,255, 0.75) 0 -1.5em 0 0, rgba(255,255,255, 0.75) 1.1em -1.1em 0 0;
    }

    /* Animation */

    @-webkit-keyframes spinner {
        0% {
            -webkit-transform: rotate(0deg);
            -moz-transform: rotate(0deg);
            -ms-transform: rotate(0deg);
            -o-transform: rotate(0deg);
            transform: rotate(0deg);
        }
        100% {
            -webkit-transform: rotate(360deg);
            -moz-transform: rotate(360deg);
            -ms-transform: rotate(360deg);
            -o-transform: rotate(360deg);
            transform: rotate(360deg);
        }
    }
    @-moz-keyframes spinner {
        0% {
            -webkit-transform: rotate(0deg);
            -moz-transform: rotate(0deg);
            -ms-transform: rotate(0deg);
            -o-transform: rotate(0deg);
            transform: rotate(0deg);
        }
        100% {
            -webkit-transform: rotate(360deg);
            -moz-transform: rotate(360deg);
            -ms-transform: rotate(360deg);
            -o-transform: rotate(360deg);
            transform: rotate(360deg);
        }
    }
    @-o-keyframes spinner {
        0% {
            -webkit-transform: rotate(0deg);
            -moz-transform: rotate(0deg);
            -ms-transform: rotate(0deg);
            -o-transform: rotate(0deg);
            transform: rotate(0deg);
        }
        100% {
            -webkit-transform: rotate(360deg);
            -moz-transform: rotate(360deg);
            -ms-transform: rotate(360deg);
            -o-transform: rotate(360deg);
            transform: rotate(360deg);
        }
    }
    @keyframes spinner {
        0% {
            -webkit-transform: rotate(0deg);
            -moz-transform: rotate(0deg);
            -ms-transform: rotate(0deg);
            -o-transform: rotate(0deg);
            transform: rotate(0deg);
        }
        100% {
            -webkit-transform: rotate(360deg);
            -moz-transform: rotate(360deg);
            -ms-transform: rotate(360deg);
            -o-transform: rotate(360deg);
            transform: rotate(360deg);
        }
    }

    .select2-container--open { color: black; }

    .modal-body {
        overflow-x: auto;
    }

    .modalkuh th{
        color: black;
        font-size: 0.75em;
        white-space: nowrap;
    }
    .modal-xl{
        width: 100%;
        max-width:1300px;
    }

</style>
@endsection
@section('content')
<div class="loading"></div>
<div class="modal fade" id="modal-large" tabindex="-1">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title" style="color: white;">Tabel Data</h4>
            </div>
            <div class="modal-body" style="overflow: hidden;">
                <table class="table table-striped table-bordered modalkuh" id="gdrive" style="width: 100%;">
                    <thead>
                        <tr class="headerr">
                            <th>No</th>
                            <th>Witel</th>
                            <th>Program Kerja</th>
                            <th>Justifikasi Kebutuhan</th>
                            <th>Nama Mitra</th>
                            <th>Nomor Surat Pesanan</th>
                            <th>Tanggal Sp</th>
                            <th>Toc</th>
                            <th>Judul Pekerjaan</th>
                            <th>Nilai Sp</th>
                            <th>Nilai Rekon</th>
                            <th>Pic Pr</th>
                            <th>Id Pr</th>
                            <th>Tanggal Input Pr</th>
                            <th>Pic Po</th>
                            <th>Id Po & No Po</th>
                            <th>Tanggal Input Po</th>
                            <th>Id Gr</th>
                            <th>Tanggal Input Gr</th>
                            <th>Tanggal Gr</th>
                            <th>Status Pekerjaan</th>
                            <th>Posisi Tagihan</th>
                            <th>Tanggal Terima Tagihan Mitra</th>
                            <th>No Berkas</th>
                            <th>Procuremet Area</th>
                            <th>Hasil Verifikasi</th>
                            <th>Tanggal Terima Diregional</th>
                            <th>Procurement Regional</th>
                            <th>Hasil Verifikasi</th>
                            <th>Tanggal Masuk Finance</th>
                            <th>Hasil Verifikasi Finance</th>
                            <th>Status Pembayaran</th>
                        </tr>
                    </thead>
                    <tbody id="data_table">
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" required>Close</button>
            </div>
        </div>
    </div>
</div>
<div class="body">
    <div class="row">
        <div id="content" class="defaults">
            <div class="row">
                <div class="col-md-12">
                    <section class="widget">
                        <header>
                            <h4>
                                Pivot <span class="fw-semi-bold">Google SpreadSheet</span>
                                <select id="list">
                                    @foreach($list_sheet as $t)
                                    <option value="{{ $t }}">{{ $t }}</option>
                                    @endforeach
                                </select>
                            </h4>
                        </header>
                        <div class="body">
                            <div id="output"></div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('footerS')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" integrity="sha256-KM512VNnjElC30ehFwehXjx1YCHPiQkOPmqnrWtpccM=" crossorigin="anonymous"></script>
<script src="/bower_component/jPages-master/js/jPages.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/jquery.inputmask.bundle.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pivottable/2.23.0/pivot.min.js" integrity="sha256-/btBGbvOvx2h/NcXVS+JPFhnoUGbZXDX0O2v6AaABLU=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

<script type="text/javascript">
    $(function(){
        $(".loading").css({
            'display' : 'none'
        });

        var list_sheet = <?= json_encode($list_sheet) ?>;

        $("#list").select2({
            width: "100%",
            placeholder: 'Pilih Sheet Sesuai Google SpreadSheet',
            allowClear: true
        });

        $("#list").val('').trigger('change');

        $("#list").change(function(){
            var id = $(this).val();
            $.ajax({
                type: 'POST',
                url: '/get_ajx/tagihan/list',
                data: {
                    "_token": "{{ csrf_token() }}",
                    'id': id
                },
                beforeSend: function() {
                    $(".loading").css({
                        'display': 'flex'
                    });
                },
                success: function (data) {
                    $(".loading").css({
                        'display': 'none'
                    });

                    var data = JSON.parse(data);
                    $("#output").pivotUI( data, {
                        rows: ["Nama Mitra"],
                        cols: ["Posisi Tagihan"],
                        vals: ["Posisi Tagihan"],
                        aggregatorName: "Count",
                        rendererName: "Heatmap",
                        onRefresh: function (config) {
                            $("#output").find('.pvtVal').each(function() {
                                if($(this).data('value') != null){
                                    $(this).css({
                                        'cursor': 'pointer'
                                    });
                                }
                            });
                        },
                        rendererOptions: {
                            table: {
                                clickCallback: function(e, value, filters, pivotData){
                                    var no = [];
                                    if (value != null) {
                                        pivotData.forEachMatchingRecord(filters, function(record){
                                            no.push(record.No);
                                            console.log(record)
                                        });
                                        // console.log(pivotData, no)
                                        $.ajax({
                                            type: 'GET',
                                            url: '/get_ajx/list/ajax_gd',
                                            data: {
                                                "_token": "{{ csrf_token() }}",
                                                'no': no,
                                                'id': id
                                            },
                                            beforeSend: function() {
                                                $(".loading").css({
                                                    'display': 'flex'
                                                });
                                            },
                                            success: function(data){
                                                var n_data = JSON.parse(data);
                                                var data ='';
                                                $(".loading").css({
                                                    'display': 'none'
                                                });

                                                $("#modal-large").modal();
                                                $.each( n_data, function( key, value ) {
                                                    data += '<tr class="tablenya">';
                                                    data += '<td>' + value["No"] + '</td>';
                                                    data += '<td>' + value["Witel"] + '</td>';
                                                    data += '<td>' + value["Program Kerja"] + '</td>';
                                                    data += '<td>' + value["Justifikasi Kebutuhan"] + '</td>';
                                                    data += '<td>' + value["Nama Mitra"] + '</td>';
                                                    data += '<td>' + value["Nomor Surat Pesanan"] + '</td>';
                                                    data += '<td>' + value["Tanggal Sp"] + '</td>';
                                                    data += '<td>' + value["Toc"] + '</td>';
                                                    data += '<td>' + value["Judul Pekerjaan"] + '</td>';
                                                    data += '<td>' + value["Nilai Sp"] + '</td>';
                                                    data += '<td>' + value["Nilai Rekon"] + '</td>';
                                                    data += '<td>' + value["Pic Pr"] + '</td>';
                                                    data += '<td>' + value["Id Pr"] + '</td>';
                                                    data += '<td>' + value["Tanggal Input Pr"] + '</td>';
                                                    data += '<td>' + value["Pic Po"] + '</td>';
                                                    data += '<td>' + value["Id Po & No Po"] + '</td>';
                                                    data += '<td>' + value["Tanggal Input Po"] + '</td>';
                                                    data += '<td>' + value["Id Gr"] + '</td>';
                                                    data += '<td>' + value["Tanggal Input Gr"] + '</td>';
                                                    data += '<td>' + value["Tanggal Gr"] + '</td>';
                                                    data += '<td>' + value["Status Pekerjaan"] + '</td>';
                                                    data += '<td>' + value["Posisi Tagihan"] + '</td>';
                                                    data += '<td>' + value["Tanggal Terima Tagihan Mitra"] + '</td>';
                                                    data += '<td>' + value["No Berkas"] + '</td>';
                                                    data += '<td>' + value["Procuremet Area"] + '</td>';
                                                    data += '<td>' + value["Hasil Verifikasi"] + '</td>';
                                                    data += '<td>' + value["Tanggal Terima Diregional"] + '</td>';
                                                    data += '<td>' + value["Procurement Regional"] + '</td>';
                                                    data += '<td>' + value["Hasil Verifikasi"] + '</td>';
                                                    data += '<td>' + value["Tanggal Masuk Finance"] + '</td>';
                                                    data += '<td>' + value["Hasil Verifikasi Finance"] + '</td>';
                                                    data += '<td>' + value["Status Pembayaran"] + '</td>';
                                                    data += '</tr>';
                                                });
                                                $('.modal-title').html('Table Data Sheet '+id);
                                                $('#data_table').html(data);
                                            }
                                        });
                                    }
                                }
                            }
                        }
                    });
                }
            });
        });
    });
</script>
@endsection