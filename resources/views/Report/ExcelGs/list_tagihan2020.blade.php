@extends('layout')
@section('title', 'List Tagihan')
@section('style')
<style type="text/css">
	table.dataTable thead .sorting {
		background: url("/bower_component/datatables/media/images/sort_both.png") no-repeat center right;
	}

	table.dataTable thead .sorting_asc {
		background: url("/bower_component/datatables/media/images/sort_asc.png") no-repeat center right;
	}

	table.dataTable thead .sorting_desc {
		background: url("/bower_component/datatables/media/images/sort_desc.png") no-repeat center right;
	}

	table.dataTable thead .sorting_asc_disabled {
		background: url("/bower_component/datatables/media/images/sort_asc_disabled.png") no-repeat center right;
	}

	table.dataTable thead .sorting_desc_disabled {
		background: url("/bower_component/datatables/media/images/sort_desc_disabled.png") no-repeat center right;
	}

	.dataTables_wrapper .dataTables_paginate {
		text-align: right;
	}

	table.table thead .sorting {
		background: url("/bower_component/datatables/media/images/sort_both.png") no-repeat center right;
	}

	table.table thead .sorting_asc {
		background: url("/bower_component/datatables/media/images/sort_asc.png") no-repeat center right;
	}

	table.table thead .sorting_desc {
		background: url("/bower_component/datatables/media/images/sort_desc.png") no-repeat center right;
	}

	table.table thead .sorting_asc_disabled {
		background: url("/bower_component/datatables/media/images/sort_asc_disabled.png") no-repeat center right;
	}

	table.table thead .sorting_desc_disabled {
		background: url("/bower_component/datatables/media/images/sort_desc_disabled.png") no-repeat center right;
	}

</style>
@endsection
@section('content')
@if (Session::has('alerts'))
  @foreach(Session::get('alerts') as $alert)
    <div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
  @endforeach
@endif
<div class="body">
	<div class="row">
		<div id="content" class="defaults">
			<h2 class="page-title">List <span class="fw-semi-bold">Data</span></h2>
			<div class="row">
				<div class="col-md-12">
					<section class="widget">
						<header>
							<h4>
								Table <span class="fw-semi-bold">Tagihan 2020</span>
							</h4>
						</header>
						<div class="body">
							<a type="button" href="/Report/input/tagihan_2020" class="btn btn-primary btn-sm pra_kontrak"><i class="material-icons">filter_list</i>Tambah Tagihan Baru</a>
							<div class="mt">
								<table id ="datatable-table" style="width:100%;" cellspacing="0" class="table table-striped table-bordered table-hover display">
									<thead>
										<tr>
											<th>#</th>
											<th>Area</th>
											<th>Nama Mitra</th>
											<th class="no-sort" >Judul Pekerjaan</th>
											<th>Umur TL</th>
											<th>Umur Proc. Area</th>
											<th>Umur Proc. Reg</th>
											<th>Umur Finance</th>
											{{-- <th>Program Kerja</th>
											<th>Periode</th>
											<th>Nomor SP</th>
											<th>Nilai SP</th>
											<th>Nilai Rekon</th>
											<th>Tanggal SP</th>
											<th>Mitra</th>
											<th>Proc. Area</th>
											<th>Proc. Regional</th>
											<th>Finance</th>
											<th>Cash & Bank</th>
											<th>Catatan / Keterangan</th> --}}
											<th class="no-sort">Action</th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
					</section>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('footerS')
<script type="text/javascript" charset="utf8" src="/bower_component/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<script type="text/javascript">
	$(function(){
		var datanya = <?= json_encode($data) ?>,
		final_dta = [],
		get_dt_tghn = [];
		console.log(datanya)
		if (datanya) {
			$.each( datanya, function( keyC1, ValueC1 ) {
				get_dt_tghn.push([
					ValueC1[0] || null,
					ValueC1[1] || null,
					ValueC1[4] || null,
					ValueC1[8] || null,
					ValueC1['umur_tl'],
					ValueC1['umur_proc_area'],
					ValueC1['umur_proc_req'],
					ValueC1['umur_fin'],
					// ValueC1[2] || null,
					// ValueC1[6] || null,
					// ValueC1[5] || null,
					// ValueC1[9] || null,
					// ValueC1[10] || null,
					// ValueC1[6] || null,
					// ValueC1[6] || null,
					// ValueC1[22] || null,
					// ValueC1[26] || null,
					// ValueC1[30] || null,
					// ValueC1[37] || null,
					// null,
					"<a type=\"button\" href=\"/Report/input/note/"+ keyC1 +"\" class=\"btn btn-primary btn-sm pra_kontrak\"><i class=\"material-icons\"> filter_list</i>Salin Data</a> <a type=\"button\" href=\"/Report/edit/tag220/"+ keyC1 +"\" class=\"btn btn-info btn-sm pra_kontrak\"><i class=\"material-icons\"> auto_fix_normal</i>Edit Data</a>"
				]);
			});

			final_dta = {
				BO: get_dt_tghn,
			};

		}

        /* Set the defaults for DataTables initialisation */
        $.extend( true, $.fn.dataTable.defaults, {
            "sDom": "<'row'<'col-md-6'l><'col-md-6'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
            "sPaginationType": "bootstrap",
            "oLanguage": {
                "sLengthMenu": "_MENU_ records per page"
            }
        } );


        /* Default class modification */
        // $.extend( $.fn.dataTableExt.oStdClasses, {
        //     "sWrapper": "dataTables_wrapper form-inline"
        // } );


        /* API method to get paging information */
        $.fn.dataTableExt.oApi.fnPagingInfo = function ( oSettings )
        {
            return {
                "iStart":         oSettings._iDisplayStart,
                "iEnd":           oSettings.fnDisplayEnd(),
                "iLength":        oSettings._iDisplayLength,
                "iTotal":         oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage":          oSettings._iDisplayLength === -1 ?
                    0 : Math.ceil( oSettings._iDisplayStart / oSettings._iDisplayLength ),
                "iTotalPages":    oSettings._iDisplayLength === -1 ?
                    0 : Math.ceil( oSettings.fnRecordsDisplay() / oSettings._iDisplayLength )
            };
        };


        /* Bootstrap style pagination control */
        $.extend( $.fn.dataTableExt.oPagination, {
            "bootstrap": {
                "fnInit": function( oSettings, nPaging, fnDraw ) {
                    var oLang = oSettings.oLanguage.oPaginate;
                    var fnClickHandler = function ( e ) {
                        e.preventDefault();
                        if ( oSettings.oApi._fnPageChange(oSettings, e.data.action) ) {
                            fnDraw( oSettings );
                        }
                    };

                    $(nPaging).append(
                        '<ul class="pagination no-margin">'+
                        '<li class="prev disabled"><a href="#">'+oLang.sPrevious+'</a></li>'+
                        '<li class="next disabled"><a href="#">'+oLang.sNext+'</a></li>'+
                        '</ul>'
                    );
                    var els = $('a', nPaging);
                    $(els[0]).bind( 'click.DT', { action: "previous" }, fnClickHandler );
                    $(els[1]).bind( 'click.DT', { action: "next" }, fnClickHandler );
                },

                "fnUpdate": function ( oSettings, fnDraw ) {
                    var iListLength = 5;
                    var oPaging = oSettings.oInstance.fnPagingInfo();
                    var an = oSettings.aanFeatures.p;
                    var i, ien, j, sClass, iStart, iEnd, iHalf=Math.floor(iListLength/2);

                    if ( oPaging.iTotalPages < iListLength) {
                        iStart = 1;
                        iEnd = oPaging.iTotalPages;
                    }
                    else if ( oPaging.iPage <= iHalf ) {
                        iStart = 1;
                        iEnd = iListLength;
                    } else if ( oPaging.iPage >= (oPaging.iTotalPages-iHalf) ) {
                        iStart = oPaging.iTotalPages - iListLength + 1;
                        iEnd = oPaging.iTotalPages;
                    } else {
                        iStart = oPaging.iPage - iHalf + 1;
                        iEnd = iStart + iListLength - 1;
                    }

                    for ( i=0, ien=an.length ; i<ien ; i++ ) {
                        // Remove the middle elements
                        $('li:gt(0)', an[i]).filter(':not(:last)').remove();

                        // Add the new list items and their event handlers
                        for ( j=iStart ; j<=iEnd ; j++ ) {
                            sClass = (j==oPaging.iPage+1) ? 'class="active"' : '';
                            $('<li '+sClass+'><a href="#">'+j+'</a></li>')
                                .insertBefore( $('li:last', an[i])[0] )
                                .bind('click', function (e) {
                                    e.preventDefault();
                                    oSettings._iDisplayStart = (parseInt($('a', this).text(),10)-1) * oPaging.iLength;
                                    fnDraw( oSettings );
                                } );
                        }

                        // Add / remove disabled classes from the static elements
                        if ( oPaging.iPage === 0 ) {
                            $('li:first', an[i]).addClass('disabled');
                        } else {
                            $('li:first', an[i]).removeClass('disabled');
                        }

                        if ( oPaging.iPage === oPaging.iTotalPages-1 || oPaging.iTotalPages === 0 ) {
                            $('li:last', an[i]).addClass('disabled');
                        } else {
                            $('li:last', an[i]).removeClass('disabled');
                        }
                    }
                }
            }
        } );

        var unsortableColumns = [];
        $('#datatable-table').find('thead th').each(function(){
            if ($(this).hasClass( 'no-sort')){
                unsortableColumns.push({"bSortable": false});
            } else {
                unsortableColumns.push(null);
            }
        });

		$('#datatable-table').DataTable( {
			sDom: "<'row'<'col-md-6 hidden-xs'l><'col-md-6'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
            oLanguage: {
                sLengthMenu: "_MENU_",
                sInfo: "Showing <strong>_START_ to _END_</strong> of _TOTAL_ entries"
            },
            oClasses: {
                sFilter: "pull-right",
                sFilterInput: "form-control input-transparent ml-sm"
            },
            aoColumns: unsortableColumns,
			drawCallback: function () {
				$( 'table.table tbody tr td' ).css( 'padding', '5px 8px 5px 8px' );
			},
			fixedHeader: {
				header: true,
				footer: true
			},
			autoWidth: true,
			order:[
				[ 0, "desc" ]
			],
			scrollX: true,
			columnDefs: [
				{
					"targets": [0, 1, 2, 4, 5, 6, 7, 8],
					"createdCell": function (td, cellData, rowData, row, col) {
						$(td).css('white-space', 'nowrap');
					}
				},
				{
					"targets": [3],
					"createdCell": function (td, cellData, rowData, row, col) {
						$(td).css('min-width', '250px');
					}
				}
			],
			data: final_dta.BO,
			deferRender: true,
			scrollCollapse: true,
			scroller: true,
		}).columns.adjust().draw();

        $(".dataTables_length select").selectpicker({
            width: 'auto'
        });

	});
</script>
@endsection