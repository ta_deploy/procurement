@extends('layout')
@section('title', 'Tabel SLA')
@section('headerS')
<link rel="stylesheet" href="/css/dataTables.bootstrap4.css">
<link rel="stylesheet" href="https://cdn.datatables.net/rowgroup/1.1.3/css/rowGroup.dataTables.min.css">

@endsection

@section('content')
@if (Session::has('alerts'))
  @foreach(Session::get('alerts') as $alert)
    <div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
  @endforeach
@endif
<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-12">
			<div class="row">
				<div class="col-md-12">
          <div class="card shadow mb-4">
            <div class="card-body">
              <p class="mb-2"><strong>Dashboard SLA</strong></p>
              <form method="GET">
                <div class="form-row">
                  <div class="form-group col-md-3">
                    <label for="witel">WITEL</label>
                    <select class="form-control select2" id="witel" name="witel">
                      <option value="All">ALL</option>
                      @foreach ($witel_mod as $val)
                        <option value="{{ $val }}">{{ $val }}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="row col-md-4">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="tgl_select">PERIODE</label>
                        <select class="form-control" id="tgl_select">
                          <option value="Tahun">Tahun</option>
                          <option value="Custom">Custom Range</option>
                          <option value="all_sp">Semua Periode SP</option>
                          <option value="Bulan">Bulan</option>
                        </select>
                        <input type="hidden" name="tgl_hidden" id="hid_tgl">
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group isi_tgl">
                      </div>
                    </div>
                  </div>
                  @if (session('auth')->proc_level != 2)
                    <div class="form-group col-md-3">
                      <label for="mitra">MITRA</label>
                      <select class="form-control select2" id="mitra" name="mitra">
                      </select>
                  </div>
                  @endif
                  @if (empty(session('auth')->peker_pup))
                    <div class="form-group col-md-2">
                      <label for="khs">Pekerjaan</label>
                      <select class="form-control select2" id="khs" name="khs">
                        <option value="All">ALL</option>
                        @foreach ($data_pekerjaan as $val)
                          <option value="{{ $val->pekerjaan }}">{{ $val->pekerjaan }}</option>
                        @endforeach
                      </select>
                    </div>
                  @endif
                  <div class="form-group col-md-2">
                    <p class="mb-2"><strong>Tampilan Tabel</strong></p>
                    <div class="custom-control custom-radio">
                      <input type="radio" id="all" name="view_table" class="custom-control-input" value="all" checked>
                      <label class="custom-control-label" for="all">Semua Aktor</label>
                    </div>
                    <div class="custom-control custom-radio">
                      <input type="radio" id="mitra_only" name="view_table" class="custom-control-input" value="mitra_only">
                      <label class="custom-control-label" for="mitra_only">Hanya Mitra</label>
                    </div>
                  </div>
                  <div class="form-group col-md-12">
                    <label for="search">&nbsp;</label>
                    <button type="submit" class="btn btn-primary btn-block btn_submit"><i class="fe fe-search fe-16"></i>&nbsp;SEARCH</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
				<div class="col-md-12 my-4">
					<div class="card shadow mb-4">
						<div class="card-body">
							<table id ="tb_sp_perM" class="table table-striped table-bordered table-hover">
								<thead class="thead-dark">
									<tr>
                    <th>Aktor</th>
										<th>Close</th>
										<th class="no_sort">rata - rata waktu (SLA)</th>
										<th>Open</th>
										<th class="no_sort">rata - rata waktu (SLA)</th>
										<th class="no_sort">Follow Up</th>
										<th class="no_sort">< 1 Hari</th>
										<th class="no_sort">1 Hari</th>
										<th class="no_sort">2 Hari</th>
										<th class="no_sort">3 Hari</th>
										<th class="no_sort">> 3 Hari</th>
										<th class="no_sort">≥ 7 Hari</th>
									</tr>
								</thead>
                <tbody>
                  @foreach ($data as $key => $val)
                  <tr>
                    <td>{{ $key }}</td>
                    <td>
                      @if (@$val['close'] != 0)
												<a class="url_m" href="/detail_sla/{{ $req->view_table }}/{{ $val['aktor'] }}/close">{{ $val['close'] ?? 0 }}</a></td>
											@else
												-
											@endif
                      @php
                        $total_hours = ROUND(array_sum($val['jml_hari_conv'] ?? []) == 0 ? 0 : (array_sum($val['jml_hari_conv'] ?? []) / $val['close']) );

                        $result = date_diff(date_create(date('Y-m-d H:i:s', strtotime('+'.$total_hours.' hours') ) ), date_create(date('Y-m-d H:i:s') ) );
                        $text = '';

                        if($result->format("%m") != 0)
                        {
                          $text .= $result->format("%m") . ' Bulan ';
                        }

                        if($result->format("%d") != 0)
                        {
                          $text .= $result->format("%d") . ' Hari ';
                        }

                        if($result->format("%h") != 0)
                        {
                          $text .= $result->format("%h") . ' Jam ';
                        }

                        if($total_hours == 0 )
                        {
                          $text = '< 1 Jam';
                        }
                      @endphp
                    <td>{{ $text }} </td>
                    <td>
											@if ($val['kada_close'] != 0)
												<a class="url_m" href="/detail_sla/{{ $req->view_table }}/{{ $val['aktor'] }}/open">{{ $val['kada_close'] ?? 0 }}</a>
											@else
												-
											@endif
                    </td>
                    @php
                        $total_hours = ROUND(array_sum($val['jml_hari_conv_open'] ?? []) == 0 ? 0 : (array_sum($val['jml_hari_conv_open'] ?? []) / $val['kada_close']) );
                        $result = date_diff(date_create(date('Y-m-d H:i:s', strtotime('+'.$total_hours.' hours') ) ), date_create(date('Y-m-d H:i:s') ) );
                        $text = '';

                        if($result->format("%m") != 0)
                        {
                          $text .= $result->format("%m") . ' Bulan ';
                        }

                        if($result->format("%d") != 0)
                        {
                          $text .= $result->format("%d") . ' Hari ';
                        }

                        if($result->format("%h") != 0)
                        {
                          $text .= $result->format("%h") . ' Jam ';
                        }

                        if($total_hours == 0 )
                        {
                          $text = '< 1 Jam';
                        }
                      @endphp
                    <td>{{ $text }}</td>
                    <td>
											@if (@$val['trouble'] != 0)
												<a class="url_m" href="/detail_sla/{{ $req->view_table }}/{{ $val['aktor'] }}/trouble">{{ $val['trouble'] ?? 0 }}</a>
											@else
												-
											@endif
                    </td>
                    <td>
											@if ($val['unclose_D1D'] != 0)
												<a class="url_m" href="/detail_sla/{{ $req->view_table }}/{{ $val['aktor'] }}/unclose_D1D">{{ $val['unclose_D1D'] ?? 0 }}</a>
											@else
												-
											@endif
                    </td>
                    <td>
											@if ($val['unclose_1D'] != 0)
												<a class="url_m" href="/detail_sla/{{ $req->view_table }}/{{ $val['aktor'] }}/unclose_1D">{{ $val['unclose_1D'] ?? 0 }}</a>
											@else
												-
											@endif
                    </td>
                    <td>
											@if ($val['unclose_2D'] != 0)
												<a class="url_m" href="/detail_sla/{{ $req->view_table }}/{{ $val['aktor'] }}/unclose_2D">{{ $val['unclose_2D'] ?? 0 }}</a>
											@else
												-
											@endif
                    </td>
                    <td>
											@if ($val['unclose_3D'] != 0)
												<a class="url_m" href="/detail_sla/{{ $req->view_table }}/{{ $val['aktor'] }}/unclose_3D">{{ $val['unclose_3D'] ?? 0 }}</a>
											@else
												-
											@endif
                    </td>
                    <td>
											@if ($val['unclose_3DD'] != 0)
												<a class="url_m" href="/detail_sla/{{ $req->view_table }}/{{ $val['aktor'] }}/unclose_3DD">{{ $val['unclose_3DD'] ?? 0 }}</a>
											@else
												-
											@endif
                    </td>
                    <td>
											@if ($val['unclose_7DD'] != 0)
												<a class="url_m" href="/detail_sla/{{ $req->view_table }}/{{ $val['aktor'] }}/unclose_7DD">{{ $val['unclose_7DD'] ?? 0 }}</a>
											@else
												-
											@endif
                    </td>
                  </tr>
                  @endforeach
                </tbody>
							</table>
              <code>* {{ ucwords("rumus perhitungan menggunakan durasi dari satu loker ke loker lainnya")}}</code>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('footerS')
<script src="/js/moment.min.js"></script>
<script src="/js/daterangepicker.js"></script>
<script src='/js/jquery.dataTables.min.js'></script>
<script src='/js/dataTables.bootstrap4.min.js'></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js@3.5.1/dist/chart.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@2.0.0"></script>
<script src="https://cdn.datatables.net/rowgroup/1.1.3/js/dataTables.rowGroup.min.js"></script>
<script src='/js/select2.min.js'></script>
<script src='/js/jquery.timepicker.js'></script>
<script>
	$('#tb_sp_perM').DataTable({
		autoWidth: true,
    columnDefs: [{
      targets: 'no_sort',
      orderable: false
    }],
		lengthMenu: [
			[16, 32, 64, -1],
			[16, 32, 64, "All"]
		],
		order: [
			[0, 'asc']
		]
	});

  var url = window.location,
  re_des = url.toString().split('?');

  if(re_des){
    $('.url_m').each(function( index ) {
      var link = $(this).attr('href');
      $(this).attr('href', link + '?' + re_des[1] )
    });
  }

  var mitra = {!! json_encode($mitra_modify) !!};
  $('#witel').val(null).change()
  $('#witel').on('select2:clear change', function() {
    var isi = $(this).val(),
    isi_select = [],
    pra_select = {};

    if(isi == 'KALSEL'){
      pra_select.KALSEL = mitra.KALSEL;
    }else if(isi == 'BALIKPAPAN'){
      pra_select.BALIKPAPAN = mitra.BALIKPAPAN;
    }else if(isi == 'All'){
      pra_select = mitra;
    }else{
      pra_select = {};
    }
    console.log(pra_select, mitra.KALSEL)
    if ($("#mitra").hasClass("select2-hidden-accessible")) {
      // $("#mitra").select2('destroy');
      $("#mitra").empty().change();
    }

    if(!$.isEmptyObject(pra_select)){
      $.each(pra_select, function(key, index){
        isi_select.push({
          id: 'all',
          text: 'ALL'
        })
        $.each(index, function(keyc1, indexc1){
          isi_select.push({
            id: indexc1.id,
            text: indexc1.text
          })
        })
      })
      if(isi_select)
      {
        $("#mitra").select2({
          theme: 'bootstrap4',
          width: '100%',
          data: isi_select
        }).change();
      }

      var mitra_selected = {!! json_encode($req->mitra) !!};
        if(mitra_selected){
          $("#mitra").val(mitra_selected).change();
        }
    }
    else
    {
      if ($("#mitra").hasClass("select2-hidden-accessible")) {
        // $("#mitra").select2('destroy');
        $("#mitra").empty().change();
      }
    }
  })

  $('#khs').select2(
  {
    theme: 'bootstrap4',
    placeholder: 'Pilih Jenis Pekerjaan !',
    width: '100%'
  });

  $('#tgl_select').val(null).change()

  $('#tgl_select').select2(
  {
    theme: 'bootstrap4',
    placeholder: 'Pilih Jenis Periode',
    width: '100%'
  });

  $('#tgl_select').on('change', function(){
    var isi = $(this).val(),
    date_select = [];
    $('#hid_tgl').val(isi);

    if($.inArray(isi, ['Tahun', 'Bulan']) !== -1 ){
      $('.isi_tgl').children().remove();
      $('.isi_tgl').html("<select id='tgl' name='tgl' class='tgl' form-control'></select>");

      $('.hid_tgl').val(isi);
      if(isi == 'Tahun'){
        var start_year = new Date().getFullYear();

        for (var i = start_year; i >= 2021; i--) {
          date_select.push({
            id: i.toString(),
            text: i.toString()
          });
        }
      }

      if(isi == 'Bulan'){
        date_select = [
          {
            id: 1,
            text: 'Januari'
          },
          {
            id: 2,
            text: 'Februari'
          },
          {
            id: 3,
            text: 'Maret'
          },
          {
            id: 4,
            text: 'April'
          },
          {
            id: 5,
            text: 'Mei'
          },
          {
            id: 6,
            text: 'Juni'
          },
          {
            id: 7,
            text: 'Juli'
          },
          {
            id: 8,
            text: 'Agustus'
          },
          {
            id: 9,
            text: 'September'
          },
          {
            id: 10,
            text: 'Oktober'
          },
          {
            id: 11,
            text: 'November'
          },
          {
            id: 12,
            text: 'Desember'
          },
        ];
      }

      $('#tgl').select2({
        theme: 'bootstrap4',
        width: '100%',
        data: date_select
      }).change();
    }else if($.inArray(isi, ['Custom']) !== -1 ){
      $('.isi_tgl').children().remove();
      $('.isi_tgl').html("<input type='text' name='tgl' id='tgl' class='tgl form-control'>");
      $('.hid_tgl').val(isi);
      var start = moment().startOf('month'),
      end = moment().endOf('month');

      $('.tgl').daterangepicker(
      {
        locale: {
          format: 'YYYY-MM-DD'
        },
        startDate: start,
        endDate: end
      })
    }else{
      $('#hid_tgl').val('all');
      $('.isi_tgl').children().remove();
    }
  })

  $('#witel').select2(
  {
    theme: 'bootstrap4',
    placeholder: 'Pilih Witel !',
    width: '100%'
  });

  $('#mitra').select2(
  {
    theme: 'bootstrap4',
    placeholder: 'Pilih Mitra yang Dicari !',
    width: '100%'
  });

  var tgl = {!! json_encode($req->tgl) !!},
  witel = {!! json_encode($req->witel) !!},
  tgl = {!! json_encode($req->tgl) !!},
  tgl_hidden = {!! json_encode($req->tgl_hidden) !!},
  khs = {!! json_encode($req->khs) !!};
  view_table = {!! json_encode($req->view_table) !!};

  if(witel != null)
  {
    $('#tgl').val(tgl).change();
    $('#khs').val(khs).change();
    $('#tgl_select').val(tgl_hidden).change();
    $('#witel').val(witel).change();
    $("input[name='view_table'][value='"+view_table+"']").prop("checked",true);
  }

</script>
@endsection