@extends('layout')
@section('title', 'List KHS')
@section('headerS')
<link rel="stylesheet" href="/css/dataTables.bootstrap4.css">
@endsection
@section('content')
@if (Session::has('alerts'))
  @foreach(Session::get('alerts') as $alert)
    <div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
  @endforeach
@endif
<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-12">
			<div class="row">
				<div class="col-md-12 my-4">
					<a type="button" href="/Report/material/input" class="btn btn-primary btn-sm" style="margin-bottom: 5px;"><i class="fe fe-plus fe-16"></i>&nbsp;Upload KHS</a>
					<a type="button" href="/Report/material_manual/input" class="btn btn-primary btn-sm" style="margin-bottom: 5px;"><i class="fe fe-plus fe-16"></i>&nbsp;Input KHS</a>
					<div class="card shadow mb-4">
						<div class="card-body">
							<h5 class="card-title">Tabel KHS</h5>
							<table id ="tb_material" class="table table-striped table-bordered table-hover">
								<thead class="thead-dark">
									<tr>
										<th class="hidden-xs">#</th>
										<th>Nama KHS</th>
										<th>Uraian</th>
										<th>Satuan</th>
										<th>Durasi Material</th>
										<th>Material</th>
										<th>Jasa</th>
										<th>Witel</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody id="data_table">
									@php
									$num = 1;
									@endphp
									@foreach($data as $d)
									<tr>
										<td class="hidden-xs">{{ $num++ }}</td>
										<td style="width: 40%;">{{ $d->designator }}</td>
										<td style="width: 40%;">{{ html_entity_decode($d->uraian, ENT_QUOTES, 'UTF-8') }}</td>
										<td style="width: 10%;">{{ $d->satuan }}</td>
										@php
											$tgl_txt = "$d->tgl_start sampai $d->tgl_end";
											if($d->tgl_start == '')
											{
												$tgl_txt = 'KOSONG';
											}
										@endphp
										<td style="width: 10%;">{{ $tgl_txt }}</td>
										<td style="width: 10%;">Rp. {{ number_format($d->material,0, ',', '.') }}</td>
										<td style="width: 10%;">Rp. {{ number_format($d->jasa,0, ',', '.') }}</td>
										<td style="width: 40%;">{{ $d->witel }}</td>
										<td>
											<a type="button" class="btn btn-sm btn-info" href="/Report/material_manual/{{ $d->id }}" style="color: white; margin-bottom: 5px;"><i class="fe fe-tool fe-16"></i>&nbsp;Edit</a>
											<a type="button" class="btn btn-sm btn-danger" href="/Report/delete_material/{{ $d->id }}" style="color: white"><i class="fe fe-trash fe-16"></i>&nbsp;Hapus</a>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('footerS')
<script src='/js/jquery.dataTables.min.js'></script>
<script src='/js/dataTables.bootstrap4.min.js'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/jquery.inputmask.bundle.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<script type="text/javascript">
	$(function(){
		$('#tb_material').DataTable({
			autoWidth: true,
			lengthMenu: [
				[16, 32, 64, -1],
				[16, 32, 64, "All"]
			]
		});
	});
</script>
@endsection