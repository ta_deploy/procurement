@extends('layout')
@section('title', 'Monitor Budget')
@section('headerS')
<link rel="stylesheet" href="/css/dataTables.bootstrap4.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" rel="stylesheet" />

<style>
	@media (min-width: 768px) {
		.modal-xl {
			width: 100%;
		max-width:1200px;
		}
	}
</style>
@endsection
@section('content')
@if (Session::has('alerts'))
  @foreach(Session::get('alerts') as $alert)
    <div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
  @endforeach
@endif
<div class="container-fluid">
  <div class="row justify-content-center">
    <div class="col-12">
      <h2 class="page-title">Monitor Budget</h2>
      <div class="row">
        <div class="col-md-12">
          <div class="card shadow">
            <div class="card-body table-responsive">
              <table class="table table-striped table-bordered table-hover" style="width: 100%;">
                <thead class="thead-dark">
                  <tr>
                    <th rowspan="2">#</th>
                    <th rowspan="2">PID</th>
                    <th rowspan="2">PID SAP</th>
                    <th rowspan="2">Nama Project</th>
                    <th colspan="6" style="text-align: center">APM</th>
                    <th colspan="5" style="text-align: center">Promise</th>
                  </tr>
                  <tr>
                    <th>Material : Non Stock</th>
                    <th>Material : Stock</th>
                    <th>Non Material : Jasa</th>
                    <th>Judul Procurement</th>
                    <th>Nomor SP</th>
                    <th>Bulan Pekerjaan</th>
                    <th>Material Rekon</th>
                    <th>Jasa Rekon</th>
                  </tr>
                </thead>
                <tbody id="data_table1">
                  @foreach($data as $no => $isi)
                  <tr>
                    <td class="text-right">{{ ++$no }}</td>
                    <td class="text-right">{{ $isi->pid ?: '-' }}</td>
                    <td class="text-right">{{ $isi->sapid ?: '-' }}</td>
                    <td class="text-right">{{ $isi->nama_project ?: '-' }}</td>
                    <td>Rp. {{ $isi->Material_Non_Stock ?: '-' }}</td>
                    <td>Rp. {{ $isi->Material_Stock ?: '-' }}</td>
                    <td class="text-right">Rp. {{ $isi->Non_Materiall_Operasional ?: '-' }}</td>
                    <td class="text-right">{{ @$isi->judul_pekerjaa_pbu ?: '-' }}</td>
                    <td class="text-right">{{ @$isi->sp ?: '-' }}</td>
                    <td class="text-right">{{ @$isi->bulan ?: '-' }}</td>
                    <td>Rp. {{ @$isi->total_material_rekon != 0 ? number_format($isi->total_material_rekon) : '-' }}</td>
                    <td>Rp. {{ @$isi->total_jasa_rekon != 0 ? number_format($isi->total_jasa_rekon) : '-' }}</td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div> <!-- end section -->
    </div> <!-- .col-12 -->
  </div> <!-- .row -->
</div>
@endsection
@section('footerS')
<script src='/js/jquery.dataTables.min.js'></script>
<script src='/js/dataTables.bootstrap4.min.js'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script type="text/javascript">
	$(function(){
		$.fn.digits = function(){
			return this.each(function(){
					$(this).text( $(this).text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") );
			});
		}

		$('select').select2();

		$('.table').DataTable({
			autoWidth: true,
			columnDefs: [
				{
					targets: 'no-sort',
					orderable: false
				}
			],
			lengthMenu: [
				[16, 32, 64, -1],
				[16, 32, 64, "All"]
			]
		});
	});
</script>
@endsection