@extends('layout')
@section('title', 'Download Format Justifikasi')
@section('headerS')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="/css/dataTables.bootstrap4.css">
<style type="text/css">
	.pull-right {
		text-align: right;
	}

	th {
		text-align: center;
	}
</style>
@endsection
@section('content')
@if (Session::has('alerts'))
  @foreach(Session::get('alerts') as $alert)
    <div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
  @endforeach
@endif
<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-12">
			<h2 class="page-title">Download Format Justifikasi</h2>
			<div class="row">
				<div class="col-md-12">
					<div class="card shadow mb-4">
						<div class="card-header">
							<strong class="card-title">Daftar Pekerjaan Justifikasi</strong>
						</div>
						<div class="card-body table-responsive">
							<table class="tbl_pid table table-striped table-bordered table-hover">
								<thead class="thead-dark">
									<tr>
										<th class="hidden-xs">#</th>
										<th>Jenis Pekerjaan</th>
										<th>Nama Pekerjaan</th>
										<th>Tanggal Request PID</th>
										<th class="no-sort">User Request</th>
										<th class="no-sort">User Update</th>
										<th>Umur (Hari)</th>
										<th>Unduh Pekerjaan</th>
										<th>Keterangan Terakhir</th>
										<th class="no-sort">Action</th>
									</tr>
								</thead>
								<tbody>
									@php
										$num = 1;
									@endphp
									@forelse($data_ready as $d)
										<tr>
											<td class="hidden-xs">{{ $num++ }}</td>
											<td>{{ $d->jenis_work }}</td>
											<td>{{ strtoupper($d->judul) }}</td>
											<td>{{ $d->created_at }}</td>
											<td>{{ $d->nama_modif ? $d->nama_modif .' ('. $d->modified_by .')' : '' }}</td>
											<td>{{ $d->nama }} ({{ $d->created_by }})</td>
											<td>{{ $d->jml_hri }}</td>
											<td>
												<a type="button" class="btn btn-sm btn-info rar_down" href="/download/full_rar/{{ $d->id }}"><i class="fe fe-download fe-16"></i>&nbsp;Justifikasi</a>
											</td>
											<td>{{ $d->detail }}</td>
											<td>
												<a type="button" class="btn btn-sm btn-modal_me btn-success" href="/progress/{{ $d->id }}" style="margin-bottom: 5px;"><i class="fe fe-plus fe-16"></i>&nbsp;Upload </a>
												<a type="button" class="btn btn-sm btn-modal_me btn-warning" href="/rollback/{{ $d->id }}" style="margin-bottom: 5px;"><i class="fe fe-corner-up-left fe-16"></i>&nbsp;Rollback </a>
											</td>
										</tr>
									@empty
									@endforelse
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('footerS')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src='/js/jquery.dataTables.min.js'></script>
<script src='/js/dataTables.bootstrap4.min.js'></script>
<script type="text/javascript">
	$(function(){
		$('.btn-modal_me').on('click', function () {
			$('#id_upload').val($(this).attr('data-id_upload'));
			$('#pid_mod').val($(this).attr('data-pid'));
		});

		$('.tbl_pid').DataTable({
			autoWidth: true,
			columnDefs: [
				{
					targets: 'no-sort',
					orderable: false
				}
			],
			lengthMenu: [
				[16, 32, 64, -1],
				[16, 32, 64, "All"]
			]
		});
	});
</script>
@endsection