@extends('layout')
@section('title', 'List Pekerjaan')
@section('headerS')
<link rel="stylesheet" href="/css/dataTables.bootstrap4.css">
@endsection
@section('content')
@if (Session::has('alerts'))
	@foreach(Session::get('alerts') as $alert)
		<div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
	@endforeach
@endif
<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-12">
			<h2 class="page-title">History Request PID</h2>
			<div class="row">
				<div class="col-md-12">
					<div class="card shadow">
						<div class="card-body table-responsive">
							<a type="button" href="/progress/input" class="btn btn-primary btn-sm" style="margin-bottom: 5px;"><i class="fe fe-plus fe-16"></i>&nbsp;Input Pekerjaan</a>
							<ul class="nav nav-tabs mb-3" id="myTab" role="tablist">
								<li class="active"><a href="#data_active" id="data2" class="nav-link active" data-toggle="tab" aria-expanded="true">Progress&nbsp;{!! count($data_active) != 0 ? "<span class='badge badge-primary'>".count($data_active)."</span>" : '' !!}</a></li>
								<li class="active"><a href="#draft_data" id="data2" class="nav-link" data-toggle="tab" aria-expanded="true">Draft&nbsp;{!! count($draft_data) != 0 ? "<span class='badge badge-primary'>".count($draft_data)."</span>" : '' !!}</a></li>
								<li class=""><a href="#data_delete" id="data2" class="nav-link" data-toggle="tab" aria-expanded="false">Pekerjaan Delete&nbsp;{!! count($data_delete) != 0 ? "<span class='badge badge-warning'>".count($data_delete)."</span>" : '' !!}</a></li>
								<li class=""><a href="#all_data" id="data2" class="nav-link" data-toggle="tab" aria-expanded="false">Belum Finish&nbsp;{!! count($all_data) != 0 ? "<span class='badge badge-info'>".count($all_data)."</span>" : '' !!}</a></li>
							</ul>
							<div id="myTabContentLeft" class="tab-content">
								<div class="tab-pane fade show active" id="data_active">
									<table class="table table-striped table-bordered table-hover">
										<thead class="thead-dark">
											<tr>
												<th class="hidden-xs">#</th>
												<th>Jenis Pekerjaan</th>
												<th>Pekerjaan</th>
												<th>Mitra</th>
												<th>Tanggal PID Created</th>
												<th>Create PID Request</th>
												<th>Harga Jasa</th>
												<th>Harga Material</th>
												<th>Total Harga</th>
												<th>Umur (Hari)</th>
												<th>Keterangan Terakhir</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
											@php
												$num = 1;
											@endphp
											@foreach($data_active as $d)
												<tr>
													<td class="hidden-xs">{{ $num++ }}</td>
													<td>{{ $d->jenis_work }}</td>
													<td>{{ $d->judul }}</td>
													<td>{{ $d->nama_company }}</td>
													<td>{{ $d->created_at }}</td>
													<td>{{ $d->nama_modif ? $d->nama_modif .' ('. $d->modified_by .')' : '' }}</td>
													<td>Rp. {{ number_format($d->total_jasa_sp, 0, ',', ',') }}</td>
													<td>Rp. {{ number_format($d->total_material_sp, 0, ',', ',') }}</td>
													<td>Rp. {{ number_format($d->gd_sp, 0, ',', ',') }}</td>
													<td>{{ $d->jml_hri }}</td>
													<td>{{ $d->detail }}</td>
													<td>
														<a class="btn btn-sm btn-primary" style="color: #fff; margin-bottom: 5px;" href="/get_detail_laporan/{{ $d->id }}" target="_blank"><i class="fe fe-eye"></i>&nbsp;Detail</a>
														<a href="/opt/delete/upload/{{ $d->id }}" data-judul="{{ $d->judul }}" class="btn delete btn-warning" style="color: white;"><i class="fe fe-trash-2 fe-16"></i>&nbsp;Delete</a></td>
												</tr>
											@endforeach
										</tbody>
									</table>
								</div>
								<div class="tab-pane fade" id="draft_data">
									<table class="table table-striped table-bordered table-hover">
										<thead class="thead-dark">
											<tr>
												<th class="hidden-xs">#</th>
												<th>Jenis Pekerjaan</th>
												<th>Pekerjaan</th>
												<th>Mitra</th>
												<th>Tanggal PID Created</th>
												<th>Create PID Request</th>
												<th>Harga Jasa</th>
												<th>Harga Material</th>
												<th>Total Harga</th>
												<th>Umur (Hari)</th>
												<th>Keterangan Terakhir</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
											@php
												$num = 1;
											@endphp
											@foreach($draft_data as $d)
												<tr>
													<td class="hidden-xs">{{ $num++ }}</td>
													<td>{{ $d->jenis_work }}</td>
													<td>{{ $d->judul }}</td>
													<td>{{ $d->nama_company }}</td>
													<td>{{ $d->created_at }}</td>
													<td>{{ $d->nama_modif ? $d->nama_modif .' ('. $d->modified_by .')' : '' }}</td>
													<td>Rp. {{ number_format($d->total_jasa_sp, 0, ',', ',') }}</td>
													<td>Rp. {{ number_format($d->total_material_sp, 0, ',', ',') }}</td>
													<td>Rp. {{ number_format($d->gd_sp, 0, ',', ',') }}</td>
													<td>{{ $d->jml_hri }}</td>
													<td>{{ $d->detail }}</td>
													<td>
														<a class="btn btn-sm btn-primary" style="color: #fff; margin-bottom: 5px;" href="/progress/{{ $d->id }}" target="_blank"><i class="fe fe-eye"></i>&nbsp;Kerjakan</a>
														<a href="/opt/delete/upload/{{ $d->id }}" data-judul="{{ $d->judul }}" class="btn delete btn-warning" style="color: white;"><i class="fe fe-trash-2 fe-16"></i>&nbsp;Delete</a></td>
												</tr>
											@endforeach
										</tbody>
									</table>
								</div>
								<div class="tab-pane fade" id="data_delete">
									<table class="table table-striped table-bordered table-hover">
										<thead class="thead-dark">
											<tr>
												<th class="hidden-xs">#</th>
												<th>Jenis Pekerjaan</th>
												<th>Pekerjaan</th>
												<th>Mitra</th>
												<th>Tanggal PID Created</th>
												<th>Create PID Request</th>
												<th>Harga Jasa</th>
												<th>Harga Material</th>
												<th>Total Harga</th>
												<th>Lama Pekerjaan Dihapus (Hari)</th>
												<th>Loker Terakhir</th>
												<th>Detail</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
											@php
												$num = 1;
											@endphp
											@foreach($data_delete as $d)
												<tr>
													<td class="hidden-xs">{{ $num++ }}</td>
													<td>{{ $d->jenis_work }}</td>
													<td>{{ $d->judul }}</td>
													<td>{{ $d->nama_company }}</td>
													<td>{{ $d->created_by }}</td>
													<td>{{ $d->nama_modif ? $d->nama_modif .' ('. $d->modified_by .')' : '' }}</td>
													<td>Rp. {{ number_format($d->total_jasa_sp, 0, ',', ',') }}</td>
													<td>Rp. {{ number_format($d->total_material_sp, 0, ',', ',') }}</td>
													<td>Rp. {{ number_format($d->gd_sp, 0, ',', ',') }}</td>
													<td>{{ $d->jml_hri }}</td>
													<td>{{ $d->nama_step }}</td>
													<td>{{ $d->detail }}</td>
													<td><a class="btn btn-info" href="/opt/return/upload/{{ $d->id }}" style="color: white;"><i class="fe fe-corner-up-left fe-16"></i>&nbsp;Kembalikan</a></td>
												</tr>
											@endforeach
										</tbody>
									</table>
								</div>
								<div class="tab-pane fade" id="all_data">
									<table class="table table-striped table-bordered table-hover">
										<thead class="thead-dark">
											<tr>
												<th class="hidden-xs">#</th>
												<th>Jenis Pekerjaan</th>
												<th>Pekerjaan</th>
												<th>Mitra</th>
												<th>Tanggal PID Created</th>
												<th>Create PID Request</th>
												<th>Harga Jasa</th>
												<th>Harga Material</th>
												<th>Total Harga</th>
												<th>Umur (Hari)</th>
												<th>Loker Terakhir</th>
												<th>Detail</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
											@php
												$num = 1;
											@endphp
											@foreach($all_data as $d)
												<tr>
													<td class="hidden-xs">{{ $num++ }}</td>
													<td>{{ $d->jenis_work }}</td>
													<td>{{ $d->judul }}</td>
													<td>{{ $d->nama_company }}</td>
													<td>{{ $d->created_at }}</td>
													<td>{{ $d->nama_modif ? $d->nama_modif .' ('. $d->modified_by .')' : '' }}</td>
													<td>Rp. {{ number_format($d->total_jasa_sp, 0, ',', ',') }}</td>
													<td>Rp. {{ number_format($d->total_material_sp, 0, ',', ',') }}</td>
													<td>Rp. {{ number_format($d->gd_sp, 0, ',', ',') }}</td>
													<td>{{ $d->jml_hri }}</td>
													<td>{{ $d->nama_step }}</td>
													<td>{{ $d->detail }}</td>
													<td>
														<a class="btn btn-sm btn-primary" style="color: #fff; margin-bottom: 5px;" href="/get_detail_laporan/{{ $d->id }}" target="_blank"><i class="fe fe-eye"></i>&nbsp;Detail</a>
														<a href="/opt/delete/upload/{{ $d->id }}" data-judul="{{ $d->judul }}" class="btn delete btn-warning" style="color: white; margin-bottom: 5px;"><i class="fe fe-trash-2 fe-16"></i>&nbsp;Delete</a>
													</td>
												</tr>
											@endforeach
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div> <!-- end section -->
		</div> <!-- .col-12 -->
	</div>
</div>
@endsection
@section('footerS')
<script src='/js/jquery.dataTables.min.js'></script>
<script src='/js/dataTables.bootstrap4.min.js'></script>
<script type="text/javascript">
	$(function(){

    $('.table').DataTable({
			autoWidth: true,
			lengthMenu: [
				[16, 32, 64, -1],
				[16, 32, 64, "All"]
			]
		});
  });
</script>
@endsection