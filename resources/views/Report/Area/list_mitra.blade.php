@extends('layout')
@section('title', 'Daftar Mitra')
@section('headerS')
<link rel="stylesheet" href="/css/dataTables.bootstrap4.css">
<style>
	th, td {
		text-align: center;
	}
</style>
@endsection
@section('content')
<div class="container-fluid">
	@if (Session::has('alerts'))
		@foreach(Session::get('alerts') as $alert)
			<div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
		@endforeach
	@endif
	<div class="row justify-content-center">
		<div class="col-12">
			<div class="row">
				<div class="col-md-12 my-4">
					<div class="card shadow mb-4">
						<div class="card-body table-responsive">
							<h5 class="card-title">Daftar Mitra</h5>

							<a type="button" href="/tools/mitra/input" class="btn btn-primary pra_kontrak"><i class="fe fe-plus-square"></i>&nbsp; Tambah Mitra Baru</a>
							<br /><br />

							<table id="tb_mitra" class="table table-striped table-bordered table-hover">
								<thead class="thead-dark">
									<tr>
										{{-- <th class="hidden-xs">#</th> --}}
										<th>ID Regional</th>
										<th>Regional</th>
										<th>Witel</th>
										<th>Mitra</th>
										<th>Pph (%)</th>
										{{-- <th>Perwakilan</th>
										<th>Jabatan</th> --}}
										<th class="hidden-xs">Action</th>
									</tr>
								</thead>
								<tbody id="data_table">
									@php $num = 1; @endphp
									@foreach($data as $d)
									<tr>
										{{-- <td class="hidden-xs">{{ $num++ }}</td> --}}
										<td>{{ $d->id_regional }}</td>
										<td>{{ $d->regional }}</td>
										<td>{{ $d->witel }}</td>
										<td>{{ $d->nama_company }}</td>
										<td>{{ $d->pph }}</td>
										{{-- <td>{{ $d->wakil_mitra }}</td>
										<td>{{ $d->jabatan_mitra }}</td> --}}
										<td>
											<a type="button" href="/tools/mitra/edit/{{$d->id}}" class="btn btn-primary btn-sm pra_kontrak"><i class="fe fe-edit"></i>&nbsp; Edit</a>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('footerS')
<script src='/js/jquery.dataTables.min.js'></script>
<script src="/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript">
	$(function(){
		$('#tb_mitra').DataTable();
	});
</script>
@endsection