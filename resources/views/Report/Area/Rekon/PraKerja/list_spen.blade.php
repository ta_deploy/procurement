@extends('layout')
@section('title', 'List Surat Penetapan')
@section('headerS')
<link rel="stylesheet" href="/css/dataTables.bootstrap4.css">
<style>
	@media (min-width: 768px) {
		.modal-xl {
			width: 100%;
		max-width:1200px;
		}
	}
</style>
@endsection
@section('content')
@if (Session::has('alerts'))
  @foreach(Session::get('alerts') as $alert)
    <div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
  @endforeach
@endif
<div class="container-fluid">
  <div class="row justify-content-center">
    <div class="col-12">
      <h2 class="page-title">List Surat Penetapan</h2>
      <p class="lead text-muted">Mengawal Surat Penetapan</p>
      <div class="row">
        <div class="col-md-12">
          <div class="card shadow">
            <div class="card-body table-responsive">
              <ul class="nav nav-tabs mb-3" id="myTab" role="tablist">
                <li class="active"><a href="#new" id="data1" class="nav-link active" data-toggle="tab">input Surat Penetapan&nbsp;{!! count($new) != 0 ? "<span class='badge badge-primary'>".count($new)."</span>" : '' !!}</a></li>
								<li class=""><a href="#not_work" id="data3" class="nav-link" data-toggle="tab" aria-expanded="false">Belum Dikerjakan Mitra&nbsp;{!! count($wait) != 0 ? "<span class='badge badge-warning'>".count($wait)."</span>" : '' !!}</a></li>
              </ul>
              <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="new">
									<table class="table table-striped table-bordered table-hover">
										<thead class="thead-dark">
											<tr>
												<th class="hidden-xs">#</th>
												<th>Pekerjaan</th>
												<th>Jenis Pekerjaan</th>
												<th>Mitra</th>
												<th>Umur (Hari)</th>
												<th>Keterangan Terakhir</th>
												<th>Update Terakhir</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody id="data_table1">
											@php
											$num = 1;
											@endphp
											@foreach($new as $d)
											<tr>
												<td class="hidden-xs">{{ $num++ }}</td>
												<td>{{ $d->judul }}</td>
												<td>{{ $d->pekerjaan }}</td>
												<td>{{ $d->mitra_nm }}</td>
												<td>{{ $d->jml_hri }}</td>
												<td>{{ $d->detail }}</td>
												<td>{{ $d->nama_modif ? $d->nama_modif .' ('. $d->modified_by .')' : '' }}</td>
												<td>
													<a class="btn btn-sm btn-primary" href="/progress/{{ $d->id }}" style="margin-bottom: 5px;"><i class="fe fe-file-plus fe-16"></i>&nbsp;Kerjakan</a>
													<a type="button" class="btn btn-sm btn-modal_me btn-warning" href="/rollback/{{ $d->id }}" style="margin-bottom: 5px;"><i class="fe fe-corner-up-left fe-16"></i> Rollback </a>
												</td>
											</tr>
											@endforeach
										</tbody>
									</table>
								</div>
								<div class="tab-pane fade" id="not_work">
									<table class="table table-striped table-bordered table-hover">
										<thead class="thead-dark">
											<tr>
												<th class="hidden-xs">#</th>
												<th>Pekerjaan</th>
												<th>Jenis Pekerjaan</th>
												<th>Nomor SP</th>
												<th>Nomor PKS</th>
												<th>Mitra</th>
												<th>Umur (Hari)</th>
												<th>Terakhir Update</th>
												<th>Detail</th>
											</tr>
										</thead>
										<tbody id="data_table3">
											@php
												$num = 1;
											@endphp
											@foreach($wait as $d)
											<tr>
												<td class="hidden-xs">{{ $num++ }}</td>
												<td>{{ $d->judul }}</td>
												<td>{{ $d->pekerjaan }}</td>
												<td>{{ $d->surat_penetapan }}</td>
												<td>{{ $d->pks }}</td>
												<td>{{ $d->mitra_nm }}</td>
												<td>{{ $d->jml_hri }}</td>
												<td>{{ $d->nama_modif ? $d->nama_modif .' ('. $d->modified_by .')' : '' }}</td>
												<td>
													<a class="btn btn-primary" style="color: #fff" href="/get_detail_laporan/{{ $d->id }}" target="_blank"><i class="fe fe-tool fe-16"></i>&nbsp;Detail</a>
												</td>
											</tr>
											@endforeach
										</tbody>
									</table>
								</div>
              </div>
            </div>
          </div>
        </div>
      </div> <!-- end section -->
    </div> <!-- .col-12 -->
  </div> <!-- .row -->
</div>
@endsection
@section('footerS')
<script src='/js/jquery.dataTables.min.js'></script>
<script src='/js/dataTables.bootstrap4.min.js'></script>
<script type="text/javascript">
	$(function(){
		$.fn.digits = function(){
			return this.each(function(){
					$(this).text( $(this).text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") );
			});
		}

		$('.table').DataTable({
			autoWidth: true,
			columnDefs: [
				{
					targets: 'no-sort',
					orderable: false
				}
			],
			lengthMenu: [
				[16, 32, 64, -1],
				[16, 32, 64, "All"]
			]
		});
	});
</script>
@endsection