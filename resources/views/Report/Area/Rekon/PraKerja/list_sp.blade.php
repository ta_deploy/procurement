@extends('layout')
@section('title', 'Surat Pesanan')
@section('headerS')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="/css/daterangepicker.css">
<link rel="stylesheet" href="/css/dataTables.bootstrap4.css">
<style type="text/css">
	th {
		text-align: center;
	}
	.pull-right {
		text-align: right;
	}
</style>
@endsection
@section('content')
@if (Session::has('alerts'))
	@foreach(Session::get('alerts') as $alert)
		<div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
	@endforeach
@endif
<div class="container-fluid">
  <div class="row justify-content-center">
    <div class="col-12">
      <h2 class="page-title">Surat Pesanan</h2>
      <p class="lead text-muted">Melanjutkan Surat Penetapan</p>
			@if(in_array(session('auth')->proc_level, [3, 4, 99, 44]))
			<button type="button" class="btn mb-2 btn-primary" data-toggle="modal" data-target="#verticalModal"><i class="fe fe-edit"></i> Buat Justifikasi PSB</button>
			<div class="modal fade" id="verticalModal" tabindex="-1" role="dialog" aria-labelledby="verticalModalTitle" aria-hidden="true">
				<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
					<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="verticalModalTitle">Justifikasi & Rincian SSL (PSB)</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<form id="form_justifikasi" class="row" method="post" action="/Admin/save/filepsb" enctype="multipart/form-data" autocomplete="off">
							{{ csrf_field() }}
							<input type="hidden" name="id_file" value="justifikasi">
							<div class="col-md-12">
								<div class="form-row">

									<div class="form-group col-md-3">
										<label class="col-form-label pull-right" for="tanggal_terbit">Tanggal Terbit</label>
											<input type="text" id="tanggal_terbit" name="tanggal_terbit" class="form-control date-picker" required="required" value="{{ date('Y-m-d') }}">
									</div>
									<div class="form-group col-md-3">
										<label class="col-form-label pull-right" for="periode_kegiatan">Periode Kegiatan</label>
											<select id="periode_kegiatan" name="periode_kegiatan" required>
											@foreach ($bulan_format as $bulan)
												<option value="{{ $bulan->id }}">{{ $bulan->text }}</option>
											@endforeach
											</select>
									</div>
									<div class="form-group col-md-3">
										<label class="col-form-label pull-right" for="date_periode1">Waktu Pelaksanaan Awal</label>
											<input type="text" class="form-control input-transparent" id="date_periode1" name="rekon_periode1" readonly>
									</div>
									<div class="form-group col-md-3">
										<label class="col-form-label pull-right" for="date_periode2">Waktu Pelaksanaan Akhir</label>
											<input type="text" class="form-control input-transparent" id="date_periode2" name="rekon_periode2" readonly>
									</div>
									<div class="form-group col-md-6">
										<label class="col-form-label pull-right" for="mitra">Mitra</label>
											<select id="mitra" name="mitra" required>
											@foreach ($get_mitra as $mitra)
												<option value="{{ $mitra->id }}">{{ $mitra->text }}</option>
											@endforeach
											</select>
									</div>
									<div class="form-group col-md-4">
										<label class="col-form-label pull-right" for="id_project">ID Project</label>
										<input type="text" class="form-control input-transparent" name="id_project" value="{{ $hss_witel->project_id }}">
									</div>
									<div class="form-group col-md-2">
										<label class="col-form-label pull-right" for="ppn_numb">PPN (%)</label>
											<input type="text" class="form-control input-transparent" name="ppn_numb" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" value="11" required>
									</div>
									<div class="form-group col-md-12 table-responsive">
										<p class="card-text">Rincian Perkiraan Pekerjaan PSB</p>
										<table class="table table-hover table-sm table-bordered tableJustif" style="text-align: center;">
											<thead class="table-primary">
											<tr>
												<th style="color: black">TYPE TRANSAKSI</th>
												<th style="color: black">JENIS LAYANAN</th>
												<th style="color: black">HSS JASA + INSENTIF</th>
												<th style="color: black;">JUMLAH SSL</th>
											</tr>
											</thead>
											<tbody>
												<tr>
													<td rowspan="5">Infrastructure New Sales Fiber<br />KU + SURVEY</td>
												</tr>
												<tr>
													<td>0P - 1P (Tlp/Int)</td>
													<td><input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->p1)) }}" style="text-align: center;" name="p1_survey_hss" id="p1_survey_hss" readonly></td>
													<td><input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" style="text-align: center;" name="p1_survey_ssl" id="p1_survey_ssl" required></td>
												</tr>
												<tr>
													<td>0P - 2P (Tlp+Int)</td>
													<td><input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->p2_inet_voice)) }}" style="text-align: center;" name="p2_tlpint_survey_hss" id="p2_tlpint_survey_hss" readonly></td>
													<td><input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" style="text-align: center;" name="p2_tlpint_survey_ssl" id="p2_tlpint_survey_ssl" required></td>
												</tr>
												<tr>
													<td>0P - 2P (Int+IPTV)</td>
													<td><input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->p2_inet_iptv)) }}" style="text-align: center;" name="p2_intiptv_survey_hss" id="p2_intiptv_survey_hss" readonly></td>
													<td><input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" style="text-align: center;" name="p2_intiptv_survey_ssl" id="p2_intiptv_survey_ssl" required></td>
												</tr>
												<tr>
													<td>0P - 3P (Tlp+Int+IPTV)</td>
													<td><input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->p3)) }}" style="text-align: center;" name="p3_survey_hss" id="p3_survey_hss" readonly></td>
													<td><input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" style="text-align: center;" name="psb_ssl_3p" id="psb_ssl_3p" required></td>
												</tr>
												<tr>
													<td rowspan="5">Infrastructure New Sales Fiber<br />KU</td>
												</tr>
												<tr>
													<td>0P - 1P (Tlp/Int)</td>
													<td><input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->p1_ku)) }}" style="text-align: center;" name="p1_hss" id="p1_hss" readonly></td>
													<td><input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" style="text-align: center;" name="p1_ssl" id="p1_ssl" required></td>
												</tr>
												{{-- <tr>
													<td>0P - 2P (OLD VERSION)</td>
													<td><input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->p2)) }}" style="text-align: center;" name="p2_hss" id="p2_hss" readonly></td>
													<td><input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" style="text-align: center;" name="p2_ssl" id="p2_ssl" required></td>
												</tr> --}}
												<tr>
													<td>0P - 2P (Tlp+Int)</td>
													<td><input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->p2_inet_voice_ku)) }}" style="text-align: center;" name="p2_tlpint_hss" id="p2_tlpint_hss" readonly></td>
													<td><input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" style="text-align: center;" name="p2_tlpint_ssl" id="p2_tlpint_ssl" required></td>
												</tr>
												<tr>
													<td>0P - 2P (Int+IPTV)</td>
													<td><input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->p2_inet_iptv_ku)) }}" style="text-align: center;" name="p2_intiptv_hss" id="p2_intiptv_hss" readonly></td>
													<td><input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" style="text-align: center;" name="p2_intiptv_ssl" id="p2_intiptv_ssl" required></td>
												</tr>
												<tr>
													<td>0P - 3P (Tlp+Int+IPTV)</td>
													<td><input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->p3_ku)) }}" style="text-align: center;" name="p3_hss" id="p3_hss" readonly></td>
													<td><input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" style="text-align: center;" name="p3_ssl" id="p3_ssl" required></td>
												</tr>
												<tr>
													<td rowspan="4">MIGRASI</td>
												</tr>
												<tr>
													<td>1P ke 2P</td>
													<td><input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->migrasi_1p)) }}" style="text-align: center;" name="migrasi_service_1p2p_hss" id="migrasi_service_1p2p_hss" readonly></td>
													<td><input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" style="text-align: center;" name="migrasi_service_1p2p_ssl" id="migrasi_service_1p2p_ssl" required></td>
												</tr>
												<tr>
													<td>1P ke 3P</td>
													<td><input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->migrasi_2p)) }}" style="text-align: center;" name="migrasi_service_1p3p_hss" id="migrasi_service_1p3p_hss" readonly></td>
													<td><input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" style="text-align: center;" name="migrasi_service_1p3p_ssl" id="migrasi_service_1p3p_ssl" required></td>
												</tr>
												<tr>
													<td>2P ke 3P</td>
													<td><input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->migrasi_3p)) }}" style="text-align: center;" name="migrasi_service_2p3p_hss" id="migrasi_service_2p3p_hss" readonly></td>
													<td><input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" style="text-align: center;" name="migrasi_service_2p3p_ssl" id="migrasi_service_2p3p_ssl" required></td>
												</tr>
												<tr>
													<td rowspan="12">ADDON</td>
												</tr>
												<tr>
													<td>LME WiFi Provisioning Type-1 (PT1) KU</td>
													<td><input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->lme_wifi)) }}" style="text-align: center;" name="lme_wifi_pt1_hss" id="lme_wifi_pt1_hss" readonly></td>
													<td><input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" style="text-align: center;" name="lme_wifi_pt1_ssl" id="lme_wifi_pt1_ssl" required></td>
												</tr>
												<tr>
													<td>LME-AP-Indoor</td>
													<td><input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->lme_ap_indoor)) }}" style="text-align: center;" name="lme_ap_indoor_hss" id="lme_ap_indoor_hss" readonly></td>
													<td><input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" style="text-align: center;" name="lme_ap_indoor_ssl" id="lme_ap_indoor_ssl" required></td>
												</tr>
												<tr>
													<td>LME-AP-Outdoor</td>
													<td><input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->lme_ap_outdoor)) }}" style="text-align: center;" name="lme_ap_outdoor_hss" id="lme_ap_outdoor_hss" readonly></td>
													<td><input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" style="text-align: center;" name="lme_ap_outdoor_ssl" id="lme_ap_outdoor_ssl" required></td>
												</tr>
												<tr>
													<td>Smart Camera Only</td>
													<td><input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->smartcam_only)) }}" style="text-align: center;" name="indihome_smart_hss" id="indihome_smart_hss" readonly></td>
													<td><input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" style="text-align: center;" name="indihome_smart_ssl" id="indihome_smart_ssl" required></td>
												</tr>
												<tr>
													<td>Instalasi PLC</td>
													<td><input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->plc)) }}" style="text-align: center;" name="plc_hss" id="plc_hss" readonly></td>
													<td><input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" style="text-align: center;" name="plc_ssl" id="plc_ssl" required></td>
												</tr>
												<tr>
													<td>IKR Add-on STB Tambahan</td>
													<td><input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->stb_tambahan)) }}" style="text-align: center;" name="ikr_addon_stb_hss" id="ikr_addon_stb_hss" readonly></td>
													<td><input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" style="text-align: center;" name="ikr_addon_stb_ssl" id="ikr_addon_stb_ssl" required></td>
												</tr>
												<tr>
													<td>Change STB</td>
													<td><input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->change_stb)) }}" style="text-align: center;" name="change_stb_hss" id="change_stb_hss" readonly></td>
													<td><input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" style="text-align: center;" name="change_stb_ssl" id="change_stb_ssl" required></td>
												</tr>
												<tr>
													<td>Penggantian ONT Premium</td>
													<td><input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->ont_premium)) }}" style="text-align: center;" name="ont_premium_hss" id="ont_premium_hss" readonly></td>
													<td><input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" style="text-align: center;" name="ont_premium_ssl" id="ont_premium_ssl" required></td>
												</tr>
												<tr>
													<td>Instalalasi Wifi Extender</td>
													<td><input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->wifiext)) }}" style="text-align: center;" name="wifiextender_hss" id="wifiextender_hss" readonly></td>
													<td><input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" style="text-align: center;" name="wifiextender_ssl" id="wifiextender_ssl" required></td>
												</tr>
												<tr>
													<td>Insert Tiang 7</td>
													<td><input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->insert_tiang)) }}" style="text-align: center;" name="pu_s7_140_hss" id="pu_s7_140_hss" readonly></td>
													<td><input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" style="text-align: center;" name="pu_s7_140_ssl" id="pu_s7_140_ssl" required></td>
												</tr>
												<tr>
													<td>Insert Tiang 9</td>
													<td><input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->insert_tiang_9)) }}" style="text-align: center;" name="pu_s9_140_hss" id="pu_s9_140_hss" readonly></td>
													<td><input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" style="text-align: center;" name="pu_s9_140_ssl" id="pu_s9_140_ssl" required></td>
												</tr>
											</tbody>
										</table>
									</div>
									<div class="form-group col-md-12">
										<button class="btn btn-block btn_submit btn-primary">Create</button>
									</div>
								</div>
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn mb-2 btn-secondary" data-dismiss="modal">Close</button>
					</div>
					</div>
				</div>
			</div>
			@endif
			<div class="row">
				<div class="col-md-12">
					<div class="card shadow">
						<div class="card-body table-responsive">
							<ul class="nav nav-tabs mb-3" id="myTab" role="tablist">
								<li class="active"><a href="#new" id="data1" class="nav-link active" data-toggle="tab">List &nbsp;{!! count($sanggup) != 0 ? "<span class='badge badge-primary'>".count($sanggup)."</span>" : '' !!}</a></li>
								<li class=""><a href="#approve" id="data2" class="nav-link" data-toggle="tab" aria-expanded="false">Proses &nbsp;{!!count($approve) != 0 ? "<span class='badge badge-success'>".count($approve)."</span>" : '' !!}</a></li>
							</ul>
							<div class="tab-content" id="myTabContent">
								<div class="tab-pane fade show active" id="new">
									<table class="table table-striped table-bordered table-hover datatables tableData" id="data_table1">
										<thead class="thead-dark" style="text-align: center;">
											<tr>
												<th class="hidden-xs">#</th>
												<th>Jenis Pekerjaan</th>
												<th>Pekerjaan</th>
												<th>Mitra</th>
												<th>Umur (Hari)</th>
												<th>Keterangan Terakhir</th>
												<th>Update Terakhir</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody style="text-align: center;">
											@php
											$num = 1;
											@endphp
											@foreach($sanggup as $d)
											<tr>
												<td class="hidden-xs">{{ $num++ }}</td>
												<td>{{ $d->pekerjaan }}</td>
												<td>{{ $d->judul }}</td>
												<td>{{ $d->mitra_nm }}</td>
												<td>{{ $d->jml_hri }}</td>
												<td>{{ $d->detail }}</td>
												<td>{{ $d->nama_modif ? $d->nama_modif .' ('. $d->modified_by .')' : '' }}</td>
												<td>
													<a class="btn btn-sm btn-primary" href="/progress/{{ $d->id }}" style="margin-bottom: 5px;"><i class="fe fe-edit"></i>&nbsp;SP</a>
													<a type="button" class="btn btn-sm btn-modal_me btn-warning" href="/rollback/{{ $d->id }}" style="margin-bottom: 5px;"><i class="fe fe-corner-up-left fe-16"></i>&nbsp;Rollback </a>
												</td>
											</tr>
											@endforeach
										</tbody>
									</table>
								</div>
								<div class="tab-pane fade" id="approve">
									<table class="table table-striped table-bordered table-hover datatables tableData" id="data_table2">
										<thead class="thead-dark">
											<tr>
												<th class="hidden-xs">#</th>
												<th>Jenis Pekerjaan</th>
												<th>Pekerjaan</th>
												<th>Nomor SP</th>
												<th>Nomor PKS</th>
												<th>Tanggal Dibuat</th>
												<th>Umur (Hari)</th>
												<th>Detail</th>
												<th>Update Terakhir</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
											@php
											$num = 1;
											@endphp
											@foreach($approve as $d)
											<tr>
												<td class="hidden-xs" style="text-align: center;">{{ $num++ }}</td>
												<td style="text-align: center;">{{ $d->pekerjaan }}</td>
												<td>{{ $d->judul }}</td>
												<td>{{ $d->surat_penetapan }}</td>
												<td>{{ $d->pks }}</td>
												<td style="text-align: center;">{{ $d->created_at }}</td>
												<td style="text-align: center;">{{ $d->jml_hri }}</td>
												<td>{{ $d->detail }}</td>
												<td>{{ $d->nama_modif ? $d->nama_modif .' ('. $d->modified_by .')' : '' }}</td>
												<td style="text-align: center;">
													@if($d->pekerjaan <> "PSB")
														<a class="btn btn-sm btn-primary" style="color: #fff" href="/get_detail_laporan/{{ $d->id }}" target="_blank"><i class="fe fe-eye"></i>&nbsp;Detail</a>
													<br />
													@endif

													@if($d->pekerjaan == "PSB")
													<br />
													<a class="btn btn-primary" style="color: #fff" href="/Admin/download/filepsb/surat_pesanan/{{ $d->id }}/download"><i class="fe fe-download"></i>&nbsp; Surat Pesanan</a>
													@endif
												</td>
											</tr>
											@endforeach
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div> <!-- end section -->
    </div> <!-- .col-12 -->
  </div> <!-- .row -->
</div>
@endsection
@section('footerS')
<script src="/js/moment.min.js"></script>
<script src="/js/jquery.timepicker.js"></script>
<script src="/js/daterangepicker.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="/js/jquery.dataTables.min.js"></script>
<script src="/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript">
	$(function(){

		$('.tableData').DataTable({
			autoWidth: true,
			lengthMenu: [
				[10, 20, 60, -1],
				[10, 20, 60, "All"]
			]
		});

		$('.tableJustif').DataTable({
			autoWidth: true,
			ordering: false,
			lengthMenu: [
				[16, 32, 64, -1],
				[16, 32, 64, "All"]
			]
		});

		$.fn.digits = function(){
			return this.each(function(){
					$(this).text( $(this).text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") );
			});
		}

		$('select[id="mitra_id"]').select2({
			width: '100%',
			placeholder: "Masukkan Nama Perusahaan Mitra",
			allowClear: true,
			ajax: {
				url: "/get_ajx/mitra/search",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						searchTerm: params.term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true,
			}
		});

		$('.modal_comp').on('click', function (){
			var data_id = $(this).attr('data-id');
			$.ajax({
				url:"/get_ajx/get/detail/sp",
				type:"GET",
				data: {
					id : data_id,
					status: 'SP'
				},
				dataType: 'json',
				success: (function(data){
					// console.log(data)
					var tgl = new Date(data.tgl_sp);
					tgl.setDate(tgl.getDate() + data.tgl_jatuh_tmp);

					$('.judul').html(data.judul);
					$('.surat_penetapan').html(data.surat_penetapan);
					$('.tgl_s_pen').html(data.tgl_s_pen);
					$('.no_sp').html(data.no_sp);
					$('.tgl_sp').html(data.tgl_sp);
					$('.jt').html(tgl.toISOString().slice(0, 10));
					$('.durasi').html(data.durasi);
					$('.lokasi').html(data.lokasi);
					$('.pks').html(data.pks);
					$('.nama_company').html(data.mitra_nm);
					$('.total_material').html(data.total_material_sp);
					$('.total_jasa').html(data.total_jasa_sp);
					$('.gd').html(data.gd_sp);
					$('.gd_ppn').html(data.gd_ppn_sp);
					$('.total').html(data.total_sp);
					$('.pid').html(data.id_project);
					$('.jenis_work').html(data.jenis_work);

					if (data.jenis_work == 'Pasang Sambungan Baru (PSB)') {
						$('.rar_down').hide();
						$('.sp_down').show();
						$('.sp_down').attr('href', '/Admin/download/filepsb/surat_pesanan/'+data.id+'/download');
					} else {
						$('.sp_down').hide();
						$('.rar_down').show();
						$('.rar_down').attr('href', '/download/full_rar/'+data.id);
					}

					$(".nama_company, .total_material, .total_jasa, .gd, .gd_ppn, .total").digits();
				}),
			})
		});

		// $('.price').keyup(function(event) {
		// // skip for arrow keys
		// 		.replace(/\D/g, "")
		// 		.replace(/\B(?=(\d{3})+(?!\d))/g, ".");
		// 	});
		// });
	});
</script>
<script type="text/javascript">
	$(function(){
        $('.date-picker').daterangepicker(
            {
                singleDatePicker: true,
                timePicker: false,
                showDropdowns: true,
                locale:
                {
                    format: 'YYYY-MM-DD',
                }
            }
		);

        $('#mitra').val(null).change();

        $('#mitra').select2({
            placeholder: "Pilih Mitra",
			width: '100%'
		});

        $('#periode_kegiatan').val(null).change();

        $('#periode_kegiatan').select2({
            placeholder: "Pilih Periode Kegiatan",
			width: '100%'
		});

        var periode = {!! json_encode($bulan_format) !!}

        $('#periode_kegiatan').on( 'select2 change', function(){
            var perd = this.value,
            d = new Date(),
            years = d.getFullYear()
            $.each(periode, function(key, index) {
                if(index.id == perd ) {
                    if(perd == '01') {
                        year = d.getFullYear() - 1
                    } else {
                        year = d.getFullYear()
                    }
                    $('#date_periode1').val(year + '-' + index.date_periode1)
                    $('#date_periode2').val(years + '-' + index.date_periode2)
                }
            })
        })

        $('.hss_jasa').keyup(function(event) {

		// skip for arrow keys
			if(event.which >= 37 && event.which <= 40) return;

		// format number
			$(this).val(function(index, value) {
				return value
				.replace(/\D/g, "")
				.replace(/\B(?=(\d{3})+(?!\d))/g, ".")
				;
			});
		});
    });
</script>
@endsection