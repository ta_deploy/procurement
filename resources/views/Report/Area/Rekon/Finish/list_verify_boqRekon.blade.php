@extends('layout')
@section('title', 'List Dokumen Rekon')
@section('headerS')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="/css/daterangepicker.css">
<link rel="stylesheet" href="/css/dataTables.bootstrap4.css" />
<style>
 @media (min-width: 768px) {
  .modal-xl {
   width: 100%;
   max-width: 1200px;
  }
 }
</style>
@endsection
@section('content')
@if (Session::has('alerts'))
@foreach(Session::get('alerts') as $alert)
<div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
@endforeach
@endif
<div class="container-fluid">
 <div class="row justify-content-center">
  <div class="col-12">
   <h2 class="page-title">List Verifikasi Pekerjaan Rekon</h2>
   <div class="row">
    <div class="col-md-12">
     <div class="card shadow">
      <div class="card-body table-responsive">
       <ul class="nav nav-tabs mb-3" id="myTab" role="tablist">
        <li class="active">
         <a href="#ready_rekon" id="data1" class="nav-link active" data-toggle="tab"> BOQ Rekon Siap Check &nbsp; {!! count($ready_rekon) != 0 ? "<span class='badge badge-primary'>".count($ready_rekon)."</span>" : '' !!} </a>
        </li>
        <li class="active">
         <a href="#proses_rekon" id="data1" class="nav-link" data-toggle="tab">Dikerjakan Mitra &nbsp; {!! count($proses_rekon) != 0 ? "<span class='badge badge-info'>".count($proses_rekon)."</span>" : '' !!} </a>
        </li>
        <li class="nav-item">
         <a href="#save_rekon" class="nav-link" data-toggle="tab" aria-expanded="false">Save Rekon &nbsp; {!! count($save_rekon) != 0 ? "<span class='badge badge-warning'>".count($save_rekon)."</span>" : '' !!} </a>
        </li>
        <li class="">
         <a href="#reject_regional" id="data4" class="nav-link" data-toggle="tab" aria-expanded="false">
          Reject Dari Regional &nbsp; {!! count($reject_rekon) != 0 ? "<span class='badge badge-warning'>".count($reject_rekon)."</span>" : '' !!}
         </a>
        </li>
       </ul>
       <div id="myTabContentLeft" class="tab-content">
        <div class="tab-pane fade active show" id="ready_rekon">
         <table class="table table-striped table-bordered table-hover tableData">
          <thead class="thead-dark">
           <tr>
            <th class="hidden-xs">#</th>
            <th>Pekerjaan</th>
            <th>Mitra</th>
            <th>Nomor SP</th>
            <th>Nomor PKS</th>
            <th>TOC</th>
            <th>Umur (Hari)</th>
            <th>Keterangan Terakhir</th>
            <th>Update Terakhir</th>
            <th class="no-sort">Action</th>
           </tr>
          </thead>
          <tbody id="data_table1">
           @php $num = 1; @endphp
           @foreach($ready_rekon as $d)
           <tr>
            <td class="hidden-xs">{{ $num++ }}</td>
            <td>{{ $d->judul }}</td>
            <td>{{ $d->mitra_nm }}</td>
            <td>{{ $d->no_sp }}</td>
            <td>{{ $d->pks }}</td>
            <td>{{ $d->tgl_jatuh_tmp }} Hari <b>({{ date('Y-m-d', strtotime('+'. $d->tgl_jatuh_tmp. ' day', strtotime($d->tgl_s_pen))) }})</b></td>
            <td>{{ $d->jml_hri }}</td>
            <td>{{ $d->detail }}</td>
            <td>{{ $d->nama_modif ? $d->nama_modif .' ('. $d->modified_by .')' : '' }}</td>
            <td>
             @if ($d->pekerjaan <> "PSB")
             <a class="btn btn-sm mb-2 btn-primary" style="color: #fff; margin-bottom: 5px;" href="/progress/{{$d->id}}"><i class="fe fe-tool fe-16"></i>&nbsp;Update</a>
             <a type="button" class="btn btn-sm btn-modal_me btn-warning" href="/rollback/{{ $d->id }}" style="margin-bottom: 5px;"><i class="fe fe-corner-up-left fe-16"></i>&nbsp;Rollback </a>
             <br />
             @endif @if($d->pekerjaan == "PSB") &nbsp;
             <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#verticalModal-{{ $d->id }}"><i class="fe fe-edit"></i>&nbsp;Input</button>
             <div class="modal fade" id="verticalModal-{{ $d->id }}" tabindex="-1" role="dialog" aria-labelledby="verticalModalTitle" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
               <div class="modal-content">
                <div class="modal-header">
                 <h5 class="modal-title" id="verticalModalTitle-{{ $d->id }}">Pengisian Nomor Surat dan Lainnya ({{ $d->id }})</h5>
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                 </button>
                </div>
                <div class="modal-body">
                 <form id="form_pengisian_surat_{{ $d->id }}" class="row" method="post" action="/Admin/save/filepsb" enctype="multipart/form-data" autocomplete="off">
                  {{ csrf_field() }}
                  <input type="hidden" name="id" value="{{ $d->id }}" />
                  <input type="hidden" name="step" value="verif_doc" />
                  <input type="hidden" name="id_file" value="dokumenpelengkap" />
                  <input type="hidden" name="periode_kegiatan" value="{{ $d->periode_kegiatan }}" />
                  <input type="hidden" name="rekon_periode1" value="{{ $d->rekon_periode1 }}" />
                  <input type="hidden" name="rekon_periode2" value="{{ $d->rekon_periode2 }}" />

                  <div class="col-md-12">
                   <div class="form-row">

                    <div class="form-group col-md-6">
                        <label class="col-form-label pull-right" for="surat_penetapan">Nomor Surat Penetapan</label>
                        <input type="text" class="form-control input-transparent" name="surat_penetapan" value="{{ $d->surat_penetapan }}" required />
                    </div>
                    <div class="form-group col-md-6">
                        <label class="col-form-label pull-right" for="tgl_s_pen">Tanggal Surat Penetapan</label>
                        <input type="text" class="form-control input-transparent date-picker" id="tgl_s_pen" name="tgl_s_pen" value="{{ $d->tgl_s_pen }}" required />
                    </div>
                    <div class="form-group col-md-6">
                      <label class="col-form-label pull-right" for="surat_kesanggupan">Nomor Surat Kesanggupan</label>
                      <input type="text" class="form-control input-transparent" name="surat_kesanggupan" value="{{ $d->surat_kesanggupan }}" required />
                    </div>
                    <div class="form-group col-md-6">
                        <label class="col-form-label pull-right" for="tgl_surat_sanggup">Tanggal Surat Kesanggupan</label>
                        <input type="text" class="form-control input-transparent date-picker" id="tgl_surat_sanggup" name="tgl_surat_sanggup" value="{{ $d->tgl_surat_sanggup }}" required />
                    </div>
                    <div class="form-group col-md-6">
                     <label class="col-form-label pull-right" for="amd_sp">Nomor Surat Amandemen SP</label>
                     <input type="text" class="form-control input-transparent" name="amd_sp" value="{{ $d->amd_sp }}" required />
                    </div>
                    <div class="form-group col-md-6">
                     <label class="col-form-label pull-right" for="no_baij">Nomor Surat BAR&IJ</label>
                     <input type="text" class="form-control input-transparent" name="no_baij" value="{{ $d->no_baij }}" required />
                    </div>
                    <div class="form-group col-md-6">
                     <label class="col-form-label pull-right" for="no_bap">Nomor Surat BA Performansi</label>
                     <input type="text" class="form-control input-transparent" name="no_bap" value="{{ $d->no_bap }}" required />
                    </div>
                    <div class="form-group col-md-6">
                     <label class="col-form-label pull-right" for="BAST">Nomor Surat BAST</label>
                     <input type="text" class="form-control input-transparent" name="BAST" value="{{ $d->BAST }}" required />
                    </div>
                    <div class="form-group col-md-6">
                     <label class="col-form-label pull-right" for="no_ba_pemeliharaan">Nomor BA Pemeliharaan</label>
                     <input type="text" class="form-control input-transparent" name="no_ba_pemeliharaan" value="{{ $d->no_ba_pemeliharaan }}" required />
                    </div>
                    <div class="form-group col-md-3">
                     <label class="col-form-label pull-right" for="perfomansi_psb">Nilai Performansi (%)</label>
                     <input type="text" class="form-control input-transparent" name="persentase_7_kpi" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" value="{{ $d->persentase_7_kpi }}" required />
                    </div>
                    <div class="form-group col-md-3">
                     <label class="col-form-label pull-right" for="ppn_numb">PPN (%)</label>
                     <input type="text" class="form-control input-transparent" name="ppn_numb" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" value="{{ $d->ppn_numb ? : '11' }}" required />
                    </div>
                    <div class="form-group col-md-12">
                     <button class="btn btn-block btn_submit btn-primary">Create</button>
                    </div>
                   </div>
                  </div>
                 </form>
                </div>
                <div class="modal-footer">
                 <button type="button" class="btn mb-2 btn-secondary" data-dismiss="modal">Close</button>
                </div>
               </div>
              </div>
             </div>
             @endif
            </td>
           </tr>
           @endforeach
          </tbody>
         </table>
        </div>
        <div class="tab-pane fade" id="proses_rekon">
         <table class="table table-striped table-bordered table-hover tableData">
          <thead class="thead-dark">
           <tr>
            <th class="hidden-xs">#</th>
            <th>Pekerjaan</th>
            <th>Mitra</th>
            <th>Nomor SP</th>
            <th>Nomor PKS</th>
            <th>Status</th>
            <th>TOC</th>
            <th>Umur (Hari)</th>
            <th>Detail</th>
            <th>Update Terakhir</th>
            <th class="no-sort">Action</th>
           </tr>
          </thead>
          <tbody id="data_table4">
           @php $num = 1; @endphp
           @foreach($proses_rekon as $d)
           <tr>
            <td class="hidden-xs">{{ $num++ }}</td>
            <td>{{ $d->judul }}</td>
            <td>{{ $d->mitra_nm }}</td>
            <td>{{ $d->no_sp }}</td>
            <td>{{ $d->pks }}</td>
            <td>{{ $d->nama_step }}</td>
            <td>{{ $d->tgl_jatuh_tmp }} Hari <b>({{ date('Y-m-d', strtotime('+'. $d->tgl_jatuh_tmp. ' day', strtotime($d->tgl_s_pen))) }})</b></td>
            <td>{{ $d->jml_hri }}</td>
            <td>{{ $d->detail }}</td>
            <td>{{ $d->nama_modif ? $d->nama_modif .' ('. $d->modified_by .')' : '' }}</td>
            <td>
             <a class="btn btn-sm btn-info" target="_blank" style="color: #fff;" href="/get_detail_laporan/{{ $d->id }}"><i class="fe fe-eye fe-16"></i>&nbsp;Detail</a>
            </td>
           </tr>
           @endforeach
          </tbody>
         </table>
        </div>
        <div class="tab-pane fade" id="save_rekon">
         <table class="table table-striped table-bordered table-hover tableData">
          <thead class="thead-dark">
           <tr>
            <th class="hidden-xs">#</th>
            <th>Pekerjaan</th>
            <th>Nomor SP</th>
            <th>Nomor PKS</th>
            <th>Tanggal Dibuat</th>
            <th>Umur (Hari)</th>
            <th>Detail</th>
            <th>Update Terakhir</th>
            <th>Action</th>
           </tr>
          </thead>
          <tbody id="data_table4">
           @php $num = 1; @endphp
           @foreach($save_rekon as $d)
           <tr>
            <td class="hidden-xs">{{ $num++ }}</td>
            <td style="width: 22%;">{{ $d->judul }}</td>
            <td style="width: 22%;">{{ $d->no_sp }}</td>
            <td style="width: 20%;">{{ $d->pks }}</td>
            <td style="width: 15%;">{{ $d->created_at }}</td>
            <td>{{ $d->jml_hri }}</td>
            <td>{{ $d->detail }}</td>
            <td>{{ $d->nama_modif ? $d->nama_modif .' ('. $d->modified_by .')' : '' }}</td>
            <td>
             <a class="btn btn-sm btn-primary" href="/progress/{{ $d->id }}"><i class="fe fe-tool fe-16"></i> Detail</a>
             <a type="button" class="btn btn-sm btn-modal_me btn-warning" href="/rollback/{{ $d->id }}" style="margin-bottom: 5px;"><i class="fe fe-corner-up-left fe-16"></i>&nbsp;Rollback </a>
            </td>
           </tr>
           @endforeach
          </tbody>
         </table>
        </div>
        <div class="tab-pane fade" id="reject_regional">
         <table class="table table-striped table-bordered table-hover tableData">
          <thead class="thead-dark">
           <tr>
            <th class="hidden-xs">#</th>
            <th>Pekerjaan</th>
            <th>Mitra</th>
            <th>Nomor SP</th>
            <th>Nomor PKS</th>
            <th>TOC</th>
            <th>Umur (Hari)</th>
            <th>Tanggal Save</th>
            <th>Detail</th>
            <th>Update Terakhir</th>
            <th class="no-sort">Action</th>
           </tr>
          </thead>
          <tbody id="data_table4">
           @php $num = 1; @endphp
           @foreach($reject_rekon as $d)
           <tr>
            <td class="hidden-xs">{{ $num++ }}</td>
            <td>{{ $d->judul }}</td>
            <td>{{ $d->mitra_nm }}</td>
            <td>{{ $d->no_sp }}</td>
            <td>{{ $d->pks }}</td>
            <td>{{ $d->tgl_jatuh_tmp }} Hari <b>({{ date('Y-m-d', strtotime('+'. $d->tgl_jatuh_tmp. ' day', strtotime($d->tgl_s_pen))) }})</b></td>
            <td>{{ $d->jml_hri }}</td>
            <td>{{ $d->tgl_last_update }}</td>
            <td>{{ $d->detail }}</td>
            <td>{{ $d->nama_modif ? $d->nama_modif .' ('. $d->modified_by .')' : '' }}</td>
            <td>
             <a class="btn btn-warning" target="_blank" style="color: #fff;" href="/get_detail_laporan/{{ $d->id }}"><i class="fe fe-eye fe-16"></i>&nbsp;Detail</a>
            </td>
           </tr>
           @endforeach
          </tbody>
         </table>
        </div>
       </div>
      </div>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
@endsection
@section('footerS')
<script src="/js/moment.min.js"></script>
<script src="/js/jquery.timepicker.js"></script>
<script src="/js/daterangepicker.js"></script>
<script src="/js/jquery.dataTables.min.js"></script>
<script src="/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script type="text/javascript">
 $(function () {

  $('.date-picker').daterangepicker(
    {
        singleDatePicker: true,
        timePicker: false,
        showDropdowns: true,
        locale:
        {
            format: 'YYYY-MM-DD',
        }
    }
  );

  $.fn.digits = function () {
   return this.each(function () {
    $(this).text(
     $(this)
      .text()
      .replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")
    );
   });
  };

  // $('#status').select2({
  // 	data: {!! json_encode($select2) !!},
  // 	placeholder: 'Select Status',
  // 	width: '100%'
  // })

  $(".tableData").DataTable({
   autoWidth: true,
   columnDefs: [
    {
     targets: "no-sort",
     orderable: false,
    },
   ],
   lengthMenu: [
    [18, 27, 64, -1],
    [18, 27, 64, "All"],
   ],
  });

 });
</script>
@endsection