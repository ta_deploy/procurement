@extends('layout')
@section('title', 'Kelengkapan Data')
@section('headerS')
<link rel="stylesheet" href="/css/dataTables.bootstrap4.css">
<style>
	@media (min-width: 768px) {
		.modal-xl {
			width: 100%;
		max-width:1200px;
		}
	}

	th, td {
		text-align: center;
	}
</style>
@endsection
@section('content')
@if (Session::has('alerts'))
  @foreach(Session::get('alerts') as $alert)
    <div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
  @endforeach
@endif
<div class="modal fade" id="modal_comp" tabindex="-1" role="dialog" aria-labelledby="modal_compTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
				<h5 class="modal-title">Detail Pekerjaan <u><i><span id="judul_pekerjaan"></span></i></u></h5>
      </div>
      <div class="modal-body data_mdl" style=" overflow: hidden;">
        <div class="row">
					<div class="col-md-6">
						<div class="form-group row">
							<label class="col-form-label col-md-3">Project ID:</label>
							<div class="col-md-9">
								<textarea style="resize:none; font-weight: bold" cols='50' rows="3" class="form-control input-transparent pid" readonly></textarea>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-md-3">Jenis Pekerjaan:</label>
							<div class="col-md-9">
								<p class="form-control input-transparent" readonly><b class="jenis_work"></b></p>
							</div>
						</div>
						<div class="row border-top py-3">
							<div class="col-md-6">
								<div class="form-group">
									<label class="col-form-label">Surat Penetapan:</label>
									<textarea style="resize:none; font-weight: bold" cols='50' rows="2" class="form-control input-transparent surat_penetapan" readonly></textarea>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="col-form-label">Tanggal Surat Penetapan:</label>
									<p class="form-control input-transparent" readonly><b class="tgl_s_pen"></b></p>
								</div>
							</div>
						</div>
						<div class="row border-top py-3">
							<div class="col-md-6">
								<div class="form-group">
									<label class="col-form-label">Surat Pesanan:</label>
									<textarea style="resize:none; font-weight: bold" cols='50' rows="2" class="form-control input-transparent no_sp" readonly></textarea>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<label class="col-form-label">Due Date:</label>
									<p class="form-control input-transparent" readonly><b class="jt"></b></p>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<label class="col-form-label">Durasi:</label>
									<p class="form-control input-transparent" readonly><b class="durasi"></b></p>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-md-3">Lokasi:</label>
							<div class="col-md-9">
								<p class="form-control input-transparent" readonly><b class="lokasi"></b></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-md-3">PKS:</label>
							<div class="col-md-9">
								<p class="form-control input-transparent" readonly><b class="pks"></b></p>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group row">
							<label class="col-form-label col-md-3">Mitra:</label>
							<div class="col-md-9">
								<p class="form-control input-transparent" readonly><b class="nama_company"></b></p>
							</div>
						</div>
						<div class="row border-top py-3">
							<div class="col-md-6">
								<div class="form-group">
									<label class="col-form-label">Total Materail:</label>
									<p class="form-control input-transparent" readonly><b class="total_material"></b></p>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="col-form-label">Total Jasa:</label>
									<p class="form-control input-transparent" readonly><b class="total_jasa"></b></p>
								</div>
							</div>
						</div>
						<div class="row border-top py-3">
							<div class="col-md-4">
								<div class="form-group">
									<label class="col-form-label">Grand Total:</label>
									<p class="form-control input-transparent" readonly><b class="gd"></b></p>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label class="col-form-label">PPN:</label>
									<p class="form-control input-transparent" readonly><b class="gd_ppn"></b></p>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label class="col-form-label">Grand Total + PPN:</label>
									<p class="form-control input-transparent" readonly><b class="total"></b></p>
								</div>
							</div>
							<div class="lanjut_full_data col-md-12" style="display:none;">
								<a type="button" class="btn btn-primary btn-block btn_full" href="" target="_blank"><i class="fe fe-arrow-right fe-16"></i>&nbsp;Isi Kelengkapan Data</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
		</div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row justify-content-center">
    <div class="col-12">
      <h2 class="page-title">List Kelengkapan Rekonsiliasi</h2>
      <div class="row">
        <div class="col-md-12">
          <div class="card shadow">
            <div class="card-body table-responsive">
              <ul class="nav nav-tabs mb-3" id="myTab" role="tablist">
								<li class="nav-item"><a href="#berkas_get" class="nav-link active" data-toggle="tab" aria-expanded="false">Proses Rekon &nbsp;{!! count($berkas_get) != 0 ? "<span class='badge badge-primary'>".count($berkas_get)."</span>" : '' !!}</a></li>
								<li class="nav-item"><a href="#reject_berkas_area" class="nav-link" data-toggle="tab" aria-expanded="false">Rekon Area Ditolak &nbsp;{!! count($reject_berkas_area) != 0 ? "<span class='badge badge-warning'>".count($reject_berkas_area)."</span>" : '' !!}</a></li>
              </ul>
							<div id="myTabContentLeft" class="tab-content">
								<div class="tab-pane fade show active" id="berkas_get">
									<table class="table table-striped table-bordered table-hover">
										<thead class="thead-dark">
											<tr>
												<th class="hidden-xs">#</th>
												<th>Pekerjaan</th>
												<th>Mitra</th>
												<th>Nomor SP</th>
												<th>Nomor Penetapan</th>
												<th>Nomor PKS</th>
												<th>Tanggal Dibuat</th>
												<th>Umur (Hari)</th>
												<th>Keterangan Terakhir</th>
												<th>Update Terakhir</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody id="data_table2">
											@php
											$num = 1;
											@endphp
											@foreach($berkas_get as $d)
											<tr>
												<td class="hidden-xs">{{ $num++ }}</td>
												<td style="width: 22%;">{{ $d->judul }}</td>
												<td style="width: 22%;">{{ $d->nama_company }}</td>
												<td style="width: 22%;">{{ $d->no_sp }}</td>
												<td style="width: 22%;">{{ $d->surat_penetapan }}</td>
												<td style="width: 20%;">{{ $d->pks }}</td>
												<td style="width: 15%;">{{ $d->created_at }}</td>
												<td style="width: 15%;">{{ $d->jml_hri }}</td>
												<td style="width: 15%;">{{ $d->detail }}</td>
												<td>{{ $d->nama_modif ? $d->nama_modif .' ('. $d->modified_by .')' : '' }}</td>
												<td>
													@if($d->pekerjaan == "PSB")
													<div class="col-md-12">
														<a class="btn btn-sm btn-primary modal_comp" data-toggle="modal" data-target="#modal_comp" style="color: #fff; margin-bottom: 5px;" data-id="{{ $d->id }}"><i class="fe fe-eye"></i>&nbsp;Detail</a>
													</div>
														<div class="col-md-12">
															<div class="row text-center">
																<div class="col-md-3">
																	<a class="btn btn-sm btn-primary" href="/Admin/download/filepsb/surat_pesanan/{{ $d->id }}/download" data-toggle="tooltip" data-placement="top" title="Surat Pesanan (PSB)"><i class="fe fe-download"></i></a>
																</div>
																&nbsp;
																<div class="col-md-3">
																	<a class="btn btn-sm btn-primary" href="/Admin/download/filepsb/justifikasi/{{ $d->id }}/download" data-toggle="tooltip" data-placement="top" title="Justifikasi & Rincian SSL (PSB)"><i class="fe fe-download"></i></a>
																</div>
															</div>
														</div>
													@endif
													@if($d->pekerjaan != "PSB")
														<a class="btn btn-sm btn-primary modal_comp" data-toggle="modal" data-target="#modal_comp" style="color: #fff; margin-bottom: 5px;" data-id="{{ $d->id }}"><i class="fe fe-eye"></i>&nbsp;Detail</a>
														<a class="btn btn-sm btn-info" style="margin-bottom: 5px;" href="/download/full_rar/{{ $d->id }}"><i class="fe fe-download"></i>&nbsp;Download</a>
														<a class="btn btn-sm btn-warning" href="/rollback/{{ $d->id }}"><i class="fe fe-corner-up-left fe-16"></i>&nbsp;Rollback </a>
													@endif
												</td>
											</tr>
											@endforeach
										</tbody>
									</table>
								</div>
								<div class="tab-pane fade" id="reject_berkas_area">
									<table class="table table-striped table-bordered table-hover">
										<thead class="thead-dark">
											<tr>
												<th class="hidden-xs">#</th>
												<th>Pekerjaan</th>
												<th>Nomor SP</th>
												<th>Nomor PKS</th>
												<th>Tanggal Dibuat</th>
												<th>Umur (Hari)</th>
												<th>Detail</th>
												<th>Update Terakhir</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody id="data_table4">
											@php
											$num = 1;
											@endphp
											@foreach($reject_berkas_area as $d)
											<tr>
												<td class="hidden-xs">{{ $num++ }}</td>
												<td style="width: 22%;">{{ $d->judul }}</td>
												<td style="width: 22%;">{{ $d->no_sp }}</td>
												<td style="width: 20%;">{{ $d->pks }}</td>
												<td style="width: 15%;">{{ $d->created_at }}</td>
												<td style="width: 15%;">{{ $d->jml_hri }}</td>
												<td style="width: 15%;">{{ $d->detail }}</td>
												<td>{{ $d->nama_modif ? $d->nama_modif .' ('. $d->modified_by .')' : '' }}</td>
												<td><a class="btn btn-primary modal_comp" data-toggle="modal" data-target="#modal_comp" style="color: #fff" data-id="{{ $d->id }}"><i class="fe fe-eye fe-16"></i> Detail</a></td>
											</tr>
											@endforeach
										</tbody>
									</table>
								</div>
							</div>
            </div>
          </div>
        </div>
      </div> <!-- end section -->
    </div> <!-- .col-12 -->
  </div> <!-- .row -->
</div>
@endsection
@section('footerS')
<script src='/js/jquery.dataTables.min.js'></script>
<script src='/js/dataTables.bootstrap4.min.js'></script>
<script type="text/javascript">
	$(function(){
		$.fn.digits = function(){
			return this.each(function(){
					$(this).text( $(this).text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") );
			});
		}

		$('.table').DataTable({
			autoWidth: true,
			columnDefs: [
				{
					targets: 'no-sort',
					orderable: false
				}
			],
			lengthMenu: [
				[16, 32, 64, -1],
				[16, 32, 64, "All"]
			]
		});

		$('.modal_comp').on('click', function (){
			var data_id = $(this).attr('data-id');
			$.ajax({
					url:"/get_ajx/get/detail/sp",
					type:"GET",
					data: {
						id : data_id,
						status: 'SP'
					},
					dataType: 'json',
					success: (function(data){
						// console.log(data, data.step_id, $.inArray(data.step_id, [11, 17]) !== -1)
						if(($.inArray(data.step_id, [17, 31]) !== -1) ){
							$('.detail').css({
								'display': 'flex'
							})
						}

						if(($.inArray(data.step_id, [11, 17, 31]) !== -1) ){
							$('.lanjut_full_data').css({
								'display' : 'flex'
							})

							$(".btn_full").attr('href', '/progress/'+data.id);
						}

						var tgl = new Date(data.tgl_sp);
						console.log(data)
						tgl.setDate(tgl.getDate() + data.tgl_jatuh_tmp);
						$('#judul_pekerjaan').html(data.judul);
						$('.surat_penetapan').html(data.surat_penetapan);
						$('.tgl_s_pen').html(data.tgl_s_pen);
						$('.no_sp').html(data.no_sp);
						$('.tgl_sp').html(data.tgl_sp);
						$('.jt').html(data.durasi);
						$('.durasi').html(data.tgl_jatuh_tmp+ ' Hari');
						$('.lokasi').html(data.lokasi ?? data.witel);
						$('.pks').html(data.pks);
						$('.nama_company').html(data.nama_company);
						$('.total_material').html( (data.total_material_rekon != 0 ? data.total_material_rekon : data.total_material_sp) );
						$('.total_jasa').html( (data.total_jasa_rekon != 0 ? data.total_jasa_rekon : data.total_jasa_sp) );
						$('.gd').html( (data.gd_rekon != 0 ? data.gd_rekon : data.gd_sp) );
						$('.budget_req').html(data.budget_req);
						$('.gd_ppn').html((data.gd_rekon != 0 ? data.gd_ppn_rekon : data.gd_ppn_sp) );
						$('.total').html((data.gd_rekon != 0 ? data.total_rekon : data.total_sp) );
						$('.pid').html(data.id_project);
						$('.jenis_work').html(data.pekerjaan + ' | ' + data.jenis_work + ' | ' + data.jenis_kontrak);
						$('.rar_down').attr('href', '/download/full_rar/'+data.id);

						$(".nama_company, .total_material, .total_jasa, .gd, .gd_ppn, .total, .budget_req").digits();
					}),
				})
		});


	});
</script>
@endsection