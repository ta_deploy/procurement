@extends('layout')
@section('title', 'List Verifikasi Rekon')
@section('headerS')
<link rel="stylesheet" href="/css/dataTables.bootstrap4.css" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

<style>
 @media (min-width: 768px) {
  .modal-xl {
   width: 100%;
   max-width: 1200px;
  }
 }
</style>
@endsection
@section('content')
@if (Session::has('alerts'))
@foreach(Session::get('alerts') as $alert)
<div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
@endforeach
@endif
<div class="modal fade" id="modal_comp" tabindex="-1" role="dialog" aria-labelledby="modal_compTitle" aria-hidden="true">
 <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
  <div class="modal-content">
   <div class="modal-header">
    <h5 class="modal-title">
     Detail Pekerjaan
     <u>
      <i><span id="judul_pekerjaan"></span></i>
     </u>
    </h5>
   </div>
   <div class="modal-body data_mdl" style="overflow: hidden;">
    <div class="row">
     <div class="col-md-6">
      <div class="form-group row">
       <label class="col-form-label col-md-3">Project ID:</label>
       <div class="col-md-9">
        <textarea style="resize: none; font-weight: bold;" cols="50" rows="3" class="form-control input-transparent pid" readonly></textarea>
       </div>
      </div>
      <div class="form-group row">
       <label class="col-form-label col-md-3">Jenis Pekerjaan:</label>
       <div class="col-md-9">
        <p class="form-control input-transparent" readonly><b class="jenis_work"></b></p>
       </div>
      </div>
      <div class="row border-top">
       <div class="col-md-6">
        <div class="form-group">
         <label class="col-form-label">Surat Penetapan:</label>
         <textarea style="resize: none; font-weight: bold;" cols="50" rows="2" class="form-control input-transparent surat_penetapan" readonly></textarea>
        </div>
       </div>
       <div class="col-md-6">
        <div class="form-group">
         <label class="col-form-label">Tanggal Surat Penetapan:</label>
         <p class="form-control input-transparent" readonly><b class="tgl_s_pen"></b></p>
        </div>
       </div>
      </div>
      <div class="row border-top">
       <div class="col-md-6">
        <div class="form-group">
         <label class="col-form-label">Surat Pesanan:</label>
         <textarea style="resize: none; font-weight: bold;" cols="50" rows="2" class="form-control input-transparent no_sp" readonly></textarea>
        </div>
       </div>
       <div class="col-md-3">
        <div class="form-group">
         <label class="col-form-label">Due Date:</label>
         <p class="form-control input-transparent" readonly><b class="jt"></b></p>
        </div>
       </div>
       <div class="col-md-3">
        <div class="form-group">
         <label class="col-form-label">Durasi:</label>
         <p class="form-control input-transparent" readonly><b class="durasi"></b></p>
        </div>
       </div>
      </div>
      <div class="form-group row">
       <label class="col-form-label col-md-3">Lokasi:</label>
       <div class="col-md-9">
        <p class="form-control input-transparent" readonly><b class="lokasi"></b></p>
       </div>
      </div>
      <div class="form-group row">
       <label class="col-form-label col-md-3">PKS:</label>
       <div class="col-md-9">
        <p class="form-control input-transparent" readonly><b class="pks"></b></p>
       </div>
      </div>
     </div>
     <div class="col-md-6">
      <div class="form-group row">
       <label class="col-form-label col-md-3">Mitra:</label>
       <div class="col-md-9">
        <p class="form-control input-transparent" readonly><b class="nama_company"></b></p>
       </div>
      </div>
      <div class="row border-top">
       <div class="col-md-6">
        <div class="form-group">
         <label class="col-form-label">Total Materail:</label>
         <p class="form-control input-transparent" readonly><b class="total_material"></b></p>
        </div>
       </div>
       <div class="col-md-6">
        <div class="form-group">
         <label class="col-form-label">Total Jasa:</label>
         <p class="form-control input-transparent" readonly><b class="total_jasa"></b></p>
        </div>
       </div>
      </div>
      <div class="row border-top">
       <div class="col-md-4">
        <div class="form-group">
         <label class="col-form-label">Grand Total:</label>
         <p class="form-control input-transparent" readonly><b class="gd"></b></p>
        </div>
       </div>
       <div class="col-md-4">
        <div class="form-group">
         <label class="col-form-label">Grand Total PPN:</label>
         <p class="form-control input-transparent" readonly><b class="gd_ppn"></b></p>
        </div>
       </div>
       <div class="col-md-4">
        <div class="form-group">
         <label class="col-form-label">Grand Total + PPN:</label>
         <p class="form-control input-transparent" readonly><b class="total"></b></p>
        </div>
       </div>
      </div>
      <a class="btn btn-primary btn-block rar_down" href=""><i class="fe fe-download fe-16"></i> File Pekerjaan</a>
     </div>
     <div class="upload_re row col-md-12" style="display: block;">
      <form enctype="multipart/form-data" action="/progressList/{{ Request::segment(2) }}" method="post" class="form-horizontal form-label-right">
       <div class="col-md-12">
        {{ csrf_field() }}
        <div class="form-group row">
         <label class="col-form-label col-md-3" for="status">Keputusan</label>
         <div class="col-md-9">
          <input type="hidden" id="id_boq_up" name="id_boq_up" value="" />
          <select name="status" id="status" style="width: 100%;"> </select>
         </div>
        </div>
        <div class="form-group row upload_aman">
         <label class="col-form-label col-md-3" for="detail">Catatan</label>
         <div class="col-md-9">
          <textarea style="resize: none;" rows="2" name="detail" id="detail" class="form-control input-transparent"></textarea>
         </div>
        </div>
        <div class="form-group row upload_aman">
         <label for="upload_aman_lamp" class="col-form-label col-md-3">File Amanademen</label>
         <div class="col-md-9">
          <input type="file" id="upload_aman_lamp" name="upload_aman_lamp" class="form-control-file" />
         </div>
        </div>
        <div class="form-group row">
         <div class="col-md-12">
          <button type="submit" class="btn btn-primary btn_submit btn-block upload_aman">Submit</button>
          <a type="button" class="btn btn-primary btn_submit btn-block progress_btn">Check Designator</a>
         </div>
        </div>
       </div>
      </form>
     </div>
    </div>
   </div>
   <div class="modal-footer">
    <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
   </div>
  </div>
 </div>
</div>
<div class="container-fluid">
 <div class="row justify-content-center">
  <div class="col-12">
   <h2 class="page-title">List Verifikasi Rekon</h2>
   <p class="lead text-muted">Mengawal Pekerjaan Rekon</p>
   <div class="row">
    <div class="col-md-12">
     <div class="card shadow">
      <div class="card-body table-responsive">
       <ul class="nav nav-tabs mb-3" id="myTab" role="tablist">
        <li class="active">
         <a href="#ready_rekon" id="data2" class="nav-link active" data-toggle="tab" aria-expanded="false"> Check Rekon&nbsp; {!! count($ready_rekon) != 0 ? "<span class='badge badge-success'>".count($ready_rekon)."</span>" : '' !!} </a>
        </li>
        <li class="">
         <a href="#reject_rekon" id="data2" class="nav-link" data-toggle="tab" aria-expanded="false"> Tolak Rekon&nbsp; {!! count($reject_rekon) != 0 ? "<span class='badge badge-danger'>".count($reject_rekon)."</span>" : '' !!} </a>
        </li>
       </ul>
       <div id="myTabContentLeft" class="tab-content">
        <div class="tab-pane fade show active table-responsive" id="ready_rekon">
         <table class="table table-striped table-bordered table-hover tableData">
          <thead class="thead-dark">
           <tr class="text-center">
            <th class="hidden-xs">#</th>
            <th>Pekerjaan</th>
            <th>Mitra</th>
            <th>Nomor SP</th>
            <th>Nomor PKS</th>
            <th>Tanggal Dibuat</th>
            <th>Umur (Hari)</th>
            <th>Keterangan Terakhir</th>
            <th>Update Terakhir</th>
            <th>Action</th>
           </tr>
          </thead>
          <tbody id="data_table1">
           @php $num = 1; @endphp
           @foreach($ready_rekon as $d)
           <tr class="text-center">
            <td class="hidden-xs">{{ $num++ }}</td>
            <td style="width: 22%;">{{ $d->judul }}</td>
            <td style="width: 22%;">{{ $d->nama_company }}</td>
            <td style="width: 22%;">{{ $d->no_sp }}</td>
            <td style="width: 20%;">{{ $d->pks }}</td>
            <td style="width: 15%;">{{ $d->created_at }}</td>
            <td style="width: 15%;">{{ $d->jml_hri }}</td>
            <td style="width: 15%;">{{ $d->detail }}</td>
            <td>{{ $d->nama_modif ? $d->nama_modif .' ('. $d->modified_by .')' : '' }}</td>
            <td>
             @if ($d->pekerjaan <> "PSB")
             <a class="btn btn-sm btn-primary" href="/progress/{{ $d->id }}" style="color: #fff; margin-bottom: 5px;" data-id="{{ $d->id }}"><i class="fe fe-edit fe-16"></i>&nbsp;Edit</a>
             <a type="button" class="btn btn-sm btn-warning" href="/rollback/{{ $d->id }}" style="margin-bottom: 5px;"><i class="fe fe-corner-up-left fe-16"></i>&nbsp;Rollback </a>
             @endif
             @if($d->pekerjaan == "PSB") &nbsp;
             <div class="modal fade" id="verticalModal-{{ $d->id }}" tabindex="-1" role="dialog" aria-labelledby="verticalModalTitle-{{ $d->id }}" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
               <div class="modal-content">
                <div class="modal-header">
                 <h5 class="modal-title" id="verticalModalTitle-{{ $d->id }}">Pengisian Nomor Surat dan Lainnya ({{ $d->id }})</h5>
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                 </button>
                </div>
                <div class="modal-body">
                 <form id="form_pengisian_surat_{{ $d->id }}" class="row" method="post" action="/Admin/save/filepsb" enctype="multipart/form-data" autocomplete="off">
                  {{ csrf_field() }}
                  <input type="hidden" name="id" value="{{ $d->id }}" />
                  <input type="hidden" name="step" value="verif_boq" />
                  <input type="hidden" name="id_file" value="dokumenpelengkap" />
                  <input type="hidden" name="periode_kegiatan" value="{{ $d->periode_kegiatan }}" />
                  <input type="hidden" name="rekon_periode1" value="{{ $d->rekon_periode1 }}" />
                  <input type="hidden" name="rekon_periode2" value="{{ $d->rekon_periode2 }}" />

                  <div class="col-md-12">
                   <div class="form-row">
                    <div class="form-group col-md-12 table-responsive">
                     <table class="table table-hover table-bordered table-sm tablePengisianSurat" style="text-align: center;">
                      <thead>
                       <tr>
                        <th style="color: black;">TYPE TRANSAKSI</th>
                        <th style="color: black;">JENIS LAYANAN</th>
                        <th style="color: black;">HSS JASA + INSENTIF</th>
                        <th style="color: black;">JUMLAH SSL</th>
                       </tr>
                      </thead>
                      <tbody>
                       <tr>
                        <td rowspan="5">
                         Infrastructure New Sales Fiber<br />
                         KU + SURVEY
                        </td>
                       </tr>
                       <tr>
                        <td>0P - 1P (Tlp/Int)</td>
                        <td>
                         <input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->p1)) }}" style="text-align: center;" name="p1_survey_hss" id="p1_survey_hss" readonly />
                        </td>
                        <td>
                         <input
                          type="text"
                          oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');"
                          style="text-align: center;"
                          name="p1_survey_ssl"
                          id="p1_survey_ssl"
                          value="{{ $d->p1_survey_ssl ? : '0' }}"
                          required
                         />
                        </td>
                       </tr>
                       <tr>
                        <td>0P - 2P (Tlp+Int)</td>
                        <td>
                         <input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->p2_inet_voice)) }}" style="text-align: center;" name="p2_tlpint_survey_hss" id="p2_tlpint_survey_hss" readonly />
                        </td>
                        <td>
                         <input
                          type="text"
                          oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');"
                          style="text-align: center;"
                          name="p2_tlpint_survey_ssl"
                          id="p2_tlpint_survey_ssl"
                          value="{{ $d->p2_tlpint_survey_ssl ? : '0' }}"
                          required
                         />
                        </td>
                       </tr>
                       <tr>
                        <td>0P - 2P (Int+IPTV)</td>
                        <td>
                         <input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->p2_inet_iptv)) }}" style="text-align: center;" name="p2_inet_iptv_hss" id="p2_inet_iptv_hss" readonly />
                        </td>
                        <td>
                         <input
                          type="text"
                          oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');"
                          style="text-align: center;"
                          name="p2_intiptv_survey_ssl"
                          id="p2_intiptv_survey_ssl"
                          value="{{ $d->p2_intiptv_survey_ssl ? : '0' }}"
                          required
                         />
                        </td>
                       </tr>
                       <tr>
                        <td>0P - 3P (Tlp+Int+IPTV)</td>
                        <td>
                         <input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->p3)) }}" style="text-align: center;" name="p3_survey_hss" id="p3_survey_hss" readonly />
                        </td>
                        <td>
                         <input
                          type="text"
                          oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');"
                          style="text-align: center;"
                          name="p3_survey_ssl"
                          id="p3_survey_ssl"
                          value="{{ $d->p3_survey_ssl ? : '0' }}"
                          required
                         />
                        </td>
                       </tr>
                       <tr>
                        <td rowspan="5">
                         Infrastructure New Sales Fiber<br />
                         KU
                        </td>
                       </tr>
                       <tr>
                        <td>0P - 1P (Tlp/Int)</td>
                        <td>
                         <input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->p1_ku)) }}" style="text-align: center;" name="p1_hss" id="p1_hss" readonly />
                        </td>
                        <td>
                         <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" style="text-align: center;" name="p1_ssl" id="p1_ssl" value="{{ $d->p1_ssl ? : '0' }}" required />
                        </td>
                       </tr>
                       {{--
                       <tr>
                        <td>0P - 2P (OLD VERSION)</td>
                        <td>
                         <input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->p2)) }}" style="text-align: center;" name="p2_hss" id="p2_hss" readonly />
                        </td>
                        <td>
                         <input
                          type="text"
                          oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');"
                          style="text-align: center;"
                          name="p2_ssl"
                          id="p2_ssl"
                          value="{{ $d->p2_ssl ? : '0' }}"
                          required
                         />
                        </td>
                       </tr>
                       --}}
                       <tr>
                        <td>0P - 2P (Tlp+Int)</td>
                        <td>
                         <input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->p2_inet_voice_ku)) }}" style="text-align: center;" name="p2_tlpint_hss" id="p2_tlpint_hss" readonly />
                        </td>
                        <td>
                         <input
                          type="text"
                          oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');"
                          style="text-align: center;"
                          name="p2_tlpint_ssl"
                          id="p2_tlpint_ssl"
                          value="{{ $d->p2_tlpint_ssl ? : '0' }}"
                          required
                         />
                        </td>
                       </tr>
                       <tr>
                        <td>0P - 2P (Int+IPTV)</td>
                        <td>
                         <input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->p2_inet_iptv_ku)) }}" style="text-align: center;" name="p2_intiptv_hss" id="p2_intiptv_hss" readonly />
                        </td>
                        <td>
                         <input
                          type="text"
                          oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');"
                          style="text-align: center;"
                          name="p2_intiptv_ssl"
                          id="p2_intiptv_ssl"
                          value="{{ $d->p2_intiptv_ssl ? : '0' }}"
                          required
                         />
                        </td>
                       </tr>
                       <tr>
                        <td>0P - 3P (Tlp+Int+IPTV)</td>
                        <td>
                         <input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->p3_ku)) }}" style="text-align: center;" name="p3_hss" id="p3_hss" readonly />
                        </td>
                        <td>
                         <input
                          type="text"
                          oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');"
                          style="text-align: center;"
                          name="p3_ssl"
                          id="p3_ssl"
                          value="{{ $d->p3_ssl ? : '0' }}"
                          required
                         />
                        </td>
                       </tr>
                       <tr>
                        <td rowspan="4">MIGRASI</td>
                       </tr>
                       <tr>
                        <td>1P ke 2P</td>
                        <td>
                         <input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->migrasi_1p)) }}" style="text-align: center;" name="migrasi_service_1p2p_hss" id="migrasi_service_1p2p_hss" readonly />
                        </td>
                        <td>
                         <input
                          type="text"
                          oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');"
                          style="text-align: center;"
                          name="migrasi_service_1p2p_ssl"
                          id="migrasi_service_1p2p_ssl"
                          value="{{ $d->migrasi_service_1p2p_ssl ? : '0' }}"
                          required
                         />
                        </td>
                       </tr>
                       <tr>
                        <td>1P ke 3P</td>
                        <td>
                         <input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->migrasi_2p)) }}" style="text-align: center;" name="migrasi_service_1p3p_hss" id="migrasi_service_1p3p_hss" readonly />
                        </td>
                        <td>
                         <input
                          type="text"
                          oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');"
                          style="text-align: center;"
                          name="migrasi_service_1p3p_ssl"
                          id="migrasi_service_1p3p_ssl"
                          value="{{ $d->migrasi_service_1p3p_ssl ? : '0' }}"
                          required
                         />
                        </td>
                       </tr>
                       <tr>
                        <td>2P ke 3P</td>
                        <td>
                         <input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->migrasi_3p)) }}" style="text-align: center;" name="migrasi_service_2p3p_hss" id="migrasi_service_2p3p_hss" readonly />
                        </td>
                        <td>
                         <input
                          type="text"
                          oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');"
                          style="text-align: center;"
                          name="migrasi_service_2p3p_ssl"
                          id="migrasi_service_2p3p_ssl"
                          value="{{ $d->migrasi_service_2p3p_ssl ? : '0' }}"
                          required
                         />
                        </td>
                       </tr>
                       <tr>
                        <td rowspan="12">ADDON</td>
                       </tr>
                       <tr>
                        <td>LME WiFi Provisioning Type-1 (PT1) KU</td>
                        <td>
                         <input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->lme_wifi)) }}" style="text-align: center;" name="lme_wifi_pt1_hss" id="lme_wifi_pt1_hss" readonly />
                        </td>
                        <td>
                         <input
                          type="text"
                          oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');"
                          style="text-align: center;"
                          name="lme_wifi_pt1_ssl"
                          id="lme_wifi_pt1_ssl"
                          value="{{ $d->lme_wifi_pt1_ssl ? : '0' }}"
                          required
                         />
                        </td>
                       </tr>
                       <tr>
                        <td>LME-AP-Indoor</td>
                        <td>
                         <input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->lme_ap_indoor)) }}" style="text-align: center;" name="lme_ap_indoor_hss" id="lme_ap_indoor_hss" readonly />
                        </td>
                        <td>
                         <input
                          type="text"
                          oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');"
                          style="text-align: center;"
                          name="lme_ap_indoor_ssl"
                          id="lme_ap_indoor_ssl"
                          value="{{ $d->lme_ap_indoor_ssl ? : '0' }}"
                          required
                         />
                        </td>
                       </tr>
                       <tr>
                        <td>LME-AP-Outdoor</td>
                        <td>
                         <input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->lme_ap_outdoor)) }}" style="text-align: center;" name="lme_ap_outdoor_hss" id="lme_ap_outdoor_hss" readonly />
                        </td>
                        <td>
                         <input
                          type="text"
                          oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');"
                          style="text-align: center;"
                          name="lme_ap_outdoor_ssl"
                          id="lme_ap_outdoor_ssl"
                          value="{{ $d->lme_ap_outdoor_ssl ? : '0' }}"
                          required
                         />
                        </td>
                       </tr>
                       <tr>
                        <td>Smart Camera Only</td>
                        <td>
                         <input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->smartcam_only)) }}" style="text-align: center;" name="indihome_smart_hss" id="indihome_smart_hss" readonly />
                        </td>
                        <td>
                         <input
                          type="text"
                          oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');"
                          style="text-align: center;"
                          name="indihome_smart_ssl"
                          id="indihome_smart_ssl"
                          value="{{ $d->indihome_smart_ssl ? : '0' }}"
                          required
                         />
                        </td>
                       </tr>
                       <tr>
                        <td>Instalasi PLC</td>
                        <td>
                         <input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->plc)) }}" style="text-align: center;" name="plc_hss" id="plc_hss" readonly />
                        </td>
                        <td>
                         <input
                          type="text"
                          oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');"
                          style="text-align: center;"
                          name="plc_ssl"
                          id="plc_ssl"
                          value="{{ $d->plc_ssl ? : '0' }}"
                          required
                         />
                        </td>
                       </tr>
                       <tr>
                        <td>IKR Add-on STB Tambahan</td>
                        <td>
                         <input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->stb_tambahan)) }}" style="text-align: center;" name="ikr_addon_stb_hss" id="ikr_addon_stb_hss" readonly />
                        </td>
                        <td>
                         <input
                          type="text"
                          oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');"
                          style="text-align: center;"
                          name="ikr_addon_stb_ssl"
                          id="ikr_addon_stb_ssl"
                          value="{{ $d->ikr_addon_stb_ssl ? : '0' }}"
                          required
                         />
                        </td>
                       </tr>
                       <tr>
                        <td>Change STB</td>
                        <td>
                         <input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->change_stb)) }}" style="text-align: center;" name="change_stb_hss" id="change_stb_hss" readonly />
                        </td>
                        <td>
                         <input
                          type="text"
                          oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');"
                          style="text-align: center;"
                          name="change_stb_ssl"
                          id="change_stb_ssl"
                          value="{{ $d->change_stb_ssl ? : '0' }}"
                          required
                         />
                        </td>
                       </tr>
                       <tr>
                        <td>Penggantian ONT Premium</td>
                        <td>
                         <input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->ont_premium)) }}" style="text-align: center;" name="ont_premium_hss" id="ont_premium_hss" readonly />
                        </td>
                        <td>
                         <input
                          type="text"
                          oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');"
                          style="text-align: center;"
                          name="ont_premium_ssl"
                          id="ont_premium_ssl"
                          value="{{ $d->ont_premium_ssl ? : '0' }}"
                          required
                         />
                        </td>
                       </tr>
                       <tr>
                        <td>Instalalasi Wifi Extender</td>
                        <td>
                         <input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->wifiext)) }}" style="text-align: center;" name="wifiextender_hss" id="wifiextender_hss" readonly />
                        </td>
                        <td>
                         <input
                          type="text"
                          oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');"
                          style="text-align: center;"
                          name="wifiextender_ssl"
                          id="wifiextender_ssl"
                          value="{{ $d->wifiextender_ssl ? : '0' }}"
                          required
                         />
                        </td>
                       </tr>
                       <tr>
                        <td>Insert Tiang 7</td>
                        <td>
                         <input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->insert_tiang)) }}" style="text-align: center;" name="pu_s7_140_hss" id="pu_s7_140_hss" readonly />
                        </td>
                        <td>
                         <input
                          type="text"
                          oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');"
                          style="text-align: center;"
                          name="pu_s7_140_ssl"
                          id="pu_s7_140_ssl"
                          value="{{ $d->pu_s7_140_ssl ? : '0' }}"
                          required
                         />
                        </td>
                       </tr>
                       <tr>
                        <td>Insert Tiang 9</td>
                        <td>
                         <input type="text" class="hss_jasa" value="{{ str_replace(',', '.', number_format($hss_witel->insert_tiang_9)) }}" style="text-align: center;" name="pu_s9_140_hss" id="pu_s9_140_hss" readonly />
                        </td>
                        <td>
                         <input
                          type="text"
                          oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');"
                          style="text-align: center;"
                          name="pu_s9_140_ssl"
                          id="pu_s9_140_ssl"
                          value="{{ $d->pu_s9_140_ssl ? : '0' }}"
                          required
                         />
                        </td>
                       </tr>
                      </tbody>
                     </table>
                    </div>
                    <div class="form-group col-md-12">
                     <button class="btn btn-block btn_submit btn-primary">Create</button>
                    </div>
                   </div>
                  </div>
                 </form>
                </div>
                <div class="modal-footer">
                 <button type="button" class="btn mb-2 btn-secondary" data-dismiss="modal">Close</button>
                </div>
               </div>
              </div>
             </div>

             <div class="col-md-12">
              <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" style="margin-bottom: 5px;" data-target="#verticalModal-{{ $d->id }}"><i class="fe fe-edit"></i>&nbsp;Input</button>
             </div>

             <div class="col-md-12">
              <div class="row text-center">
               <div class="col-md-3">
                <a class="btn btn-sm btn-primary" href="/Admin/download/filepsb/surat_pesanan/{{ $d->id }}/download" data-toggle="tooltip" data-placement="top" title="Surat Pesanan (PSB)">
                 <i class="fe fe-download"></i>
                </a>
               </div>
               &nbsp;
               <div class="col-md-3">
                <a class="btn btn-sm btn-primary" href="/Admin/download/filepsb/justifikasi/{{ $d->id }}/download" data-toggle="tooltip" data-placement="top" title="Justifikasi & Rincian SSL (PSB)">
                 <i class="fe fe-download"></i>
                </a>
               </div>
              </div>
             </div>
             @endif
            </td>
           </tr>
           @endforeach
          </tbody>
         </table>
        </div>
        <div class="tab-pane fade" id="reject_rekon">
         <table class="table table-striped table-bordered table-hover tableData">
          <thead class="thead-dark">
           <tr class="text-center">
            <th class="hidden-xs">#</th>
            <th>Pekerjaan</th>
            <th>Nomor SP</th>
            <th>Nomor PKS</th>
            <th>Tanggal Dibuat</th>
            <th>Umur (Hari)</th>
            <th>Detail</th>
            <th>Update Terakhir</th>
            <th>Action</th>
           </tr>
          </thead>
          <tbody id="data_table2">
           @php $num = 1; @endphp
           @foreach($reject_rekon as $d)
           <tr class="text-center">
            <td class="hidden-xs">{{ $num++ }}</td>
            <td style="width: 22%;">{{ $d->judul }}</td>
            <td style="width: 22%;">{{ $d->no_sp }}</td>
            <td style="width: 20%;">{{ $d->pks }}</td>
            <td style="width: 15%;">{{ $d->created_at }}</td>
            <td style="width: 15%;">{{ $d->jml_hri }}</td>
            <td style="width: 15%;">{{ $d->detail }}</td>
            <td>{{ $d->nama_modif ? $d->nama_modif .' ('. $d->modified_by .')' : '' }}</td>
            <td>
             <a class="btn btn-sm btn-primary modal_comp" data-toggle="modal" data-target="#modal_comp" style="color: #fff; margin-bottom: 10px;" data-id="{{ $d->id }}"> <i class="fe fe-eye fe-16"></i>&nbsp;Detail </a>
             <a type="button" class="btn btn-sm btn-warning" href="/rollback/{{ $d->id }}"><i class="fe fe-corner-up-left fe-16"></i>&nbsp;Rollback </a>
            </td>
           </tr>
           @endforeach
          </tbody>
         </table>
        </div>
       </div>
      </div>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
@endsection
@section('footerS')
<script src="/js/jquery.dataTables.min.js"></script>
<script src="/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script type="text/javascript">
 $(function(){
 	$.fn.digits = function(){
 		return this.each(function(){
 				$(this).text( $(this).text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") );
 		});
 	}

 	var select_arr = {!! json_encode($select2) !!};

 	select_arr.push({
 		id: 'rework',
 		text: 'Input rekon'
 	})

 	$('#status').select2({
 		placeholder: 'Pilih Status',
 		data: select_arr,
 		width: '100%'
 	})

 	$('#status').on('change', function(){
 		var isi = $(this).val();
 		if(isi == 'rework'){
 			$('#upload_aman_lamp').removeAttr('required');
 			$('.upload_aman').hide();
 			$('.progress_btn').show();
 		}else{
 			$('.upload_aman').show();
 			$('.progress_btn').hide();
 			$('#upload_aman_lamp').attr('required', 'required');
 		}
 	})

 	$('select').select2();

 	$('.tableData').DataTable({
 		autoWidth: true,
 		columnDefs: [
 			{
 				targets: 'no-sort',
 				orderable: false
 			}
 		],
 		lengthMenu: [
 			[18, 27, 64, -1],
 			[18, 27, 64, "All"]
 		]
 	});

 	$('.modal_comp').on('click', function (){
 		$('#status').val('rework').trigger('change')

 		var data_id = $(this).attr('data-id');
 		$.ajax({
 			url:"/get_ajx/get/detail/sp",
 			type:"GET",
 			data: {
 				id : data_id,
 				status: 'REKON'
 			},
 			dataType: 'json',
 			success: (function(data){
 				console.log(data, data_id)
 				var tgl = new Date(data.tgl_sp);
 				tgl.setDate(tgl.getDate() + data.tgl_jatuh_tmp);

 				$('.upload_re').css({
 					'display': 'block'
 				});

 				$('#judul_pekerjaan').html(data.judul);
 				$('.surat_penetapan').html(data.surat_penetapan);
 				$('.tgl_s_pen').html(data.tgl_s_pen);
 				$('.no_sp').html(data.no_sp);
 				$('.tgl_sp').html(data.tgl_sp);
 				$('.jt').html(data.durasi);
 				$('#id_boq_up').val(data.id);
 				$('.durasi').html(data.tgl_jatuh_tmp+ ' Hari');
 				$('.lokasi').html(data.lokasi);
 				$('.pks').html(data.pks);
 				$('.nama_company').html(data.nama_company);
 				$('.total_material').html(data.total_material_sp);
 				$('.total_jasa').html(data.total_jasa_sp);
 				$('.gd').html(data.gd_sp);
 				$('.gd_ppn').html(data.gd_ppn_sp);
 				$('.total').html(data.total_sp);
 				$('.pid').html(data.id_project);
 				$('.jenis_work').html(data.pekerjaan + ' | ' + data.jenis_work + ' | ' + data.jenis_kontrak);
 				$('.rar_down').attr('href', '/download/full_rar/'+data.id);
 				$('.progress_btn').attr('href', '/progress/'+data.id);

 				$(".nama_company, .total_material, .total_jasa, .gd, .gd_ppn, .total").digits();
 			}),
 		})
 	});

 });
</script>
@endsection