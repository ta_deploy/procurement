@extends('layout')
@section('title', 'Pekerjaan Budget Exceeded')
@section('headerS')
<link rel="stylesheet" href="/css/dataTables.bootstrap4.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" rel="stylesheet" />

<style>
	@media (min-width: 768px) {
		.modal-xl {
			width: 100%;
		max-width:1200px;
		}
	}
</style>
@endsection
@section('content')
@if (Session::has('alerts'))
  @foreach(Session::get('alerts') as $alert)
    <div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
  @endforeach
@endif
<div class="container-fluid">
  <div class="row justify-content-center">
    <div class="col-12">
      <h2 class="page-title">List Pekerjaan Budget Exceeded</h2>
      <div class="row">
        <div class="col-md-12">
          <div class="card shadow">
            <div class="card-body table-responsive">
              <table class="table table-striped table-bordered table-hover">
                <thead class="thead-dark">
                  <tr>
                    <th class="hidden-xs">#</th>
                    <th>PID</th>
                    <th>Nama Mitra</th>
                    <th>Budget Diperlukan</th>
                    <th>Jumlah Pekerjaan Exceeded</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody id="data_table1">
                  @php
                  $num = 1;
                  @endphp
                  @foreach($data as $d)
                  <tr>
                    <td class="hidden-xs">{{ $num++ }}</td>
                    <td>{{ $d->pid }}</td>
                    <td>{{preg_replace('/^PT/', 'PT.',$d->nama_company) }}</td>
                    <td>{{ number_format($d->used_budget, 0, '.', '.') }}</td>
                    <td>{{ $d->jml_be }}</td>
                    <td>
                      @if(in_array(session('auth')->proc_level, [4, 7, 99, 44]) )
                        <a class="btn btn-danger btn-sm" href="/Report/pid/be/{{$d->id_pp}}" target="_blank" style="color: #fff">Update Budget</a>
                      @endif
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div> <!-- end section -->
    </div> <!-- .col-12 -->
  </div> <!-- .row -->
</div>
@endsection
@section('footerS')
<script src='/js/jquery.dataTables.min.js'></script>
<script src='/js/dataTables.bootstrap4.min.js'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script type="text/javascript">
	$(function(){
		$.fn.digits = function(){
			return this.each(function(){
					$(this).text( $(this).text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") );
			});
		}

		$('select').select2();

		$('.table').DataTable({
			autoWidth: true,
			columnDefs: [
				{
					targets: 'no-sort',
					orderable: false
				}
			],
			lengthMenu: [
				[16, 32, 64, -1],
				[16, 32, 64, "All"]
			]
		});
	});
</script>
@endsection