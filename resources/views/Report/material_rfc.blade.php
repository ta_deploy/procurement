@extends('layout')
@section('title', 'Rekap Material RFC')
@section('headerS')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link rel="stylesheet" href="/css/dataTables.bootstrap4.css">
<style type="text/css">
	/* Transparent Overlay */
	.loading:before {
		content: '';
		display: block;
		position: fixed;
		top: 0;
		left: 0;
		width: 100%;
		height: 100%;
		background: radial-gradient(rgba(20, 20, 20,.8), rgba(0, 0, 0, .8));
		z-index: 9999!important;
		background: -webkit-radial-gradient(rgba(20, 20, 20,.8), rgba(0, 0, 0,.8));
	}

	/* :not(:required) hides these rules from IE9 and below */
	.loading:not(:required) {
		/* hide "loading..." text */
		font: 0/0 a;
		color: transparent;
		text-shadow: none;
		background-color: transparent;
		border: 0;
	}

	.loading:not(:required):after {
		content: '';
		display: block;
		font-size: 10px;
		z-index: 10000!important;
		width: 1em;
		height: 1em;
		margin-top: -0.5em;
		-webkit-animation: spinner 1500ms infinite linear;
		-moz-animation: spinner 1500ms infinite linear;
		-ms-animation: spinner 1500ms infinite linear;
		-o-animation: spinner 1500ms infinite linear;
		animation: spinner 1500ms infinite linear;
		border-radius: 0.5em;
		top: 50%;
		left: 50%;
		position: absolute;
		-webkit-box-shadow: rgba(255,255,255, 0.75) 1.5em 0 0 0, rgba(255,255,255, 0.75) 1.1em 1.1em 0 0, rgba(255,255,255, 0.75) 0 1.5em 0 0, rgba(255,255,255, 0.75) -1.1em 1.1em 0 0, rgba(255,255,255, 0.75) -1.5em 0 0 0, rgba(255,255,255, 0.75) -1.1em -1.1em 0 0, rgba(255,255,255, 0.75) 0 -1.5em 0 0, rgba(255,255,255, 0.75) 1.1em -1.1em 0 0;
		box-shadow: rgba(255,255,255, 0.75) 1.5em 0 0 0, rgba(255,255,255, 0.75) 1.1em 1.1em 0 0, rgba(255,255,255, 0.75) 0 1.5em 0 0, rgba(255,255,255, 0.75) -1.1em 1.1em 0 0, rgba(255,255,255, 0.75) -1.5em 0 0 0, rgba(255,255,255, 0.75) -1.1em -1.1em 0 0, rgba(255,255,255, 0.75) 0 -1.5em 0 0, rgba(255,255,255, 0.75) 1.1em -1.1em 0 0;
	}

	/* Animation */

	@-webkit-keyframes spinner {
		0% {
			-webkit-transform: rotate(0deg);
			-moz-transform: rotate(0deg);
			-ms-transform: rotate(0deg);
			-o-transform: rotate(0deg);
			transform: rotate(0deg);
		}
		100% {
			-webkit-transform: rotate(360deg);
			-moz-transform: rotate(360deg);
			-ms-transform: rotate(360deg);
			-o-transform: rotate(360deg);
			transform: rotate(360deg);
		}
	}
	@-moz-keyframes spinner {
		0% {
			-webkit-transform: rotate(0deg);
			-moz-transform: rotate(0deg);
			-ms-transform: rotate(0deg);
			-o-transform: rotate(0deg);
			transform: rotate(0deg);
		}
		100% {
			-webkit-transform: rotate(360deg);
			-moz-transform: rotate(360deg);
			-ms-transform: rotate(360deg);
			-o-transform: rotate(360deg);
			transform: rotate(360deg);
		}
	}
	@-o-keyframes spinner {
		0% {
			-webkit-transform: rotate(0deg);
			-moz-transform: rotate(0deg);
			-ms-transform: rotate(0deg);
			-o-transform: rotate(0deg);
			transform: rotate(0deg);
		}
		100% {
			-webkit-transform: rotate(360deg);
			-moz-transform: rotate(360deg);
			-ms-transform: rotate(360deg);
			-o-transform: rotate(360deg);
			transform: rotate(360deg);
		}
	}
	@keyframes spinner {
		0% {
			-webkit-transform: rotate(0deg);
			-moz-transform: rotate(0deg);
			-ms-transform: rotate(0deg);
			-o-transform: rotate(0deg);
			transform: rotate(0deg);
		}
		100% {
			-webkit-transform: rotate(360deg);
			-moz-transform: rotate(360deg);
			-ms-transform: rotate(360deg);
			-o-transform: rotate(360deg);
			transform: rotate(360deg);
		}
	}

	@media (min-width: 768px) {
		.modal-xl {
			width: 100%;
			max-width:1200px;
		}
	}
</style>
@endsection
@section('content')
<div class="loading"></div>
<div class="modal fade modal_ket_material ket_material" id="ket_material">
	<div class="modal-dialog modal-dialog-centered modal-xl" role="document" style="width: 95%; margin: auto;">
		<div class="modal-content">
			<div class="modal-header" >
				<h5 class="modal-title" id="exampleModalLabel" style="width: 400px;"><div id="nama_rfc"></div></h5>
			</div>
			<div class="modal-body table-responsive" style=" overflow-y:auto;padding: 0px 0px 0px 0px;">
				<table class="table table-striped table-bordered toggle-circle">
					<thead>
						<tr>
							<th style="color: black;">RFC</th>
							<th style="color: black;">Pemakai</th>
							<th style="color: black;">ID Item</th>
							<th style="color: black;">Terpakai</th>
							<th style="color: black;">Keluar  Gudang</th>
							<th style="color: black;">Kembali</th>
						</tr>
					</thead>
					<tbody id='testis'>
					</tbody>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-12">
			<div class="row">
				<div class="col-md-12 my-4">
					<div class="card shadow mb-4">
						<div class="card-body table-responsive">
							<h5 class="card-title">Tabel RFC</h5>
							<table id ="tb_rfc" class="table table-striped table-bordered table-hover">
								<thead class="thead-dark">
									<tr>
										<th data-toggle="true">Nomor RFC</th>
										<th>Tim</th>
										<th>Nik 1</th>
										<th>Nik 2</th>
										<th>Terpakai</th>
										<th>Keluar Gudang</th>
										<th>Kembali</th>
									</tr>
								</thead>
								<tbody id="data_table">
									@foreach($data as $no => $order)
										<tr>
											<td><a type="button" class="btn btn-info btn-sm rfc_detail_lapo" href="" style="color:white;" data-rfc= "{{ $order->rfc_n }}"> {{ $order->rfc_n }}</a></td>
											<td>{{ $order->regu_name }}</td>
											<td>{{ $order->nik1 or '-' }}</td>
											<td>{{ $order->nik2 or '-' }}</td>
											<td>{{ $order->terpakai != 0 ? $order->terpakai : '-'  }}</td>
											<td>{{ $order->keluar_gudang != 0 ? $order->keluar_gudang : '-'  }}</td>
											<td>{{ $order->kembali != 0 ? $order->kembali : '-'  }}</td>
										</tr>
										@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('footerS')
<script src='/js/jquery.dataTables.min.js'></script>
<script src='/js/dataTables.bootstrap4.min.js'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/jquery.inputmask.bundle.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<script type="text/javascript">
	$(function(){
		$(".loading").css({
			'display' : 'none'
		});

		$('#tb_rfc').DataTable({
			autoWidth: true,
			lengthMenu: [
				[16, 32, 64, -1],
				[16, 32, 64, "All"]
			]
		});

		$('.rfc_detail_lapo').on('click', function(e){
			e.preventDefault();
			value = $(this).data('rfc');
			m1 = {!! json_encode($m1) !!}
			m2 = {!! json_encode($m2) !!}
			$.ajax({
				type: 'GET',
				data: {
					data : value,
					m1 : m1,
					m2 : m2,
				},
				url : "/get_ajx/rfc/search/please/help",
				beforeSend: function() {
					$(".loading").css({
						'display' : 'block'
					});
				}
			}).done(function(data) {
				console.log(data);
				$(".loading").css({
					'display' : 'none'
				});
				$('#ket_material').modal('show');
				$('#nama_rfc').text('Tabel RFC '+data[0].rfc);
				var value_isi;
				$.each( data, function( key, value ) {
					value_isi += '<tr><td>'+value.rfc+'</td><td>'+value.created_by+'</td><td>'+value.id_item+'</td><td>'+value.terpakai+'</td><td>'+value.keluar_gudang+'</td><td>'+value.kembali+'</td></tr>'
				});
				$('#testis').html(value_isi);
			}).fail(function(data){
				console.log(data);
			});
		});
	});
</script>
@endsection