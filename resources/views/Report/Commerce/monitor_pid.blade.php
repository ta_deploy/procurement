@extends('layout')
@section('title', 'Monitoring PID')
@section('headerS')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="https://cdn.datatables.net/rowgroup/1.1.3/css/rowGroup.bootstrap4.min.css" />
<link rel="stylesheet" href="/css/dataTables.bootstrap4.css">
<style type="text/css">
	.pull-right {
		text-align: right;
	}
</style>
@endsection
@section('content')
@if (Session::has('alerts'))
	@foreach(Session::get('alerts') as $alert)
		<div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
	@endforeach
@endif
<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-md-12">
      <div class="form-group">
        <a class="btn btn-primary" href="/Report/see/pid"><i class="fe fe-eye fe-16"></i>&nbsp;List PID</a>
      </div>
      <div class="card shadow mb-4">
				<div class="card-header">
					<strong class="card-title">History PID</strong>
				</div>
				<div class="card-body table-responsive">
					<ul class="nav nav-tabs mb-3" id="myTab" role="tablist">
						<li class="active"><a href="#list_pid" id="data1" class="nav-link active" data-toggle="tab">List PID&nbsp;{!! count($pid_aman) != 0 ? "<span class='badge badge-info'>".count($pid_aman)."</span>" : '' !!}</a></li>
						<li class=""><a href="#progress" id="data4" class="nav-link" data-toggle="tab" aria-expanded="false">Budget Exceeded&nbsp;{!! count($pid_be) != 0 ? "<span class='badge badge-success'>".count($pid_be)."</span>" : '' !!}</a></li>
					</ul>
					<div id="myTabContentLeft" class="tab-content">
						<div class="tab-pane fade active show" id="list_pid">
							<table class="list_pid table table-striped table-bordered table-hover">
								<thead class="thead-dark">
									<tr>
										<th class="hidden-xs">#</th>
										<th>PID</th>
										<th>Keterangan</th>
										<th>Project</th>
										<th>Total SP</th>
										<th>Total Rekon</th>
										<th>Material : Stock</th>
										<th>Material : Non Stock</th>
										{{-- <th>Budget PID</th>
										<th>Sisa Budget</th> --}}
										{{-- <th>Budget Diperlukan</th> --}}
										<th>Status</th>
									</tr>
								</thead>
								<tbody>
									@php
										$no = 0;
									@endphp
									@foreach ($pid_aman as $key => $val)
										@php
											$first = true;
										@endphp
											@foreach ($val['judul'] as $j)
											<tr>
												@if ($first == true)
													<td rowspan="{{ count($val['judul']) }}">{{ ++$no }}</td>
													<td rowspan="{{ count($val['judul']) }}">{{ $val['pid'] }}</td>
													<td rowspan="{{ count($val['judul']) }}">{{ $val['keterangan'] }}</td>
													{{-- <td rowspan="{{ count($val['judul']) }}">{{ number_format($val['budget'], 0, '.', '.') }}</td>
													<td rowspan="{{ count($val['judul']) }}">{{ number_format($val['budget'] - $val['budget_all'], 0, '.', '.') }}</td> --}}
												@endif
												<td>{{ $j }}</td>
												<td>Rp. {{ number_format($val['total_sp'], 0, '.', '.') }}</td>
												<td>Rp. {{ number_format($val['total_rekon'], 0, '.', '.') }}</td>
												<td>Rp. {{ @$val['keperluan']['Material : Stock'] ?? 0 }}</td>
												<td>Rp. {{ @$val['keperluan']['Material : Non Stock'] ?? 0 }}</td>
												@if ($first == true)
													{{-- <td rowspan="{{ count($val['judul']) }}">{{ number_format($val['budget_all'], 0, '.', '.') }}</td> --}}
												@endif
												@if ($first == true)
													<td rowspan="{{ count($val['judul']) }}"><span class="badge badge-pill badge-success">Aman</span></td>
													@php
														$first = false;
													@endphp
												@endif
											</tr>
											@endforeach
									@endforeach
								</tbody>
							</table>
						</div>
						<div class="tab-pane fade " id="progress">
							<table class="list_pid table table-striped table-bordered table-hover">
								<thead class="thead-dark">
									<tr>
										<th class="hidden-xs">#</th>
										<th>PID</th>
										<th>Keterangan</th>
										{{-- <th>Budget PID</th>
										<th>Sisa Budget</th> --}}
										<th>Project</th>
										<th>Budget Diperlukan</th>
										<th>Material : Stock</th>
										<th>Material : Non Stock</th>
										<th>Status</th>
									</tr>
								</thead>
								<tbody>
									@php
										$no = 0;
									@endphp
									@foreach ($pid_be as $key => $val)
										@php
											$first = true;
										@endphp
											@foreach ($val['judul'] as $j)
											<tr>
												@if ($first == true)
													<td rowspan="{{ count($val['judul']) }}">{{ ++$no }}</td>
													<td rowspan="{{ count($val['judul']) }}">{{ $val['pid'] }}</td>
													<td rowspan="{{ count($val['judul']) }}">{{ $val['keterangan'] }}</td>
													{{-- <td rowspan="{{ count($val['judul']) }}">{{ number_format($val['budget'], 0, '.', '.') }}</td>
													<td rowspan="{{ count($val['judul']) }}">{{ number_format($val['budget'] - $val['budget_all'], 0, '.', '.') }}</td> --}}
												@endif
												<td>{{ $j }}</td>
												@if ($first == true)
													<td rowspan="{{ count($val['judul']) }}">{{ number_format($val['budget_all'], 0, '.', '.') }}</td>
													<td rowspan="{{ count($val['judul']) }}">Rp. {{ @$val['keperluan']['Material : Stock'] ?? 0 }}</td>
													<td rowspan="{{ count($val['judul']) }}">Rp. {{ @$val['keperluan']['Material : Non Stock'] ?? 0 }}</td>
												@endif
												@if ($first == true)
													<td rowspan="{{ count($val['judul']) }}">
														<a type="button" class='btn btn-danger btn-sm' href="/Report/pid/be/{{ $val['id'] }}"><i class="fe fe-tool"></i>&nbsp;Update Budget</a>
													</td>
													@php
														$first = false;
													@endphp
											@endif
											</tr>
											@endforeach
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('footerS')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src='/js/jquery.dataTables.min.js'></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/rowgroup/1.1.3/js/dataTables.rowGroup.js"></script>
<script src='/js/dataTables.bootstrap4.min.js'></script>
<script type="text/javascript">
	$(function(){
		$('.table').DataTable({
			autoWidth: true,
			columnDefs: [
				{
					targets: 'no-sort',
					orderable: false
				}
			],
			lengthMenu: [
				[16, 32, 64, -1],
				[16, 32, 64, "All"]
			],
			rowGroup: {
				dataSrc: [ 3 ],
				startRender: function ( rows, group ) {
					var value = (rows.count())
					return group +' ('+value+')';
				}
			}
		});
	});
</script>
@endsection