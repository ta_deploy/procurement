@extends('layout')
@section('title', 'Monitoring PID')
@section('headerS')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="/css/dataTables.bootstrap4.css">
<style type="text/css">
	.pull-right {
		text-align: right;
	}
</style>
@endsection
@section('content')
@if (Session::has('alerts'))
	@foreach(Session::get('alerts') as $alert)
		<div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
	@endforeach
@endif
<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-md-12">
      <div class="card shadow mb-4">
				<div class="card-header">
					<strong class="card-title">History PID</strong>
				</div>
				<div class="card-body table-responsive">
					<ul class="nav nav-tabs mb-3" id="myTab" role="tablist">
						<li class="active"><a href="#unused_budget_apm" id="data1" class="nav-link active" data-toggle="tab">Unused Budget APM&nbsp;{!! count($data['unused_budget_apm']) != 0 ? "<span class='badge badge-info'>".count($data['unused_budget_apm'])."</span>" : '' !!}</a></li>
						<li class=""><a href="#used_budget_apm" id="data2" class="nav-link" data-toggle="tab" aria-expanded="false">Budget APM Terpakai&nbsp;{!! count($data['used_budget_apm']) != 0 ? "<span class='badge badge-success'>".count($data['used_budget_apm'])."</span>" : '' !!}</a></li>
						<li class=""><a href="#unknown_pid" id="data4" class="nav-link" data-toggle="tab" aria-expanded="false">Unknown&nbsp;{!! count($data['unknown_pid']) != 0 ? "<span class='badge badge-warning'>".count($data['unknown_pid'])."</span>" : '' !!}</a></li>
					</ul>
					<div id="myTabContentLeft" class="tab-content">
						<div class="tab-pane fade active show" id="unused_budget_apm">
							<table class="unused_budget_apm table table-striped table-bordered table-hover">
								<thead class="thead-dark">
									<tr>
										<th class="hidden-xs">#</th>
										<th>PID</th>
										<th>WBS</th>
										<th>Material : Stock</th>
										<th>Material : Non Stock</th>
									</tr>
								</thead>
								<tbody>
									@foreach ($data['unused_budget_apm'] as $k => $v)
										<tr>
											<td>{{ ++$k }}</td>
											<td>{{ $v['pid'] }}</td>
											<td>{{ $v['wbs'] }}</td>
											<td>Rp. {{ $v['keperluan']['Material : Stock'] }}</td>
											<td>Rp. {{ $v['keperluan']['Material : Non Stock'] }}</td>
										</tr>
									@endforeach
								</tbody>
							</table>
						</div>
						<div class="tab-pane fade" id="used_budget_apm">
							<table class="used_budget_apm table table-striped table-bordered table-hover">
								<thead class="thead-dark">
									<tr>
										<th class="hidden-xs">#</th>
										<th>PID</th>
										<th>WBS</th>
										<th>Judul</th>
										<th>Total SP</th>
										<th>Total Rekon</th>
										<th>Material : Stock</th>
										<th>Material : Non Stock</th>
									</tr>
								</thead>
								<tbody>
									@foreach ($data['used_budget_apm'] as $k => $v)
										@php ($first = true) @endphp
										@forelse($v['pekerjaan'] as $vv)
											<tr>
												<td>{{ ++$k }}</td>
												<td>{{ $v['pid'] }}</td>
												<td>{{ $v['wbs'] }}</td>
												@if($first == true)
													<td rowspan="{{ count($v['pekerjaan']) }}">{{ $vv['judul_pbu'] }}</td>
													<td rowspan="{{ count($v['pekerjaan']) }}">Rp. {{ number_format($vv['total_sp']) ?? 0 }}</td>
													<td rowspan="{{ count($v['pekerjaan']) }}">Rp. {{ number_format($vv['total_rekon']) }}</td>
													@php ($first = false) @endphp
												@endif
												<td>Rp. {{ $v['keperluan']['Material : Stock'] }}</td>
												<td>Rp. {{ $v['keperluan']['Material : Non Stock'] }}</td>
											</tr>
										@empty
										@endforelse
									@endforeach
								</tbody>
							</table>
						</div>
						<div class="tab-pane fade" id="unknown_pid">
							<table class="unknown_pid table table-striped table-bordered table-hover">
								<thead class="thead-dark">
									<tr>
										<th class="hidden-xs">#</th>
										<th>WBS</th>
										<th>Judul</th>
										<th>Total SP</th>
										<th>Total Rekon</th>
									</tr>
								</thead>
								<tbody>
									@foreach ($data['unknown_pid'] as $k => $v)
										@php ($first = true) @endphp
										@forelse($v['data'] as $vv)
											<tr>
												@if($first == true)
													<td rowspan="{{ count($v['data']) }}">{{ ++$k }}</td>
													<td rowspan="{{ count($v['data']) }}">{{ $v['pid'] }}</td>
													@php ($first = false) @endphp
												@endif
												<td>{{ $vv->judul }}</td>
												<td>Rp. {{ number_format($vv->total_sp ?? 0) }}</td>
												<td>Rp. {{ number_format( ($vv->total_material_rekon + $vv->total_jasa_rekon) ?? 0) }}</td>
											</tr>
										@empty
										@endforelse
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('footerS')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src='/js/jquery.dataTables.min.js'></script>
<script src='/js/dataTables.bootstrap4.min.js'></script>
<script type="text/javascript">
	$(function(){
		$('table').DataTable({
			autoWidth: true,
			columnDefs: [
				{
					targets: 'no-sort',
					orderable: false
				}
			],
			lengthMenu: [
				[16, 32, 64, -1],
				[16, 32, 64, "All"]
			]
		});
	});
</script>
@endsection