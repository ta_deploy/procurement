@extends('layout')
@section('title', 'Request Budgeting')
@section('headerS')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="/css/dataTables.bootstrap4.css">
<link rel="stylesheet" href="/css/daterangepicker.css">
<style type="text/css">
	.pull-right {
		text-align: right;
	}
</style>
@endsection
@section('content')
@if (Session::has('alerts'))
  @foreach(Session::get('alerts') as $alert)
    <div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
  @endforeach
@endif
<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-md-12">
      <div class="card shadow mb-4">
				<div class="card-header">
					<strong class="card-title">Requst Budgeting</strong>
				</div>
				<div class="card-body table-responsive">
					<ul class="nav nav-tabs mb-3" id="myTab" role="tablist">
						<li class="active"><a href="#avail_data" id="data1" class="nav-link active" data-toggle="tab">List Pekerjaan&nbsp;{!! count($data_ada) != 0 ? "<span class='badge badge-info'>".count($data_ada)."</span>" : '' !!}</a></li>
						<li class=""><a href="#be" id="data4" class="nav-link" data-toggle="tab" aria-expanded="false">Budget Exceeded&nbsp;{!! count($data_ex) != 0 ? "<span class='badge badge-warning'>".count($data_ex)."</span>" : '' !!}</a></li>
					</ul>
					<div id="myTabContentLeft" class="tab-content">
						<div class="tab-pane fade active show" id="avail_data">
							<table  class="table table-striped table-bordered table-hover">
								<thead class="thead-dark">
									<tr>
										<th class="hidden-xs">#</th>
										<th>Jenis Pekerjaan</th>
										<th>Pekerjaan</th>
										<th>Wbs</th>
										<th>PID</th>
										<th>Material : Stock</th>
										<th>Material : Non Stock</th>
										<th>Total Material : Stock</th>
										<th>Total Material : Non Stock</th>
										<th>Tanggal Request Created</th>
										<th>Umur (Hari)</th>
										<th>User Request</th>
										<th>Keterangan Terakhir</th>
										<th>Update Terakhir</th>
										<th class="no-sort">Action</th>
									</tr>
								</thead>
								<tbody>
									@php
										$num = 1;
									@endphp
									@forelse($data_ada as $k => $d)
										@php ($first = true) @endphp
										@forelse($d['apm'] as $vxv)
											<tr>
												@if($first == true)
													<td class="hidden-xs" rowspan="{{ count($d['apm']) }}">{{ $num++ }}</td>
													<td rowspan="{{ count($d['apm']) }}">{{ $d['jenis_work'] }}</td>
													<td rowspan="{{ count($d['apm']) }}">{{ $d['judul'] }}</td>
													<td rowspan="{{ count($d['apm']) }}">{{ $d['id_project'] }}</td>
												@endif
												<td>{{ $vxv['wbs'] }}</td>
												<td>Rp. {{ $vxv['keperluan']['Material : Stock'] ?? 0 }}</td>
												<td>Rp. {{ $vxv['keperluan']['Material : Non Stock'] ?? 0 }}</td>
												@if($first == true)
													<td rowspan="{{ count($d['apm']) }}">Rp. {{ number_format(array_sum($d['total_stock']) ) }}</td>
													<td rowspan="{{ count($d['apm']) }}">Rp. {{ number_format(array_sum($d['total_non_stock']) ) }}</td>
													<td rowspan="{{ count($d['apm']) }}">{{ $d['tgl_last_update'] }}</td>
													<td rowspan="{{ count($d['apm']) }}">{{ $d['jml_hri'] }}</td>
													<td rowspan="{{ count($d['apm']) }}">{{ $d['created_by'] }}</td>
													<td rowspan="{{ count($d['apm']) }}">{{ $d['detail'] }}</td>
													<td rowspan="{{ count($d['apm']) }}">{{ $d['modified_by'] }}</td>
													<td rowspan="{{ count($d['apm']) }}">
														<a type="button" href="/progress/{{ $k }}" class="btn btn-sm btn-primary" style="color: #fff; margin-bottom: 5px;"><i class="fe fe-tool fe-16"></i>&nbsp;Approve Budget</a>
														<a type="button" class="btn btn-sm btn-modal_me btn-warning" href="/rollback/{{ $k }}" style="margin-bottom: 5px;"><i class="fe fe-corner-up-left fe-16"></i>&nbsp;Rollback </a>
													</td>
													@php ($first = false) @endphp
												@endif
											</tr>
										@empty
										@endforelse
									@empty
									@endforelse
								</tbody>
							</table>
						</div>
						<div class="tab-pane fade" id="be">
							<table class="table table-striped table-bordered table-hover">
								<thead class="thead-dark">
									<tr>
										<th class="hidden-xs">#</th>
										<th>Jenis Pekerjaan</th>
										<th>Pekerjaan</th>
										<th>PID Exceeded</th>
										<th>PID</th>
										<th>Material : Stock</th>
										<th>Material : Non Stock</th>
										<th>Tanggal Request Created</th>
										<th>Umur (Hari)</th>
										<th>Update Terakhir</th>
										<th>Detail</th>
										<th class="no-sort">Action</th>
										<th class="no-sort">Update Budget</th>
									</tr>
								</thead>
								<tbody>
									@php
										$num = 1;
									@endphp
									@forelse($data_ex as $kd => $d)
										@php ($first = true) @endphp
										@forelse($d['apm'] as $vxv)
											<tr>
												@if($first == true)
													<td class="hidden-xs" rowspan="{{ count($d['apm']) }}">{{ $num++ }}</td>
													<td rowspan="{{ count($d['apm']) }}">{{ $d['jenis_work'] }}</td>
													<td rowspan="{{ count($d['apm']) }}">{{ $d['judul'] }}</td>
													@php
														$ex = [];
														foreach($d['exceeded'] as $v)
														{
															foreach ($v as $vv) {
																$ex[] = $vv;
															}
														}
													@endphp
													<td rowspan="{{ count($d['apm']) }}">{{ implode(', ', $ex) }}</td>
												@endif
												<td>{{ $vxv['wbs'] }}</td>
												<td>Rp. {{ $vxv['keperluan']['Material : Stock'] ?? 0}}</td>
												<td>Rp. {{ $vxv['keperluan']['Material : Non Stock'] ?? 0}}</td>
												@if($first == true)
													<td rowspan="{{ count($d['apm']) }}">{{ $d['created_at'] }}</td>
													<td rowspan="{{ count($d['apm']) }}">{{ $d['jml_hri'] }}</td>
													<td rowspan="{{ count($d['apm']) }}">{{ $d['modified_by'] }}</td>
													<td rowspan="{{ count($d['apm']) }}">{{ $d['detail'] }}</td>
													<td rowspan="{{ count($d['apm']) }}"><a class="btn btn-sm btn-warning" href="/rollback/{{ $kd }}"><i class="fe fe-corner-up-left fe-16"></i>&nbsp;Rollback </a>
													</td>
													<td rowspan="{{ count($d['apm']) }}">
														@foreach($d['exceeded'] as $k => $v)
															<a type="button" href="/Report/pid/be/{{ $k }}" class="btn btn-sm btn-info" style="color: black; margin-bottom: 10px;"><i class="fe fe-tool"></i>&nbsp;Update {{ implode(', ', $v) }}</a><br/>
														@endforeach
													</td>
													@php ($first = false) @endphp
												@endif
											</tr>
										@empty
										@endforelse
									@empty
									@endforelse
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('footerS')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src='/js/jquery.dataTables.min.js'></script>
<script src='/js/dataTables.bootstrap4.min.js'></script>
<script src="/js/moment.min.js"></script>
<script src='/js/jquery.timepicker.js'></script>
<script src='/js/daterangepicker.js'></script>
<script type="text/javascript">
	$(function(){
		$('.date-picker').daterangepicker(
      {
        singleDatePicker: true,
        timePicker: false,
        showDropdowns: true,
        locale:
        {
					format: 'YYYY-MM-DD',
        }
      }
		);

		$('.btn-modal_me').on('click', function () {
			$('.donwload_me').attr('href', '/download/full_rar/'+$(this).attr('data-id_up') );
			$('#id_up').val($(this).attr('data-id_up'));
		});

		$('#budget').keyup(function(event) {
		// skip for arrow keys
			if(event.which >= 37 && event.which <= 40) return;
		// format number
			$(this).val(function(index, value) {
				return value
				.replace(/\D/g, "")
				.replace(/\B(?=(\d{3})+(?!\d))/g, ".");
			});
		});

		// $('.table').DataTable({
		// 	autoWidth: true,
		// 	columnDefs: [
		// 		{
		// 			targets: 'no-sort',
		// 			orderable: false
		// 		}
		// 	],
		// 	lengthMenu: [
		// 		[16, 32, 64, -1],
		// 		[16, 32, 64, "All"]
		// 	]
		// });
	});
</script>
@endsection