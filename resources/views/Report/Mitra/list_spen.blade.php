@extends('layout')
@section('title', 'List Surat Penetapan')
@section('headerS')
<link rel="stylesheet" href="/css/dataTables.bootstrap4.css">
<style>
	@media (min-width: 768px) {
		.modal-xl {
			width: 100%;
		max-width:1200px;
		}
	}
</style>
@endsection
@section('content')
@if (Session::has('alerts'))
  @foreach(Session::get('alerts') as $alert)
    <div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
  @endforeach
@endif
<div class="modal fade" id="modal_comp" tabindex="-1" role="dialog" aria-labelledby="modal_compTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body data_mdl" style="overflow: hidden;">
				<div class="row">
					<div class="col-md-7">
						<div class="form-group row">
							<label class="col-form-label col-md-3" for="sto">Judul:</label>
							<div class="col-md-9">
								<textarea disabled style="resize:none; font-weight: bold" cols='50' rows="2" class="form-control input-transparent judul"></textarea>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-md-3" for="sto">Project ID:</label>
							<div class="col-md-9">
								<p class="form-control input-transparent" readonly><b class="pid"></b></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-md-3" for="sto">Jenis Pekerjaan:</label>
							<div class="col-md-9">
								<p class="form-control input-transparent" readonly><b class="jenis_work"></b></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-md-3" for="sto">Surat Penetapan:</label>
							<div class="col-md-9">
								<textarea disabled style="resize:none; font-weight: bold" cols='50' rows="2" class="form-control input-transparent surat_penetapan"></textarea>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-md-3" for="sto">Tanggal Surat Penetapan:</label>
							<div class="col-md-9">
								<p class="form-control input-transparent" readonly><b class="tgl_s_pen"></b></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-md-3" for="sto">Surat Pesanan:</label>
							<div class="col-md-9">
								<textarea disabled style="resize:none; font-weight: bold" cols='50' rows="2" class="form-control input-transparent no_sp"></textarea>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-md-3" for="sto">Tanggal Surat pesanan:</label>
							<div class="col-md-9">
								<p class="form-control input-transparent" readonly><b class="tgl_sp"></b></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-md-3" for="sto">Tanggal Jatuh Tempo:</label>
							<div class="col-md-9">
								<p class="form-control input-transparent" readonly><b class="jt"></b></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-md-3" for="sto">Durasi:</label>
							<div class="col-md-9">
								<p class="form-control input-transparent" readonly><b class="durasi"></b></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-md-3" for="sto">Lokasi:</label>
							<div class="col-md-9">
								<p class="form-control input-transparent" readonly><b class="lokasi"></b></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-md-3" for="sto">PKS:</label>
							<div class="col-md-9">
								<p class="form-control input-transparent" readonly><b class="pks"></b></p>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group row">
							<label class="col-form-label col-md-3" for="sto">Download Data Upload Dalam RAR:</label>
							<div class="col-md-9">
								<a class="btn btn-primary rar_down" href="">Download</a>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-md-3" for="sto">Mitra:</label>
							<div class="col-md-9">
								<p class="form-control input-transparent" readonly><b class="nama_company"></b></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-md-3" for="sto">Total Materail:</label>
							<div class="col-md-9">
								<p class="form-control input-transparent" readonly><b class="total_material"></b></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-md-3" for="sto">Total Jasa:</label>
							<div class="col-md-9">
								<p class="form-control input-transparent" readonly><b class="total_jasa"></b></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-md-3" for="sto">Grand Total:</label>
							<div class="col-md-9">
								<p class="form-control input-transparent" readonly><b class="gd"></b></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-md-3" for="sto">Grand Total PPN:</label>
							<div class="col-md-9">
								<p class="form-control input-transparent" readonly><b class="gd_ppn"></b></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-md-3" for="sto">Grand Total + PPN:</label>
							<div class="col-md-9">
								<p class="form-control input-transparent" readonly><b class="total"></b></p>
							</div>
						</div>
					</div>
				</div>
			</div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row justify-content-center">
    <div class="col-12">
      <h2 class="page-title">List Surat Kesanggupan</h2>
      <p class="lead text-muted">Mengawal dari Surat Penetapan</p>
      <div class="row">
        <div class="col-md-12">
          <div class="card shadow">
            <div class="card-body table-responsive">
              <ul class="nav nav-tabs mb-3" id="myTab" role="tablist">
                <li class="active"><a href="#avail" id="data1" class="nav-link active" data-toggle="tab">Surat Penetapan Tersedia&nbsp;{!! count($avail) != 0 ? "<span class='badge badge-primary'>".count($avail)."</span>" : '' !!}</a></li>
								<li class=""><a href="#save" id="data4" class="nav-link" data-toggle="tab" aria-expanded="false">Save Penetapan&nbsp;{!! count($save) != 0 ? "<span class='badge badge-warning'>".count($save)."</span>" : '' !!}</a></li>
								<li class=""><a href="#reject" id="data5" class="nav-link" data-toggle="tab" aria-expanded="false">Ditolak&nbsp;{!! count($reject) != 0 ? "<span class='badge badge-danger'>".count($reject)."</span>" : '' !!}</a></li>
								<li class=""><a href="#sanggup" id="data3" class="nav-link" data-toggle="tab" aria-expanded="false">Sedang Progress&nbsp;{!! count($sanggup) != 0 ? "<span class='badge badge-info'>".count($sanggup)."</span>" : '' !!}</a></li>
              </ul>
							<div id="myTabContentLeft" class="tab-content">
								<div class="tab-pane fade active show" id="avail">
									<table class="table table-striped table-bordered table-hover">
										<thead class="thead-dark">
											<tr>
												<th class="hidden-xs">#</th>
												<th>Pekerjaan</th>
												<th>Mitra</th>
												<th>Nomor Penetapan</th>
												<th>Nomor PKS</th>
												<th>TOC</th>
												<th>Umur (Hari)</th>
												<th>Keterangan Terakhir</th>
												<th>Update Terakhir</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody id="data_table1">
											@php
											$num = 1;
											@endphp
											@foreach($avail as $d)
											<tr>
												<td class="hidden-xs">{{ $num++ }}</td>
												<td>{{ $d->judul }}</td>
												<td>{{ $d->mitra_nm }}</td>
												<td>{{ $d->surat_penetapan }}</td>
												<td>{{ $d->pks }}</td>
												<td>{{ $d->tgl_jatuh_tmp }} Hari <b>({{ date('Y-m-d', strtotime('+'. $d->tgl_jatuh_tmp. ' day', strtotime($d->tgl_s_pen))) }})</b></td>
												<td>{{ $d->jml_hri }}</td>
												<td>{{ $d->detail }}</td>
												<td>{{ $d->nama_modif ? $d->nama_modif .' ('. $d->modified_by .')' : '' }}</td>
												<td>
													<a class="btn btn-sm btn-primary" href="/progress/{{ $d->id }}" style="margin-bottom: 5px;"><i class="fe fe-file-plus fe-16"></i>&nbsp;Dokumen</a>
													<a type="button" class="btn btn-sm btn-modal_me btn-warning" href="/rollback/{{ $d->id }}" style="margin-bottom: 5px;"><i class="fe fe-corner-up-left fe-16"></i>&nbsp;Rollback </a>
												</td>
											</tr>
											@endforeach
										</tbody>
									</table>
								</div>
								<div class="tab-pane fade " id="save">
									<table class="table table-striped table-bordered table-hover">
										<thead class="thead-dark">
											<tr>
												<th class="hidden-xs">#</th>
												<th>Pekerjaan</th>
												<th>Mitra</th>
												<th>Nomor Penetapan</th>
												<th>Nomor PKS</th>
												<th>TOC</th>
												<th>Umur (Hari)</th>
												<th>Tanggal Save</th>
												<th>Update Terakhir</th>
												<th>Detail</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody id="data_table4">
											@php
											$num = 1;
											@endphp
											@foreach($save as $d)
											<tr>
												<td class="hidden-xs">{{ $num++ }}</td>
												<td>{{ $d->judul }}</td>
												<td>{{ $d->mitra_nm }}</td>
												<td>{{ $d->surat_penetapan }}</td>
												<td>{{ $d->pks }}</td>
												<td>{{ $d->tgl_jatuh_tmp }} Hari <b>({{ date('Y-m-d', strtotime('+'. $d->tgl_jatuh_tmp. ' day', strtotime($d->tgl_s_pen))) }})</b></td>
												<td>{{ $d->jml_hri }}</td>
												<td>{{ $d->tgl_last_update }}</td>
												<td>{{ $d->nama_modif ? $d->nama_modif .' ('. $d->modified_by .')' : '' }}</td>
												<td>{{ $d->detail }}</td>
												<td>
													<a class="btn btn-sm btn-primary" href="/progress/{{ $d->id }}" style="margin-bottom: 5px;"><i class="fe fe-edit-2 fe-16"></i>&nbsp;Dokumen</a>
													<a type="button" class="btn btn-sm btn-modal_me btn-warning" href="/rollback/{{ $d->id }}" style="margin-bottom: 5px;"><i class="fe fe-corner-up-left fe-16"></i>&nbsp;Rollback </a>
												</td>
											</tr>
											@endforeach
										</tbody>
									</table>
								</div>
								<div class="tab-pane fade " id="reject">
									<table class="table table-striped table-bordered table-hover">
										<thead class="thead-dark">
											<tr>
												<th class="hidden-xs">#</th>
												<th>Pekerjaan</th>
												<th>Mitra</th>
												<th>Nomor Penetapan</th>
												<th>Nomor PKS</th>
												<th>TOC</th>
												<th>Umur (Hari)</th>
												<th>Update Terakhir</th>
												<th>Action</th>
												<th>Detail</th>
											</tr>
										</thead>
										<tbody id="data_table5">
											@php
											$num = 1;
											@endphp
											@foreach($reject as $d)
											<tr>
												<td class="hidden-xs">{{ $num++ }}</td>
												<td>{{ $d->judul }}</td>
												<td>{{ $d->mitra_nm }}</td>
												<td>{{ $d->surat_penetapan }}</td>
												<td>{{ $d->pks }}</td>
												<td>{{ $d->tgl_jatuh_tmp }} Hari <b>({{ date('Y-m-d', strtotime('+'. $d->tgl_jatuh_tmp. ' day', strtotime($d->tgl_s_pen))) }})</b></td>
												<td>{{ $d->jml_hri }}</td>
												<td>{{ $d->nama_modif ? $d->nama_modif .' ('. $d->modified_by .')' : '' }}</td>
												<td>{{ $d->detail }}</td>
												<td>
													<a class="btn btn-sm btn-primary" href="/progress/{{ $d->id }}" style="margin-bottom: 5px;"><i class="fe fe-tool fe-16"></i>&nbsp;Dokumen</a>
													<a type="button" class="btn btn-sm btn-modal_me btn-warning" href="/rollback/{{ $d->id }}" style="margin-bottom: 5px;"><i class="fe fe-corner-up-left fe-16"></i>&nbsp;Rollback </a>
												</td>
											</tr>
											@endforeach
										</tbody>
									</table>
								</div>
								<div class="tab-pane fade" id="sanggup">
									<table class="table table-striped table-bordered table-hover">
										<thead class="thead-dark">
											<tr>
												<th class="hidden-xs">#</th>
												<th>Pekerjaan</th>
												<th>Nomor Penetapan</th>
												<th>Nomor Kesanggupan</th>
												<th>Nomor PKS</th>
												<th>Tanggal Dibuat</th>
												<th>Update Terakhir</th>
												<th>Detail</th>
												<th>Umur (Hari)</th>
											</tr>
										</thead>
										<tbody id="data_table3">
											@php
											$num = 1;
											@endphp
											@foreach($sanggup as $d)
											<tr>
												<td class="hidden-xs">{{ $num++ }}</td>
												<td>{{ $d->judul }}</td>
												<td>{{ $d->surat_penetapan }}</td>
												<td>{{ $d->surat_kesanggupan }}</td>
												<td>{{ $d->pks }}</td>
												<td>{{ $d->created_at }}</td>
												<td>{{ $d->nama_modif ? $d->nama_modif .' ('. $d->modified_by .')' : '' }}</td>
												<td>{{ $d->detail }}</td>
												<td>{{ $d->jml_hri }}</td>
											</tr>
											@endforeach
										</tbody>
									</table>
								</div>
							</div>
            </div>
          </div>
        </div>
      </div> <!-- end section -->
    </div> <!-- .col-12 -->
  </div> <!-- .row -->
</div>
@endsection
@section('footerS')
<script src='/js/jquery.dataTables.min.js'></script>
<script src='/js/dataTables.bootstrap4.min.js'></script>
<script type="text/javascript">
	$(function(){
		$.fn.digits = function(){
			return this.each(function(){
					$(this).text( $(this).text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") );
			});
		}

		$('.table').DataTable({
			autoWidth: true,
			columnDefs: [
				{
					targets: 'no-sort',
					orderable: false
				}
			],
			lengthMenu: [
				[16, 32, 64, -1],
				[16, 32, 64, "All"]
			]
		});

		$('.modal_comp').on('click', function (){
			var data_id = $(this).attr('data-id');
			$.ajax({
					url:"/get_ajx/get/detail/sp",
					type:"GET",
					data: {
						id : data_id,
						status: 'SP'
					},
					dataType: 'json',
					success: (function(data){
						console.log(data)
						var tgl = new Date(data.tgl_sp);
						tgl.setDate(tgl.getDate() + data.tgl_jatuh_tmp);

						$('.judul').html(data.judul);
						$('.surat_penetapan').html(data.surat_penetapan);
						$('.tgl_s_pen').html(data.tgl_s_pen);
						$('.no_sp').html(data.no_sp);
						$('.tgl_sp').html(data.tgl_sp);
						$('.jt').html(tgl.toISOString().slice(0, 10));
						$('.durasi').html(data.tgl_jatuh_tmp);
						$('.lokasi').html(data.lokasi);
						$('.sto').html(data.sto);
						$('.pks').html(data.pks);
						$('.nama_company').html(data.mitra_nm);
						$('.total_material').html(data.total_material_sp);
						$('.total_jasa').html(data.total_jasa_sp);
						$('.gd').html(data.gd_sp);
						$('.pid').html(data.id_project);
						$('.jenis_work').html(data.jenis_work);
						$('.gd_ppn').html(data.gd_ppn_sp);
						$('.total').html(data.total_sp);
						$('.rar_down').attr('href', '/download/full_rar/'+data.id);
						$(".nama_company, .total_material, .total_jasa, .gd, .gd_ppn, .total").digits();
					}),
				})
		});

	});
</script>
@endsection