@extends('layout')
@section('title', 'List Upload Tanda Tangan')
@section('headerS')
<link rel="stylesheet" href="/css/dataTables.bootstrap4.css">
<style>
	@media (min-width: 768px) {
		.modal-xl {
			width: 100%;
		max-width:1200px;
		}
	}
</style>
@endsection
@section('content')
@if (Session::has('alerts'))
  @foreach(Session::get('alerts') as $alert)
    <div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
  @endforeach
@endif
<div class="container-fluid">
  <div class="row justify-content-center">
    <div class="col-12">
      <h2 class="page-title">List Kelengkapan Dokumen Rekon</h2>
      <div class="row">
        <div class="col-md-12">
          <div class="card shadow">
            <div class="card-body table-responsive">
              <ul class="nav nav-tabs mb-3" id="myTab" role="tablist">
                <li class="active"><a href="#ready_rekon" id="data1" class="nav-link active" data-toggle="tab">BOQ Siap Upload TTD &nbsp;{!! count($ready_rekon) != 0 ? "<span class='badge badge-primary'>".count($ready_rekon)."</span>" : '' !!}</a></li>
								<li class=""><a href="#reject_regional" id="data4" class="nav-link" data-toggle="tab" aria-expanded="false">Reject Dari Regional{!! count($reject_rekon) != 0 ? "<span class='badge badge-warning'>".count($reject_rekon)."</span>" : '' !!}</a></li>

              </ul>
							<div id="myTabContentLeft" class="tab-content">
								<div class="tab-pane fade active show" id="ready_rekon">
									<table class="table table-striped table-bordered table-hover">
										<thead class="thead-dark">
											<tr>
												<th class="hidden-xs">#</th>
												<th>Pekerjaan</th>
												<th>Mitra</th>
												<th>Nomor SP</th>
												<th>Nomor PKS</th>
												<th>Status</th>
												<th>TOC</th>
												<th>Umur (Hari)</th>
												<th>Keterangan Terakhir</th>
												<th>Update Terakhir</th>
												<th class="no-sort">Action</th>
											</tr>
										</thead>
										<tbody id="data_table1">
											@php
												$num = 1;
											@endphp
											@foreach($ready_rekon as $d)
											<tr>
												<td class="hidden-xs">{{ $num++ }}</td>
												<td>{{ $d->judul }}</td>
												<td>{{ $d->mitra_nm }}</td>
												<td>{{ $d->no_sp }}</td>
												<td>{{ $d->pks }}</td>
												<td>{{ $d->nama_step }}</td>
												<td>{{ $d->tgl_jatuh_tmp }} Hari <b>({{ date('Y-m-d', strtotime('+'. $d->tgl_jatuh_tmp. ' day', strtotime($d->tgl_s_pen))) }})</b></td>
												<td>{{ $d->jml_hri }}</td>
												<td>{{ $d->detail }}</td>
												<td>{{ $d->nama_modif ? $d->nama_modif .' ('. $d->modified_by .')' : '' }}</td>
												<td>
													<a class="btn btn-sm btn-primary" style="color: #fff" href="/progress/{{ $d->id }}"><i class="fe fe-upload fe-16"></i>&nbsp;TTD</a>
													@if($d->pekerjaan != "PSB")
														<a class="btn btn-sm btn-warning" style="margin-top: 5px;" href="/rollback/{{ $d->id }}"><i class="fe fe-corner-up-left fe-16"></i>&nbsp;Rollback </a>
													@endif
												</td>
											</tr>
											@endforeach
										</tbody>
									</table>
								</div>
								<div class="tab-pane fade " id="reject_regional">
									<table class="table table-striped table-bordered table-hover">
										<thead class="thead-dark">
											<tr>
												<th class="hidden-xs">#</th>
												<th>Pekerjaan</th>
												<th>Mitra</th>
												<th>Nomor SP</th>
												<th>Nomor PKS</th>
												<th>TOC</th>
												<th>Umur (Hari)</th>
												<th>Tanggal Save</th>
												<th>Detail</th>
												<th>Update Terakhir</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody id="data_table4">
											@php
											$num = 1;
											@endphp
											@foreach($reject_rekon as $d)
											<tr>
												<td class="hidden-xs">{{ $num++ }}</td>
												<td>{{ $d->judul }}</td>
												<td>{{ $d->mitra_nm }}</td>
												<td>{{ $d->no_sp }}</td>
												<td>{{ $d->pks }}</td>
												<td>{{ $d->tgl_jatuh_tmp }} Hari <b>({{ date('Y-m-d', strtotime('+'. $d->tgl_jatuh_tmp. ' day', strtotime($d->tgl_s_pen))) }})</b></td>
												<td>{{ $d->jml_hri }}</td>
												<td>{{ $d->tgl_last_update }}</td>
												<td>{{ $d->detail }}</td>
												<td>{{ $d->nama_modif ? $d->nama_modif .' ('. $d->modified_by .')' : '' }}</td>
												<td><a class="btn btn-sm btn-primary" style="color: #fff" href="/progress/{{ $d->id }}"><i class="fe fe-upload fe-16"></i>&nbsp;Upload Ulang</a></td>
											</tr>
											@endforeach
										</tbody>
									</table>
								</div>
							</div>
            </div>
          </div>
        </div>
      </div> <!-- end section -->
    </div> <!-- .col-12 -->
  </div> <!-- .row -->
</div>
@endsection
@section('footerS')
<script src='/js/jquery.dataTables.min.js'></script>
<script src='/js/dataTables.bootstrap4.min.js'></script>
<script type="text/javascript">
	$(function(){
		$.fn.digits = function(){
			return this.each(function(){
					$(this).text( $(this).text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") );
			});
		}

		$('.table').DataTable({
			autoWidth: true,
			columnDefs: [
				{
					targets: 'no-sort',
					orderable: false
				}
			],
			lengthMenu: [
				[16, 32, 64, -1],
				[16, 32, 64, "All"]
			]
		});

		$('.modal_comp').on('click', function (){
			var data_id = $(this).attr('data-id');
			$.ajax({
					url:"/get_ajx/get/detail/sp",
					type:"GET",
					data: {
						id : data_id,
						status: 'Rekon'
					},
					dataType: 'json',
					success: (function(data){
						console.log(data)
						var tgl = new Date(data.tgl_sp);
						tgl.setDate(tgl.getDate() + data.tgl_jatuh_tmp);

						if(data.step_id == 18){
							$('.reject_reg').css({
								'display': 'block'
							});

							$('.approve_me, .procc_mit').css({
								'display': 'none'
							});

							$('.alasan_tl').html('Tambahkan Alasan :');

							$('#detail_reg').val(data.detail)
						}else{
							$('.approve_me, .procc_mit').css({
								'display': 'block'
							});

							$('.reject_reg').css({
								'display': 'none'
							});
						}

						$('#id_boq_up').val(data.id)
						$('.judul').html(data.judul);
						$('.surat_penetapan').html(data.surat_penetapan);
						$('.tgl_s_pen').html(data.tgl_s_pen);
						$('.no_sp').html(data.no_sp);
						$('.tgl_sp').html(data.tgl_sp);
						$('.jt').html(tgl.toISOString().slice(0, 10));
						$('.durasi').html(data.tgl_jatuh_tmp);
						$('.lokasi').html(data.lokasi);
						$('.pid').html(data.id_project);
						$('.jenis_work').html(data.jenis_work);
						$('.budget_req').html(data.budget_req);
						$('.sto').html(data.sto);
						$('.pks').html(data.pks);
						$('.nama_company').html(data.mitra_nm);
						$('.total_material').html(data.total_material_rekon);
						$('.total_jasa').html(data.total_jasa_rekon);
						$('.gd').html(data.gd_rekon);
						$('.gd_ppn').html(data.gd_ppn_rekon);
						$('.total').html(data.total_rekon);
						$('.rar_down').attr('href', '/download/full_rar/'+data.id);
						$(".nama_company, .total_material, .total_jasa, .gd, .gd_ppn, .total, .budget_req").digits();
					}),
				})
		});

	});
</script>
@endsection