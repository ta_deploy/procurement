@extends('layout')
@section('title', 'Upload Kelengkapan Data Rekon')
@section('headerS')
<link rel="stylesheet" href="/css/dataTables.bootstrap4.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" rel="stylesheet" />

<style>
	@media (min-width: 768px) {
		.modal-xl {
			width: 100%;
		max-width:1200px;
		}
	}
</style>
@endsection
@section('content')
@if (Session::has('alerts'))
  @foreach(Session::get('alerts') as $alert)
    <div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
  @endforeach
@endif
<div class="modal fade" id="modal_comp" tabindex="-1" role="dialog" aria-labelledby="modal_compTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
				<h5 class="modal-title">Detail Pekerjaan <u><i><span id="judul_pekerjaan"></span></i></u></h5>
      </div>
      <div class="modal-body data_mdl" style=" overflow: hidden;">
        <div class="row">
					<div class="col-md-6">
						<div class="form-group row">
							<label class="col-form-label col-md-3">Project ID:</label>
							<div class="col-md-9">
								<textarea style="resize:none; font-weight: bold" cols='50' rows="3" class="form-control input-transparent pid" readonly></textarea>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-md-3">Jenis Pekerjaan:</label>
							<div class="col-md-9">
								<p class="form-control input-transparent" readonly><b class="jenis_work"></b></p>
							</div>
						</div>
						<div class="row border-top">
							<div class="col-md-6">
								<div class="form-group">
									<label class="col-form-label">Surat Penetapan:</label>
									<textarea style="resize:none; font-weight: bold" cols='50' rows="2" class="form-control input-transparent surat_penetapan" readonly></textarea>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="col-form-label">Tanggal Surat Penetapan:</label>
									<p class="form-control input-transparent" readonly><b class="tgl_s_pen"></b></p>
								</div>
							</div>
						</div>
						<div class="row border-top">
							<div class="col-md-6">
								<div class="form-group">
									<label class="col-form-label">Surat Pesanan:</label>
									<textarea style="resize:none; font-weight: bold" cols='50' rows="2" class="form-control input-transparent no_sp" readonly></textarea>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<label class="col-form-label">Due Date:</label>
									<p class="form-control input-transparent" readonly><b class="jt"></b></p>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<label class="col-form-label">Durasi:</label>
									<p class="form-control input-transparent" readonly><b class="durasi"></b></p>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-md-3">Lokasi:</label>
							<div class="col-md-9">
								<p class="form-control input-transparent" readonly><b class="lokasi"></b></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-md-3">PKS:</label>
							<div class="col-md-9">
								<p class="form-control input-transparent" readonly><b class="pks"></b></p>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group row">
							<label class="col-form-label col-md-3">Mitra:</label>
							<div class="col-md-9">
								<p class="form-control input-transparent" readonly><b class="nama_company"></b></p>
							</div>
						</div>
						<div class="row border-top">
							<div class="col-md-6">
								<div class="form-group">
									<label class="col-form-label">Total Materail:</label>
									<p class="form-control input-transparent" readonly><b class="total_material"></b></p>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="col-form-label">Total Jasa:</label>
									<p class="form-control input-transparent" readonly><b class="total_jasa"></b></p>
								</div>
							</div>
						</div>
						<div class="row border-top">
							<div class="col-md-12">
								<div class="form-group">
									<label class="col-form-label">Grand Total:</label>
									<p class="form-control input-transparent" readonly><b class="gd"></b></p>
								</div>
							</div>
						</div>
						<a class="btn btn-primary btn-block rar_down" href="">Download File Pekerjaan</a>
					</div>
					<div class="upload_re row col-md-12" style="display: none;">
						<form id="upload_rekon" enctype="multipart/form-data" action="/progressList/{{ Request::segment(2) }}" method="post" class="form-horizontal form-label-right">
							<div class="col-md-12">
								{{ csrf_field() }}
								<div class="form-group row">
									<label class="col-form-label col-md-3" for="upload_boq">Lampiran BOQ Rekon</label>
									<div class="col-md-9">
										<input type="hidden" id="id_boq_up" name="id_boq_up" value="">
										<input type="file" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" class="form-control" name="upload_boq">
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-md-3" for="pdf_boq_rek">PDF BOQ Rekon</label>
									<div class="col-md-9">
											<input type="file" accept="application/pdf" id="pdf_boq_rek" name="pdf_boq_rek" class="form-control input-transparent">
									</div>
								</div>
								<div class="form-group row">
									<div class="col-md-12">
										<button type="submit" class="btn btn-primary btn_full btn-block">Submit</button>
									</div>
								</div>
							</form>
							<div class="form-group row shortcut" style="display: none">
								<div class="col-md-12">
									<a type="button" href="" class="btn btn-warning btn_shortcut btn-block">Submit dengan data Plan</a>
								</div>
							</div>
							</div>
					</div>
				</div>
			</div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row justify-content-center">
    <div class="col-12">
      <h2 class="page-title">List Rekonsiliasi</h2>
      <div class="row">
        <div class="col-md-12">
          <div class="card shadow">
            <div class="card-body table-responsive">
              <ul class="nav nav-tabs mb-3" id="myTab" role="tablist">
								<li class="active"><a href="#siap_berkas" id="data2" class="nav-link active" data-toggle="tab" aria-expanded="true">Siap Dikerjakan&nbsp;{!! count($siap_berkas) != 0 ? "<span class='badge badge-primary'>".count($siap_berkas)."</span>" : '' !!}</a></li>
								<li class=""><a href="#wait_berkas" id="data2" class="nav-link" data-toggle="tab" aria-expanded="false">Proses Rekon&nbsp;{!! count($wait_berkas) != 0 ? "<span class='badge badge-info'>".count($wait_berkas)."</span>" : '' !!}</a></li>
								<li class=""><a href="#reject_berkas" id="data2" class="nav-link" data-toggle="tab" aria-expanded="false">Rekon Ditolak&nbsp;{!! count($reject_berkas) != 0 ? "<span class='badge badge-danger'>".count($reject_berkas)."</span>" : '' !!}</a></li>
              </ul>
							<div id="myTabContentLeft" class="tab-content">
								<div class="tab-pane fade show active" id="siap_berkas">
									<table class="table table-striped table-bordered table-hover">
										<thead class="thead-dark">
											<tr>
												<th class="hidden-xs">#</th>
												<th>Pekerjaan</th>
												<th>Nomor SP</th>
												<th>Nomor PKS</th>
												<th>Tanggal Dibuat</th>
												<th>Update Terakhir</th>
												<th>Umur (Hari)</th>
												<th>Keterangan Terakhir</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody id="data_table1">
											@php
											$num = 1;
											@endphp
											@foreach($siap_berkas as $d)
											<tr>
												<td class="hidden-xs">{{ $num++ }}</td>
												<td style="width: 22%;">{{ $d->judul }}</td>
												<td style="width: 22%;">{{ $d->no_sp }}</td>
												<td style="width: 20%;">{{ $d->pks }}</td>
												<td style="width: 15%;">{{ $d->created_at }}</td>
												<td>{{ $d->nama_modif ? $d->nama_modif .' ('. $d->modified_by .')' : '' }}</td>
												<td style="width: 15%;">{{ $d->jml_hri }}</td>
												<td style="width: 15%;">{{ $d->detail }}</td>
												<td>
													{{-- @if (date('Y-m-d', strtotime($d->created_at) ) == '2021-11-04')
														<a class="btn btn-primary" href="/shortcut_boq_rekon/{{ $d->id }}" style="color: #fff" data-id="{{ $d->id }}">Input</a></td>
													@else
														<a class="btn btn-primary" href="/progress/{{ $d->id }}" style="color: #fff" data-id="{{ $d->id }}">Input</a></td>
													@endif --}}
													<a class="btn btn-sm btn-primary" href="/progress/{{ $d->id }}" style="color: #fff; margin-bottom: 5px;" data-id="{{ $d->id }}"><i class="fe fe-plus fe-16"></i>&nbsp;Input</a>
													<a type="button" class="btn btn-sm btn-modal_me btn-warning" href="/rollback/{{ $d->id }}" style="margin-bottom: 5px;"><i class="fe fe-corner-up-left fe-16"></i>&nbsp;Rollback </a>
												</td>
											</tr>
											@endforeach
										</tbody>
									</table>
								</div>
								<div class="tab-pane fade" id="wait_berkas">
									<table class="table table-striped table-bordered table-hover">
										<thead class="thead-dark">
											<tr>
												<th class="hidden-xs">#</th>
												<th>Pekerjaan</th>
												<th>Nomor SP</th>
												<th>Nomor PKS</th>
												<th>Tanggal Dibuat</th>
												<th>Update Terakhir</th>
												<th>Umur (Hari)</th>
												<th>Detail</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody id="data_table2">
											@php
											$num = 1;
											@endphp
											@foreach($wait_berkas as $d)
												<tr>
													<td class="hidden-xs">{{ $num++ }}</td>
													<td style="width: 22%;">{{ $d->judul }}</td>
													<td style="width: 22%;">{{ $d->no_sp }}</td>
													<td style="width: 20%;">{{ $d->pks }}</td>
													<td style="width: 15%;">{{ $d->created_at }}</td>
													<td>{{ $d->nama_modif ? $d->nama_modif .' ('. $d->modified_by .')' : '' }}</td>
													<td style="width: 15%;">{{ $d->jml_hri }}</td>
													<td style="width: 15%;">{{ $d->detail }}</td>
													<td><a class="btn btn-primary modal_comp" data-toggle="modal" data-target="#modal_comp" style="color: #fff" data-id="{{ $d->id}}"><i class="fe fe-eye fe-16"></i>&nbsp;Detail</a></td>
												</tr>
											@endforeach
										</tbody>
									</table>
								</div>
								<div class="tab-pane fade" id="reject_berkas">
									<table class="table table-striped table-bordered table-hover">
										<thead class="thead-dark">
											<tr>
												<th class="hidden-xs">#</th>
												<th>Pekerjaan</th>
												<th>Nomor SP</th>
												<th>Nomor PKS</th>
												<th>Tanggal Dibuat</th>
												<th>Update Terakhir</th>
												<th>Umur (Hari)</th>
												<th>Detail</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody id="data_table3">
											@php
												$num = 1;
											@endphp
											@foreach($reject_berkas as $d)
											<tr>
												<td class="hidden-xs">{{ $num++ }}</td>
												<td style="width: 22%;">{{ $d->judul }}</td>
												<td style="width: 22%;">{{ $d->no_sp }}</td>
												<td style="width: 20%;">{{ $d->pks }}</td>
												<td style="width: 15%;">{{ $d->created_at }}</td>
												<td>{{ $d->nama_modif ? $d->nama_modif .' ('. $d->modified_by .')' : '' }}</td>
												<td style="width: 15%;">{{ $d->jml_hri }}</td>
												<td style="width: 15%;">{{ $d->detail }}</td>
												<td>
													{{-- @if (date('Y-m-d', strtotime($d->created_at) ) == '2021-11-04')
														<a class="btn btn-primary" href="/shortcut_boq_rekon/{{ $d->id }}" style="color: #fff" data-id="{{ $d->id }}">Input</a></td>
													@else
														<a class="btn btn-primary" href="/progress/{{ $d->id }}" style="color: #fff" data-id="{{ $d->id }}">Input</a></td>
													@endif --}}
													<a class="btn btn-primary" href="/progress/{{ $d->id }}" style="color: #fff; margin-bottom: 5px;" data-id="{{ $d->id }}"><i class="fe fe-edit-2 fe-16"></i>&nbsp;Edit</a>
													<a type="button" class="btn btn-sm btn-modal_me btn-warning" href="/rollback/{{ $d->id }}" style="margin-bottom: 5px;"><i class="fe fe-corner-up-left fe-16"></i>&nbsp;Rollback </a>
												</td>
											</tr>
											@endforeach
										</tbody>
									</table>
								</div>
							</div>
            </div>
          </div>
        </div>
      </div> <!-- end section -->
    </div> <!-- .col-12 -->
  </div> <!-- .row -->
</div>
@endsection
@section('footerS')
<script src='/js/jquery.dataTables.min.js'></script>
<script src='/js/dataTables.bootstrap4.min.js'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script type="text/javascript">
	$(function(){
		$.fn.digits = function(){
			return this.each(function(){
				$(this).text( $(this).text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") );
			});
		}

		$('select').select2();

		$('.table').DataTable({
			autoWidth: true,
			columnDefs: [
				{
					targets: 'no-sort',
					orderable: false
				}
			],
			lengthMenu: [
				[16, 32, 64, -1],
				[16, 32, 64, "All"]
			]
		});

		$('.modal_comp').on('click', function (){
			var data_id = $(this).attr('data-id');
			$.ajax({
					url:"/get_ajx/get/detail/sp",
					type:"GET",
					data: {
						id : data_id,
						status: 'SP'
					},
					dataType: 'json',
					success: (function(data){
						$('.btn_full').on('click', function(e){
							if(data.step_id == 16){
								msg = 'Pastikan BOQ Realisasi Sudah sesuai format agar tidak ditolak lagi!'
							}else{
								msg = 'Pastikan Lampiran BOQ Realisasi Di <i>upload</i> dengan format yang disediakan!'
							}
							e.preventDefault();
							let timerInterval
							swal.fire({
								showClass: {
									popup: 'animate__animated animate__fadeInDown'
								},
								hideClass: {
									popup: 'animate__animated animate__fadeOutUp'
								},
								title: '<strong><i>Submit</i> lampiran BOQ Realisasi</strong>',
								timer: 4000,
								icon: 'warning',
								html: msg,
								showCancelButton: true,
								focusConfirm: false,
								confirmButtonText:'<i class="fe fe-check fe-16"></i> Ok',
								confirmButtonColor: '#3085d6',
								cancelButtonText:'<i class="fe fe-alert-triangle fe-16"></i> Hitung Kembali',
								cancelButtonColor: '#d33',
								allowOutsideClick: false,
								allowEscapeKey: false,
								didOpen: () => {
									timerInterval = setInterval(() => {
										const left = (Swal.getTimerLeft() / 1000).toFixed(0)
										Swal.getConfirmButton().querySelector('i').textContent = left
										Swal.getConfirmButton().disabled = true;

										if(left == 0){
											Swal.stopTimer()
											clearInterval(timerInterval)
											Swal.getConfirmButton().querySelector('i').textContent = ''
											Swal.getConfirmButton().disabled = false;
										}
									}, 100)
								},
								willClose: () => {
									clearInterval(timerInterval)
								}
							}).then((result) => {
								if (result.isConfirmed) {
									$('#upload_rekon').submit()
									Swal.fire('Disubmit!', '', 'success')
								}
							})
						})
						if($.inArray(data.step_id, [6, 16]) !== -1){
							$('.upload_re').css({
								'display': 'block'
							})
						}else{
							$('.upload_re').css({
								'display': 'none'
							})
						}

						if(data.step_id == 16)
						{
							$('.detail_field').css({
								'display': 'flex'
							})
						}
						else
						{
							$('.detail_field').css({
								'display': 'none'
							})
						}

						var tgl = new Date(data.tgl_sp);
						tgl.setDate(tgl.getDate() + data.tgl_jatuh_tmp);

						$('#judul_pekerjaan').html(data.judul);
						$('.surat_penetapan').html(data.surat_penetapan);
						$('.tgl_s_pen').html(data.tgl_s_pen);
						$('.no_sp').html(data.no_sp);
						$('.tgl_sp').html(data.tgl_sp);
						$('.jt').html(data.durasi);
						$('.durasi').html(data.tgl_jatuh_tmp+ ' Hari');
						$('.lokasi').html(data.lokasi);
						$('#id_boq_up').val(data.id);
						$('.pks').html(data.pks);
						$('.nama_company').html(data.nama_company);
						$('.total_material').html(data.total_material_rekon_plan);
						$('.total_jasa').html(data.total_jasa_rekon_plan);
						$('.gd').html(data.total_material_rekon_plan + data.total_jasa_rekon_plan);
						$('.pid').html(data.id_project);
						$('.jenis_work').html(data.pekerjaan + ' | ' + data.jenis_work + ' | ' + data.jenis_kontrak);
						$('.rar_down').attr('href', '/download/full_rar/'+data.id);

						var tgl = data.created_at.split(' ');

						if(tgl[0] == '2021-11-04'){
							$(".btn_full").hide()
							$('.shortcut').show()
							$('.btn_shortcut').attr('href', '/shortcut_boq_rekon/' + data.id);
						}

						$(".nama_company, .total_material, .total_jasa, .gd, .budget_req").digits();
					}),
				})
		});


	});
</script>
@endsection