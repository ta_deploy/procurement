@extends('layout')
@section('title', 'List Pekerjaan')
@section('headerS')
<link rel="stylesheet" href="/css/dataTables.bootstrap4.css">

@endsection

@section('content')
@if (Session::has('alerts'))
  @foreach(Session::get('alerts') as $alert)
    <div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
  @endforeach
@endif
<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-12">
			<div class="row">
				<div class="col-md-12 my-4">
					<div class="card shadow mb-4">
						<div class="card-body">
							@php
								$judul = '';
								$url = explode('/', Request::url() );
								$url = strtolower(end($url) );

								switch ($url) {
									case 'user':
										$judul = "User";
									break;
									case 'mitra':
										$judul = "Mitra";
									break;
									case 'proc_a':
										$judul = "Procurement Area";
									break;
									case 'proc_r':
										$judul = "Procurement Regional";
									break;
									case 'finn':
										$judul = "Finance";
									break;
									case 'cashbank':
										$judul = "Cash & Bank";
									break;
									case 'commerce_create_pid':
										$judul = "Create PID";
									break;
									case 'operation_req_pid':
										$judul = "Request PID";
									break;
									case 'operation_upload_justif':
										$judul = "Upload Justifikasi";
									break;
									case 'proca_s_pen':
										$judul = "Surat Penetapan";
									break;
									case 'mitra_sanggup':
										$judul = "Surat Kesanggupan";
									break;
									case 'proca_sp':
										$judul = "Surat Pesanan";
									break;
									case 'mitra_boq_rek':
										$judul = "BOQ Rekon";
									break;
									case 'operation_verif_boq':
										$judul = "Amandemen";
									break;
									case 'commerce_budget':
										$judul = "Budget";
									break;
									case 'mitra_pelurusan_material':
										$judul = "Pelurusan Material";
									break;
									case 'mitra_lengkapi_nomor':
										$judul = "ABD";
									break;
									case 'proca_ver_doc_area':
										$judul = "Nomor BA";
									break;
									case 'mitra_ttd':
										$judul = "Upload TTD";
									break;
									case 'procg_ver_doc_reg':
										$judul = "Verifikasi Dokumen Regional";
									break;
									case 'finance':
										$judul = "Finance";
									break;
									case 'cb':
										$judul = "Cash & Bank";
									break;
								}
							@endphp
							<a type="button" href="/download_listspm/" class="btn btn-primary btn-sm download_excel" style="margin-bottom: 5px;"><i class="fe fe-download fe-16"></i>&nbsp;Unduh List Pekerjaan</a>
							<h5 class="card-title">List Pekerjaan {{ $judul }}</h5>
							<table id ="tb_sp_perM" class="table table-striped table-bordered table-hover">
								<thead class="thead-dark">
									<tr>
										<th>Mitra</th>
										<th>Tanggal Buat</th>
										<th>Jenis Pekerjaan</th>
										<th style="width:40%">Dokumen</th>
										<th style="width:10%">Nilai SP</th>
										<th style="width:10%">Nilai Rekon</th>
										<th style="width:10%">Step Terakhir</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody id="data_table">
									@php
										$num = 1;
									@endphp
									@forelse($data as $k => $d)
										<tr>
											<td>{{ $d->nama_company }}</td>
											<td>{{ $d->created_at }}</td>
											<td>{{ $d->pekerjaan }}</td>
											<td>{{ $d->judul_work }}</td>
											<td>{{ number_format($d->total_material_sp + $d->total_jasa_sp, 0, '.', '.') }}</td>
											<td>{{ number_format($d->total_material_rekon + $d->total_jasa_rekon, 0, '.', '.') }}</td>
											<td>{{ $d->nama_step }}</td>
											<td>
												<a style="margin-bottom: 5px;" href="/get_detail_laporan/{{$d->id}}" target="_blank" class="btn btn-sm btn-primary"><i class="fe fe-eye fe-16"></i>&nbsp;Detail</a>
												@if($d->pekerjaan == 'PSB')
												<a style="margin-bottom: 5px;" href="/download/full_zip_psb/khs_2021/{{$d->id}}" class="btn btn-sm btn-info"><i class="fe fe-download fe-16"></i>&nbsp;Download</a>
												@else
												<a style="margin-bottom: 5px;" href="/download/full_rar/{{$d->id}}" class="btn btn-sm btn-info"><i class="fe fe-download fe-16"></i>&nbsp;Download</a>
												@endif
												<a style="margin-bottom: 5px;" href="/timeline/{{$d->id}}" class="btn btn-sm btn-secondary"><i class="fe fe-clock fe-16"></i>&nbsp;Timeline</a>
											</td>
										</tr>
									@empty
									@endforelse
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('footerS')
<script src='/js/jquery.dataTables.min.js'></script>
<script src='/js/dataTables.bootstrap4.min.js'></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<script type="text/javascript">
	$(function(){
		var link = window.location.href.split('/');
		if(link[3] == 'detail_dashboard'){
			var gl = window.location.href.split('/').splice(4);
			$(".download_excel").attr('href', $('.download_excel').attr('href') + 'dd/' + gl.join('/') );
		}else if(link[3]+ '/' +link[4]+ '/' +link[5] == 'Report/list/spM'){
			var gl = window.location.href.split('/').splice(6);
			$(".download_excel").attr('href', $('.download_excel').attr('href') + 'rls/' + gl.join('/') );
		}else{
			var gl = window.location.href.split('/').splice(4);
			$(".download_excel").attr('href', $('.download_excel').attr('href') + 'dp/' + gl.join('/') );
		}

		$('#tb_sp_perM').DataTable({
			autoWidth: true,
			lengthMenu: [
				[16, 32, 64, -1],
				[16, 32, 64, "All"]
			]
		});
	});
</script>
@endsection