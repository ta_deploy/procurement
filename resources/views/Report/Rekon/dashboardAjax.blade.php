@if($jenis == 'Simple')
  @php
    $user = $mitra = $proc_a = $proc_r = $finn = $cashbank = 0;
  @endphp
<thead class="thead-dark">
  <tr>
    <th>Jenis Pekerjaan</th>
    <th colspan="{{ $data[1] }}">User / Operations</th>
    <th colspan="{{ $data[1] }}">Mitra</th>
    <th colspan="{{ $data[1] }}">Procurement Area</th>
    <th colspan="{{ $data[1] }}">Procurement Regional</th>
    <th colspan="{{ $data[1] }}">Finance</th>
    <th colspan="{{ $data[1] }}">Cash&Bank</th>
  </tr>
</thead>
<tbody>
  @php
    $user = $mitra = $proc_a = $proc_r = $finn = $cashbank = $hrg_user = $hrg_mitra = $hrg_proc_a = $hrg_proc_r = $hrg_finn = $hrg_cashbank = 0;
  @endphp
  @forelse($data[0] as $K => $d)
    @if($K != 'total')
      <tr>
        <td>{{ $K }}</td>
        @if(in_array($tampilan, ['val_price_sp']) )
          @php
            $hrg_user     += $d['data']['hrg_user'];
            $hrg_mitra    += $d['data']['hrg_mitra'];
            $hrg_proc_a   += $d['data']['hrg_proc_a'];
            $hrg_proc_r   += $d['data']['hrg_proc_r'];
            $hrg_finn     += $d['data']['hrg_finn'];
            $hrg_cashbank += $d['data']['hrg_cashbank'];

            $user     += $d['data']['user_c'];
            $mitra    += $d['data']['mitra_c'];
            $proc_a   += $d['data']['proc_a_c'];
            $proc_r   += $d['data']['proc_r_c'];
            $finn     += $d['data']['finn_c'];
            $cashbank += $d['data']['cashbank_c'];
          @endphp
          <td>
            {!! $d['data']['user_c'] ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. $K .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/user" class="badge badge-pill badge-info" style="color: white;">' . number_format($d['data']['user_c'], 0, '.', '.') .'</a>' : 0 !!}
          </td>
          <td>
            {!! $d['data']['hrg_user'] ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. $K .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/user" class="badge badge-pill badge-info" style="color: white;">Rp. ' . number_format($d['data']['hrg_user'], 0, '.', '.') .'</a>' : 0 !!}
          </td>
          <td>
            {{ $d['data']['user_per'] ?? 0 }}
          </td>
          <td>
            {!! $d['data']['mitra_c'] ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. $K .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/mitra" class="badge badge-pill badge-info" style="color: white;">' . number_format($d['data']['mitra_c'], 0, '.', '.') .'</a>' : 0 !!}
          </td>
          <td>
            {!! $d['data']['hrg_mitra'] ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. $K .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/mitra" class="badge badge-pill badge-info" style="color: white;">Rp. ' . number_format($d['data']['hrg_mitra'], 0, '.', '.') .'</a>' : 0 !!}
          </td>
          <td>
            {{ $d['data']['mitra_per'] ?? 0 }} </td>
          <td>
            {!! $d['data']['proc_a_c'] ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. $K .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .
          '/proc_a" class="badge badge-pill badge-info" style="color: white;">' . number_format($d['data']['proc_a_c'], 0, '.', '.') .'</a>' : 0 !!}
          </td>
          <td>
            {!! $d['data']['hrg_proc_a'] ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. $K .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/proc_a" class="badge badge-pill badge-info" style="color: white;">Rp. ' . number_format($d['data']['hrg_proc_a'], 0, '.', '.') .'</a>' : 0 !!}
          </td>
          <td>
            {{ $d['data']['proc_a_per'] ?? 0 }}
          </td>
          <td>
            {!! $d['data']['proc_r_c'] ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. $K .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/proc_r" class="badge badge-pill badge-info" style="color: white;">' . number_format($d['data']['proc_r_c'], 0, '.', '.') .'</a>' : 0 !!}
          </td>
          <td>
            {!! $d['data']['hrg_proc_r'] ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. $K .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/proc_r" class="badge badge-pill badge-info" style="color: white;">Rp. ' . number_format($d['data']['hrg_proc_r'], 0, '.', '.') .'</a>' : 0 !!}
          </td>
          <td>
            {{ $d['data']['proc_r_per'] ?? 0 }}
          </td>
          <td>
            {!! $d['data']['finn_c'] ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. $K .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/finn" class="badge badge-pill badge-info" style="color: white;">' . number_format($d['data']['finn_c'], 0, '.', '.') .'</a>' : 0 !!}
          </td>
          <td>
            {!! $d['data']['hrg_finn'] ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. $K .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/finn" class="badge badge-pill badge-info" style="color: white;">Rp. ' . number_format($d['data']['hrg_finn'], 0, '.', '.') .'</a>' : 0 !!}
          </td>
          <td>
            {{ $d['data']['finn_per'] ?? 0 }}
          </td>
          <td>
            {!! $d['data']['cashbank_c'] ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. $K .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/cashbank" class="badge badge-pill badge-info" style="color: white;">' . number_format($d['data']['cashbank_c'], 0, '.', '.') .'</a>' : 0 !!}
          </td>
          <td>
            {!! $d['data']['hrg_cashbank'] ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. $K .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/cashbank" class="badge badge-pill badge-info" style="color: white;">Rp. ' . number_format($d['data']['hrg_cashbank'], 0, '.', '.') .'</a>' : 0 !!}
          </td>
          <td>{{ $d['data']['cashbank_per'] ?? 0 }} </td>
        @endif

        @if(in_array($tampilan, ['price_sp']) || empty($tampilan) )
          @php
            $hrg_user     += $d['data']['hrg_user'];
            $hrg_mitra    += $d['data']['hrg_mitra'];
            $hrg_proc_a   += $d['data']['hrg_proc_a'];
            $hrg_proc_r   += $d['data']['hrg_proc_r'];
            $hrg_finn     += $d['data']['hrg_finn'];
            $hrg_cashbank += $d['data']['hrg_cashbank'];
          @endphp
          <td>
            {!! $d['data']['hrg_user'] ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. $K .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/user" class="badge url_m badge-pill badge-info" style="color: white;">Rp. ' . number_format($d['data']['hrg_user'], 0, '.', '.') .'</a>' : 0 !!}
          </td>
          <td>
            {{ $d['data']['user_per'] ?? 0 }}
          </td>
          <td>
            {!! $d['data']['hrg_mitra'] ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. $K .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/mitra" class="badge badge-pill badge-info" style="color: white;">Rp. ' . number_format($d['data']['hrg_mitra'], 0, '.', '.') .'</a>' : 0 !!}
          </td>
          <td>
            {{ $d['data']['mitra_per'] ?? 0 }}
          </td>
          <td>
            {!! $d['data']['hrg_proc_a'] ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. $K .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/proc_a" class="badge badge-pill badge-info" style="color: white;">Rp. ' . number_format($d['data']['hrg_proc_a'], 0, '.', '.') .'</a>' : 0 !!}
          </td>
          <td>
            {{ $d['data']['proc_a_per'] ?? 0 }}
          </td>
          <td>
            {!! $d['data']['hrg_proc_r'] ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. $K .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/proc_r" class="badge badge-pill badge-info" style="color: white;">Rp. ' . number_format($d['data']['hrg_proc_r'], 0, '.', '.') .'</a>' : 0 !!}
          </td>
          <td>
            {{ $d['data']['proc_r_per'] ?? 0 }}
          </td>
          <td>
            {!! $d['data']['hrg_finn'] ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. $K .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/finn" class="badge badge-pill badge-info" style="color: white;">Rp. ' . number_format($d['data']['hrg_finn'], 0, '.', '.') .'</a>' : 0 !!}
          </td>
          <td>
            {{ $d['data']['finn_per'] ?? 0 }}
          </td>
          <td>
            {!! $d['data']['hrg_cashbank'] ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. $K .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/cashbank" class="badge badge-pill badge-info" style="color: white;">Rp. ' . number_format($d['data']['hrg_cashbank'], 0, '.', '.') .'</a>' : 0 !!}
          </td>
          <td>
            {{ $d['data']['cashbank_per'] ?? 0 }}
          </td>
        @endif

        @if($tampilan == 'val_sp')
          @php
            $user     += $d['data']['user_c'];
            $mitra    += $d['data']['mitra_c'];
            $proc_a   += $d['data']['proc_a_c'];
            $proc_r   += $d['data']['proc_r_c'];
            $finn     += $d['data']['finn_c'];
            $cashbank += $d['data']['cashbank_c'];
          @endphp
          <td>
            {!! $d['data']['user_c'] ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. $K .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/user" class="badge badge-pill badge-info" style="color: white;">'. number_format($d['data']['user_c'], 0, '.', '.') .'</a>' : 0 !!}
          </td>
          <td>
            {{ $d['data']['user_per'] ?? 0 }}
          </td>
          <td>
            {!! $d['data']['mitra_c'] ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. $K .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/mitra" class="badge badge-pill badge-info" style="color: white;">'. number_format($d['data']['mitra_c'], 0, '.', '.') .'</a>' : 0 !!}
          </td>
          <td>
            {{ $d['data']['mitra_per'] ?? 0 }}
          </td>
          <td>
            {!! $d['data']['proc_a_c'] ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. $K .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/proc_a" class="badge badge-pill badge-info" style="color: white;">'. number_format($d['data']['proc_a_c'], 0, '.', '.') .'</a>' : 0 !!}
          </td>
          <td>
            {{ $d['data']['proc_a_per'] ?? 0 }}
          </td>
          <td>
            {!! $d['data']['proc_r_c'] ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. $K .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/proc_r" class="badge badge-pill badge-info" style="color: white;">'. number_format($d['data']['proc_r_c'], 0, '.', '.') .'</a>' : 0 !!}
          </td>
          <td>
            {{ $d['data']['proc_r_per'] ?? 0 }}
          </td>
          <td>
            {!! $d['data']['finn_c'] ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. $K .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/finn" class="badge badge-pill badge-info" style="color: white;">'. number_format($d['data']['finn_c'], 0, '.', '.') .'</a>' : 0 !!}
          </td>
          <td>
            {{ $d['data']['finn_per'] ?? 0 }}
          </td>
          <td>
            {!! $d['data']['cashbank_c'] ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. $K .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/cashbank" class="badge badge-pill badge-info" style="color: white;">'. number_format($d['data']['cashbank_c'], 0, '.', '.') .'</a>' : 0 !!}
          </td>
          <td>
            {{ $d['data']['cashbank_per'] ?? 0 }}
          </td>
        @endif

        @if(in_array($tampilan, ['price_rekon']) )
          @php
            $hrg_user     += $d['data']['hrg_user'];
            $hrg_mitra    += $d['data']['hrg_mitra'];
            $hrg_proc_a   += $d['data']['hrg_proc_a'];
            $hrg_proc_r   += $d['data']['hrg_proc_r'];
            $hrg_finn     += $d['data']['hrg_finn'];
            $hrg_cashbank += $d['data']['hrg_cashbank'];
          @endphp
          <td>
            {!! $d['data']['hrg_user'] ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. $K .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/user" class="badge badge-pill badge-info" style="color: white;">Rp. ' . number_format($d['data']['hrg_user'], 0, '.', '.') .'</a>' : 0 !!}
          </td>
          <td>
            {{ $d['data']['user_per'] ?? 0 }}
          </td>
          <td>
            {!! $d['data']['hrg_mitra'] ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. $K .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/mitra" class="badge badge-pill badge-info" style="color: white;">Rp. ' . number_format($d['data']['hrg_mitra'], 0, '.', '.') .'</a>' : 0 !!}
          </td>
          <td>
            {{ $d['data']['mitra_per'] ?? 0 }}
          </td>
          <td>
            {!! $d['data']['hrg_proc_a'] ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. $K .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/proc_a" class="badge badge-pill badge-info" style="color: white;">Rp. ' . number_format($d['data']['hrg_proc_a'], 0, '.', '.') .'</a>' : 0 !!}
          </td>
          <td>
            {{ $d['data']['proc_a_per'] ?? 0 }}
          </td>
          <td>
            {!! $d['data']['hrg_proc_r'] ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. $K .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/proc_r" class="badge badge-pill badge-info" style="color: white;">Rp. ' . number_format($d['data']['hrg_proc_r'], 0, '.', '.') .'</a>' : 0 !!}
          </td>
          <td>
            {{ $d['data']['proc_r_per'] ?? 0 }}
          </td>
          <td>
            {!! $d['data']['hrg_finn'] ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. $K .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/finn" class="badge badge-pill badge-info" style="color: white;">Rp. ' . number_format($d['data']['hrg_finn'], 0, '.', '.') .'</a>' : 0 !!}
          </td>
          <td>
            {{ $d['data']['finn_per'] ?? 0 }}
          </td>
          <td>
            {!! $d['data']['hrg_cashbank'] ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. $K .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/cashbank" class="badge badge-pill badge-info" style="color: white;">Rp. ' . number_format($d['data']['hrg_cashbank'], 0, '.', '.') .'</a>' : 0 !!}
          </td>
          <td>
            {{ $d['data']['cashbank_per'] ?? 0 }}
          </td>
        @endif

        @if(in_array($tampilan, ['val_sp_price_rekon']) )
          @php
            $hrg_user     += $d['data']['hrg_user'];
            $hrg_mitra    += $d['data']['hrg_mitra'];
            $hrg_proc_a   += $d['data']['hrg_proc_a'];
            $hrg_proc_r   += $d['data']['hrg_proc_r'];
            $hrg_finn     += $d['data']['hrg_finn'];
            $hrg_cashbank += $d['data']['hrg_cashbank'];

            $user     += $d['data']['user_c'];
            $mitra    += $d['data']['mitra_c'];
            $proc_a   += $d['data']['proc_a_c'];
            $proc_r   += $d['data']['proc_r_c'];
            $finn     += $d['data']['finn_c'];
            $cashbank += $d['data']['cashbank_c'];
          @endphp
          <td>
            {!! $d['data']['user_c'] ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. $K .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/user" class="badge badge-pill badge-info" style="color: white;">' . number_format($d['data']['user_c'], 0, '.', '.') .'</a>' : 0 !!}
          </td>
          <td>
            {!! $d['data']['hrg_user'] ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. $K .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/user" class="badge badge-pill badge-info" style="color: white;">Rp. ' . number_format($d['data']['hrg_user'], 0, '.', '.') .'</a>' : 0 !!}
          </td>
          <td>
            {{ $d['data']['user_per'] ?? 0 }}
          </td>
          <td>
            {!! $d['data']['mitra_c'] ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. $K .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/mitra" class="badge badge-pill badge-info" style="color: white;">' . number_format($d['data']['mitra_c'], 0, '.', '.') .'</a>' : 0 !!}
          </td>
          <td>
            {!! $d['data']['hrg_mitra'] ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. $K .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/mitra" class="badge badge-pill badge-info" style="color: white;">Rp. ' . number_format($d['data']['hrg_mitra'], 0, '.', '.') .'</a>' : 0 !!}
          </td>
          <td>
            {{ $d['data']['mitra_per'] ?? 0 }}
          </td>
          <td>
            {!! $d['data']['proc_a_c'] ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. $K .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/proc_a" class="badge badge-pill badge-info" style="color: white;">' . number_format($d['data']['proc_a_c'], 0, '.', '.') .'</a>' : 0 !!}
          </td>
          <td>
            {!! $d['data']['hrg_proc_a'] ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. $K .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/proc_a" class="badge badge-pill badge-info" style="color: white;">Rp. ' . number_format($d['data']['hrg_proc_a'], 0, '.', '.') .'</a>' : 0 !!}
          </td>
          <td>
            {{ $d['data']['proc_a_per'] ?? 0 }}
          </td>
          <td>
            {!! $d['data']['proc_r_c'] ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. $K .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/proc_r" class="badge badge-pill badge-info" style="color: white;">' . number_format($d['data']['proc_r_c'], 0, '.', '.') .'</a>' : 0 !!}
          </td>
          <td>
            {!! $d['data']['hrg_proc_r'] ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. $K .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/proc_r" class="badge badge-pill badge-info" style="color: white;">Rp. ' . number_format($d['data']['hrg_proc_r'], 0, '.', '.') .'</a>' : 0 !!}
          </td>
          <td>
            {{ $d['data']['proc_r_per'] ?? 0 }}
          </td>
          <td>
            {!! $d['data']['finn_c'] ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. $K .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/finn" class="badge badge-pill badge-info" style="color: white;">' . number_format($d['data']['finn_c'], 0, '.', '.') .'</a>' : 0 !!}
          </td>
          <td>
            {!! $d['data']['hrg_finn'] ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. $K .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/finn" class="badge badge-pill badge-info" style="color: white;">Rp. ' . number_format($d['data']['hrg_finn'], 0, '.', '.') .'</a>' : 0 !!}
          </td>
          <td>
            {{ $d['data']['finn_per'] ?? 0 }}
          </td>
          <td>
            {!! $d['data']['cashbank_c'] ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. $K .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/cashbank" class="badge badge-pill badge-info" style="color: white;">' . number_format($d['data']['cashbank_c'], 0, '.', '.') .'</a>' : 0 !!}
          </td>
          <td>
            {!! $d['data']['hrg_cashbank'] ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. $K .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/cashbank" class="badge badge-pill badge-info" style="color: white;">Rp. ' . number_format($d['data']['hrg_cashbank'], 0, '.', '.') .'</a>' : 0 !!}
          </td>
          <td>
            {{ $d['data']['cashbank_per'] ?? 0 }}
          </td>
        @endif
      </tr>
    @endif
  @empty
  @endforelse
</tbody>
  @if(count($data[0]) != 0 )
    <tfoot>
      <tr>
        <td>
          Grand Total
        </td>
        @php
          $sum_satu = array_sum([$hrg_user, $hrg_mitra, $hrg_proc_a, $hrg_proc_r, $hrg_finn, $hrg_cashbank]);
        @endphp
        @if(in_array($tampilan, ['val_price_sp']) )
          <td>{!! $user ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. (@$req['khs'] ?? 'All') .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/user" class="badge badge-pill badge-info" style="color: white"> '. number_format($user, 0, '.', '.') : 0 !!} </td>
          <td>{!! $hrg_user ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. (@$req['khs'] ?? 'All') .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/user" class="badge badge-pill badge-info" style="color: white">Rp. '. number_format($hrg_user, 0, '.', '.') : 0 !!} </td>
          <td>{{ ROUND( ($sum_satu == 0 ? 0 : $hrg_user / $sum_satu ) * 100, 1) }}%</td>
          <td>{!! $mitra ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. (@$req['khs'] ?? 'All') .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/mitra" class="badge badge-pill badge-info" style="color: white"> '. number_format($mitra, 0, '.', '.') : 0 !!} </td>
          <td>{!! $hrg_mitra ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. (@$req['khs'] ?? 'All') .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/mitra" class="badge badge-pill badge-info" style="color: white">Rp. '. number_format($hrg_mitra, 0, '.', '.') : 0 !!} </td>
          <td>{{ ROUND( ($sum_satu == 0 ? 0 : $hrg_mitra / $sum_satu ) * 100, 1) }}% </td>
          <td>{!! $proc_a ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. (@$req['khs'] ?? 'All') .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/proc_a" class="badge badge-pill badge-info" style="color: white"> '. number_format($proc_a, 0, '.', '.') : 0 !!} </td>
          <td>{!! $hrg_proc_a ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. (@$req['khs'] ?? 'All') .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/proc_a" class="badge badge-pill badge-info" style="color: white">Rp. '. number_format($hrg_proc_a, 0, '.', '.') : 0 !!} </td>
          <td>{{ ROUND( ($sum_satu == 0 ? 0 : $hrg_proc_a / $sum_satu ) * 100, 1) }}% </td>
          <td>{!! $proc_r ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. (@$req['khs'] ?? 'All') .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/proc_r" class="badge badge-pill badge-info" style="color: white"> '. number_format($proc_r, 0, '.', '.') : 0 !!} </td>
          <td>{!! $hrg_proc_r ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. (@$req['khs'] ?? 'All') .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/proc_r" class="badge badge-pill badge-info" style="color: white">Rp. '. number_format($hrg_proc_r, 0, '.', '.') : 0 !!} </td>
          <td>{{ ROUND( ($sum_satu == 0 ? 0 : $hrg_proc_r / $sum_satu ) * 100, 1) }}% </td>
          <td>{!! $finn ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. (@$req['khs'] ?? 'All') .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/finn" class="badge badge-pill badge-info" style="color: white"> '. number_format($finn, 0, '.', '.') : 0 !!} </td>
          <td>{!! $hrg_finn ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. (@$req['khs'] ?? 'All') .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/finn" class="badge badge-pill badge-info" style="color: white">Rp. '. number_format($hrg_finn, 0, '.', '.') : 0 !!} </td>
          <td>{{ ROUND( ($sum_satu == 0 ? 0 : $hrg_finn / $sum_satu ) * 100, 1) }}% </td>
          <td>{!! $cashbank ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. (@$req['khs'] ?? 'All') .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/cashbank" class="badge badge-pill badge-info" style="color: white"> '. number_format($cashbank, 0, '.', '.') : 0 !!} </td>
          <td>{!! $hrg_cashbank ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. (@$req['khs'] ?? 'All') .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/cashbank" class="badge badge-pill badge-info" style="color: white">Rp. '. number_format($hrg_cashbank, 0, '.', '.') : 0 !!} </td>
          <td>{{ ROUND( ($sum_satu == 0 ? 0 : $hrg_cashbank / $sum_satu ) * 100, 1) }}% </td>
        @endif

        @if(in_array($tampilan, ['price_sp']) || empty($tampilan) )
          <td>{!! $hrg_user ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. (@$req['khs'] ?? 'All') .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/user" class="badge badge-pill badge-info" style="color: white">Rp. '. number_format($hrg_user, 0, '.', '.') : 0 !!} </td>
          <td>{{ ROUND( ($sum_satu == 0 ? 0 : $hrg_user / $sum_satu ) * 100, 1) }}%</td>
          <td>{!! $hrg_mitra ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. (@$req['khs'] ?? 'All') .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/mitra" class="badge badge-pill badge-info" style="color: white">Rp. '. number_format($hrg_mitra, 0, '.', '.') : 0 !!} </td>
          <td>{{ ROUND( ($sum_satu == 0 ? 0 : $hrg_mitra / $sum_satu ) * 100, 1) }}% </td>
          <td>{!! $hrg_proc_a ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. (@$req['khs'] ?? 'All') .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/proc_a" class="badge badge-pill badge-info" style="color: white">Rp. '. number_format($hrg_proc_a, 0, '.', '.') : 0 !!} </td>
          <td>{{ ROUND( ($sum_satu == 0 ? 0 : $hrg_proc_a / $sum_satu ) * 100, 1) }}% </td>
          <td>{!! $hrg_proc_r ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. (@$req['khs'] ?? 'All') .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/proc_r" class="badge badge-pill badge-info" style="color: white">Rp. '. number_format($hrg_proc_r, 0, '.', '.') : 0 !!} </td>
          <td>{{ ROUND( ($sum_satu == 0 ? 0 : $hrg_proc_r / $sum_satu ) * 100, 1) }}% </td>
          <td>{!! $hrg_finn ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. (@$req['khs'] ?? 'All') .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/finn" class="badge badge-pill badge-info" style="color: white">Rp. '. number_format($hrg_finn, 0, '.', '.') : 0 !!} </td>
          <td>{{ ROUND( ($sum_satu == 0 ? 0 : $hrg_finn / $sum_satu ) * 100, 1) }}% </td>
          <td>{!! $hrg_cashbank ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. (@$req['khs'] ?? 'All') .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/cashbank" class="badge badge-pill badge-info" style="color: white">Rp. '. number_format($hrg_cashbank, 0, '.', '.') : 0 !!} </td>
          <td>{{ ROUND( ($sum_satu == 0 ? 0 : $hrg_cashbank / $sum_satu ) * 100, 1) }}% </td>
        @endif

        @if($tampilan == 'val_sp')
          @php
            $sum_hrg_tot = array_sum([$user, $mitra, $proc_a, $proc_r, $finn, $cashbank]);
          @endphp
          <td>{!! $user ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. (@$req['khs'] ?? 'All') .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/user" class="badge badge-pill badge-info" style="color: white;">' . $user . '</a>' : 0 !!} </td>
          <td>{{ ROUND( ( $sum_hrg_tot == 0 ? 0 : $user / $sum_hrg_tot ) * 100, 1) }}%</td>
          <td>{!! $mitra ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. (@$req['khs'] ?? 'All') .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/mitra" class="badge badge-pill badge-info" style="color: white;">' . $mitra . '</a>' : 0 !!} </td>
          <td>{{ ROUND( ( $sum_hrg_tot == 0 ? 0 : $mitra / $sum_hrg_tot ) * 100, 1) }}% </td>
          <td>{!! $proc_a ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. (@$req['khs'] ?? 'All') .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/proc_a" class="badge badge-pill badge-info" style="color: white;">' . $proc_a . '</a>' : 0 !!} </td>
          <td>{{ ROUND( ( $sum_hrg_tot == 0 ? 0 : $proc_a / $sum_hrg_tot ) * 100, 1) }}% </td>
          <td>{!! $proc_r ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. (@$req['khs'] ?? 'All') .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/proc_r" class="badge badge-pill badge-info" style="color: white;">' . $proc_r . '</a>' : 0 !!} </td>
          <td>{{ ROUND( ( $sum_hrg_tot == 0 ? 0 : $proc_r / $sum_hrg_tot ) * 100, 1) }}% </td>
          <td>{!! $finn ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. (@$req['khs'] ?? 'All') .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/finn" class="badge badge-pill badge-info" style="color: white;">' . $finn . '</a>' : 0 !!} </td>
          <td>{{ ROUND( ( $sum_hrg_tot == 0 ? 0 : $finn / $sum_hrg_tot ) * 100, 1) }}% </td>
          <td>{!! $cashbank ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. (@$req['khs'] ?? 'All') .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/cashbank" class="badge badge-pill badge-info" style="color: white;">' . $cashbank . '</a>' : 0 !!} </td>
          <td>{{ ROUND( ( $sum_hrg_tot == 0 ? 0 : $cashbank / $sum_hrg_tot ) * 100, 1) }}% </td>
        @endif

        @if(in_array($tampilan, ['price_rekon']) )
          <td>{!! $hrg_user ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. (@$req['khs'] ?? 'All') .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/user" class="badge badge-pill badge-info" style="color: white">Rp. '. number_format($hrg_user, 0, '.', '.') : 0 !!} </td>
          <td>{{ ROUND( ($sum_satu == 0 ? 0 : $hrg_user / $sum_satu ) * 100, 1) }}%</td>
          <td>{!! $hrg_mitra ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. (@$req['khs'] ?? 'All') .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/mitra" class="badge badge-pill badge-info" style="color: white">Rp. '. number_format($hrg_mitra, 0, '.', '.') : 0 !!} </td>
          <td>{{ ROUND( ($sum_satu == 0 ? 0 : $hrg_mitra / $sum_satu ) * 100, 1) }}% </td>
          <td>{!! $hrg_proc_a ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. (@$req['khs'] ?? 'All') .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/proc_a" class="badge badge-pill badge-info" style="color: white">Rp. '. number_format($hrg_proc_a, 0, '.', '.') : 0 !!} </td>
          <td>{{ ROUND( ($sum_satu == 0 ? 0 : $hrg_proc_a / $sum_satu ) * 100, 1) }}% </td>
          <td>{!! $hrg_proc_r ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. (@$req['khs'] ?? 'All') .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/proc_r" class="badge badge-pill badge-info" style="color: white">Rp. '. number_format($hrg_proc_r, 0, '.', '.') : 0 !!} </td>
          <td>{{ ROUND( ($sum_satu == 0 ? 0 : $hrg_proc_r / $sum_satu ) * 100, 1) }}% </td>
          <td>{!! $hrg_finn ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. (@$req['khs'] ?? 'All') .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/finn" class="badge badge-pill badge-info" style="color: white">Rp. '. number_format($hrg_finn, 0, '.', '.') : 0 !!} </td>
          <td>{{ ROUND( ($sum_satu == 0 ? 0 : $hrg_finn / $sum_satu ) * 100, 1) }}% </td>
          <td>{!! $hrg_cashbank ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. (@$req['khs'] ?? 'All') .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/cashbank" class="badge badge-pill badge-info" style="color: white">Rp. '. number_format($hrg_cashbank, 0, '.', '.') : 0 !!} </td>
          <td>{{ ROUND( ($sum_satu == 0 ? 0 : $hrg_cashbank / $sum_satu ) * 100, 1) }}% </td>
        @endif

        @if(in_array($tampilan, ['val_sp_price_rekon']) )
          <td>{!! $user ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. (@$req['khs'] ?? 'All') .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/user" class="badge badge-pill badge-info" style="color: white"> '. number_format($user, 0, '.', '.') : 0 !!} </td>
          <td>{!! $hrg_user ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. (@$req['khs'] ?? 'All') .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/user" class="badge badge-pill badge-info" style="color: white">Rp. '. number_format($hrg_user, 0, '.', '.') : 0 !!} </td>
          <td>{{ ROUND( ($sum_satu == 0 ? 0 : $hrg_user / $sum_satu ) * 100, 1) }}%</td>
          <td>{!! $mitra ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. (@$req['khs'] ?? 'All') .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/mitra" class="badge badge-pill badge-info" style="color: white"> '. number_format($mitra, 0, '.', '.') : 0 !!} </td>
          <td>{!! $hrg_mitra ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. (@$req['khs'] ?? 'All') .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/mitra" class="badge badge-pill badge-info" style="color: white">Rp. '. number_format($hrg_mitra, 0, '.', '.') : 0 !!} </td>
          <td>{{ ROUND( ($sum_satu == 0 ? 0 : $hrg_mitra / $sum_satu ) * 100, 1) }}% </td>
          <td>{!! $proc_a ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. (@$req['khs'] ?? 'All') .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/proc_a" class="badge badge-pill badge-info" style="color: white"> '. number_format($proc_a, 0, '.', '.') : 0 !!} </td>
          <td>{!! $hrg_proc_a ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. (@$req['khs'] ?? 'All') .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/proc_a" class="badge badge-pill badge-info" style="color: white">Rp. '. number_format($hrg_proc_a, 0, '.', '.') : 0 !!} </td>
          <td>{{ ROUND( ($sum_satu == 0 ? 0 : $hrg_proc_a / $sum_satu ) * 100, 1) }}% </td>
          <td>{!! $proc_r ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. (@$req['khs'] ?? 'All') .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/proc_r" class="badge badge-pill badge-info" style="color: white"> '. number_format($proc_r, 0, '.', '.') : 0 !!} </td>
          <td>{!! $hrg_proc_r ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. (@$req['khs'] ?? 'All') .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/proc_r" class="badge badge-pill badge-info" style="color: white">Rp. '. number_format($hrg_proc_r, 0, '.', '.') : 0 !!} </td>
          <td>{{ ROUND( ($sum_satu == 0 ? 0 : $hrg_proc_r / $sum_satu ) * 100, 1) }}% </td>
          <td>{!! $finn ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. (@$req['khs'] ?? 'All') .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/finn" class="badge badge-pill badge-info" style="color: white"> '. number_format($finn, 0, '.', '.') : 0 !!} </td>
          <td>{!! $hrg_finn ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. (@$req['khs'] ?? 'All') .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/finn" class="badge badge-pill badge-info" style="color: white">Rp. '. number_format($hrg_finn, 0, '.', '.') : 0 !!} </td>
          <td>{{ ROUND( ($sum_satu == 0 ? 0 : $hrg_finn / $sum_satu ) * 100, 1) }}% </td>
          <td>{!! $cashbank ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. (@$req['khs'] ?? 'All') .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/cashbank" class="badge badge-pill badge-info" style="color: white"> '. number_format($cashbank, 0, '.', '.') : 0 !!} </td>
          <td>{!! $hrg_cashbank ? '<a href="/detail_dashboard/simple/'. (@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') .'/'. (@$req['khs'] ?? 'All') .'/'. $req['tampilan'] .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period'] .'/cashbank" class="badge badge-pill badge-info" style="color: white">Rp. '. number_format($hrg_cashbank, 0, '.', '.') : 0 !!} </td>
          <td>{{ ROUND( ($sum_satu == 0 ? 0 : $hrg_cashbank / $sum_satu ) * 100, 1) }}% </td>
        @endif
      </tr>
    </tfoot>
  @else
  @endif
@else
<thead class="thead-dark">
  <tr role="row">
    <th rowspan="2">NAMA MITRA</th>
    <th colspan="{{ @$req['khs'] != 'PSB' ? 7 : 2 }}">PRA KERJA</th>
    <th colspan="{{ @$req['khs'] != 'PSB' ? 10 : 7 }}">PASCA KERJA</th>
    <th rowspan="2">TOTAL MITRA</th>
  </tr>
  <tr>
    @if(@$req['khs'] != 'PSB')
      <th>REQUEST PID <br/><b>(Operation)</b></th>
      <th>INPUT PID <br/><b>(Commerce)</b></th>
      <th>JUSTIFIKASI <br/><b>(Operation)</b></th>
      <th>SURAT PENETAPAN <br/><b>(Proc. Area)</b></th>
      <th>KESANGGUPAN <br/><b>(Mitra)</b></th>
    @endif
    <th>SURAT PESANAN <br/><b>(Proc. Area)</b></th>
    <th>TOTAL PRA KERJA</th>
    {{-- rekon --}}
    @if(@$req['khs'] != 'PSB')
      <th>INPUT BOQ REKON <br/><b>(Mitra)</b></th>
    @endif
      <th>VERIFIKASI BOQ REKON <br/><b>(Operation)</b></th>
    @if(@$req['khs'] != 'PSB')
      <th>PELURUSAN RFC <br/><b>(Mitra)</b></th>
      <th>BUDGETING <br/><b>(Commerce)</b></th>
    @endif
    <th>INPUT NOMOR BA <br/><b>(Proc. Area)</b></th>
    <th>INPUT KELENGKAPAN NOMOR <br/><b>(Mitra)</b></th>
    <th>DOKUMEN TTD <br/><b>(Proc. Area & Mitra)</b></th>
    <th>VERIFIKASI DOKUMEN REKON REGIONAL <br/><b>(Proc. Regional)</b></th>
    <th>VERIFIKASI FINNANCE <br/><b>(Finance)</b></th>
    <th>TOTAL PASCA KERJA</th>
  </tr>
</thead>
<tbody>
  @php
    $operation_req_pid = $commerce_create_pid = $operation_upload_justif = $proca_s_pen = $mitra_sanggup = $proca_sp = $total_pra_kerja = $mitra_boq_rek = $operation_verif_boq = $mitra_pelurusan_material = $commerce_budget = $mitra_lengkapi_nomor = $proca_ver_doc_area = $mitra_ttd = $procg_ver_doc_reg = $finance = $total_pasca_kerja = $grand_total = 0;
  @endphp
  @forelse ($data as $v)
    @php
      $operation_req_pid += $v->operation_req_pid;
      $commerce_create_pid += $v->commerce_create_pid;
      $operation_upload_justif += $v->operation_upload_justif;
      $proca_s_pen += $v->proca_s_pen;
      $mitra_sanggup += $v->mitra_sanggup;
      $proca_sp += $v->proca_sp;
      $total_pra_kerja += $v->total_pra_kerja;
      // rekon
      $mitra_boq_rek += $v->mitra_boq_rek;
      $operation_verif_boq += $v->operation_verif_boq;
      $mitra_pelurusan_material += $v->mitra_pelurusan_material;
      $commerce_budget += $v->commerce_budget;
      $proca_ver_doc_area += $v->proca_ver_doc_area;
      $mitra_lengkapi_nomor += $v->mitra_lengkapi_nomor;
      $mitra_ttd += $v->mitra_ttd;
      $procg_ver_doc_reg += $v->procg_ver_doc_reg;
      $finance += $v->finance;
      $total_pasca_kerja += $v->total_pasca_kerja;
      $grand_total += $v->total;
    @endphp
    <tr>
      <td>{{ $v->label_mitra }}</td>
      @if(@$req['khs'] != 'PSB')
        <td style="background-color: #ebff00;">{!! $v->operation_req_pid != 0 ? "<a class='url_m' href='/detail_per/".(@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/' . $v->mitra_nm . '/' . (@$req['khs'] ?? 'All') .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period']."/operation_req_pid'>$v->operation_req_pid</a>" : '-' !!}</td>
        <td style="background-color: #ebff00;">{!! $v->commerce_create_pid != 0 ? "<a class='url_m' href='/detail_per/".(@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/' . $v->mitra_nm . '/' . (@$req['khs'] ?? 'All') .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period']."/commerce_create_pid'>$v->commerce_create_pid</a>" : '-' !!}</td>
        <td style="background-color: #ebff00;">{!! $v->operation_upload_justif != 0 ? "<a class='url_m' href='/detail_per/".(@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/' . $v->mitra_nm . '/' . (@$req['khs'] ?? 'All') .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period']."/operation_upload_justif'>$v->operation_upload_justif</a>" : '-' !!}</td>
        <td style="background-color: #ebff00;">{!! $v->proca_s_pen != 0 ? "<a class='url_m' href='/detail_per/".(@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/' . $v->mitra_nm . '/' . (@$req['khs'] ?? 'All') .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period']."/proca_s_pen'>$v->proca_s_pen</a>" : '-' !!}</td>
        <td style="background-color: #ebff00;">{!! $v->mitra_sanggup != 0 ? "<a class='url_m' href='/detail_per/".(@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/' . $v->mitra_nm . '/' . (@$req['khs'] ?? 'All') .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period']."/mitra_sanggup'>$v->mitra_sanggup</a>" : '-' !!}</td>
      @endif
      <td style="background-color: #ebff00;">{!! $v->proca_sp != 0 ? "<a class='url_m' href='/detail_per/".(@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/' . $v->mitra_nm . '/' . (@$req['khs'] ?? 'All') .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period']."/proca_sp'>$v->proca_sp</a>" : '-' !!}</td>
      <td style="background-color: #ebff00;">{!! $v->total_pra_kerja != 0 ? "<a class='url_m' href='/detail_per/".(@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/' . $v->mitra_nm . '/' . (@$req['khs'] ?? 'All') .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period']."/total_pra_kerja'>$v->total_pra_kerja</a>" : '-' !!}</td>
      @if(@$req['khs'] != 'PSB')
        <td style="background-color: #42ff00;">{!! $v->mitra_boq_rek != 0 ? "<a class='url_m' href='/detail_per/".(@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/' . $v->mitra_nm . '/' . (@$req['khs'] ?? 'All') .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period']."/mitra_boq_rek'>$v->mitra_boq_rek</a>" : '-' !!}</td>
        @endif
      <td style="background-color: #42ff00;">{!! $v->operation_verif_boq != 0 ? "<a class='url_m' href='/detail_per/".(@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/' . $v->mitra_nm . '/' . (@$req['khs'] ?? 'All') .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period']."/operation_verif_boq'>$v->operation_verif_boq</a>" : '-' !!}</td>
      @if(@$req['khs'] != 'PSB')
        <td style="background-color: #42ff00;">{!! $v->mitra_pelurusan_material != 0 ? "<a class='url_m' href='/detail_per/".(@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/' . $v->mitra_nm . '/' . (@$req['khs'] ?? 'All') .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period']."/mitra_pelurusan_material'>$v->mitra_pelurusan_material</a>" : '-' !!}</td>
        <td style="background-color: #42ff00;">{!! $v->commerce_budget != 0 ? "<a class='url_m' href='/detail_per/".(@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/' . $v->mitra_nm . '/' . (@$req['khs'] ?? 'All') .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period']."/commerce_budget'>$v->commerce_budget</a>" : '-' !!}</td>
      @endif
      <td style="background-color: #42ff00;">{!! $v->proca_ver_doc_area != 0 ? "<a class='url_m' href='/detail_per/".(@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/' . $v->mitra_nm . '/' . (@$req['khs'] ?? 'All') .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period']."/proca_ver_doc_area'>$v->proca_ver_doc_area</a>" : '-' !!}</td>
      <td style="background-color: #42ff00;">{!! $v->mitra_lengkapi_nomor != 0 ? "<a class='url_m' href='/detail_per/".(@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/' . $v->mitra_nm . '/' . (@$req['khs'] ?? 'All') .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period']."/mitra_lengkapi_nomor'>$v->mitra_lengkapi_nomor</a>" : '-' !!}</td>
      <td style="background-color: #42ff00;">{!! $v->mitra_ttd != 0 ? "<a class='url_m' href='/detail_per/".(@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/' . $v->mitra_nm . '/' . (@$req['khs'] ?? 'All') .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period']."/mitra_ttd'>$v->mitra_ttd</a>" : '-' !!}</td>
      <td style="background-color: #42ff00;">{!! $v->procg_ver_doc_reg != 0 ? "<a class='url_m' href='/detail_per/".(@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/' . $v->mitra_nm . '/' . (@$req['khs'] ?? 'All') .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period']."/procg_ver_doc_reg'>$v->procg_ver_doc_reg</a>" : '-' !!}</td>
      <td style="background-color: #42ff00;">{!! $v->finance != 0 ? "<a class='url_m' href='/detail_per/".(@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/' . $v->mitra_nm . '/' . (@$req['khs'] ?? 'All') .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period']."/finance'>$v->finance</a>" : '-' !!}</td>
      <td style="background-color: #42ff00;">{!! $v->total_pasca_kerja != 0 ? "<a class='url_m' href='/detail_per/".(@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/' . $v->mitra_nm . '/' . (@$req['khs'] ?? 'All') .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period']."/total_pasca_kerja'>$v->total_pasca_kerja</a>" : '-' !!}</td>
      <td>{!! $v->total != 0 ? "<a class='url_m' href='/detail_per/".(@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/' . $v->mitra_nm . '/' . (@$req['khs'] ?? 'All') .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period']."/all'>$v->total</a>" : '-' !!}</td>
    </tr>
  @empty
  @endforelse
</tbody>
<tfoot>
  <tr>
    <td>Total</td>
    @if(@$req['khs'] != 'PSB')
      <td style="background-color: #ebff00;">{!! $operation_req_pid != 0 ? "<a href='/detail_per/".(@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') . '/'. (@$req['khs'] ?? 'All') .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period']."/operation_req_pid'>$operation_req_pid</a>" : '-' !!}</td>
      <td style="background-color: #ebff00;">{!! $commerce_create_pid != 0 ? "<a href='/detail_per/".(@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') . '/'. (@$req['khs'] ?? 'All') .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period']."/commerce_create_pid'>$commerce_create_pid</a>" : '-' !!}</td>
      <td style="background-color: #ebff00;">{!! $operation_upload_justif != 0 ? "<a href='/detail_per/".(@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') . '/'. (@$req['khs'] ?? 'All') .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period']."/operation_upload_justif'>$operation_upload_justif</a>" : '-' !!}</td>
      <td style="background-color: #ebff00;">{!! $proca_s_pen != 0 ? "<a href='/detail_per/".(@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') . '/'. (@$req['khs'] ?? 'All') .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period']."/proca_s_pen'>$proca_s_pen</a>" : '-' !!}</td>
      <td style="background-color: #ebff00;">{!! $mitra_sanggup != 0 ? "<a href='/detail_per/".(@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') . '/'. (@$req['khs'] ?? 'All') .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period']."/mitra_sanggup'>$mitra_sanggup</a>" : '-' !!}</td>
    @endif
    <td style="background-color: #ebff00;">{!! $proca_sp != 0 ? "<a href='/detail_per/".(@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') . '/'. (@$req['khs'] ?? 'All') .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period']."/proca_sp'>$proca_sp</a>" : '-' !!}</td>
    <td style="background-color: #ebff00;">{!! $total_pra_kerja != 0 ? "<a href='/detail_per/".(@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') . '/'. (@$req['khs'] ?? 'All') .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period']."/total_pra_kerja'>$total_pra_kerja</a>" : '-' !!}</td>
    @if(@$req['khs'] != 'PSB')
      <td style="background-color: #42ff00;">{!! $mitra_boq_rek != 0 ? "<a href='/detail_per/".(@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') . '/'. (@$req['khs'] ?? 'All') .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period']."/mitra_boq_rek'>$mitra_boq_rek</a>" : '-' !!}</td>
    @endif
    <td style="background-color: #42ff00;">{!! $operation_verif_boq != 0 ? "<a href='/detail_per/".(@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') . '/'. (@$req['khs'] ?? 'All') .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period']."/operation_verif_boq'>$operation_verif_boq</a>" : '-' !!}</td>
    @if(@$req['khs'] != 'PSB')
      <td style="background-color: #42ff00;">{!! $mitra_pelurusan_material != 0 ? "<a href='/detail_per/".(@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') . '/'. (@$req['khs'] ?? 'All') .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period']."/mitra_pelurusan_material'>$mitra_pelurusan_material</a>" : '-' !!}</td>
      <td style="background-color: #42ff00;">{!! $commerce_budget != 0 ? "<a href='/detail_per/".(@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') . '/'. (@$req['khs'] ?? 'All') .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period']."/commerce_budget'>$commerce_budget</a>" : '-' !!}</td>
    @endif
    <td style="background-color: #42ff00;">{!! $proca_ver_doc_area != 0 ? "<a href='/detail_per/".(@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') . '/'. (@$req['khs'] ?? 'All') .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period']."/proca_ver_doc_area'>$proca_ver_doc_area</a>" : '-' !!}</td>
    <td style="background-color: #42ff00;">{!! $mitra_lengkapi_nomor != 0 ? "<a href='/detail_per/".(@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') . '/'. (@$req['khs'] ?? 'All') .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period']."/mitra_lengkapi_nomor'>$mitra_lengkapi_nomor</a>" : '-' !!}</td>
    <td style="background-color: #42ff00;">{!! $mitra_ttd != 0 ? "<a href='/detail_per/".(@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') . '/'. (@$req['khs'] ?? 'All') .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period']."/mitra_ttd'>$mitra_ttd</a>" : '-' !!}</td>
    <td style="background-color: #42ff00;">{!! $procg_ver_doc_reg != 0 ? "<a href='/detail_per/".(@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') . '/'. (@$req['khs'] ?? 'All') .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period']."/procg_ver_doc_reg'>$procg_ver_doc_reg</a>" : '-' !!}</td>
    <td style="background-color: #42ff00;">{!! $finance != 0 ? "<a href='/detail_per/".(@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') . '/'. (@$req['khs'] ?? 'All') .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period']."/finance'>$finance</a>" : '-' !!}</td>
    <td style="background-color: #42ff00;">{!! $total_pasca_kerja != 0 ? "<a href='/detail_per/".(@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') . '/'. (@$req['khs'] ?? 'All') .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period']."/total_pasca_kerja'>$total_pasca_kerja</a>" : '-' !!}</td>
    <td>{!! $grand_total != 0 ? "<a href='/detail_per/".(@$req['witel'] ?? 'All') .'/'. (@$req['tgl_hidden'] ?? 'All') .'/'. (@$req['tgl'] ?? 'all') .'/'. (@$req['mitra'] ?? 'all') . '/'. (@$req['khs'] ?? 'All') .'/'. $req['tgl_progress_kerja'] .'/'. $req['jenis_period']."/all'>$grand_total</a>" : '-' !!}</td>
  </tr>
</tfoot>
@endif