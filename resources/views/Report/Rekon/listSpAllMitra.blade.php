@extends('layout')
@section('title', 'Mitra SP')
@section('style')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<style type="text/css">
	.select2-results__option{
		color: black;
	}
</style>
@endsection
@section('content')
@if (Session::has('alerts'))
  @foreach(Session::get('alerts') as $alert)
    <div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
  @endforeach
@endif
<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-12">
			<div class="row">
				<div class="col-md-12 my-4">
					<div class="card shadow mb-4">
						<div class="card-body">
							<h5 class="card-title">Data Mitra</h5>
							<div class="list-group list-group-flush my-n3">
								@foreach($final_data as $key => $raw_data)
									<a href='/Report/list/spM/{{ $raw_data->mitra_id }}/{{ Request::segment(4) }}' class="url_m list-group-item">
										<div class="row align-items-center">
											<div class="col-auto">
												<span class="fe fe-box fe-24"></span>
											</div>
											<div class="col">
												<span class="badge badge-info">{{ $raw_data->jml }}</span>
												<small><strong>{{ $raw_data->nama_company }}</strong></small>
											</div>
										</div>
									</a>
								@endforeach
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('footerS')
<script>
  var url = window.location,
  re_des = url.toString().split('?');

  if(re_des){
    $('.url_m').each(function( index ) {
      var link = $(this).attr('href');
      $(this).attr('href', link + '?' + re_des[1] )
    });
	}
</script>
@endsection