@extends('layout')
@section('title', 'Form Dalapa')
@section('headerS')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="/css/dataTables.bootstrap4.css">
<link rel="stylesheet" href="/css/daterangepicker.css">
<style type="text/css">
	.pull-right {
		text-align: right;
	}
</style>
@endsection
@section('content')
@if (Session::has('alerts'))
  @foreach(Session::get('alerts') as $alert)
    <div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
  @endforeach
@endif
<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-md-12">
			<h2 class="page-title">Dashboard BOQ Dalapa</h2>
			<div class="card-deck" style="display: block">
        <div class="col-md-12">
          <form class="row" method="get">
            {{ csrf_field() }}
            <div class="col-md-12">
              <div class="card shadow mb-4">
                <div class="card-body">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group row">
                        <label class="col-form-label col-md-2 pull-right" for="bulan">Bulan</label>
                        <div class="col-md-10">
                          <select class="select2 form-control input-transparent" name="bulan" id="bulan">
                            <option value="1">Januari</option>
                            <option value="2">Februari</option>
                            <option value="3">Maret</option>
                            <option value="4">April</option>
                            <option value="5">Mei</option>
                            <option value="6">Juni</option>
                            <option value="7">Juli</option>
                            <option value="8">Agustus</option>
                            <option value="9">September</option>
                            <option value="10">Oktober</option>
                            <option value="11">November</option>
                            <option value="12">Desember</option>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group row">
                        <label class="col-form-label col-md-2 pull-right" for="tahun">Tahun</label>
                        <div class="col-md-10">
                          <select class="select2 form-control input-transparent" name="tahun" id="tahun">
                            @for ($tgl = 2021; $tgl <= date('Y'); $tgl++ )
                              <option value="{{ $tgl }}">{{ $tgl }}</option>
                            @endfor
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-form-label col-md-2 pull-right" for="witel">Witel</label>
                    <div class="col-md-10">
                      @if(!in_array(session('auth')->proc_level, [4, 99, 44]) )
                        <select class="select2 form-control input-transparent" name="witel" id="witel">
                          @foreach ($witel as $val)
                            <option value="{{ $val }}">{{ $val }}</option>
                          @endforeach
                        </select>
                      @else
                        <input type="text" class="form-control input-transparent" name="witel" id="witel_manual" value="{{ session('auth')->Witel_New }}" readonly>
                      @endif
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-form-label col-md-2 pull-right" for="mitra">Mitra</label>
                    <div class="col-md-10">
                      @if(!in_array(session('auth')->proc_level, [4, 99, 44]) )
                        <select class="select2 form-control input-transparent" name="mitra" id="mitra"></select>
                      @else
                      <select class="form-control input-transparent" name="mitra" id="mitra_manual" readonly>
                        <option value="{{ session('auth')->dalapa_mitra_id }}">{{ session('auth')->nama_company }}</option>
                      </select>
                      @endif
                    </div>
                  </div>
                  <div class="form-group mb-3">
                    <div class="custom-file">
                      <button type="submit" class="btn btn-block btn-primary"><i class="fe fe-zoom-in fe-16"></i>&nbsp;Cari Data</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
        <div class="col-md-12">
          <div class="card shadow mb-4">
            <div class="card-header">
              <strong class="card-title">Daftar BOQ Pekerjaan</strong>
            </div>
            <div class="card-body table-responsive">
              @if($result)
                @if(file_exists(public_path()."/excel/ODP_SEHAT_". session('auth')->dalapa_mitra_id ."_". $req->tahun ."_". $req->bulan. ".xlsx") )
                  <a type="button" href="/excel/ODP_SEHAT_{{ session('auth')->dalapa_mitra_id }}_{{ $req->tahun }}_{{ $req->bulan }}.xlsx" class="btn btn-info" style="color:#fff">Download BOQ Dalapa - ODP</a>
                @endif
                @if(file_exists(public_path()."/excel/ODC_SEHAT_". session('auth')->dalapa_mitra_id ."_". $req->tahun ."_". $req->bulan. ".xlsx") )
                  <a type="button" href="/excel/ODC_SEHAT_{{ session('auth')->dalapa_mitra_id }}_{{ $req->tahun }}_{{ $req->bulan }}.xlsx" class="btn btn-info" style="color:#fff">Download BOQ Dalapa - ODC</a>
                @endif
              @endif
              <table id ="tb_rekon" class="table table-striped table-bordered table-hover">
                <thead class="thead-dark">
                  <tr>
                    <th class="hidden-xs">#</th>
                    <th>Witel</th>
                    <th>Mitra</th>
                    <th>Tim</th>
                    <th>Teknisi</th>
                    <th>Status BOQ</th>
                    <th>Tanggal Pengerjaan</th>
                    <th>Alpro</th>
                    <th>Pekerjaan</th>
                    <th>Nilai Total</th>
                  </tr>
                </thead>
                <tbody>
                  @forelse ($result as $k => $val)
                    <tr>
                      <td>{{ ++$k }}</td>
                      <td>{{ session('auth')->Witel_New }}</td>
                      <td>{{ $val->nama_mitra }}</td>
                      <td>{{ $val->nama_team }}</td>
                      <td>{{ $val->workforce_name_conf }}</td>
                      <td>{{ $val->on_progress }}</td>
                      <td>{{ $val->date_complete ?? '-' }}</td>
                      <td>{{ $val->alpro_name }}</td>
                      <td>{{ $val->jenis_pekerjaan }}</td>
                      <td>{{ number_format($val->total_hrg, 0, '.', ',') }}</td>
                    </tr>
                  @empty
                  @endforelse
                </tbody>
              </table>
            </div>
          </div>
        </div>
		  </div>
	  </div>
  </div>
</div>
@endsection
@section('footerS')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src='/js/jquery.dataTables.min.js'></script>
<script src='/js/dataTables.bootstrap4.min.js'></script>
<script src="/js/moment.min.js"></script>
<script src='/js/jquery.timepicker.js'></script>
<script src='/js/daterangepicker.js'></script>
<script type="text/javascript">
	$(function(){

    var mitra = {!! json_encode($mitra_modify) !!};

    if($('#witel') )
    {
      $('#witel').val(null).change()
      $('#witel').on('select2:clear change', function() {
        var isi = $(this).val(),
        isi_select = [],
        pra_select = {};
        if(isi == 'KALSEL'){
          pra_select.KALSEL = mitra.KALSEL;
        }else if(isi == 'BALIKPAPAN'){
          pra_select.BALIKPAPAN = mitra.BALIKPAPAN;
        }else{
          pra_select = {};
        }
        console.log(pra_select, mitra.KALSEL)
        if ($("#mitra").hasClass("select2-hidden-accessible")) {
          // $("#mitra").select2('destroy');
          $("#mitra").empty().change();
        }

        if(!$.isEmptyObject(pra_select)){
          $.each(pra_select, function(key, index){
            $.each(index, function(keyc1, indexc1){
              isi_select.push({
                id: indexc1.id,
                text: indexc1.text
              })
            })
          })
          if(isi_select)
          {
            $("#mitra").select2({
              theme: 'bootstrap4',
              width: '100%',
              data: isi_select
            }).change();
          }
        }
        else
        {
          if ($("#mitra").hasClass("select2-hidden-accessible")) {
            // $("#mitra").select2('destroy');
            $("#mitra").empty().change();
          }
        }
      })
    }

    var bulan = {!! json_encode($req->bulan) !!},
    tahun = {!! json_encode($req->tahun) !!},
    witel = {!! json_encode($req->witel) !!},
    mitra_load = {!! json_encode($req->mitra) !!};

    if(witel != null)
    {
      $('#bulan').val(bulan).change();
      $('#tahun').val(tahun).change();
      $('#witel').val(witel).change();
      $('#mitra').val(mitra_load).change();
    }

		$(".select2").select2({
			width: "100%",
		});

		$('.date-picker').daterangepicker(
      {
        singleDatePicker: true,
        timePicker: false,
        showDropdowns: true,
        locale:
        {
					format: 'YYYY-MM-DD',
        }
      }
		);

		$('#tb_rekon').DataTable({
			autoWidth: true,
			columnDefs: [
				{
					targets: 'no-sort',
					orderable: false
				}
			],
			lengthMenu: [
				[16, 32, 64, -1],
				[16, 32, 64, "All"]
			]
		});
	});
</script>
@endsection