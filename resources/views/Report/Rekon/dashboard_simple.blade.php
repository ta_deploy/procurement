@extends('layout')
@section('title', 'Dashboard - PROMISE')
@section('content')
@if (Session::has('alerts'))
  @foreach(Session::get('alerts') as $alert)
    <div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
  @endforeach
@endif
<div class="container-fluid">
  <div class="row justify-content-center">
    <div class="col-md-12">
      <form id="submit_dash" method="GET">
				{{ csrf_field() }}
        <div class="row">
          <div class="col-md-12">
            <div class="table-responsive">
							<table class="table isi_tbl table-bordered table-striped" style="width: 100%">
                @php
                  $user = $mitra = $proc_a = $proc_r = $finn = $cashbank = 0;
                @endphp
                <thead class="thead-dark">
                  <tr>
                    <th>Jenis Pekerjaan</th>
                    <th colspan="{{ $data[1] }}">User / Operations</th>
                    <th colspan="{{ $data[1] }}">Mitra</th>
                    <th colspan="{{ $data[1] }}">Procurement Area</th>
                    <th colspan="{{ $data[1] }}">Procurement Regional</th>
                    <th colspan="{{ $data[1] }}">Finance</th>
                    <th colspan="{{ $data[1] }}">Cash&Bank</th>
                  </tr>
                </thead>
                <tbody>
                  @php
                    $user = $mitra = $proc_a = $proc_r = $finn = $cashbank = $hrg_user = $hrg_mitra = $hrg_proc_a = $hrg_proc_r = $hrg_finn = $hrg_cashbank = 0;
                  @endphp
                  @forelse($data[0] as $K => $d)
                    @if($K != 'total')
                      <tr>
                        <td>{{ $K }}</td>
                        @if(in_array($tampilan, ['val_price_sp']) )
                          @php
                            $hrg_user += $d['data']['hrg_user'];
                            $hrg_mitra += $d['data']['hrg_mitra'];
                            $hrg_proc_a += $d['data']['hrg_proc_a'];
                            $hrg_proc_r += $d['data']['hrg_proc_r'];
                            $hrg_finn += $d['data']['hrg_finn'];
                            $hrg_cashbank += $d['data']['hrg_cashbank'];

                            $user += $d['data']['user_c'];
                            $mitra += $d['data']['mitra_c'];
                            $proc_a += $d['data']['proc_a_c'];
                            $proc_r += $d['data']['proc_r_c'];
                            $finn += $d['data']['finn_c'];
                            $cashbank += $d['data']['cashbank_c'];
                          @endphp
                          <td>
                            {!! $d['data']['user_c'] ? '<a class="badge badge-pill badge-info" style="color: white;">' . number_format($d['data']['user_c'], 0, '.', '.') .'</a>' : 0 !!}
                          </td>
                          <td>
                            {!! $d['data']['hrg_user'] ? '<a class="badge badge-pill badge-info" style="color: white;">' . number_format($d['data']['hrg_user'], 0, '.', '.') .'</a>' : 0 !!}
                          </td>
                          <td>
                            {{ $d['data']['user_per'] ?? 0 }}
                          </td>
                          <td>
                            {!! $d['data']['mitra_c'] ? '<a class="badge badge-pill badge-info" style="color: white;">' . number_format($d['data']['mitra_c'], 0, '.', '.') .'</a>' : 0 !!}
                          </td>
                          <td>
                            {!! $d['data']['hrg_mitra'] ? '<a class="badge badge-pill badge-info" style="color: white;">' . number_format($d['data']['hrg_mitra'], 0, '.', '.') .'</a>' : 0 !!}
                          </td>
                          <td>
                            {{ $d['data']['mitra_per'] ?? 0 }} </td>
                          <td>
                            {!! $d['data']['proc_a_c'] ? '<a/proc_a" class="badge badge-pill badge-info" style="color: white;">' . number_format($d['data']['proc_a_c'], 0, '.', '.') .'</a>' : 0 !!}
                          </td>
                          <td>
                            {!! $d['data']['hrg_proc_a'] ? '<a class="badge badge-pill badge-info" style="color: white;">' . number_format($d['data']['hrg_proc_a'], 0, '.', '.') .'</a>' : 0 !!}
                          </td>
                          <td>
                            {{ $d['data']['proc_a_per'] ?? 0 }}
                          </td>
                          <td>
                            {!! $d['data']['proc_r_c'] ? '<a class="badge badge-pill badge-info" style="color: white;">' . number_format($d['data']['proc_r_c'], 0, '.', '.') .'</a>' : 0 !!}
                          </td>
                          <td>
                            {!! $d['data']['hrg_proc_r'] ? '<a class="badge badge-pill badge-info" style="color: white;">' . number_format($d['data']['hrg_proc_r'], 0, '.', '.') .'</a>' : 0 !!}
                          </td>
                          <td>
                            {{ $d['data']['proc_r_per'] ?? 0 }}
                          </td>
                          <td>
                            {!! $d['data']['finn_c'] ? '<a class="badge badge-pill badge-info" style="color: white;">' . number_format($d['data']['finn_c'], 0, '.', '.') .'</a>' : 0 !!}
                          </td>
                          <td>
                            {!! $d['data']['hrg_finn'] ? '<a class="badge badge-pill badge-info" style="color: white;">' . number_format($d['data']['hrg_finn'], 0, '.', '.') .'</a>' : 0 !!}
                          </td>
                          <td>
                            {{ $d['data']['finn_per'] ?? 0 }}
                          </td>
                          <td>
                            {!! $d['data']['cashbank_c'] ? '<a class="badge badge-pill badge-info" style="color: white;">' . number_format($d['data']['cashbank_c'], 0, '.', '.') .'</a>' : 0 !!}
                          </td>
                          <td>
                            {!! $d['data']['hrg_cashbank'] ? '<a class="badge badge-pill badge-info" style="color: white;">' . number_format($d['data']['hrg_cashbank'], 0, '.', '.') .'</a>' : 0 !!}
                          </td>
                          <td>{{ $d['data']['cashbank_per'] ?? 0 }} </td>
                        @endif

                        @if(in_array($tampilan, ['price_sp']) || empty($tampilan) )
                          @php
                            $hrg_user += $d['data']['hrg_user'];
                            $hrg_mitra += $d['data']['hrg_mitra'];
                            $hrg_proc_a += $d['data']['hrg_proc_a'];
                            $hrg_proc_r += $d['data']['hrg_proc_r'];
                            $hrg_finn += $d['data']['hrg_finn'];
                            $hrg_cashbank += $d['data']['hrg_cashbank'];
                          @endphp
                          <td>
                            {!! $d['data']['hrg_user'] ? '<a class="badge url_m badge-pill badge-info" style="color: white;">' . number_format($d['data']['hrg_user'], 0, '.', '.') .'</a>' : 0 !!}
                          </td>
                          <td>
                            {{ $d['data']['user_per'] ?? 0 }}
                          </td>
                          <td>
                            {!! $d['data']['hrg_mitra'] ? '<a class="badge badge-pill badge-info" style="color: white;">' . number_format($d['data']['hrg_mitra'], 0, '.', '.') .'</a>' : 0 !!}
                          </td>
                          <td>
                            {{ $d['data']['mitra_per'] ?? 0 }}
                          </td>
                          <td>
                            {!! $d['data']['hrg_proc_a'] ? '<a class="badge badge-pill badge-info" style="color: white;">' . number_format($d['data']['hrg_proc_a'], 0, '.', '.') .'</a>' : 0 !!}
                          </td>
                          <td>
                            {{ $d['data']['proc_a_per'] ?? 0 }}
                          </td>
                          <td>
                            {!! $d['data']['hrg_proc_r'] ? '<a class="badge badge-pill badge-info" style="color: white;">' . number_format($d['data']['hrg_proc_r'], 0, '.', '.') .'</a>' : 0 !!}
                          </td>
                          <td>
                            {{ $d['data']['proc_r_per'] ?? 0 }}
                          </td>
                          <td>
                            {!! $d['data']['hrg_finn'] ? '<a class="badge badge-pill badge-info" style="color: white;">' . number_format($d['data']['hrg_finn'], 0, '.', '.') .'</a>' : 0 !!}
                          </td>
                          <td>
                            {{ $d['data']['finn_per'] ?? 0 }}
                          </td>
                          <td>
                            {!! $d['data']['hrg_cashbank'] ? '<a class="badge badge-pill badge-info" style="color: white;">' . number_format($d['data']['hrg_cashbank'], 0, '.', '.') .'</a>' : 0 !!}
                          </td>
                          <td>
                            {{ $d['data']['cashbank_per'] ?? 0 }}
                          </td>
                        @endif

                        @if($tampilan == 'val_sp')
                          @php
                            $user += $d['data']['user_c'];
                            $mitra += $d['data']['mitra_c'];
                            $proc_a += $d['data']['proc_a_c'];
                            $proc_r += $d['data']['proc_r_c'];
                            $finn += $d['data']['finn_c'];
                            $cashbank += $d['data']['cashbank_c'];
                          @endphp
                          <td>
                            {!! $d['data']['user_c'] ? '<a class="badge badge-pill badge-info" style="color: white;">'. number_format($d['data']['user_c'], 0, '.', '.') .'</a>' : 0 !!}
                          </td>
                          <td>
                            {{ $d['data']['user_per'] ?? 0 }}
                          </td>
                          <td>
                            {!! $d['data']['mitra_c'] ? '<a class="badge badge-pill badge-info" style="color: white;">'. number_format($d['data']['mitra_c'], 0, '.', '.') .'</a>' : 0 !!}
                          </td>
                          <td>
                            {{ $d['data']['mitra_per'] ?? 0 }}
                          </td>
                          <td>
                            {!! $d['data']['proc_a_c'] ? '<a class="badge badge-pill badge-info" style="color: white;">'. number_format($d['data']['proc_a_c'], 0, '.', '.') .'</a>' : 0 !!}
                          </td>
                          <td>
                            {{ $d['data']['proc_a_per'] ?? 0 }}
                          </td>
                          <td>
                            {!! $d['data']['proc_r_c'] ? '<a class="badge badge-pill badge-info" style="color: white;">'. number_format($d['data']['proc_r_c'], 0, '.', '.') .'</a>' : 0 !!}
                          </td>
                          <td>
                            {{ $d['data']['proc_r_per'] ?? 0 }}
                          </td>
                          <td>
                            {!! $d['data']['finn_c'] ? '<a class="badge badge-pill badge-info" style="color: white;">'. number_format($d['data']['finn_c'], 0, '.', '.') .'</a>' : 0 !!}
                          </td>
                          <td>
                            {{ $d['data']['finn_per'] ?? 0 }}
                          </td>
                          <td>
                            {!! $d['data']['cashbank_c'] ? '<a class="badge badge-pill badge-info" style="color: white;">'. number_format($d['data']['cashbank_c'], 0, '.', '.') .'</a>' : 0 !!}
                          </td>
                          <td>
                            {{ $d['data']['cashbank_per'] ?? 0 }}
                          </td>
                        @endif

                        @if(in_array($tampilan, ['price_rekon']) )
                          @php
                            $hrg_user += $d['data']['hrg_user'];
                            $hrg_mitra += $d['data']['hrg_mitra'];
                            $hrg_proc_a += $d['data']['hrg_proc_a'];
                            $hrg_proc_r += $d['data']['hrg_proc_r'];
                            $hrg_finn += $d['data']['hrg_finn'];
                            $hrg_cashbank += $d['data']['hrg_cashbank'];
                          @endphp
                          <td>
                            {!! $d['data']['hrg_user'] ? '<a class="badge badge-pill badge-info" style="color: white;">' . number_format($d['data']['hrg_user'], 0, '.', '.') .'</a>' : 0 !!}
                          </td>
                          <td>
                            {{ $d['data']['user_per'] ?? 0 }}
                          </td>
                          <td>
                            {!! $d['data']['hrg_mitra'] ? '<a class="badge badge-pill badge-info" style="color: white;">' . number_format($d['data']['hrg_mitra'], 0, '.', '.') .'</a>' : 0 !!}
                          </td>
                          <td>
                            {{ $d['data']['mitra_per'] ?? 0 }}
                          </td>
                          <td>
                            {!! $d['data']['hrg_proc_a'] ? '<a class="badge badge-pill badge-info" style="color: white;">' . number_format($d['data']['hrg_proc_a'], 0, '.', '.') .'</a>' : 0 !!}
                          </td>
                          <td>
                            {{ $d['data']['proc_a_per'] ?? 0 }}
                          </td>
                          <td>
                            {!! $d['data']['hrg_proc_r'] ? '<a class="badge badge-pill badge-info" style="color: white;">' . number_format($d['data']['hrg_proc_r'], 0, '.', '.') .'</a>' : 0 !!}
                          </td>
                          <td>
                            {{ $d['data']['proc_r_per'] ?? 0 }}
                          </td>
                          <td>
                            {!! $d['data']['hrg_finn'] ? '<a class="badge badge-pill badge-info" style="color: white;">' . number_format($d['data']['hrg_finn'], 0, '.', '.') .'</a>' : 0 !!}
                          </td>
                          <td>
                            {{ $d['data']['finn_per'] ?? 0 }}
                          </td>
                          <td>
                            {!! $d['data']['hrg_cashbank'] ? '<a class="badge badge-pill badge-info" style="color: white;">' . number_format($d['data']['hrg_cashbank'], 0, '.', '.') .'</a>' : 0 !!}
                          </td>
                          <td>
                            {{ $d['data']['cashbank_per'] ?? 0 }}
                          </td>
                        @endif

                        @if(in_array($tampilan, ['val_sp_price_rekon']) )
                          @php
                            $hrg_user += $d['data']['hrg_user'];
                            $hrg_mitra += $d['data']['hrg_mitra'];
                            $hrg_proc_a += $d['data']['hrg_proc_a'];
                            $hrg_proc_r += $d['data']['hrg_proc_r'];
                            $hrg_finn += $d['data']['hrg_finn'];
                            $hrg_cashbank += $d['data']['hrg_cashbank'];

                            $user += $d['data']['user_c'];
                            $mitra += $d['data']['mitra_c'];
                            $proc_a += $d['data']['proc_a_c'];
                            $proc_r += $d['data']['proc_r_c'];
                            $finn += $d['data']['finn_c'];
                            $cashbank += $d['data']['cashbank_c'];
                          @endphp
                          <td>
                            {!! $d['data']['user_c'] ? '<a class="badge badge-pill badge-info" style="color: white;">' . number_format($d['data']['user_c'], 0, '.', '.') .'</a>' : 0 !!}
                          </td>
                          <td>
                            {!! $d['data']['hrg_user'] ? '<a class="badge badge-pill badge-info" style="color: white;">' . number_format($d['data']['hrg_user'], 0, '.', '.') .'</a>' : 0 !!}
                          </td>
                          <td>
                            {{ $d['data']['user_per'] ?? 0 }}
                          </td>
                          <td>
                            {!! $d['data']['mitra_c'] ? '<a class="badge badge-pill badge-info" style="color: white;">' . number_format($d['data']['mitra_c'], 0, '.', '.') .'</a>' : 0 !!}
                          </td>
                          <td>
                            {!! $d['data']['hrg_mitra'] ? '<a class="badge badge-pill badge-info" style="color: white;">' . number_format($d['data']['hrg_mitra'], 0, '.', '.') .'</a>' : 0 !!}
                          </td>
                          <td>
                            {{ $d['data']['mitra_per'] ?? 0 }}
                          </td>
                          <td>
                            {!! $d['data']['proc_a_c'] ? '<a class="badge badge-pill badge-info" style="color: white;">' . number_format($d['data']['proc_a_c'], 0, '.', '.') .'</a>' : 0 !!}
                          </td>
                          <td>
                            {!! $d['data']['hrg_proc_a'] ? '<a class="badge badge-pill badge-info" style="color: white;">' . number_format($d['data']['hrg_proc_a'], 0, '.', '.') .'</a>' : 0 !!}
                          </td>
                          <td>
                            {{ $d['data']['proc_a_per'] ?? 0 }}
                          </td>
                          <td>
                            {!! $d['data']['proc_r_c'] ? '<a class="badge badge-pill badge-info" style="color: white;">' . number_format($d['data']['proc_r_c'], 0, '.', '.') .'</a>' : 0 !!}
                          </td>
                          <td>
                            {!! $d['data']['hrg_proc_r'] ? '<a class="badge badge-pill badge-info" style="color: white;">' . number_format($d['data']['hrg_proc_r'], 0, '.', '.') .'</a>' : 0 !!}
                          </td>
                          <td>
                            {{ $d['data']['proc_r_per'] ?? 0 }}
                          </td>
                          <td>
                            {!! $d['data']['finn_c'] ? '<a class="badge badge-pill badge-info" style="color: white;">' . number_format($d['data']['finn_c'], 0, '.', '.') .'</a>' : 0 !!}
                          </td>
                          <td>
                            {!! $d['data']['hrg_finn'] ? '<a class="badge badge-pill badge-info" style="color: white;">' . number_format($d['data']['hrg_finn'], 0, '.', '.') .'</a>' : 0 !!}
                          </td>
                          <td>
                            {{ $d['data']['finn_per'] ?? 0 }}
                          </td>
                          <td>
                            {!! $d['data']['cashbank_c'] ? '<a class="badge badge-pill badge-info" style="color: white;">' . number_format($d['data']['cashbank_c'], 0, '.', '.') .'</a>' : 0 !!}
                          </td>
                          <td>
                            {!! $d['data']['hrg_cashbank'] ? '<a class="badge badge-pill badge-info" style="color: white;">' . number_format($d['data']['hrg_cashbank'], 0, '.', '.') .'</a>' : 0 !!}
                          </td>
                          <td>
                            {{ $d['data']['cashbank_per'] ?? 0 }}
                          </td>
                        @endif
                      </tr>
                    @endif
                  @empty
                  @endforelse
                </tbody>
                @if(count($data[0]) != 0 )
                  <tfoot>
                    <tr>
                      <td>
                        Grand Total
                      </td>
                      @php
                        $sum_satu = array_sum([$hrg_user, $hrg_mitra, $hrg_proc_a, $hrg_proc_r, $hrg_finn, $hrg_cashbank]);
                      @endphp
                      @if(in_array($tampilan, ['val_price_sp']) )
                        <td>{!! $user ? '<a class="badge badge-pill badge-info" style="color: white"> '. number_format($user, 0, '.', '.') : 0 !!} </td>
                        <td>{!! $hrg_user ? '<a class="badge badge-pill badge-info" style="color: white"> '. number_format($hrg_user, 0, '.', '.') : 0 !!} </td>
                        <td>{{ ROUND( ($sum_satu == 0 ? 0 : $hrg_user / $sum_satu ) * 100, 1) }}%</td>
                        <td>{!! $mitra ? '<a class="badge badge-pill badge-info" style="color: white"> '. number_format($mitra, 0, '.', '.') : 0 !!} </td>
                        <td>{!! $hrg_mitra ? '<a class="badge badge-pill badge-info" style="color: white"> '. number_format($hrg_mitra, 0, '.', '.') : 0 !!} </td>
                        <td>{{ ROUND( ($sum_satu == 0 ? 0 : $hrg_mitra / $sum_satu ) * 100, 1) }}% </td>
                        <td>{!! $proc_a ? '<a class="badge badge-pill badge-info" style="color: white"> '. number_format($proc_a, 0, '.', '.') : 0 !!} </td>
                        <td>{!! $hrg_proc_a ? '<a class="badge badge-pill badge-info" style="color: white"> '. number_format($hrg_proc_a, 0, '.', '.') : 0 !!} </td>
                        <td>{{ ROUND( ($sum_satu == 0 ? 0 : $hrg_proc_a / $sum_satu ) * 100, 1) }}% </td>
                        <td>{!! $proc_r ? '<a class="badge badge-pill badge-info" style="color: white"> '. number_format($proc_r, 0, '.', '.') : 0 !!} </td>
                        <td>{!! $hrg_proc_r ? '<a class="badge badge-pill badge-info" style="color: white"> '. number_format($hrg_proc_r, 0, '.', '.') : 0 !!} </td>
                        <td>{{ ROUND( ($sum_satu == 0 ? 0 : $hrg_proc_r / $sum_satu ) * 100, 1) }}% </td>
                        <td>{!! $finn ? '<a class="badge badge-pill badge-info" style="color: white"> '. number_format($finn, 0, '.', '.') : 0 !!} </td>
                        <td>{!! $hrg_finn ? '<a class="badge badge-pill badge-info" style="color: white"> '. number_format($hrg_finn, 0, '.', '.') : 0 !!} </td>
                        <td>{{ ROUND( ($sum_satu == 0 ? 0 : $hrg_finn / $sum_satu ) * 100, 1) }}% </td>
                        <td>{!! $cashbank ? '<a class="badge badge-pill badge-info" style="color: white"> '. number_format($cashbank, 0, '.', '.') : 0 !!} </td>
                        <td>{!! $hrg_cashbank ? '<a class="badge badge-pill badge-info" style="color: white"> '. number_format($hrg_cashbank, 0, '.', '.') : 0 !!} </td>
                        <td>{{ ROUND( ($sum_satu == 0 ? 0 : $hrg_cashbank / $sum_satu ) * 100, 1) }}% </td>
                      @endif

                      @if(in_array($tampilan, ['price_sp']) || empty($tampilan) )
                        <td>{!! $hrg_user ? '<a class="badge badge-pill badge-info" style="color: white"> '. number_format($hrg_user, 0, '.', '.') : 0 !!} </td>
                        <td>{{ ROUND( ($sum_satu == 0 ? 0 : $hrg_user / $sum_satu ) * 100, 1) }}%</td>
                        <td>{!! $hrg_mitra ? '<a class="badge badge-pill badge-info" style="color: white"> '. number_format($hrg_mitra, 0, '.', '.') : 0 !!} </td>
                        <td>{{ ROUND( ($sum_satu == 0 ? 0 : $hrg_mitra / $sum_satu ) * 100, 1) }}% </td>
                        <td>{!! $hrg_proc_a ? '<a class="badge badge-pill badge-info" style="color: white"> '. number_format($hrg_proc_a, 0, '.', '.') : 0 !!} </td>
                        <td>{{ ROUND( ($sum_satu == 0 ? 0 : $hrg_proc_a / $sum_satu ) * 100, 1) }}% </td>
                        <td>{!! $hrg_proc_r ? '<a class="badge badge-pill badge-info" style="color: white"> '. number_format($hrg_proc_r, 0, '.', '.') : 0 !!} </td>
                        <td>{{ ROUND( ($sum_satu == 0 ? 0 : $hrg_proc_r / $sum_satu ) * 100, 1) }}% </td>
                        <td>{!! $hrg_finn ? '<a class="badge badge-pill badge-info" style="color: white"> '. number_format($hrg_finn, 0, '.', '.') : 0 !!} </td>
                        <td>{{ ROUND( ($sum_satu == 0 ? 0 : $hrg_finn / $sum_satu ) * 100, 1) }}% </td>
                        <td>{!! $hrg_cashbank ? '<a class="badge badge-pill badge-info" style="color: white"> '. number_format($hrg_cashbank, 0, '.', '.') : 0 !!} </td>
                        <td>{{ ROUND( ($sum_satu == 0 ? 0 : $hrg_cashbank / $sum_satu ) * 100, 1) }}% </td>
                      @endif

                      @if($tampilan == 'val_sp')
                        @php
                          $sum_hrg_tot = array_sum([$user, $mitra, $proc_a, $proc_r, $finn, $cashbank]);
                        @endphp
                        <td>{!! $user ? '<a class="badge badge-pill badge-info" style="color: white;">' . $user . '</a>' : 0 !!} </td>
                        <td>{{ ROUND( ( $sum_hrg_tot == 0 ? 0 : $user / $sum_hrg_tot ) * 100, 1) }}%</td>
                        <td>{!! $mitra ? '<a class="badge badge-pill badge-info" style="color: white;">' . $mitra . '</a>' : 0 !!} </td>
                        <td>{{ ROUND( ( $sum_hrg_tot == 0 ? 0 : $mitra / $sum_hrg_tot ) * 100, 1) }}% </td>
                        <td>{!! $proc_a ? '<a class="badge badge-pill badge-info" style="color: white;">' . $proc_a . '</a>' : 0 !!} </td>
                        <td>{{ ROUND( ( $sum_hrg_tot == 0 ? 0 : $proc_a / $sum_hrg_tot ) * 100, 1) }}% </td>
                        <td>{!! $proc_r ? '<a class="badge badge-pill badge-info" style="color: white;">' . $proc_r . '</a>' : 0 !!} </td>
                        <td>{{ ROUND( ( $sum_hrg_tot == 0 ? 0 : $proc_r / $sum_hrg_tot ) * 100, 1) }}% </td>
                        <td>{!! $finn ? '<a class="badge badge-pill badge-info" style="color: white;">' . $finn . '</a>' : 0 !!} </td>
                        <td>{{ ROUND( ( $sum_hrg_tot == 0 ? 0 : $finn / $sum_hrg_tot ) * 100, 1) }}% </td>
                        <td>{!! $cashbank ? '<a class="badge badge-pill badge-info" style="color: white;">' . $cashbank . '</a>' : 0 !!} </td>
                        <td>{{ ROUND( ( $sum_hrg_tot == 0 ? 0 : $cashbank / $sum_hrg_tot ) * 100, 1) }}% </td>
                      @endif

                      @if(in_array($tampilan, ['price_rekon']) )
                        <td>{!! $hrg_user ? '<a class="badge badge-pill badge-info" style="color: white"> '. number_format($hrg_user, 0, '.', '.') : 0 !!} </td>
                        <td>{{ ROUND( ($sum_satu == 0 ? 0 : $hrg_user / $sum_satu ) * 100, 1) }}%</td>
                        <td>{!! $hrg_mitra ? '<a class="badge badge-pill badge-info" style="color: white"> '. number_format($hrg_mitra, 0, '.', '.') : 0 !!} </td>
                        <td>{{ ROUND( ($sum_satu == 0 ? 0 : $hrg_mitra / $sum_satu ) * 100, 1) }}% </td>
                        <td>{!! $hrg_proc_a ? '<a class="badge badge-pill badge-info" style="color: white"> '. number_format($hrg_proc_a, 0, '.', '.') : 0 !!} </td>
                        <td>{{ ROUND( ($sum_satu == 0 ? 0 : $hrg_proc_a / $sum_satu ) * 100, 1) }}% </td>
                        <td>{!! $hrg_proc_r ? '<a class="badge badge-pill badge-info" style="color: white"> '. number_format($hrg_proc_r, 0, '.', '.') : 0 !!} </td>
                        <td>{{ ROUND( ($sum_satu == 0 ? 0 : $hrg_proc_r / $sum_satu ) * 100, 1) }}% </td>
                        <td>{!! $hrg_finn ? '<a class="badge badge-pill badge-info" style="color: white"> '. number_format($hrg_finn, 0, '.', '.') : 0 !!} </td>
                        <td>{{ ROUND( ($sum_satu == 0 ? 0 : $hrg_finn / $sum_satu ) * 100, 1) }}% </td>
                        <td>{!! $hrg_cashbank ? '<a class="badge badge-pill badge-info" style="color: white"> '. number_format($hrg_cashbank, 0, '.', '.') : 0 !!} </td>
                        <td>{{ ROUND( ($sum_satu == 0 ? 0 : $hrg_cashbank / $sum_satu ) * 100, 1) }}% </td>
                      @endif

                      @if(in_array($tampilan, ['val_sp_price_rekon']) )
                        <td>{!! $user ? '<a class="badge badge-pill badge-info" style="color: white"> '. number_format($user, 0, '.', '.') : 0 !!} </td>
                        <td>{!! $hrg_user ? '<a class="badge badge-pill badge-info" style="color: white"> '. number_format($hrg_user, 0, '.', '.') : 0 !!} </td>
                        <td>{{ ROUND( ($sum_satu == 0 ? 0 : $hrg_user / $sum_satu ) * 100, 1) }}%</td>
                        <td>{!! $mitra ? '<a class="badge badge-pill badge-info" style="color: white"> '. number_format($mitra, 0, '.', '.') : 0 !!} </td>
                        <td>{!! $hrg_mitra ? '<a class="badge badge-pill badge-info" style="color: white"> '. number_format($hrg_mitra, 0, '.', '.') : 0 !!} </td>
                        <td>{{ ROUND( ($sum_satu == 0 ? 0 : $hrg_mitra / $sum_satu ) * 100, 1) }}% </td>
                        <td>{!! $proc_a ? '<a class="badge badge-pill badge-info" style="color: white"> '. number_format($proc_a, 0, '.', '.') : 0 !!} </td>
                        <td>{!! $hrg_proc_a ? '<a class="badge badge-pill badge-info" style="color: white"> '. number_format($hrg_proc_a, 0, '.', '.') : 0 !!} </td>
                        <td>{{ ROUND( ($sum_satu == 0 ? 0 : $hrg_proc_a / $sum_satu ) * 100, 1) }}% </td>
                        <td>{!! $proc_r ? '<a class="badge badge-pill badge-info" style="color: white"> '. number_format($proc_r, 0, '.', '.') : 0 !!} </td>
                        <td>{!! $hrg_proc_r ? '<a class="badge badge-pill badge-info" style="color: white"> '. number_format($hrg_proc_r, 0, '.', '.') : 0 !!} </td>
                        <td>{{ ROUND( ($sum_satu == 0 ? 0 : $hrg_proc_r / $sum_satu ) * 100, 1) }}% </td>
                        <td>{!! $finn ? '<a class="badge badge-pill badge-info" style="color: white"> '. number_format($finn, 0, '.', '.') : 0 !!} </td>
                        <td>{!! $hrg_finn ? '<a class="badge badge-pill badge-info" style="color: white"> '. number_format($hrg_finn, 0, '.', '.') : 0 !!} </td>
                        <td>{{ ROUND( ($sum_satu == 0 ? 0 : $hrg_finn / $sum_satu ) * 100, 1) }}% </td>
                        <td>{!! $cashbank ? '<a class="badge badge-pill badge-info" style="color: white"> '. number_format($cashbank, 0, '.', '.') : 0 !!} </td>
                        <td>{!! $hrg_cashbank ? '<a class="badge badge-pill badge-info" style="color: white"> '. number_format($hrg_cashbank, 0, '.', '.') : 0 !!} </td>
                        <td>{{ ROUND( ($sum_satu == 0 ? 0 : $hrg_cashbank / $sum_satu ) * 100, 1) }}% </td>
                      @endif
                    </tr>
                  </tfoot>
                @else
                @endif
							</table>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection