@extends('layout')
@section('title', 'Dokumen Mitra')
@section('headerS')
<style>
	.centered {
		text-align: center;
		vertical-align: middle;
	}
</style>
@endsection
@section('content')
@if (Session::has('alerts'))
  @foreach(Session::get('alerts') as $alert)
    <div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
  @endforeach
@endif
<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-12">
			<div class="row">
				<div class="col-md-12 my-4">
				<div class="mb-2 align-items-center">
					<div class="card shadow mb-4">
						<div class="card-body">
							<h5 class="card-title">Tabel Rekon</h5>
							<p class="card-text">Tabel Rekon yang pernah di submit</p>
							<table id ="tb_rekon" class="table table-striped table-bordered table-hover">
								<thead class="thead-dark">
									<tr>
										<th class="centered">Mitra</th>
										<th class="centered">Tanggal Buat</th>
										<th class="centered">Dokumen</th>
										<th class="centered">Keterangan Terakhir</th>
										<th class="centered">Progress</th>
										<th class="centered">Action</th>
									</tr>
								</thead>
								<tbody>
									@php
										$num = 1;
									@endphp
									@foreach($final_res as $k => $d)
										@php ($first = true) @endphp
										@foreach($d as $data)
											<tr>
											@if($first == true)
												<td rowspan="{{ count($d) }}"> {{ $k }} </td>
												@php
													($first = false)
												@endphp
											@endif
												<td>{{ $data->created_at }}</td>
												<td>{{ $data->judul_work }}</td>
												<td>{{ $data->fil_desc }}</td>
												<td>
													<div class="progress progress-sm" style="height: 20px;">
														@php
															$count = (ROUND((1.4 * $data->fil_log), 2) + 0.2) * 10;
														@endphp
														<div class="progress-bar bg-success" role="progressbar" style="width: {{ $count }}%;" aria-valuemin="0" aria-valuemax="100">{{ $count }}%</div>
													</div>
												</td>
												<td><a target="_blank" href="/timeline/{{ $data->id_boq_up }}" class="badge badge-pill badge-info">Lihat</a></td>
											</tr>
										@endforeach
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('footerS')
<script type="text/javascript">
	$(function(){

	});
</script>
@endsection