@extends('layout')
@section('title', 'Dashboard - PROMISE')
@section('content')
@if (Session::has('alerts'))
  @foreach(Session::get('alerts') as $alert)
    <div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
  @endforeach
@endif
<div class="container-fluid">
  <div class="row justify-content-center">
    <div class="col-md-12">
      <form id="submit_dash" method="GET">
				{{ csrf_field() }}
        <div class="row">
          <div class="col-md-12">
            <div class="table">
							<table class="table isi_tbl table-bordered table-striped" style="width: 100%">
								<thead class="thead-dark">
									<tr role="row">
										<th rowspan="2">NAMA MITRA</th>
										<th colspan="7">PRA KERJA</th>
										<th colspan="10">PASCA KERJA</th>
										<th rowspan="2">TOTAL MITRA</th>
									</tr>
									<tr>
										<th>REQUEST PID <br/><b>(Operation)</b></th>
										<th>INPUT PID <br/><b>(Commerce)</b></th>
										<th>JUSTIFIKASI <br/><b>(Operation)</b></th>
										<th>SURAT PENETAPAN <br/><b>(Proc. Area)</b></th>
										<th>KESANGGUPAN <br/><b>(Mitra)</b></th>
										<th>SURAT PESANAN <br/><b>(Proc. Area)</b></th>
										<th>TOTAL PRA KERJA</th>
										{{-- rekon --}}
										<th>INPUT BOQ REKON <br/><b>(Mitra)</b></th>
										<th>VERIFIKASI BOQ REKON <br/><b>(Operation)</b></th>
										<th>PELURUSAN RFC <br/><b>(Mitra)</b></th>
										<th>BUDGETING <br/><b>(Commerce)</b></th>
										<th>INPUT NOMOR BA <br/><b>(Proc. Area)</b></th>
										<th>INPUT KELENGKAPAN NOMOR <br/><b>(Mitra)</b></th>
										<th>DOKUMEN TTD <br/><b>(Proc. Area & Mitra)</b></th>
										<th>VERIFIKASI DOKUMEN REKON REGIONAL <br/><b>(Proc. Regional)</b></th>
										<th>VERIFIKASI FINNANCE <br/><b>(Finance)</b></th>
										<th>TOTAL PASCA KERJA</th>
									</tr>
								</thead>
								<tbody>
									@php
										$operation_req_pid = $commerce_create_pid = $operation_upload_justif = $proca_s_pen = $mitra_sanggup = $proca_sp = $total_pra_kerja = $mitra_boq_rek = $operation_verif_boq = $mitra_pelurusan_material = $commerce_budget = $mitra_lengkapi_nomor = $proca_ver_doc_area = $mitra_ttd = $procg_ver_doc_reg = $finn_ver = $total_pasca_kerja = $grand_total = 0;
									@endphp
									@forelse ($data as $v)
										@php
											$operation_req_pid += $v->operation_req_pid;
											$commerce_create_pid += $v->commerce_create_pid;
											$operation_upload_justif += $v->operation_upload_justif;
											$proca_s_pen += $v->proca_s_pen;
											$mitra_sanggup += $v->mitra_sanggup;
											$proca_sp += $v->proca_sp;
											$total_pra_kerja += $v->total_pra_kerja;
											$mitra_boq_rek += $v->mitra_boq_rek;
											$operation_verif_boq += $v->operation_verif_boq;
											$mitra_pelurusan_material += $v->mitra_pelurusan_material;
											$commerce_budget += $v->commerce_budget;
											$mitra_lengkapi_nomor += $v->mitra_lengkapi_nomor;
											$proca_ver_doc_area += $v->proca_ver_doc_area;
											$mitra_ttd += $v->mitra_ttd;
											$procg_ver_doc_reg += $v->procg_ver_doc_reg;
											$finn_ver += $v->finance;
											$total_pasca_kerja += $v->total_pasca_kerja;
											$grand_total += $v->total;
										@endphp
										<tr>
											<td>{{ $v->mitra_nm }}</td>
											<td style="background-color: #ebff00;">{!! $v->operation_req_pid != 0 ? "<a class='url_m' href='/detail_per/$v->mitra_nm/operation_req_pid'>$v->operation_req_pid</a>" : '-' !!}</td>
											<td style="background-color: #ebff00;">{!! $v->commerce_create_pid != 0 ? "<a class='url_m' href='/detail_per/$v->mitra_nm/commerce_create_pid'>$v->commerce_create_pid</a>" : '-' !!}</td>
											<td style="background-color: #ebff00;">{!! $v->operation_upload_justif != 0 ? "<a class='url_m' href='/detail_per/$v->mitra_nm/operation_upload_justif'>$v->operation_upload_justif</a>" : '-' !!}</td>
											<td style="background-color: #ebff00;">{!! $v->proca_s_pen != 0 ? "<a class='url_m' href='/detail_per/$v->mitra_nm/proca_s_pen'>$v->proca_s_pen</a>" : '-' !!}</td>
											<td style="background-color: #ebff00;">{!! $v->mitra_sanggup != 0 ? "<a class='url_m' href='/detail_per/$v->mitra_nm/mitra_sanggup'>$v->mitra_sanggup</a>" : '-' !!}</td>
											<td style="background-color: #ebff00;">{!! $v->proca_sp != 0 ? "<a class='url_m' href='/detail_per/$v->mitra_nm/proca_sp'>$v->proca_sp</a>" : '-' !!}</td>
											<td style="background-color: #ebff00;">{!! $v->total_pra_kerja != 0 ? "<a class='url_m' href='/detail_per/$v->mitra_nm/total_pra_kerja'>$v->total_pra_kerja</a>" : '-' !!}</td>
											<td style="background-color: #42ff00;">{!! $v->mitra_boq_rek != 0 ? "<a class='url_m' href='/detail_per/$v->mitra_nm/mitra_boq_rek'>$v->mitra_boq_rek</a>" : '-' !!}</td>
											<td style="background-color: #42ff00;">{!! $v->operation_verif_boq != 0 ? "<a class='url_m' href='/detail_per/$v->mitra_nm/operation_verif_boq'>$v->operation_verif_boq</a>" : '-' !!}</td>
											<td style="background-color: #42ff00;">{!! $v->mitra_pelurusan_material != 0 ? "<a class='url_m' href='/detail_per/$v->mitra_nm/mitra_pelurusan_material'>$v->mitra_pelurusan_material</a>" : '-' !!}</td>
											<td style="background-color: #42ff00;">{!! $v->commerce_budget != 0 ? "<a class='url_m' href='/detail_per/$v->mitra_nm/commerce_budget'>$v->commerce_budget</a>" : '-' !!}</td>
											<td style="background-color: #42ff00;">{!! $v->mitra_lengkapi_nomor != 0 ? "<a class='url_m' href='/detail_per/$v->mitra_nm/mitra_lengkapi_nomor'>$v->mitra_lengkapi_nomor</a>" : '-' !!}</td>
											<td style="background-color: #42ff00;">{!! $v->proca_ver_doc_area != 0 ? "<a class='url_m' href='/detail_per/$v->mitra_nm/proca_ver_doc_area'>$v->proca_ver_doc_area</a>" : '-' !!}</td>
											<td style="background-color: #42ff00;">{!! $v->mitra_ttd != 0 ? "<a class='url_m' href='/detail_per/$v->mitra_nm/mitra_ttd'>$v->mitra_ttd</a>" : '-' !!}</td>
											<td style="background-color: #42ff00;">{!! $v->procg_ver_doc_reg != 0 ? "<a class='url_m' href='/detail_per/$v->mitra_nm/procg_ver_doc_reg'>$v->procg_ver_doc_reg</a>" : '-' !!}</td>
      								<td style="background-color: #42ff00;">{!! $v->finance != 0 ? "<a class='url_m' href='/detail_per/$v->mitra_nm /finance'>$v->finance</a>" : '-' !!}</td>
											<td style="background-color: #42ff00;">{!! $v->total_pasca_kerja != 0 ? "<a class='url_m' href='/detail_per/$v->mitra_nm/total_pasca_kerja'>$v->total_pasca_kerja</a>" : '-' !!}</td>
											<td>{!! $v->total != 0 ? "<a class='url_m' href='/detail_per/$v->mitra_nm/all'>$v->total</a>" : '-' !!}</td>
										</tr>
									@empty
									@endforelse
								</tbody>
								<tfoot>
									<tr>
										<td>Total</td>
										<td style="background-color: #ebff00;">{!! $operation_req_pid != 0 ? "<a href='/detail_per/all/operation_req_pid'>$operation_req_pid</a>" : '-' !!}</td>
										<td style="background-color: #ebff00;">{!! $commerce_create_pid != 0 ? "<a href='/detail_per/all/commerce_create_pid'>$commerce_create_pid</a>" : '-' !!}</td>
										<td style="background-color: #ebff00;">{!! $operation_upload_justif != 0 ? "<a href='/detail_per/all/operation_upload_justif'>$operation_upload_justif</a>" : '-' !!}</td>
										<td style="background-color: #ebff00;">{!! $proca_s_pen != 0 ? "<a href='/detail_per/all/proca_s_pen'>$proca_s_pen</a>" : '-' !!}</td>
										<td style="background-color: #ebff00;">{!! $mitra_sanggup != 0 ? "<a href='/detail_per/all/mitra_sanggup'>$mitra_sanggup</a>" : '-' !!}</td>
										<td style="background-color: #ebff00;">{!! $proca_sp != 0 ? "<a href='/detail_per/all/proca_sp'>$proca_sp</a>" : '-' !!}</td>
										<td style="background-color: #ebff00;">{!! $total_pra_kerja != 0 ? "<a href='/detail_per/all/total_pra_kerja'>$total_pra_kerja</a>" : '-' !!}</td>
										<td style="background-color: #42ff00;">{!! $mitra_boq_rek != 0 ? "<a href='/detail_per/all/mitra_boq_rek'>$mitra_boq_rek</a>" : '-' !!}</td>
										<td style="background-color: #42ff00;">{!! $operation_verif_boq != 0 ? "<a href='/detail_per/all/operation_verif_boq'>$operation_verif_boq</a>" : '-' !!}</td>
										<td style="background-color: #42ff00;">{!! $mitra_pelurusan_material != 0 ? "<a href='/detail_per/all/mitra_pelurusan_material'>$mitra_pelurusan_material</a>" : '-' !!}</td>
										<td style="background-color: #42ff00;">{!! $commerce_budget != 0 ? "<a href='/detail_per/all/commerce_budget'>$commerce_budget</a>" : '-' !!}</td>
										<td style="background-color: #42ff00;">{!! $mitra_lengkapi_nomor != 0 ? "<a href='/detail_per/all/mitra_lengkapi_nomor'>$mitra_lengkapi_nomor</a>" : '-' !!}</td>
										<td style="background-color: #42ff00;">{!! $proca_ver_doc_area != 0 ? "<a href='/detail_per/all/proca_ver_doc_area'>$proca_ver_doc_area</a>" : '-' !!}</td>
										<td style="background-color: #42ff00;">{!! $mitra_ttd != 0 ? "<a href='/detail_per/all/mitra_ttd'>$mitra_ttd</a>" : '-' !!}</td>
										<td style="background-color: #42ff00;">{!! $procg_ver_doc_reg != 0 ? "<a href='/detail_per/all/procg_ver_doc_reg'>$procg_ver_doc_reg</a>" : '-' !!}</td>
										<td style="background-color: #42ff00;">{!! $finn_ver != 0 ? "<a href='/detail_per/all/finn_ver'>$finn_ver</a>" : '-' !!}</td>
										<td style="background-color: #42ff00;">{!! $total_pasca_kerja != 0 ? "<a href='/detail_per/all/total_pasca_kerja'>$total_pasca_kerja</a>" : '-' !!}</td>
										<td>{!! $grand_total != 0 ? "<a href='/detail_per/all/all'>$grand_total</a>" : '-' !!}</td>
									</tr>
								</tfoot>
							</table>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection