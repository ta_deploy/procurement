@extends('layout')
@section('title', 'Dashboard - PROMISE')
@section('headerS')
<style>
	.centered {
		text-align: center;
		vertical-align: middle;
	}
  tr, th {
    text-align: center;
		vertical-align: middle;
  }
  .opac {
    opacity: 25%;
  }
	/* ini css======================================= */
	.cssload-loader {
		position: absolute;
		left: 50%;
		width: 47.284271247462px;
		height: 47.284271247462px;
		margin-left: -23.142135623731px;
		margin-top: -23.142135623731px;
		border-radius: 100%;
		animation-name: cssload-loader;
			-o-animation-name: cssload-loader;
			-ms-animation-name: cssload-loader;
			-webkit-animation-name: cssload-loader;
			-moz-animation-name: cssload-loader;
		animation-iteration-count: infinite;
			-o-animation-iteration-count: infinite;
			-ms-animation-iteration-count: infinite;
			-webkit-animation-iteration-count: infinite;
			-moz-animation-iteration-count: infinite;
		animation-timing-function: linear;
			-o-animation-timing-function: linear;
			-ms-animation-timing-function: linear;
			-webkit-animation-timing-function: linear;
			-moz-animation-timing-function: linear;
		animation-duration: 4.6s;
			-o-animation-duration: 4.6s;
			-ms-animation-duration: 4.6s;
			-webkit-animation-duration: 4.6s;
			-moz-animation-duration: 4.6s;
	}
	.cssload-loader .cssload-side {
		display: block;
		width: 6px;
		height: 19px;
		background-color: rgb(27,103,255);
		margin: 2px;
		position: absolute;
		border-radius: 50%;
		animation-duration: 1.73s;
			-o-animation-duration: 1.73s;
			-ms-animation-duration: 1.73s;
			-webkit-animation-duration: 1.73s;
			-moz-animation-duration: 1.73s;
		animation-iteration-count: infinite;
			-o-animation-iteration-count: infinite;
			-ms-animation-iteration-count: infinite;
			-webkit-animation-iteration-count: infinite;
			-moz-animation-iteration-count: infinite;
		animation-timing-function: ease;
			-o-animation-timing-function: ease;
			-ms-animation-timing-function: ease;
			-webkit-animation-timing-function: ease;
			-moz-animation-timing-function: ease;
	}
	.cssload-loader .cssload-side:nth-child(1),
	.cssload-loader .cssload-side:nth-child(5) {
		transform: rotate(0deg);
			-o-transform: rotate(0deg);
			-ms-transform: rotate(0deg);
			-webkit-transform: rotate(0deg);
			-moz-transform: rotate(0deg);
		animation-name: cssload-rotate0;
			-o-animation-name: cssload-rotate0;
			-ms-animation-name: cssload-rotate0;
			-webkit-animation-name: cssload-rotate0;
			-moz-animation-name: cssload-rotate0;
	}
	.cssload-loader .cssload-side:nth-child(3),
	.cssload-loader .cssload-side:nth-child(7) {
		transform: rotate(90deg);
			-o-transform: rotate(90deg);
			-ms-transform: rotate(90deg);
			-webkit-transform: rotate(90deg);
			-moz-transform: rotate(90deg);
		animation-name: cssload-rotate90;
			-o-animation-name: cssload-rotate90;
			-ms-animation-name: cssload-rotate90;
			-webkit-animation-name: cssload-rotate90;
			-moz-animation-name: cssload-rotate90;
	}
	.cssload-loader .cssload-side:nth-child(2),
	.cssload-loader .cssload-side:nth-child(6) {
		transform: rotate(45deg);
			-o-transform: rotate(45deg);
			-ms-transform: rotate(45deg);
			-webkit-transform: rotate(45deg);
			-moz-transform: rotate(45deg);
		animation-name: cssload-rotate45;
			-o-animation-name: cssload-rotate45;
			-ms-animation-name: cssload-rotate45;
			-webkit-animation-name: cssload-rotate45;
			-moz-animation-name: cssload-rotate45;
	}
	.cssload-loader .cssload-side:nth-child(4),
	.cssload-loader .cssload-side:nth-child(8) {
		transform: rotate(135deg);
			-o-transform: rotate(135deg);
			-ms-transform: rotate(135deg);
			-webkit-transform: rotate(135deg);
			-moz-transform: rotate(135deg);
		animation-name: cssload-rotate135;
			-o-animation-name: cssload-rotate135;
			-ms-animation-name: cssload-rotate135;
			-webkit-animation-name: cssload-rotate135;
			-moz-animation-name: cssload-rotate135;
	}
	.cssload-loader .cssload-side:nth-child(1) {
		top: 23.142135623731px;
		left: 47.284271247462px;
		margin-left: -3px;
		margin-top: -10px;
		animation-delay: 0;
			-o-animation-delay: 0;
			-ms-animation-delay: 0;
			-webkit-animation-delay: 0;
			-moz-animation-delay: 0;
	}
	.cssload-loader .cssload-side:nth-child(2) {
		top: 40.213203431093px;
		left: 40.213203431093px;
		margin-left: -3px;
		margin-top: -10px;
		animation-delay: 0;
			-o-animation-delay: 0;
			-ms-animation-delay: 0;
			-webkit-animation-delay: 0;
			-moz-animation-delay: 0;
	}
	.cssload-loader .cssload-side:nth-child(3) {
		top: 47.284271247462px;
		left: 23.142135623731px;
		margin-left: -3px;
		margin-top: -10px;
		animation-delay: 0;
			-o-animation-delay: 0;
			-ms-animation-delay: 0;
			-webkit-animation-delay: 0;
			-moz-animation-delay: 0;
	}
	.cssload-loader .cssload-side:nth-child(4) {
		top: 40.213203431093px;
		left: 7.0710678163691px;
		margin-left: -3px;
		margin-top: -10px;
		animation-delay: 0;
			-o-animation-delay: 0;
			-ms-animation-delay: 0;
			-webkit-animation-delay: 0;
			-moz-animation-delay: 0;
	}
	.cssload-loader .cssload-side:nth-child(5) {
		top: 23.142135623731px;
		left: 0px;
		margin-left: -3px;
		margin-top: -10px;
		animation-delay: 0;
			-o-animation-delay: 0;
			-ms-animation-delay: 0;
			-webkit-animation-delay: 0;
			-moz-animation-delay: 0;
	}
	.cssload-loader .cssload-side:nth-child(6) {
		top: 7.0710678163691px;
		left: 7.0710678163691px;
		margin-left: -3px;
		margin-top: -10px;
		animation-delay: 0;
			-o-animation-delay: 0;
			-ms-animation-delay: 0;
			-webkit-animation-delay: 0;
			-moz-animation-delay: 0;
	}
	.cssload-loader .cssload-side:nth-child(7) {
		top: 0px;
		left: 23.142135623731px;
		margin-left: -3px;
		margin-top: -10px;
		animation-delay: 0;
			-o-animation-delay: 0;
			-ms-animation-delay: 0;
			-webkit-animation-delay: 0;
			-moz-animation-delay: 0;
	}
	.cssload-loader .cssload-side:nth-child(8) {
		top: 7.0710678163691px;
		left: 40.213203431093px;
		margin-left: -3px;
		margin-top: -10px;
		animation-delay: 0;
			-o-animation-delay: 0;
			-ms-animation-delay: 0;
			-webkit-animation-delay: 0;
			-moz-animation-delay: 0;
	}

	@keyframes cssload-rotate0 {
		0% {
			transform: rotate(0deg);
		}
		60% {
			transform: rotate(180deg);
		}
		100% {
			transform: rotate(180deg);
		}
	}

	@-o-keyframes cssload-rotate0 {
		0% {
			-o-transform: rotate(0deg);
		}
		60% {
			-o-transform: rotate(180deg);
		}
		100% {
			-o-transform: rotate(180deg);
		}
	}

	@-ms-keyframes cssload-rotate0 {
		0% {
			-ms-transform: rotate(0deg);
		}
		60% {
			-ms-transform: rotate(180deg);
		}
		100% {
			-ms-transform: rotate(180deg);
		}
	}

	@-webkit-keyframes cssload-rotate0 {
		0% {
			-webkit-transform: rotate(0deg);
		}
		60% {
			-webkit-transform: rotate(180deg);
		}
		100% {
			-webkit-transform: rotate(180deg);
		}
	}

	@-moz-keyframes cssload-rotate0 {
		0% {
			-moz-transform: rotate(0deg);
		}
		60% {
			-moz-transform: rotate(180deg);
		}
		100% {
			-moz-transform: rotate(180deg);
		}
	}

	@keyframes cssload-rotate90 {
		0% {
			transform: rotate(90deg);
							transform: rotate(90deg);
		}
		60% {
			transform: rotate(270deg);
							transform: rotate(270deg);
		}
		100% {
			transform: rotate(270deg);
							transform: rotate(270deg);
		}
	}

	@-o-keyframes cssload-rotate90 {
		0% {
			-o-transform: rotate(90deg);
							transform: rotate(90deg);
		}
		60% {
			-o-transform: rotate(270deg);
							transform: rotate(270deg);
		}
		100% {
			-o-transform: rotate(270deg);
							transform: rotate(270deg);
		}
	}

	@-ms-keyframes cssload-rotate90 {
		0% {
			-ms-transform: rotate(90deg);
							transform: rotate(90deg);
		}
		60% {
			-ms-transform: rotate(270deg);
							transform: rotate(270deg);
		}
		100% {
			-ms-transform: rotate(270deg);
							transform: rotate(270deg);
		}
	}

	@-webkit-keyframes cssload-rotate90 {
		0% {
			-webkit-transform: rotate(90deg);
							transform: rotate(90deg);
		}
		60% {
			-webkit-transform: rotate(270deg);
							transform: rotate(270deg);
		}
		100% {
			-webkit-transform: rotate(270deg);
							transform: rotate(270deg);
		}
	}

	@-moz-keyframes cssload-rotate90 {
		0% {
			-moz-transform: rotate(90deg);
							transform: rotate(90deg);
		}
		60% {
			-moz-transform: rotate(270deg);
							transform: rotate(270deg);
		}
		100% {
			-moz-transform: rotate(270deg);
							transform: rotate(270deg);
		}
	}

	@keyframes cssload-rotate45 {
		0% {
			transform: rotate(45deg);
							transform: rotate(45deg);
		}
		60% {
			transform: rotate(225deg);
							transform: rotate(225deg);
		}
		100% {
			transform: rotate(225deg);
							transform: rotate(225deg);
		}
	}

	@-o-keyframes cssload-rotate45 {
		0% {
			-o-transform: rotate(45deg);
							transform: rotate(45deg);
		}
		60% {
			-o-transform: rotate(225deg);
							transform: rotate(225deg);
		}
		100% {
			-o-transform: rotate(225deg);
							transform: rotate(225deg);
		}
	}

	@-ms-keyframes cssload-rotate45 {
		0% {
			-ms-transform: rotate(45deg);
							transform: rotate(45deg);
		}
		60% {
			-ms-transform: rotate(225deg);
							transform: rotate(225deg);
		}
		100% {
			-ms-transform: rotate(225deg);
							transform: rotate(225deg);
		}
	}

	@-webkit-keyframes cssload-rotate45 {
		0% {
			-webkit-transform: rotate(45deg);
							transform: rotate(45deg);
		}
		60% {
			-webkit-transform: rotate(225deg);
							transform: rotate(225deg);
		}
		100% {
			-webkit-transform: rotate(225deg);
							transform: rotate(225deg);
		}
	}

	@-moz-keyframes cssload-rotate45 {
		0% {
			-moz-transform: rotate(45deg);
							transform: rotate(45deg);
		}
		60% {
			-moz-transform: rotate(225deg);
							transform: rotate(225deg);
		}
		100% {
			-moz-transform: rotate(225deg);
							transform: rotate(225deg);
		}
	}

	@keyframes cssload-rotate135 {
		0% {
			transform: rotate(135deg);
							transform: rotate(135deg);
		}
		60% {
			transform: rotate(315deg);
							transform: rotate(315deg);
		}
		100% {
			transform: rotate(315deg);
							transform: rotate(315deg);
		}
	}

	@-o-keyframes cssload-rotate135 {
		0% {
			-o-transform: rotate(135deg);
							transform: rotate(135deg);
		}
		60% {
			-o-transform: rotate(315deg);
							transform: rotate(315deg);
		}
		100% {
			-o-transform: rotate(315deg);
							transform: rotate(315deg);
		}
	}

	@-ms-keyframes cssload-rotate135 {
		0% {
			-ms-transform: rotate(135deg);
							transform: rotate(135deg);
		}
		60% {
			-ms-transform: rotate(315deg);
							transform: rotate(315deg);
		}
		100% {
			-ms-transform: rotate(315deg);
							transform: rotate(315deg);
		}
	}

	@-webkit-keyframes cssload-rotate135 {
		0% {
			-webkit-transform: rotate(135deg);
							transform: rotate(135deg);
		}
		60% {
			-webkit-transform: rotate(315deg);
							transform: rotate(315deg);
		}
		100% {
			-webkit-transform: rotate(315deg);
							transform: rotate(315deg);
		}
	}

	@-moz-keyframes cssload-rotate135 {
		0% {
			-moz-transform: rotate(135deg);
							transform: rotate(135deg);
		}
		60% {
			-moz-transform: rotate(315deg);
							transform: rotate(315deg);
		}
		100% {
			-moz-transform: rotate(315deg);
							transform: rotate(315deg);
		}
	}

	@keyframes cssload-loader {
		0% {
			transform: rotate(0deg);
							transform: rotate(0deg);
		}
		100% {
			transform: rotate(360deg);
							transform: rotate(360deg);
		}
	}

	@-o-keyframes cssload-loader {
		0% {
			-o-transform: rotate(0deg);
							transform: rotate(0deg);
		}
		100% {
			-o-transform: rotate(360deg);
							transform: rotate(360deg);
		}
	}

	@-ms-keyframes cssload-loader {
		0% {
			-ms-transform: rotate(0deg);
							transform: rotate(0deg);
		}
		100% {
			-ms-transform: rotate(360deg);
							transform: rotate(360deg);
		}
	}

	@-webkit-keyframes cssload-loader {
		0% {
			-webkit-transform: rotate(0deg);
							transform: rotate(0deg);
		}
		100% {
			-webkit-transform: rotate(360deg);
							transform: rotate(360deg);
		}
	}

	@-moz-keyframes cssload-loader {
		0% {
			-moz-transform: rotate(0deg);
							transform: rotate(0deg);
		}
		100% {
			-moz-transform: rotate(360deg);
							transform: rotate(360deg);
		}
	}
</style>
<link rel="stylesheet" href="/css/daterangepicker.css">
@endsection
@section('content')
@if (Session::has('alerts'))
  @foreach(Session::get('alerts') as $alert)
    <div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
  @endforeach
@endif
<div class="container-fluid">
  <div class="row justify-content-center">
    <div class="col-md-12">
      <form id="submit_dash" method="GET">
				{{ csrf_field() }}
        <div class="row">
          <div class="col-md-12">
            <div class="card shadow mb-4">
              <div class="card-body">
                <p class="mb-2"><strong>Dashboard Pekerjaan</strong></p>
                <div class="form-row">
                  <div class="form-group col-md-3">
                    <label for="witel">WITEL</label>
										<input type="hidden" name="jenis_period" id="jenis_period" value="all">
                    <select class="form-control select2" id="witel" name="witel">
                      <option value="All">ALL</option>
                      @foreach ($witel_mod as $val)
                        <option value="{{ $val }}">{{ $val }}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="row col-md-4">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="tgl_select">PERIODE</label>
                        <select class="form-control" id="tgl_select">
                          <option value="Tahun">Tahun</option>
                          <option value="Custom">Custom Range</option>
                          <option value="all_sp">Semua Periode SP</option>
                          <option value="Bulan">Bulan</option>
                        </select>
                        <input type="hidden" name="tgl_hidden" id="hid_tgl" value="all">
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group isi_tgl">
                      </div>
                    </div>
                  </div>
                  @if (session('auth')->proc_level != 2)
										<div class="form-group col-md-3">
											<label for="mitra">MITRA</label>
											<select class="form-control select2" id="mitra" name="mitra">
											</select>
										</div>
									@endif
									@if (empty(session('auth')->peker_pup))
										<div class="form-group col-md-2">
											<label for="khs">Pekerjaan</label>
											<select class="form-control select2" id="khs" name="khs">
												<option value="All">ALL Pekerjaan</option>
												@foreach ($data_pekerjaan as $val)
													<option value="{{ $val->pekerjaan }}">{{ $val->pekerjaan }}</option>
												@endforeach
											</select>
										</div>
									@endif
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class=" ldg">
              <div class="cssload-side"></div>
              <div class="cssload-side"></div>
              <div class="cssload-side"></div>
              <div class="cssload-side"></div>
              <div class="cssload-side"></div>
              <div class="cssload-side"></div>
              <div class="cssload-side"></div>
              <div class="cssload-side"></div>
            </div>
            <div class="table-responsive">
              <table class="table isi_tbl table-bordered table-striped" style="width: 100%">
                <thead class="thead-dark">
                  <tr>
                    <th>Jenis Pekerjaan</th>
                    <th>User / Operations</th>
                    <th>Mitra</th>
                    <th>Procurement Area</th>
                    <th>Procurement Regional</th>
                    <th>Finance</th>
                    <th>Cash&Bank</th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
          <div class="col-md-12">
            <div class="card shadow mb-4">
              <div class="card-body">
                <div class="form-row">
                  <div class="form-group col-md-3">
                    <label for="jenis_dashboard">Jenis Dashboard</label>
                    <select class="form-control select2" id="jenis_dashboard" name="jenis_dashboard">
                        <option value="Simple">Simple</option>
                        <option value="Detail">Detail</option>
                    </select>
                  </div>
                  <div class="row col-md-4 div_tamp">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="tampilan">Tampilan</label>
                        <select class="form-control select2" id="tampilan" name="tampilan">
                          <option value="val_sp">Tampilkan jumlah SP saja</option>
                          <option value="price_sp">Tampilkan nilai SP saja</option>
                          <option value="price_rekon">Tampilkan nilai rekon saja</option>
                          <option value="val_price_sp">Tampilkan jumlah dan nominal SP</option>
                          <option value="val_sp_price_rekon">Tampilkan jumlah SP dan nilai rekon</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="form-group col-md-3">
                    <label for="tgl_progress_kerja">Tanggal Proses Pengerjaan</label>
                    <input type='text' id='tgl_progress_kerja' name="tgl_progress_kerja" class='tgl_progress_kerja form-control'>
                  </div>
                  <div class="form-group col-md-2">
                    <label>Excel Download</label>
                    <a type="button" class="btn btn-block btn_download btn-success" style="color: white"><i class="fe fe-download fe-16"></i>&nbsp;Download Excel</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection
@section('footerS')
<script src="/js/moment.min.js"></script>
<script src="/js/daterangepicker.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js@3.5.1/dist/chart.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@2.0.0"></script>
<script src='/js/select2.min.js'></script>
<script src='/js/jquery.timepicker.js'></script>
<script>
	$(function(){
		var mitra = {!! json_encode($mitra_modify) !!};
		console.log(mitra)
		$('#witel').val(null).change()
		$('#witel').on('change', function() {
			var isi = $(this).val(),
			isi_select = [],
			pra_select = {};
			if(isi == 'KALSEL'){
				pra_select.KALSEL = mitra.KALSEL;
			}else if(isi == 'BALIKPAPAN'){
				pra_select.BALIKPAPAN = mitra.BALIKPAPAN;
			}else if(isi == 'All'){
				pra_select = mitra;
			}else{
				pra_select = {};
			}

			if ($("#mitra").hasClass("select2-hidden-accessible")) {
				// $("#mitra").select2('destroy');
				$("#mitra").empty().change();
			}

			if(!$.isEmptyObject(pra_select)){
				$.each(pra_select, function(key, index){
					isi_select.push({
						id: 'all',
						text: 'Semua Mitra'
					})
					$.each(index, function(keyc1, indexc1){
						isi_select.push({
							id: indexc1.id,
							text: indexc1.text
						})
					})
				})

				if(isi_select){
					$("#mitra").select2({
						theme: 'bootstrap4',
						width: '100%',
						data: isi_select
					}).change();
				}else{
					pra_select = {};
					isi_select = {};
				}
			}else{
				if ($("#mitra").hasClass("select2-hidden-accessible")) {
					$("#mitra").empty().change();
					isi_select = {};
					pra_select = {};
				}
			}
		})

		$('#khs').select2(
		{
			theme: 'bootstrap4',
			placeholder: 'Pilih Jenis Pekerjaan!',
			width: '100%'
		});

		$('#tgl_select').val(null).change();
		$('.isi_tgl').empty();
		$('#tgl_select').select2(
		{
			theme: 'bootstrap4',
			placeholder: 'Pilih Jenis Periode',
			width: '100%'
		});

		var start = moment().startOf('month'),
		end = moment().endOf('month');

		$('#tgl_select').on('change', function(){
			var isi = $(this).val(),
			date_select = [];
			console.log(isi)
			$('#hid_tgl').val(isi);
			$('#jenis_period').val('sp')
			if($.inArray(isi, ['Tahun', 'Bulan']) !== -1 ){
				$('.isi_tgl').empty();

				$('.isi_tgl').html("<select id='tgl' name='tgl' class='tgl form-control select2'></select>");
				if(isi == 'Tahun'){
					var start_year = new Date().getFullYear();

					for (var i = start_year; i >= 2021; i--) {
						date_select.push({
							id: i.toString(),
							text: i.toString()
						});
					}
				}

				if(isi == 'Bulan'){
					date_select = [
						{
							id: 1,
							text: 'Januari'
						},
						{
							id: 2,
							text: 'Februari'
						},
						{
							id: 3,
							text: 'Maret'
						},
						{
							id: 4,
							text: 'April'
						},
						{
							id: 5,
							text: 'Mei'
						},
						{
							id: 6,
							text: 'Juni'
						},
						{
							id: 7,
							text: 'Juli'
						},
						{
							id: 8,
							text: 'Agustus'
						},
						{
							id: 9,
							text: 'September'
						},
						{
							id: 10,
							text: 'Oktober'
						},
						{
							id: 11,
							text: 'November'
						},
						{
							id: 12,
							text: 'Desember'
						},
					];
				}

				if ($("#tgl").hasClass("select2-hidden-accessible")) {
					$("#tgl").select2('destroy');
				}

				$('#tgl').select2({
					theme: 'bootstrap4',
					width: '100%',
					placeholder:'Silahkan Pilih Periode',
					data: date_select
				});

				$('#tgl').val(null).change()

			}else if($.inArray(isi, ['Custom']) !== -1 ){
				$('.isi_tgl').empty();
				$('.isi_tgl').html("<input type='text' name='tgl' id='tgl' class='tgl form-control select2'>");

				$('.tgl').daterangepicker(
				{
					locale: {
						format: 'YYYY-MM-DD'
					},
					startDate: start,
					endDate: end
				})
			}else{
				console.log('ini hapus')
				$('.isi_tgl').empty();
			}
		})

		$(document).on('select2:select change','.select2',function(){
			// console.log($(this))
			$.ajax({
				url: "/get_ajx/getDashboardData",
				type: "GET",
				data: $("#submit_dash").serialize(),
				beforeSend: function (res) {
					$('.ldg').toggleClass('cssload-loader')
					$('.isi_tbl').toggleClass('opac')
				}
			}).done(function(result) {
				$('.ldg').toggleClass('cssload-loader')
				$('.isi_tbl').toggleClass('opac')
				$(".isi_tbl").html(result);
			});
		});

		$('.tgl_progress_kerja').daterangepicker(
		{
			locale: {
				format: 'YYYY-MM-DD'
			},
			startDate: start,
			endDate: end
		})

		$(".tgl_progress_kerja").change(function(){
			$('#jenis_period').val('progress_kerja')
			$.ajax({
				url: "/get_ajx/getDashboardData",
				type: "GET",
				data: $("#submit_dash").serialize(),
				beforeSend: function (res) {
					$('.ldg').toggleClass('cssload-loader')
					$('.isi_tbl').toggleClass('opac')
				}
			}).done(function(result) {
				$('.ldg').toggleClass('cssload-loader')
				$('.isi_tbl').toggleClass('opac')
				$(".isi_tbl").html(result);
			});
		})

		$('#witel').select2(
		{
			theme: 'bootstrap4',
			placeholder: 'Pilih Jenis Witel!',
			width: '100%'
		});

		$('#jenis_dashboard').select2(
		{
			theme: 'bootstrap4',
			placeholder: 'Pilih Jenis Tabel!',
			width: '100%'
		});

		$('#jenis_dashboard').on('select2:select change', function(){
			if($(this).val() == 'Detail'){
				$('#tampilan').val(null).change();
				$('#tampilan').attr('disabled', true);
				$('.div_tamp').hide();
			}else{
				$('#tampilan').attr('disabled', false);
				$('.div_tamp').show();
				$('#tampilan').val('val_sp').change();
			}
		})

		$('#tampilan').select2(
		{
			theme: 'bootstrap4',
			placeholder: 'Pilih Data Yang Ditampilkan!',
			width: '100%'
		});

		$('#mitra').select2(
		{
			theme: 'bootstrap4',
			placeholder: 'Pilih Mitra yang dicari!',
			width: '100%'
		});

		$('.btn_download').on('click', function(e){
			e.preventDefault();
			var witel = $('#witel').val(),
			tgl = $('#tgl').val(),
			hid_tgl = $('#hid_tgl').val(),
			mitra = $('#mitra').val(),
			khs = $('#khs').val(),
			tgl_progress_kerja = $('#tgl_progress_kerja').val(),
			jenis_period = $('#jenis_period').val(),
			tanggal = 'all',
			jenis_tanggal = 'all';

			if(jenis_period == 'sp'){
				tanggal = tgl;
				jenis_tanggal = hid_tgl;
			}else{
				tanggal = tgl_progress_kerja;
				jenis_tanggal = 'tgl_progress_kerja';
			}
			window.open('/download/excel/'+ (witel ?? 'All') + '/' +( mitra ?? 'All') + '/' + (khs ?? 'All') + '/' + (jenis_period.length != 0 ? jenis_period : 'progress_kerja') + '/' + jenis_tanggal + '/' + tanggal, '_blank');
		})

		$("#jenis_dashboard").val('Simple').change();

	})
</script>
@endsection