@extends('layout')
@section('title', 'List Pekerjaan')
@section('headerS')
<link rel="stylesheet" href="/css/dataTables.bootstrap4.css">

@endsection

@section('content')
@if (Session::has('alerts'))
  @foreach(Session::get('alerts') as $alert)
    <div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
  @endforeach
@endif
<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-12">
			<div class="row">
				<div class="col-md-12 my-4">
					<div class="card shadow mb-4">
						<div class="card-body">
							@php
								$judul = '';
								// switch (Request::segment(11)) {
								// 	case 'user':
								// 		$judul = "User";
								// 	break;
								// 	case 'mitra':
								// 		$judul = "Mitra";
								// 	break;
								// 	case 'proc_a':
								// 		$judul = "Procurement Area";
								// 	break;
								// 	case 'proc_r':
								// 		$judul = "Procurement Regional";
								// 	break;
								// 	case 'finn':
								// 		$judul = "Finance";
								// 	break;
								// 	case 'cashbank':
								// 		$judul = "Cash & Bank";
								// 	break;
								// 	case 'commerce_create_pid':
								// 		$judul = "Create PID";
								// 	break;
								// 	case 'operation_req_pid':
								// 		$judul = "Request PID";
								// 	break;
								// 	case 'operation_upload_justif':
								// 		$judul = "Upload Justifikasi";
								// 	break;
								// 	case 'proca_s_pen':
								// 		$judul = "Surat Penetapan";
								// 	break;
								// 	case 'mitra_sanggup':
								// 		$judul = "Surat Kesanggupan";
								// 	break;
								// 	case 'proca_sp':
								// 		$judul = "Surat Pesanan";
								// 	break;
								// 	case 'mitra_boq_rek':
								// 		$judul = "BOQ Rekon";
								// 	break;
								// 	case 'operation_verif_boq':
								// 		$judul = "Amandemen";
								// 	break;
								// 	case 'commerce_budget':
								// 		$judul = "Budget";
								// 	break;
								// 	case 'mitra_pelurusan_material':
								// 		$judul = "Pelurusan Material";
								// 	break;
								// 	case 'mitra_lengkapi_nomor':
								// 		$judul = "ABD";
								// 	break;
								// 	case 'proca_ver_doc_area':
								// 		$judul = "Verifikasi Dokumen Area";
								// 	break;
								// 	case 'mitra_ttd':
								// 		$judul = "Upload TTD";
								// 	break;
								// 	case 'procg_ver_doc_reg':
								// 		$judul = "Verifikasi Dokumen Regional";
								// 	break;
								// 	case 'finance':
								// 		$judul = "Finance";
								// 	break;
								// 	case 'cb':
								// 		$judul = "Cash & Bank";
								// 	break;
								// }
							@endphp
							<a type="button" href="/download_work/{{ Request::segment(2) }}/{{ Request::segment(3) }}/{{ Request::segment(4) }}" class="btn btn-info btn-sm download_excel" style="margin-bottom: 5px;"><i class="fe fe-download fe-16"></i>&nbsp;Unduh List Pekerjaan</a>
							<h5 class="card-title">Tabel List Pekerjaan <b><u>{{ $title }}</u></b></h5>
							<table id ="tb_sp_perM" class="table table-striped table-bordered table-hover">
								<thead class="thead-dark">
									<tr>
										<th>Mitra</th>
										<th class="no_sort">Tanggal Buat</th>
										<th>Umur (Hari)</th>
										<th style="width:40%">Dokumen</th>
										@if($view_table != 'mitra_only')
											<th class="no_sort" style="width:10%">PID</th>
										@endif
										<th style="width:10%">Nilai SP</th>
										<th style="width:10%">Nilai Rekon</th>
										<th class="no_sort">Status</th>
										<th class="no_sort">Action</th>
									</tr>
								</thead>
								<tbody id="data_table">
									@php
										$num = 1;
									@endphp
									@forelse($result as $k => $d)
										<tr>
											<td>{{ $d->nama_company }}</td>
											<td>{{ $d->created_at }}</td>
											<td>{{ $d->jml_hri }}</td>
											<td>{{ $d->judul_work }}</td>
											@if($view_table != 'mitra_only')
												<td>{{ $d->id_project }}</td>
											@endif
											<td>Rp. {{ number_format($d->total_material_sp + $d->total_jasa_sp, 0, '.', '.') }}</td>
											<td>Rp. {{ number_format($d->total_material_rekon + $d->total_jasa_rekon, 0, '.', '.') }}</td>
											<td>{{ $d->nama_step }}<br/>({{ $d->aktor_nama_step }})</td>
											<td>
												<a style="margin-bottom: 5px;" href="/get_detail_laporan/{{$d->id}}" target="_blank" class="btn btn-sm btn-primary"><i class="fe fe-eye fe-16"></i></a>
												<a style="margin-bottom: 5px;" href="/download/full_rar/{{$d->id}}" class="btn btn-sm btn-info"><i class="fe fe-download fe-16"></i></a>
												<a style="margin-bottom: 5px;" href="/timeline/{{$d->id}}" class="btn btn-sm btn-secondary"><i class="fe fe-clock fe-16"></i></a>
											</td>
										</tr>
									@empty
									@endforelse
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('footerS')
<script src='/js/jquery.dataTables.min.js'></script>
<script src='/js/dataTables.bootstrap4.min.js'></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<script type="text/javascript">
	$(function(){
		$(".download_excel").attr('href', $('.download_excel').attr('href') + '?' + window.location.href.split('?')[1]);
		$('#tb_sp_perM').DataTable({
			autoWidth: true,
			columnDefs: [{
				targets: 'no_sort',
				orderable: false
			}],
			lengthMenu: [
				[16, 32, 64, -1],
				[16, 32, 64, "All"]
			]
		});
	});
</script>
@endsection