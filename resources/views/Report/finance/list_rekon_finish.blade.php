@extends('layout')
@section('title', 'List Dokumen Finish')
@section('headerS')
<link rel="stylesheet" href="/css/dataTables.bootstrap4.css">
<style>
	@media (min-width: 768px) {
		.modal-xl {
			width: 100%;
		max-width:1200px;
		}
	}
</style>
@endsection
@section('content')
@if (Session::has('alerts'))
  @foreach(Session::get('alerts') as $alert)
    <div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
  @endforeach
@endif
<div class="modal fade" id="modal_comp" tabindex="-1" role="dialog" aria-labelledby="modal_compTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
				<h5 class="modal-title">Detail Pekerjaan <u><i><span id="judul_pekerjaan"></span></i></u></h5>
      </div>
      <div class="modal-body data_mdl" style=" overflow: hidden;">
        <div class="row">
					<div class="col-md-6">
						<div class="form-group row">
							<label class="col-form-label col-md-3">Project ID:</label>
							<div class="col-md-9">
								<textarea style="resize:none; font-weight: bold" cols='50' rows="3" class="form-control input-transparent pid" readonly></textarea>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-md-3">Jenis Pekerjaan:</label>
							<div class="col-md-9">
								<p class="form-control input-transparent" readonly><b class="jenis_work"></b></p>
							</div>
						</div>
						<div class="row border-top py-3">
							<div class="col-md-6">
								<div class="form-group">
									<label class="col-form-label">Surat Penetapan:</label>
									<textarea style="resize:none; font-weight: bold" cols='50' rows="2" class="form-control input-transparent surat_penetapan" readonly></textarea>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="col-form-label">Tanggal Surat Penetapan:</label>
									<p class="form-control input-transparent" readonly><b class="tgl_s_pen"></b></p>
								</div>
							</div>
						</div>
						<div class="row border-top py-3">
							<div class="col-md-6">
								<div class="form-group">
									<label class="col-form-label">Surat Pesanan:</label>
									<textarea style="resize:none; font-weight: bold" cols='50' rows="2" class="form-control input-transparent no_sp" readonly></textarea>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<label class="col-form-label">Due Date:</label>
									<p class="form-control input-transparent" readonly><b class="jt"></b></p>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<label class="col-form-label">Durasi:</label>
									<p class="form-control input-transparent" readonly><b class="durasi"></b></p>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-md-3">Lokasi:</label>
							<div class="col-md-9">
								<p class="form-control input-transparent" readonly><b class="lokasi"></b></p>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-md-3">PKS:</label>
							<div class="col-md-9">
								<p class="form-control input-transparent" readonly><b class="pks"></b></p>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group row">
							<label class="col-form-label col-md-3">Mitra:</label>
							<div class="col-md-9">
								<p class="form-control input-transparent" readonly><b class="nama_company"></b></p>
							</div>
						</div>
						<div class="row border-top py-3">
							<div class="col-md-6">
								<div class="form-group">
									<label class="col-form-label">Total Materail:</label>
									<p class="form-control input-transparent" readonly><b class="total_material"></b></p>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="col-form-label">Total Jasa:</label>
									<p class="form-control input-transparent" readonly><b class="total_jasa"></b></p>
								</div>
							</div>
						</div>
						<div class="row border-top py-3">
							<div class="col-md-4">
								<div class="form-group">
									<label class="col-form-label">Grand Total:</label>
									<p class="form-control input-transparent" readonly><b class="gd"></b></p>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label class="col-form-label">Grand Total PPN:</label>
									<p class="form-control input-transparent" readonly><b class="gd_ppn"></b></p>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label class="col-form-label">Grand Total + PPN:</label>
									<p class="form-control input-transparent" readonly><b class="total"></b></p>
								</div>
							</div>
						</div>
						<a class="btn btn-info btn-block rar_down" href=""><i class="fe fe-download fe-16"></i> File Pekerjaan</a>
					</div>
					<div class="col-md-12 border-top py-3">
						<div class="row" style="display: block;">
							<form method="post" class="form-label-right">
								<div class="col-md-12">
									{{ csrf_field() }}
									<div class="form-group row check_lurus">
										<label class="col-form-label col-md-3" for="upload_boq">Keputusan :</label>
										<input type="hidden" id="id_boq_up" name="id_boq_up" value="">
										<div class="col-md-9">
											<select class="form-control status_select" name="status">
												<option value='APPROVE'>Finish</option>
												<option value='REJECT'>Reject</option>
											</select>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-form-label col-md-3" for='detail'>Alasan :</label>
										<div class="col-md-9">
											<textarea style="resize: none;" rows="2" name="detail" id="detail" class="form-control input-transparent" required></textarea>
										</div>
									</div>
									<div class="form-group row no_resi_return" style="display: none;">
										<label class="col-form-label col-md-3" for='no_resi_return'>No Resi Pengiriman:</label>
										<div class="col-md-9">
											<input name="no_resi_return" id="no_resi_return" class="form-control input-transparent">
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<button type="submit" class="btn btn-primary check_lurus_btn btn-block">Submit</button>
										</div>
										{{-- <div class="col-md-6">
											<a type="button" style="color: white;" class="btn btn-block btn_roll btn-warning"><i class="fe fe-corner-up-left fe-16"></i> Rollback </a>
										</div> --}}
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row justify-content-center">
    <div class="col-12">
      <h2 class="page-title">List Pekerjaan Finish</h2>
      <div class="row">
        <div class="col-md-12">
          <div class="card shadow">
            <div class="card-body table-responsive">
              <ul class="nav nav-tabs mb-3" id="myTab" role="tablist">
                <li class="active"><a href="#ready_rekon" id="data1" class="nav-link active" data-toggle="tab">BOQ Rekon Siap Check&nbsp;{!! count($ready_rekon) != 0 ? "<span class='badge badge-primary'>".count($ready_rekon)."</span>" : '' !!}</a></li>
								{{-- <li class=""><a href="#reject_regional" id="data4" class="nav-link" data-toggle="tab" aria-expanded="false">Dokumen Reject&nbsp;{!! count($reject_rekon) != 0 ? "<span class='badge badge-warning'>".count($reject_rekon)."</span>" : '' !!}</a></li> --}}
              </ul>
							<div id="myTabContentLeft" class="tab-content">
								<div class="tab-pane fade active show" id="ready_rekon">
									<table class="table table-striped table-bordered table-hover">
										<thead class="thead-dark">
											<tr>
												<th class="hidden-xs">#</th>
												<th>Pekerjaan</th>
												<th>Mitra</th>
												<th>Nomor SP</th>
												<th>Nomor PKS</th>
												<th>TOC</th>
												<th>Umur (Hari)</th>
												<th>Keterangan Terakhir</th>
												<th>Update Terakhir</th>
												<th class="no-sort">Action</th>
											</tr>
										</thead>
										<tbody id="data_table1">
											@php
												$num = 1;
											@endphp
											@foreach($ready_rekon as $d)
											<tr>
												<td class="hidden-xs">{{ $num++ }}</td>
												<td>{{ $d->judul }}</td>
												<td>{{ $d->mitra_nm }}</td>
												<td>{{ $d->no_sp }}</td>
												<td>{{ $d->pks }}</td>
												<td>{{ $d->tgl_jatuh_tmp }} Hari <b>({{ date('Y-m-d', strtotime('+'. $d->tgl_jatuh_tmp. ' day', strtotime($d->tgl_s_pen))) }})</b></td>
												<td>{{ $d->jml_hri }}</td>
												<td>{{ $d->detail }}</td>
												<td>{{ $d->nama_modif ? $d->nama_modif .' ('. $d->modified_by .')' : '' }}</td>
												<td><a class="btn btn-sm btn-primary modal_comp" style="color: #fff" data-toggle="modal" data-target="#modal_comp" data-stts='check' data-id="{{ $d->id }}"><i class="fe fe-eye fe-16"></i>&nbsp;Detail</a></td>
											</tr>
											@endforeach
										</tbody>
									</table>
								</div>
								{{-- <div class="tab-pane fade " id="reject_regional">
									<table class="table table-striped table-bordered table-hover">
										<thead class="thead-dark">
											<tr>
												<th class="hidden-xs">#</th>
												<th>Nomor SP</th>
												<th>Pekerjaan</th>
												<th>Nomor PKS</th>
												<th>TOC</th>
												<th>Umur</th>
												<th>Mitra</th>
												<th>Tanggal Save</th>
												<th>Detail</th>
												<th class="no-sort">Action</th>
											</tr>
										</thead>
										<tbody id="data_table4">
											@php
											$num = 1;
											@endphp
											@foreach($reject_rekon as $d)
											<tr>
												<td class="hidden-xs">{{ $num++ }}</td>
												<td>{{ $d->surat_penetapan }}</td>
												<td>{{ $d->judul }}</td>
												<td>{{ $d->pks }}</td>
												<td>{{ $d->tgl_jatuh_tmp }} Hari <b>({{ date('Y-m-d', strtotime('+'. $d->tgl_jatuh_tmp. ' day', strtotime($d->tgl_s_pen))) }})</b></td>
												<td>{{ $d->jml_hri }} Hari</td>
												<td>{{ $d->nama_company }}</td>
												<td>{{ $d->tgl_last_update }}</td>
												<td>{{ $d->detail }}</td>
												<td><a class="btn btn-sm btn-primary modal_comp" style="color: #fff" data-toggle="modal" data-target="#modal_comp" data-stts='reject' data-id="{{ $d->id }}"><i class="fe fe-eye fe-16"></i>&nbsp;Detail</a></td>
											</tr>
											@endforeach
										</tbody>
									</table>
								</div> --}}
							</div>
            </div>
          </div>
        </div>
      </div> <!-- end section -->
    </div> <!-- .col-12 -->
  </div> <!-- .row -->
</div>
@endsection
@section('footerS')
<script src='/js/jquery.dataTables.min.js'></script>
<script src='/js/dataTables.bootstrap4.min.js'></script>
<script type="text/javascript">
	$(function(){
		$.fn.digits = function(){
			return this.each(function(){
					$(this).text( $(this).text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") );
			});
		}
		$('.status_select').val('APPROVE').change();

		$('.status_select').on('change', function(){
			if($(this).val() == 'REJECT'){
				$('.no_resi_return').css({
					display: 'flex'
				});

				$('#no_resi_return').prop('required', true)

			}else{
				$('.no_resi_return').css({
					display: 'none'
				});

				$('#no_resi_return').removeAttr('required')

				$('#no_resi_return').val();
			}
		})

		$('.table').DataTable({
			autoWidth: true,
			columnDefs: [
				{
					targets: 'no-sort',
					orderable: false
				}
			],
			lengthMenu: [
				[16, 32, 64, -1],
				[16, 32, 64, "All"]
			]
		});

		$('.modal_comp').on('click', function (){
			var data_id = $(this).attr('data-id'),
			stts = $(this).attr('data-stts');

			if(stts == 'REJECT'){
				$('#detail').attr('disabled', true);
				$('#detail').html(data.detail);
				$('.check_lurus, .check_lurus_btn').css({
					'display': 'none'
				});
				$('#no_resi_return').prop('required', true)
			}else{
				$('#detail').attr('disabled', false);
				$('.check_lurus').css({
					'display': 'flex'
				});
				$('.check_lurus_btn').css({
					'display': 'block'
				});
				$('#no_resi_return').removeAttr('required')
			}

			var data_id = $(this).attr('data-id');
			$.ajax({
				url:"/get_ajx/get/detail/sp",
				type:"GET",
				data: {
					id : data_id,
					status: 'SP'
				},
				dataType: 'json',
				success: (function(data){
					console.log(data)
					var tgl = new Date(data.tgl_sp);
					tgl.setDate(tgl.getDate() + data.tgl_jatuh_tmp);

					$('#judul_pekerjaan').html(data.judul);
					$('.surat_penetapan').html(data.surat_penetapan);
					$('.tgl_s_pen').html(data.tgl_s_pen);
					$('.no_sp').html(data.no_sp);
					$('.tgl_sp').html(data.tgl_sp);
					$('.jt').html(data.durasi);
					$('.durasi').html(data.tgl_jatuh_tmp+ ' Hari');
					$('.lokasi').html(data.lokasi ?? data.witel);
					$('.pks').html(data.pks);
					$('.nama_company').html(data.nama_company);
					$('.total_material').html(data.total_material_sp);
					$('.total_jasa').html(data.total_jasa_sp);
					$('.gd').html(data.gd_sp);
					$('#id_boq_up').val(data.id);
					$('.budget_req').html(data.budget_req);
					$('.gd_ppn').html(data.gd_ppn_sp);
					$('.total').html(data.total_sp);
					$('.pid').html(data.id_project);
					$('.jenis_work').html(data.pekerjaan + ' | ' + data.jenis_work + ' | ' + data.jenis_kontrak);
					if (data.pekerjaan == 'PSB')
					{
						$('.rar_down').attr('href', '/upload/'+data.id+'/dokumen_ttd/File TTD.rar');
					} else {
						$('.rar_down').attr('href', '/download/full_rar/'+data.id);
					}
					$('.btn_roll').attr('href', '/rollback/'+data.id);

					$(".nama_company, .total_material, .total_jasa, .gd, .gd_ppn, .total, .budget_req").digits();
				}),
			})
		});

	});
</script>
@endsection