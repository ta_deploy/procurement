@extends('layout')
@section('title', 'Upload PO')
@section('headerS')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" rel="stylesheet" />
<link rel="stylesheet" href="/css/dataTables.bootstrap4.css">
<link rel="stylesheet" href="/css/bootstrap-datepicker.css">
<style type="text/css">
	.pull-right {
		text-align: right;
	}

  .ui-datepicker-calendar {
    display: none;
	}

	.opac {
    opacity: 25%;
  }
	/* ini css======================================= */
	.cssload-loader {
		position: absolute;
		left: 50%;
		width: 47.284271247462px;
		height: 47.284271247462px;
		margin-left: -23.142135623731px;
		margin-top: -23.142135623731px;
		border-radius: 100%;
		animation-name: cssload-loader;
			-o-animation-name: cssload-loader;
			-ms-animation-name: cssload-loader;
			-webkit-animation-name: cssload-loader;
			-moz-animation-name: cssload-loader;
		animation-iteration-count: infinite;
			-o-animation-iteration-count: infinite;
			-ms-animation-iteration-count: infinite;
			-webkit-animation-iteration-count: infinite;
			-moz-animation-iteration-count: infinite;
		animation-timing-function: linear;
			-o-animation-timing-function: linear;
			-ms-animation-timing-function: linear;
			-webkit-animation-timing-function: linear;
			-moz-animation-timing-function: linear;
		animation-duration: 4.6s;
			-o-animation-duration: 4.6s;
			-ms-animation-duration: 4.6s;
			-webkit-animation-duration: 4.6s;
			-moz-animation-duration: 4.6s;
	}
	.cssload-loader .cssload-side {
		display: block;
		width: 6px;
		height: 19px;
		background-color: rgb(27,103,255);
		margin: 2px;
		position: absolute;
		border-radius: 50%;
		animation-duration: 1.73s;
			-o-animation-duration: 1.73s;
			-ms-animation-duration: 1.73s;
			-webkit-animation-duration: 1.73s;
			-moz-animation-duration: 1.73s;
		animation-iteration-count: infinite;
			-o-animation-iteration-count: infinite;
			-ms-animation-iteration-count: infinite;
			-webkit-animation-iteration-count: infinite;
			-moz-animation-iteration-count: infinite;
		animation-timing-function: ease;
			-o-animation-timing-function: ease;
			-ms-animation-timing-function: ease;
			-webkit-animation-timing-function: ease;
			-moz-animation-timing-function: ease;
	}
	.cssload-loader .cssload-side:nth-child(1),
	.cssload-loader .cssload-side:nth-child(5) {
		transform: rotate(0deg);
			-o-transform: rotate(0deg);
			-ms-transform: rotate(0deg);
			-webkit-transform: rotate(0deg);
			-moz-transform: rotate(0deg);
		animation-name: cssload-rotate0;
			-o-animation-name: cssload-rotate0;
			-ms-animation-name: cssload-rotate0;
			-webkit-animation-name: cssload-rotate0;
			-moz-animation-name: cssload-rotate0;
	}
	.cssload-loader .cssload-side:nth-child(3),
	.cssload-loader .cssload-side:nth-child(7) {
		transform: rotate(90deg);
			-o-transform: rotate(90deg);
			-ms-transform: rotate(90deg);
			-webkit-transform: rotate(90deg);
			-moz-transform: rotate(90deg);
		animation-name: cssload-rotate90;
			-o-animation-name: cssload-rotate90;
			-ms-animation-name: cssload-rotate90;
			-webkit-animation-name: cssload-rotate90;
			-moz-animation-name: cssload-rotate90;
	}
	.cssload-loader .cssload-side:nth-child(2),
	.cssload-loader .cssload-side:nth-child(6) {
		transform: rotate(45deg);
			-o-transform: rotate(45deg);
			-ms-transform: rotate(45deg);
			-webkit-transform: rotate(45deg);
			-moz-transform: rotate(45deg);
		animation-name: cssload-rotate45;
			-o-animation-name: cssload-rotate45;
			-ms-animation-name: cssload-rotate45;
			-webkit-animation-name: cssload-rotate45;
			-moz-animation-name: cssload-rotate45;
	}
	.cssload-loader .cssload-side:nth-child(4),
	.cssload-loader .cssload-side:nth-child(8) {
		transform: rotate(135deg);
			-o-transform: rotate(135deg);
			-ms-transform: rotate(135deg);
			-webkit-transform: rotate(135deg);
			-moz-transform: rotate(135deg);
		animation-name: cssload-rotate135;
			-o-animation-name: cssload-rotate135;
			-ms-animation-name: cssload-rotate135;
			-webkit-animation-name: cssload-rotate135;
			-moz-animation-name: cssload-rotate135;
	}
	.cssload-loader .cssload-side:nth-child(1) {
		top: 23.142135623731px;
		left: 47.284271247462px;
		margin-left: -3px;
		margin-top: -10px;
		animation-delay: 0;
			-o-animation-delay: 0;
			-ms-animation-delay: 0;
			-webkit-animation-delay: 0;
			-moz-animation-delay: 0;
	}
	.cssload-loader .cssload-side:nth-child(2) {
		top: 40.213203431093px;
		left: 40.213203431093px;
		margin-left: -3px;
		margin-top: -10px;
		animation-delay: 0;
			-o-animation-delay: 0;
			-ms-animation-delay: 0;
			-webkit-animation-delay: 0;
			-moz-animation-delay: 0;
	}
	.cssload-loader .cssload-side:nth-child(3) {
		top: 47.284271247462px;
		left: 23.142135623731px;
		margin-left: -3px;
		margin-top: -10px;
		animation-delay: 0;
			-o-animation-delay: 0;
			-ms-animation-delay: 0;
			-webkit-animation-delay: 0;
			-moz-animation-delay: 0;
	}
	.cssload-loader .cssload-side:nth-child(4) {
		top: 40.213203431093px;
		left: 7.0710678163691px;
		margin-left: -3px;
		margin-top: -10px;
		animation-delay: 0;
			-o-animation-delay: 0;
			-ms-animation-delay: 0;
			-webkit-animation-delay: 0;
			-moz-animation-delay: 0;
	}
	.cssload-loader .cssload-side:nth-child(5) {
		top: 23.142135623731px;
		left: 0px;
		margin-left: -3px;
		margin-top: -10px;
		animation-delay: 0;
			-o-animation-delay: 0;
			-ms-animation-delay: 0;
			-webkit-animation-delay: 0;
			-moz-animation-delay: 0;
	}
	.cssload-loader .cssload-side:nth-child(6) {
		top: 7.0710678163691px;
		left: 7.0710678163691px;
		margin-left: -3px;
		margin-top: -10px;
		animation-delay: 0;
			-o-animation-delay: 0;
			-ms-animation-delay: 0;
			-webkit-animation-delay: 0;
			-moz-animation-delay: 0;
	}
	.cssload-loader .cssload-side:nth-child(7) {
		top: 0px;
		left: 23.142135623731px;
		margin-left: -3px;
		margin-top: -10px;
		animation-delay: 0;
			-o-animation-delay: 0;
			-ms-animation-delay: 0;
			-webkit-animation-delay: 0;
			-moz-animation-delay: 0;
	}
	.cssload-loader .cssload-side:nth-child(8) {
		top: 7.0710678163691px;
		left: 40.213203431093px;
		margin-left: -3px;
		margin-top: -10px;
		animation-delay: 0;
			-o-animation-delay: 0;
			-ms-animation-delay: 0;
			-webkit-animation-delay: 0;
			-moz-animation-delay: 0;
	}

	@keyframes cssload-rotate0 {
		0% {
			transform: rotate(0deg);
		}
		60% {
			transform: rotate(180deg);
		}
		100% {
			transform: rotate(180deg);
		}
	}

	@-o-keyframes cssload-rotate0 {
		0% {
			-o-transform: rotate(0deg);
		}
		60% {
			-o-transform: rotate(180deg);
		}
		100% {
			-o-transform: rotate(180deg);
		}
	}

	@-ms-keyframes cssload-rotate0 {
		0% {
			-ms-transform: rotate(0deg);
		}
		60% {
			-ms-transform: rotate(180deg);
		}
		100% {
			-ms-transform: rotate(180deg);
		}
	}

	@-webkit-keyframes cssload-rotate0 {
		0% {
			-webkit-transform: rotate(0deg);
		}
		60% {
			-webkit-transform: rotate(180deg);
		}
		100% {
			-webkit-transform: rotate(180deg);
		}
	}

	@-moz-keyframes cssload-rotate0 {
		0% {
			-moz-transform: rotate(0deg);
		}
		60% {
			-moz-transform: rotate(180deg);
		}
		100% {
			-moz-transform: rotate(180deg);
		}
	}

	@keyframes cssload-rotate90 {
		0% {
			transform: rotate(90deg);
							transform: rotate(90deg);
		}
		60% {
			transform: rotate(270deg);
							transform: rotate(270deg);
		}
		100% {
			transform: rotate(270deg);
							transform: rotate(270deg);
		}
	}

	@-o-keyframes cssload-rotate90 {
		0% {
			-o-transform: rotate(90deg);
							transform: rotate(90deg);
		}
		60% {
			-o-transform: rotate(270deg);
							transform: rotate(270deg);
		}
		100% {
			-o-transform: rotate(270deg);
							transform: rotate(270deg);
		}
	}

	@-ms-keyframes cssload-rotate90 {
		0% {
			-ms-transform: rotate(90deg);
							transform: rotate(90deg);
		}
		60% {
			-ms-transform: rotate(270deg);
							transform: rotate(270deg);
		}
		100% {
			-ms-transform: rotate(270deg);
							transform: rotate(270deg);
		}
	}

	@-webkit-keyframes cssload-rotate90 {
		0% {
			-webkit-transform: rotate(90deg);
							transform: rotate(90deg);
		}
		60% {
			-webkit-transform: rotate(270deg);
							transform: rotate(270deg);
		}
		100% {
			-webkit-transform: rotate(270deg);
							transform: rotate(270deg);
		}
	}

	@-moz-keyframes cssload-rotate90 {
		0% {
			-moz-transform: rotate(90deg);
							transform: rotate(90deg);
		}
		60% {
			-moz-transform: rotate(270deg);
							transform: rotate(270deg);
		}
		100% {
			-moz-transform: rotate(270deg);
							transform: rotate(270deg);
		}
	}

	@keyframes cssload-rotate45 {
		0% {
			transform: rotate(45deg);
							transform: rotate(45deg);
		}
		60% {
			transform: rotate(225deg);
							transform: rotate(225deg);
		}
		100% {
			transform: rotate(225deg);
							transform: rotate(225deg);
		}
	}

	@-o-keyframes cssload-rotate45 {
		0% {
			-o-transform: rotate(45deg);
							transform: rotate(45deg);
		}
		60% {
			-o-transform: rotate(225deg);
							transform: rotate(225deg);
		}
		100% {
			-o-transform: rotate(225deg);
							transform: rotate(225deg);
		}
	}

	@-ms-keyframes cssload-rotate45 {
		0% {
			-ms-transform: rotate(45deg);
							transform: rotate(45deg);
		}
		60% {
			-ms-transform: rotate(225deg);
							transform: rotate(225deg);
		}
		100% {
			-ms-transform: rotate(225deg);
							transform: rotate(225deg);
		}
	}

	@-webkit-keyframes cssload-rotate45 {
		0% {
			-webkit-transform: rotate(45deg);
							transform: rotate(45deg);
		}
		60% {
			-webkit-transform: rotate(225deg);
							transform: rotate(225deg);
		}
		100% {
			-webkit-transform: rotate(225deg);
							transform: rotate(225deg);
		}
	}

	@-moz-keyframes cssload-rotate45 {
		0% {
			-moz-transform: rotate(45deg);
			transform: rotate(45deg);
		}
		60% {
			-moz-transform: rotate(225deg);
			transform: rotate(225deg);
		}
		100% {
			-moz-transform: rotate(225deg);
			transform: rotate(225deg);
		}
	}

	@keyframes cssload-rotate135 {
		0% {
			transform: rotate(135deg);
			transform: rotate(135deg);
		}
		60% {
			transform: rotate(315deg);
			transform: rotate(315deg);
		}
		100% {
			transform: rotate(315deg);
			transform: rotate(315deg);
		}
	}

	@-o-keyframes cssload-rotate135 {
		0% {
			-o-transform: rotate(135deg);
			transform: rotate(135deg);
		}
		60% {
			-o-transform: rotate(315deg);
			transform: rotate(315deg);
		}
		100% {
			-o-transform: rotate(315deg);
			transform: rotate(315deg);
		}
	}

	@-ms-keyframes cssload-rotate135 {
		0% {
			-ms-transform: rotate(135deg);
							transform: rotate(135deg);
		}
		60% {
			-ms-transform: rotate(315deg);
							transform: rotate(315deg);
		}
		100% {
			-ms-transform: rotate(315deg);
							transform: rotate(315deg);
		}
	}

	@-webkit-keyframes cssload-rotate135 {
		0% {
			-webkit-transform: rotate(135deg);
							transform: rotate(135deg);
		}
		60% {
			-webkit-transform: rotate(315deg);
							transform: rotate(315deg);
		}
		100% {
			-webkit-transform: rotate(315deg);
							transform: rotate(315deg);
		}
	}

	@-moz-keyframes cssload-rotate135 {
		0% {
			-moz-transform: rotate(135deg);
							transform: rotate(135deg);
		}
		60% {
			-moz-transform: rotate(315deg);
							transform: rotate(315deg);
		}
		100% {
			-moz-transform: rotate(315deg);
							transform: rotate(315deg);
		}
	}

	@keyframes cssload-loader {
		0% {
			transform: rotate(0deg);
							transform: rotate(0deg);
		}
		100% {
			transform: rotate(360deg);
							transform: rotate(360deg);
		}
	}

	@-o-keyframes cssload-loader {
		0% {
			-o-transform: rotate(0deg);
							transform: rotate(0deg);
		}
		100% {
			-o-transform: rotate(360deg);
							transform: rotate(360deg);
		}
	}

	@-ms-keyframes cssload-loader {
		0% {
			-ms-transform: rotate(0deg);
							transform: rotate(0deg);
		}
		100% {
			-ms-transform: rotate(360deg);
							transform: rotate(360deg);
		}
	}

	@-webkit-keyframes cssload-loader {
		0% {
			-webkit-transform: rotate(0deg);
							transform: rotate(0deg);
		}
		100% {
			-webkit-transform: rotate(360deg);
							transform: rotate(360deg);
		}
	}

	@-moz-keyframes cssload-loader {
		0% {
			-moz-transform: rotate(0deg);
							transform: rotate(0deg);
		}
		100% {
			-moz-transform: rotate(360deg);
							transform: rotate(360deg);
		}
	}
</style>
@endsection
@section('content')
@if (Session::has('alerts'))
	@foreach(Session::get('alerts') as $alert)
		<div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
	@endforeach
@endif
<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-md-12">
			<h2 class="page-title">Upload PO</h2>
			<div class="card-deck" style="display: block">
				<div class="col-md-12">
					<form id="form_po" class="row" method="post" enctype="multipart/form-data">
						{{ csrf_field() }}
						<div class="card shadow mb-4">
							<div class="card-body">
								<div class="row">
									<div class="form-group mb-3 col-md-3">
										<label class="control-label pull-right" for="no_sp">Nomor SP</label>
										<input type="text" id="no_sp" name="no_sp" class="form-control input-transparent" value="{{ @$data->no_sp }}">
									</div>
									<div class="form-group mb-3 col-md-3">
										<label class="control-label pull-right" for="tgl_sp">Tanggal SP</label>
										<input rows="2" style="resize:none;" cols="50" id="tgl_sp" name="tgl_sp" class="form-control input-transparent date-picker2" value="{{ @$data->tgl_sp }}">
									</div>
									<div class="form-group mb-3 col-md-3">
										<label class="control-label pull-right" for="no_po">Nomor PO</label>
										<input type="text" id="no_po" name="no_po" class="form-control input-transparent" value="{{ @$data->no_po }}">
									</div>
									<div class="form-group mb-3 col-md-3">
										<label class="control-label pull-right" for="bulan">Bulan Terbit</label>
										<input rows="2" style="resize:none;" cols="50" id="bulan" name="bulan" class="form-control input-transparent date-picker" value="{{ @$data->bulan_terbit }}">
									</div>
								</div>
								<div class="row border-top py-3">
									<div class="form-group mb-1 col-md-4">
										<label class="col-form-label pull-right" for="upload_po">Upload PO</label>
										<input type="file" id="upload_po" required name="upload_po" accept="application/pdf" class="form-control-file">
									</div>
									<div class="form-group mb-1 col-md-4">
										<label class="control-label pull-right" for="nilai_po">Nilai PO</label>
										<input type="text" id="nilai_po" name="nilai_po" class="form-control input-transparent price" value="{{ @$data->nilai_po }}">
									</div>
									<div class="form-group mb-1 col-md-4">
										<label class="col-form-label pull-right" for="upload_sp">Upload SP</label>
										<input type="file" id="upload_sp" required name="upload_sp" accept="application/pdf" class="form-control-file">
									</div>
								</div>
								<div class="form-group row border-top py-3">
                  <label class="control-label col-md-2 pull-right" for="tematik">Tematik</label>
                  <div class="col-md-10">
                    <select class="form-control select2" id="tematik" name="tematik">
										</select>
										<div class="ldg">
											<div class="cssload-side"></div>
											<div class="cssload-side"></div>
											<div class="cssload-side"></div>
											<div class="cssload-side"></div>
											<div class="cssload-side"></div>
											<div class="cssload-side"></div>
											<div class="cssload-side"></div>
											<div class="cssload-side"></div>
										</div>
                  </div>
                </div>
								<div class="col-md-12">
									<div class='card shadow mb-4'>
										<div class='card-body table-responsive'>
											<table>
												<table class='table table-striped table-bordered table-hover' style='width:100%'>
													<thead class='thead-dark'>
														<th>Project Yang Dipilih</th>
													</thead>
													<tbody class="project_selected"></tbody>
												</table>
											</table>
										</div>
									</div>
                </div>
								<input type="hidden" name="pro_id_input">
								<div id="panel_data"></div>
								<div class="form-group mb-3">
									<div class="custom-file">
										<button type="submit" class="btn btn-block btn_submit btn-primary"><i class="fe fe-download fe-16"></i>&nbsp;Upload PO</button>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('footerS')
<script src="/js/moment.min.js"></script>
<script src='/js/bootstrap-datepicker.min.js'></script>
<script src='/js/jquery.timepicker.js'></script>
<script src='/js/select2.min.js'></script>
<script src='/js/jquery.dataTables.min.js'></script>
<script src='/js/dataTables.bootstrap4.min.js'></script>
<script type="text/javascript">
	$(function(){
		$('.price').val(function(index, value) {
			return value.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".");
		});

		$("#tematik").select2({
			width: '100%',
			placeholder: 'Silahkan Pilih Tematik',
			data:[
				{
					id: '1',
					text: 'FTTH',
					children: [
						{ id: 'OSP', text: 'OSP-FO' },
						{ id: 'NODE', text: 'NODE-B' },
						{ id: 'PT2', text: 'PT-2' },
						{ id: 'FEEDER', text: 'FEEDER' },
					]
				},
				{
					id: '2',
					text: 'QE',
					children: [
						{ id: 'AKSES', text: 'AKSES' },
						{ id: 'RECOVER', text: 'RECOVER' },
						{ id: 'RELOKASI', text: 'RELOKASI' },
						{ id: 'HEM', text: 'HEM' },
						{ id: 'FTM', text: 'FTM' },
					]
				},
				{
					id: '3',
					text: 'PSB',
					children: [
						{ id: 'HSI', text: 'HSI' },
						{ id: 'WMS', text: 'WMS' },
						{ id: 'CONSUMER', text: 'CONSUMER' },
					]
				},
			]
		});

		var load_data = {!! json_encode($data ?? null) !!},
		jenis_input = 'input';

		if(load_data && !$.isEmptyObject(load_data) ){
			jenis_input = 'load';
		}

		$("#tematik").on('select2:select change', function(){
			var isi = $(this).val();
			$.ajax({
				url: "/get_ajx/get_tematik_proaktif",
				type: "GET",
				data: {
					isi: isi,
					jenis_input: jenis_input
				},
				dataType: 'json',
				beforeSend: function (res) {
					$('.ldg').toggleClass('cssload-loader')
					$('.container-fluid').toggleClass('opac')
				},
			}).fail(function(data){
				$('.ldg').toggleClass('cssload-loader')
				$('.container-fluid').toggleClass('opac')
				console.log(data);
			}).done(function(data){
				if(load_data && !$.isEmptyObject(load_data) ){
					var data_order = JSON.parse(load_data.id_proaktif_project),
					intersection = data.filter(o1 => data_order.some(o2 => o1.id === parseInt(o2) ) ),
					diff = data.filter(o1 => !data_order.some(o2 => o1.id === parseInt(o2) ) );

					$.each(intersection, function(k, v){
						intersection[k].selected = 'ya';
					})

					data = intersection.concat(diff);
				}

				$('.ldg').toggleClass('cssload-loader')
				$('.container-fluid').toggleClass('opac')
				table_temp = '';

				var delete_btn = '';

				table_temp += "<div class='col-md-12'>";
				table_temp += "<div class='card shadow mb-4'>";
				table_temp += "<div class='card-body table-responsive'>";
				table_temp += "<table class='table table_pro table-striped table-bordered table-hover' style='width:100%'>";
				table_temp += "<thead class='thead-dark'>";
				table_temp += "<tr>";
				table_temp += "<th>Nama Project</th>";
				table_temp += "<th>Jenis</th>";
				table_temp += "<th>PID</th>";
				table_temp += "<th>WBS</th>";
				table_temp += "<th>Action</th>";
				table_temp += "</tr>";
				table_temp += "</thead>";
				table_temp += "<tbody>";

				$.each(data, function(k, v){
					table_temp += "<tr>";
					table_temp += `<td>${v.nama_project}</td>`;
					table_temp += `<td>${v.portofolio}</td>`;
					table_temp += `<td>${v.pid}</td>`;
					table_temp += `<td>${v.sapid}</td><td>`;
					table_temp += "<div class='custom-control custom-checkbox'>";
					table_temp += `<input type='checkbox' class='custom-control-input check_project' `+ (v.selected && v.selected == 'ya' ? 'checked' : '') +` id='pilih_pro_${v.id}' name='pilih_pro[]' value='${v.id}'>`;
					table_temp += `<label class='custom-control-label' for='pilih_pro_${v.id}'>Pilih Project</label>`;
					table_temp += '</div>';

					table_temp += "</td></tr>";
				})
				table_temp += "</tbody>";
				table_temp += "</table>";
				table_temp += "</div>";
				table_temp += "</div>";
				table_temp += "</div>";

				$('#panel_data').html(table_temp);

				var isi_td = [],
				td_tbl_load = '';

				$.each($('.check_project'), function(k, v){
					var tulisan = $(this).parent().parent().parent().find('td').eq(0).text();

					if($(this).is(':checked') ){
						isi_td.push(tulisan);
					}

					if(!$(this).is(':checked') ){
						isi_td = isi_td.filter(function(elem){
							return elem != tulisan;
						});
					}
				});

				isi_td = [...new Set(isi_td)];

				$.each(isi_td, function(k, v){
					td_tbl_load += `<tr><td>${v}</td></tr>`;
				});

				$('.project_selected').html(td_tbl_load);

				$('.check_project').on('click', function(){
					var td_tbl = '';

					$.each($('.check_project'), function(k, v){
						var tulisan = $(this).parent().parent().parent().find('td').eq(0).text();

						if($(this).is(':checked') ){
							isi_td.push(tulisan);
						}

						if(!$(this).is(':checked') ){
							isi_td = isi_td.filter(function(elem){
								return elem != tulisan;
							});
						}
					});

					isi_td = [...new Set(isi_td)];

					$.each(isi_td, function(k, v){
						td_tbl += `<tr><td>${v}</td></tr>`;
					});

					$('.project_selected').html(td_tbl);
				})

				$('.table_pro').DataTable({
					autoWidth: true,
					columnDefs: [
						{
							targets: 'no-sort',
							orderable: false
						},
					],
					aaSorting: [],
					lengthMenu: [
						[16, 32, 64, -1],
						[16, 32, 64, "All"]
					]
				});
			});
		});

		$('#form_po').on('submit', function(e){
			var submit = [];
			$.each($("input[name='pilih_pro[]']"), function(k, v){
				if($(this).is(':checked') ){
					submit.push($(this).val());
				}
			});

			let hasil = JSON.stringify(submit);
			$("input[name='pro_id_input']").val(hasil)
		})

		if(load_data && !$.isEmptyObject(load_data) ){
			$("#tematik").val(load_data.tematik).change();
		}else{
			$("#tematik").val(null).change();
		}

		$('.price').keyup(function(event) {
			if(event.which >= 37 && event.which <= 40) return;

			$(this).val(function(index, value) {
				return value
				.replace(/\D/g, "")
				.replace(/\B(?=(\d{3})+(?!\d))/g, ".")
				;
			});
		});

    $('.date-picker').datepicker( {
      format: "yyyy-mm",
      startView: "months",
      minViewMode: "months",
      autoclose: true
    });

    $('.date-picker2').datepicker( {
      format: "yyyy-mm-dd",
      autoclose: true
    });
	})
</script>
@endsection