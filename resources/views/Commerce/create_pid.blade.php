@extends('layout')
@section('title', (empty($data) ? 'Create PID' : ((count($data) && $data['be'] == 0 )? 'Update PID' : (count($data) && $data['be'] == 1 ? 'Update Budget Exceeded' : '') ) ) )
@section('headerS')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="/css/dataTables.bootstrap4.css">
<link rel="stylesheet" href="/css/daterangepicker.css">
<style type="text/css">
	.pull-right {
		text-align: right;
	}
</style>
@endsection
@section('content')
@if (Session::has('alerts'))
  @foreach(Session::get('alerts') as $alert)
    <div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
  @endforeach
@endif
@php
  $prev_url = explode('/', url()->previous() );
  unset($prev_url[0]);
  unset($prev_url[1]);
  unset($prev_url[2]);

  $path = implode('/', $prev_url);
@endphp
<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-md-12">
			<h2 class="page-title">{{ (empty($data) ? 'Create PID' : ((count($data) && $data['be'] == 0 )? 'Update PID' : (count($data) && $data['be'] == 1 ? 'Update Budget Exceeded' : '') ) ) }}</h2>
			<div class="card-deck" style="display: block">
        <div class="col-md-12">
          <form class="row" method="post">
            {{ csrf_field() }}
            <input type="hidden" name="prev_url" value="{{ $path == '/comm/pid' ? '/Report/pid/list' : $path }}">
            <div class="card shadow mb-4">
              <div class="card-body">
                <div class="form-group row">
                  <label class="col-form-label col-md-2 pull-right" for="pid">PID</label>
                  <div class="col-md-10">
                    <input type="text" placeholder="Masukkan Nama PID" class="form-control input-transparent" name="pid" id="pid" required value="{{ @$data['pid'] }}">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-form-label col-md-2 pull-right" for="desc">Deskripsi</label>
                  <div class="col-md-10">
                    <textarea rows="2" placeholder="Deskripsi PID" style="resize: none;" class="form-control input-transparent" name="desc" id="desc" required>{{ @$data['keterangan'] }}</textarea>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-form-label col-md-2 pull-right" for="mitra_id">Mitra</label>
                  <div class="col-md-10">
                    <select id="mitra_id" name="mitra_id"></select>
                  </div>
                </div>
                <div class="form-group row">
									<label class="col-form-label col-md-2 pull-right" for="pid_be">Status Budget:</label>
                  <div class="col-md-10">
										<div class="custom-control custom-radio">
											<input type="radio" id="ada_budget" name="status_pid" class="custom-control-input ada_budget status_pid" value="ADA BUDGET" checked>
											<label class="custom-control-label" for="ada_budget">Ada Budget</label>
										</div>
									</div>
									<div class="offset-md-2 col-md-10">
										<div class="custom-control custom-checkbox">
											<input type="checkbox" id="be_mm" name="be_mm" class="custom-control-input be_data be_mm" value="01-01-02">
											<label class="custom-control-label" for="be_mm">Budget Exceeded Material Mitra</label>
										</div>
									</div>
									<div class="offset-md-2 col-md-10">
										<div class="custom-control custom-checkbox">
											<input type="checkbox" id="be_mt" name="be_mt" class="custom-control-input be_data be_mt" value="01-01-01">
											<label class="custom-control-label" for="be_mt">Budget Exceeded Material TA</label>
										</div>
									</div>
									<div class="offset-md-2 col-md-10">
										<div class="custom-control custom-checkbox">
											<input type="checkbox" id="bej" name="bej" class="custom-control-input be_data bej" value="01-02-01">
											<label class="custom-control-label" for="bej">Budget Exceeded Jasa</label>
										</div>
									</div>
								</div>
                {{-- @if (@$data['budget'])
                  <div class="form-group row">
                    <label class="col-form-label col-md-2 pull-right" for="harga_skrg">Budget Sebelumnya</label>
                    <div class="col-md-10">
                      <input type="text" class="form-control input-transparent price" name="harga_skrg" id="harga_skrg" disabled value="{{ @$data['budget'] }}">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-form-label col-md-2 pull-right" for="nominal_harga">Tambah Budget</label>
                    <div class="col-md-10">
                      <input type="text" class="form-control input-transparent price" name="nominal_harga" id="nominal_harga" required value="{{ @$data['jumlah'] }}">
                    </div>
                  </div>
                @else
                  <div class="form-group row kolom_budget">
                    <label class="col-form-label col-md-2 pull-right" for="nominal_harga">Nominal Budget</label>
                    <div class="col-md-10">
                      <input type="text" class="form-control input-transparent price" name="nominal_harga" id="nominal_harga" required value="{{ @$data['jumlah'] }}">
                    </div>
                  </div>
                @endif --}}
                @if ($data)
                  <div class="form-group row kolom_budget" style="display: none;">
                    <label class="col-form-label col-md-2 pull-right" for="nominal_harga">Nominal Budget</label>
                    <div class="col-md-10">
                      <input type="text" class="form-control input-transparent price" name="nominal_harga" id="nominal_harga" disabled value="{{ $data['jumlah'] }}">
                    </div>
                  </div>
                @endif
                <div class="form-group mb-3">
                  <div class="custom-file">
                    <button type="submit" class="btn btn-block btn-primary"><i class="fe fe-tool fe-16"></i>&nbsp;{{ (empty(@$data) ? 'Create PID' : ( (count(@$data) && @$data['be'] == 0 )? 'Update PID' : (count(@$data) && @$data['be'] == 1 ? 'Update Budget Exceeded' : '') ) ) }}</button>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
		  </div>
      @if($history_be)
        <div class="card-deck" style="display: block">
          <div class="col-md-12">
            {{-- <h6 class="mb-3">Tabel Produktifitas Tim</h6> --}}
            <div class="table-responsive">
              <table class="table table-bordered table-striped" style="width: 100%">
                <thead class="thead-dark">
                  <tr>
                    <th>Judul Pekerjaan</th>
                    <th>Jenis Pekerjaan</th>
                    <th>Tanggal Buat</th>
                    <th>Nilai Pekerjaan SP</th>
                    <th>Nilai Pekerjaan REKON</th>
                    <th>Status</th>
                    <th>Detail</th>
                    <th>Download BOQ</th>
                  </tr>
                </thead>
                <tbody>
                  @forelse ($history_be as $val)
                    @php
                      $total_sp = $val->total_material_sp + $val->total_jasa_sp;
                    @endphp
                    <tr>
                      <td>{{ $val->judul }}</td>
                      <td>{{ $val->pekerjaan }} | {{ $val->jenis_work }} | {{ $val->jenis_kontrak }}</td>
                      <td>{{ $val->created_at }}</td>
                      <td>Rp. {{ number_format($total_sp, 0, '.', '.') }}</td>
                      <td>Rp. {{ number_format($val->total_material_rekon + $val->total_jasa_rekon, 0, '.', '.') }}</td>
                      <td>{{ $val->status_budget }}</td>
                      <td><a class="btn btn-primary btn-sm" href="/get_detail_laporan/{{$val->id}}" target="_blank" style="color: #fff" data-id="{{ $val->id }}">Detail</a></td>
                      <td><a class="btn btn-primary btn-sm" href="/download/boq_op/{{$val->id}}" style="color: #fff" data-id="{{ $val->id }}">BOQ</a></td>
                    </tr>
                  @empty
                  @endforelse
                </tbody>
              </table>
            </div>
          </div>
        </div>
      @endif
	  </div>
  </div>
</div>
@endsection
@section('footerS')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src='/js/jquery.dataTables.min.js'></script>
<script src='/js/dataTables.bootstrap4.min.js'></script>
<script src="/js/moment.min.js"></script>
<script src='/js/jquery.timepicker.js'></script>
<script src='/js/daterangepicker.js'></script>
<script type="text/javascript">
	$(function(){
    $(".be_data").on('click', function(){
      $('.status_pid').prop('checked', false);
    });

    $(".ada_budget").on('click', function(){
      $('.be_data').prop('checked', false);
    });

    $('.price').val(function(index, value) {
      return value
      .replace(/\D/g, "")
      .replace(/\B(?=(\d{3})+(?!\d))/g, ".")
      ;
    });

    // $('.status_pid').change(function() {
    //   if($(this).val() == 'BE'){
    //     $('.kolom_budget').hide();
    //     // $('#nominal_harga').val('');
    //   }else{
    //     $('.kolom_budget').show();
    //   }
    // });

    $('.price').keyup(function(event) {
      if(event.which >= 37 && event.which <= 40) return;
      $(this).val(function(index, value) {
        return value
        .replace(/\D/g, "")
        .replace(/\B(?=(\d{3})+(?!\d))/g, ".")
        ;
      });
    });

    $('select[id="mitra_id"]').select2({
			width: '100%',
			placeholder: "Masukkan Nama Perusahaan Mitra",
			allowClear: true,
			ajax: {
				url: "/get_ajx/mitra/search/per_witel",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						searchTerm: params.term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true,
			}
		});

    var data_pid_mitra = {!! json_encode($data_pid_mitra ?? null) !!}
    // console.log(data_pid_mitra)
    if(data_pid_mitra){
      var newOption = new Option(data_pid_mitra.nama_company, data_pid_mitra.id_company, false, false);
      $('select[id="mitra_id"]').append(newOption).trigger('change');
    }

		$('.date-picker').daterangepicker(
      {
        singleDatePicker: true,
        timePicker: false,
        showDropdowns: true,
        locale:
        {
					format: 'YYYY-MM-DD',
        }
      }
		);

		$('#tb_rekon').DataTable({
			autoWidth: true,
			columnDefs: [
				{
					targets: 'no-sort',
					orderable: false
				}
			],
			lengthMenu: [
				[16, 32, 64, -1],
				[16, 32, 64, "All"]
			]
		});
	});
</script>
@endsection