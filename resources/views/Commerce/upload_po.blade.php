@extends('layout')
@section('title', 'Daftar PO Upload')
@section('headerS')
<link rel="stylesheet" href="/css/dataTables.bootstrap4.css">
<style>
	th, td {
		text-align: center;
	}
</style>
@endsection
@section('content')
<div class="container-fluid">
	@if (Session::has('alerts'))
		@foreach(Session::get('alerts') as $alert)
			<div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
		@endforeach
	@endif
	<div class="row justify-content-center">
		<div class="col-12">
			<div class="row">
				<div class="col-md-12 my-4">
					<div class="card shadow mb-4">
						<div class="card-body table-responsive">
							<h5 class="card-title">Daftar Upload PO</h5>
							<a type="button" href="/comm/insert/po" class="btn btn-primary pra_kontrak"><i class="fe fe-plus-square"></i>&nbsp; Tambah PO Baru</a>
							<br /><br />
							<table id="tb_mitra" class="table table-striped table-bordered table-hover">
								<thead class="thead-dark">
									<tr>
										<th class="hidden-xs">#</th>
										<th>Nomor SP</th>
										<th>Tanggal Sp</th>
										<th>Bulan Pekerjaan</th>
										<th>Nilai PO</th>
										<th>File</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody id="data_table">
									@php
									$num = 1;
									@endphp
									@foreach($data as $d)
									<tr>
										<td class="hidden-xs">{{ $num++ }}</td>
										<td>{{ $d->no_sp }}</td>
										<td>{{ $d->tgl_sp }}</td>
										<td>{{ $d->bulan_terbit }}</td>
										<td>Rp. {{ number_format($d->nilai_po) }}</td>
										<td>
											@php
                        $paths = public_path()."/upload/Commerce/{$d->id}";
                        $file_po = @preg_grep('~^Upload PO.*$~', scandir($paths.'/PO') );
                        $file_sp = @preg_grep('~^Upload SP.*$~', scandir($paths.'/SP') );
                        $path = null;
                      @endphp
                      @if (count($file_po) != 0)
                        @php
                          $files_po = array_values($file_po);
                          $path = "/upload/Commerce/" . $d->id . "/PO/" . $files_po[0];
                        @endphp
                        <a href="{{ $path }}">
                          <h5><span class="badge badge-pill badge-info"><i class="fe fe-download"></i>&nbsp;Download PO</span></h5>
                        </a>
                      @endif

                      @if (count($file_sp) != 0)
                        @php
                          $files_sp = array_values($file_sp);
                          $path = "/upload/Commerce/" . $d->id . "/PO/" . $files_sp[0];
                        @endphp
                        <a href="{{ $path }}">
                          <h5><span class="badge badge-pill badge-primary"><i class="fe fe-download"></i>&nbsp;Download SP</span></h5>
                        </a>
                      @endif
										</td>
										<td><a href="/comm/edit/po/{{ $d->id }}"><h5><span class="badge badge-pill badge-primary"><i class="fe fe-tool"></i>&nbsp;Edit</span></h5></a></td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('footerS')
<script src='/js/jquery.dataTables.min.js'></script>
<script src="/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript">
	$(function(){
		$('#tb_mitra').DataTable();
	});
</script>
@endsection