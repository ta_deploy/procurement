@extends('layout')
@section('title', 'Approve Budget')
@section('headerS')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="/css/dataTables.bootstrap4.css">
<link rel="stylesheet" href="/css/daterangepicker.css">
<style type="text/css">
	.pull-right {
		text-align: right;
	}
</style>
@endsection
@section('content')
@if (Session::has('alerts'))
  @foreach(Session::get('alerts') as $alert)
    <div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
  @endforeach
@endif
<div class="modal fade" id="detail_be" tabindex="-1" role="dialog" aria-labelledby="judul_mdl" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body data_mdl" style=" overflow: hidden;">
        <form class="form-horizontal form-label-right" method="POST" action="/comm/rekon_be/{{ Request::segment(2) }}">
					<div class="col-md-12">
						<section class="widget">
							<header>
								<h4>
									<i class="fa fa-check-square-o"></i>
									Form Budget Exceeded
								</h4>
							</header>
							<fieldset>
								{{ csrf_field() }}
                <input type="hidden" name="lokasi_get" id="lokasi_get">
								<div class="form-group">
									<label class="col-form-label col-md-2" for="pid_be">PID:</label>
									<div class="col-md-12">
										<select class="form-control pid_be" id="pid_be" multiple name="pid_be[]" required>
											{{-- @foreach ($get_pid as $v)
												<option value="{{ $v }}">{{ $v }}</option>
											@endforeach --}}
										</select>
									</div>
								</div>
								{{-- <div class="form-group">
									<label class="col-form-label col-md-2" for="pid_be">Status PID:</label>
									<div class="col-md-12">
											<div class="custom-control custom-radio">
													<input type="radio" id="BE" name="status_pid" class="custom-control-input" value="BE">
													<label class="custom-control-label" for="BE" checked>Budget Exceeded</label>
											</div>
									</div>
									<div class="col-md-12">
											<div class="custom-control custom-radio">
													<input type="radio" id="salah_pid" name="status_pid" class="custom-control-input" value="SALAH PID">
													<label class="custom-control-label" for="salah_pid">Salah PID</label>
											</div>
									</div>
								</div> --}}
								<input type="hidden" id="BE" name="status_pid" value="BE">
								<div class="form-group">
									<label class="col-form-label col-md-2" for="detail">Catatan:</label>
									<div class="col-md-12">
										<textarea style="resize:none; font-weight: bold" required cols='50' rows="2" class="form-control input-transparent" id="detail" name="detail"></textarea>
									</div>
								</div>
                <div class="form-actions">
                  <div class="row">
                    <div class="col-md-12">
                      <button type="submit" class="btn btn-block btn-danger">Submit PID</button>
                    </div>
                  </div>
                </div>
							</fieldset>
						</section>
					</div>
				</form>
			</div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-md-12">
			<h2 class="page-title">Approve Budget</h2>
			<div class="card-deck" style="display: block">
        <form class="row" method="post">
          {{ csrf_field() }}
          <div class="card shadow mb-4">
            <div class="card-header">
              <strong class="card-title">{{ $data_sp->judul }}</strong>
            </div>
            <div class="card-body">
              <div class="form-group row">
                <label class="col-form-label col-md-2 pull-right" for="work_jenis">Jenis Pekerjaan</label>
                <div class="col-md-10">
                  <input type="text" class="form-control input-transparent" name="work_jenis" id="work_jenis" value="{{ $data_sp->pekerjaan }} | {{ $data_sp->jenis_work }} | {{ $data_sp->jenis_kontrak }}" disabled>
                </div>
              </div>
              {{-- <div class="form-group row">
                <div class="col-md-12 offset-md-2">
                  <a class="btn btn-primary rar_down" id='rar_down' href="/download/full_rar/{{ $data_sp->id }}">Download Data Upload RAR</a>
                </div>
              </div> --}}
              <div class="form-group row">
                <label class="col-form-label col-md-2 pull-right" for="nominal_plan">Download BOQ</label>
                <div class="col-md-10">
                  <a type="button" class="btn btn-primary" href="/download/boq_op/{{ $data_sp->id }}">Download BOQ</a>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-form-label col-md-2 pull-right" for="created_at">Tanggal Dibuat</label>
                <div class="col-md-10">
                  <input type="text" class="form-control input-transparent" name="created_at" id="created_at" value="{{ $data_sp->created_at }} / {{ $data_sp->nama }} ({{ $data_sp->created_by }})" disabled>
                </div>
              </div>
              <div class="row border-top">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-form-label" for="nominal_plan">Nominal BOQ Plan</label>
                    <input type="text" class="form-control input-transparent price" name="nominal_plan" id="nominal_plan" value="{{ number_format($data_sp->gd_sp, 0, '.', '.') }}" disabled>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-form-label" for="nominal_rekon">Nominal BOQ Rekon</label>
                    <input type="text" class="form-control input-transparent price" name="nominal_rekon" id="nominal_rekon" value="{{ number_format($data_sp->gd_rekon, 0, '.', '.') }}" disabled>
                  </div>
                </div>
              </div>
              <div class="pid_bulk">
              </div>
              <div class="alert_danger"></div>
              <div class="form-group row">
                <a class="btn btn-info col-md-12 add_pid" style="color: white"><i class="fe fe-plus fe-16"></i>&nbsp;Tambah PID</a>
              </div>
              <div class="form-group mb-3 row" style="margin-top: 70px;">
                <div class="custom-file col-md-6">
                  <button type="submit" class="btn btn-block btn_submit btn-primary">Approve Budget</button>
                </div>
                @if ($data_sp->step_id != 28)
                  <div class="custom-file col-md-6">
							      <a class="btn btn-block btn-danger" type="button" data-toggle="modal" data-target="#detail_be" style="color: white;"><i class="fe fe-alert-triangle fe-16"></i>&nbsp;PID Over Budget</a>
                  </div>
                @endif
              </div>
            </div>
          </div>
        </form>
		  </div>
	  </div>
  </div>
</div>
@endsection
@section('footerS')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/js/select2.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src='/js/jquery.dataTables.min.js'></script>
<script src='/js/dataTables.bootstrap4.min.js'></script>
<script src="/js/moment.min.js"></script>
<script src='/js/jquery.timepicker.js'></script>
<script src='/js/daterangepicker.js'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.js" integrity="sha512-lbwH47l/tPXJYG9AcFNoJaTMhGvYWhVM9YI43CT+uteTRRaiLCui8snIgyAN8XWgNjNhCqlAUdzZptso6OCoFQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script type="text/javascript">
	$(function(){
    var data_load = [],
    wbs_id = {!! json_encode($get_wbs) !!},
    load_data = {!! json_encode($load_data) !!},
    prev_data = {!! json_encode(Session::get('alerts')[0]['load_data'] ) !!};

    if(prev_data !== null){
      load_data = prev_data;
    }

    $('#detail_be').on('shown.bs.modal', function (e) {
      var list_pid = [],
      grab_lokasi = [];

      $('.pid').each(function() {
        var lokasi = $(this).parent().parent().next().eq(0).find('select').val();
        isi_pid = $(this).val(),
        text_pid = $(this).text(),
        find_data = $.grep(load_data, function(e){ return e.id_pid_req == isi_pid; })[0];

        if(find_data){
          $.each(find_data.pid_sto.split(', '), function(k, v){
            list_pid.push({
              id: v,
              text: v
            });

            grab_lokasi.push({
              lokasi_id: lokasi,
              pid: v
            })
          });
        }else{
          find_data_exclude = $.grep(wbs_id, function(e){ return e.id == lokasi; })[0];
          $.each(find_data_exclude.idp.split(','), function(k, v){
            list_pid.push({
              id: `${text_pid}-${v}`,
              text: `${text_pid}-${v}`
            });

            grab_lokasi.push({
              lokasi_id: lokasi,
              pid: `${text_pid}-${v}`
            })
          });
        }
      });

      $('#lokasi_get').val(JSON.stringify(grab_lokasi) )

      $("#pid_be").select2({
        width: '100%',
        data: list_pid,
        placeholder: 'Silahkan Pilih PID WBS'
      })
      // console.log(list_pid )
    })

    function initializeSelect2(selectElementObj) {
      selectElementObj.select2({
        width: '100%',
        placeholder: "Ketik pid",
        theme: 'bootstrap4',
        allowClear: true,
        ajax: {
          url: '/get_ajx/get_data/pid/witel',
          dataType: 'json',
          type: "GET",
          delay: 50,
          data: function (term) {
            return {
              searchTerm: term.term,
              id_upload : {!! json_encode($data_sp->id) !!}
            };
          },
          processResults: function (data) {
            data_load = data;
            return {
              results: data
            };
          }
        },
        language: {
          noResults: function(){
            return "PID Tidak Ditemukan? Silahkan Input Di Proaktif dan Tungguh Besok!";
          }
        },
        escapeMarkup: function (markup) {
          return markup;
        }
      });
    }

    $('.price').val(function(index, value) {
      return value
      .replace(/\D/g, "")
      .replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    });

    $('.pid').each(function() {
      initializeSelect2($(this) );
    });

    var data_lokasi = {!! json_encode($lokasi) !!},
    sel2_lok = [];
    // console.log(data_lokasi)
    $.each(data_lokasi, function(key, val){
      no = ++key;
      sel2_lok.push({
        id: val.id,
        text: 'Kolom Baris ' + no + ' (Lokasi ' + val.lokasi + '/ STO ' + val.sto + '/ ' + val.sub_jenis_p + ')'
      })
    })

    $('.lokasi').select2({
      placeholder: 'Silahkan Pilih Data',
      data: sel2_lok,
      width: '100%'
    });

    $('.lokasi').val(null).change();

    var budget_left = 0;
    $(document).on('select2:select', '.lok', function(){
      var isi = $(this).val(),
      material = $(this).parent().parent().next().next().next().find('.material'),
      jasa = $(this).parent().parent().next().next().next().next().find('.jasa'),
      total = $(this).parent().parent().next().next().next().next().next().find('.total'),
      // c_budget = parseInt($(this).parent().parent().next().next().next().next().find('.budget').val('') ) || 0,
      save_data = [];

      $.each(data_lokasi, function(key, val){
        if(val.id == isi){
          material.val((val.total_material_rekon || 0).toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
          jasa.val((val.total_jasa_rekon || 0).toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, "."))
          total.val((val.gd_rekon || 0).toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, "."))
        }
      })

      $('.list_input_pid').each(function(k, v){
        var pid = $(this).children().eq(0).find('.pid').val(),
        lokasi = $(this).children().eq(1).find('.lok').val(),
        // budget = parseInt($(this).children().eq(5).find('.budget').val().split('.').join("") ) || 0;
        budget = 0;

        save_data.push({
          lokasi: lokasi,
          budget: budget
        });
      })

      var zz = 0;
      $.each(save_data, function(k, v){
        if(isi == v.lokasi)
        {
          zz += parseInt(v.budget) || 0;
        }
      })

      budget_left = total.val().split('.').join("") - zz
      $(this).parent().parent().next().next().next().next().find('.budget').val(budget_left.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
    });

    $(document).on('select2:select', '.pid', function(){
      // var lokasi = $(this).parent().parent().next().find('.lokasi'),
      // material = $(this).parent().parent().next().next().find('.material');
      // jasa = $(this).parent().parent().next().next().next().find('.jasa'),
      // total = $(this).parent().parent().next().next().next().next().find('.total'),
      // // budget = $(this).parent().parent().next().next().next().next().next().find('.budget'),
      // save_data = [];

      // lokasi.val(null).change()
      // material.val('')
      // jasa.val('')
      // total.val('')
      // // budget.val('')
      var selected = $.grep(data_load, function(e){ return e.selected == true; })[0];
      console.log(selected)
      mns = $(this).parent().parent().next().next().find('.mns'),
      ms = $(this).parent().parent().next().next().next().find('.ms');
      console.log($(this).parent().parent().next().next())
      mns.val(selected.keperluan["Material : Non Stock"]);
      ms.val(selected.keperluan["Material : Stock"]);
      // mns
      // ms
    });

    var no_pid_urut = 1;
    $(".add_pid").on('click', function(){
      toastr.options = {
				"closeButton": false,
				"debug": false,
				"newestOnTop": false,
				"progressBar": true,
				"positionClass": "toast-top-center",
				"preventDuplicates": false,
				"onclick": null,
				"showDuration": "300",
				"hideDuration": "1000",
				"timeOut": "5000",
				"extendedTimeOut": "1000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut"
			}
			toastr["success"]("Form Pengisian PID Ditambahkan!", "Sukses")

      var nomor = ++no_pid_urut;
      var html = "<div class='row'>";
      html += "<div class='col-md-12'>";
      html += "<div class='card shadow mb-4'>";
      html += "<div class='card-header'>";
      html += "<strong class='card-title'>PID Nomor " + nomor + ":</strong>";
      html += "</div>";
      html += "<div class='card-body list_input_pid'>";
      html += "<div class='form-group row'>";
      html += "<label class='col-form-label col-md-2 pull-right' for='pid'>PID</label>";
      html += "<div class='col-md-10'>";
      html += "<select required class='form-control required pid' name='pid[]'></select>";
      html += "</div>";
      html += "</div>";
      html += "<div class='form-group row'>";
      html += "<label class='col-form-label col-md-2 pull-right' for='lokasi'>Lokasi</label>";
      html += "<div class='col-md-10'>";
      html += "<select class='form-control lok lokasi lokasi"+nomor+"' name='lokasi[]'></select>";
      html += "</div>";
      html += "</div>";
      html += "<div class='form-group row'>";
      html += "<label class='col-form-label col-md-2 pull-right' for='mns'>Material : Non Stock</label>";
      html += "<div class='col-md-10'>";
      html += "<input type='text' class='form-control input-transparent mns' name='mns[]' disabled>";
      html += "</div>";
      html += "</div>";
      html += "<div class='form-group row'>";
      html += "<label class='col-form-label col-md-2 pull-right' for='ms'>Material : Stock</label>";
      html += "<div class='col-md-10'>";
      html += "<input type='text' class='form-control input-transparent ms' name='ms[]' disabled>";
      html += "</div>";
      html += "</div>";
      html += "<div class='form-group row'>";
      html += "<label class='col-form-label col-md-2 pull-right' for='material'>material</label>";
      html += "<div class='col-md-10'>";
      html += "<input type='text' class='form-control input-transparent material' name='material[]' disabled>";
      html += "</div>";
      html += "</div>";
      html += "<div class='form-group row'>";
      html += "<label class='col-form-label col-md-2 pull-right' for='jasa'>jasa</label>";
      html += "<div class='col-md-10'>";
      html += "<input type='text' class='form-control input-transparent jasa' name='jasa[]' disabled>";
      html += "</div>";
      html += "</div>";
      html += "<div class='form-group row'>";
      html += "<label class='col-form-label col-md-2 pull-right' for='total'>total</label>";
      html += "<div class='col-md-10'>";
      html += "<input type='text' class='form-control input-transparent total' name='total[]' disabled>";
      html += "</div>";
      html += "</div>";
      // html += "<div class='form-group row'>";
      // html += "<label class='col-form-label col-md-2 pull-right' for='budget'>Nilai Pekerjaan</label>";
      // html += "<div class='col-md-10'>";
      // html += "<input type='text' class='form-control input-transparent budget price' name='budget[]'"
      // html += "</div>";
      // html += "</div>";
      html += "<a class='btn btn-danger mb-2 hapus' style='color: white'>Hapus</a>";
      html += "</div>";
      html += "</div>";
      html += "</div>";
      html += "</div>";

      $('.pid_bulk').append(html)

      $('.hapus').on('click', function() {
        toastr.options = {
          "closeButton": false,
          "debug": false,
          "newestOnTop": false,
          "progressBar": false,
          "positionClass": "toast-top-right",
          "preventDuplicates": false,
          "onclick": null,
          "showDuration": "300",
          "hideDuration": "1000",
          "timeOut": "5000",
          "extendedTimeOut": "1000",
          "showEasing": "swing",
          "hideEasing": "linear",
          "showMethod": "fadeIn",
          "hideMethod": "fadeOut"
        }

        toastr["warning"]("Form Pengisian PID Berhasil Dihapus", "Warning")

        $(this).parent().parent().parent().parent().remove();
        --no;
        // console.log($(this).parent().parent().parent().parent() );
      });

      $('.lokasi' + nomor).select2({
        placeholder: 'Silahkan Pilih Data',
        data: sel2_lok,
        width: '100%'
      });


      $('.lokasi' + nomor).val(null).change();

      $('.pid').each(function() {
        initializeSelect2($(this) );
      });

    });

		$('.date-picker').daterangepicker(
      {
        singleDatePicker: true,
        timePicker: false,
        showDropdowns: true,
        locale:
        {
					format: 'YYYY-MM-DD',
        }
      }
		);

    $(document).on('keyup', '.price', function(){
      var c_lokasi = $(this).parent().parent().prev().prev().prev().prev().find('select').val(),
      c_total = parseInt($(this).parent().parent().prev().find('input').val().split('.').join("") ) || 0,
      save_data = [];

      $('.list_input_pid').each(function(k, v){
        var pid = $(this).children().eq(0).find('.pid').val(),
        lokasi = $(this).children().eq(1).find('.lok').val(),
        budget = 0;
        // budget = parseInt($(this).children().eq(5).find('.budget').val().split('.').join("") );

        save_data.push({
          lokasi: lokasi,
          budget: budget
        });
      })

      var zz = 0;
      $.each(save_data, function(k, v){
        if(c_lokasi == v.lokasi)
        {
          zz += parseInt(v.budget) || 0;
        }
      })
      var result = c_total - zz
      budget_left = result;
      // console.log(result, $(this).val().split('.').join(""), budget_left);

      if(budget_left < 0 ){
        pakai_ini = parseInt($(this).val().split('.').join("")) + (budget_left);
        // console.log(pakai_ini)
        $(this).val(pakai_ini.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") )
      }

      if(event.which >= 37 && event.which <= 40) return;
      $(this).val(function(index, value) {
        return value
        .replace(/\D/g, "")
        .replace(/\B(?=(\d{3})+(?!\d))/g, ".")
        ;
      });
    });

		$('#tb_rekon').DataTable({
			autoWidth: true,
			columnDefs: [
				{
					targets: 'no-sort',
					orderable: false
				}
			],
			lengthMenu: [
				[16, 32, 64, -1],
				[16, 32, 64, "All"]
			]
		});

    $('.btn_submit').on('click', function(e){
      var load_select = sel2_lok,
      load_lokasi = [],
      unused_lokasi = [];

      $('.lokasi').each(function(k, v){
        var isi_lokasi = parseInt($(this).val() ) || 0;
        if(isi_lokasi != 0 && load_lokasi.indexOf(isi_lokasi) === -1){
          load_lokasi.push(isi_lokasi)
        }else{
          console.log("This item already exists");
        }
      });

      $.each(load_select, function(k, v){
        if( ($.inArray(v.id, load_lokasi) ) == -1 ){
          unused_lokasi.push(v);
        }
      })

      if(unused_lokasi.length != 0){
        var psn_html = '';

        $.each(unused_lokasi, function(k, v){
          psn_html += `<div class="alert alert-danger">${v.text} Tidak Ada PID!!</div>`;
        });

        $('.alert_danger').html(psn_html);

        e.preventDefault();
      }

      // var no = 0,
      // count = 0;
      // $(".total").each(function() {
      //   ++count;
      //   var hgr = ($(this).val().trim() ? $(this).val() : '0.0');
      //   no += parseInt(hgr.split('.').join(''));
      // });
      // var no = no.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".")
      // let timerInterval
      // swal.fire({
      //   showClass: {
      //     popup: 'animate__animated animate__fadeInDown'
      //   },
      //   hideClass: {
      //     popup: 'animate__animated animate__fadeOutUp'
      //   },
      //   title: '<strong>Akumulasi Request Budget PID</strong>',
      //   timer: 4000,
      //   icon: 'info',
      //   html:
      //     'Diketahui, total budget sementara dari '+ count +' PID adalah Rp. ' + no,
      //   showCancelButton: true,
      //   focusConfirm: false,
      //   confirmButtonText:'<i class="fe fe-check fe-16"></i> Ok',
      //   confirmButtonColor: '#3085d6',
      //   cancelButtonText:'<i class="fe fe-alert-triangle fe-16"></i> Hitung Kembali',
      //   cancelButtonColor: '#d33',
      //   allowOutsideClick: false,
      //   allowEscapeKey: false,
      //   didOpen: () => {
      //     timerInterval = setInterval(() => {
      //       const left = (Swal.getTimerLeft() / 1000).toFixed(0)
      //       Swal.getConfirmButton().querySelector('i').textContent = left
      //       Swal.getConfirmButton().disabled = true;

      //       if(left == 0){
      //         Swal.stopTimer()
      //         clearInterval(timerInterval)
      //         Swal.getConfirmButton().querySelector('i').textContent = ''
      //         Swal.getConfirmButton().disabled = false;
      //       }
      //     }, 100)
      //   },
      //   willClose: () => {
      //     clearInterval(timerInterval)
      //   }
      // }).then((result) => {
      //   if (result.isConfirmed) {
      //     $('#form_req_pid').submit()
      //     Swal.fire('Disubmit!', '', 'success')
      //   }
      // })
    })

    $('.btn_budget_exce').on('click', function(e){
      e.preventDefault();
      let timerInterval
      swal.fire({
        showClass: {
          popup: 'animate__animated animate__fadeInDown'
        },
        hideClass: {
          popup: 'animate__animated animate__fadeOutUp'
        },
        title: '<strong>Budget Exceeded</strong>',
        icon: 'question',
        html:'Pastikan PID Budget Exceeded',
        showCancelButton: true,
        focusConfirm: false,
        confirmButtonText:'<i class="fe fe-check fe-16"></i> Ok',
        confirmButtonColor: '#3085d6',
        cancelButtonText:'<i class="fe fe-alert-triangle fe-16"></i> Cek Kembali',
        cancelButtonColor: '#d33',
        allowOutsideClick: false,
        allowEscapeKey: false,
      }).then((result) => {
        if (result.isConfirmed) {
          $('#form_req_pid').submit()
          Swal.fire('Disubmit!', '', 'success')
        }
      })
    })

    if(load_data.length)
    {
      var nomor_load = 0;
      $.each(load_data, function(k, v){
        nomor_load = ++k;
        var html = "<div class='row'>";
        html += "<div class='col-md-12'>";
        html += "<div class='card shadow mb-4'>";
        html += "<div class='card-header'>";
        html += "<strong class='card-title'>PID Nomor " + nomor_load + ":</strong>";
        html += "</div>";
        html += "<div class='card-body list_input_pid'>";
        html += "<div class='form-group row'>";
        html += "<label class='col-form-label col-md-2 pull-right' for='pid'>PID</label>";
        html += "<div class='col-md-10'>";
        html += "<select required class='form-control pid pid"+nomor_load+"' name='pid[]'></select>";
        html += "</div>";
        html += "</div>";
        html += "<div class='form-group row'>";
        html += "<label class='col-form-label col-md-2 pull-right' for='lokasi'>Lokasi</label>";
        html += "<div class='col-md-10'>";
        html += "<select class='form-control lok lokasi lokasi"+nomor_load+"' name='lokasi[]'></select>";
        html += "</div>";
        html += "</div>";
        html += "<div class='form-group row'>";
        html += "<label class='col-form-label col-md-2 pull-right' for='mns'>Material : Non Stock</label>";
        html += "<div class='col-md-10'>";
        html += "<input type='text' class='form-control input-transparent mns mns"+nomor_load+"' name='mns[]' disabled>";
        html += "</div>";
        html += "</div>";
        html += "<div class='form-group row'>";
        html += "<label class='col-form-label col-md-2 pull-right' for='ms'>Material : Stock</label>";
        html += "<div class='col-md-10'>";
        html += "<input type='text' class='form-control input-transparent ms ms"+nomor_load+"' name='ms[]' disabled>";
        html += "</div>";
        html += "</div>";
        html += "<div class='form-group row'>";
        html += "<label class='col-form-label col-md-2 pull-right' for='material'>material</label>";
        html += "<div class='col-md-10'>";
        html += "<input type='text' class='form-control input-transparent material material"+nomor_load+"' name='material[]' disabled>";
        html += "</div>";
        html += "</div>";
        html += "<div class='form-group row'>";
        html += "<label class='col-form-label col-md-2 pull-right' for='jasa'>jasa</label>";
        html += "<div class='col-md-10'>";
        html += "<input type='text' class='form-control input-transparent jasa jasa"+nomor_load+"' name='jasa[]' disabled>";
        html += "</div>";
        html += "</div>";
        html += "<div class='form-group row'>";
        html += "<label class='col-form-label col-md-2 pull-right' for='total'>total</label>";
        html += "<div class='col-md-10'>";
        html += "<input type='text' class='form-control input-transparent total total"+nomor_load+"' name='total[]' disabled>";
        html += "</div>";
        html += "</div>";
        // html += "<div class='form-group row'>";
        // html += "<label class='col-form-label col-md-2 pull-right' for='budget'>Nilai Pekerjaan</label>";
        // html += "<div class='col-md-10'>";
        // html += "<input type='text' class='form-control input-transparent budget budget"+nomor_load+" price' name='budget[]'"
        // html += "</div>";
        // html += "</div>";
        // if(nomor_load > 1){
        //   html += "<a class='btn btn-danger mb-2 hapus' style='color: white'>Hapus</a>";
        // }
        html += "</div>";
        html += "</div>";
        html += "</div>";
        html += "</div>";

        $('.pid_bulk').append(html)

        $('.lokasi' + nomor_load).select2({
          placeholder: 'Silahkan Pilih Data',
          data: sel2_lok,
          width: '100%'
        });
        // console.log(sel2_lok, v.id_lokasi)
        $('.pid').each(function() {
          initializeSelect2($(this) );
        });

        $('.lokasi'+nomor_load).val(v.id_lokasi).trigger('change');

        if(v.pid){
          $('.pid'+nomor_load).append(new Option(v.pid, v.pid, false, true) ).trigger('change');
        }

        $('.mns'+nomor_load).val(v.mns);
        $('.ms'+nomor_load).val(v.ms);
        $('.material'+nomor_load).val((v.total_material_rekon || 0).toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") );
        $('.jasa'+nomor_load).val((v.total_jasa_rekon || 0).toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") );
        $('.total'+nomor_load).val((v.gd_rekon || 0).toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") );
        $('.budget'+nomor_load).val((v.budget || 0).toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") );

      });

      no_pid_urut = load_data.length;

    }
    else
    {
      var nomor_default = 1;
      var html = "<div class='row'>";
      html += "<div class='col-md-12'>";
      html += "<div class='card shadow mb-4'>";
      html += "<div class='card-header'>";
      html += "<strong class='card-title'>PID Nomor " + nomor_default + ":</strong>";
      html += "</div>";
      html += "<div class='card-body list_input_pid'>";
      html += "<div class='form-group row'>";
      html += "<label class='col-form-label col-md-2 pull-right' for='pid'>PID</label>";
      html += "<div class='col-md-10'>";
      html += "<select required class='form-control pid' name='pid[]'></select>";
      html += "</div>";
      html += "</div>";
      html += "<div class='form-group row'>";
      html += "<label class='col-form-label col-md-2 pull-right' for='lokasi'>Lokasi</label>";
      html += "<div class='col-md-10'>";
      html += "<select class='form-control lok lokasi lokasi"+nomor_default+"' name='lokasi[]'></select>";
      html += "</div>";
      html += "</div>";
      html += "<div class='form-group row'>";
      html += "<label class='col-form-label col-md-2 pull-right' for='mns'>Material : Non Stock</label>";
      html += "<div class='col-md-10'>";
      html += "<input type='text' class='form-control input-transparent mns' name='mns[]' disabled>";
      html += "</div>";
      html += "</div>";
      html += "<div class='form-group row'>";
      html += "<label class='col-form-label col-md-2 pull-right' for='ms'>Material : Stock</label>";
      html += "<div class='col-md-10'>";
      html += "<input type='text' class='form-control input-transparent ms' name='ms[]' disabled>";
      html += "</div>";
      html += "</div>";
      html += "<div class='form-group row'>";
      html += "<label class='col-form-label col-md-2 pull-right' for='material'>material</label>";
      html += "<div class='col-md-10'>";
      html += "<input type='text' class='form-control input-transparent material' name='material[]' disabled>";
      html += "</div>";
      html += "</div>";
      html += "<div class='form-group row'>";
      html += "<label class='col-form-label col-md-2 pull-right' for='jasa'>jasa</label>";
      html += "<div class='col-md-10'>";
      html += "<input type='text' class='form-control input-transparent jasa' name='jasa[]' disabled>";
      html += "</div>";
      html += "</div>";
      html += "<div class='form-group row'>";
      html += "<label class='col-form-label col-md-2 pull-right' for='total'>total</label>";
      html += "<div class='col-md-10'>";
      html += "<input type='text' class='form-control input-transparent total' name='total[]' disabled>";
      html += "</div>";
      html += "</div>";
      // html += "<div class='form-group row'>";
      // html += "<label class='col-form-label col-md-2 pull-right' for='budget'>Nilai Pekerjaan</label>";
      // html += "<div class='col-md-10'>";
      // html += "<input type='text' class='form-control input-transparent budget price' name='budget[]'"
      // html += "</div>";
      // html += "</div>";
      html += "</div>";
      html += "</div>";
      html += "</div>";
      html += "</div>";

      $('.pid_bulk').append(html)

      $('.lokasi' + nomor_default).select2({
        placeholder: 'Silahkan Pilih Data',
        data: sel2_lok,
        width: '100%'
      });


      $('.lokasi' + nomor_default).val(null).change();

      $('.pid').each(function() {
        initializeSelect2($(this) );
      });
    }
	});
</script>
@endsection