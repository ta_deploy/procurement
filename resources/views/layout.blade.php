<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/favicon.ico">
    <title>@yield('title')</title>
    <!-- Simple bar CSS -->
    <link rel="stylesheet" href="/css/simplebar.css">
    <!-- Fonts CSS -->
    <link href="https://fonts.googleapis.com/css2?family=Overpass:ital,wght@0,100;0,200;0,300;0,400;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    <!-- Icons CSS -->
    <link rel="stylesheet" href="/css/feather.css">
    <!-- Date Range Picker CSS -->
    <!-- App CSS -->
    <link rel="stylesheet" href="/css/app-light.css" id="lightTheme">
    <link rel="stylesheet" href="/css/select2.css">
    <link rel="stylesheet" href="/css/uppy.min.css">
    <link rel="stylesheet" href="/css/jquery.steps.css">
    <link rel="stylesheet" href="/css/jquery.timepicker.css">
    <link rel="stylesheet" href="/css/quill.snow.css">
    <link rel="stylesheet" href="/css/app-dark.css" id="darkTheme" disabled>
    @yield('headerS')
  </head>
  <body class="vertical light">
    <div class="wrapper">
      <nav class="topnav navbar navbar-light">
        <button type="button" class="navbar-toggler text-muted mt-2 p-0 mr-3 collapseSidebar">
          <i class="fe fe-menu navbar-toggler-icon"></i>
        </button>
        {{-- <form class="form-inline mr-auto searchform text-muted">
          <input class="form-control mr-sm-2 bg-transparent border-0 pl-4 text-muted" type="search" placeholder="Type something..." aria-label="Search">
        </form> --}}
        <ul class="nav">
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle text-muted pr-0" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              {{-- <span class="avatar avatar-sm mt-2">
                <img src="https://apps.telkomakses.co.id/wimata/photo/crop_{{ session('auth')->id_karyawan }}.jpg" alt="..." class="avatar-img rounded-circle">
              </span> --}}
              &nbsp; {{ session('auth')->nama }}
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
              @if (in_array(session('auth')->proc_level, [2, 4, 99, 44]))
                <a class="dropdown-item" href="/tools/edit/mitra">Mitra</a>
              @endif
              <a class="dropdown-item" href="/setting_pekerjaan">KHS Pekerjaan</a>
              <a class="dropdown-item" href="/logout">Logout</a>
              {{-- <a class="dropdown-item" href="#">Activities</a> --}}
            </div>
          </li>
        </ul>
      </nav>
      <aside class="sidebar-left border-right bg-white shadow" id="leftSidebar" data-simplebar>
        <a href="#" class="btn collapseSidebar toggle-btn d-lg-none text-muted ml-2 mt-3" data-toggle="toggle">
          <i class="fe fe-x"><span class="sr-only"></span></i>
        </a>
        <nav class="vertnav navbar navbar-light">
          <!-- nav bar -->
          <div class="w-100 mb-4 d-flex">
            {{-- <a class="navbar-brand mx-auto mt-2 flex-fill text-center" href="/">
              <svg version="1.1" id="logo" class="navbar-brand-img brand-sm" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 120 120" xml:space="preserve">
                <g>
                  <polygon class="st0" points="78,105 15,105 24,87 87,87  " />
                  <polygon class="st0" points="96,69 33,69 42,51 105,51   " />
                  <polygon class="st0" points="78,33 15,33 24,15 87,15  " />
                </g>
              </svg>
            </a> --}}
            <a href="/"><img style="vertical-align:middle; width: 100%; height: auto;" src="/images/logo.png"></a>
          </div>
          <ul class="navbar-nav flex-fill w-100 mb-2">
            <li class="nav-item dropdown">
              <a href="#dashboard" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle nav-link">
                <i class="fe fe-settings fe-16"></i>
                <span class="ml-3 item-text">Dashboard</span>
              </a>
              <ul class="collapse list-unstyled pl-4 w-100 {{ in_array(Request::path(),['home','dashboard'])?'show':'' }}" id="dashboard">
                <li class="nav-item"><a class="nav-link pl-3" href="/home?witel={{ session('auth')->Witel_New }}&tgl_hidden=&mitra=all&khs=All">log</a></li>
                <li class="nav-item"><a class="nav-link pl-3" href="/dashboard">Pekerjaan</a></li>
                <li class="nav-item"><a class="nav-link pl-3" href="/table_sla?witel={{ session('auth')->Witel_New }}&tgl_hidden=&mitra=all&khs=All&view_table=all">SLA</a></li>
              </ul>
            </li>
          </ul>
          <p class="text-muted nav-heading mt-4 mb-1">
            <span>Rekon</span>
          </p>
          <ul class="navbar-nav flex-fill w-100 mb-2">
            @if (in_array(session('auth')->proc_level, [1, 2, 3, 4, 7, 99, 44]) && session('auth')->Witel_New)
              <li class="nav-item dropdown">
                <a href="#inisiasi" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle nav-link">
                  <i class="fe fe-file-text fe-16"></i>
                  <span class="ml-3 item-text">Inisiasi/Pra-Kerja&nbsp;({{ $countMenu->Inisiasi ?? 0 }})</span>
                </a>
                <ul class="collapse list-unstyled pl-4 w-100 {{ in_array(Request::path(),['progress/input','progressList/1','progressList/2','progressList/3','progressList/4','progressList/5'])?'show':'' }}" id="inisiasi">
                  @if (in_array(session('auth')->proc_level, [3, 4, 99, 44]))
                    <li class="nav-item"><a class="nav-link pl-3 {{ Request::path()=='progressList/input'?'active':'' }}" href="/progressList/input">Tambah Pekerjaan &nbsp;({{ $countMenu->operation_req_pid ?? 0 }})
                      <span class="badge badge-pill badge-warning">Operation</span></a>
                    </li>
                  @endif
                  @if (in_array(session('auth')->proc_level, [7, 4, 99, 44]))
                    <li class="nav-item">
                      <a class="nav-link pl-3 {{ Request::path()=='progressList/1'?'active':'' }}" href="/progressList/1">Input PID &nbsp;({{ $countMenu->commerce_create_pid ?? 0 }})<span class="badge badge-pill badge-danger">Commerce</span>
                      </a>
                    </li>
                  @endif
                  @if (in_array(session('auth')->proc_level, [3, 4, 99, 44]))
                    <li class="nav-item">
                      <a class="nav-link pl-3 {{ Request::path()=='progressList/2'?'active':'' }}" href="/progressList/2">
                        Unduh Justifikasi &nbsp;({{ $countMenu->operation_upload_justif ?? 0 }})<span class="badge badge-pill badge-warning">Operation</span>
                      </a>
                    </li>
                  @endif
                  @if (in_array(session('auth')->proc_level, [1, 4, 99, 44]))
                    <li class="nav-item">
                      <a class="nav-link pl-3 {{ Request::path()=='progressList/3'?'active':'' }}" href="/progressList/3">
                        Surat Penetapan &nbsp;({{ $countMenu->proca_s_pen ?? 0 }})<span class="badge badge-pill badge-info">Proc. Area</span>
                      </a>
                    </li>
                  @endif
                  @if (in_array(session('auth')->proc_level, [2, 4, 99, 44]))
                    <li class="nav-item">
                      <a class="nav-link pl-3 {{ Request::path()=='progressList/4'?'active':'' }}" href="/progressList/4">
                        Kesanggupan &nbsp;({{ $countMenu->mitra_sanggup ?? 0 }})<span class="badge badge-pill badge-primary">Mitra</span>
                      </a>
                    </li>
                  @endif
                  @if (in_array(session('auth')->proc_level, [1, 3, 4, 99, 44]))
                    <li class="nav-item">
                      <a class="nav-link pl-3 {{ Request::path()=='progressList/5' ? 'active':'' }}" href="/progressList/5">
                        Surat Pesanan &nbsp;({{ $countMenu->proca_sp ?? 0 }})<span class="badge badge-pill badge-info">Proc. Area</span> &nbsp; <span class="badge badge-pill badge-warning">Operation</span>
                      </a>
                    </li>
                  @endif
                </ul>
              </li>
            @endif
            @if (in_array(session('auth')->proc_level, [1, 2, 3, 4, 6, 7, 8, 99, 44]) && session('auth')->Witel_New)
              <li class="nav-item dropdown">
                <a href="#finishing" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle nav-link">
                  <i class="fe fe-flag fe-16"></i>
                  <span class="ml-3 item-text">Finising/Pasca-Kerja&nbsp;({{ $countMenu->Finishing ?? 0 }})</span>
                </a>
                <ul class="collapse list-unstyled pl-4 w-100 {{ in_array(Request::path(),['progressList/6','progressList/7','progressList/8','progressList/9','progressList/10','progressList/11','progressList/12','progressList/20'])?'show':'' }}" id="finishing">
                  @if (in_array(session('auth')->proc_level, [5, 2, 4, 99, 44]))
                    <li class="nav-item">
                      <a class="nav-link pl-3 {{ Request::path()=='progressList/6'?'active':'' }}" href="/progressList/6">
                        Input BOQ Rekon &nbsp;({{ $countMenu->mitra_boq_rek ?? 0 }})<span class="badge badge-pill badge-primary">Mitra</span>
                      </a>
                    </li>
                  @endif
                  @if (in_array(session('auth')->proc_level, [3, 4, 99, 44]))
                    <li class="nav-item">
                      <a class="nav-link pl-3 {{ Request::path()=='progressList/7'?'active':'' }}" href="/progressList/7">
                        Verifikasi BOQ Rekon &nbsp;({{ $countMenu->operation_verif_boq ?? 0 }})<span class="badge badge-pill badge-warning">Operation</span>
                      </a>
                    </li>
                  @endif
                  @if (in_array(session('auth')->proc_level, [2, 4, 99, 44]))
                    <li class="nav-item">
                      <a class="nav-link pl-3 {{ Request::path()=='progressList/8'?'active':'' }}" href="/progressList/8">
                        Pelurusan RFC &nbsp;({{ $countMenu->mitra_pelurusan_material ?? 0 }})<span class="badge badge-pill badge-primary">Mitra</span>
                      </a>
                    </li>
                  @endif
                  @if (in_array(session('auth')->proc_level, [7, 4, 99, 44]))
                    <li class="nav-item">
                      <a class="nav-link pl-3 {{ Request::path()=='progressList/9'?'active':'' }}" href="/progressList/9">
                        Budgeting &nbsp;({{ $countMenu->commerce_budget ?? 0 }})<span class="badge badge-pill badge-danger">Commerce</span>
                      </a>
                    </li>
                  @endif
                  @if (in_array(session('auth')->proc_level, [1, 4, 99, 44]))
                    <li class="nav-item">
                      {{-- <a class="nav-link pl-3 {{ Request::path()=='Mitra/list_kelengkapan/rekon'?'active':'' }}" href="/Mitra/list_kelengkapan/rekon"> --}}
                      <a class="nav-link pl-3 {{ Request::path()=='progressList/10'?'active':'' }}" href="/progressList/10">
                        Input Nomor BA &nbsp;({{ $countMenu->proca_ver_doc_area ?? 0 }})<span class="badge badge-pill badge-info">Proc. Area</span>
                      </a>
                    </li>
                  @endif
                  @if (in_array(session('auth')->proc_level, [2, 4, 99, 44]))
                    <li class="nav-item">
                      {{-- <a class="nav-link pl-3 {{ Request::path()=='Admin/verify/boq'?'active':'' }}" href="/Admin/verify/boq"> --}}
                      <a class="nav-link pl-3 {{ Request::path()=='progressList/11'?'active':'' }}" href="/progressList/11">
                        Input kelengkapan Nomor &nbsp;({{ $countMenu->mitra_lengkapi_nomor ?? 0 }})<span class="badge badge-pill badge-primary">Mitra</span>
                      </a>
                    </li>
                  @endif
                  @if (in_array(session('auth')->proc_level, [1, 2, 4, 99, 44]))
                    <li class="nav-item">
                      {{-- <a class="nav-link pl-3 {{ Request::path()=='Mitra/list/uploadTD'?'active':'' }}" href="/Mitra/list/uploadTD"> --}}
                      <a class="nav-link pl-3 {{ Request::path()=='progressList/12'?'active':'' }}" href="/progressList/12">
                        Upload Dokumen TTD &nbsp;({{ $countMenu->mitra_ttd ?? 0 }})<span class="badge badge-pill badge-primary">Mitra</span><span class="badge badge-pill badge-info">Proc. Area</span>
                      </a>
                    </li>
                  @endif
                  @if (in_array(session('auth')->proc_level, [6, 4, 99, 44]))
                    <li class="nav-item">
                      <a class="nav-link pl-3 {{ Request::path()=='progressList/20'?'active':'' }}" href="/progressList/20">
                        Verifikasi Dokumen Rekon &nbsp;({{ $countMenu->procg_ver_doc_reg ?? 0 }})<span class="badge badge-pill" style="background-color: orange">Proc. Reg</span>
                      </a>
                    </li>
                  @endif
                  @if (in_array(session('auth')->proc_level, [4, 8, 99, 44]))
                    <li class="nav-item">
                      <a class="nav-link pl-3 {{ Request::path()=='progressList/13'?'active':'' }}" href="/progressList/13">
                        Dokumen&nbsp;({{ $countMenu->finance ?? 0 }})<span class="badge badge-pill" style="color: white;background-color: green;">Finance</span>
                      </a>
                    </li>
                  @endif
                </ul>
              </li>
            @endif
            @if (in_array(session('auth')->proc_level, [2, 3, 4, 5, 99, 44]))
            <li class="nav-item dropdown">
              <a href="#prov_online" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle nav-link">
                <i class="fe fe-archive fe-16"></i>
                <span class="ml-3 item-text">Provisioning Online </span>
              </a>
              <ul class="collapse list-unstyled pl-4 w-100" id="prov_online">
                <li class="nav-item">
                  <a class="nav-link pl-3" href="/Rekon/dashboard?startDate={{ date('Y-m-01') }}&endDate={{ date('Y-m-d') }}">Rekon Order</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link pl-3" href="/Rekon/rekonDashboardOrder?regional=REG6&witel=ALL&mitra=ALL&startDate={{ date('Y-m-01') }}&endDate={{ date('Y-m-d') }}">Rekon Order Based
                    &nbsp;<span class="badge badge-pill" style="color: white;background-color: black">Beta HO</span>
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link pl-3" href="/Rekon/provisioning/dashboard/order?startDate={{ date('Y-m-01') }}&endDate={{ date('Y-m-d') }}">Rekon Order Based 
                    &nbsp;<span class="badge badge-pill" style="color: white;background-color: black">Beta Kalimantan</span>
                  </a>
                </li>
              </ul>
            </li>
            @endif
          </ul>
          <p class="text-muted nav-heading mt-4 mb-1">
            <span>Tools</span>
          </p>
          <ul class="navbar-nav flex-fill w-100 mb-2">
            @if (in_array(session('auth')->proc_level, [4, 99, 44]))
              <li class="nav-item dropdown">
                <a href="#pembuatan" data-toggle="collapse" class="dropdown-toggle nav-link">
                  <i class="fe fe-edit fe-16"></i>
                  <span class="ml-3 item-text">Pembuatan</span>
                </a>
                <ul class="collapse list-unstyled pl-4 w-100 {{ in_array(Request::path(),['/tools/pid_main/create'])?'show':'' }}" id="pembuatan">
                  @if (in_array(session('auth')->proc_level, [4, 99, 44]))
                    <li class="nav-item"><a class="nav-link pl-3" href="/tools/pid_main/create">PID</a></li>
                  @endif
                </ul>
              </li>
            @endif
            @if (in_array(session('auth')->proc_level, [1, 3, 4, 7, 99, 44]))
              <li class="nav-item dropdown">
                <a href="#report" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle nav-link">
                  <i class="fe fe-book fe-16"></i>
                  <span class="ml-3 item-text">List&nbsp;</span>
                </a>
                <ul class="collapse list-unstyled pl-4 w-100 {{ in_array(Request::segment(1).'/'.Request::segment(2),['Report/pks','Report/sp','Report/rfc','Report/data','Report/tagihan','Report/list'])?'show':'' }}" id="report">
                @if (in_array(session('auth')->proc_level, [1, 4, 99, 44]))
                  <li class="nav-item"><a class="nav-link pl-3" href="/Report/dalapaBOQ">Dalapa</a></li>
                  <li class="nav-item"><a class="nav-link pl-3" href="/tools/data/mitra">Mitra</a></li>
                @endif
                @if (in_array(session('auth')->proc_level, [1, 4, 7, 99, 44]))
                  <li class="nav-item"><a class="nav-link pl-3" href="/Report/pid/list">PID</a></li>
                @endif
                @if (in_array(session('auth')->proc_level, [1, 3, 4, 99, 44]))
                  @if (in_array(session('auth')->proc_level, [1, 4, 99, 44]) )
                    <li class="nav-item"><a class="nav-link pl-3" href="/Report/rfc/detail/{{ date('Y-m-d',strtotime('first day of this month')) }}/{{ date('Y-m-d') }}">RFC</a></li>
                  @endif
                  @if (in_array(session('auth')->proc_level, [3, 4, 99, 44]) )
                    <li class="nav-item"><a class="nav-link pl-3" href="/Report/data/material">KHS</a></li>
                    <li class="nav-item"><a class="nav-link pl-3" href="/tools/hss_psb">HSS PSB</a></li>
                  @endif
                @endif
                @if (in_array(session('auth')->proc_level, [1, 4, 99, 44]))
                  <li class="nav-item"><a class="nav-link pl-3" href="/tools/list/user_dok">User Dokumen</a></li>
                @endif
                </ul>
              </li>
            @endif
            @if (in_array(session('auth')->proc_level, [1, 4, 7, 99, 44]))
              <li class="nav-item dropdown">
                <a href="#monitor" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle nav-link">
                  <i class="fe fe-eye fe-16"></i>
                  <span class="ml-3 item-text">Monitoring</span>
                </a>
                @if (in_array(session('auth')->proc_level, [1, 4, 7, 99, 44]))
                  <ul class="collapse list-unstyled pl-4 w-100 {{ in_array(Request::segment(1).'/'.Request::segment(2),['Report/pks','Report/sp','Report/rfc','Report/data','Report/tagihan','Report/list'])?'show':'' }}" id="monitor">
                    <li class="nav-item"><a class="nav-link pl-3" href="/monitor/budget">Budget</a></li>
                  </ul>
                @endif
              </li>
            @endif
          </ul>
          <ul class="navbar-nav flex-fill w-100 mb-2">
            <li class="nav-item w-100">
              <a class="nav-link" href="/searching">
                <i class="fe fe-search fe-16"></i>
                <span class="ml-3 item-text">Pencarian</span>
              </a>
            </li>
          </ul>
          <ul class="navbar-nav flex-fill w-100 mb-2">
            <li class="nav-item w-100">
              <a class="nav-link" href="/tools/summary_rfcr">
                <i class="fe fe-hexagon fe-16"></i>
                <span class="ml-3 item-text">Summary RFC/RFR</span>
              </a>
            </li>
          </ul>
          @if (in_array(session('auth')->id_user, [18940469, 18930155]) )
            <ul class="navbar-nav flex-fill w-100 mb-2">
              <li class="nav-item w-100">
                <a class="nav-link" href="/tools/list_planning_fixed">
                  <i class="fe fe-dollar-sign fe-16"></i>
                  <span class="ml-3 item-text">Planning Fixed Value</span>
                </a>
              </li>
            </ul>
          @endif
          @if (in_array(session('auth')->proc_level, [4, 7, 99, 44]))
            <ul class="navbar-nav flex-fill w-100 mb-2">
              <li class="nav-item w-100">
                <a class="nav-link" href="/comm/upload_po">
                  <i class="fe fe-briefcase fe-16"></i>
                  <span class="ml-3 item-text">Upload PO</span>
                </a>
              </li>
            </ul>
          @endif
          <p class="text-muted nav-heading mt-4 mb-1">
            <span>Super Admin</span>
          </p>
            <ul class="navbar-nav flex-fill w-100 mb-2">
              <li class="nav-item dropdown">
                <a href="#pengaturan" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle nav-link">
                  <i class="fe fe-settings fe-16"></i>
                  <span class="ml-3 item-text">Pengaturan</span>
                </a>
                <ul class="collapse list-unstyled pl-4 w-100 {{ in_array(Request::path(),['Admin/data/mitra', '/compare_rfc'])?'show':'' }}" id="pengaturan">
                  <li class="nav-item"><a class="nav-link pl-3" href="/Admin/compare_rfc">Compare RFC</a></li>
                  <li class="nav-item"><a class="nav-link pl-3" href="/Admin/rfc_wh_sync">Material RFC & WH</a></li>
                  <li class="nav-item"><a class="nav-link pl-3" href="/Admin/upload_file_mass">Upload File</a></li>
                </ul>
              </li>
            </ul>
        </nav>
      </aside>
      <main role="main" class="main-content">
        @yield('content')
      </main> <!-- main -->
    </div> <!-- .wrapper -->
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script> --}}
    <script src="/js/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/simplebar.min.js"></script>
    <script src="/js/jquery.stickOnScroll.js"></script>
    <script src="/js/tinycolor-min.js"></script>
    <script src="/js/config.js"></script>
    <script src="/js/jquery.validate.min.js"></script>
    <script src="/js/jquery.steps.min.js"></script>
    <script src="/js/apps.js"></script>
    @yield('footerS')
  </body>
</html>