@extends('layout')
@section('title', 'Dashboard - PROMISE')
@section('headerS')
<style>
	.centered {
		text-align: center;
		vertical-align: middle;
	}
  tr, th {
    text-align: center;
		vertical-align: middle;
  }
  .container-fluid * {
    font-size: 1em;!important;
  }



</style>

<link rel="stylesheet" href="/css/daterangepicker.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
@endsection
@section('content')
@if (Session::has('alerts'))
  @foreach(Session::get('alerts') as $alert)
    <div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
  @endforeach
@endif
<div class="container-fluid">
  <div class="row justify-content-center">
    <div class="col-12">
      <div class="row">
        <div class="col-md-12">
          <div class="card shadow mb-4">
            <div class="card-body">
              <p class="mb-2"><strong>Dashboard Log</strong></p>
              <form method="GET">
                <div class="form-row">
                  <div class="form-group col-md-3">
                    <label for="witel">WITEL</label>
                    <select class="form-control select2" id="witel" name="witel">
                      <option value="All">ALL</option>
                      @foreach ($witel_mod as $val)
                        <option value="{{ $val }}">{{ $val }}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="row col-md-4">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="tgl_select">PERIODE</label>
                        <select class="form-control" id="tgl_select">
                          <option value="Tahun">Tahun</option>
                          <option value="Custom">Custom Range</option>
                          <option value="all_sp">Semua Periode SP</option>
                          <option value="Bulan">Bulan</option>
                        </select>
                        <input type="hidden" name="tgl_hidden" id="hid_tgl">
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group isi_tgl">
                      </div>
                    </div>
                  </div>
                  @if (session('auth')->proc_level != 2)
                    <div class="form-group col-md-3">
                      <label for="mitra">MITRA</label>
                      <select class="form-control select2" id="mitra" name="mitra">
                      </select>
                    </div>
                  @endif
                  @if (empty(session('auth')->peker_pup))
                    <div class="form-group col-md-2">
                      <label for="khs">Pekerjaan</label>
                      <select class="form-control select2" id="khs" name="khs">
                        <option value="All">ALL</option>
                        @foreach ($data_pekerjaan as $val)
                          <option value="{{ $val->pekerjaan }}">{{ $val->pekerjaan }}</option>
                        @endforeach
                      </select>
                    </div>
                  @endif
                  <div class="form-group col-md-12">
                    <label for="search">&nbsp;</label>
                    <button type="submit" class="btn btn-primary btn-block btn_submit"><i class="fe fe-search fe-16"></i>&nbsp;SEARCH</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
        @if($req->all())
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="table table-bordered table-striped" style="width: 100%;">
                <thead class="thead-dark">
                  <tr role="row">
                    <th rowspan="2">NAMA MITRA</th>
                    <th colspan="7">AKTOR</th>
                    <th rowspan="2">TOTAL PER MITRA</th>
                    <th rowspan="2">ACH %</th>
                  </tr>
                  <tr>
                    <th>PROC. AREA</th>
                    <th>MITRA</th>
                    <th>OPERATION</th>
                    <th>PROC. REG</th>
                    <th>COMMERCE</th>
                    <th>FINNANCE</th>
                    <th>FINISH/DONE</th>
                  </tr>
                </thead>
                <tbody>
                  @php
                    $total_PA = $total_mitra = $total_operation = $total_pr = $total_commerce = $total_finnance = $total_finish = 0;
                  @endphp
                  @forelse ($tbl as $key => $val)
                    @foreach ($val as $kk => $vv)
                      @php
                        $total_PA        += $vv['PA'];
                        $total_mitra     += $vv['mitra'];
                        $total_operation += $vv['operation'];
                        $total_pr        += $vv['pr'];
                        $total_commerce  += $vv['commerce'];
                        $total_finnance  += $vv['finance'];
                        $total_finish    += $vv['finish'];
                      @endphp
                      <tr>
                        <td>{{ $kk }}</td>
                        <td>{!! $vv['PA'] != 0 ? '<a class="url_m" href="/Report/list/spM/'. $kk .'/1"> '. $vv['PA'] .'</a>' : '-' !!}</td>
                        <td>{!! $vv['mitra'] != 0 ? '<a class="url_m" href="/Report/list/spM/'. $kk .'/2"> '. $vv['mitra'] .'</a>' : '-' !!}</td>
                        <td>{!! $vv['operation'] != 0 ? '<a class="url_m" href="/Report/list/spM/'. $kk .'/3"> '. $vv['operation'] .'</a>' : '-' !!}</td>
                        <td>{!! $vv['pr'] != 0 ? '<a class="url_m" href="/Report/list/spM/'. $kk .'/6"> '. $vv['pr'] .'</a>' : '-' !!}</td>
                        <td>{!! $vv['commerce'] != 0 ? '<a class="url_m" href="/Report/list/spM/'. $kk .'/7"> '. $vv['commerce'] .'</a>' : '-' !!}</td>
                        <td>{!! $vv['finance'] != 0 ? '<a class="url_m" href="/Report/list/spM/'. $kk .'/8"> '. $vv['finance'] .'</a>' : '-' !!}</td>
                        <td>{!! $vv['finish'] != 0 ? '<a class="url_m" href="/Report/list/spM/'. $kk .'/finish"> '. $vv['finish'] .'</a>' : '-' !!}</td>
                        <td>{!! $vv['total'] != 0 ? '<a class="url_m" href="/Report/list/spM/'. $kk .'/all"> '. $vv['total'] .'</a>' : '-' !!}</td>
                        <td>{{ @ROUND( ($vv['finish'] / $vv['total']) * 100, 2 ) }} %</td>
                      </tr>
                    @endforeach
                  @empty
                  @endforelse
                </tbody>
                <tfoot>
                  <tr>
                    @php
                      $total_all = $total_PA + $total_mitra + $total_operation + $total_pr + $total_commerce + $total_finnance + $total_finish;
                    @endphp
                    <td>TOTAL</td>
                    <td>{!! $total_PA != 0 ? '<a class="url_m" href="/Report/list/spM/all/1"> '. $total_PA .'</a>' : '-' !!}</td>
                    <td>{!! $total_mitra != 0 ? '<a class="url_m" href="/Report/list/spM/all/2"> '. $total_mitra .'</a>' : '-' !!}</td>
                    <td>{!! $total_operation != 0 ? '<a class="url_m" href="/Report/list/spM/all/3"> '. $total_operation .'</a>' : '-' !!}</td>
                    <td>{!! $total_pr != 0 ? '<a class="url_m" href="/Report/list/spM/all/6"> '. $total_pr .'</a>' : '-' !!}</td>
                    <td>{!! $total_commerce != 0 ? '<a class="url_m" href="/Report/list/spM/all/7"> '. $total_commerce .'</a>' : '-' !!}</td>
                    <td>{!! $total_finnance != 0 ? '<a class="url_m" href="/Report/list/spM/all/8"> '. $total_finnance .'</a>' : '-' !!}</td>
                    <td>{!! $total_finish != 0 ? '<a class="url_m" href="/Report/list/spM/all/finish"> '. $total_finish .'</a>' : '-' !!}</td>
                    <td>{!! $total_all != 0 ? '<a class="url_m" href="/Report/list/spM/all/all"> '. $total_all .'</a>' : '-' !!}</td>
                    <td>{{ @ROUND( ($total_finish / ($total_all) ) *100, 2 ) }}%</td>
                  </tr>
                </tfoot>
              </table>
            </div>
          </div>
          <div class="col-md-12">
            <div class="card shadow">
              <div class="card-header">
                <strong class="card-title">Logging</strong>
              </div>
              <div class="card-body">
                <div class="list-group list-group-flush my-n3" style="overflow-y: auto; height:400px;">
                  @forelse ($log as $l)
                    <div class="list-group-item">
                      <div class="row align-items-center">
                        <div class="col-auto">
                          <span class="circle circle-sm bg-{{ is_float($l->step_id) || $l->ss == 'Rollback' ? 'warning' : ($l->step_id < 14 ? 'info' : ($l->step_id == 14 ? 'success' : 'primary' ) ) }}">
                            <i class="fe fe-{{ $l->ss == 'Rollback' ? 'arrow-down' : (is_float($l->step_id) ? 'alert-triangle' : ($l->step_id < 14 ? 'arrow-up' : ($l->step_id == 14 ? 'check' : 'archive' ) ) ) }} fe-16 text-white"></i>
                          </span>
                        </div>
                        <div class="col">
                          <small><strong>{{ $l->mitra_nm }} | {{ $l->created_at }}</strong></small>
                          <div class="mb-2">Pekerjaan : <strong>{{ $l->jenis_work }}</strong><br/>Witel: <strong>{{ $l->witel_new }}</strong><br/> Judul: <strong>{{ $l->judul }}</strong> <br/>Status: <strong>{{ $l->ss == 'Rollback' ? 'Dikembalikan' : (is_float($l->step_id) ? 'DITOLAK' : ($l->step_id < 14 ? 'LANJUT' : ($l->step_id == 14 ? 'SELESAI' : 'DIPROSES' ) ) ) }}</strong>
                            @if($l->keterangan != 'Data Rekon Selesai!')
                              <br/>Loker: <strong>{{ $l->keterangan }}</strong>
                            @endif
                          </div>
                          <span class="badge badge-pill badge-{{ is_float($l->step_id) ? 'warning' : 'success' }}">{{ $l->aktor }}</span>
                        </div>
                        {{-- <div class="col-auto pr-0">
                          <small class="fe fe-more-vertical fe-16 text-muted"></small>
                        </div> --}}
                      </div>
                    </div>
                  @empty
                  @endforelse
                </div>
              </div>
            </div>
          </div>
        @endif
      </div>
    </div>
  </div>
</div>
@endsection
@section('footerS')
<script src="/js/moment.min.js"></script>
<script src="/js/daterangepicker.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js@3.5.1/dist/chart.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@2.0.0"></script>
<script src='/js/select2.min.js'></script>
<script src='/js/jquery.timepicker.js'></script>
<script>
  var url = window.location,
  re_des = url.toString().split('?');

  if(re_des){
    $('.url_m').each(function( index ) {
      var link = $(this).attr('href');
      $(this).attr('href', link + '?' + re_des[1] )
    });
  }

  var mitra = {!! json_encode($mitra_modify) !!};
  $('#witel').val(null).change()
  $('#witel').on('select2:clear change', function() {
    var isi = $(this).val(),
    isi_select = [],
    pra_select = {};

    if(isi == 'KALSEL'){
      pra_select.KALSEL = mitra.KALSEL;
    }else if(isi == 'BALIKPAPAN'){
      pra_select.BALIKPAPAN = mitra.BALIKPAPAN;
    }else if(isi == 'All'){
      pra_select = mitra;
    }else{
      pra_select = {};
    }
    console.log(pra_select, mitra.KALSEL)
    if ($("#mitra").hasClass("select2-hidden-accessible")) {
      // $("#mitra").select2('destroy');
      $("#mitra").empty().change();
    }

    if(!$.isEmptyObject(pra_select)){
      $.each(pra_select, function(key, index){
        isi_select.push({
          id: 'all',
          text: 'ALL'
        })
        $.each(index, function(keyc1, indexc1){
          isi_select.push({
            id: indexc1.id,
            text: indexc1.text
          })
        })
      })
      if(isi_select)
      {
        $("#mitra").select2({
          theme: 'bootstrap4',
          width: '100%',
          data: isi_select
        }).change();

        var mitra_selected = {!! json_encode($req->mitra) !!};
        if(mitra_selected){
          $("#mitra").val(mitra_selected).change();
        }
      }
    }
    else
    {
      if ($("#mitra").hasClass("select2-hidden-accessible")) {
        $("#mitra").empty().change();
      }
    }
  })

  $('#khs').select2(
  {
    theme: 'bootstrap4',
    placeholder: 'Pilih Jenis Pekerjaan !',
    width: '100%'
  });

  $('#tgl_select').val(null).change()

  $('#tgl_select').select2(
  {
    theme: 'bootstrap4',
    placeholder: 'Pilih Jenis Periode',
    width: '100%'
  });

  $('#tgl_select').on('change', function(){
    var isi = $(this).val(),
    date_select = [];
    $('#hid_tgl').val(isi);

    if($.inArray(isi, ['Tahun', 'Bulan']) !== -1 ){
      $('.isi_tgl').children().remove();
      $('.isi_tgl').html("<select id='tgl' name='tgl' class='tgl' form-control'></select>");

      $('.hid_tgl').val(isi);
      if(isi == 'Tahun'){
        var start_year = new Date().getFullYear();

        for (var i = start_year; i >= 2021; i--) {
          date_select.push({
            id: i.toString(),
            text: i.toString()
          });
        }
      }

      if(isi == 'Bulan'){
        date_select = [
          {
            id: 1,
            text: 'Januari'
          },
          {
            id: 2,
            text: 'Februari'
          },
          {
            id: 3,
            text: 'Maret'
          },
          {
            id: 4,
            text: 'April'
          },
          {
            id: 5,
            text: 'Mei'
          },
          {
            id: 6,
            text: 'Juni'
          },
          {
            id: 7,
            text: 'Juli'
          },
          {
            id: 8,
            text: 'Agustus'
          },
          {
            id: 9,
            text: 'September'
          },
          {
            id: 10,
            text: 'Oktober'
          },
          {
            id: 11,
            text: 'November'
          },
          {
            id: 12,
            text: 'Desember'
          },
        ];
      }

      $('#tgl').select2({
        theme: 'bootstrap4',
        width: '100%',
        data: date_select
      }).change();
    }else if($.inArray(isi, ['Custom']) !== -1 ){
      $('.isi_tgl').children().remove();
      $('.isi_tgl').html("<input type='text' name='tgl' id='tgl' class='tgl form-control'>");
      $('.hid_tgl').val(isi);
      var start = moment().startOf('month'),
      end = moment().endOf('month');

      $('.tgl').daterangepicker(
      {
        locale: {
          format: 'YYYY-MM-DD'
        },
        startDate: start,
        endDate: end
      })
    }else{
      $('#hid_tgl').val('all');
      $('.isi_tgl').children().remove();
    }
  })

  $('#witel').select2(
  {
    theme: 'bootstrap4',
    placeholder: 'Pilih Witel !',
    width: '100%'
  });

  $('#mitra').select2(
  {
    theme: 'bootstrap4',
    placeholder: 'Pilih Mitra yang Dicari !',
    width: '100%'
  });

  var tgl = {!! json_encode($req->tgl) !!},
  witel = {!! json_encode($req->witel) !!},
  tgl = {!! json_encode($req->tgl) !!},
  tgl_hidden = {!! json_encode($req->tgl_hidden) !!},
  khs = {!! json_encode($req->khs) !!};

  if(witel != null)
  {
    $('#tgl').val(tgl).change();
    $('#khs').val(khs).change();
    $('#tgl_select').val(tgl_hidden).change();
    $('#witel').val(witel).change();
  }

</script>
@endsection